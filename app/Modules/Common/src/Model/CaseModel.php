<?php

namespace Common\Model;

use Common\Model\Relays\Relays;
use Forms\Model\CustomForms;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use App\Http\Helpers;
use Setting\Model\Setting;


class CaseModel extends Model
{
    protected $table = 'char_cases';

    protected static $rules;

    public static $_translator = [
        "noaa_albtak"=>'card_type',
        "rkm_alhoy" =>'id_card_number',
        "alasm_alaol_aarby"=>'first_name',
        "alasm_althany_aarby"=>'second_name',
        "alasm_althalth_aarby"=>'third_name',
        "alaaael_aarby"=>'last_name',
        "asm_alaaael_alsabk"=>'prev_family_name',
        "alasm_alaol_anjlyzy"=>'en_first_name',
        "alasm_althany_anjlyzy"=>'en_second_name',
        "alasm_althalth_anjlyzy"=>'en_third_name',
        "alaaael_anjlyzy"=>'en_last_name',
        "tarykh_almylad"=>'birthday',
        "mkan_almylad"=>'birth_place',
        "aljns"=>'gender',
        "aljnsy"=>'nationality',
        "aadd_alzojat"=>'spouses',
        "alhal_alajtmaaay"=>'marital_status_id',
        "hal_almoatn"=>'refugee',
        "rkm_krt_altmoyn"=>'unrwa_card_number',
        "aldol"=>'country',
        "almhafth"=>'governarate',
        "almdyn"=>'city',
        "almntk"=>'location_id',
        "almsjd"=>'mosques_id',
        "tfasyl_alaanoan"=>'street_address',
        "rkm_joal_asasy"=>'primary_mobile',
        "rkm_alotny"=>'wataniya',
        "rkm_joal_ahtyaty"=>'secondary_mobile',
        "rkm_hatf"=>'phone',
        "asm_albnk"=>'bank_id',
        "alfraa"=>'branch_name',
        "asm_sahb_alhsab"=>'account_owner',
        "rkm_alhsab"=>'account_number',
        "hl_yaaml"=>'working',
        "hl_taaml"=>'working',
        "kadr_aal_alaaml"=>'can_work',
        "sbb_aadm_alkdr"=>'work_reason_id',
        "hal_alaaml"=>'work_status_id',
        "fe_alajr"=>'work_wage_id',
        "alothyf"=>'work_job_id',
        "mkan_alaaml"=>'work_location',
        "aldkhl_alshhry_shykl"=>'monthly_income',

        "aadd_alghrf"=>'rooms',
        "almsah_balmtr"=>'area',
        "kym_alayjar"=>'rent_value',
        "noaa_almlky"=>'property_type_id',
        "mlky_almnzl"=>'property_type_id',
        "noaa_skf_almnzl"=>'roof_material_id',
        "skf_almnzl"=>'roof_material_id',

        "hal_almskn"=>'residence_condition',
        "hal_alathath"=>'indoor_condition',
        "kably_almnzl_llskn"=>'habitable',

        "odaa_almnzl"=>'house_condition',

        "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
        "asm_almoess_alty_oaadt_balbna"=>'organization_name',
        "ahtyajat_alaaael_bshd"=>'needs',
        "tosyat_albahth_alajtmaaay"=>'notes',
        "ydrs_ao_mtaalm"=>"study" ,
        "noaa_aldras"=>"type" ,
        "jh_aldras"=>"authority" ,
        "almoehl_alaalmy"=>"stage" ,
        "almrhl_aldrasy"=>"degree" ,
        "altkhss"=>"specialization" ,
        "alsf"=>"grade" ,
        "alsn_aldarsy"=>"year" ,
        "asm_almdrs"=>"school" ,
        "akhr_ntyj" =>"points" ,
        "almsto_althsyl"=>"level",
        "alhal_alshy"=>"condition",
        "almrd_ao_aleaaak"=>"disease_id",
        "tfasyl_alhal_alshy"=>"details",
        "tarykh_alzyar" => "visited_at",
        "asm_albahth_alajtmaaay" => "visitor",
        "hl_ysly"=>"prayer",
        "sbb_trk_alsla"=>"prayer_reason",
        "hl_yhfth_alkran"=>"save_quran",
        "aadd_alajza"=>"quran_parts",
        "aadd_alsor"=>"quran_chapters",
        "sbb_trk_althfyth"=>"quran_reason",
        "mlthk_balmsjd"=>"quran_center",
        "tarykh_ofa_alab"=>"quran_center",
        "tarykh_alofa"=>"death_date",
        "sbb_alofa"=>"death_cause_id",
        "sl_alkrab"=>"kinship_id",
        "hl_alam_maayl"=>"mother_is_guardian",
        "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
        "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
        "hl_bhaj_ltrmym_llmoaem" => "need_repair",
        "osf_odaa_almnzl" => "repair_notes",
        "hl_ldyh_tamyn_shy" => "has_health_insurance",
        "noaa_altamyn_alshy" => "health_insurance_type",
        "rkm_altamyn_alshy" => "health_insurance_number",
        "hl_ystkhdm_jhaz" => "has_device",
        "asm_aljhaz_almstkhdm" => "used_device_name",
        "almrd_ao_alaaaak" => "disease_id",
        "rkm_hoy_albahth" => "visitor_card",
        "ray_albahth_alajtmaaay" => "visitor_opinion",
        "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",
        "hl_ldyh_thmm_maly"=> "receivables",
        "kym_althmm_balshykl"=> "receivables_sk_amount",
    ];

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'person_id' => 'required',
                'category_id' => 'required',
                //'organization_id' => 'required',
            );
        }

        return self::$rules;
    }

    public function person()
    {
        return $this->belongsTo('Sponsorship\Model\Person','person_id', 'id');
    }

    public static function fetch($options = array(),$id)
    {
        $language_id = 1;
        $en_language_id= 2;

        $defaults = array(
            'action' => null,
            'full_name' => false,
            'marital_status_name' => false,
            'gender_name' => false,
            'category' => false,
            'category_type' => null,
            'category_name' => false,
            'organization_name' => false,
            'person' => false,
            'persons_i18n' => false,
            'gender' => false,
            'contacts' => false,
            'health' => false,
            'islamic' => false,
            'education' => false,
            'work' => false,
            'residence' => false,
            'case_needs' => false,
            'reconstructions' => false,
            'banks' => false,
            'home_indoor' => false,
            'financial_aid_source' => false,
            'non_financial_aid_source' => false,
            'persons_properties' => false,
            'persons_documents' => false ,
            'father' => false,
            'father_id' => false ,
            'mother' => false,
            'mother_id' => false ,
            'guardian' => false,
            'guardian_id' => false,
            'guardian_kinship' => false,
            'visitor_note' => false,
            'childs' => false,
            'wives' => false,
            'parents' => false,
            'family' => false,
            'lang' => 'ar'
        );

        $options = array_merge($defaults, $options);

        if (isset($options['lang'])) {
            if ($options['lang'] == 'en') {
                App::setLocale('en');
                $language_id = 2;
            }
        }

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $not_selected = trans('common::application.not_selected');
        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');
        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');
        $bad = trans('common::application.bad_condition');
        $very_bad = trans('common::application.very_bad_condition');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $is_father_ = trans('common::application.father');
        $is_mother_ = trans('common::application.mother');
        $the_same_person = trans('common::application.the_same_person');
        $other_person = trans('common::application.other_person');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $male = trans('common::application.male');
        $female = trans('common::application.female');


        if (isset($options['persons_documents'])) {
            if ($options['persons_documents']) {
                $option['category']=true;
                $option['father_id']=true;
                $option['mother_id']=true;
            }
        }else{
            $options['persons_documents']=false;
        }
        if (!isset($options['gender'])) {
            $options['gender']=false;
        }
        if (isset($options['visitor_note'])) {
            if ($options['visitor_note']) {
                $option['category']=true;
                $option['person']=true;
            }
        }
        if (isset($options['family_member'])) {
            if ($options['family_member']) {
                $option['category']=true;
                $option['father_id']=true;
                $option['mother_id']=true;
                $option['guardian_id']=true;
            }
        }else{
            $options['family_member']=false;
        }
        $query = self::query();
        $query->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id');
        $query->selectRaw("char_persons.family_cnt,char_persons.family_cnt as individual_count,
        char_persons.male_live,char_persons.female_live,char_persons.spouses,char_persons.death_date ,
        char_get_active_case_count(char_cases.person_id) as active_case_count,
        CASE WHEN  char_persons.birthday is null THEN ' ' Else 
                               EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age");

        $query->selectRaw("CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                    CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                    CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                    CASE
                                        WHEN char_cases.visitor_evaluation is null THEN ' '
                                        WHEN char_cases.visitor_evaluation = 0 THEN '$very_bad'
                                        WHEN char_cases.visitor_evaluation = 1 THEN '$bad'
                                        WHEN char_cases.visitor_evaluation = 2 THEN '$good'
                                        WHEN char_cases.visitor_evaluation = 3 THEN '$very_good'
                                        WHEN char_cases.visitor_evaluation = 4 THEN '$excellent'
                                        Else ' '
                                    END AS visitor_evaluation_,
                                    CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                    CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card,
                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                    CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                    CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                    CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card,
                                    char_cases.organization_id,
                                    CASE WHEN char_cases.visitor_evaluation = 0 THEN '*' Else ' ' END AS very_bad_condition_evaluation,
                                    CASE WHEN char_cases.visitor_evaluation = 1 THEN '*' Else ' ' END AS bad_condition_evaluation,
                                    CASE WHEN char_cases.visitor_evaluation = 2 THEN '*' Else ' ' END AS good_evaluation,
                                    CASE WHEN char_cases.visitor_evaluation = 3 THEN '*' Else ' ' END AS very_good_evaluation,
                                    CASE WHEN char_cases.visitor_evaluation = 4 THEN '*' Else ' ' END AS excellent_evaluation   
                                    ");

        if($options['category_type'] == 1 || $options['guardian_id'] || $options['guardian']) {
            $query->leftjoin('char_guardians', function($q) use ($language_id){
                $q->on('char_guardians.individual_id','=','char_cases.person_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            });
            $query->selectRaw("char_guardians.guardian_id,char_guardians.kinship_id,char_guardians.individual_id");
            $query->selectRaw("CASE WHEN char_guardians.guardian_id =  char_cases.person_id THEN '$the_same_person'
                                    WHEN char_guardians.guardian_id =  char_persons.father_id THEN '$is_father_'
                                    WHEN char_guardians.guardian_id =  char_persons.mother_id THEN '$is_mother_'
                                    Else '$other_person'
                               END AS guardian_is");


            $query->selectRaw("CASE WHEN char_guardians.guardian_id =  char_persons.mother_id THEN '$yes'
                                    Else '$no'
                               END AS mother_is_guardian");

        }

        if ($options['action']=='show') {
            $query->selectRaw("
                                     CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                     CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                     CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card,
                                     
                                     CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                     CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                     CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS notes,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'father',null) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'father',null) END AS father_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'father',1) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'father',1) END AS male_father_brothers,                                                                                    CASE WHEN char_get_siblings_count(char_cases.person_id,'father',2) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'father',2) END AS female_father_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'mother',null) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'mother',null) END AS mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'mother',1) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'mother',1) END AS male_mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'mother',2) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'mother',3) END AS female_mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'both',null) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'both',null) END AS brothers,
                                     CASE WHEN char_get_children_count(char_cases.person_id,null) is null THEN 0
                                          Else char_get_children_count(char_cases.person_id,null) END AS children,
                                     CASE WHEN char_get_children_count(char_cases.person_id,1) is null THEN 0
                                          Else char_get_children_count(char_cases.person_id,1) END AS male_children,
                                     CASE WHEN char_get_children_count(char_cases.person_id,2) is null THEN 0
                                          Else char_get_children_count(char_cases.person_id,2) END AS female_children,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'both',1) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'both',1) END AS male_brothers,
                                     CASE WHEN char_get_siblings_count(char_cases.person_id,'both',2) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'both',2) END AS female_brothers
                               ");
        }

        if ($options['category_name'] || $options['category_type'] || $options['category'] || $options['visitor_note']) {
            $category_type = $options['category_type'];

            $query->join('char_categories', function ($join) use ($category_type) {
                $join->on('char_cases.category_id', '=', 'char_categories.id');
            })->addSelect(array('char_categories.name AS category_name',
                'char_categories.type as category_type',
                'char_categories.case_is',
                'char_categories.family_structure',
                'char_categories.has_mother',
                'char_categories.father_dead',
                'char_categories.has_Wives'));

            if($options['category_type'] == 1) {
                $query->selectRaw("CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_persons.father_id
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_cases.person_id
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_guardians.guardian_id
                               END AS l_person_id,
                               CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,null,char_persons.id )
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,null,char_persons.id )
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,char_persons.id )
                                            END AS family_count,
                                            CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,1,char_persons.id )
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,1,char_persons.id )
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,1,char_persons.id )
                                            END AS male_count,
                                            CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,2,char_persons.id )
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,2,char_persons.id )
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,2,char_persons.id )
                                            END AS female_count") ;
            }
            else{
                $query->selectRaw("char_persons.family_cnt AS family_count,char_persons.male_live AS male_count,char_persons.female_live AS female_count");
            }

        }

        if ($options['guardian_id'] || $options['guardian'] || $options['guardian_kinship']) {

            $query->leftjoin('char_persons as guardian', 'char_guardians.guardian_id', '=', 'guardian.id');

            if ($options['guardian']) {
                $query->selectRaw("CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',ifnull(guardian.third_name, ' '),' ', ifnull(guardian.last_name,' ')) AS guardian_name");
            }
            if ($options['guardian_kinship']) {
                $query->leftjoin('char_kinship_i18n', function($join) use ($language_id) {
                    $join->on('char_guardians.kinship_id', '=', 'char_kinship_i18n.kinship_id')
                        ->where('char_kinship_i18n.language_id', $language_id);
                })
                    ->addSelect(array('char_kinship_i18n.name AS guardian_kinship'));
            }
        }

        if ($options['organization_name']) {
            $query->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
                ->selectRaw("CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name");
        }

        if ($options['person'] || $options['gender'] || $options['full_name'] || $options['gender_name']|| $options['marital_status_name']
            || $options['father_id'] || $options['mother_id']|| $options['father'] || $options['mother']  || $options['visitor_note']) {

            if ($options['gender_name']) {
                $query->selectRaw("CASE
                                       WHEN char_persons.gender is null THEN ' '
                                       WHEN char_persons.gender = 1 THEN '$male'
                                       WHEN char_persons.gender = 2 THEN '$female'
                                 END
                                 AS gender_name");
            }

            if ($options['father']) {
                $query->leftjoin('char_persons as father', 'char_persons.father_id', '=', 'father.id');
                $query->selectRaw("CONCAT(ifnull(father.first_name, ' '), ' ' ,ifnull(father.second_name, ' '),' ',ifnull(father.third_name, ' '),' ', ifnull(father.last_name,' ')) AS father_name");
            }

            if ($options['mother']) {
                $query->leftjoin('char_persons as mother', 'char_persons.mother_id', '=', 'mother.id');
                $query->selectRaw("CONCAT(ifnull(mother.first_name, ' '), ' ' ,ifnull(mother.second_name, ' '),' ',ifnull(mother.third_name, ' '),' ', ifnull(mother.last_name,' ')) AS mother_name");
            }

            if ($options['gender']) {
                $query->selectRaw("char_persons.gender");
            }

            if ($options['father_id']) {
                $query->selectRaw("char_persons.father_id");
            }
            if ($options['mother_id']) {
                $query->selectRaw("char_persons.mother_id");
            }

            if ($options['person']){
                if ($options['action']=='show') {
                    $query->selectRaw("CASE WHEN char_persons.street_address  is null THEN ' ' Else char_persons.street_address END AS street_address");
                    $query->selectRaw("CASE WHEN char_persons.actual_monthly_income  is null
                                                THEN ' ' Else char_persons.actual_monthly_income END AS actual_monthly_income");

                    $query->leftjoin('char_marital_status_i18n', function($join) use ($language_id) {
                        $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                            ->where('char_marital_status_i18n.language_id', $language_id);
                    })
                        ->selectRaw("CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status_name");


                    $query->leftjoin('char_locations_i18n As birth_place_name', function($join) use ($language_id) {
                        $join->on('char_persons.birth_place', '=','birth_place_name.location_id' )
                            ->where('birth_place_name.language_id', $language_id);
                    })
                        ->selectRaw("CASE WHEN char_persons.birth_place is null THEN ' ' Else birth_place_name.name  END  AS birth_place");

                    $query->leftjoin('char_locations_i18n As nationality_name', function($join) use ($language_id)  {
                        $join->on('char_persons.nationality', '=','nationality_name.location_id' )
                            ->where('nationality_name.language_id', $language_id);

                    })
                        ->selectRaw("CASE WHEN char_persons.nationality is null THEN ' ' Else nationality_name.name  END  AS nationality");


                    if($options['category_type'] == 1) {
                        $query->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                            $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                                ->where('mosques_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name");

                        $query->leftjoin('char_locations_i18n As location_name', function($join) use ($language_id)  {
                            $join->on('char_persons.location_id', '=','location_name.location_id' )
                                ->where('location_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.location_id is null THEN ' ' Else location_name.name END   AS nearLocation");

                        $query->leftjoin('char_locations_i18n As city_name', function($join) use ($language_id)  {
                            $join->on('char_persons.city', '=','city_name.location_id' )
                                ->where('city_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN city_name.name  is null THEN ' ' Else city_name.name END   AS city");

                        $query->leftjoin('char_locations_i18n As district_name', function($join)  use ($language_id) {
                            $join->on('char_persons.governarate', '=','district_name.location_id' )
                                ->where('district_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN district_name.name  is null THEN ' ' Else district_name.name END   AS governarate");

                        $query->leftjoin('char_locations_i18n As country_name', function($join)  use ($language_id) {
                            $join->on('char_persons.country', '=','country_name.location_id' )
                                ->where('country_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN country_name.name  is null THEN ' ' Else country_name.name END   AS country");

                        $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' ',
                        ifnull(district_name.name,' '),' ',
                        ifnull(city_name.name,' '),' ', 
                        ifnull(location_name.name,' '),' ',
                        ifnull(mosques_name.name,' '),' ',
                        ifnull(char_persons.street_address,' '))
                                            AS address");

                    }else{

                        $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                            $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                                ->where('country_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country");


                        $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                            $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                                ->where('district_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate");

                        $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                            $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                                ->where('region_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adsregion_id is null THEN ' ' Else region_name.name END   AS region_name");

                        $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                            $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                                ->where('location_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation");

                        $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                            $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                                ->where('square_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name");

                        $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                            $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                                ->where('mosques_name.language_id', $language_id);
                        })
                            ->selectRaw("CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name");

                        $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),' _ ',
                                                            ifnull(region_name.name,' '),' _ ',
                                                            ifnull(location_name.name,' '),' _ ',
                                                            ifnull(square_name.name,' '),' _ ',
                                                            ifnull(mosques_name.name,' '),' _ ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address");

                    }


                    $query->selectRaw("CASE WHEN char_persons.street_address  is null THEN ' ' Else char_persons.street_address END AS street_address");
                    $query->selectRaw("
                                   CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                   CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                   CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport '
                                     END
                                     AS card_type,
                                   CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                   CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                   CASE
                                       WHEN char_persons.refugee is null THEN '$not_selected'
                                       WHEN char_persons.refugee = 0 THEN '$not_selected'
                                       WHEN char_persons.refugee = 1 THEN ' $refugee '
                                       WHEN char_persons.refugee = 2 THEN '$citizen'
                                   END
                                   AS refugee,
                                   CASE WHEN char_persons.refugee = 2 THEN '$yes' Else '$no' END AS is_refugee,
                                   CASE WHEN char_persons.refugee = 2 THEN '*' Else ' ' END AS per_refugee,
                                   CASE WHEN char_persons.refugee = 1 THEN '$yes' Else '$no' END AS is_citizen,
                                   CASE WHEN char_persons.refugee = 1 THEN '*' Else ' ' END AS per_citizen,
                                   CASE
                                       WHEN char_persons.unrwa_card_number is null THEN ' '
                                       WHEN char_persons.refugee is null THEN ' '
                                       WHEN char_persons.refugee = 0 THEN ' '
                                       WHEN char_persons.refugee = 1 THEN ' '
                                       WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                 END
                                 AS unrwa_card_number,
                                 char_persons.gender as gender_id,
                                 CASE
                                       WHEN char_persons.gender is null THEN ' '
                                       WHEN char_persons.gender = 1 THEN '$male'
                                       WHEN char_persons.gender = 2 THEN '$female'
                                 END
                                 AS gender,
                                 CASE WHEN char_persons.gender = 1 THEN '*' Else ' ' END AS gender_male,         
                                 CASE WHEN char_persons.gender = 2 THEN '*' Else ' ' END AS gender_female,
                                  CASE
                                       WHEN char_persons.deserted is null THEN '$no'
                                       WHEN char_persons.deserted = 1 THEN '$yes'
                                       WHEN char_persons.deserted = 0 THEN '$no'
                                 END
                                 AS deserted,
                                 CASE WHEN (char_persons.deserted is null or char_persons.deserted = 1 ) THEN '*'  
                                      Else ' ' END AS is_deserted,         
                                 CASE WHEN char_persons.deserted = 0 THEN '*' Else ' ' END AS not_deserted,
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                                 CASE WHEN char_persons.first_name is null THEN ' ' Else char_persons.first_name  END  AS first_name,
                                 CASE WHEN char_persons.first_name is null THEN ' ' Else char_persons.first_name  END  AS first_name,
                                 CASE WHEN char_persons.second_name is null THEN ' ' Else char_persons.second_name  END  AS second_name,
                                 CASE WHEN char_persons.third_name is null THEN ' ' Else char_persons.third_name  END  AS third_name,
                                 CASE WHEN char_persons.last_name is null THEN ' ' Else char_persons.last_name  END  AS last_name,
                                 CASE WHEN char_persons.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                 CASE WHEN char_persons.receivables = 1 THEN '*' Else ' ' END AS is_receivables,         
                                 CASE WHEN char_persons.receivables = 0 THEN '*' Else ' ' END AS not_receivables,
                                 CASE
                                      WHEN char_persons.receivables is null THEN '0'
                                      WHEN char_persons.receivables = 0 THEN '0'
                                      WHEN char_persons.receivables = 1 THEN char_persons.receivables_sk_amount
                                 END AS receivables_sk_amount ,
                                        CASE WHEN char_persons.is_qualified = 1 THEN '$qualified' Else '$not_qualified' END AS is_qualified,
                                        CASE WHEN char_persons.is_qualified = 1 THEN '*' Else ' ' END AS qualified,         
                                        CASE WHEN char_persons.is_qualified != 1 THEN '*' Else ' ' END AS not_qualified,
                                         CASE WHEN char_persons.qualified_card_number is null THEN '-' Else char_persons.qualified_card_number  END  AS qualified_card_number,
                                 CASE WHEN char_persons.has_commercial_records = 1 THEN '$yes' Else '$no' END AS has_commercial_records,
                                 CASE WHEN char_persons.has_commercial_records = 1 THEN '*' Else ' ' END AS have_commercial_records,         
                                 CASE WHEN char_persons.has_commercial_records = 0 THEN '*' Else ' ' END AS not_have_commercial_records,
                                 CASE
                                      WHEN char_persons.has_commercial_records is null THEN '0'
                                      WHEN char_persons.has_commercial_records = 0 THEN '0'
                                      WHEN char_persons.has_commercial_records = 1 THEN char_persons.active_commercial_records
                                 END AS active_commercial_records ,
                                  CASE
                                      WHEN char_persons.has_commercial_records is null THEN ' '
                                      WHEN char_persons.has_commercial_records = 0 THEN ' '
                                      WHEN char_persons.has_commercial_records = 1 THEN char_persons.gov_commercial_records_details
                                 END AS gov_commercial_records_details
                               ");

                    $query->selectRaw("CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status_name");

                }
                else{
                    $query->addSelect('char_persons.*');
                    $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name");
                }
            }
            if ($options['full_name']) {
                $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name");
            }

        }

        if ($options['persons_i18n']) {
            $query->leftjoin('char_persons_i18n', function($join)  use ($en_language_id) {
                $join->on('char_cases.person_id', '=','char_persons_i18n.person_id')
                    ->where('char_persons_i18n.language_id',$en_language_id);
            });
            if ($options['action']=='show') {

                $query->selectRaw("CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                   CASE WHEN char_persons_i18n.first_name  is null THEN ' ' Else char_persons_i18n.first_name  END  AS en_first_name,
                                   CASE WHEN char_persons_i18n.second_name is null THEN ' ' Else char_persons_i18n.second_name END  AS en_second_name,
                                   CASE WHEN char_persons_i18n.third_name  is null THEN ' ' Else char_persons_i18n.third_name  END  AS en_third_name,
                                   CASE WHEN char_persons_i18n.last_name   is null THEN ' ' Else char_persons_i18n.last_name   END  AS en_last_name
                                 ");
            }
            else{
                $query->selectRaw("char_persons_i18n.first_name   AS en_first_name,
                                   char_persons_i18n.second_name  AS en_second_name,
                                   char_persons_i18n.third_name   AS en_third_name,
                                   char_persons_i18n.last_name    AS en_last_name
                                 ");
            }
        }

        if ($options['contacts']) {
            $query->leftjoin('char_persons_contact as primary_mob_number', function($join) {
                $join->on('char_cases.person_id', '=','primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type','primary_mobile');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN primary_mob_number.contact_value is null THEN ' ' Else primary_mob_number.contact_value  END  AS primary_mobile");
            }else{
                $query->selectRaw("primary_mob_number.contact_value  AS primary_mobile");
            }

            $query->leftjoin('char_persons_contact as wataniya_num', function($join) {
                $join->on('char_cases.person_id', '=','wataniya_num.person_id')
                    ->where('wataniya_num.contact_type','wataniya');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya");
            }else{
                $query->selectRaw("wataniya_num.contact_value  AS wataniya");
            }

            $query->leftjoin('char_persons_contact as secondary_mob_number', function($join) {
                $join->on('char_cases.person_id', '=','secondary_mob_number.person_id')
                    ->where('secondary_mob_number.contact_type','secondary_mobile');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN secondary_mob_number.contact_value is null THEN ' ' Else secondary_mob_number.contact_value  END  AS secondary_mobile");
            }else{
                $query->selectRaw("secondary_mob_number.contact_value  AS secondary_mobile");
            }


            $query->leftjoin('char_persons_contact as phone_number', function($join) {
                $join->on('char_cases.person_id', '=','phone_number.person_id')
                    ->where('phone_number.contact_type','phone');
            });
            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN phone_number.contact_value is null THEN ' ' Else phone_number.contact_value  END  AS phone");
            }else{
                $query->selectRaw("phone_number.contact_value  AS phone");
            }

        }

        if ($options['work']) {
            $query->leftjoin('char_persons_work','char_cases.person_id', '=','char_persons_work.person_id');

            if($options['action']=='show'){

                $query->selectRaw("
                                 CASE WHEN char_persons_work.gov_work_details is null THEN ' ' 
                                      Else char_persons_work.gov_work_details  END  AS gov_work_details,
                                 CASE WHEN char_persons_work.working is null THEN ' '
                                        WHEN char_persons_work.working = 1 THEN '$work'
                                        WHEN char_persons_work.working = 2 THEN '$not_work'
                                        END
                                  AS working,
                                  CASE WHEN char_persons_work.working is null THEN ' '
                                        WHEN char_persons_work.working = 1 THEN '$yes'
                                        WHEN char_persons_work.working = 2 THEN '$no'
                                        END
                                  AS works, 
                                  CASE WHEN char_persons_work.has_other_work_resources is null THEN ' '
                                        WHEN char_persons_work.has_other_work_resources = 1 THEN '$yes'
                                        WHEN char_persons_work.has_other_work_resources = 0 THEN '$no'
                                        END
                                  AS has_other_work_resources,
                                  CASE WHEN char_persons_work.has_other_work_resources = 1 THEN '*' Else ' ' END AS has_other_resources,         
                                  CASE WHEN char_persons_work.has_other_work_resources is null THEN '*'
                                       WHEN char_persons_work.has_other_work_resources != 1 THEN '*' Else ' ' END AS not_has_other_resources,                                       
                                  CASE WHEN char_persons_work.working = 1 THEN '*' Else ' ' END AS is_working,         
                                  CASE WHEN char_persons_work.working is null THEN '*'
                                       WHEN char_persons_work.working != 1 THEN '*' Else ' ' END AS not_working         
                                  ");

                $is_can_work = trans('common::application.is_can_work');
                $not_can_work = trans('common::application.not_can_work');

                $query->selectRaw("CASE WHEN char_persons_work.can_work is null THEN ' '
                                       WHEN char_persons_work.can_work = 1 THEN '$is_can_work'
                                       WHEN char_persons_work.can_work = 2 THEN '$not_can_work'
                                       END 
                                  AS can_work,
                                  CASE WHEN char_persons_work.can_work is null THEN ' '
                                       WHEN char_persons_work.can_work = 1 THEN '$yes'
                                       WHEN char_persons_work.can_work = 2 THEN '$no'
                                       END 
                                  AS can_works,
                                   CASE WHEN char_persons_work.can_work = 1 THEN '*' Else ' ' END AS can_working,         
                                  CASE WHEN char_persons_work.can_work is null THEN '*'
                                       WHEN char_persons_work.can_work = 2 THEN '*' Else ' ' END AS can_not_working         
                                  ");

                $query->selectRaw("CASE WHEN char_persons_work.working is null THEN ' '
                                       WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                       WHEN char_persons_work.working = 2 THEN ' '
                                       END
                                  AS work_location");

                $query->leftjoin('char_work_jobs_i18n', function($join) use ($language_id){
                    $join->on('char_persons_work.work_job_id', '=',  'char_work_jobs_i18n.work_job_id')
                        ->where('char_work_jobs_i18n.language_id', $language_id);
                })
                    ->selectRaw("CASE  WHEN char_persons_work.working is null THEN ' '
                                                    WHEN char_persons_work.working = 1 THEN char_work_jobs_i18n.name
                                                    WHEN char_persons_work.working = 2 THEN ' '
                                                    END
                                              AS work_job");


                $query->leftjoin('char_work_status_i18n', function($join) use ($language_id) {
                    $join->on('char_persons_work.work_status_id', '=',  'char_work_status_i18n.work_status_id')
                        ->where('char_work_status_i18n.language_id', $language_id);
                })
                    ->selectRaw("CASE  WHEN char_persons_work.working is null THEN ' '
                                                   WHEN char_persons_work.working = 1 THEN char_work_status_i18n.name
                                                   WHEN char_persons_work.working = 2 THEN ' '
                                                   END
                                             AS work_status");

                $query->leftjoin('char_work_wages',function($join) use ($language_id) {
                    $join->on('char_persons_work.work_wage_id', '=', 'char_work_wages.id');
                })->selectRaw("CASE
                                                       WHEN char_persons_work.working is null THEN ' '
                                                       WHEN char_persons_work.working = 1 THEN char_work_wages.name
                                                       WHEN char_persons_work.working = 2 THEN ' '
                                                 END
                                                 AS work_wage");

                $query ->leftjoin('char_work_reasons_i18n', function($join) use ($language_id)  {
                    $join->on('char_persons_work.work_reason_id', '=', 'char_work_reasons_i18n.work_reason_id')
                        ->where('char_work_reasons_i18n.language_id', $language_id);
                })->selectRaw("CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN ' '
                                           WHEN char_persons_work.can_work = 2 THEN char_work_reasons_i18n.name
                                       END AS work_reason");
            }else{
                $query->addSelect('char_persons_work.*');
            }


        }

        if ($options['residence']) {
            $query->leftjoin('char_residence', 'char_cases.person_id', '=','char_residence.person_id');

            if($options['action']=='show'){

                $query ->leftjoin('char_roof_materials_i18n', function($join) use ($language_id){
                    $join->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id')
                        ->where('char_roof_materials_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_roof_materials_i18n.name is null THEN ' ' Else char_roof_materials_i18n.name END   AS roof_materials");

                $query->leftjoin('char_property_types_i18n', function ($join) use ($language_id){
                    $join->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id')
                        ->where('char_property_types_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_property_types_i18n.name is null THEN ' ' Else char_property_types_i18n.name END   AS property_types");

                $query->leftjoin('char_building_status_i18n', function($join) use ($language_id){
                    $join->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition')
                        ->where('char_building_status_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition");

                $query->leftjoin('char_house_status_i18n', function($join)  use ($language_id){
                    $join->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition')
                        ->where('char_house_status_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition");

                $query->leftjoin('char_furniture_status_i18n', function($join)  use ($language_id){
                    $join->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition')
                        ->where('char_furniture_status_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition");

                $query ->leftjoin('char_habitable_status_i18n', function($join)  use ($language_id){
                    $join->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable')
                        ->where('char_habitable_status_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable");

                $query ->selectRaw("CASE WHEN char_residence.rooms is null THEN ' ' Else char_residence.rooms  END  AS rooms");
                $query->selectRaw("CASE WHEN char_residence.area is null THEN ' ' Else char_residence.area  END  AS area");
                $query->selectRaw("CASE WHEN char_residence.rent_value is null       THEN ' ' 
                                                Else char_residence.rent_value  END  AS rent_value ,
                                             CASE WHEN char_residence.repair_notes is null       THEN ' ' 
                                                Else char_residence.repair_notes  END  AS repair_notes ,
                                            CASE WHEN char_residence.need_repair is null THEN ' '
                                                WHEN char_residence.need_repair = 1 THEN '$yes'
                                                WHEN char_residence.need_repair = 2 THEN '$no'
                                                END
                                            AS need_repair,
                                          CASE WHEN char_residence.need_repair = 1 THEN '*' Else ' ' END AS repair,         
                                          CASE WHEN char_residence.need_repair = 2 THEN '*' Else ' ' END AS not_repair
                                   ");

            }else{
                $query->addSelect('char_residence.*');
            }


        }

        if ($options['education']) {
            $query->leftjoin('char_persons_education', 'char_cases.person_id', '=','char_persons_education.person_id');

            if($options['action']=='show'){
                $query->selectRaw("  CASE  WHEN char_persons_education.study is null THEN ' '
                                           WHEN char_persons_education.study = 1 THEN '$yes'
                                           WHEN char_persons_education.study = 0 THEN '$no'
                                    END  AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.authority = 1 THEN ' '
                                                               WHEN char_persons_education.authority = 2 THEN 'X'
                                                               WHEN char_persons_education.authority = 3 THEN ' '
                                                                Else ' '
                                                     END
                                                     AS authority_is_paid, 
                                                     CASE
                                                               WHEN char_persons_education.authority = 1 THEN 'X'
                                                               WHEN char_persons_education.authority = 2 THEN ' '
                                                               WHEN char_persons_education.authority = 3 THEN 'X'
                                                                Else ' '
                                                     END
                                                     AS authority_is_not__paid,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.specialization is null THEN ' ' Else char_persons_education.specialization END  AS specialization,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school
                                             ");
                $query->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)use ($language_id){
                    $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                    $q->where('char_edu_stages.language_id', $language_id);
                })->selectRaw("CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage");

                $query->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q)use ($language_id){
                    $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                    $q->where('char_edu_authorities.language_id', $language_id);
                })->selectRaw("CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority");

                $query->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q)use ($language_id){
                    $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                    $q->where('char_edu_degrees.language_id', $language_id);
                })->selectRaw("CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree");


            }else{
                $query->addSelect('char_persons_education.*');
            }
        }

        if($options['islamic']) {

            $regularly = trans('common::application.regularly');
            $does_not_pray = trans('common::application.does_not_pray');
            $sometimes = trans('common::application.sometimes');

            $query->leftjoin('char_persons_islamic_commitment', 'char_cases.person_id', '=', 'char_persons_islamic_commitment.person_id');
            if($options['action']=='show') {
                $query->SelectRaw("CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                     WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                     WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                     WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                               END AS prayer,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                               END AS save_quran,
                                CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN 'X'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN 'X'
                                    Else ' '
                               END AS is_not_save_quran,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN ' '
                                    Else 'X'
                               END AS is_save_quran,
                               CASE WHEN char_persons_islamic_commitment.quran_center is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    WHEN char_persons_islamic_commitment.quran_center = 1 THEN '$yes'
                                    Else '$yes'
                               END AS quran_center,
                               CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' 
                                    Else char_persons_islamic_commitment.prayer_reason 
                               END AS prayer_reason,
                               CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' 
                                    Else char_persons_islamic_commitment.quran_reason 
                               END AS quran_reason,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' 
                                    Else char_persons_islamic_commitment.quran_parts
                               END  AS quran_parts,
                               CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' 
                                    Else char_persons_islamic_commitment.quran_chapters 
                               END AS quran_chapters
                              ");

            }else{
                $query->addSelect('char_persons_islamic_commitment.*');
            }
        }

        if($options['health']){
            $query->leftjoin('char_persons_health', 'char_cases.person_id', '=','char_persons_health.person_id');

            if($options['action']=='show') {
                $query->leftjoin('char_diseases_i18n', function ($q)  use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS diseases_name");

                $query->selectRaw("CASE WHEN char_persons_health.condition is null THEN ' '
                                                  WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                  WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                  WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                           END AS health_status,
                                          CASE WHEN char_persons_health.condition = 1 THEN '*' Else ' ' END AS good_health_status,
                                          CASE WHEN char_persons_health.condition = 2 THEN '*' Else ' ' END AS chronic_disease,
                                          CASE WHEN char_persons_health.condition = 3 THEN '*' Else ' ' END AS disabled,
                                          CASE WHEN char_persons_health.condition = 1 THEN 'X' Else ' ' END AS has_good_health,
                                          CASE WHEN char_persons_health.condition is null THEN ' '
                                                  WHEN char_persons_health.condition = 1 THEN ' '
                                                  WHEN char_persons_health.condition = 2 THEN 'X'
                                                  WHEN char_persons_health.condition = 3 THEN 'X'
                                           END AS has_disease,
                                          CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS health_details,
                                          CASE
                                                  WHEN char_persons_health.has_health_insurance is null THEN ' '
                                                  WHEN char_persons_health.has_health_insurance = 1 THEN '$yes'
                                                  WHEN char_persons_health.has_health_insurance = 0 THEN '$no'
                                          END  AS has_health_insurance,
                                          CASE WHEN char_persons_health.has_health_insurance = 1 THEN '*' Else ' ' END AS have_health_insurance,         
                                          CASE WHEN char_persons_health.has_health_insurance = 0 THEN '*' Else ' ' END AS not_have_health_insurance,
                                 
                                          CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' Else char_persons_health.health_insurance_number END   AS health_insurance_number,
                                          CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' Else char_persons_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_persons_health.has_device is null THEN ' '
                                                          WHEN char_persons_health.has_device = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                          CASE WHEN char_persons_health.has_device = 1 THEN '*' Else ' ' END AS use_device,         
                                          CASE WHEN char_persons_health.has_device = 0 THEN '*' Else ' ' END AS not_use_device,
                                          CASE WHEN char_persons_health.used_device_name is null THEN ' ' Else char_persons_health.used_device_name END   AS used_device_name,
                                          CASE WHEN char_persons_health.gov_health_details is null THEN ' ' Else char_persons_health.gov_health_details END   AS gov_health_details
                                                    ");
            }else{
                $query->selectRaw('char_persons_health.*,char_persons_health.condition as health_condition');
            }
        }

        if ($options['case_needs']) {
            $query->leftjoin('char_case_needs', 'char_cases.id', '=','char_case_needs.case_id');

            if($options['action']=='show'){
                $query->selectRaw("CASE WHEN char_case_needs.needs is null       THEN ' ' Else char_case_needs.needs  END  AS needs");
            }else{
                $query->addSelect('char_case_needs.*');
            }

        }

        if ($options['reconstructions']) {
            $query->leftjoin('char_reconstructions', 'char_cases.id', '=','char_reconstructions.case_id');

            if($options['action']=='show'){


                $not_promise = trans('common::application.not_promise');
                $promise = trans('common::application.promise');

                $query->selectRaw("CASE WHEN char_reconstructions.promised = 2 THEN '$yes'  Else '$no'  END AS promised,
                                   CASE WHEN char_reconstructions.promised = 2 THEN '$promise' Else '$not_promise' END AS if_promised,
                                   CASE WHEN char_reconstructions.promised = 2 THEN '*' Else ' ' END AS was_promised,
                                   CASE WHEN char_reconstructions.promised = 2 THEN ' ' Else '*' END AS not_promised,
                                   CASE WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name Else ' ' END 
                                   AS reconstructions_organization_name");
            }else{
                $query->addSelect(array('char_reconstructions.organization_name as reconstructions_organization_name','char_reconstructions.promised'));
            }
        }

        $return =$query->selectRaw("char_cases.id,
                char_cases.person_id,
                char_cases.organization_id,
                char_cases.category_id,
                char_cases.level as case_level,
                char_cases.notes,
                char_cases.last_updated_at,
                char_cases.visited_at,
                char_cases.visitor_evaluation,
                char_cases.visitor_opinion,
                char_cases.visitor_card,
                char_cases.visitor,
                char_cases.status,
                char_cases.rank, 
                char_cases.user_id,
                char_cases.created_at, 
                char_cases.updated_at,
                char_cases.deleted_at ,
                char_cases.id AS case_id")->where('char_cases.id', '=', $id)->first();

        if($return){

            $return->has_image=PersonDocument::hasPersonalImagePath($return->person_id,$return->category_type);
            $return->family_member=[];
            $return->banks=[];
            $return->home_indoor=[];
            $return->financial_aid_source=[];
            $return->non_financial_aid_source=[];
            $return->persons_properties=[];
            $return->persons_documents=[];
            $person_id=$return->person_id;
            $category_id=$return->category_id;

            if ($options['family_member']) {
                $l_person_family_member=$person_id;

                if($return->category_type == 1){
                    if($return->case_is == 1 && $return->family_structure == 2){
                        if(!(is_null($return->father_id) || $return->father_id == '')){
                             $l_person_family_member=$return->father_id;
                         }
                    }else if($return->case_is  == 2 && $return->family_structure == 1){
                        
                         if(!(is_null($return->guardian_id) || $return->guardian_id == '')){
                             $l_person_family_member=$return->guardian_id;
                         }
                    }

                }

                $guardian_id =$return->guardian_id;
                $return->l_person_id=$l_person_family_member;
                $kinship_ids = [2,22,21,29];
                if ($options['childs']){
                    $kinship_ids = [2,22];
                }
                if ($options['wives']){
                    $kinship_ids = [21,29];
                }

                if ($options['parents']){
                    $kinship_ids = [1,5];
                }

                if($options['action']=='show' || $return->category_type == 2){
                    $family_member=\DB::table('char_persons')
                        ->leftjoin('char_locations_i18n As L', function($q)use ($language_id){
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id', $language_id);
                        })
                        ->join('char_persons_kinship', function($q) use($l_person_family_member,$kinship_ids,$return,$options){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            if($return->category_type == 1){
                                $q->whereIn('char_persons_kinship.kinship_id', $kinship_ids);
                            }else{
                                if ($options['childs']){
                                    $q->whereRaw("(char_persons_kinship.kinship_id in ( 2,22) and char_persons.marital_status_id = 10)");
                                }else {
                                    if ($options['parents']){
                                        $q->whereRaw("(char_persons_kinship.kinship_id in ( 1,5))");
                                    }else{
                                        if ($options['wives']){
                                            $q->whereRaw("(char_persons_kinship.kinship_id in ( 21,29))");
                                        }else{
                                            $q->whereRaw("(char_persons_kinship.kinship_id in ( 21,29) or 
                                          (char_persons_kinship.kinship_id in ( 2,22) and char_persons.marital_status_id = 10))");
                                        }

                                    }
                                }
                            }
                            
                            if(!(is_null($l_person_family_member) || $l_person_family_member == '')){
                               $q->where('char_persons_kinship.l_person_id', '=', $l_person_family_member);                                
                            }
                        })
                        ->leftjoin('char_kinship_i18n', function($q)use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id', $language_id);
                        });

                    $family_member->leftjoin('char_persons_kinship as guardians_persons_kinship', function($q) use($guardian_id){
                        $q->on('guardians_persons_kinship.l_person_id', '=', 'char_persons.id');
                        $q->where('guardians_persons_kinship.r_person_id', '=', $guardian_id);
                    })
                        ->leftjoin('char_kinship_i18n As guardians_kinship', function($q)use ($language_id){
                            $q->on('guardians_kinship.kinship_id', '=', 'guardians_persons_kinship.kinship_id');
                            $q->where('guardians_kinship.language_id', $language_id);
                        });
                    $family_member->leftjoin('char_marital_status_i18n', function($q)use ($language_id){
                        $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                        $q->where('char_marital_status_i18n.language_id', $language_id);
                    })
                        ->leftjoin('char_persons_contact as mob', function ($join) {
                            $join->on('mob.person_id', '=', 'char_persons.id')
                                ->where('mob.contact_type', 'primary_mobile');
                        })
                        ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_health', 'char_persons_health.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_diseases_i18n', function ($q)  use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id', $language_id);
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)use ($language_id){
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id', $language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id) {
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id', $language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q)use ($language_id){
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id', $language_id);
                        })
                        ->leftjoin('char_work_jobs_i18n', function($join)  use ($language_id){
                            $join->on('char_persons_work.work_job_id', '=',  'char_work_jobs_i18n.work_job_id')
                                ->where('char_work_jobs_i18n.language_id', $language_id);
                        });

                    $family_member->whereRaw("char_persons.id not in ( $return->person_id ,$l_person_family_member )");
                    if($return->category_type == 1 && $return->case_is = 1 && $return->family_structure= 2){
                        if($return->mother_id != null){
                            $family_member->where('char_persons.id', '!=', $return->mother_id);
                        }
                    }
                    $family_member->selectRaw("char_persons.id as id,
                                        char_persons_kinship.l_person_id,
                                        char_persons_kinship.kinship_id,
                                                     CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                                     CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                                     CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                                     CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                                     CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                     CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name  END  AS birth_place,
                                                     char_persons.death_date,
                                                     char_persons.marital_status_id,
                                                     CASE WHEN  char_persons.birthday is null THEN ' ' Else  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age,
                                                     CASE WHEN char_persons.marital_status_id is null THEN ' '  Else char_marital_status_i18n.name END AS marital_status,
                                                     CASE WHEN char_persons.gender is null THEN ' '
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                     END AS gender,
                                                     CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                      CASE  WHEN char_persons_education.currently_study is null THEN '-'
                                                            WHEN char_persons_education.currently_study = 1 THEN '$yes'
                                                            WHEN char_persons_education.currently_study = 0 THEN '$no'
                                                         END
                                                      AS currently_study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS study_type,
                                                     CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                                 WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                    END
                                                     AS grade,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.school is null THEN ' '   Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_stages.name is null THEN ' '  Else char_edu_stages.name END AS stage,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' '  Else char_edu_authorities.name END AS authority,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                     CASE WHEN char_persons_work.working is null THEN ' '
                                                           WHEN char_persons_work.working = 1 THEN '$work'
                                                           WHEN char_persons_work.working = 2 THEN '$not_work'
                                                           END
                                                      AS working,
                                                      CASE  WHEN char_persons_work.working is null THEN ' '
                                                           WHEN char_persons_work.work_job_id is null THEN ' '
                                                           WHEN char_persons_work.working = 1 THEN char_work_jobs_i18n.name
                                                           WHEN char_persons_work.working = 2 THEN ' '
                                                        END
                                                   AS work_job,
                                                   CASE WHEN mob.contact_value is null THEN ' ' Else mob.contact_value  END  AS primary_mobile,
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                      CASE WHEN char_persons_health.details is null THEN ' '   Else char_persons_health.details END  AS diseases_details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS diseases_name,
                                                     char_kinship_i18n.name as kinship_name
                                                   ");


                    if($return->category_type == 1){
                        $family_member->selectRaw("CASE WHEN guardians_kinship.name is null THEN ' ' Else guardians_kinship.name  END  AS guardian_kinship");
                        $family_member->selectRaw("CASE WHEN guardians_kinship.name is null THEN ' ' Else guardians_kinship.name  END  AS kinship_name");
                    }

                    $return->family_member =  $family_member->orderBy('char_persons.birthday')->get();
                }
                else{

                    $organization_id=$return->organization_id;
                    $return->family_member=[];

                    $fm_cases=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_family_member,$options){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_family_member);
                        })
                        ->join('char_cases', function($q) use($organization_id){
                            $q->on('char_cases.person_id', '=', 'char_persons.id')
                                ->where('char_cases.organization_id', '=', $organization_id)
                                ->whereNull('char_cases.deleted_at');
                            ;
                        })
                        ->join('char_categories as ca', function($q){
                            $q->on('ca.id', '=', 'char_cases.category_id');
                            $q->where('ca.type','=',1);
                        })
                        ->join('char_kinship_i18n', function($q)use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id', $language_id);
                        })
                        ->leftjoin('char_marital_status_i18n', function($q)use ($language_id){
                            $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                            $q->where('char_marital_status_i18n.language_id', $language_id);
                        })
                        ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_health', 'char_persons_health.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)use ($language_id){
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id', $language_id);
                        })
                        ->where('char_persons.id', '!=', $return->person_id);

                    if($return->category_type == 1 && $return->case_is = 1 && $return->family_structure= 2){
                        if($return->mother_id != null){
                            $fm_cases->where('char_persons.id', '!=', $return->mother_id);
                        }
                    }

                    $fm_cases=$fm_cases->selectRaw("char_cases.id as case_id,char_cases.organization_id,
                                                ca.name as category_name,
                                                                  char_persons_kinship.kinship_id,
  char_persons.id as id,char_persons_kinship.l_person_id,
                                                     CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                                     CASE WHEN char_edu_stages.name is null THEN ' '  Else char_edu_stages.name END AS edu_stages,
                                 CASE WHEN  char_persons.birthday is null THEN ' ' Else  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age,
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons.gender is null THEN ' '
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                     END AS gender,
                                                     CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                                     CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                                     CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d') END  AS birthday,
                                                     CASE WHEN char_persons.marital_status_id is null THEN ' '  Else char_marital_status_i18n.name END AS marital_status,
                                                     char_kinship_i18n.name as kinship_name
                                                   ")->orderBy('char_persons.birthday')->get();

                    $fm=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_family_member,$options){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_family_member);
                        })
                        ->join('char_kinship_i18n', function($q)use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id', $language_id);
                        })
                        ->leftjoin('char_marital_status_i18n', function($q)use ($language_id){
                            $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                            $q->where('char_marital_status_i18n.language_id', $language_id);
                        })
                        ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_health', 'char_persons_health.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)use ($language_id){
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id', $language_id);
                        })

                        ->whereNotin('char_persons.id', function($subQuery)use($organization_id){
//                        $query->select('char_cases.person_id')->from('char_cases');
                            $subQuery->select('char_cases.person_id')
                                ->where('char_cases.organization_id','=', $organization_id )
                                ->whereNull('deleted_at')
                                ->from('char_cases');
//                        $query->select('person_id')->from('char_cases')->where('deleted_at', Null);
                        });


                    if($return->category_type == 1 && $return->case_is = 1 && $return->family_structure= 2){
                        if($return->mother_id != null){
                            $fm->where('char_persons.id', '!=', $return->mother_id);
                        }
                    }

                    $fm=$fm
                        ->where('char_persons.id', '!=', $return->person_id)
                        ->selectRaw("char_persons.id as id,char_persons_kinship.l_person_id,
                                                          char_persons_kinship.kinship_id,
               CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                                     CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                                     CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                                     CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                     CASE WHEN char_persons.marital_status_id is null THEN ' '  Else char_marital_status_i18n.name END AS marital_status,
                                                     CASE WHEN char_edu_stages.name is null THEN ' '  Else char_edu_stages.name END AS edu_stages,
                                 CASE WHEN  char_persons.birthday is null THEN ' ' Else  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age,
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                          CASE WHEN char_persons.gender is null THEN ' '
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                     END AS gender,
                                                     char_kinship_i18n.name as kinship_name
                                                   ")
                        ->orderBy('char_persons.birthday')
                        ->get();

                    $return->family_member=['fm' => $fm ,'fm_cases' => $fm_cases];
                }

            }
            if ($options['banks']) {
                $return->banks=\Common\Model\PersonModels\PersonBank::getPersonBanks($return->person_id,$return->organization_id);
            }
            if ($options['home_indoor']) {

                $home_indoor =\DB::table('char_essentials')
                    ->leftjoin('char_cases_essentials', function($q) use($id){
                        $q->on('char_cases_essentials.essential_id', '=', 'char_essentials.id');
                        $q->where('char_cases_essentials.case_id', '=', $id);
                    })
                    ->orderBy('char_essentials.name')
                    ->selectRaw("char_essentials.id as essential_id,char_essentials.name,char_cases_essentials.needs");

                if($options['action']=='show') {
                    $home_indoor->selectRaw("CASE WHEN char_cases_essentials.condition = 0 THEN '$not_exist'  Else '$exist' END AS  exist,
                                             CASE WHEN char_cases_essentials.condition = 0 THEN '$not_exist'
                                                  WHEN char_cases_essentials.condition = 1 THEN '$very_bad_condition'
                                                  WHEN char_cases_essentials.condition = 2 THEN '$bad_condition'
                                                  WHEN char_cases_essentials.condition = 3 THEN '$good'
                                                  WHEN char_cases_essentials.condition = 4 THEN '$very_good'
                                                  WHEN char_cases_essentials.condition = 5 THEN '$excellent'
                                             END AS essentials_condition,
                                             CASE WHEN char_cases_essentials.condition = 0 THEN '*' Else ' ' END AS not_exist,
                                             CASE WHEN char_cases_essentials.condition = 1 THEN '*' Else ' ' END AS very_bad_condition,
                                             CASE WHEN char_cases_essentials.condition = 2 THEN '*' Else ' ' END AS bad_condition,
                                             CASE WHEN char_cases_essentials.condition = 3 THEN '*' Else ' ' END AS good,
                                             CASE WHEN char_cases_essentials.condition = 4 THEN '*' Else ' ' END AS very_good,
                                             CASE WHEN char_cases_essentials.condition = 5 THEN '*' Else ' ' END AS excellent                                             
                                             
                                             
                                             ");

                }else{
                    $home_indoor->selectRaw("char_cases_essentials.condition");

                }
                $return->home_indoor=$home_indoor->get();

            }
            if ($options['financial_aid_source']) {
                $aid_type=1;
                $financial_aid_source= \DB::table('char_aid_sources')
                    ->leftjoin('char_persons_aids', function($join) use($person_id,$aid_type){
                        $join->on('char_persons_aids.aid_source_id', '=', 'char_aid_sources.id')
                            ->where('char_persons_aids.person_id', '=', $person_id)
                            ->where('char_persons_aids.aid_type', '=', $aid_type);
                    })
                    ->orderBy('char_aid_sources.name')
                    ->selectRaw("char_persons_aids.*,
                                                           char_aid_sources.name,
                                                           char_aid_sources.id as aid_source_id");

                if($options['action']=='show') {
                    $financial_aid_source->leftjoin('char_currencies','char_persons_aids.currency_id' , '=','char_currencies.id')
                        ->selectRaw("
                                                            CASE WHEN char_persons_aids.aid_value is null THEN '$no' 
                                                                 WHEN char_persons_aids.aid_take = 0 THEN '$no'  Else '$yes' END AS  aid_take,
                                                                 
                                                            CASE WHEN char_persons_aids.aid_value is null THEN ' '
                                                                 WHEN char_persons_aids.aid_take = 0 THEN ' '
                                                                 WHEN char_currencies.name is null THEN ' '  
                                                                 Else char_currencies.name END 
                                                            AS  currency_name,                                                                 
                                                            CASE WHEN char_persons_aids.aid_value is null THEN '0' 
                                                                 WHEN char_persons_aids.aid_take = 0 THEN '0'
                                                                 WHEN char_persons_aids.aid_value is null THEN '0'  
                                                                 Else char_persons_aids.aid_value END 
                                                            AS  aid_value,   
                                                            CASE WHEN char_persons_aids.aid_value is null THEN '*' 
                                                                 WHEN char_persons_aids.aid_take = 0 THEN '*'  Else ' ' END 
                                                            AS  not_take ,
                                                            CASE WHEN char_persons_aids.aid_value is not null THEN '*' 
                                                               WHEN char_persons_aids.aid_take = 1 THEN '*'  Else ' ' END 
                                                            AS  take
                                                         ");
                }

                $return->financial_aid_source=$financial_aid_source->get();



            }
            if ($options['non_financial_aid_source']) {
                $aid_type= 2;
                $non_financial_aid_source= \DB::table('char_aid_sources')
                    ->leftjoin('char_persons_aids', function($join) use($person_id,$aid_type){
                        $join->on('char_persons_aids.aid_source_id', '=', 'char_aid_sources.id')
                            ->where('char_persons_aids.person_id', '=', $person_id)
                            ->where('char_persons_aids.aid_type', '=', $aid_type);
                    })
                    ->orderBy('char_aid_sources.name')
                    ->selectRaw("char_persons_aids.*,
                                                           char_aid_sources.name,
                                                           char_aid_sources.id as aid_source_id");

                if($options['action']=='show') {
                    $non_financial_aid_source->selectRaw("CASE WHEN char_persons_aids.aid_value is null THEN '$no' 
                                                           WHEN char_persons_aids.aid_take = 0 THEN '$no'  Else '$yes' END 
                                                      AS  aid_take,
                                                      CASE WHEN char_persons_aids.aid_value is null THEN '0' 
                                                           WHEN char_persons_aids.aid_take = 0 THEN '0'
                                                           WHEN char_persons_aids.aid_value is null THEN '0'  
                                                          Else char_persons_aids.aid_value END 
                                                      AS  aid_value,   
                                                      CASE WHEN char_persons_aids.aid_value is null THEN '*' 
                                                           WHEN char_persons_aids.aid_take = 0 THEN '*'  Else ' ' END 
                                                      AS  not_take ,
                                                      CASE WHEN char_persons_aids.aid_value is not null THEN '*' 
                                                           WHEN char_persons_aids.aid_take = 1 THEN '*'  Else ' ' END 
                                                      AS  take
                                                     ");
                }

                $return->non_financial_aid_source=$non_financial_aid_source->get();



            }
            if ($options['persons_properties']) {
                $persons_properties= \DB::table('char_properties_i18n')
                    ->leftjoin('char_persons_properties', function($join) use($person_id){
                        $join->on('char_properties_i18n.property_id', '=', 'char_persons_properties.property_id')
                            ->where('char_persons_properties.person_id', '=', $person_id);
                    })
                    ->where('char_properties_i18n.language_id', $language_id)
                    ->orderBy('char_properties_i18n.name')
                    ->selectRaw("char_properties_i18n.*");


                if($options['action']=='show') {
                    $persons_properties->selectRaw("CASE WHEN char_persons_properties.has_property is null THEN '$not_exist' 
                                                     WHEN char_persons_properties.has_property = 1 THEN '$exist'  
                                                     Else '$not_exist' END 
                                                AS  has_property,
                                                CASE WHEN char_persons_properties.has_property is null THEN '0' 
                                                     WHEN char_persons_properties.has_property = 0 THEN 0  
                                                     WHEN char_persons_properties.quantity is null THEN 0  
                                                     Else char_persons_properties.quantity END
                                                AS  quantity,
                                                CASE WHEN char_persons_properties.has_property is null THEN '*' 
                                                     WHEN char_persons_properties.has_property = 1 THEN ' '  
                                                     Else '*' END 
                                                AS  not_exist ,
                                                CASE WHEN char_persons_properties.has_property is not null THEN '*' 
                                                     WHEN char_persons_properties.has_property = 1 THEN '*'  
                                                     Else ' ' END 
                                                AS  exist");
                }else{
                    $persons_properties->selectRaw("char_persons_properties.has_property,char_persons_properties.quantity");
                }
                $return->persons_properties=$persons_properties->get();
            }
            if ($options['persons_documents']) {
                $documents=array();
                $scope=0;
                if($return->category_type ==1) {
                    $scope=3;
                }
                $persons_documents=\Common\Model\CategoriesDocument::getPersonCategoryDocuments($category_id,$person_id,$scope,$options['action'],$language_id);

                if(sizeof($persons_documents) != 0){
                    foreach ($persons_documents as $doc){
                        array_push($documents,$doc);
                    }
                }
                if($return->category_type ==1){
                    if($return->father_id){
                        $father_documents=\Common\Model\CategoriesDocument::getPersonCategoryDocuments($category_id,$return->father_id,1,$options['action'],$language_id);

                        if(sizeof($father_documents) != 0){
                            foreach ($father_documents as $doc){
                                array_push($documents,$doc);
                            }
                        }
                    }
                    if($return->mother_id){
                        $mother_documents=\Common\Model\CategoriesDocument::getPersonCategoryDocuments($category_id,$return->mother_id,2,$options['action'],$language_id);
                        $return->persons_documents=$mother_documents;
                        if(sizeof($mother_documents) != 0){
                            foreach ($mother_documents as $doc){
                                array_push($documents,$doc);
                            }
                        }
                    }
                    if($return->guardian_id){
                        $guardian_documents=\Common\Model\CategoriesDocument::getPersonCategoryDocuments($category_id,$return->guardian_id,4,$options['action'],$language_id);
                        if(sizeof($guardian_documents) != 0){
                            foreach ($guardian_documents as $doc){
                                array_push($documents,$doc);
                            }
                        }
                    }
                }

                $return->persons_documents=$documents;

            }
            if ($options['visitor_note']) {

                $return->visitor_note='';
                $return->visitor_note_status=false;


                if($return->category_type ==1){
                    $visitor_note=\Common\Model\SponsorshipsCases::getVisitorNotes($id,$return->category_id,$return->organization_id,$return->gender_id,$options['lang']);
                }else{
                    $visitor_note=\Common\Model\AidsCases::getVisitorNotes($id,$return->category_id,$return->organization_id,$return->gender_id,$options['lang']);
                }

                if($visitor_note['status'] != false){
                    $return->visitor_note_status=true;
                    $return->visitor_note=$visitor_note['note'];
                }

            }

        }

        App::setLocale('ar');
        return $return;
    }

    protected function filters()
    {
        return array(
            'id' => ['column' => 'char_cases.id', 'table' => 'char_cases'],
            'category_id' => ['column' => 'char_cases.category_id', 'table' => 'char_cases'],
            'organization_id' => ['column' => 'char_cases.organization_id', 'table' => 'char_cases'],
            'person_id' => ['column' => 'char_cases.person_id', 'table' => 'char_cases'],
            'rank' => ['column' => 'char_cases.rank', 'table' => 'char_cases'],

            'father_id_card' => ['column' => 'father.id_card_number', 'table' => 'char_persons AS father'],
            'mother_id_card' => ['column' => 'mother.id_card_number', 'table' => 'char_persons AS mother'],
            'guardian_id_card' => ['column' => 'guardian.id_card_number', 'table' => 'char_persons AS guardian'],
        );
    }

    public function getPersonFullName()
    {
        if (!isset($this->first_name)) {
            return '';
        }
        return sprintf('%s %s %s %s', $this->first_name, $this->second_name, $this->third_name, $this->last_name);
    }

    public static function getInCompleteCases($page,$type,$filters,$organization_id,$action){

        $language_id =  \App\Http\Helpers::getLocale();
        $filter = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','category_id','visitor'];
        $condition = [];
        $query=null;
        $all_organization=null;

        foreach ($filters as $key => $value) {
            if(in_array($key, $filter)) {
                if ( $value != "" ) {
                    if($type =='aids') {
                        $data = ['char_aid_cases_incomplete_data.' . $key => $value];
                    }else{
                        $data = ['char_sponsorship_cases_incomplete_data.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
        }

        if($type =='aids'){
            $query= \DB::table('char_aid_cases_incomplete_data')
                ->whereNull('char_aid_cases_incomplete_data.case_deleted_at');
        }else{
            $query= \DB::table('char_sponsorship_cases_incomplete_data')
//                ->leftjoin('char_guardians', function($q) use ($language_id){
//                    $q->on('char_sponsorship_cases_incomplete_data.person_id', '=', 'char_guardians.individual_id')
//                        ->on('char_sponsorship_cases_incomplete_data.organization_id', '=', 'char_guardians.organization_id')
//                        ->where('char_guardians.status', '=',1);
//                })
                ->whereNull('char_sponsorship_cases_incomplete_data.case_deleted_at');
        }

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = [
                    'char_aid_cases_incomplete_data.first_name', 'char_aid_cases_incomplete_data.second_name',
                    'char_aid_cases_incomplete_data.third_name', 'char_aid_cases_incomplete_data.last_name',
                    'char_sponsorship_cases_incomplete_data.first_name', 'char_sponsorship_cases_incomplete_data.second_name',
                    'char_sponsorship_cases_incomplete_data.third_name', 'char_sponsorship_cases_incomplete_data.last_name'
                ];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
                if($filters['organization_ids'][0]==""){
                    unset($filters['organization_ids'][0]);
                }
                $organizations =$filters['organization_ids'];
            }
            if(!empty($organizations)){

                if($type =='aids'){
                    $query->wherein('char_aid_cases_incomplete_data.organization_id',$organizations);
                }else{
                    $query->wherein('char_sponsorship_cases_incomplete_data.organization_id',$organizations);
                }

            }else{
                if($type =='aids'){
                    $query->where('char_aid_cases_incomplete_data.organization_id',$organization_id);
                }else{
                    $query->where('char_sponsorship_cases_incomplete_data.organization_id',$organization_id);
                }
            }
        }
        elseif($all_organization ==1){
            if($type =='aids'){
                $query->where('char_aid_cases_incomplete_data.organization_id',$organization_id);
            }else{
                $query->where('char_sponsorship_cases_incomplete_data.organization_id',$organization_id);
            }
        }

        if($type !='aids'){
            $query->selectRaw("char_sponsorship_cases_incomplete_data.*");
        }
        $itemsCount = ($filters['itemsCount'] !== null)?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page);
    }

    public static function getCaseAttachments($page,$filters,$organization_id,$status,$type)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $char_cases = ['category_id']; //        'organization_id'
        $condition = [];
        $category_type = $filters['category_type'];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ($value != "") {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_cases)) {

                if ($value != "") {
                    $data = ['char_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $language_id =  \App\Http\Helpers::getLocale();

        $query = self::query()->addSelect('char_cases.*');
        $query->selectRaw("get_document_type(char_cases.person_id,char_cases.category_id) as person_dcoument");

        $query->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->selectRaw("CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
            CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
            
            char_persons.father_id ,
            char_persons.mother_id ,
         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name
                         ");

        $query->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
            ->selectRaw("CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name ");

        $query->join('char_categories', function ($join) use ($category_type) {
            $join->on('char_cases.category_id', '=', 'char_categories.id');
            $join->where('char_categories.type', $category_type);
        })->selectRaw("CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                     char_categories.type as category_type");


        if ((int)$category_type == 1) {
            $query->leftjoin('char_guardians', function ($join) {
                $join->on('char_cases.person_id', '=', 'char_guardians.individual_id')
                    ->on('char_guardians.organization_id', '=', 'char_cases.organization_id')
                    ->where('char_guardians.status',1);
            })->leftjoin('char_persons AS g', 'g.id', '=', 'char_guardians.guardian_id');

            $query->orderBy('char_persons.first_name', 'g.first_name');
            $query->selectRaw("char_guardians.guardian_id,CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name ,
                               get_document_type(char_persons.father_id,char_cases.category_id) as father_dcoument,
                               get_document_type(char_persons.mother_id,char_cases.category_id) as mother_dcoument,
                               get_document_type(g.id,char_cases.category_id) as guardian_dcoument");
        } else {
            $query->orderBy('char_persons.first_name');
        }


        $all_organization = null;
        if (isset($filters['all_organization'])) {
            if ($filters['all_organization'] === true || $filters['all_organization'] === 'true') {


                $organizations = [];
                if (isset($filters['organization_ids']) && $filters['organization_ids'] != null && $filters['organization_ids'] != "" &&
                    $filters['organization_ids'] != [] && sizeof($filters['organization_ids']) != 0
                ) {

                    if ($filters['organization_ids'][0] == "") {
                        unset($filters['organization_ids'][0]);
                    }
                    $organizations = $filters['organization_ids'];
                }

                $organization_category=[];
                if(isset($filters['organization_category']) && $filters['organization_category'] !=null &&
                    $filters['organization_category'] !="" && $filters['organization_category'] !=[] && sizeof($filters['organization_category']) != 0) {
                    if($filters['organization_category'][0]==""){
                        unset($filters['organization_category'][0]);
                    }
                    $organization_category = $filters['organization_category'];
                }

                if (!empty($organizations)) {
                    $query->where(function ($anq) use ($organizations,$organization_category) {
                        $anq->wherein('char_cases.organization_id', $organizations);
                        if(!empty($organization_category)){
                            $anq->wherein('char_organizations.category_id',$organization_category);
                        }
                    });
                }
                else {
                    $user = \Auth::user();
                    $UserType=$user->type;

                    if($UserType == 2) {
                        $query->where(function ($anq) use ($organization_id,$user,$organization_category) {
                            $anq->where('char_cases.organization_id',$organization_id);
                            $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                                $quer->select('organization_id')
                                    ->from('char_user_organizations')
                                    ->where('user_id', '=', $user->id);
                            });
                            if(!empty($organization_category)){
                                $anq->wherein('char_organizations.category_id',$organization_category);
                            }
                        });

                    }else{

                        $query->where(function ($anq) use ($organization_id,$organization_category) {
                            $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                                $decq->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                            if(!empty($organization_category)){
                                $anq->wherein('char_organizations.category_id',$organization_category);
                            }
                        });
                    }

                }

            } elseif ($filters['all_organization'] == false) {
                $query->where('char_cases.organization_id', $organization_id);
            }
        }

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = ['char_persons.last_name', 'char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('char_cases.created_at', '>=', $date_from);
            $query = $query->whereDate('char_cases.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('char_cases.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('char_cases.created_at', '<=', $date_to);
        }
        $query->whereNull('char_cases.deleted_at');

        if ($status == 'exportToExcel') {

            $query->leftjoin('char_persons_contact as primary_mob_number', function ($join) {
                $join->on('char_cases.person_id', '=', 'primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type', 'primary_mobile');
            })
                ->selectRaw("CASE WHEN primary_mob_number.contact_value is null THEN ' ' Else primary_mob_number.contact_value  END  AS primary_mobile");

            $query->leftjoin('char_persons_contact as secondary_mob_number', function ($join) {
                $join->on('char_cases.person_id', '=', 'secondary_mob_number.person_id')
                    ->where('secondary_mob_number.contact_type', 'secondary_mobile');
            })
                ->selectRaw("CASE WHEN secondary_mob_number.contact_value is null THEN ' ' Else secondary_mob_number.contact_value  END  AS secondary_mobile");

            $query->leftjoin('char_persons_contact as phone_number', function ($join) {
                $join->on('char_cases.person_id', '=', 'phone_number.person_id')
                    ->where('phone_number.contact_type', 'phone');
            })
                ->selectRaw("CASE WHEN phone_number.contact_value is null THEN ' ' Else phone_number.contact_value  END  AS phone");

            if ($category_type == 1) {
                $query->leftjoin('char_persons_contact as guardian_primary_mob_number', function ($join) {
                    $join->on('char_guardians.guardian_id', '=', 'guardian_primary_mob_number.person_id')
                        ->where('guardian_primary_mob_number.contact_type', 'primary_mobile');
                })
                    ->selectRaw("CASE WHEN guardian_primary_mob_number.contact_value is null THEN ' ' Else guardian_primary_mob_number.contact_value  END  AS guardian_primary_mobile");

                $query->leftjoin('char_persons_contact as guardian_secondary_mob_number', function ($join) {
                    $join->on('char_guardians.guardian_id', '=', 'guardian_secondary_mob_number.person_id')
                        ->where('guardian_secondary_mob_number.contact_type', 'secondary_mobile');
                })
                    ->selectRaw("CASE WHEN guardian_secondary_mob_number.contact_value is null THEN ' ' Else guardian_secondary_mob_number.contact_value  END  AS guardian_secondary_mobile");

                $query->leftjoin('char_persons_contact as guardian_phone_number', function ($join) {
                    $join->on('char_guardians.guardian_id', '=', 'guardian_phone_number.person_id')
                        ->where('guardian_phone_number.contact_type', 'phone');
                })
                    ->selectRaw("CASE WHEN guardian_phone_number.contact_value is null THEN ' ' Else guardian_phone_number.contact_value  END  AS guardian_phone");

            }
            
            if(isset($filters['items'])){
               if(sizeof($filters['items']) > 0 ){
                $query->whereIn('char_cases.id',$filters['items']);  
              }      
            } 
            
            return $query->get();
        }
        elseif ($status == 'export') {
//            $query->groupBy('char_cases.id')
            
            if(isset($filters['items'])){
               if(sizeof($filters['items']) > 0 ){
                $query->whereIn('char_cases.id',$filters['items']);  
              }      
            } 
            $result = $query->get();

            $return = [];
            foreach ($result as $key => $value) {

                $record =\Common\Model\CaseModel::fetch(array(
                    'action' => 'show',
                    'category'      => true,
                    'father_id'      => true,
                    'mother_id'      => true,
                    'guardian_id'      => true,
                    'persons_documents'  =>true
                ),$value->id);

                if (sizeof($record->persons_documents) != 0) {
                    $return[] = ['name' => $value->name,'id_card_number'=>$value->id_card_number,'files'=>$record->persons_documents];
                }
            }
            return $return;

        }
        else {
   
            $itemsCount = ($filters['itemsCount'] !== null)?$filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query->paginate($records_per_page);
        }

    }

    public static function getFamily($options,$r_person_id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name','id_card_number', 'gender', 'marital_status_id'];
        $char_persons_kinship = ['kinship_id'];

        foreach ($options as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    array_push($condition, ['char_persons.' . $key => $value]);
                }
            }
            if(in_array($key, $char_persons_kinship)) {
                if ( $value != "" ) {
                    array_push($condition, ['char_persons_kinship.' . $key => $value]);
                }
            }
        }
        $language_id =  \App\Http\Helpers::getLocale();

        $result =
            \DB::table('char_persons_kinship')
                ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                ->join('char_kinship_i18n', function($q) use ($language_id){
                    $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                    $q->where('char_kinship_i18n.language_id',$language_id);
                })->join('char_marital_status_i18n', function($q) use ($language_id){
                    $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                    $q->where('char_marital_status_i18n.language_id',$language_id);
                })   ->where('char_persons_kinship.r_person_id', '=', $r_person_id)
                ->where('char_persons.father_id', '=', $r_person_id);

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });


        }

        $birthday_to=null;
        $birthday_from=null;

        if(isset($options['birthday_to']) && $options['birthday_to'] !=null){
            $birthday_to=date('Y-m-d',strtotime($options['birthday_to']));
        }
        if(isset($options['birthday_from']) && $options['birthday_from'] !=null){
            $birthday_from=date('Y-m-d',strtotime($options['birthday_from']));
        }
        if($birthday_from != null && $birthday_to != null) {
            $result = $result->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
        }elseif($birthday_from != null && $birthday_to == null) {
            $result = $result->whereDate('char_persons.birthday', '>=', $birthday_from);
        }elseif($birthday_from == null && $birthday_to != null) {
            $result = $result->whereDate('char_persons.birthday', '<=', $birthday_to);
        }

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        return $result
            ->orderBy('char_persons.first_name')
            ->selectRaw("char_persons.id as id,
                         CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))
                         AS name,
                         char_persons.id_card_number as id_card_number,
                         char_persons.id_card_number as old_id_card_number,
                         CASE
                                  WHEN char_persons.gender = 1 THEN '$male'
                                  WHEN char_persons.gender = 2 THEN '$female'
                         END
                          AS gender,
                        char_marital_status_i18n.name as maritalstatus,
                        char_kinship_i18n.name as kinship_name
                        "
            ) ->get();

    }

    public static function getCaseVouchers($filters,$organization_id)
    {
        $user = \Auth::user();
        $char_vouchers=['voucher_type','title','content','notes','organization_id','category_id','currency_id'];
        $char_vouchers_persons=['voucher_id','receipt_location'];
        $char_cases=['category_id','case_id','person_id'];
        $condition=[];
        $char_cases_condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $char_cases_condition [] = ['char_cases.'. $key => $value];
                }
            }
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {

                    if($key == 'voucher_type'){
                        $key='type';
                    }
                    array_push($condition, ['v.' . $key => $value]);
                }
            }
            if(in_array($key, $char_vouchers_persons)) {
                if ( $value != "" ) {
                    array_push($condition, ['char_vouchers_persons.' . $key => $value]);
                }
            }
        }

        $query = \DB::table('char_persons')
            ->join('char_vouchers_persons', 'char_persons.id', '=', 'char_vouchers_persons.person_id')
            ->leftjoin('char_vouchers AS v', function($q){
                $q->on('v.id', '=', 'char_vouchers_persons.voucher_id');
                $q->whereNull('v.deleted_at');
            })
            ->leftjoin('char_vouchers_categories', 'v.category_id', '=', 'char_vouchers_categories.id')
            ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
            ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
            ->leftjoin('char_currencies', 'v.currency_id', '=', 'char_currencies.id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'v.transfer_company_id');


        if (count($char_cases_condition) != 0) {
            $query =  $query->whereIn('char_persons.id',function ($q) use ($char_cases_condition) {
                $q->select('person_id')->from('char_cases');

                for ($i = 0; $i < count($char_cases_condition); $i++) {
                    foreach ($char_cases_condition[$i] as $key => $value) {
                        $q->where($key, '=', $value);
                    }
                }
            });
        }


        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['v.content', 'v.title', 'v.notes','char_vouchers_persons.receipt_location'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        $language_id =  \App\Http\Helpers::getLocale();
        $query->orderBy('v.title')
            ->selectRaw("CASE  WHEN v.title is null THEN ' ' Else v.title END AS voucher_name,
                                   CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name ");

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name");
        }
        else{

            if($language_id == 1){

                $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
            }else{

                $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.en_name END AS sponsor_name");
            }

        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('char_vouchers_persons.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('char_vouchers_persons.receipt_date', '<=', $receipt_date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $date_to=null;
        $date_from=null;

        if(isset($filters['voucher_source']) && $filters['voucher_source'] !=null){
            if($filters['voucher_source'] == 1){
                $query = $query->whereNull('v.project_id');
            }else{
                $query = $query->whereNotNull('v.project_id');
            }
        }

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }


        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $manually_ = trans('aid::application.manually_');

        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');

        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $receipt = trans('aid::application.receipt');
        $not_receipt = trans('aid::application.not_receipt');


        if($filters['action'] =='export'){

            $query->selectRaw("CASE  WHEN char_vouchers_categories.name is null THEN ' ' Else char_vouchers_categories.name END AS category_type,
                           CASE  WHEN v.value is null THEN ' ' Else v.value END AS voucher_value,
                           CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name_,
                           CASE  WHEN v.type = 1 THEN '$non_financial' WHEN v.type = 2 THEN '$financial' END AS voucher_type,
                           CASE  WHEN v.urgent = 0 THEN '$not_urgent' WHEN v.urgent = 1 THEN '$urgent' END  AS voucher_urgent,
                           CASE  WHEN v.project_id is null THEN '$from_project' Else '$manually_' END  AS voucher_source,     
                           CASE  WHEN char_vouchers_persons.status = 1 THEN '$receipt'  WHEN char_vouchers_persons.status = 2 THEN '$not_receipt' END AS receipt_status,
                           CASE  WHEN char_vouchers_persons.receipt_date is null THEN ' ' Else char_vouchers_persons.receipt_date END AS receipt_date,
                           CASE WHEN char_vouchers_persons.receipt_time is null THEN ' ' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time,
                           CASE  WHEN char_vouchers_persons.receipt_location is null THEN ' ' Else char_vouchers_persons.receipt_location END AS receipt_location,
                           CASE  WHEN v.notes is null THEN ' ' Else v.notes END AS voucher_note
                          ");
            return $query->get();
        }



        $query->selectRaw("CASE  WHEN char_vouchers_categories.name is null THEN ' ' Else char_vouchers_categories.name END AS category_type,
                           CASE  WHEN v.value is null THEN ' ' Else v.value END AS voucher_value,
                           CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name_,
                           CASE  WHEN v.type = 1 THEN '$non_financial' WHEN v.type = 2 THEN '$financial' END AS voucher_type,
                           CASE  WHEN v.urgent = 0 THEN '$not_urgent' WHEN v.urgent = 1 THEN '$urgent' END  AS voucher_urgent,
                           CASE  WHEN v.project_id is null THEN '$from_project' Else '$manually_' END  AS voucher_source,     
                           CASE  WHEN char_vouchers_persons.status = 1 THEN '$receipt'  WHEN char_vouchers_persons.status = 2 THEN '$not_receipt' END AS receipt_status,
                           CASE  WHEN char_vouchers_persons.receipt_date is null THEN ' ' Else char_vouchers_persons.receipt_date END AS receipt_date,
                           CASE WHEN char_vouchers_persons.receipt_time is null THEN ' ' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time,
                           CASE  WHEN char_vouchers_persons.receipt_location is null THEN ' ' Else char_vouchers_persons.receipt_location END AS receipt_location,
                           CASE  WHEN v.notes is null THEN ' ' Else v.notes END AS voucher_note
                          ");
        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->paginate()->total());
        $response['data'] = $query->paginate($records_per_page);

        $amount = 0;
        $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
        if($total){
            $amount = $total->total;
        }

        $response['total'] =round($amount,0);

        return $response;
    }

    public static function getSponsorships($filters){
        $char_cases=['category_id','case_id','person_id'];
        $char_sponsorship_cases=['status','sponsor_id'];
//        $char_sponsorship=['category_id']; // category of sponsorship

        $condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $query = self::query()
            ->join('char_sponsorship_cases','char_sponsorship_cases.case_id',  '=', 'char_cases.id')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->join('char_organizations as org', 'org.id', '=', 'char_cases.organization_id')
            ->join('char_categories as ca','ca.id', '=', 'char_cases.category_id');


        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        $q->where($key, '=', $value);
                    }
                }
            });
        }

        $to_sponsorship_date=null;
        $from_sponsorship_date=null;

        if(isset($filters['to_sponsorship_date']) && $filters['to_sponsorship_date'] !=null){
            $to_sponsorship_date=date('Y-m-d',strtotime($filters['to_sponsorship_date']));
        }
        if(isset($filters['from_sponsorship_date']) && $filters['from_sponsorship_date'] !=null){
            $from_sponsorship_date=date('Y-m-d',strtotime($filters['from_sponsorship_date']));
        }

        if($from_sponsorship_date != null && $to_sponsorship_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.created_at', [ $from_sponsorship_date, $to_sponsorship_date]);
        }elseif($from_sponsorship_date != null && $to_sponsorship_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '>=', $from_sponsorship_date);
        }elseif($from_sponsorship_date == null && $to_sponsorship_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '<=', $to_sponsorship_date);
        }


        $to_guaranteed_date=null;
        $from_guaranteed_date=null;


        if(isset($filters['to_guaranteed_date']) && $filters['to_guaranteed_date'] !=null){
            $to_guaranteed_date=date('Y-m-d',strtotime($filters['to_guaranteed_date']));
        }
        if(isset($filters['from_guaranteed_date']) && $filters['from_guaranteed_date'] !=null){
            $from_guaranteed_date=date('Y-m-d',strtotime($filters['from_guaranteed_date']));
        }

        if($from_guaranteed_date != null && $to_guaranteed_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.sponsorship_date', [ $from_guaranteed_date, $to_guaranteed_date]);
        }elseif($from_guaranteed_date != null && $to_guaranteed_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '>=', $from_guaranteed_date);
        }elseif($from_guaranteed_date == null && $to_guaranteed_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '<=', $to_guaranteed_date);
        }
        $language_id =  \App\Http\Helpers::getLocale();
        $query->selectRaw("CONCAT(ifnull(char_persons.first_name,' '), ' ' ,ifnull(char_persons.second_name, ' '), ' ',ifnull(char_persons.third_name, ' '), ' ', ifnull(char_persons.last_name,' ')) AS name,
                           char_persons.id_card_number,char_persons.old_id_card_number,
                           CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name,
                            CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name");

        if($user->super_admin){

            if($language_id == 1){

                $query->selectRaw("
                                     CASE  WHEN char_organizations.name is null THEN ' ' Else  char_organizations.name END AS sponsor_name,
                                     char_sponsorship_cases.status as sponsorship_status,
                                     CASE  WHEN char_sponsorship_cases.sponsor_number is null THEN ' ' Else char_sponsorship_cases.sponsor_number END AS sponsor_number
                                     ");
            }else{

                $query->selectRaw("
                                     CASE  WHEN char_organizations.en_name is null THEN ' ' Else  char_organizations.en_name END AS sponsor_name,
                                     char_sponsorship_cases.status as sponsorship_status,
                                     CASE  WHEN char_sponsorship_cases.sponsor_number is null THEN ' ' Else char_sponsorship_cases.sponsor_number END AS sponsor_number
                                     ");
            }
        }else{
            if($language_id == 1){
                $query->selectRaw("CASE  WHEN (char_organizations.name is null or char_cases.organization_id != '$organization_id') THEN ' ' Else 
                                     char_organizations.name END AS sponsor_name,
                                     char_sponsorship_cases.status as sponsorship_status,
                                     CASE  WHEN (char_sponsorship_cases.sponsor_number is null or char_cases.organization_id != '$organization_id') THEN ' ' Else 
                                     char_sponsorship_cases.sponsor_number END AS sponsor_number
                                     ");
            }else{

                $query->selectRaw("CASE  WHEN (char_organizations.en_name is null or char_cases.organization_id != '$organization_id') THEN ' ' Else 
                                     char_organizations.en_name END AS sponsor_name,
                                     char_sponsorship_cases.status as sponsorship_status,
                                     CASE  WHEN (char_sponsorship_cases.sponsor_number is null or char_cases.organization_id != '$organization_id') THEN ' ' Else 
                                     char_sponsorship_cases.sponsor_number END AS sponsor_number
                                     ");
            }
        }

        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');

        if($filters['action'] =='filters'){
            $query->selectRaw("CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                              WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                              WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                              WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                              WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                         END AS  status");
        }else{
            $query->selectRaw("CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                              WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                              WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                              WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                               WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                          END AS  sponsorship_status");
        }

        $query->selectRaw("DATE_FORMAT(char_sponsorship_cases.created_at,'%Y-%m-%d') as created_at,
                           CASE WHEN char_sponsorship_cases.sponsorship_date is null THEN ' ' Else char_sponsorship_cases.sponsorship_date
                                END AS guaranteed_date,
                             char_sponsorship_cases.id
                        ");

        if($filters['action'] =='filters'){
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $query=$query->paginate($records_per_page);
            return $query;

        }
        return $query->get();

    }

    public static function getGuardians($filters){

        $language_id =  \App\Http\Helpers::getLocale();
        $char_cases=['category_id','case_id'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','category_id','visitor'];
        $char_guardians = ['kinship_id','organization_id','status','created_at','individual_id','guardian_id']; //   ,
        $condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.'.$key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_guardians)) {
                if ( $value != "" ) {
                    $data = ['char_guardians.'.$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()
            ->leftjoin('char_guardians', function($q){
                $q->on('char_guardians.individual_id', '=', 'char_cases.person_id');
                $q->on('char_guardians.organization_id', '=', 'char_cases.organization_id');
            })
            ->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id');

        if($filters['target'] == 'guardians'){
            $query->join('char_persons', 'char_guardians.guardian_id', '=', 'char_persons.id');
        }else{
            $query->join('char_persons', 'char_guardians.individual_id', '=', 'char_persons.id');
        }

        $query->groupBy('char_persons.id');

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        $to_birthday=null;
        $from_birthday=null;

        if(isset($filters['to_birthday']) && $filters['to_birthday'] !=null){
            $to_birthday=date('Y-m-d',strtotime($filters['to_birthday']));
        }
        if(isset($filters['from_birthday']) && $filters['from_birthday'] !=null){
            $from_birthday=date('Y-m-d',strtotime($filters['from_birthday']));
        }
        if($from_birthday != null && $to_birthday != null) {
            $query = $query->whereBetween( 'char_persons.birthday', [ $from_birthday, $to_birthday]);
        }elseif($from_birthday != null && $to_birthday == null) {
            $query = $query->whereDate('char_persons.birthday', '>=', $from_birthday);
        }elseif($from_birthday == null && $to_birthday != null) {
            $query = $query->whereDate('char_persons.birthday', '<=', $to_birthday);
        }

        $to_created_at=null;
        $from_created_at=null;

        if(isset($filters['to_created_at']) && $filters['to_created_at'] !=null){
            $to_created_at=date('Y-m-d',strtotime($filters['to_created_at']));
        }
        if(isset($filters['from_created_at']) && $filters['from_created_at'] !=null){
            $from_created_at=date('Y-m-d',strtotime($filters['from_created_at']));
        }

        if($from_created_at != null && $to_created_at != null) {
            $query = $query->whereBetween( 'char_guardians.created_at', [ $from_created_at, $to_created_at]);
        }elseif($from_created_at != null && $to_created_at == null) {
            $query = $query->whereDate('char_guardians.created_at', '>=', $from_created_at);
        }elseif($from_created_at == null && $to_created_at != null) {
            $query = $query->whereDate('char_guardians.created_at', '<=', $to_created_at);
        }
//        DATE_FORMAT(char_guardians.created_at,'%d-%m-%Y') as created_at,

        $current_guardian = trans('sponsorship::application.current guardian');
        $old_guardian = trans('sponsorship::application.old guardian');
        $current_individual = trans('sponsorship::application.current individual');
        $old_individual = trans('sponsorship::application.old individual');

        if($filters['target'] == 'guardians'){
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS guardian_name,
                               CASE WHEN char_guardians.status = 1 THEN '$current_guardian'  Else '$old_guardian'  END AS status
                              ");
        }else{
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS individual_name,
                               CASE WHEN char_guardians.status = 1 THEN '$current_individual'  Else '$old_individual'  END AS status
                              ");
        }

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $query->selectRaw("
        CASE WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                           char_guardians.created_at,
                           CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  As id_card_number,  
                           CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  As old_id_card_number,  
                           CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  As birthday,  
                           CASE
                               WHEN char_persons.gender is null THEN ' '
                               WHEN char_persons.gender = 1 THEN '$male'
                               WHEN char_persons.gender = 2 THEN '$female'
                           END AS the_gender
                           ");

        if($filters['action'] =='filters'){
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $query=$query->paginate($records_per_page);
            return $query;
        }
        else if($filters['action'] =='export'){
            $query = $query
                ->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id){
                    $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As location_name', function($q) use ($language_id){
                    $q->on('location_name.location_id', '=', 'char_persons.location_id');
                    $q->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As city_name', function($q) use ($language_id){
                    $q->on('city_name.location_id', '=', 'char_persons.city');
                    $q->where('city_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As district_name', function($q) use ($language_id){
                    $q->on('district_name.location_id', '=', 'char_persons.governarate');
                    $q->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As country_name', function($q) use ($language_id){
                    $q->on('country_name.location_id', '=', 'char_persons.country');
                    $q->where('country_name.language_id',$language_id);
                })
                ->leftjoin('char_persons_contact as phone_number', function($q) use ($language_id){
                    $q->on('phone_number.person_id', '=', 'char_persons.id');
                    $q->where('phone_number.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as primary_mobile', function($q) use ($language_id){
                    $q->on('primary_mobile.person_id', '=', 'char_persons.id');
                    $q->where('primary_mobile.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as secondary_mobile', function($q) use ($language_id){
                    $q->on('secondary_mobile.person_id', '=', 'char_persons.id');
                    $q->where('secondary_mobile.contact_type','secondary_mobile');
                })
                ->selectRaw("CONCAT(ifnull(country_name.name,' '),' ',ifnull(district_name.name,' '),' ',ifnull(city_name.name,' '),' ', ifnull(location_name.name,' '),ifnull(mosques_name.name,' ' ),' ',ifnull(char_persons.street_address,' '))
                             AS address,
                             CASE WHEN phone_number.contact_value is null THEN ' ' Else phone_number.contact_value END   AS phone,
                             CASE WHEN primary_mobile.contact_value is null THEN ' ' Else primary_mobile.contact_value END   AS primary_mobile,
                             CASE WHEN secondary_mobile.contact_value is null THEN ' ' Else secondary_mobile.contact_value END   AS secondary_mobile
                            ") ->get();

        }
        return $query;
    }

    public static function getPaymentRecords($target,$filters,$id,$father_id,$mother_id,$page,$organization_id,$sponsor_id){
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $char_cases=['category_id','case_id'];
//        $char_payments_cases = ['case_id','guardian_id'];
        $char_payments_int = ['sponsorship_id','organization_id','sponsor_id', 'status'];
        $c = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $user = \Auth::user();

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_payments_int)) {
                if ($value != "" ) {
                    $data = ['char_payments.' . $key => (int) $value];
                    array_push($condition, $data);
                }
            }
//            if(in_array($key, $char_payments_cases)) {
//                if ($value != "" ) {
//                    $data = ['char_payments_cases.' . $key => (int) $value];
//                    array_push($condition, $data);
//                }
//            }
            if(in_array($key, $c)) {
                if ($value != "" ) {
                    $data = ['c.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $query = self::query()
            ->join('char_payments_cases','char_payments_cases.case_id',  '=', 'char_cases.id')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments', function($q){
                $q->on('char_payments.id','=','char_payments_cases.payment_id');
                $q->on('char_payments.organization_id','=','char_cases.organization_id');
            })
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->leftjoin('char_persons AS g','g.id','=','char_payments_cases.guardian_id')
            ->whereIn('char_payments_cases.status',[1,2]);

        if($target =='guardian' || $target =='guardians'){
            $query->join('char_persons as c','c.id',  '=', 'char_cases.person_id');
        }else{
            $query->join('char_persons as c','c.id',  '=', 'char_payments_cases.guardian_id');
        }

        if($organization_id != null){

            $user = \Auth::user();
            $UserType=$user->type;

            if($UserType == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('char_cases.organization_id',$organization_id);
                    $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });

            }else{

                $query->where(function ($anq) use ($organization_id) {
                    /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                    $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                        $decq->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        if($target =='family'){
            $query=$query->wherein('char_cases.person_id', function($q)use($father_id,$mother_id,$id){
                $q->select('char_persons.id')->from('char_persons');
                if($father_id != null){
                    $q->where('char_persons.father_id', $father_id);
                }

                if($mother_id != null){
                    $q->where('char_persons.mother_id', $mother_id);
                }

                if($father_id == null && $mother_id ==null){
                    $q->where('char_persons.id', $id);
                }
            });
        }

        elseif($target =='guardian' || $target =='guardians'){
            $query=$query->where('char_payments_cases.guardian_id','=',$id);
        }else{
            $query=$query->where('char_cases.person_id','=',$id);
        }

        if($sponsor_id != null){
            $query=$query->where('char_payments.sponsor_id','=',$sponsor_id);
        }

        $end_date_from=null;
        $begin_date_from=null;

        if(isset($filters['end_date_from']) && $filters['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($filters['end_date_from']));
        }
        if(isset($filters['begin_date_from']) && $filters['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($filters['begin_date_from']));
        }
        if($begin_date_from != null && $end_date_from != null) {
            $query = $query->whereBetween( 'char_payments_cases.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $query = $query->whereDate('char_payments_cases.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $query = $query->whereDate('char_payments_cases.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($filters['begin_date_to']) && $filters['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($filters['begin_date_to']));
        }

        if(isset($filters['end_date_to']) && $filters['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($filters['end_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $query = $query->whereBetween( 'char_payments_cases.date_to', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $query = $query->whereDate('char_payments_cases.date_to', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $query = $query->whereDate('char_payments_cases.date_to', '<=', $end_date_to);
        }

        $end_payment_date=null;
        $start_payment_date=null;

        if($start_payment_date != null && $end_payment_date != null) {
            $query = $query->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $query = $query->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($start_payment_date == null && $end_payment_date != null) {
            $query = $query->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            $names = ['g.first_name', 'g.second_name', 'g.third_name', 'g.last_name'];
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $query->selectRaw("CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                           CASE WHEN c.id_card_number is null THEN ' ' Else c.id_card_number END AS id_card_number,
                           CASE WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name,
                           CASE WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organizations_name ");


        if($user->super_admin){
            $query->selectRaw("CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name");
        }else{

            if($language_id == 1){
                $query->selectRaw("CASE  WHEN (sponsor.name is null or char_payments.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
            }else{
                $query->selectRaw("CASE  WHEN (sponsor.name is null or char_payments.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.en_name END AS sponsor_name");
            }
        }

        $deposit = trans('common::application.Bank deposit');
        $check = trans('common::application.Bank check');
        $card = trans('common::application.ID card');
        $Internal_check = trans('common::application.Internal check');

        $query->selectRaw("char_payments_cases.case_id,
        CASE WHEN char_payments_cases.sponsor_number is null THEN ' ' 
                                      Else char_payments_cases.sponsor_number END AS sponsor_number,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else char_payments_cases.amount  END  AS amount,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else (char_payments_cases.shekel_amount)  END  AS amount_in_shekel,
                           CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                           CASE WHEN char_payments_cases.recipient is null THEN ' '
                                WHEN char_payments_cases.recipient = 0 THEN CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                WHEN char_payments_cases.recipient = 1 THEN 
                                CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                           END  AS recipients_name,
                           CASE WHEN char_payments_cases.date_to is null THEN ' ' Else char_payments_cases.date_to END  AS date_to,
                           CASE WHEN char_payments_cases.date_from is null THEN ' ' Else char_payments_cases.date_from  END  AS date_from,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.exchange_date is null THEN ' 'Else char_payments.exchange_date END  AS exchange_date,
                           CASE WHEN char_payments.exchange_type is  null THEN ' '
                                WHEN char_payments.exchange_type = 1 THEN '$deposit'
                                WHEN char_payments.exchange_type = 2 THEN '$check'
                                WHEN char_payments.exchange_type = 4 THEN '$card'
                                WHEN char_payments.exchange_type = 3 THEN '$Internal_check'
                           END  AS exchange_type");

        $amount = 0;
        if($filters['action'] =='filters'){
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $paginate=$query->paginate($records_per_page);
            if($page == 1){
                $total    = $query->selectRaw("sum((char_payments_cases.shekel_amount)) as total")->first();
                $amount = $total->total;
            }
            return ['payment' => $paginate, 'total' =>round($amount,0)];
        }
        return $query->get();
    }

    public static function createExcelToExport($items,$sheetName,$HeadTo)
    {

        if(sizeof($items) !=0){
            $rows=[];

            if($sheetName =='case_vouchers_'){
                foreach($items as $key =>$value){
//                    $value = json_decode($value, true);
                    $rows[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'&& $k != 'r_person_id'&& $k != 'l_person_id'&& $k != 'l_person_id'){
                            if(substr( $k, 0, 7 ) === "mother_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 7 ) === "father_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                            }else{
                                $rows[$key][trans('sponsorship::application.' . $k)]= $v;
                            }
                        }
                    }
                }
            }elseif ($sheetName == 'family_member'){
                foreach($items as $key =>$value){
                    $rows[$key][trans('setting::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'&& $k != 'r_person_id'&& $k != 'kinship_name'&& $k != 'l_person_id'){
                            if($k =='sponsor_number'){
                                $rows[$key][trans('sponsorship::application.sponsorship_number')]= $v;
                            } else if($k =='amount_in_shekel'){
                                $rows[$key][trans('sponsorship::application.' . $k)]= round($v,0);
                            }else{
                                $rows[$key][trans('sponsorship::application.' . $k)]= $v;
                            }
                        }
                    }
                }
            }else{
                foreach($items as $key =>$value){
                    $value = json_decode($value, true);
                    $rows[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'&& $k != 'r_person_id'&& $k != 'l_person_id'&& $k != 'l_person_id'){
                            if(substr( $k, 0, 7 ) === "mother_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 7 ) === "father_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                $rows[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                            }else{
                                $rows[$key][trans('sponsorship::application.' . $k)]= $v;
                            }
                        }
                    }
                }
            }
            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows,$sheetName,$HeadTo) {
                $excel->sheet($sheetName,function($sheet) use($rows,$HeadTo) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:".$HeadTo .'1')->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($rows);
                });
            })->store('xlsx', storage_path('tmp/'));

            return response()->json(['download_token' => $token]);
        }
        return response()->json(['status' => false]);
    }

    public static function filterCases($page,$filters,$organization_id,$master)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $condition =array();
        $output=array();
        $all_organization=null;
        $category_type=$filters['category_type'];

        $user = \Auth::user();
        $UserType=$user->type;
        $ancestor = \Organization\Model\Organization::getAncestor($organization_id);

        $char_cases = ['category_id','visitor','status','notes','visitor','visitor_card', 'visitor_opinion', 'visitor_evaluation'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id","is_qualified","qualified_card_number",
            "adsdistrict_id", "adsmosques_id", "card_type", "adsneighborhood_id", "adsregion_id",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount',
            'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];
        $char_persons_i18n = ['en_first_name', 'en_second_name', 'en_third_name', 'en_last_name'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location',
            'gov_work_details'];
        $char_persons_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name', 'gov_health_details'];
        $char_persons_education = ['grade', 'authority','type','stage','level','year','school','points'];
        $char_persons_islamic_commitment = ['prayer','quran_parts', 'quran_chapters','quran_center'];
        $reconstructions=['promised','reconstructions_organization_name'];
        $secondary_mobile=['secondary_mobile'];
        $primary_mobile=['primary_mobile'];
        $phone=['phone'];
        $char_organization_persons_banks = ['bank_id'];
        $char_persons_banks = ['bank_id','branch_name','account_number','account_owner'];
        $char_persons_father = ['father_id_card_number','father_card_type','father_death_cause_id','father_first_name', 'father_monthly_income', 'father_second_name', 'father_third_name', 'father_last_name','father_alive'];
        $char_persons_mother = ['mother_id_card_number','mother_card_type','mother_death_cause_id','mother_first_name','mother_marital_status_id' ,'mother_second_name','mother_monthly_income', 'mother_third_name','mother_nationality', 'mother_last_name','mother_alive'];
        $char_persons_guardian=['guardian_id_card_number','guardian_card_type','guardian_first_name', 'guardian_second_name','guardian_third_name',
            'guardian_gender','guardian_marital_status_id','guardian_location_id','guardian_country','guardian_governarate','guardian_city',
            'guardian_monthly_income','guardian_birth_place','guardian_nationality', 'guardian_last_name','guardian_mosques_id'];
        $guardian_residence = ['guardian_property_type_id', 'guardian_residence_condition','guardian_indoor_condition','guardian_roof_material_id'];
        $mother_health = ['mother_condition','mother_disease_id'];
        $father_health = ['father_condition','father_disease_id'];
        $father_work = ['father_working','father_work_job_id','father_work_location'];
        $mother_work = ['mother_working','mother_work_job_id','mother_work_location'];
        $guardian_work = ['guardian_working','guardian_work_job_id','guardian_work_location'];
        $father_education = ['father_grade', 'father_authority','father_type','father_stage','father_level','father_year','father_school','father_points'];
        $mother_education = ['mother_grade', 'mother_authority','mother_type','mother_stage','mother_level','mother_year','mother_school','mother_points'];
        $guardian_education = ['guardian_grade', 'guardian_authority','guardian_type','guardian_stage','guardian_level','guardian_year','guardian_school','guardian_points'];
        $guardian_cont1=['guardian_primary_mobile'];
        $guardian_cont2=['guardian_secondary_mobile'];
        $guardian_cont3=['guardian_phone'];


        $char_persons_health_cnt =
        $char_persons_work_cnt  =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt =
        $primary_num_cnt = $secondary_num_cnt =  $phone_num_cnt = $reconstruct_cnt=
        $char_persons_islamic_commitment_cnt =
        $char_organization_persons_banks_cnt=
        $char_persons_banks_cnt =
        $char_persons_father_cnt =
        $char_persons_mother_cnt =
        $char_persons_guardian_cnt =
        $guardian_residence_cnt =
        $mother_health_cnt =
        $father_health_cnt =
        $father_work_cnt    =
        $mother_work_cnt  =
        $guardian_work_cnt =
        $father_education_cnt =
        $mother_education_cnt =
        $guardian_education_cnt =
        $guardian_cont1_cnt =
        $guardian_cont2_cnt =
        $guardian_cont3_cnt = 0;

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    if($key =='case_status'){
                        $data = ['char_cases.status'=> $value];
                    }else{
                        $data = ['char_cases.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $reconstructions)) {
                if ( $value != "" ) {
                    if($key =='reconstructions_organization_name'){
                        $data = ['char_reconstructions.organization_name'=> $value];
                    }else{
                        $data = ['char_reconstructions.' . $key => $value];
                    }
                    $reconstruct_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_i18n)) {
                if ( $value != "" ) {
                    $data = ['char_persons_i18n.' . substr($key,3)=>$value ];
                    $char_persons_i18n_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_education)) {
                if ( $value != "" ) {
                    $data = ['char_persons_education.' . $key => $value];
                    $char_persons_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_islamic_commitment)) {
                if ( $value != "" ) {
                    $data = ['char_persons_islamic_commitment.' . $key => $value];
                    $char_persons_islamic_commitment_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $phone)) {
                if ( $value != "" ) {
                    $data = ['phone_num.contact_value' => $value]; $phone_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $primary_mobile)) {
                if ( $value != "" ) {
                    $data = ['primary_num.contact_value' => $value];
                    $primary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $secondary_mobile)) {
                if ( $value != "" ) {
                    $data = ['secondary_num.contact_value' => $value];
                    $secondary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_organization_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_organization_persons_banks.'. $key => $value];
                    $char_organization_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_persons_banks.'. $key => $value];
                    $char_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    if($key =="father_alive"){
                        $data = ['f.death_date'  => $value];
                    }else{
                        $data = ['f.' . substr($key, 7) => $value];
                    }
                    $char_persons_father_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    if($key =="mother_alive"){
                        $data = ['m.death_date'  => $value];
                    }else{
                        $data = ['m.' . substr($key, 7) => $value];
                    }
                    $char_persons_mother_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 9) => $value];
                    $char_persons_guardian_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_residence)) {
                if ( $value != "" ) {
                    $data = ['guardian_residence.' . substr($key, 9) => $value];
                    $guardian_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$mother_health)) {
                if ( $value != "" ) {
                    $data = ['mother_health.' . substr($key, 7) => $value];
                    $mother_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$father_health)) {
                if ( $value != "" ) {
                    $data = ['father_health.' . substr($key, 7) => $value];
                    $father_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_work)) {

                if ( $value != "" ) {
                    $father_work_cnt   ++;
                    $data = ['father_work.' . substr($key, 7) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_work)) {

                if ( $value != "" ) {
                    $data = ['mother_work.' . substr($key, 7) => $value];
                    $mother_work_cnt ++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_work)) {

                if ( $value != "" ) {
                    $data = ['guardian_work.' . substr($key, 9) => $value];
                    $guardian_work_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_education)) {

                if ( $value != "" ) {
                    $data = ['father_education.' . substr($key, 7) => $value];
                    $father_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_education)) {

                if ( $value != "" ) {
                    $data = ['mother_education.' . substr($key, 7) => $value];
                    $mother_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_education)) {

                if ( $value != "" ) {
                    $data = ['guardian_education.' . substr($key, 9) => $value];
                    $guardian_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont1)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont1.contact_value' => $value];
                    $guardian_cont1_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont2)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont2.contact_value' => $value];
                    $guardian_cont2_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont3)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont3.contact_value' => $value];
                    $guardian_cont3_cnt++;
                    array_push($condition, $data);
                }
            }

        }

        $result = \DB::table('char_cases')
            ->selectRaw("char_get_active_case_count(char_cases.person_id) as active_case_count")
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q) use($category_type){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',$category_type);
            })
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                $q->where('char_marital_status.language_id',$language_id);
            });

        if($category_type == 1){
            $result ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) use ($language_id){
                    $q->on('guardian_cont2.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')

                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                });
        }
        else{
            $result ->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                $q->on('primary_num.person_id', '=', 'char_persons.id');
                $q->where('primary_num.contact_type','primary_mobile');
            })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                });
        }

        if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {

            $result->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                });

            if($category_type == 1) {

                $result ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                    $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                    $q->where('mosques_name.language_id',$language_id);
                })
                    ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                        $q->on('L1.location_id', '=', 'char_persons.location_id');
                        $q->where('L1.language_id',$language_id);
                    })
                    ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                        $q->on('L4.location_id', '=', 'char_persons.country');
                        $q->where('L4.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id', '=', 'char_persons.id')
                    ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                    ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                        $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                        $q->where('char_edu_stages.language_id',$language_id);
                    })
                    ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                        $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                        $q->where('char_edu_authorities.language_id',$language_id);
                    })
                    ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                        $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                        $q->where('char_edu_degrees.language_id',$language_id);
                    })
                    ->leftjoin('char_death_causes_i18n as father_death_causes', function($q) use ($language_id){
                        $q->on('father_death_causes.death_cause_id', '=', 'f.death_cause_id');
                        $q->where('father_death_causes.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id')
                    ->leftjoin('char_work_jobs_i18n as father_work_jobs', function($q) use ($language_id){
                        $q->on('father_work_jobs.work_job_id', '=', 'father_work.work_job_id');
                        $q->where('father_work_jobs.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id')
                    ->leftjoin('char_edu_degrees_i18n as father_edu_degrees', function($q) use ($language_id){
                        $q->on('father_edu_degrees.edu_degree_id', '=', 'father_education.degree');
                        $q->where('father_edu_degrees.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id')
                    ->leftjoin('char_diseases_i18n as father_diseases', function($q) use ($language_id){
                        $q->on('father_diseases.disease_id', '=', 'father_health.disease_id');
                        $q->where('father_diseases.language_id',$language_id);
                    })
//                    ->leftjoin('char_death_causes_i18n as mother_death_causes', function($q) use ($language_id){
//                        $q->on('mother_death_causes.death_cause_id', '=', 'm.death_cause_id');
//                        $q->where('mother_death_causes.language_id',$language_id);
//                    })
                    ->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id')
                    ->leftjoin('char_work_jobs_i18n as mother_work_jobs', function($q) use ($language_id){
                        $q->on('mother_work_jobs.work_job_id', '=', 'mother_work.work_job_id');
                        $q->where('mother_work_jobs.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id')
                    ->leftjoin('char_edu_stages_i18n as mother_edu_stages', function($q) use ($language_id){
                        $q->on('mother_edu_stages.edu_stage_id', '=', 'mother_education.stage');
                        $q->where('mother_edu_stages.language_id',$language_id);
                    })
                    ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                        $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                        $q->where('mother_marital_status.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
                    ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                        $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                        $q->where('mother_diseases.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                        $q->on('guardian_cont1.person_id', '=', 'char_guardians.guardian_id');
                        $q->where('guardian_cont1.contact_type','phone');
                    })

                    ->leftjoin('char_persons_contact as guardian_cont4', function($q) use ($language_id){
                        $q->on('guardian_cont4.person_id', '=', 'char_guardians.guardian_id');
                        $q->where('guardian_cont4.contact_type','wataniya');
                    })
//                    ->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
//                        $q->on('guardian_cont3.person_id', '=', 'char_guardians.guardian_id');
//                        $q->where('guardian_cont3.contact_type','secondery_mobile');
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L1', function($q) use ($language_id){
//                        $q->on('guardian_L1.location_id', '=', 'g.location_id');
//                        $q->where('guardian_L1.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L2', function($q) use ($language_id){
//                        $q->on('guardian_L2.location_id', '=', 'g.city');
//                        $q->where('guardian_L2.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L3', function($q) use ($language_id){
//                        $q->on('guardian_L3.location_id', '=', 'g.governarate');
//                        $q->where('guardian_L3.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L4', function($q) use ($language_id){
//                        $q->on('guardian_L4.location_id', '=', 'g.country');
//                        $q->where('guardian_L4.language_id',$language_id);
//                    })
                    ->leftjoin('char_residence as guardian_residence','guardian_residence.person_id', '=', 'g.id')
                    ->leftjoin('char_property_types_i18n as guardian_property_types', function($q) use ($language_id){
                        $q->on('guardian_property_types.property_type_id', '=', 'guardian_residence.property_type_id');
                        $q->where('guardian_property_types.language_id',$language_id);
                    })
                    ->leftjoin('char_roof_materials_i18n as guardian_roof_materials', function($q) use ($language_id){
                        $q->on('guardian_roof_materials.roof_material_id', '=', 'guardian_residence.roof_material_id');
                        $q->where('guardian_roof_materials.language_id',$language_id);
                    })
                    ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                        $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'guardian_residence.indoor_condition');
                        $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                    })
                    ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                        $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                        $q->where('guardian_house_status_i18n.language_id',$language_id);
                    })
                ;
            }
            else{

                $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                        ->where('country_name.language_id',$language_id);
                })

                    ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                        $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                            ->where('location_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                        $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                            ->where('square_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                        $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                            ->where('mosques_name.language_id',$language_id);
                    })
                    ->leftjoin('char_reconstructions','char_reconstructions.case_id', '=', 'char_cases.id');
            }

            $result
                ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                    $q->on('L.location_id', '=', 'char_persons.birth_place');
                    $q->where('L.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                    $q->on('L0.location_id', '=', 'char_persons.nationality');
                    $q->where('L0.language_id',$language_id);
                })
                ->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id')


                ->leftjoin('char_persons_contact as wataniya_num', function($q) use ($language_id){
                    $q->on('wataniya_num.person_id', '=', 'char_persons.id');
                    $q->where('wataniya_num.contact_type','wataniya');
                })
//                ->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
//                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
//                    $q->where('secondary_num.contact_type','secondary_mobile');
//                })
                ->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                    $q->on('phone_num.person_id', '=', 'char_persons.id');
                    $q->where('phone_num.contact_type','phone');
                })
                ->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id')
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_work', 'char_persons_work.person_id', '=', 'char_persons.id')
                ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                    $q->on('work_job.work_job_id', '=', 'char_persons_work.work_job_id');
                    $q->where('work_job.language_id',$language_id);
                })
                ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                    $q->on('work_wage.id', '=', 'char_persons_work.work_wage_id');
                })
                ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                    $q->on('work_status.work_status_id', '=', 'char_persons_work.work_status_id');
                    $q->where('work_status.language_id',$language_id);
                })
                ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                    $q->on('work_reason.work_reason_id', '=', 'char_persons_work.work_reason_id');
                    $q->where('work_reason.language_id',$language_id);
                });


        }
        else{

            if($char_persons_i18n_cnt != 0){
                $result->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id');
            }
            if($char_residence_cnt != 0) {
                $result->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
            }
            if($char_persons_work_cnt != 0){
                $result ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
            }
            if($char_persons_health_cnt!= 0 ){
                $result->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
            }
            if($char_persons_education_cnt!= 0 ){
                $result->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
            }
            if($char_persons_islamic_commitment_cnt!= 0  || isset($filters['save_quran'])){
                $result->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id','=','char_persons.id');
            }

            if($char_organization_persons_banks_cnt!= 0  || $char_persons_banks_cnt != 0) {
                $result->leftjoin('char_organization_persons_banks', function ($q) {
                    $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                    $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                })
                    ->leftjoin('char_persons_banks', function ($q) {
                        $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                    });

            }
            if($primary_num_cnt != 0){
                $result->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                    $q->on('primary_num.person_id', '=', 'char_persons.id');
                    $q->where('primary_num.contact_type','primary_mobile');
                });
            }
            if($secondary_num_cnt != 0){
                $result->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
                    $q->where('secondary_num.contact_type','secondary_mobile');
                });
            }
            if($phone_num_cnt != 0){
                $result->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                    $q->on('phone_num.person_id', '=', 'char_persons.id');
                    $q->where('phone_num.contact_type','phone');
                });
            }

            if($category_type == 2){
                if($reconstruct_cnt != 0){
                    $result->leftjoin('char_reconstructions', function($q){
                        $q->on('char_reconstructions.case_id', '=', 'char_cases.id');
                    });
                }
            }
            else{
                if($guardian_residence_cnt!= 0 ){
                    $result->leftjoin('char_residence as guardian_residence','guardian_residence.person_id','=','g.id')   ;
                }
                if($guardian_cont1_cnt!= 0 ){
                    $result->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                        $q->on('guardian_cont1.person_id', '=', 'g.id');
                        $q->where('guardian_cont1.contact_type','primary_mobile');
                    })      ;
                }

                if($guardian_cont3_cnt!= 0 ){
                    $result->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
                        $q->on('guardian_cont3.person_id', '=', 'g.id');
                        $q->where('guardian_cont3.contact_type','phone');
                    })        ;
                }
                if($guardian_education_cnt!= 0 ){
                    $result->leftjoin('char_persons_education as guardian_education','guardian_education.person_id','=','g.id');
                }
                if($guardian_work_cnt!= 0 ){
                    $result->leftjoin('char_persons_work as guardian_work','guardian_work.person_id','=','g.id')  ;
                }
                if($category_type == 1) {
                    if($mother_health_cnt!= 0 ){
                        $result->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id');
                    }
                    if($mother_work_cnt != 0 ){
                        $result->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id');
                    }
                    if($mother_education_cnt!= 0 ){
                        $result->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id');
                    }

                }

                if($father_health_cnt!= 0 ){
                    $result->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id');
                }
                if($father_work_cnt   != 0 ){
                    $result->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id');
                }
                if($father_education_cnt!= 0 ){
                    $result->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id');
                }
            }


        }

        if(isset($filters['deleted']) && $filters['deleted'] !=null && $filters['deleted'] !=""){
            if($filters['deleted']==false){
                $result= $result->whereNull('char_cases.deleted_at');
            }else
                if($filters['deleted']==true) {
                    $result= $result->whereNotNull('char_cases.deleted_at');
                }
        }else{
            $result= $result->whereNull('char_cases.deleted_at');
        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name',
                        'char_persons.third_name', 'char_persons.last_name',
                        'char_persons_i18n.first_name', 'char_persons_i18n.second_name', 'char_persons_i18n.third_name',
                        'char_persons_i18n.last_name','char_residence.repair_notes',
                        'char_cases.visitor','char_cases.notes','char_cases.visitor_card','char_cases.visitor_opinion',
                        'char_persons_work.work_location','char_persons_banks.account_owner',
                        'm.first_name', 'm.second_name', 'm.third_name', 'm.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name',
                        'char_persons_health.health_insurance_number','char_persons_health.used_device_name',
                        'char_persons_health.gov_health_details',
                        'char_persons.gov_commercial_records_details','char_residence.repair_notes',
                        'char_persons_work.work_location', 'char_persons_work.gov_work_details' ,
                        'char_persons_health.used_device_name', 'char_persons_health.gov_health_details'
                    ];


                    $live = ['m.death_date','f.death_date'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else if(in_array($key, $live)){
                                if($value == 0){
                                    $q->where($key, '=', null);
                                }else{
                                    $q->where($key, '!=', null);
                                }
                            }else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');


        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true || $filters['all_organization'] ==='true'){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false || $filters['all_organization'] ==='false'){
                $all_organization=1;
            }
        }

        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null &&
                $filters['organization_ids'] !="" && $filters['organization_ids'] !=[]) {

                if(is_array($filters['organization_ids'])){
                    if(sizeof($filters['organization_ids']) != 0){
                        if($filters['organization_ids'][0]==""){
                            unset($filters['organization_ids'][0]);
                        }
                        $organizations =$filters['organization_ids'];
                    }
                }
            }

            $organization_category=[];
            if(isset($filters['organization_category']) && $filters['organization_category'] !=null &&
                $filters['organization_category'] !="" && $filters['organization_category'] !=[] && sizeof($filters['organization_category']) != 0) {
                if($filters['organization_category'][0]==""){
                    unset($filters['organization_category'][0]);
                }
                $organization_category = $filters['organization_category'];
            }

            if (!empty($organizations)) {
                $result->where(function ($anq) use ($organizations,$organization_category) {
                    $anq->wherein('char_cases.organization_id', $organizations);
                    if(!empty($organization_category)){
                        $anq->wherein('org.category_id',$organization_category);
                    }
                });
            }
            else{

                if($UserType == 2) {
                    $result->where(function ($anq) use ($organization_id,$user,$organization_category) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });

                }else{

                    $result->where(function ($anq) use ($organization_id,$organization_category) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });
                }
            }
        }
        elseif($all_organization ==1){
            if(isset($filters['organizationsCases']) && !empty($filters['organizationsCases']) && $filters['organizationsCases'])
                $result= $result->where('char_cases.organization_id','!=',$organization_id);
            else
                $result= $result->where('char_cases.organization_id',$organization_id);
        }

        if(isset($filters['save_quran']) && ($filters['save_quran'] !=null || $filters['save_quran'] !="")){
//            'quran_parts', 'quran_chapters'
            if($filters['save_quran'] == 1){
                $result= $result->whereNotNull('char_persons_islamic_commitment.quran_parts');
            }else{
                $result->whereNull('char_persons_islamic_commitment.quran_parts');
            }
        }

        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

            $birthday_to=null;
            $birthday_from=null;

            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $result->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $result->whereDate('char_persons.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $result->whereDate('char_persons.birthday', '<=', $birthday_to);
            }

        }
        if((isset($filters['deleted_to']) && $filters['deleted_to'] !=null) || (isset($filters['deleted_from']) && $filters['deleted_from'] !=null)){

            $deleted_to=null;
            $deleted_from=null;

            if(isset($filters['deleted_to']) && $filters['deleted_to'] !=null){
                $deleted_to=date('Y-m-d',strtotime($filters['deleted_to']));
            }
            if(isset($filters['deleted_from']) && $filters['deleted_from'] !=null){
                $deleted_from=date('Y-m-d',strtotime($filters['deleted_from']));
            }
            if($deleted_from != null && $deleted_to != null) {
                $result->whereBetween( 'char_cases.deleted_at', [ $deleted_from, $deleted_to]);
            }elseif($deleted_from != null && $deleted_to == null) {
                $result->whereDate('char_cases.deleted_at', '>=', $deleted_from);
            }elseif($deleted_from == null && $deleted_to != null) {
                $result->whereDate('char_cases.deleted_at', '<=', $deleted_to);
            }


        }
        if((isset($filters['date_to']) && $filters['date_to'] !=null) || (isset($filters['date_from']) && $filters['date_from'] !=null)){

            $date_to=null;
            $date_from=null;

            if(isset($filters['date_to']) && $filters['date_to'] !=null){
                $date_to=date('Y-m-d',strtotime($filters['date_to']));
            }
            if(isset($filters['date_from']) && $filters['date_from'] !=null){
                $date_from=date('Y-m-d',strtotime($filters['date_from']));
            }
            if($date_from != null && $date_to != null) {
                $result->whereBetween( 'char_cases.created_at', [ $date_from, $date_to]);
            }elseif($date_from != null && $date_to == null) {
                $result->whereDate('char_cases.created_at', '>=', $date_from);
            }elseif($date_from == null && $date_to != null) {
                $result->whereDate('char_cases.created_at', '<=', $date_to);
            }


        }
        if((isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null) || (isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null)){
            $visited_at_to=null;
            $visited_at_from=null;

            if(isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null){
                $visited_at_to=date('Y-m-d',strtotime($filters['visited_at_to']));
            }
            if(isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null){
                $visited_at_from=date('Y-m-d',strtotime($filters['visited_at_from']));
            }

            if($visited_at_from != null && $visited_at_to != null) {
                $result->whereBetween( 'char_cases.visited_at', [ $visited_at_from, $visited_at_to]);
            }elseif($visited_at_from != null && $visited_at_to == null) {
                $result->whereDate('char_cases.visited_at', '>=', $visited_at_from);
            }elseif($visited_at_from == null && $visited_at_to != null) {
                $result->whereDate('char_cases.visited_at', '<=', $visited_at_to);
            }

        }
        if((isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !="")|| (isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !="")){

            $max_rank=null;
            $min_rank=null;

            if(isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !=""){
                $max_rank=$filters['max_rank'];
            }
            if(isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !=""){
                $min_rank=$filters['min_rank'];
            }

            if($min_rank != null && $max_rank != null) {
                $result->whereRaw(" ( char_cases.rank between ? and  ?)", array($min_rank, $max_rank));
            }
            elseif($max_rank != null && $min_rank == null) {
                $result->whereRaw(" $max_rank >= char_cases.rank");
            }
            elseif($max_rank == null && $min_rank != null) {
                $result->whereRaw(" $min_rank <= char_cases.rank");
            }
        }
        if((isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !="")||
            (isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !="")){

            $monthly_income_from=null;
            $monthly_income_to=null;
            if(isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !=""){
                $monthly_income_to=$filters['monthly_income_to'];
            }
            if(isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !=""){
                $monthly_income_from=$filters['monthly_income_from'];
            }

            if($monthly_income_from != null && $monthly_income_to != null) {
                $result->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
                $result->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
            elseif($monthly_income_to != null && $monthly_income_from == null) {
                $result->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
            }
            elseif($monthly_income_to == null && $monthly_income_from != null) {
                $result->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
        }

        if($category_type !=1) {
            if((isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !="")||
                (isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !="")){

                $max_family_count=null;
                $min_family_count=null;

                if(isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !=""){
                    $max_family_count=$filters['max_family_count'];
                }
                if(isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !=""){
                    $min_family_count=$filters['min_family_count'];
                }
                if($min_family_count != null && $max_family_count != null) {
                    $result = $result->whereRaw(" ( char_persons.family_cnt between ? and  ?)", array($min_family_count, $max_family_count));
                }elseif($max_family_count != null && $min_family_count == null) {
                    $result = $result->whereRaw(" $max_family_count >= char_persons.family_cnt");
                }elseif($max_family_count == null && $min_family_count != null) {
                    $result = $result->whereRaw(" $min_family_count <= char_persons.family_cnt");
                }
            }

            if(isset($filters['Properties'])) {
                for ($x = 0; $x < sizeof($filters['Properties']); $x++) {
                    $item=$filters['Properties'][$x];
                    if(isset($item['exist'])){
                        if($item['exist'] != "") {
                            $has_property=(int)$item['exist'];
                            
                            if($has_property == 1){
                                $op = '=';
                                $has_property_val = 1;

                                $result->whereNotIn('char_cases.person_id', function ($query) use ($item) {
                                    $query->select('person_id')
                                        ->from('char_persons_properties')
                                        ->where(function ($sq) use ($item){
                                            $sq->where('has_property', 1)
                                                ->where('property_id', $item['id']);
                                        });
                                });
                            }else{
                                $op = '!=';
                                $has_property_val = 1;

                                $result->whereNotIn('char_cases.person_id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_properties')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('has_property', 1)
                                                       ->where('property_id', $item['id']);
                                             });
                                   });
                            }
                            
//                            $id=(int)$item['id'];
//                            $table='char_persons_properties as prop'.$x;
//                            $result=$result ->join($table, function($q) use($x,$id,$has_property,$op,$has_property_val){
//                                $q->on('prop'.$x.'.person_id', '=', 'char_persons.id');
//                                $q->where('prop'.$x.'.property_id', '=', $id);
//                                $q->where('prop'.$x.'.has_property', '=', $has_property_val);
//                            });
                        }
                    }

                }
            }
            if(isset($filters['AidSources'])) {
                for ($x = 0; $x < sizeof($filters['AidSources']); $x++) {
                    $item=$filters['AidSources'][$x];
                    if(isset($item['aid_take'])){
                        if($item['aid_take'] != "") {
                            $aid_take=(int)$item['aid_take'];
                            if($aid_take == 1){
                                $op = '=';
                                $aid_take_val = 1;
                                $result->whereIn('char_persons.id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_aids')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('aid_take','=', 1)
                                                       ->where('aid_source_id', $item['id']);
                                             });
                                   });
                                
                            }else{
                                $op = '!=';
                                $aid_take_val = 1;  
                                 $result->whereNotIn('char_persons.id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_aids')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('aid_take','=', 1)
                                                       ->where('aid_source_id', $item['id']);
                                             });
                                   });
                                
                            }
//                            $id=(int)$item['id'];
//                            $table='char_persons_aids as aid'.$x;
//                            $result=$result ->join($table, function($q) use($x,$id,$aid_take,$op,$aid_take_val){
//                                $q->on('aid'.$x.'.person_id', '=', 'char_persons.id');
//                                $q->where('aid'.$x.'.aid_source_id', '=', $id);
//                                $q->where('aid'.$x.'.aid_take', $op, $aid_take_val);
//                            });
                        }
                    }

                }
            }
            if(isset($filters['Essential'])) {
                $custom=[];
                foreach($filters['Essential'] as $item){
                    if((isset($item['max']) && $item['max'] !=null && $item['max'] !="" ) ||
                        isset($item['min']) && $item['min'] !=null && $item['min'] !="" ){
                        array_push($custom,$item);
                    }
                }

                foreach($custom as $k => $value) {
                    $max=null;
                    $min=null;
                    $id=(int)$value['id'];
                    $table='char_cases_essentials as essent'.$k;
                    $need='essent'.$k.'.needs';

                    if(isset($value['max']) && $value['max'] !=null && $value['max'] !=""){
                        $max=(int)$value['max'];
                    }
                    if(isset($value['min']) && $value['min'] !=null && $value['min'] !=""){
                        $min=(int)$value['min'];
                    }

                    $result=$result ->join($table, function($q) use($k,$id,$min,$max) {
                        $q->on('essent' . $k . '.case_id', '=', 'char_cases.id');
                        $q->where('essent' . $k . '.essential_id', '=', $id);

                        if($min != null && $max != null) {
                            $q->where('essent'.$k.'.needs', '>=', $min);
                            $q->where('essent'.$k.'.needs', '<=', $max);
                        }elseif($max == null && $min != null) {
                            $q->where('essent'.$k.'.needs', '>=', $min);
                        }elseif($max != null && $min == null) {
                            $q->where('essent'.$k.'.needs', '<=', $max);
                        }
                    });
                }
            }

            if((isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !="")||
                (isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !="")){

                $max_total_vouchers=null;
                $min_total_vouchers=null;
                if(isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !=""){
                    $max_total_vouchers=$filters['max_total_vouchers'];
                }
                if(isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !=""){
                    $min_total_vouchers=$filters['min_total_vouchers'];
                }

                if($min_total_vouchers != null && $max_total_vouchers != null) {
                    $result->whereRaw(" ( char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') between ? and  ?)", array($min_total_vouchers, $max_total_vouchers));
                }
                elseif($max_total_vouchers != null && $min_total_vouchers == null) {
                    $result->whereRaw(" $max_total_vouchers >= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
                }
                elseif($max_total_vouchers == null && $min_total_vouchers != null) {
                    $result->whereRaw(" $min_total_vouchers <= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
                }
            }

            if(isset($filters['voucher_sponsors']) || isset($filters['voucher_organization']) || isset($filters['voucher_ids']) ||
                isset($filters['got_voucher']) || isset($filters['voucher_to']) || isset($filters['voucher_from'])){

                $voucher_organization=[];
//                if($master == true){
                    if(isset($filters['voucher_organization']) && $filters['voucher_organization'] !=null &&
                        $filters['voucher_organization'] !="" && $filters['voucher_organization'] !=[] && sizeof($filters['voucher_organization'] )!= 0) {
                        if($filters['voucher_organization'][0]==""){
                            unset($filters['voucher_organization'][0]);
                        }
                        $voucher_organization =$filters['voucher_organization'];
                    }
//                }else{
//                    $voucher_organization =[$organization_id];
//                }

                $voucher_sponsors=[];
                if(isset($filters['voucher_sponsors']) && $filters['voucher_sponsors'] !=null && $filters['voucher_sponsors'] !="" &&
                    $filters['voucher_sponsors'] !=[] && sizeof($filters['voucher_sponsors'] ) != 0) {
                    if($filters['voucher_sponsors'][0]==""){
                        unset($filters['voucher_sponsors'][0]);
                    }
                    $voucher_sponsors =$filters['voucher_sponsors'];
                }

                $voucher_ids=[];
                if(isset($filters['voucher_ids']) && $filters['voucher_ids'] !=null && $filters['voucher_ids'] !="" &&
                    $filters['voucher_ids'] !=[] && sizeof($filters['voucher_ids'] )!= 0) {
                    if($filters['voucher_ids'][0]==""){
                        unset($filters['voucher_ids'][0]);
                    }
                    $voucher_ids =$filters['voucher_ids'];
                }

                $voucher_to=null;
                $voucher_from=null;
                if(isset($filters['voucher_to']) && $filters['voucher_to'] !=null){
                    $voucher_to=date('Y-m-d',strtotime($filters['voucher_to']));
                }
                if(isset($filters['voucher_from']) && $filters['voucher_from'] !=null){
                    $voucher_from=date('Y-m-d',strtotime($filters['voucher_from']));
                }
                if(isset($filters['got_voucher']) && $filters['got_voucher'] !=null  && $filters['got_voucher'] !="") {
                    if($filters['got_voucher']==0 ){
                        $result->whereNotIn('char_persons.id', function($query)use($organization_id,$user,$voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                            $query->select('person_id')->from('char_vouchers_persons');

                            $query->join('char_vouchers', function($q) use($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors){
                                $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                                $q->whereNull('char_vouchers.deleted_at');
                            });

                            $query->where(function ($query_) use ($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors,$voucher_from,$voucher_to) {

                                if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0 ){

                                    if(sizeof($voucher_organization) !=0){
                                        $query_->wherein('char_vouchers.organization_id', $voucher_organization);
                                    }
                                    if(sizeof($voucher_sponsors) !=0){
                                        $query_->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                    }

                                    if(sizeof($voucher_ids) !=0){
                                        $query_->wherein('char_vouchers.id', $voucher_ids);
                                    }
                                }else{
                                    if($user->type == 2) {
                                        $query_->where(function ($anq) use ($organization_id,$user) {
                                            $anq->where('char_vouchers.organization_id',$organization_id);
                                            $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                                $quer->select('organization_id')
                                                    ->from('char_user_organizations')
                                                    ->where('user_id', '=', $user->id);
                                            });
                                        });

                                    }
                                    else{
//
                                        $query_->wherein('char_vouchers.organization_id', function($quer) use($organization_id) {
                                            $quer->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    }
                                }

                                if($voucher_from != null && $voucher_to != null) {
                                    $query_->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                                }elseif($voucher_from != null && $voucher_to == null) {
                                    $query_->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                                }elseif($voucher_from == null && $voucher_to != null) {
                                    $query_->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                                }

                            });


                        });
                    }else{
                        $result->whereIn('char_persons.id', function($query)use($organization_id,$user,$voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                            $query->select('person_id')->from('char_vouchers_persons');

                            $query->join('char_vouchers', function($q) use($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors){
                                $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                                $q->whereNull('char_vouchers.deleted_at');
                            });

                            $query->where(function ($query_) use ($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors,$voucher_from,$voucher_to) {

                                if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0 ){

                                    if(sizeof($voucher_organization) !=0){
                                        $query_->wherein('char_vouchers.organization_id', $voucher_organization);
                                    }
                                    if(sizeof($voucher_sponsors) !=0){
                                        $query_->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                    }

                                    if(sizeof($voucher_ids) !=0){
                                        $query_->wherein('char_vouchers.id', $voucher_ids);
                                    }
                                }else{
                                    if($user->type == 2) {
                                        $query_->where(function ($anq) use ($organization_id,$user) {
                                            $anq->where('char_vouchers.organization_id',$organization_id);
                                            $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                                $quer->select('organization_id')
                                                    ->from('char_user_organizations')
                                                    ->where('user_id', '=', $user->id);
                                            });
                                        });

                                    }
                                    else{
//
                                        $query_->wherein('char_vouchers.organization_id', function($quer) use($organization_id) {
                                            $quer->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    }
                                }

                                if($voucher_from != null && $voucher_to != null) {
                                    $query_->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                                }elseif($voucher_from != null && $voucher_to == null) {
                                    $query_->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                                }elseif($voucher_from == null && $voucher_to != null) {
                                    $query_->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                                }

                            });

                        });
                    }
                }
            }

        }
        else{


            if((isset($filters['max_brother_number']) && $filters['max_brother_number'] !=null && $filters['max_brother_number'] !="")||
                (isset($filters['min_brother_number']) && $filters['min_brother_number'] !=null && $filters['min_brother_number'] !="")){

                $max_brother_number=null;
                $min_brother_number=null;

                if(isset($filters['max_brother_number']) && $filters['max_brother_number'] !=null && $filters['max_brother_number'] !=""){
                    $max_brother_number=$filters['max_brother_number'];
                }
                if(isset($filters['min_brother_number']) && $filters['min_brother_number'] !=null && $filters['min_brother_number'] !=""){
                    $min_brother_number=$filters['min_brother_number'];
                }

                if($min_brother_number != null && $max_brother_number != null) {
                    $result->whereRaw(' char_categories.case_is = 1 and char_categories.family_structure= 2 and ( char_get_family_count ( "left",char_persons.father_id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                    $result->orwhereRaw(' char_categories.case_is = 1 and char_categories.family_structure=1 and ( char_get_family_count ( "left",char_persons.id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                    $result->orwhereRaw(' char_categories.case_is = 2 and char_categories.family_structure= 2 and ( char_get_family_count ( "left",char_guardians.guardian_id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                }elseif($max_brother_number != null && $min_brother_number == null) {
                    $result->whereRaw(" char_categories.case_is = 1 and char_categories.family_structure= 2 and ( $max_brother_number >= char_get_family_count('left',char_persons.father_id,null,char_persons.id))");
                    $result->orwhereRaw(" char_categories.case_is = 1 and char_categories.family_structure=1 and ( $max_brother_number >= char_get_family_count('left',char_persons.id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 2 and char_categories.family_structure= 2 and ( $max_brother_number >= char_get_family_count('left',char_guardians.guardian_id,null,char_persons.id ))");

                }elseif($max_brother_number == null && $min_brother_number != null) {
                    $result->whereRaw(" char_categories.case_is = 1 and char_categories.family_structure= 2 and ( $min_brother_number <= char_get_family_count('left',char_persons.father_id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 1 and char_categories.family_structure=1 and ( $min_brother_number <= char_get_family_count('left',char_persons.id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 2 and char_categories.family_structure= 2 and ( $min_brother_number <= char_get_family_count('left',char_guardians.guardian_id,null,char_persons.id ))");
                }
            }
            if((isset($filters['max_monthy_payments_average']) && $filters['max_monthy_payments_average'] !=null && $filters['max_monthy_payments_average'] !="")||
                (isset($filters['min_monthy_payments_average']) && $filters['min_monthy_payments_average'] !=null && $filters['min_monthy_payments_average'] !="")){
                $max_monthy_payments_average=null;
                $min_monthy_payments_average=null;
                if(isset($filters['max_monthy_payments_average']) && $filters['max_monthy_payments_average'] !=null && $filters['max_monthy_payments_average'] !=""){
                    $max_monthy_payments_average=$filters['max_monthy_payments_average'];
                }
                if(isset($filters['min_monthy_payments_average']) && $filters['min_monthy_payments_average'] !=null && $filters['min_monthy_payments_average'] !=""){
                    $min_monthy_payments_average=$filters['min_monthy_payments_average'];
                }

                if($min_monthy_payments_average != null && $max_monthy_payments_average != null) {
                    $result->whereRaw(" ( char_get_person_average_payments ( c.id ,'$first', '$last') between ? and  ?)", array($min_monthy_payments_average, $max_monthy_payments_average));
                }elseif($max_monthy_payments_average != null && $min_monthy_payments_average == null) {
                    $result->whereRaw(" $max_monthy_payments_average >= char_get_person_average_payments ( c.id ,'$first', '$last')");
                }elseif($max_monthy_payments_average == null && $min_monthy_payments_average != null) {
                    $result->whereRaw(" $min_monthy_payments_average <= char_get_person_average_payments ( c.id ,'$first', '$last')");
                }

            }

            if((isset($filters['max_total_amount_of_payments']) && $filters['max_total_amount_of_payments'] !=null && $filters['max_total_amount_of_payments'] !="") ||
                (isset($filters['min_total_amount_of_payments']) && $filters['min_total_amount_of_payments'] !=null && $filters['min_total_amount_of_payments'] !="")){


                $max_total_amount_of_payments=null;
                $min_total_amount_of_payments=null;
                if(isset($filters['max_total_amount_of_payments']) && $filters['max_total_amount_of_payments'] !=null && $filters['max_total_amount_of_payments'] !=""){
                    $max_total_amount_of_payments=$filters['max_total_amount_of_payments'];
                }
                if(isset($filters['min_total_amount_of_payments']) && $filters['min_total_amount_of_payments'] !=null && $filters['min_total_amount_of_payments'] !=""){
                    $min_total_amount_of_payments=$filters['min_total_amount_of_payments'];
                }

                if($min_total_amount_of_payments != null && $max_total_amount_of_payments != null) {
                    $result->whereRaw(" ( char_get_person_total_payments ( c.id ,'$first', '$last') between ? and  ?)", array($min_total_amount_of_payments, $max_total_amount_of_payments));
                }
                elseif($max_total_amount_of_payments != null && $min_total_amount_of_payments == null) {
                    $result->whereRaw(" $max_total_amount_of_payments >= char_get_person_total_payments ( c.id ,'$first', '$last')");
                }
                elseif($max_total_amount_of_payments == null && $min_total_amount_of_payments != null) {
                    $result->whereRaw(" $min_total_amount_of_payments <= char_get_person_total_payments ( c.id ,'$first', '$last')");
                }

            }


            $sponsor=[];
            if(isset($filters['sponsor']) && $filters['sponsor'] !=null && $filters['sponsor'] !=[] && sizeof($filters['sponsor']) != 0) {
                if($filters['sponsor'][0]==""){
                    unset($filters['sponsor'][0]);
                }
                $sponsor =$filters['sponsor'];
            }
            if(isset($filters['guaranteed_status']) && $filters['guaranteed_status'] !=null && $filters['guaranteed_status'] !=""){
                $guaranteed_status=$filters['guaranteed_status'];

                if(in_array($guaranteed_status,[1,2,3,4,5])){
                    $result->whereIn('char_cases.id', function($query)use($guaranteed_status,$sponsor){
                        if(sizeof($sponsor) ==0){
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status', $guaranteed_status);

                        }else{
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status', $guaranteed_status)
                                ->wherein('char_sponsorship_cases.sponsor_id', $sponsor);
                        }
                    });
                }
                if(in_array($guaranteed_status,[-1,-2,-3,-4,-5])){
                    $guaranteed_status=$guaranteed_status * -1;
                    $result->whereNotIn('char_cases.id', function($query)use($guaranteed_status,$sponsor){
                        if(sizeof($sponsor) ==0){
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status' ,'!=',$guaranteed_status);

                        }else{
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status','!=', $guaranteed_status)
                                ->wherein('char_sponsorship_cases.sponsor_id', $sponsor);
                        }
                    });
                }
            }

            if((isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null) ||
                (isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null)) {

                $mother_birthday_to=null;
                $mother_birthday_from=null;

                if(isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null){
                    $mother_birthday_to=date('Y-m-d',strtotime($filters['mother_birthday_to']));
                }
                if(isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null){
                    $mother_birthday_from=date('Y-m-d',strtotime($filters['mother_birthday_from']));
                }

                if($mother_birthday_from != null && $mother_birthday_to != null) {
                    $result->whereBetween( 'm.birthday', [ $mother_birthday_from, $mother_birthday_to]);
                }elseif($mother_birthday_from != null && $mother_birthday_to == null) {
                    $result->whereDate('m.birthday', '>=', $mother_birthday_from);
                }elseif($mother_birthday_from == null && $mother_birthday_to != null) {
                    $result->whereDate('m.birthday', '<=', $mother_birthday_to);
                }

            }

            if((isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null) ||
                (isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null)) {
                $mother_death_date_to=null;
                $mother_death_date_from=null;

                if(isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null){
                    $mother_death_date_to=date('Y-m-d',strtotime($filters['mother_death_date_to']));
                }
                if(isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null){
                    $mother_death_date_from=date('Y-m-d',strtotime($filters['mother_death_date_from']));
                }
                if($mother_death_date_from != null && $mother_death_date_to != null) {
                    $result->whereBetween( 'm.death_date', [ $mother_death_date_from, $mother_death_date_to]);
                }elseif($mother_death_date_from != null && $mother_death_date_to == null) {
                    $result->whereDate('m.death_date', '>=', $mother_death_date_from);
                }elseif($mother_death_date_from == null && $mother_death_date_to != null) {
                    $result->whereDate('m.death_date', '<=', $mother_death_date_to);
                }
            }

            if((isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null) ||
                (isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null)) {

                $father_death_date_to=null;
                $father_death_date_from=null;

                if(isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null){
                    $father_death_date_to=date('Y-m-d',strtotime($filters['father_death_date_to']));
                }
                if(isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null){
                    $father_death_date_from=date('Y-m-d',strtotime($filters['father_death_date_from']));
                }
                if($father_death_date_from != null && $father_death_date_to != null) {
                    $result->whereBetween( 'f.death_date', [ $father_death_date_from, $father_death_date_to]);
                }elseif($father_death_date_from != null && $father_death_date_to == null) {
                    $result->whereDate('f.death_date', '>=', $father_death_date_from);
                }elseif($father_death_date_from == null && $father_death_date_to != null) {
                    $result->whereDate('f.death_date', '<=', $father_death_date_to);
                }
            }

            if((isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null)||
                (isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null)){

                $father_birthday_to=null;
                $father_birthday_from=null;

                if(isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null){
                    $father_birthday_to=date('Y-m-d',strtotime($filters['father_birthday_to']));
                }
                if(isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null){
                    $father_birthday_from=date('Y-m-d',strtotime($filters['father_birthday_from']));
                }

                if($father_birthday_from != null && $father_birthday_to != null) {
                    $result->whereBetween( 'f.birthday', [ $father_birthday_from, $father_birthday_to]);
                }
                elseif($father_birthday_from != null && $father_birthday_to == null) {
                    $result->whereDate('f.birthday', '>=', $father_birthday_from);
                }
                elseif($father_birthday_from == null && $father_birthday_to != null) {
                    $result->whereDate('f.birthday', '<=', $father_birthday_to);
                }

            }

        }

        if($filters['action'] =='ExportToWord' || $filters['action'] =='pdf' || 
           $filters['action'] =='childs' || $filters['action'] =='parents' || $filters['action'] =='wives' ||$filters['action'] =='FamilyMember' || $filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv'){
                if(isset($filters['persons'])){
                   if(sizeof($filters['persons']) > 0 ){
                      $result->whereIn('char_cases.id',$filters['persons']);
                   } 
               }    
       }
   
        $map = [
            "organization_name" => 'org.name',
            "name" => 'char_persons.first_name',
            "guardian_name" => 'g.first_name',
            "id_card_number" => 'char_persons.id_card_number',
            "gender" => 'char_persons.gender',
            "case_gender" => 'char_persons.gender',

            "marital_status" => 'char_marital_status.name',

            "category_name" => 'char_categories.name',
            "rank" => 'char_cases.rank',
            "status" => 'char_cases.status',
            "cases_status" => 'char_cases.status',
            "visitor" => 'char_cases.visitor',
            "delete_reason" => 'char_cases.reason',
            "deleted_at" => 'char_cases.reason',

        ];

        if($category_type == 2){
            $map['primary_mobile'] = 'primary_num.contact_value';
            $map['region_name'] = 'region_name.name';
            $map['governarate'] = 'district_name.name';
        }else{
            $map['primary_mobile'] = 'guardian_cont2.contact_value';
            $map['region_name'] = 'L2.name';
            $map['governarate'] = 'L3.name';
        }

        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','desc');
                    $result->orderBy('char_persons.second_name','desc');
                    $result->orderBy('char_persons.third_name','desc');
                    $result->orderBy('char_persons.last_name','desc');
                }elseif($key_ == 'guardian_name'){
                    $result->orderBy('g.first_name','desc');
                    $result->orderBy('g.second_name','desc');
                    $result->orderBy('g.third_name','desc');
                    $result->orderBy('g.last_name','desc');
                }else{
                    $result->orderBy($map[$key_],'desc');
                }
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','asc');
                    $result->orderBy('char_persons.second_name','asc');
                    $result->orderBy('char_persons.third_name','asc');
                    $result->orderBy('char_persons.last_name','asc');
                }elseif($key_ == 'guardian_name'){
                    $result->orderBy('g.first_name','asc');
                    $result->orderBy('g.second_name','asc');
                    $result->orderBy('g.third_name','asc');
                    $result->orderBy('g.last_name','asc');
                }else{
                    $result->orderBy($map[$key_],'asc');
                }
            }
        }


        if(!$order){
            $result->orderBy('char_cases.rank','char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');
        }

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        if($filters['action'] =='ExportToWord' || $filters['action'] =='pdf'){

            if($category_type == 1){
                $fetch_array =array(
                    'action' => 'show',
                    'category'      => true,
                    'full_name'     => true,
                    'family_member' => true,
                    'person'        => true,
                    'persons_i18n'  => true,
                    'contacts'      => true,
                    'education'     => true,
                    'islamic'       => true,
                    'health'        => true,
                    'work'          => true,
                    'residence'     => true,
                    'default_bank'  => true,
                    'banks'         => true,
                    'persons_documents'  => true,
                    'organization_name'      => true,
                    'father_id'     => true,
                    'mother_id'     => true,
                    'guardian_id'   => true,
                    'guardian_kinship'   => true,
                    'visitor_note'  => true
                );
            }
            else{
                $fetch_array =array(
                    'action' => 'show',
                    'category'      => true,
                    'organization_name'      => true,
                    'full_name'     => true,
                    'family_member' => true,
                    'person'        => true,
                    'persons_i18n'  => true,
                    'contacts'      => true,
                    'work'          => true,
                    'residence'    => true,
                    'case_needs'    => true,
                    'default_bank'   => true,
                    'banks'         => true,
                    'home_indoor'   => true,
                    'reconstructions' => true,
                    'financial_aid_source' => true,
                    'non_financial_aid_source' => true,
                    'persons_properties' => true,
                    'persons_documents'  => true,
                    'visitor_note' => true,
                    'health' => true,
                );
            }

            if($category_type ==1) {
                $result->selectRaw("char_persons.father_id,char_persons.mother_id,char_guardians.guardian_id");
            }

            $result= $result->selectRaw("char_cases.id")->get();
            foreach($result as $item) {
                $sub= self::fetch($fetch_array,$item->id);
                if($sub){
                    if($category_type ==1){
                        $get=array('action'=>'show','person'=>true,'persons_i18n'=>true,'health'=>true,'education'=>true,'work'=>true);

                        if($item->father_id != null){
                            $get['id'] = $item->father_id;
                            $sub->father =\Common\Model\Person::fetch($get);
                        }

                        if($item->mother_id != null) {
                            $get['id'] = $item->mother_id;
                            $sub->mother = \Common\Model\Person::fetch($get);
                        }
                        if($item->guardian_id != null) {
                            $sub->guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $item->guardian_id,'person' =>true,    'persons_i18n' => true, 'contacts' => true,  'education' => true,
                                'work' => true, 'residence' => true, 'banks' => true));
                        }
                    }
                    array_push($output,$sub);
                }
            }
        }
        else if($filters['action'] =='FamilyMember'){


            if($category_type ==1) {
                $result->selectRaw("char_persons.father_id,char_persons.mother_id,char_guardians.guardian_id");
            }
            if($category_type ==1) {
                $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
            }

            $result= $result->selectRaw("char_cases.id as case_id,
                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                              CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                              CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();

            $keys_options=array(
                'action' => 'show',
                'category_type' => $category_type,
                'category' => false,
                'gender' => false,
                'family_member' => true,
                'full_name' => false,
                'visitor_note' => false,
                'person' => false,
                'persons_i18n' => false,
                'contacts' => true,
                'education' => false,
                'islamic' => false,
                'health' => false,
                'work' => false,
                'residence' => false,
                'case_needs' => false,
                'banks' => false,
                'home_indoor' => false,
                'reconstructions' => false,
                'financial_aid_source' => false,
                'non_financial_aid_source' => false,
                'persons_properties' => false,
                'persons_documents' => false,
                'case_sponsorships' => false,
                'case_payments' => false,
                'father_id' => true,
                'mother_id' => true,
                'guardian_id' => true,
                'father' => false,
                'mother' => false,
                'guardian' => true,
                'guardian_kinship' => true,
                'family_payments' => false,
                'guardian_payments' => false
            );
            if ($category_type == 1) {
                $keys_options['father_id'] = true;
                $keys_options['mother_id'] = true;
                $keys_options['guardian_id' ] =  true;
                $keys_options['father'] =  false;
                $keys_options['mother'] = false;
                $keys_options['guardian'] =  false;
                $keys_options['family_payments'] =  false;
                $keys_options['guardian_payments'] =  false;
                $keys_options['guardian_kinship'] =  false;
            }
            foreach($result as $item) {
                $members = \Common\Model\CaseModel::fetch($keys_options, $item->case_id);

                if (sizeof($members['family_member']) > 0) {
                    foreach ($members['family_member'] as $key => $member) {
                        if ($category_type == 1) {
                            $member->father_name = $item->father_name;
                            $member->mother_name = $item->mother_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_id_card_number = $item->guardian_id_card_number;
                            $member->mother_id_card_number = $item->mother_id_card_number;
                            $member->father_id_card_number = $item->father_id_card_number;
                        } else {
                            $member->father_name = $item->full_name;
                            $member->father_id_card_number = $item->id_card_number;
                        }
                        array_push($output, $member);
                    }
                }

            }

            return $output;
        }
        else if($filters['action'] =='childs'){

            if($category_type ==1) {
                $result->selectRaw("char_persons.father_id,char_persons.mother_id,char_guardians.guardian_id");
            }
            if($category_type ==1) {
                $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
            }

            $result= $result->selectRaw("char_cases.id as case_id,
                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                              CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                              CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();

            $keys_options=array(
                'action' => 'show',
                'category_type' => $category_type,
                'category' => false,
                'gender' => false,
                'family_member' => true,
                'full_name' => false,
                'visitor_note' => false,
                'person' => false,
                'persons_i18n' => false,
                'contacts' => true,
                'education' => false,
                'islamic' => false,
                'health' => false,
                'work' => false,
                'residence' => false,
                'case_needs' => false,
                'banks' => false,
                'home_indoor' => false,
                'reconstructions' => false,
                'financial_aid_source' => false,
                'non_financial_aid_source' => false,
                'persons_properties' => false,
                'persons_documents' => false,
                'case_sponsorships' => false,
                'case_payments' => false,
                'father_id' => true,
                'mother_id' => true,
                'guardian_id' => true,
                'father' => false,
                'mother' => false,
                'guardian' => true,
                'guardian_kinship' => true,
                'family_payments' => false,
                'guardian_payments' => false,
                'childs' => true
            );
            if ($category_type == 1) {
                $keys_options['father_id'] = true;
                $keys_options['mother_id'] = true;
                $keys_options['guardian_id' ] =  true;
                $keys_options['father'] =  false;
                $keys_options['mother'] = false;
                $keys_options['guardian'] =  false;
                $keys_options['family_payments'] =  false;
                $keys_options['guardian_payments'] =  false;
                $keys_options['guardian_kinship'] =  false;
            }
            foreach($result as $item) {
                $members = \Common\Model\CaseModel::fetch($keys_options, $item->case_id);

                if (sizeof($members['family_member']) > 0) {
                    foreach ($members['family_member'] as $key => $member) {
                        if ($category_type == 1) {
                            $member->father_name = $item->father_name;
                            $member->mother_name = $item->mother_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_id_card_number = $item->guardian_id_card_number;
                            $member->mother_id_card_number = $item->mother_id_card_number;
                            $member->father_id_card_number = $item->father_id_card_number;
                        } else {
                            $member->father_name = $item->full_name;
                            $member->father_id_card_number = $item->id_card_number;
                        }
                        array_push($output, $member);
                    }
                }

            }

            return $output;
        }
        else if($filters['action'] =='wives'){

            if($category_type ==1) {
                $result->selectRaw("char_persons.father_id,char_persons.mother_id,char_guardians.guardian_id");
            }
            if($category_type ==1) {
                $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
            }

            $result= $result->selectRaw("char_cases.id as case_id,
                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                              CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                              CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();

            $keys_options=array(
                'action' => 'show',
                'category_type' => $category_type,
                'category' => false,
                'gender' => false,
                'family_member' => true,
                'full_name' => false,
                'visitor_note' => false,
                'person' => false,
                'persons_i18n' => false,
                'contacts' => true,
                'education' => false,
                'islamic' => false,
                'health' => false,
                'work' => false,
                'residence' => false,
                'case_needs' => false,
                'banks' => false,
                'home_indoor' => false,
                'reconstructions' => false,
                'financial_aid_source' => false,
                'non_financial_aid_source' => false,
                'persons_properties' => false,
                'persons_documents' => false,
                'case_sponsorships' => false,
                'case_payments' => false,
                'father_id' => true,
                'mother_id' => true,
                'guardian_id' => true,
                'father' => false,
                'mother' => false,
                'guardian' => true,
                'guardian_kinship' => true,
                'family_payments' => false,
                'guardian_payments' => false,
                'wives' => true
            );
            if ($category_type == 1) {
                $keys_options['father_id'] = true;
                $keys_options['mother_id'] = true;
                $keys_options['guardian_id' ] =  true;
                $keys_options['father'] =  false;
                $keys_options['mother'] = false;
                $keys_options['guardian'] =  false;
                $keys_options['family_payments'] =  false;
                $keys_options['guardian_payments'] =  false;
                $keys_options['guardian_kinship'] =  false;
            }
            foreach($result as $item) {
                $members = \Common\Model\CaseModel::fetch($keys_options, $item->case_id);

                if (sizeof($members['family_member']) > 0) {
                    foreach ($members['family_member'] as $key => $member) {
                        if ($category_type == 1) {
                            $member->father_name = $item->father_name;
                            $member->mother_name = $item->mother_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_id_card_number = $item->guardian_id_card_number;
                            $member->mother_id_card_number = $item->mother_id_card_number;
                            $member->father_id_card_number = $item->father_id_card_number;
                        } else {
                            $member->father_name = $item->full_name;
                            $member->father_id_card_number = $item->id_card_number;
                        }
                        array_push($output, $member);
                    }
                }

            }

            return $output;
        }
        else if($filters['action'] =='parents'){

            if($category_type ==1) {
                $result->selectRaw("char_persons.father_id,char_persons.mother_id,char_guardians.guardian_id");
            }
            if($category_type ==1) {
                $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
            }

            $result= $result->selectRaw("char_cases.id as case_id,
                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                              CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                              CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();

            $keys_options=array(
                'action' => 'show',
                'category_type' => $category_type,
                'category' => false,
                'gender' => false,
                'family_member' => true,
                'full_name' => false,
                'visitor_note' => false,
                'person' => false,
                'persons_i18n' => false,
                'contacts' => true,
                'education' => false,
                'islamic' => false,
                'health' => false,
                'work' => false,
                'residence' => false,
                'case_needs' => false,
                'banks' => false,
                'home_indoor' => false,
                'reconstructions' => false,
                'financial_aid_source' => false,
                'non_financial_aid_source' => false,
                'persons_properties' => false,
                'persons_documents' => false,
                'case_sponsorships' => false,
                'case_payments' => false,
                'father_id' => true,
                'mother_id' => true,
                'guardian_id' => true,
                'father' => false,
                'mother' => false,
                'guardian' => true,
                'guardian_kinship' => true,
                'family_payments' => false,
                'guardian_payments' => false,
                'parents' => true
            );
            if ($category_type == 1) {
                $keys_options['father_id'] = true;
                $keys_options['mother_id'] = true;
                $keys_options['guardian_id' ] =  true;
                $keys_options['father'] =  false;
                $keys_options['mother'] = false;
                $keys_options['guardian'] =  false;
                $keys_options['family_payments'] =  false;
                $keys_options['guardian_payments'] =  false;
                $keys_options['guardian_kinship'] =  false;
            }
            foreach($result as $item) {
                $members = \Common\Model\CaseModel::fetch($keys_options, $item->case_id);

                if (sizeof($members['family_member']) > 0) {
                    foreach ($members['family_member'] as $key => $member) {
                        if ($category_type == 1) {
                            $member->father_name = $item->father_name;
                            $member->mother_name = $item->mother_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_name = $item->guardian_name;
                            $member->guardian_id_card_number = $item->guardian_id_card_number;
                            $member->mother_id_card_number = $item->mother_id_card_number;
                            $member->father_id_card_number = $item->father_id_card_number;
                        } else {
                            $member->father_name = $item->full_name;
                            $member->father_id_card_number = $item->id_card_number;
                        }
                        array_push($output, $member);
                    }
                }

            }

            return $output;
        }
        else if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {
            if(isset($filters['deleted']) && $filters['deleted'] !=null && $filters['deleted'] !="" && $filters['deleted']==true){
                $output = $result->selectRaw("
                                       CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ', ifnull(char_persons.street_address,' ')) AS address,
                                        CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                        CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                        CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone,
                                        char_cases.reason as delete_reason,
                                        char_cases.deleted_at
                                      ")->get();
            }
            else{
                $result->selectRaw("char_cases.id as case_id,
                                 char_cases.created_at as created_at_case,
                                 char_persons.id as person_id,
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                 CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                 org.name as organization_name,
                                 CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                 CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                 CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type
                                ");
                if($category_type ==1) {
                    $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.old_id_card_number  END  AS mother_old_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.old_id_card_number  END  AS father_old_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
                }


                $result->selectRaw("CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                    CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  
                                    CASE
                                           WHEN char_persons.gender is null THEN ' '
                                           WHEN char_persons.gender = 1 THEN '$male'
                                           WHEN char_persons.gender = 2 THEN '$female'
                                     END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status_id ");

                if($category_type == 2) {
                    $result->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted");
                }
                $result->selectRaw("CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                     CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS nationality,
                                           char_persons.family_cnt AS family_count,
                                           char_persons.spouses AS spouses,
                                           char_persons.male_live AS male_count,
                                           char_persons.female_live AS female_count
                                          ");
                if($category_type == 2){

                    $result->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                        CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                        CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                        CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address");
                }
                else{
                    $result->selectRaw("CASE WHEN char_persons.country  is null THEN ' ' Else L4.name END   AS country,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name,
                                        CASE WHEN char_persons.location_id is null THEN ' ' Else L1.name END   AS nearLocation,
                                        CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ',
                                                  ifnull(mosques_name.name,' '),' ',
                                       ifnull(char_persons.street_address,' '))
                                     AS address");
                }

                if($category_type == 2) {
                    $result->selectRaw("CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                     CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                     CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone");

                }
                $result->selectRaw("CASE WHEN char_persons.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                 CASE
                                      WHEN char_persons.receivables is null THEN '0'
                                      WHEN char_persons.receivables = 0 THEN '0'
                                      WHEN char_persons.receivables = 1 THEN char_persons.receivables_sk_amount
                                 END AS receivables_sk_amount ,
                                 CASE
                                               WHEN char_persons.is_qualified is null THEN '-'
                                               WHEN char_persons.is_qualified = 0 THEN '$not_qualified '
                                               WHEN char_persons.is_qualified = 1 THEN ' $qualified '
                                         END
                                        AS is_qualified,
                                 CASE WHEN char_persons.qualified_card_number is null THEN '-' Else char_persons.qualified_card_number  END  AS qualified_card_number,
                                 CASE WHEN char_persons.has_commercial_records = 1 THEN '$yes' Else '$no' END AS has_commercial_records,
                                 CASE
                                      WHEN char_persons.has_commercial_records is null THEN '0'
                                      WHEN char_persons.has_commercial_records = 0 THEN '0'
                                      WHEN char_persons.has_commercial_records = 1 THEN char_persons.active_commercial_records
                                 END AS active_commercial_records ,
                                  CASE
                                      WHEN char_persons.has_commercial_records is null THEN ' '
                                      WHEN char_persons.has_commercial_records = 0 THEN ' '
                                      WHEN char_persons.has_commercial_records = 1 THEN char_persons.gov_commercial_records_details
                                 END AS gov_commercial_records_details,
                                 CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                    CASE WHEN char_persons.actual_monthly_income is null THEN ' ' Else char_persons.actual_monthly_income  END  AS actual_monthly_income,
                                    CASE
                                       WHEN char_persons_work.working is null THEN ' '
                                       WHEN char_persons_work.working = 1 THEN '$yes'
                                       WHEN char_persons_work.working = 2 THEN '$no'
                                     END
                                     AS working,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_status.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_status,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_job.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_job,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_wage.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_wage,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_location,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN '$yes'
                                           WHEN char_persons_work.can_work = 2 THEN '$no'
                                     END
                                     AS can_work,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN ' '
                                           WHEN char_persons_work.can_work = 2 THEN work_reason.name
                                     END
                                     AS work_reason,
                                     CASE WHEN char_persons_work.has_other_work_resources is null THEN ' '
                                          WHEN char_persons_work.has_other_work_resources = 1 THEN '$yes'
                                          WHEN char_persons_work.has_other_work_resources = 0 THEN '$no'
                                     END AS has_other_work_resources,
                                     CASE WHEN char_persons_work.gov_work_details is null THEN ' ' Else char_persons_work.gov_work_details  END  AS gov_work_details,
                                     CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                     CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                     CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                     CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                     CASE WHEN char_residence.need_repair is null THEN ' '
                                          WHEN char_residence.need_repair = 1 THEN '$yes'
                                          WHEN char_residence.need_repair = 2 THEN '$no'
                                        END AS need_repair ,
                                     CASE WHEN char_residence.repair_notes is null   THEN ' ' Else char_residence.repair_notes  END  AS repair_notes
                                    ");
                if($category_type == 2){
                    $result->selectRaw("CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' $citizen '
                                           WHEN char_persons.refugee = 2 THEN '$refugee'
                                     END
                                     AS refugee,
                                     CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' '
                                           WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                     END
                                     AS unrwa_card_number,
                                     CASE
                                        WHEN char_reconstructions.promised is null THEN ' '
                                        WHEN char_reconstructions.promised = 1 THEN '$no'
                                        WHEN char_reconstructions.promised = 2 THEN '$yes'
                                    END
                                    AS promised,
                                    CASE
                                       WHEN char_reconstructions.promised = 1 THEN ' '
                                       WHEN char_reconstructions.promised is null THEN ' '
                                       WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name
                                   END
                                   AS ReconstructionOrg,
                                   char_cases.rank,
                                   CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                   CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                   CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS notes,
                                   CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                   CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                   CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card,
                                   CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE
                                                          WHEN char_persons_health.has_health_insurance is null THEN ' '
                                                          WHEN char_persons_health.has_health_insurance = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_health_insurance = 0 THEN '$no'
                                                     END
                                                     AS has_health_insurance,
                                                     CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' Else char_persons_health.health_insurance_number END   AS health_insurance_number,
                                                     CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' Else char_persons_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_persons_health.has_device is null THEN ' '
                                                          WHEN char_persons_health.has_device = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                                     CASE WHEN char_persons_health.used_device_name is null THEN ' ' Else char_persons_health.used_device_name END   AS used_device_name,
                                                     CASE WHEN char_persons_health.gov_health_details is null THEN ' ' Else char_persons_health.gov_health_details END   AS gov_health_details
                                                     
                                   ");
                }
                else if($category_type ==1){
                    $result->selectRaw("
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE
                                                          WHEN char_persons_health.has_health_insurance is null THEN ' '
                                                          WHEN char_persons_health.has_health_insurance = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_health_insurance = 0 THEN '$no'
                                                     END
                                                     AS has_health_insurance,
                                                     CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' Else char_persons_health.health_insurance_number END   AS health_insurance_number,
                                                     CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' Else char_persons_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_persons_health.has_device is null THEN ' '
                                                          WHEN char_persons_health.has_device = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                                     CASE WHEN char_persons_health.used_device_name is null THEN ' ' Else char_persons_health.used_device_name END   AS used_device_name,
                                                     CASE WHEN char_persons_health.gov_health_details is null THEN ' ' Else char_persons_health.gov_health_details END   AS gov_health_details,
                                                     CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS study_type,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS type,
                                                      CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                                   Else ' '

                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                    CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                         WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                         WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                         WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                   END AS prayer,
                                                   CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                                        WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                        Else '$yes'
                                                    END AS save_quran,
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,
                                                     CASE WHEN f.death_date is null THEN ' '  Else father_death_causes.name END AS father_death_cause,
                                                     CASE
                                                            WHEN father_health.condition is null THEN ' '
                                                            WHEN father_health.condition = 1 THEN '$perfect'
                                                            WHEN father_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN father_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS father_health_status,
                                                     CASE WHEN father_health.details is null THEN ' ' Else father_health.details END   AS father_details,
                                                     CASE WHEN father_diseases.name is null THEN ' ' Else father_diseases.name END   AS father_disease_id,
                                                     CASE WHEN father_education.specialization is null THEN ' '  Else father_education.specialization END  AS father_Specialization,
                                                     CASE WHEN father_edu_degrees.name is null THEN ' ' Else father_edu_degrees.name END as father_degree,
                                                     CASE
                                                          WHEN father_work.working is null THEN ' '
                                                          WHEN father_work.working = 1 THEN '$yes'
                                                          WHEN father_work.working = 2 THEN '$no'
                                                       END
                                                     AS father_working,
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work_jobs.name
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_type,
                                                     CASE
                                                       WHEN father_work.working is null THEN ' '
                                                       WHEN father_work.working = 1 THEN f.monthly_income
                                                       WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_monthly_income,
                                                    
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work.work_location
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_location,
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN m.death_date is null THEN ' '  Else father_death_causes.name END AS mother_death_cause,
                                                      CASE
                                                            WHEN mother_health.condition is null THEN ' '
                                                            WHEN mother_health.condition = 1 THEN '$perfect'
                                                            WHEN mother_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN mother_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS mother_health_status,
                                                   CASE WHEN mother_health.details is null THEN ' ' Else mother_health.details END   AS mother_details,
                                                   CASE WHEN mother_diseases.name is null THEN ' ' Else mother_diseases.name END   AS mother_disease_id,
                                                   CASE WHEN mother_education.specialization is null THEN ' '  Else mother_education.specialization END  AS mother_Specialization,
                                                     CASE WHEN mother_edu_stages.name is null THEN ' ' Else mother_edu_stages.name END as mother_stage,
                                                      CASE
                                                          WHEN mother_work.working is null THEN ' '
                                                          WHEN mother_work.working = 1 THEN '$yes'
                                                          WHEN mother_work.working = 2 THEN '$no'
                                                       END
                                                     AS mother_working,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work_jobs.name
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_type,
                                                     CASE
                                                       WHEN mother_work.working is null THEN ' '
                                                       WHEN mother_work.working = 1 THEN m.monthly_income
                                                       WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_monthly_income,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work.work_location
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_location,
                                                     
                                                     CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   AS guardian_phone,
                                                     CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                                     CASE WHEN guardian_cont4.contact_value is null THEN ' ' Else guardian_cont4.contact_value END   AS guardian_wataniya,
                                                     CASE WHEN guardian_property_types.name is null THEN ' ' Else guardian_property_types.name END   AS guardian_property_type,
                                                     CASE WHEN guardian_roof_materials.name is null THEN ' ' Else guardian_roof_materials.name END   AS guardian_roof_material,
                                                     CASE WHEN guardian_residence.area is null THEN ' ' Else guardian_residence.area  END  AS guardian_area,
                                                     CASE WHEN guardian_residence.rooms is null THEN ' ' Else guardian_residence.rooms  END  AS guardian_rooms,
                                                      CASE WHEN guardian_furniture_status_i18n.name is null THEN ' ' Else guardian_furniture_status_i18n.name  END  AS guardian_indoor_condition,
                                                         CASE WHEN guardian_house_status_i18n.name is null     THEN ' ' Else guardian_house_status_i18n.name  END  AS guardian_residence_condition
                                                    ");
                }

//            CASE
//                WHEN g.Location_id is null THEN ' '
//                                                          Else CONCAT(guardian_L4.name, ' _ ',guardian_L3.name, ' _ ',guardian_L2.name, ' _ ', guardian_L1.name, ' _ ', g.street_address)
//                                                          END
//                                                     AS guardian_address,
//                ->offset(10)->limit(5)


                $result=$result->get();
                
                $elements = []; $properties = []; $essentials = []; $aid_source = [];

                if($filters['action'] =='ExportToExcel' || $filters['action'] =='csv'){
                    if($category_type == 1){
                        $setting_id = 'aid_custom_form';
                    }else{
                        $setting_id = 'aid_custom_form';
                    }
                    $user = \Auth::user();
                    $form =Setting::where(['id' =>$setting_id, 'organization_id'=> $user->organization_id])->first();

                    $getCustom =false;
                    if($form) {
                        $ActiveForm = CustomForms::where(function ($q) use ($form) {
                            $q->where('id', '=', $form->value);
                            $q->where('status', '=', 1);
                        })->first();

                        if($ActiveForm){
                            $getCustom =true;
                        }
                    }
                    $elements = [];
                    if($getCustom){

                        foreach($result as $item) {
                            $CustomData=self::getCustomData($item->case_id);
                            $item->elements=$CustomData['elements'];
                            $item->data=$CustomData['data'];
                        }

                        $elements_query = \DB::table('char_forms_elements')
                            ->join('char_forms', function($q){
                                $q->on('char_forms.id', '=', 'char_forms_elements.form_id')
                                    ->whereNull('char_forms.deleted_at')
                                    ->where('char_forms.type','=',2)
                                ;
                            })
                            ->selectRaw('char_forms_elements.id,char_forms_elements.label');

                        if($all_organization ==0){
                            $organizations=[];
                            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null &&
                                $filters['organization_ids'] !="" && $filters['organization_ids'] !=[]) {

                                if(is_array($filters['organization_ids'])){
                                    if(sizeof($filters['organization_ids']) != 0){
                                        if($filters['organization_ids'][0]==""){
                                            unset($filters['organization_ids'][0]);
                                        }
                                        $organizations =$filters['organization_ids'];
                                    }
                                }
                            }

                            if(!empty($organizations)){
                                $elements_query->wherein('char_forms.organization_id',$organizations);
                            }else{

                                if($UserType == 2) {
                                    $elements_query->where(function ($anq) use ($organization_id,$user) {
                                        $anq->where('char_forms.organization_id',$organization_id);
                                        $anq->orwherein('char_forms.organization_id', function($quer) use($user) {
                                            $quer->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });

                                }else{

                                    $elements_query->where(function ($anq) use ($organization_id) {
                                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                        $anq->wherein('char_forms.organization_id', function($decq) use($organization_id) {
                                            $decq->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    });


                                }
                            }

                        }
                        elseif($all_organization ==1){
                            if(isset($filters['organizationsCases']) && !empty($filters['organizationsCases']) && $filters['organizationsCases'])
                                $elements_query->where('char_forms.organization_id','!=',$organization_id);
                            else
                                $elements_query->where('char_forms.organization_id',$organization_id);
                        }
                        $elements = $elements_query->get();

                    }

                    $financial_aid_source = [];
                    $aid_source = [];
                    $properties = [];

                    if($category_type != 1){

                        $aid_source= \DB::table('char_aid_sources')
                            ->selectRaw(" char_aid_sources.name, char_aid_sources.id")
                            ->orderBy('char_aid_sources.name')->get();

                        $properties = \DB::table('char_properties_i18n')
                            ->selectRaw(" char_properties_i18n.name, char_properties_i18n.property_id as id")
                            ->where('char_properties_i18n.language_id', $language_id)
                            ->orderBy('char_properties_i18n.name')->get();

                        $essentials = \DB::table('char_essentials')
                            ->selectRaw(" char_essentials.id as id,char_essentials.name")
                            ->orderBy('char_essentials.name')->get();

                        foreach($result as $item) {

                            $financial_aid_source_= \DB::table('char_persons_aids')
                                ->where('char_persons_aids.aid_type', '=', 1)
                                ->where('char_persons_aids.person_id', '=', $item->person_id)
                                ->selectRaw("aid_source_id ,CASE WHEN char_persons_aids.aid_value is null THEN '0' 
                                                                  WHEN char_persons_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_persons_aids.aid_value is null THEN '0'  
                                                                                     Else char_persons_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                            $data_ = [];
                            $item->financial_aid_source_elements = [];
                            foreach($financial_aid_source_ as $kk=>$i) {
                                if(!isset($data_[$i->aid_source_id])){
                                    $item->financial_aid_source_elements[]=$i->aid_source_id;
                                    $data_[$i->aid_source_id]=$i->aid_value;
                                }
                            }
                            $item->financial_aid_source = $data_;

                            $non_financial_aid_source_= \DB::table('char_persons_aids')
                                ->where('char_persons_aids.aid_type', '=', 2)
                                ->where('char_persons_aids.person_id', '=', $item->person_id)
                                ->selectRaw("aid_source_id ,CASE WHEN char_persons_aids.aid_value is null THEN '0' 
                                                                  WHEN char_persons_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_persons_aids.aid_value is null THEN '0'  
                                                                                     Else char_persons_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                            $data_ = [];
                            $item->non_financial_aid_source_elements = [];
                            foreach($non_financial_aid_source_ as $kk=>$i) {
                                if(!isset($data_[$i->aid_source_id])){
                                    $item->non_financial_aid_source_elements[]=$i->aid_source_id;
                                    $data_[$i->aid_source_id]=$i->aid_value;
                                }
                            }
                            $item->non_financial_aid_source = $data_;

                            $persons_properties= \DB::table('char_persons_properties')
                                ->where('char_persons_properties.person_id', '=', $item->person_id)
                                ->selectRaw("char_persons_properties.property_id as property_id ,
                                                             CASE WHEN char_persons_properties.has_property is null THEN '0' 
                                                                  WHEN char_persons_properties.has_property = 0 THEN 0  
                                                                  WHEN char_persons_properties.quantity is null THEN 0  
                                                                  Else char_persons_properties.quantity END
                                                             AS  quantity")
                                ->get();

                            $data_=[];
                            $item->persons_properties_elements = [];
                            foreach($persons_properties as $kk=>$i) {
                                if(!isset($data_[$i->property_id])){
                                    $item->persons_properties_elements[]=$i->property_id;
                                    $data_[$i->property_id]=$i->quantity;
                                }
                            }

                            $item->persons_properties = $data_;

                            $persons_essentials= \DB::table('char_cases_essentials')
                                ->where('char_cases_essentials.case_id', '=', $item->case_id)
                                ->selectRaw("char_cases_essentials.essential_id ,
                                          char_cases_essentials.needs as needs")
                                ->get();

                            $data_=[];
                            $item->persons_essentials_elements = [];
                            foreach($persons_essentials as $kk=>$i) {
                                if(!isset($data_[$i->essential_id])){
                                    $item->persons_essentials_elements[]=$i->essential_id;
                                    $data_[$i->essential_id]=$i->needs;
                                }
                            }

                            $item->persons_essentials = $data_;

                        }

                    }

                }


                $output = array('statistic'=>$result, 'elements'=>$elements,
                    'essentials'=>$essentials ,'properties'=>$properties , 'aid_source'=>$aid_source);
            }
        }
        else {
            $result->selectRaw("
                               CASE WHEN char_cases.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                               CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                              CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                              CASE WHEN char_persons.gender is null THEN ' '
                                   WHEN char_persons.gender = 1 THEN '$male'
                                   WHEN char_persons.gender = 2 THEN '$female'
                              END AS case_gender,char_persons.gender,
                             CASE WHEN  char_persons.birthday is null THEN '-' Else  DATE_FORMAT(char_persons.birthday,'%Y/%m/%d') END AS birthday,
                              CASE WHEN  char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date_,
                              char_persons.death_date,  
                               char_persons.death_date ,  char_cases.id as case_id,
                              char_cases.category_id ,
                              CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                              char_cases.person_id ,
                              char_cases.organization_id,org.name as organization_name,
                              CASE WHEN  char_persons.birthday is null THEN ' ' Else 
                               EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age");
            if(isset($filters['deleted']) && $filters['deleted'] !=null && $filters['deleted'] !="" && $filters['deleted']==true) {
                $result->selectRaw("char_cases.reason as delete_reason,char_cases.deleted_at");
            }else{
                $result->selectRaw("char_cases.visitor,
                                         CASE WHEN char_cases.status = 1 THEN '$inactive'WHEN char_cases.status = 0 THEN '$active' END AS cases_status,
                                         char_cases.status,
                                         char_cases.rank,
                                         CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status
                                       ");

                if($category_type == 1) {
                    $result->selectRaw("
char_categories.father_dead,char_guardians.guardian_id,char_persons.mother_id,char_persons.father_id,
                                            CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
      CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,null,char_persons.id )
           WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,null,char_persons.id )
           WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,char_persons.id )
     END AS family_count,
                                            CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,1,char_persons.id )
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,1,char_persons.id )
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,1,char_persons.id )
                                            END AS male_count,
                                            CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,2,char_persons.id )
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,2,char_persons.id )
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,2,char_persons.id )
                                            END AS female_count,
                                            CASE WHEN (char_categories.case_is = 1 and char_categories.family_structure= 2) THEN char_persons.father_id
                                                 WHEN (char_categories.case_is = 1 and char_categories.family_structure=1) THEN char_persons.id
                                                 WHEN (char_categories.case_is = 2 and char_categories.family_structure=1) THEN g.id
                                            END AS l_person_id,
                                            CASE WHEN char_get_person_total_payments ( char_persons.id ,'$first', '$last') is null THEN 0
                                                Else char_get_person_total_payments ( char_persons.id ,'$first', '$last')
                                            END AS total_amount,
                                            CASE WHEN char_get_person_average_payments ( char_persons.id ,'$first', '$last') is null THEN 0
                                                  Else char_get_person_average_payments ( char_persons.id ,'$first', '$last')
                                            END AS amount,
                                            char_get_sponsorship_count ( char_cases.id ,3 ,char_cases.organization_id)  AS guarantee_count,
                                            char_get_sponsorship_count ( char_cases.id ,1 ,char_cases.organization_id) AS candidate_count
                                           ") ;
                }
                else{
                    $result->selectRaw("  char_persons.family_cnt AS family_count,
                                          char_persons.male_live AS male_count,
                                          char_persons.female_live AS female_count,
                                         CASE WHEN char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') is null THEN 0
                                                 Else char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')
                                             END AS voucher_count
                                         ");
                }



                if($category_type == 2){

                    $result->selectRaw("
                                        CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                        CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        char_persons.adscountry_id ,char_persons.adsdistrict_id ,char_persons.adsregion_id ,
                                        char_persons.adsneighborhood_id ,char_persons.adssquare_id ,char_persons.adsmosques_id 
                                 
                                       ");
                }else{
                    $result->selectRaw("
                                        CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name
                                      ");
                }
            }

            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
            $output=$result->paginate($records_per_page);
        }

        return $output;
    }

    public static function getCustomData($id)
    {

        $data=[];
        $elements=[];

        $forms_cases_data=
            \DB::table('char_forms_cases_data')
                ->leftjoin('char_forms_elements_options', function($q){
                    $q->on('char_forms_elements_options.id', '=', 'char_forms_cases_data.option_id');
                })
                ->where('char_forms_cases_data.case_id',$id)
                ->selectRaw("char_forms_cases_data.element_id,char_forms_cases_data.case_id,
                             CASE WHEN char_forms_cases_data.option_id is null THEN char_forms_cases_data.value 
                                  Else char_forms_elements_options.label  END  AS value
                                    ")
                ->get();

        foreach($forms_cases_data as $i) {
            if(!isset($data[$i->element_id])){
                $data[$i->element_id]=$i->value;
                if(!in_array($i->element_id,$elements)){
                    array_push($elements,$i->element_id);
                }
            }
        }

        return ['data' => $data ,'elements' => $elements];
    }

    public static function relatedDocuments($filters)
    {
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $char_cases = ['category_id']; //        'organization_id'
        $condition = [];

        $category_type = $filters['category_type'];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ($value != "") {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_cases)) {

                if ($value != "") {
                    $data = ['char_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $query = self::query()
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_categories_document_types', 'char_cases.category_id', '=', 'char_categories_document_types.category_id')
            ->join('char_categories', function($join)use($category_type) {
                $join->on( 'char_cases.category_id', '=', 'char_categories.id')
                    ->where('char_categories.type', $category_type);
            })->addSelect([
                'char_cases.id',
                'char_cases.person_id',
                'char_cases.category_id',
                'char_cases.organization_id',
                'char_categories_document_types.scope',
                'char_categories_document_types.document_type_id',
            ]);


        if ($filters['person_name'] || $filters['father_id'] || $filters['mother_id']) {
            $query->selectRaw("char_persons.father_id,char_persons.mother_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                 CONCAT(ifnull(char_persons.first_name ,' '), ' ' ,  ifnull(char_persons.second_name,' '),' ',
                                                     ifnull(char_persons.third_name ,' '),' ',  ifnull(char_persons.last_name  ,' ')) AS person_name
                  ");

        }

        if ($filters['sponsor_id'] || $filters['sponsor_status']) {
            $query->join('char_sponsorship_cases', 'char_cases.id', '=', 'char_sponsorship_cases.case_id');
            if ($filters['sponsor_id']) {
                $query->where('char_sponsorship_cases.sponsor_id', '=', $filters['sponsor_id']);
            }
            if ($filters['sponsor_status']) {
                $query->where('char_sponsorship_cases.status', '=', $filters['sponsor_status']);
            }
        }
        if ($filters['document_type_name']) {
            $query->join('char_document_types_i18n', function ($join) use($filters) {
                $join->on('char_categories_document_types.document_type_id', '=', 'char_document_types_i18n.document_type_id')
                    ->where('char_document_types_i18n.language_id', '=', $filters['language_id']);
            })->addSelect([
                'char_document_types_i18n.name AS document_type_name'
            ]);
        }
        if ($filters['guardian_id']) {
            $query->leftJoin('char_guardians', function($join) {
                $join->on('char_cases.person_id', '=', 'char_guardians.individual_id')
                    ->on('char_cases.organization_id', '=', 'char_guardians.organization_id')
                    ->where('char_guardians.status', '=',1);
            })->addSelect('char_guardians.guardian_id');
        }

        $query=$query ->where(function ($sub) use($filters,$condition){
            $sub->whereNull('char_cases.deleted_at');

            if($filters['all_organization'] === false || $filters['all_organization'] === 'false'){
                $sub->where('char_cases.organization_id', $filters['organization_id']);
            }
            else{

                if(isset($filters['organizations']) && $filters['organizations'] !=null && $filters['organizations'] !="" && $filters['organizations'] !=[] &&
                    sizeof($filters['organizations'] )!= 0) {
                    if($filters['organizations'][0]==""){
                        unset($filters['organizations'][0]);
                    }
                }
                if(sizeof($filters['organizations']) >0 ){
                    $sub->wherein('char_cases.organization_id',$filters['organizations']);
                }
                else{

                    $user = \Auth::user();
                    $UserType=$user->type;
                    $organization_id=$user->organization_id;

                    if($UserType == 2) {
                        $sub->where(function ($anq) use ($organization_id,$user) {
                            $anq->where('char_cases.organization_id',$organization_id);
                            $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                                $quer->select('organization_id')
                                    ->from('char_user_organizations')
                                    ->where('user_id', '=', $user->id);
                            });
                        });
                    }else{

                        $sub->where(function ($anq) use ($organization_id) {
                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                            $anq->wherein('char_cases.organization_id', function($query) use($organization_id) {
                                $query->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                        });
                    }
                }
            }

            if(isset($filters['items'])){
               if(sizeof($filters['items']) > 0 ){
                $sub->whereIn('char_cases.id',$filters['items']);  
              }      
            } 
        });
        if (count($condition) != 0) {
            $query->where(function ($q) use ($filters,$condition) {
                $names = ['char_persons.last_name', 'char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }

            });
        }

        $cases = [];
        $entries = $query->get();
        foreach ($entries as $entry) {
            if (!isset($cases[$entry->person_id])) {
                $cases[$entry->person_id] = array(
                    'person_id' => $entry->person_id,
                    'case_id' => $entry->id,
                    'name' => $filters['person_name']? $entry->person_name : '',
                    'documents' => [],
                );
            }
            if ($entry->scope == 1) {
                if (!$entry->father_id) {
                    continue;
                }
                $target = $entry->father_id;
            } else if ($entry->scope == 2) {
                if (!$entry->mother_id) {
                    continue;
                }
                $target = $entry->mother_id;
            } else if ($entry->scope == 3 || $entry->scope == 0) {
                $target = $entry->person_id;
            } else if ($entry->scope == 4) {
                if (!$entry->guardian_id) {
                    continue;
                }
                $target = $entry->guardian_id;
            }

            $cases[$entry->person_id]['documents'][$entry->document_type_id] = [
                'id' => $entry->document_type_id,
                'name' => $filters['document_type_name']? $entry->document_type_name : '',
                'target' => $target,
            ];
        }

        return $cases;

    }

    public static function FileMap($ype,$constraint,$keys){

        $map=array('mother'=>[],'father'=>[],'guardian'=>[],'case'=>[]);
        $translate = [
            "noaa_albtak"=>'card_type',
            "mhjor"=>'deserted',
            "rkm_alhoy" =>'id_card_number',
            "alasm_alaol_aarby"=>'first_name',
            "alasm_althany_aarby"=>'second_name',
            "alasm_althalth_aarby"=>'third_name',
            "alaaael_aarby"=>'last_name',
            "asm_alaaael_alsabk"=>'prev_family_name',
            "alasm_alaol_anjlyzy"=>'en_first_name',
            "alasm_althany_anjlyzy"=>'en_second_name',
            "alasm_althalth_anjlyzy"=>'en_third_name',
            "alaaael_anjlyzy"=>'en_last_name',
            "tarykh_almylad"=>'birthday',
            "mkan_almylad"=>'birth_place',
            "aljns"=>'gender',
            "aljnsy"=>'nationality',
            "aadd_alzojat"=>'spouses',
            "alhal_alajtmaaay"=>'marital_status_id',
            "hal_almoatn"=>'refugee',
            "rkm_krt_altmoyn"=>'unrwa_card_number',
            "tfasyl_alaanoan"=>'street_address',
            "rkm_joal_asasy"=>'primary_mobile',
            "rkm_joal_ahtyaty"=>'secondary_mobile',
            "rkm_alotny"=>'wataniya',
            "rkm_hatf"=>'phone',
            "asm_albnk"=>'bank_id',
            "alfraa"=>'branch_name',
            "asm_sahb_alhsab"=>'account_owner',
            "rkm_alhsab"=>'account_number',
            "hl_yaaml"=>'working',
            "hl_taaml"=>'working',
            "kadr_aal_alaaml"=>'can_work',
            "sbb_aadm_alkdr"=>'work_reason_id',
            "hal_alaaml"=>'work_status_id',
            "fe_alajr"=>'work_wage_id',
            "alothyf"=>'work_job_id',
            "mkan_alaaml"=>'work_location',
            "aldkhl_alshhry_shykl"=>'monthly_income',

            "aadd_alghrf"=>'rooms',
            "almsah_balmtr"=>'area',
            "kym_alayjar"=>'rent_value',
            "noaa_almlky"=>'property_type_id',
            "mlky_almnzl"=>'property_type_id',
            "noaa_skf_almnzl"=>'roof_material_id',
            "skf_almnzl"=>'roof_material_id',

            "hal_almskn"=>'residence_condition',
            "hal_alathath"=>'indoor_condition',
            "kably_almnzl_llskn"=>'habitable',

            "odaa_almnzl"=>'house_condition',

            "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
            "asm_almoess_alty_oaadt_balbna"=>'organization_name',
            "ahtyajat_alaaael_bshd"=>'needs',
            "tosyat_albahth_alajtmaaay"=>'notes',
            "ydrs_ao_mtaalm"=>"study" ,
            "noaa_aldras"=>"type" ,
            "jh_aldras"=>"authority" ,
            "almoehl_alaalmy"=>"stage" ,
            "almrhl_aldrasy"=>"degree" ,
            "altkhss"=>"specialization" ,
            "alsf"=>"grade" ,
            "alsn_aldarsy"=>"year" ,
            "asm_almdrs"=>"school" ,
            "akhr_ntyj" =>"points" ,
            "almsto_althsyl"=>"level",
            "alhal_alshy"=>"condition",
            "almrd_ao_aleaaak"=>"disease_id",
            "tfasyl_alhal_alshy"=>"details",
            "tarykh_alzyar" => "visited_at",
            "asm_albahth_alajtmaaay" => "visitor",
            "hl_ysly"=>"prayer",
            "sbb_trk_alsla"=>"prayer_reason",
            "hl_yhfth_alkran"=>"save_quran",
            "aadd_alajza"=>"quran_parts",
            "aadd_alsor"=>"quran_chapters",
            "sbb_trk_althfyth"=>"quran_reason",
            "mlthk_balmsjd"=>"quran_center",
            "tarykh_ofa_alab"=>"quran_center",
            "tarykh_alofa"=>"death_date",
            "sbb_alofa"=>"death_cause_id",
            "sl_alkrab"=>"kinhip_id",
            "hl_alam_maayl"=>"mother_is_guardian",
            "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
            "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
            "hl_bhaj_ltrmym_llmoaem" => "need_repair",
            "osf_odaa_almnzl" => "repair_notes",
            "hl_ldyh_tamyn_shy" => "has_health_insurance",
            "noaa_altamyn_alshy" => "health_insurance_type",
            "rkm_altamyn_alshy" => "health_insurance_number",
            "hl_ystkhdm_jhaz" => "has_device",
            "asm_aljhaz_almstkhdm" => "used_device_name",
            "almrd_ao_alaaaak" => "disease_id",
            "rkm_hoy_albahth" => "visitor_card",
            "ray_albahth_alajtmaaay" => "visitor_opinion",
            "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",
            "hl_ldyh_thmm_maly"=> "receivables",
            "kym_althmm_balshykl"=> "receivables_sk_amount",
        ];

        if($ype == 1){
            $translate["aldol"]='country';
            $translate["almhafth"]='governarate';
            $translate["almdyn"]='city';
            $translate[ "almntk"]='location_id';
            $translate["almsjd"]='mosques_id';

        }else{
            $translate["aldol"]= "adscountry_id";
            $translate["almhafth"]= "adsdistrict_id";
            $translate["almntk"]= "adsregion_id";
            $translate["alhy"]= "adsneighborhood_id";
            $translate["almrbaa"]="adssquare_id";
            $translate["almsjd"]= "adsmosques_id";
        }
        foreach ($constraint as $item) {
            if(in_array($item,$keys)){
                if($ype == 1){
                    if($item == 'hl_alam_maayl'){
                        $map['mother'][$item]=$translate[$item];
                    }
                    elseif(substr( $item, -5 ) === "_alam"){
                        $map['mother'][$item]=$translate[substr($item, 0, -5)];
                    }
                    elseif(substr( $item,  -5 ) === "_alab"){
                        $map['father'][$item]=$translate[substr($item, 0, -5)];
                    }
                    elseif(substr( $item, -8 ) === "_almaayl"){
                        $map['guardian'][$item]=$translate[substr($item, 0, -8)];
                    }
                    else{
                        $map['case'][$item]=$translate[$item];
                    }
                }
                else{
                    $map['case'][$item]=$translate[$item];
                }
            }
        }

        return $map;

    }

    public static function constantId($name,$value,$isChild,$parent){

        if(!is_null($value) && $value != ''){
            switch ($name) {
                case 'payment_category'  : {
                    $object= \Setting\Model\PaymentCategoryI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->payment_category_id;
                }
                    break;
                case 'birth_place'  : {
                    $object= \Setting\Model\LocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'nationality'  :{
                    $object= \Setting\Model\LocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'marital_status_id'  : {
                    $object= \Setting\Model\MaritalStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->marital_status_id;
                }
                    break;
                case 'country'  : {
                    $object= \Setting\Model\LocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'governarate'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\LocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'city'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\LocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'mosques_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\LocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'location_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\LocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'bank_id'  : {
                    $object= \Setting\Model\Bank::where(['name'=>$value ])->first();
                    return is_null($object)? null : (int) $object->id;
                }
                    break;
                case 'branch_name'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\Branch::getBranchId($parent,$name);
                    }
                    return null;
                }
                    break;
                case 'work_reason_id'  : {
                    $object= \Setting\Model\Work\ReasonI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_reason_id;
                }
                    break;
                case 'work_status_id'  :{
                    $object= \Setting\Model\Work\StatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_status_id;
                }
                    break;
                case 'work_wage_id'  : {
                    $object= \Setting\Model\Work\Wage::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->work_wage_id;
                }
                    break;
                case 'work_job_id'  : {
                    $object= \Setting\Model\Work\JobI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_job_id;
                }
                    break;
                case 'death_cause_id'  : {
                    $object= \Setting\Model\DeathCauseI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->death_cause_id;
                }
                    break;
                case 'property_type_id'  : {
                    $object= \Setting\Model\PropertyTypeI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->property_type_id;
                }
                    break;
                case 'roof_material_id'  : {
                    $object= \Setting\Model\RoofMaterialI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->roof_material_id;
                }
                    break;
                case 'residence_condition'  : {
                    $object= \Setting\Model\HouseStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->house_status_id;
                }
                    break;
                case 'indoor_condition'  : {
                    $object= \Setting\Model\FurnitureStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->furniture_status_id;
                } break;
                case 'habitable'  : {
                    $object= \Setting\Model\HabitableStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->habitable_status_id;
                } break;
                case 'house_condition'  : {
                    $object= \Setting\Model\BuildingStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->building_status_id;
                } break;
                case 'disease_id'  : {
                    $object= \Setting\Model\DiseaseI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->disease_id;
                }break;
                case 'authority'  : {
                    $object= \Setting\Model\Education\AuthorityI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_authority_id;
                }break;
                case 'stage'  : {
                    $object= \Setting\Model\Education\StageI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_stage_id;
                }break;
                case 'degree'  : {
                    $object= \Setting\Model\Education\DegreeI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_degree_id;
                }break;
                case 'kinship_id'  : {
                    $object= \Setting\Model\KinshipI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->kinship_id;
                }break;
                case 'adscountry_id'  : {
                    $object= \Setting\Model\aidsLocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'adsdistrict_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'adsregion_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'adsneighborhood_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'adssquare_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'adsmosques_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
            }
        }
        return null;

    }

    public static function reMapInputs($inputs){

        $outputs=array('mother'=>[],'father'=>[],'guardian'=>[],'case'=>[]);

        foreach ($inputs as $k=>$v) {
            if(!is_null($inputs[$k]) && $inputs[$k] != " "){
                if($k == 'hl_alam_maayl'){
                    $outputs['mother'][$k] = $v;
                }
                elseif($k == 'sl_alkrab'){
                    $outputs['guardian'][$k] = $v;
                }
                elseif (substr($k, -5) === "_alam") {
                    $outputs['mother'][$k] = $v;
                } elseif (substr($k, -5) === "_alab") {
                    $outputs['father'][$k] = $v;
                } elseif (substr($k, -9) === "_rb_alasr") {
                    $outputs['father'][$k] = $v;
                } elseif (substr($k, -8) === "_almaayl") {
                    $outputs['guardian'][$k]= $v;
                }else{
                    $outputs['case'][$k] = $v;
                }
            }
        }
        return $outputs;
    }

    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    public static function filterInputs($is_case,$category_id,$category_type,$data){
        $user = \Auth::user();
        $dates = ['birthday','visited_at','death_date'];
        $constant =  ['birth_place', 'nationality', 'marital_status_id','death_cause_id',
            'country', 'governarate', 'city', 'location_id','kinship_id','mosques_id',
            'bank_id', 'branch_name','disease_id','authority', 'stage','degree',
            'work_reason_id', 'work_status_id', 'work_wage_id', 'work_job_id',
            'property_type_id', 'roof_material_id',
            'residence_condition', 'indoor_condition', 'habitable', 'house_condition',"adscountry_id",
            "adsdistrict_id", "adsmosques_id", "adsneighborhood_id", "adsregion_id", "adssquare_id"];
        $hasParent =  ['governarate', 'city', 'location_id', 'branch_name','mosques_id',"adsdistrict_id", "adsmosques_id", "adsneighborhood_id", "adsregion_id", "adssquare_id"];
        $Parents = ['governarate'=>'country',
            'city' => 'governarate',
            'location_id' => 'city',
            'mosques_id' => 'location_id',
            'branch_name' => 'bank_id' ,
            "adsdistrict_id" =>'adscountry_id',
            "adsregion_id" =>'adsdistrict_id',
            "adsneighborhood_id" =>'adsregion_id',
            "adssquare_id" =>'adsneighborhood_id',
            "adsmosques_id" =>'adssquare_id'];

        $ParentsNames = ['governarate'=>'aldol',
            'city' => 'almhafth',
            'location_id' => 'almdyn',
            'mosques_id' => 'almntk', 'branch_name' => 'asm_albnk',
            "adsdistrict_id" =>'aldol',
            "adsregion_id" =>'almhafth',
            "adsneighborhood_id" =>'almntk',
            "adssquare_id" =>'alhy',
            "adsmosques_id" =>'almrbaa'];

        $numeric =  ['id_card_number', 'spouses', 'unrwa_card_number',
            'primary_mobile', 'wataniya', 'secondary_mobile', 'phone',
            'monthly_income', 'rooms','area','rent_value',"points" ,"quran_parts","quran_chapters",
            "family_cnt","female_live","male_live","spouses",
        ];

        if($is_case){
//            'case_id'=> true,
            $row=['category_type'=> $category_type, 'category_id'=> $category_id,
                'user_id'=> $user->id,'mode'=> 'new', 'target'=>'case', 'organization_id'=> $user->organization_id];
            $translator =self::translator($category_type);
        }else{
            $row=[];
            $translator =self::$_translator;

        }
        $row['errors']=[];

        foreach ($data as $k=>$v) {
            $key=null;
            if($is_case){
                if(isset($translator[$k])){
                    $key=$translator[$k];
                }
            }
            else{
                if($k == 'sl_alkrab'){
                    $key=$translator[$k];
                }
                elseif($k == 'hl_alam_maayl'){
                    $key=$translator[$k];
                }
                elseif(substr( $k, -5 ) === "_alam"){
                    $key=$translator[substr($k, 0, -5)];
                }
                elseif(substr( $k,  -5 ) === "_alab"){
                    $key=$translator[substr($k, 0, -5)];
                }
                elseif(substr( $k, -8 ) === "_almaayl"){
                    $key=$translator[substr($k, 0, -8)];
                }
                elseif(substr( $k, -8 ) === "_almaayl"){
                    $key=$translator[substr($k, 0, -8)];
                }
                else{
                    $key=$translator[$k];
                }
            }

            if(!is_null($key)){

                if(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(!is_numeric($v)){
                            $row['errors'][$key]=trans('common::application.invalid_number');
                        }
                    }
                }

                if(in_array($key,$dates)){
                    if(!is_null($v) && $v != " "){
                        $date_ = Helpers::getFormatedDate($v);
                        if (!is_null($date_)) {
                            $row[$key]=$date_;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_date');
                        }
                    }
                }
                elseif($key =='gender'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'ذكر') ? 1 : 2;
                    }
                }elseif($key =='deserted'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_device'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_health_insurance'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='need_repair'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? '1' : '2';
                    }
                }elseif($key =='has_other_work_resources'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='card_type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        if ($vlu == 'بطاقة تعريف'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جواز سفر'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'تقني') ? 2 : 1;
                    }
                }
                elseif($key =='level'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'مقبول'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'ضعيف'){
                            $row[$key] = 5;
                        }
                    }
                }elseif($key =='visitor_evaluation'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'سيء'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'سيء جدا'){
                            $row[$key] = 0;
                        }
                    }
                }
                elseif($key =='grade'){
                    $vlu = trim($v);
                    if(!is_null($vlu) && $vlu != " "){
                        if($vlu == 'اول ابتدائي'){ $row[$key] = 1;}
                        elseif($vlu == 'ثاني ابتدائي'){$row[$key] = 2;}
                        elseif ($vlu == 'ثالث ابتدئي'){$row[$key] = 3;}
                        elseif ($vlu == 'رابع ابتدئي'){$row[$key] = 4;}
                        elseif ($vlu == 'خامس ابتدئي'){$row[$key] = 5;}
                        elseif ($vlu == 'سادس ابتدئي'){$row[$key] = 6;}
                        elseif ($vlu == 'السابع'){$row[$key] = 7;}
                        elseif ($vlu == 'الثامن'){$row[$key] = 8;}
                        elseif ($vlu == 'التاسع'){$row[$key] = 9;}
                        elseif ($vlu == 'العاشر'){$row[$key] = 10;}
                        elseif ($vlu == 'الحادي عشر'){$row[$key] = 11;}
                        elseif ($vlu == 'الثاني عشر'){$row[$key] = 12;}
                        elseif ($vlu == 'دراسات خاصة'){$row[$key] = 13;}
                        elseif ($vlu == 'تدريب مهنى'){$row[$key] = 14;}
                        elseif ($vlu == 'سنة أولى جامعة'){$row[$key] = 15;}
                        elseif ($vlu == 'سنة ثانية جامعة'){$row[$key] = 16;}
                        elseif ($vlu == 'سنة ثالثة جامعة'){$row[$key] = 17;}
                        elseif ($vlu == 'سنة رابعة جامعة'){$row[$key] = 18;}
                        elseif ($vlu == 'سنة خامسة جامعة'){$row[$key] = 19;}
                    }
                }
                elseif($key =='condition'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if ($vlu == 'مرض مزمن'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'ذوي احتياجات خاصة'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='refugee'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'مواطن') ? 1 : 2;
                    }
                }
                elseif($key =='working'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='mother_is_guardian'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='study'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='receivables'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='is_qualified'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }
                elseif($key =='can_work'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif($key =='promised'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif(in_array($key,$constant)){
                    $parent =null;
                    $isChild =false;
                    if(in_array($key,$hasParent)){
                        $isChild =true;
                        $parentKey=$Parents[$key];
                        $parentName=$ParentsNames[$key];
                        if(isset($row[$parentKey])){
                            $parent=$row[$parentKey];
                        }
//                                                else{
//                                                    $parent=$value[$parentName];
//                                                }
                    }
                    $vlu = $v;



                    if(!is_null($vlu) && $vlu != '') {
                        $cons_val = CaseModel::constantId($key,$vlu,$isChild,$parent);

                        if(!is_null($cons_val)) {
                            $row[$key] = $cons_val;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_constant');
                        }
                    }
                }
                elseif(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(is_numeric($v)){
                            $row[$key]=(int)$v;
                        }
                    }
                }else{
                    if(!is_null($v) && $v != " "){
                        $row[$key]=$v;
                    }
                }
            }


        }

        $row['outSource']=true;
        if(isset($row['bank_id']) && isset($row['account_number'])&& isset($row['branch_name'])&& isset($row['branch_name'])){
            $row['banks'][]=[
                'bank_id' => $row['bank_id'],
                'account_owner' => $row['account_owner'],
                'account_number' => $row['account_number'],
                'branch_name' => $row['branch_name'],
                'check' => true
            ];

        }

        return $row;
    }

    public static function hasPermission ($permission){

        $user = \Auth::user();
        if ($user->hasPermission($permission)) {
            return true;
        }
        return false;

    }

    public static function translator ($type){

        if($type == 1){
            return [
                "rkm_alhoy" =>'id_card_number',
                "noaa_albtak"=>'card_type',
                "mhjor"=>'deserted',
                "alasm_alaol_aarby"=>'first_name',
                "alasm_althany_aarby"=>'second_name',
                "alasm_althalth_aarby"=>'third_name',
                "alaaael_aarby"=>'last_name',
                "asm_alaaael_alsabk"=>'prev_family_name',
                "alasm_alaol_anjlyzy"=>'en_first_name',
                "alasm_althany_anjlyzy"=>'en_second_name',
                "alasm_althalth_anjlyzy"=>'en_third_name',
                "alaaael_anjlyzy"=>'en_last_name',
                "tarykh_almylad"=>'birthday',
                "mkan_almylad"=>'birth_place',
                "aljns"=>'gender',
                "aljnsy"=>'nationality',
                "aadd_alzojat"=>'spouses',
                "alhal_alajtmaaay"=>'marital_status_id',
                "hal_almoatn"=>'refugee',
                "rkm_krt_altmoyn"=>'unrwa_card_number',
                "aldol"=>'country',
                "almhafth"=>'governarate',
                "almdyn"=>'city',
                "almntk"=>'location_id',
                "almsjd"=>'mosques_id',
                "tfasyl_alaanoan"=>'street_address',
                "rkm_joal_asasy"=>'primary_mobile',
                "rkm_alotny"=>'wataniya',
                "rkm_joal_ahtyaty"=>'secondary_mobile',
                "rkm_hatf"=>'phone',
                "asm_albnk"=>'bank_id',
                "alfraa"=>'branch_name',
                "asm_sahb_alhsab"=>'account_owner',
                "rkm_alhsab"=>'account_number',
                "hl_yaaml"=>'working',
                "hl_taaml"=>'working',
                "kadr_aal_alaaml"=>'can_work',
                "sbb_aadm_alkdr"=>'work_reason_id',
                "hal_alaaml"=>'work_status_id',
                "fe_alajr"=>'work_wage_id',
                "alothyf"=>'work_job_id',
                "mkan_alaaml"=>'work_location',
                "aldkhl_alshhry_shykl"=>'monthly_income',


                "aadd_alghrf"=>'rooms',
                "almsah_balmtr"=>'area',
                "kym_alayjar"=>'rent_value',
                "noaa_almlky"=>'property_type_id',
                "mlky_almnzl"=>'property_type_id',
                "noaa_skf_almnzl"=>'roof_material_id',
                "skf_almnzl"=>'roof_material_id',

                "hal_almskn"=>'residence_condition',
                "hal_alathath"=>'indoor_condition',
                "kably_almnzl_llskn"=>'habitable',

                "odaa_almnzl"=>'house_condition',


                "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
                "asm_almoess_alty_oaadt_balbna"=>'organization_name',
                "ahtyajat_alaaael_bshd"=>'needs',
                "tosyat_albahth_alajtmaaay"=>'notes',
                "ydrs_ao_mtaalm"=>"study" ,
                "noaa_aldras"=>"type" ,
                "jh_aldras"=>"authority" ,
                "almoehl_alaalmy"=>"stage" ,
                "almrhl_aldrasy"=>"degree" ,
                "altkhss"=>"specialization" ,
                "alsf"=>"grade" ,
                "alsn_aldarsy"=>"year" ,
                "asm_almdrs"=>"school" ,
                "akhr_ntyj" =>"points" ,
                "almsto_althsyl"=>"level",
                "alhal_alshy"=>"condition",
                "almrd_ao_aleaaak"=>"disease_id",
                "tfasyl_alhal_alshy"=>"details",
                "tarykh_alzyar" => "visited_at",
                "asm_albahth_alajtmaaay" => "visitor",
                "hl_ysly"=>"prayer",
                "sbb_trk_alsla"=>"prayer_reason",
                "hl_yhfth_alkran"=>"save_quran",
                "aadd_alajza"=>"quran_parts",
                "aadd_alsor"=>"quran_chapters",
                "sbb_trk_althfyth"=>"quran_reason",
                "mlthk_balmsjd"=>"quran_center",
                "tarykh_ofa_alab"=>"quran_center",
                "tarykh_alofa"=>"death_date",
                "sbb_alofa"=>"death_cause_id",
                "sl_alkrab"=>"kinship_id",
                "hl_alam_maayl"=>"mother_is_guardian",
                "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
                "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
                "hl_bhaj_ltrmym_llmoaem" => "need_repair",
                "osf_odaa_almnzl" => "repair_notes",
                "hl_ldyh_tamyn_shy" => "has_health_insurance",
                "noaa_altamyn_alshy" => "health_insurance_type",
                "rkm_altamyn_alshy" => "health_insurance_number",
                "hl_ystkhdm_jhaz" => "has_device",
                "asm_aljhaz_almstkhdm" => "used_device_name",
                "almrd_ao_alaaaak" => "disease_id",
                "rkm_hoy_albahth" => "visitor_card",
                "ray_albahth_alajtmaaay" => "visitor_opinion",
                "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",
                "hl_ldyh_thmm_maly"=> "receivables",
                "kym_althmm_balshykl"=> "receivables_sk_amount",
            ];
        }

        return [
            "rkm_alhoy" =>'id_card_number',
            "noaa_albtak"=>'card_type',
            "alasm_alaol_aarby"=>'first_name',
            "alasm_althany_aarby"=>'second_name',
            "alasm_althalth_aarby"=>'third_name',
            "alaaael_aarby"=>'last_name',
            "asm_alaaael_alsabk"=>'prev_family_name',
            "alasm_alaol_anjlyzy"=>'en_first_name',
            "alasm_althany_anjlyzy"=>'en_second_name',
            "alasm_althalth_anjlyzy"=>'en_third_name',
            "alaaael_anjlyzy"=>'en_last_name',
            "tarykh_almylad"=>'birthday',
            "mkan_almylad"=>'birth_place',
            "aljns"=>'gender',
            "aljnsy"=>'nationality',
            "aadd_alzojat"=>'spouses',
            "alhal_alajtmaaay"=>'marital_status_id',
            "hal_almoatn"=>'refugee',
            "rkm_krt_altmoyn"=>'unrwa_card_number',
            "aldol"=>'adscountry_id',
            "almhafth"=>'adsdistrict_id',
            "almntk"=>'adsregion_id',
            "alhy"=>'adsneighborhood_id',
            "almrbaa"=>'adssquare_id',
            "almsjd"=>'adsmosques_id',
            "tfasyl_alaanoan"=>'street_address',
            "rkm_joal_asasy"=>'primary_mobile',
            "rkm_alotny"=>'wataniya',
            "rkm_joal_ahtyaty"=>'secondary_mobile',
            "rkm_hatf"=>'phone',
            "asm_albnk"=>'bank_id',
            "alfraa"=>'branch_name',
            "asm_sahb_alhsab"=>'account_owner',
            "rkm_alhsab"=>'account_number',
            "hl_yaaml"=>'working',
            "hl_taaml"=>'working',
            "kadr_aal_alaaml"=>'can_work',
            "sbb_aadm_alkdr"=>'work_reason_id',
            "hal_alaaml"=>'work_status_id',
            "fe_alajr"=>'work_wage_id',
            "alothyf"=>'work_job_id',
            "mkan_alaaml"=>'work_location',
            "aldkhl_alshhry_shykl"=>'monthly_income',

            "aadd_alghrf"=>'rooms',
            "almsah_balmtr"=>'area',
            "kym_alayjar"=>'rent_value',
            "noaa_almlky"=>'property_type_id',
            "mlky_almnzl"=>'property_type_id',
            "noaa_skf_almnzl"=>'roof_material_id',
            "skf_almnzl"=>'roof_material_id',

            "hal_almskn"=>'residence_condition',
            "hal_alathath"=>'indoor_condition',
            "kably_almnzl_llskn"=>'habitable',

            "odaa_almnzl"=>'house_condition',

            "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
            "asm_almoess_alty_oaadt_balbna"=>'organization_name',
            "ahtyajat_alaaael_bshd"=>'needs',
            "tosyat_albahth_alajtmaaay"=>'notes',
            "ydrs_ao_mtaalm"=>"study" ,
            "noaa_aldras"=>"type" ,
            "jh_aldras"=>"authority" ,
            "almoehl_alaalmy"=>"stage" ,
            "almrhl_aldrasy"=>"degree" ,
            "altkhss"=>"specialization" ,
            "alsf"=>"grade" ,
            "alsn_aldarsy"=>"year" ,
            "asm_almdrs"=>"school" ,
            "akhr_ntyj" =>"points" ,
            "almsto_althsyl"=>"level",
            "alhal_alshy"=>"condition",
            "almrd_ao_aleaaak"=>"disease_id",
            "tfasyl_alhal_alshy"=>"details",
            "tarykh_alzyar" => "visited_at",
            "asm_albahth_alajtmaaay" => "visitor",
            "hl_ysly"=>"prayer",
            "sbb_trk_alsla"=>"prayer_reason",
            "hl_yhfth_alkran"=>"save_quran",
            "aadd_alajza"=>"quran_parts",
            "aadd_alsor"=>"quran_chapters",
            "sbb_trk_althfyth"=>"quran_reason",
            "mlthk_balmsjd"=>"quran_center",
            "tarykh_ofa_alab"=>"quran_center",
            "tarykh_alofa"=>"death_date",
            "sbb_alofa"=>"death_cause_id",
            "sl_alkrab"=>"kinship_id",
            "hl_alam_maayl"=>"mother_is_guardian",

            "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
            "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
            "hl_bhaj_ltrmym_llmoaem" => "need_repair",
            "osf_odaa_almnzl" => "repair_notes",
            "hl_ldyh_tamyn_shy" => "has_health_insurance",
            "noaa_altamyn_alshy" => "health_insurance_type",
            "rkm_altamyn_alshy" => "health_insurance_number",
            "hl_ystkhdm_jhaz" => "has_device",
            "asm_aljhaz_almstkhdm" => "used_device_name",
            "almrd_ao_alaaaak" => "disease_id",
            "rkm_hoy_albahth" => "visitor_card",
            "ray_albahth_alajtmaaay" => "visitor_opinion",
            "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",

            "hl_ldyh_thmm_maly"=> "receivables",
            "kym_althmm_balshykl"=> "receivables_sk_amount",

        ];

    }

    public static function DisableCaseInNoConnected($case,$CurOrg,$user_id){

        $person_id = $case->person_id;

        $query = \DB::table('char_cases')
            ->join('char_categories', function ($join){
                $join->on('char_categories.id', '=', 'char_cases.category_id');
                $join->where('char_categories.type', 2);
            })

            ->where(function ($q) use ($CurOrg,$person_id) {
                $q ->where('char_cases.organization_id','!=',$CurOrg);
                $q->where('char_cases.person_id',$person_id);
            })
            ->selectRaw('char_cases.*')->get();

        foreach ($query as $k=>$v) {
            if( $v->organization_id != $CurOrg){
                $passed = true;
                $mainConnector=OrgLocations::getConnected($v->organization_id);
                if(!is_null($case->adsdistrict_id)){
                    if(!in_array($case->adsdistrict_id,$mainConnector)){
                        $passed = false;
                    }else{
                        if(!is_null($case->adsregion_id)){
                            if(!in_array($case->adsregion_id,$mainConnector)){
                                $passed = false;
                            }else{

                                if(!is_null($case->adsneighborhood_id)){
                                    if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                        $passed = false;
                                    }else{

                                        if(!is_null($case->adssquare_id)){
                                            if(!in_array($case->adssquare_id,$mainConnector)){
                                                $passed = false;
                                            }else{
                                                if(!is_null($case->adsmosques_id)){
                                                    if(!in_array($case->adsmosques_id,$mainConnector)){
                                                        $passed = false;
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }
                    }
                }else{
                    $passed = false;
                }


                if(!$passed){
                    \Common\Model\AidsCases::where('id',$v->id)->update(['status' => 1]);
                    $action='CASE_UPDATED';
                    $message= trans('common::application.edited data') . '  : '.' "'.$case->full_name. ' " ';
                    \Log\Model\Log::saveNewLog($action,$message);
                    \Common\Model\CasesStatusLog::create(['case_id'=>$v->id, 'user_id'=>$v->user_id, 'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                }

            }

        }
        return true;


    }

    public static function DisablePersonInNoConnected($Person,$CurOrg,$user_id){

        $person_id = $Person->id;

        $query = \DB::table('char_cases')
            ->join('char_categories', function ($join){
                $join->on('char_categories.id', '=', 'char_cases.category_id');
                $join->where('char_categories.type', 2);
            })

            ->where(function ($q) use ($person_id) {
//                $q ->where('char_cases.organization_id','!=',$CurOrg);
                $q->where('char_cases.person_id',$person_id);
            })
            ->selectRaw('char_cases.*')->get();

        foreach ($query as $k=>$v) {
            $passed = true;
            $mainConnector=OrgLocations::getConnected($v->organization_id);
            if(!is_null($Person->adsdistrict_id)){
                if(!in_array($Person->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($Person->adsregion_id)){
                        if(!in_array($Person->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($Person->adsneighborhood_id)){
                                if(!in_array($Person->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($Person->adssquare_id)){
                                        if(!in_array($Person->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($Person->adsmosques_id)){
                                                if(!in_array($Person->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }


            if(!$passed){
                \Common\Model\AidsCases::where('id',$v->id)->update(['status' => 1]);
                $action='CASE_UPDATED';
                $message=trans('common::application.edited data') . '  : '.' "'.$Person->full_name. ' " ';
                \Log\Model\Log::saveNewLog($action,$message);
                \Common\Model\CasesStatusLog::create(['case_id'=>$v->id, 'user_id'=>$v->user_id,
                    'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.$Person->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'), 'status'=>1, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$Person->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
            }
        }
        return true;


    }

    public static function PersonId($id){
        $query = self::query()->addSelect('char_cases.person_id')->where('char_cases.id', '=', $id)->first();
        return $query->person_id;
    }

    public static function getByPersonId($person_id,$organization_id){
        $query = self::query()->addSelect('char_cases.*')->where(['char_cases.person_id'=> $person_id,'organization_id'=> $organization_id,'status'=>0])->first();
        return $query;
    }

    public static function filterCasesByCard_($cards,$category_type)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = \DB::table('char_cases')
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q) use($category_type){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',$category_type);
            })
            ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                $q->where('char_marital_status.language_id',$language_id);
            })
            ->where(function ($q) use ($cards){
                $q->whereNull('char_cases.deleted_at');
                $q->whereIn('char_persons.id_card_number',$cards);
            });


        if($category_type == 1){
            $result ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) use ($language_id){
                    $q->on('guardian_cont2.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')

                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                });
        }
        else{
            $result ->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                $q->on('primary_num.person_id', '=', 'char_persons.id');
                $q->where('primary_num.contact_type','primary_mobile');
            })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                });
        }

        if($category_type == 1) {

            $result ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                $q->where('mosques_name.language_id',$language_id);
            })
                ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                    $q->on('L1.location_id', '=', 'char_persons.location_id');
                    $q->where('L1.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                    $q->on('L4.location_id', '=', 'char_persons.country');
                    $q->where('L4.language_id',$language_id);
                })
                ->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id', '=', 'char_persons.id')
                ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                    $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                    $q->where('char_edu_stages.language_id',$language_id);
                })
                ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                    $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                    $q->where('char_edu_authorities.language_id',$language_id);
                })
                ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                    $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                    $q->where('char_edu_degrees.language_id',$language_id);
                })
                ->leftjoin('char_death_causes_i18n as father_death_causes', function($q) use ($language_id){
                    $q->on('father_death_causes.death_cause_id', '=', 'f.death_cause_id');
                    $q->where('father_death_causes.language_id',$language_id);
                })
                ->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id')
                ->leftjoin('char_work_jobs_i18n as father_work_jobs', function($q) use ($language_id){
                    $q->on('father_work_jobs.work_job_id', '=', 'father_work.work_job_id');
                    $q->where('father_work_jobs.language_id',$language_id);
                })
                ->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id')
                ->leftjoin('char_edu_degrees_i18n as father_edu_degrees', function($q) use ($language_id){
                    $q->on('father_edu_degrees.edu_degree_id', '=', 'father_education.degree');
                    $q->where('father_edu_degrees.language_id',$language_id);
                })
                ->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id')
                ->leftjoin('char_diseases_i18n as father_diseases', function($q) use ($language_id){
                    $q->on('father_diseases.disease_id', '=', 'father_health.disease_id');
                    $q->where('father_diseases.language_id',$language_id);
                })
//                    ->leftjoin('char_death_causes_i18n as mother_death_causes', function($q) use ($language_id){
//                        $q->on('mother_death_causes.death_cause_id', '=', 'm.death_cause_id');
//                        $q->where('mother_death_causes.language_id',$language_id);
//                    })
                ->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id')
                ->leftjoin('char_work_jobs_i18n as mother_work_jobs', function($q) use ($language_id){
                    $q->on('mother_work_jobs.work_job_id', '=', 'mother_work.work_job_id');
                    $q->where('mother_work_jobs.language_id',$language_id);
                })
                ->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id')
                ->leftjoin('char_edu_stages_i18n as mother_edu_stages', function($q) use ($language_id){
                    $q->on('mother_edu_stages.edu_stage_id', '=', 'mother_education.stage');
                    $q->where('mother_edu_stages.language_id',$language_id);
                })
                ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                    $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                    $q->where('mother_marital_status.language_id',$language_id);
                })
                ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
                ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                    $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                    $q->where('mother_diseases.language_id',$language_id);
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                    $q->on('guardian_cont1.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont1.contact_type','phone');
                })

                ->leftjoin('char_persons_contact as guardian_cont4', function($q) use ($language_id){
                    $q->on('guardian_cont4.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont4.contact_type','wataniya');
                })
//                    ->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
//                        $q->on('guardian_cont3.person_id', '=', 'char_guardians.guardian_id');
//                        $q->where('guardian_cont3.contact_type','secondery_mobile');
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L1', function($q) use ($language_id){
//                        $q->on('guardian_L1.location_id', '=', 'g.location_id');
//                        $q->where('guardian_L1.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L2', function($q) use ($language_id){
//                        $q->on('guardian_L2.location_id', '=', 'g.city');
//                        $q->where('guardian_L2.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L3', function($q) use ($language_id){
//                        $q->on('guardian_L3.location_id', '=', 'g.governarate');
//                        $q->where('guardian_L3.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L4', function($q) use ($language_id){
//                        $q->on('guardian_L4.location_id', '=', 'g.country');
//                        $q->where('guardian_L4.language_id',$language_id);
//                    })
                ->leftjoin('char_residence as guardian_residence','guardian_residence.person_id', '=', 'g.id')
                ->leftjoin('char_property_types_i18n as guardian_property_types', function($q) use ($language_id){
                    $q->on('guardian_property_types.property_type_id', '=', 'guardian_residence.property_type_id');
                    $q->where('guardian_property_types.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n as guardian_roof_materials', function($q) use ($language_id){
                    $q->on('guardian_roof_materials.roof_material_id', '=', 'guardian_residence.roof_material_id');
                    $q->where('guardian_roof_materials.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'guardian_residence.indoor_condition');
                    $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                    $q->where('guardian_house_status_i18n.language_id',$language_id);
                })
            ;
        }
        else{
            $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })

                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_reconstructions','char_reconstructions.case_id', '=', 'char_cases.id');
        }

        $result->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
            $q->on('L.location_id', '=', 'char_persons.birth_place');
            $q->where('L.language_id',$language_id);
        })
            ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                $q->on('L0.location_id', '=', 'char_persons.nationality');
                $q->where('L0.language_id',$language_id);
            })
            ->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id')

            ->leftjoin('char_persons_contact as wataniya_num', function($q) use ($language_id){
                $q->on('wataniya_num.person_id', '=', 'char_persons.id');
                $q->where('wataniya_num.contact_type','wataniya');
            })
//                ->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
//                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
//                    $q->where('secondary_num.contact_type','secondary_mobile');
//                })
            ->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                $q->on('phone_num.person_id', '=', 'char_persons.id');
                $q->where('phone_num.contact_type','phone');
            })
            ->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id')
            ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                $q->where('char_property_types_i18n.language_id',$language_id);
            })
            ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                $q->where('char_roof_materials_i18n.language_id',$language_id);
            })
            ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                $q->where('char_building_status_i18n.language_id',$language_id);
            })
            ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                $q->where('char_furniture_status_i18n.language_id',$language_id);
            })
            ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                $q->where('char_habitable_status_i18n.language_id',$language_id);
            })
            ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                $q->where('char_house_status_i18n.language_id',$language_id);
            })
            ->leftjoin('char_persons_work', 'char_persons_work.person_id', '=', 'char_persons.id')
            ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                $q->on('work_job.work_job_id', '=', 'char_persons_work.work_job_id');
                $q->where('work_job.language_id',$language_id);
            })
            ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                $q->on('work_wage.id', '=', 'char_persons_work.work_wage_id');
            })
            ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                $q->on('work_status.work_status_id', '=', 'char_persons_work.work_status_id');
                $q->where('work_status.language_id',$language_id);
            })
            ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                $q->on('work_reason.work_reason_id', '=', 'char_persons_work.work_reason_id');
                $q->where('work_reason.language_id',$language_id);
            });


        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');


        $result->orderBy('char_cases.rank','char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');

        $result->selectRaw("
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                 CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                 org.name as organization_name,
                                 CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                 CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                 CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type
                                ");
        if($category_type ==1) {
            $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.old_id_card_number  END  AS mother_old_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.old_id_card_number  END  AS father_old_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
        }

        $result->selectRaw("CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                            CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  
                            CASE
                                           WHEN char_persons.gender is null THEN ' '
                                           WHEN char_persons.gender = 1 THEN '$male'
                                           WHEN char_persons.gender = 2 THEN '$female'
                                     END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status_id ");

        if($category_type == 2) {
            $result->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted");
        }
        $result->selectRaw("CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                     CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS nationality,
                                      char_persons.family_cnt AS family_count,
                                           char_persons.male_live AS male_count,
                                           char_persons.female_live AS female_count
                                          ");
        if($category_type == 2){

            $result->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                    CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                    CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                    CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                    CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                    CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                    CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                    CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address,
                                            CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                            CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                     CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                     CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone
                                    ");
        }
        else{
            $result->selectRaw("CASE WHEN char_persons.country  is null THEN ' ' Else L4.name END   AS country,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name,
                                        CASE WHEN char_persons.location_id is null THEN ' ' Else L1.name END   AS nearLocation,
                                        CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ',
                                                  ifnull(mosques_name.name,' '),' ',
                                       ifnull(char_persons.street_address,' '))
                                     AS address,CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income");
        }

        $result->selectRaw(" CASE
                                       WHEN char_persons_work.working is null THEN ' '
                                       WHEN char_persons_work.working = 1 THEN '$yes'
                                       WHEN char_persons_work.working = 2 THEN '$no'
                                     END
                                     AS working,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_status.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_status,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_job.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_job,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_wage.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_wage,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_location,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN '$yes'
                                           WHEN char_persons_work.can_work = 2 THEN '$no'
                                     END
                                     AS can_work,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN ' '
                                           WHEN char_persons_work.can_work = 2 THEN work_reason.name
                                     END
                                     AS work_reason,
                                     CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                     CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                     CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                     CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                     CASE WHEN char_residence.need_repair is null THEN ' '
                                          WHEN char_residence.need_repair = 1 THEN '$yes'
                                          WHEN char_residence.need_repair = 2 THEN '$no'
                                        END AS need_repair ,
                                     CASE WHEN char_residence.repair_notes is null   THEN ' ' Else char_residence.repair_notes  END  AS repair_notes
                                     
                                    ");
        if($category_type == 2){
            $result->selectRaw("CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' $citizen '
                                           WHEN char_persons.refugee = 2 THEN '$refugee'
                                     END
                                     AS refugee,
                                     CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' '
                                           WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                     END
                                     AS unrwa_card_number,
                                     CASE
                                        WHEN char_reconstructions.promised is null THEN ' '
                                        WHEN char_reconstructions.promised = 1 THEN '$no'
                                        WHEN char_reconstructions.promised = 2 THEN '$yes'
                                    END
                                    AS promised,
                                    CASE
                                       WHEN char_reconstructions.promised = 1 THEN ' '
                                       WHEN char_reconstructions.promised is null THEN ' '
                                       WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name
                                   END
                                   AS ReconstructionOrg,
                                   char_cases.rank,
                                   CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                   CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                   CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS notes,
                                   CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                   CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                   CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                                 ");
        }
        else
            if($category_type ==1){
                $result->selectRaw("
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                      CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS study_type,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS type,
                                                      CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                                   Else ' '

                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                    CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                         WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                         WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                         WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                   END AS prayer,
                                                   CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                                        WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                        Else '$yes'
                                                    END AS save_quran,
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,
                                                     CASE WHEN f.death_date is null THEN ' '  Else father_death_causes.name END AS father_death_cause,
                                                     CASE
                                                            WHEN father_health.condition is null THEN ' '
                                                            WHEN father_health.condition = 1 THEN '$perfect'
                                                            WHEN father_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN father_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS father_health_status,
                                                     CASE WHEN father_health.details is null THEN ' ' Else father_health.details END   AS father_details,
                                                     CASE WHEN father_diseases.name is null THEN ' ' Else father_diseases.name END   AS father_disease_id,
                                                     CASE WHEN father_education.specialization is null THEN ' '  Else father_education.specialization END  AS father_Specialization,
                                                     CASE WHEN father_edu_degrees.name is null THEN ' ' Else father_edu_degrees.name END as father_degree,
                                                     CASE
                                                          WHEN father_work.working is null THEN ' '
                                                          WHEN father_work.working = 1 THEN '$yes'
                                                          WHEN father_work.working = 2 THEN '$no'
                                                       END
                                                     AS father_working,
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work_jobs.name
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_type,
                                                     CASE
                                                       WHEN father_work.working is null THEN ' '
                                                       WHEN father_work.working = 1 THEN f.monthly_income
                                                       WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_monthly_income,
                                                    
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work.work_location
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_location,
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN m.death_date is null THEN ' '  Else father_death_causes.name END AS mother_death_cause,
                                                      CASE
                                                            WHEN mother_health.condition is null THEN ' '
                                                            WHEN mother_health.condition = 1 THEN '$perfect'
                                                            WHEN mother_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN mother_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS mother_health_status,
                                                   CASE WHEN mother_health.details is null THEN ' ' Else mother_health.details END   AS mother_details,
                                                   CASE WHEN mother_diseases.name is null THEN ' ' Else mother_diseases.name END   AS mother_disease_id,
                                                   CASE WHEN mother_education.specialization is null THEN ' '  Else mother_education.specialization END  AS mother_Specialization,
                                                     CASE WHEN mother_edu_stages.name is null THEN ' ' Else mother_edu_stages.name END as mother_stage,
                                                      CASE
                                                          WHEN mother_work.working is null THEN ' '
                                                          WHEN mother_work.working = 1 THEN '$yes'
                                                          WHEN mother_work.working = 2 THEN '$no'
                                                       END
                                                     AS mother_working,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work_jobs.name
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_type,
                                                     CASE
                                                       WHEN mother_work.working is null THEN ' '
                                                       WHEN mother_work.working = 1 THEN m.monthly_income
                                                       WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_monthly_income,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work.work_location
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_location,
                                                     
                                                     CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   AS guardian_phone,
                                                     CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                                     CASE WHEN guardian_cont4.contact_value is null THEN ' ' Else guardian_cont4.contact_value END   AS guardian_wataniya,
                                                     CASE WHEN guardian_property_types.name is null THEN ' ' Else guardian_property_types.name END   AS guardian_property_type,
                                                     CASE WHEN guardian_roof_materials.name is null THEN ' ' Else guardian_roof_materials.name END   AS guardian_roof_material,
                                                     CASE WHEN guardian_residence.area is null THEN ' ' Else guardian_residence.area  END  AS guardian_area,
                                                     CASE WHEN guardian_residence.rooms is null THEN ' ' Else guardian_residence.rooms  END  AS guardian_rooms,
                                                      CASE WHEN guardian_furniture_status_i18n.name is null THEN ' ' Else guardian_furniture_status_i18n.name  END  AS guardian_indoor_condition,
                                                         CASE WHEN guardian_house_status_i18n.name is null     THEN ' ' Else guardian_house_status_i18n.name  END  AS guardian_residence_condition
                                                    ");
            }


        $output = $result->get();
        return $output;
    }

    public static function listCasesByCard($cards,$category_type)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = \DB::table('char_cases')
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q) use($category_type){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',$category_type);
            })
            ->where(function ($q) use ($cards){
                $q->whereNull('char_cases.deleted_at');
                $q->whereIn('char_persons.id_card_number',$cards);
            });


        if($category_type == 1){
            $result ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) use ($language_id){
                    $q->on('guardian_cont2.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')

                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                });
        }
        else{
            $result ->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                $q->on('primary_num.person_id', '=', 'char_persons.id');
                $q->where('primary_num.contact_type','primary_mobile');
            })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                });
        }

        if($category_type == 1) {

            $result ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                $q->where('mosques_name.language_id',$language_id);
            })
                ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                    $q->on('L1.location_id', '=', 'char_persons.location_id');
                    $q->where('L1.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                    $q->on('L4.location_id', '=', 'char_persons.country');
                    $q->where('L4.language_id',$language_id);
                })
                ->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id', '=', 'char_persons.id')
                ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                    $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                    $q->where('char_edu_stages.language_id',$language_id);
                })
                ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                    $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                    $q->where('char_edu_authorities.language_id',$language_id);
                })
                ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                    $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                    $q->where('char_edu_degrees.language_id',$language_id);
                })
                ->leftjoin('char_death_causes_i18n as father_death_causes', function($q) use ($language_id){
                    $q->on('father_death_causes.death_cause_id', '=', 'f.death_cause_id');
                    $q->where('father_death_causes.language_id',$language_id);
                })
                ->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id')
                ->leftjoin('char_work_jobs_i18n as father_work_jobs', function($q) use ($language_id){
                    $q->on('father_work_jobs.work_job_id', '=', 'father_work.work_job_id');
                    $q->where('father_work_jobs.language_id',$language_id);
                })
                ->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id')
                ->leftjoin('char_edu_degrees_i18n as father_edu_degrees', function($q) use ($language_id){
                    $q->on('father_edu_degrees.edu_degree_id', '=', 'father_education.degree');
                    $q->where('father_edu_degrees.language_id',$language_id);
                })
                ->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id')
                ->leftjoin('char_diseases_i18n as father_diseases', function($q) use ($language_id){
                    $q->on('father_diseases.disease_id', '=', 'father_health.disease_id');
                    $q->where('father_diseases.language_id',$language_id);
                })
//                    ->leftjoin('char_death_causes_i18n as mother_death_causes', function($q) use ($language_id){
//                        $q->on('mother_death_causes.death_cause_id', '=', 'm.death_cause_id');
//                        $q->where('mother_death_causes.language_id',$language_id);
//                    })
                ->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id')
                ->leftjoin('char_work_jobs_i18n as mother_work_jobs', function($q) use ($language_id){
                    $q->on('mother_work_jobs.work_job_id', '=', 'mother_work.work_job_id');
                    $q->where('mother_work_jobs.language_id',$language_id);
                })
                ->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id')
                ->leftjoin('char_edu_stages_i18n as mother_edu_stages', function($q) use ($language_id){
                    $q->on('mother_edu_stages.edu_stage_id', '=', 'mother_education.stage');
                    $q->where('mother_edu_stages.language_id',$language_id);
                })
                ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                    $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                    $q->where('mother_marital_status.language_id',$language_id);
                })
                ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
                ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                    $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                    $q->where('mother_diseases.language_id',$language_id);
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                    $q->on('guardian_cont1.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont1.contact_type','phone');
                })

                ->leftjoin('char_persons_contact as guardian_cont4', function($q) use ($language_id){
                    $q->on('guardian_cont4.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont4.contact_type','wataniya');
                })
//                    ->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
//                        $q->on('guardian_cont3.person_id', '=', 'char_guardians.guardian_id');
//                        $q->where('guardian_cont3.contact_type','secondery_mobile');
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L1', function($q) use ($language_id){
//                        $q->on('guardian_L1.location_id', '=', 'g.location_id');
//                        $q->where('guardian_L1.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L2', function($q) use ($language_id){
//                        $q->on('guardian_L2.location_id', '=', 'g.city');
//                        $q->where('guardian_L2.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L3', function($q) use ($language_id){
//                        $q->on('guardian_L3.location_id', '=', 'g.governarate');
//                        $q->where('guardian_L3.language_id',$language_id);
//                    })
//                    ->leftjoin('char_locations_i18n As guardian_L4', function($q) use ($language_id){
//                        $q->on('guardian_L4.location_id', '=', 'g.country');
//                        $q->where('guardian_L4.language_id',$language_id);
//                    })
                ->leftjoin('char_residence as guardian_residence','guardian_residence.person_id', '=', 'g.id')
                ->leftjoin('char_property_types_i18n as guardian_property_types', function($q) use ($language_id){
                    $q->on('guardian_property_types.property_type_id', '=', 'guardian_residence.property_type_id');
                    $q->where('guardian_property_types.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n as guardian_roof_materials', function($q) use ($language_id){
                    $q->on('guardian_roof_materials.roof_material_id', '=', 'guardian_residence.roof_material_id');
                    $q->where('guardian_roof_materials.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'guardian_residence.indoor_condition');
                    $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                    $q->where('guardian_house_status_i18n.language_id',$language_id);
                })
            ;
        }
        else{
            $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })

                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_reconstructions','char_reconstructions.case_id', '=', 'char_cases.id');
        }




        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');


        $result->orderBy('char_cases.rank','char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');

        $result->selectRaw("
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                 CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                 org.name as organization_name,
                                 CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                 CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                 CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type
                                ");
        if($category_type ==1) {
            $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.old_id_card_number  END  AS mother_old_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.old_id_card_number  END  AS father_old_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
        }

        $result->selectRaw("CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                            CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  

                                     CASE
                                           WHEN char_persons.gender is null THEN ' '
                                           WHEN char_persons.gender = 1 THEN '$male'
                                           WHEN char_persons.gender = 2 THEN '$female'
                                     END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status_id ");

        if($category_type == 2) {
            $result->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted");
        }
        $result->selectRaw("CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                     CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS nationality,
                                      char_persons.family_cnt AS family_count,
                                           char_persons.male_live AS male_count,
                                           char_persons.female_live AS female_count
                                          ");
        if($category_type == 2){

            $result->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                    CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                    CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                    CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                    CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                    CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                    CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                    CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address,
                                            CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                            CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                     CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                     CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone
                                    ");
        }
        else{
            $result->selectRaw("CASE WHEN char_persons.country  is null THEN ' ' Else L4.name END   AS country,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name,
                                        CASE WHEN char_persons.location_id is null THEN ' ' Else L1.name END   AS nearLocation,
                                        CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ',
                                                  ifnull(mosques_name.name,' '),' ',
                                       ifnull(char_persons.street_address,' '))
                                     AS address,CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income");
        }

        $result->selectRaw(" CASE
                                       WHEN char_persons_work.working is null THEN ' '
                                       WHEN char_persons_work.working = 1 THEN '$yes'
                                       WHEN char_persons_work.working = 2 THEN '$no'
                                     END
                                     AS working,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_status.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_status,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_job.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_job,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_wage.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_wage,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_location,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN '$yes'
                                           WHEN char_persons_work.can_work = 2 THEN '$no'
                                     END
                                     AS can_work,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN ' '
                                           WHEN char_persons_work.can_work = 2 THEN work_reason.name
                                     END
                                     AS work_reason,
                                     CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                     CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                     CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                     CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                     CASE WHEN char_residence.need_repair is null THEN ' '
                                          WHEN char_residence.need_repair = 1 THEN '$yes'
                                          WHEN char_residence.need_repair = 2 THEN '$no'
                                        END AS need_repair ,
                                     CASE WHEN char_residence.repair_notes is null   THEN ' ' Else char_residence.repair_notes  END  AS repair_notes
                                    ");
        if($category_type == 2){
            $result->selectRaw("CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' $citizen '
                                           WHEN char_persons.refugee = 2 THEN '$refugee'
                                     END
                                     AS refugee,
                                     CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' '
                                           WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                     END
                                     AS unrwa_card_number,
                                     CASE
                                        WHEN char_reconstructions.promised is null THEN ' '
                                        WHEN char_reconstructions.promised = 1 THEN '$no'
                                        WHEN char_reconstructions.promised = 2 THEN '$yes'
                                    END
                                    AS promised,
                                    CASE
                                       WHEN char_reconstructions.promised = 1 THEN ' '
                                       WHEN char_reconstructions.promised is null THEN ' '
                                       WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name
                                   END
                                   AS ReconstructionOrg,
                                   char_cases.rank,
                                   CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                   CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                   CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS notes,
                                   CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                   CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                   CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                                 ");
        }
        else
            if($category_type ==1){
                $result->selectRaw("
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                      CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS study_type,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS type,
                                                      CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                                   Else ' '

                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                    CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                         WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                         WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                         WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                   END AS prayer,
                                                   CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                                        WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                        Else '$yes'
                                                    END AS save_quran,
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,
                                                     CASE WHEN f.death_date is null THEN ' '  Else father_death_causes.name END AS father_death_cause,
                                                     CASE
                                                            WHEN father_health.condition is null THEN ' '
                                                            WHEN father_health.condition = 1 THEN '$perfect'
                                                            WHEN father_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN father_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS father_health_status,
                                                     CASE WHEN father_health.details is null THEN ' ' Else father_health.details END   AS father_details,
                                                     CASE WHEN father_diseases.name is null THEN ' ' Else father_diseases.name END   AS father_disease_id,
                                                     CASE WHEN father_education.specialization is null THEN ' '  Else father_education.specialization END  AS father_Specialization,
                                                     CASE WHEN father_edu_degrees.name is null THEN ' ' Else father_edu_degrees.name END as father_degree,
                                                     CASE
                                                          WHEN father_work.working is null THEN ' '
                                                          WHEN father_work.working = 1 THEN '$yes'
                                                          WHEN father_work.working = 2 THEN '$no'
                                                       END
                                                     AS father_working,
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work_jobs.name
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_type,
                                                     CASE
                                                       WHEN father_work.working is null THEN ' '
                                                       WHEN father_work.working = 1 THEN f.monthly_income
                                                       WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_monthly_income,
                                                    
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work.work_location
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_location,
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN m.death_date is null THEN ' '  Else father_death_causes.name END AS mother_death_cause,
                                                      CASE
                                                            WHEN mother_health.condition is null THEN ' '
                                                            WHEN mother_health.condition = 1 THEN '$perfect'
                                                            WHEN mother_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN mother_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS mother_health_status,
                                                   CASE WHEN mother_health.details is null THEN ' ' Else mother_health.details END   AS mother_details,
                                                   CASE WHEN mother_diseases.name is null THEN ' ' Else mother_diseases.name END   AS mother_disease_id,
                                                   CASE WHEN mother_education.specialization is null THEN ' '  Else mother_education.specialization END  AS mother_Specialization,
                                                     CASE WHEN mother_edu_stages.name is null THEN ' ' Else mother_edu_stages.name END as mother_stage,
                                                      CASE
                                                          WHEN mother_work.working is null THEN ' '
                                                          WHEN mother_work.working = 1 THEN '$yes'
                                                          WHEN mother_work.working = 2 THEN '$no'
                                                       END
                                                     AS mother_working,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work_jobs.name
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_type,
                                                     CASE
                                                       WHEN mother_work.working is null THEN ' '
                                                       WHEN mother_work.working = 1 THEN m.monthly_income
                                                       WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_monthly_income,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work.work_location
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_location,
                                                     
                                                     CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   AS guardian_phone,
                                                     CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                                     CASE WHEN guardian_cont4.contact_value is null THEN ' ' Else guardian_cont4.contact_value END   AS guardian_wataniya,
                                                     CASE WHEN guardian_property_types.name is null THEN ' ' Else guardian_property_types.name END   AS guardian_property_type,
                                                     CASE WHEN guardian_roof_materials.name is null THEN ' ' Else guardian_roof_materials.name END   AS guardian_roof_material,
                                                     CASE WHEN guardian_residence.area is null THEN ' ' Else guardian_residence.area  END  AS guardian_area,
                                                     CASE WHEN guardian_residence.rooms is null THEN ' ' Else guardian_residence.rooms  END  AS guardian_rooms,
                                                      CASE WHEN guardian_furniture_status_i18n.name is null THEN ' ' Else guardian_furniture_status_i18n.name  END  AS guardian_indoor_condition,
                                                         CASE WHEN guardian_house_status_i18n.name is null     THEN ' ' Else guardian_house_status_i18n.name  END  AS guardian_residence_condition
                                                    ");
            }


        $output = $result->get();
        return $output;
    }

    public static function filterCasesByCard($cards,$category_type)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = \DB::table('char_cases')
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q) use($category_type){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',$category_type);
            })
            ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                $q->where('char_marital_status.language_id',$language_id);
            })
            ->where(function ($q) use ($cards){
                $q->whereNull('char_cases.deleted_at');
                $q->whereIn('char_persons.id_card_number',$cards);
            });


        if($category_type == 1){
            $result ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) use ($language_id){
                    $q->on('guardian_cont2.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')

                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                });
        }
        else{
            $result ->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                $q->on('primary_num.person_id', '=', 'char_persons.id');
                $q->where('primary_num.contact_type','primary_mobile');
            })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                });
        }

        if($category_type == 1) {

            $result ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                $q->where('mosques_name.language_id',$language_id);
            })
                ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                    $q->on('L1.location_id', '=', 'char_persons.location_id');
                    $q->where('L1.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                    $q->on('L4.location_id', '=', 'char_persons.country');
                    $q->where('L4.language_id',$language_id);
                })
                ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                    $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                    $q->where('mother_marital_status.language_id',$language_id);
                })
                ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
                ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                    $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                    $q->where('mother_diseases.language_id',$language_id);
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                    $q->on('guardian_cont1.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont1.contact_type','phone');
                })

                ->leftjoin('char_persons_contact as guardian_cont4', function($q) use ($language_id){
                    $q->on('guardian_cont4.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont4.contact_type','wataniya');
                })
                ->leftjoin('char_residence as guardian_residence','guardian_residence.person_id', '=', 'g.id')
                ->leftjoin('char_property_types_i18n as guardian_property_types', function($q) use ($language_id){
                    $q->on('guardian_property_types.property_type_id', '=', 'guardian_residence.property_type_id');
                    $q->where('guardian_property_types.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n as guardian_roof_materials', function($q) use ($language_id){
                    $q->on('guardian_roof_materials.roof_material_id', '=', 'guardian_residence.roof_material_id');
                    $q->where('guardian_roof_materials.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'guardian_residence.indoor_condition');
                    $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                    $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                    $q->where('guardian_house_status_i18n.language_id',$language_id);
                })
            ;
        }
        else{
            $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })

                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_reconstructions','char_reconstructions.case_id', '=', 'char_cases.id');
        }

        $result
            ->leftjoin('char_persons_contact as wataniya_num', function($q) use ($language_id){
                $q->on('wataniya_num.person_id', '=', 'char_persons.id');
                $q->where('wataniya_num.contact_type','wataniya');
            })
            ->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                $q->on('phone_num.person_id', '=', 'char_persons.id');
                $q->where('phone_num.contact_type','phone');
            })
        ;


        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $male = trans('common::application.male');
        $female = trans('common::application.female');


        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');


        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');


        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');


        $result->orderBy('char_cases.rank','char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');

        $result->selectRaw(" char_cases.id as case_id ,
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                 CASE WHEN char_cases.status = 1 THEN '$inactive'WHEN char_cases.status = 0 THEN '$active' END AS cases_status,
                                 org.name as organization_name,
                                 CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                 CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                 CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type
                                ");
        if($category_type ==1) {
            $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.old_id_card_number  END  AS mother_old_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.old_id_card_number  END  AS father_old_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
        }

        $result->selectRaw("CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                            CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  
                            CASE
                                           WHEN char_persons.gender is null THEN ' '
                                           WHEN char_persons.gender = 1 THEN '$male'
                                           WHEN char_persons.gender = 2 THEN '$female'
                                     END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status_id ");

        if($category_type == 2) {
            $result->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted");
        }
        $result->selectRaw(" char_persons.family_cnt AS family_count,char_persons.male_live AS male_count, char_persons.female_live AS female_count");

        if($category_type == 2){

            $result->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                    CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                    CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                    CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                    CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                    CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                    CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                    CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),'  ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address,
                                            CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                            CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                     CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                     CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone
                                    ");
        }
        else{
            $result->selectRaw("CASE WHEN char_persons.country  is null THEN ' ' Else L4.name END   AS country,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name,
                                        CASE WHEN char_persons.location_id is null THEN ' ' Else L1.name END   AS nearLocation,
                                        CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ',
                                                  ifnull(mosques_name.name,' '),' ',
                                       ifnull(char_persons.street_address,' '))
                                     AS address,CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income");
        }

        if($category_type == 2){
            $result->selectRaw("char_cases.rank,
                                   CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS visited_at,
                                   CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS visitor,
                                   CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS notes,
                                   CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                                   CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                                   CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                                 ");
        }
        else
            if($category_type ==1){
                $result->selectRaw("           CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   AS guardian_phone,
                                                     CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                                     CASE WHEN guardian_cont4.contact_value is null THEN ' ' Else guardian_cont4.contact_value END   AS guardian_wataniya
                                                    ");
            }


        $output = $result->get();
        return $output;
    }

    public static function caseList($id_card_number,$user)
    {

        return \DB::table('char_cases')
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->where(function ($sq) use ($id_card_number,$user){
//                $sq->where('organization_id',$user->organization_id);
                $sq->where('person_id', function($q) use($id_card_number,$user){
                       $q->select('id')
                         ->from('char_persons')
                         ->where('id_card_number',$id_card_number);
                });
            })
            ->selectRaw('char_cases.id')->get();
    }

    public static function CatType($id)
    {
        $category= \DB::table('char_categories')
            ->where('char_categories.id',$id)
            ->first();

        return $category->type;

    }

    public static function filterBasicCases($page,$filters,$organization_id,$master)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $condition =array();
        $output=array();
        $all_organization=null;
        $category_type=$filters['category_type'];

        $user = \Auth::user();
        $UserType=$user->type;
        $ancestor = \Organization\Model\Organization::getAncestor($organization_id);

        $char_cases = ['category_id','visitor','status','notes','visitor','visitor_card', 'visitor_opinion', 'visitor_evaluation'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id","is_qualified","qualified_card_number",
            "adsdistrict_id", "adsmosques_id", "card_type", "adsneighborhood_id", "adsregion_id",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount',
            'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];
        $char_persons_i18n = ['en_first_name', 'en_second_name', 'en_third_name', 'en_last_name'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location',
            'gov_work_details'];
        $char_persons_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name', 'gov_health_details'];
        $char_persons_education = ['grade', 'authority','type','stage','level','year','school','points'];
        $char_persons_islamic_commitment = ['prayer','quran_parts', 'quran_chapters','quran_center'];
        $reconstructions=['promised','reconstructions_organization_name'];
        $secondary_mobile=['secondary_mobile'];
        $primary_mobile=['primary_mobile'];
        $phone=['phone'];
        $char_organization_persons_banks = ['bank_id'];
        $char_persons_banks = ['bank_id','branch_name','account_number','account_owner'];
        $char_persons_father = ['father_id_card_number','father_card_type','father_death_cause_id','father_first_name', 'father_monthly_income', 'father_second_name', 'father_third_name', 'father_last_name','father_alive'];
        $char_persons_mother = ['mother_id_card_number','mother_card_type','mother_death_cause_id','mother_first_name','mother_marital_status_id' ,'mother_second_name','mother_monthly_income', 'mother_third_name','mother_nationality', 'mother_last_name','mother_alive'];
        $char_persons_guardian=['guardian_id_card_number','guardian_card_type','guardian_first_name', 'guardian_second_name','guardian_third_name',
            'guardian_gender','guardian_marital_status_id','guardian_location_id','guardian_country','guardian_governarate','guardian_city',
            'guardian_monthly_income','guardian_birth_place','guardian_nationality', 'guardian_last_name','guardian_mosques_id'];
        $guardian_residence = ['guardian_property_type_id', 'guardian_residence_condition','guardian_indoor_condition','guardian_roof_material_id'];
        $mother_health = ['mother_condition','mother_disease_id'];
        $father_health = ['father_condition','father_disease_id'];
        $father_work = ['father_working','father_work_job_id','father_work_location'];
        $mother_work = ['mother_working','mother_work_job_id','mother_work_location'];
        $guardian_work = ['guardian_working','guardian_work_job_id','guardian_work_location'];
        $father_education = ['father_grade', 'father_authority','father_type','father_stage','father_level','father_year','father_school','father_points'];
        $mother_education = ['mother_grade', 'mother_authority','mother_type','mother_stage','mother_level','mother_year','mother_school','mother_points'];
        $guardian_education = ['guardian_grade', 'guardian_authority','guardian_type','guardian_stage','guardian_level','guardian_year','guardian_school','guardian_points'];
        $guardian_cont1=['guardian_primary_mobile'];
        $guardian_cont2=['guardian_secondary_mobile'];
        $guardian_cont3=['guardian_phone'];


        $char_persons_health_cnt =
        $char_persons_work_cnt  =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt =
        $primary_num_cnt = $secondary_num_cnt =  $phone_num_cnt = $reconstruct_cnt=
        $char_persons_islamic_commitment_cnt =
        $char_organization_persons_banks_cnt=
        $char_persons_banks_cnt =
        $char_persons_father_cnt =
        $char_persons_mother_cnt =
        $char_persons_guardian_cnt =
        $guardian_residence_cnt =
        $mother_health_cnt =
        $father_health_cnt =
        $father_work_cnt    =
        $mother_work_cnt  =
        $guardian_work_cnt =
        $father_education_cnt =
        $mother_education_cnt =
        $guardian_education_cnt =
        $guardian_cont1_cnt =
        $guardian_cont2_cnt =
        $guardian_cont3_cnt = 0;

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    if($key =='case_status'){
                        $data = ['char_cases.status'=> $value];
                    }else{
                        $data = ['char_cases.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $reconstructions)) {
                if ( $value != "" ) {
                    if($key =='reconstructions_organization_name'){
                        $data = ['char_reconstructions.organization_name'=> $value];
                    }else{
                        $data = ['char_reconstructions.' . $key => $value];
                    }
                    $reconstruct_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_i18n)) {
                if ( $value != "" ) {
                    $data = ['char_persons_i18n.' . substr($key,3)=>$value ];
                    $char_persons_i18n_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_education)) {
                if ( $value != "" ) {
                    $data = ['char_persons_education.' . $key => $value];
                    $char_persons_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_islamic_commitment)) {
                if ( $value != "" ) {
                    $data = ['char_persons_islamic_commitment.' . $key => $value];
                    $char_persons_islamic_commitment_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $phone)) {
                if ( $value != "" ) {
                    $data = ['phone_num.contact_value' => $value]; $phone_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $primary_mobile)) {
                if ( $value != "" ) {
                    $data = ['primary_num.contact_value' => $value];
                    $primary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $secondary_mobile)) {
                if ( $value != "" ) {
                    $data = ['secondary_num.contact_value' => $value];
                    $secondary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_organization_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_organization_persons_banks.'. $key => $value];
                    $char_organization_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_persons_banks.'. $key => $value];
                    $char_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    if($key =="father_alive"){
                        $data = ['f.death_date'  => $value];
                    }else{
                        $data = ['f.' . substr($key, 7) => $value];
                    }
                    $char_persons_father_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    if($key =="mother_alive"){
                        $data = ['m.death_date'  => $value];
                    }else{
                        $data = ['m.' . substr($key, 7) => $value];
                    }
                    $char_persons_mother_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 9) => $value];
                    $char_persons_guardian_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_residence)) {
                if ( $value != "" ) {
                    $data = ['guardian_residence.' . substr($key, 9) => $value];
                    $guardian_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$mother_health)) {
                if ( $value != "" ) {
                    $data = ['mother_health.' . substr($key, 7) => $value];
                    $mother_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$father_health)) {
                if ( $value != "" ) {
                    $data = ['father_health.' . substr($key, 7) => $value];
                    $father_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_work)) {

                if ( $value != "" ) {
                    $father_work_cnt   ++;
                    $data = ['father_work.' . substr($key, 7) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_work)) {

                if ( $value != "" ) {
                    $data = ['mother_work.' . substr($key, 7) => $value];
                    $mother_work_cnt ++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_work)) {

                if ( $value != "" ) {
                    $data = ['guardian_work.' . substr($key, 9) => $value];
                    $guardian_work_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_education)) {

                if ( $value != "" ) {
                    $data = ['father_education.' . substr($key, 7) => $value];
                    $father_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_education)) {

                if ( $value != "" ) {
                    $data = ['mother_education.' . substr($key, 7) => $value];
                    $mother_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_education)) {

                if ( $value != "" ) {
                    $data = ['guardian_education.' . substr($key, 9) => $value];
                    $guardian_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont1)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont1.contact_value' => $value];
                    $guardian_cont1_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont2)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont2.contact_value' => $value];
                    $guardian_cont2_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont3)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont3.contact_value' => $value];
                    $guardian_cont3_cnt++;
                    array_push($condition, $data);
                }
            }

        }

        $result = \DB::table('char_cases')
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q) use($category_type){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',$category_type);
            })
            ->join('char_persons', 'char_persons.id', '=', 'char_cases.person_id')
            ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                $q->where('char_marital_status.language_id',$language_id);
            })
            ->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                $q->on('primary_num.person_id', '=', 'char_persons.id');
                $q->where('primary_num.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as wataniya_num', function($q) use ($language_id){
                $q->on('wataniya_num.person_id', '=', 'char_persons.id');
                $q->where('wataniya_num.contact_type','wataniya');
            })
            ->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
                $q->on('secondary_num.person_id', '=', 'char_persons.id');
                $q->where('secondary_num.contact_type','secondary_mobile');
            })
            ->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                $q->on('phone_num.person_id', '=', 'char_persons.id');
                $q->where('phone_num.contact_type','phone');
            });

        if($category_type == 1){
            $result ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) use ($language_id){
                    $q->on('guardian_cont2.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont4', function($q) use ($language_id){
                    $q->on('guardian_cont4.person_id', '=', 'char_guardians.guardian_id');
                    $q->where('guardian_cont4.contact_type','wataniya');
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')

                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                });

            $result ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                $q->where('mosques_name.language_id',$language_id);
            })
                ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                    $q->on('L1.location_id', '=', 'char_persons.location_id');
                    $q->where('L1.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                    $q->on('L4.location_id', '=', 'char_persons.country');
                    $q->where('L4.language_id',$language_id);
                });
        }
        else{
            $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })

                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                });
        }


        if($char_persons_i18n_cnt != 0){
            $result->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id');
        }
        if($char_residence_cnt != 0) {
            $result->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
        }
        if($char_persons_work_cnt != 0){
            $result ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
        }
        if($char_persons_health_cnt!= 0 ){
            $result->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
        }
        if($char_persons_education_cnt!= 0 ){
            $result->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
        }
        if($char_persons_islamic_commitment_cnt!= 0  || isset($filters['save_quran'])){
            $result->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id','=','char_persons.id');
        }

        if($char_organization_persons_banks_cnt!= 0  || $char_persons_banks_cnt != 0) {
            $result->leftjoin('char_organization_persons_banks', function ($q) {
                $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
            })
                ->leftjoin('char_persons_banks', function ($q) {
                    $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                });

        }

        if($category_type == 2){
            if($reconstruct_cnt != 0){
                $result->leftjoin('char_reconstructions', function($q){
                    $q->on('char_reconstructions.case_id', '=', 'char_cases.id');
                });
            }
        }
        else{

            $result->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                $q->on('guardian_cont1.person_id', '=', 'g.id');
                $q->where('guardian_cont1.contact_type','primary_mobile');
            })      ;

            $result->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
                $q->on('guardian_cont3.person_id', '=', 'g.id');
                $q->where('guardian_cont3.contact_type','phone');
            })        ;


            if($guardian_residence_cnt!= 0 ){
                $result->leftjoin('char_residence as guardian_residence','guardian_residence.person_id','=','g.id')   ;
            }

            if($guardian_education_cnt!= 0 ){
                $result->leftjoin('char_persons_education as guardian_education','guardian_education.person_id','=','g.id');
            }
            if($guardian_work_cnt!= 0 ){
                $result->leftjoin('char_persons_work as guardian_work','guardian_work.person_id','=','g.id')  ;
            }

            if($mother_health_cnt!= 0 ){
                $result->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id');
            }
            if($mother_work_cnt != 0 ){
                $result->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id');
            }
            if($mother_education_cnt!= 0 ){
                $result->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id');
            }

            if($father_health_cnt!= 0 ){
                $result->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id');
            }
            if($father_work_cnt   != 0 ){
                $result->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id');
            }
            if($father_education_cnt!= 0 ){
                $result->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id');
            }
        }


        if(isset($filters['deleted']) && $filters['deleted'] !=null && $filters['deleted'] !=""){
            if($filters['deleted']==false){
                $result= $result->whereNull('char_cases.deleted_at');
            }else
                if($filters['deleted']==true) {
                    $result= $result->whereNotNull('char_cases.deleted_at');
                }
        }else{
            $result= $result->whereNull('char_cases.deleted_at');
        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name',
                        'char_persons.third_name', 'char_persons.last_name',
                        'char_persons_i18n.first_name', 'char_persons_i18n.second_name', 'char_persons_i18n.third_name',
                        'char_persons_i18n.last_name','char_residence.repair_notes',
                        'char_cases.visitor','char_cases.notes','char_cases.visitor_card','char_cases.visitor_opinion',
                        'char_persons_work.work_location','char_persons_banks.account_owner',
                        'm.first_name', 'm.second_name', 'm.third_name', 'm.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name',
                        'char_persons_health.health_insurance_number','char_persons_health.used_device_name',
                        'char_persons_health.gov_health_details',
                        'char_persons.gov_commercial_records_details','char_residence.repair_notes',
                        'char_persons_work.work_location', 'char_persons_work.gov_work_details' ,
                        'char_persons_health.used_device_name', 'char_persons_health.gov_health_details'
                    ];


                    $live = ['m.death_date','f.death_date'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else if(in_array($key, $live)){
                                if($value == 0){
                                    $q->where($key, '=', null);
                                }else{
                                    $q->where($key, '!=', null);
                                }
                            }else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true || $filters['all_organization'] ==='true'){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false || $filters['all_organization'] ==='false'){
                $all_organization=1;
            }
        }

        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null &&
                $filters['organization_ids'] !="" && $filters['organization_ids'] !=[]) {

                if(is_array($filters['organization_ids'])){
                    if(sizeof($filters['organization_ids']) != 0){
                        if($filters['organization_ids'][0]==""){
                            unset($filters['organization_ids'][0]);
                        }
                        $organizations =$filters['organization_ids'];
                    }
                }
            }

            $organization_category=[];
            if(isset($filters['organization_category']) && $filters['organization_category'] !=null &&
                $filters['organization_category'] !="" && $filters['organization_category'] !=[] && sizeof($filters['organization_category']) != 0) {
                if($filters['organization_category'][0]==""){
                    unset($filters['organization_category'][0]);
                }
                $organization_category = $filters['organization_category'];
            }

            if (!empty($organizations)) {
                $result->where(function ($anq) use ($organizations,$organization_category) {
                    $anq->wherein('char_cases.organization_id', $organizations);
                    if(!empty($organization_category)){
                        $anq->wherein('org.category_id',$organization_category);
                    }
                });
            }
            else{

                if($UserType == 2) {
                    $result->where(function ($anq) use ($organization_id,$user,$organization_category) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });

                }else{

                    $result->where(function ($anq) use ($organization_id,$organization_category) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });
                }
            }
        }
        elseif($all_organization ==1){
            if(isset($filters['organizationsCases']) && !empty($filters['organizationsCases']) && $filters['organizationsCases'])
                $result= $result->where('char_cases.organization_id','!=',$organization_id);
            else
                $result= $result->where('char_cases.organization_id',$organization_id);
        }

        if(isset($filters['save_quran']) && ($filters['save_quran'] !=null || $filters['save_quran'] !="")){
//            'quran_parts', 'quran_chapters'
            if($filters['save_quran'] == 1){
                $result= $result->whereNotNull('char_persons_islamic_commitment.quran_parts');
            }else{
                $result->whereNull('char_persons_islamic_commitment.quran_parts');
            }
        }

        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

            $birthday_to=null;
            $birthday_from=null;

            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $result->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $result->whereDate('char_persons.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $result->whereDate('char_persons.birthday', '<=', $birthday_to);
            }

        }
        if((isset($filters['deleted_to']) && $filters['deleted_to'] !=null) || (isset($filters['deleted_from']) && $filters['deleted_from'] !=null)){

            $deleted_to=null;
            $deleted_from=null;

            if(isset($filters['deleted_to']) && $filters['deleted_to'] !=null){
                $deleted_to=date('Y-m-d',strtotime($filters['deleted_to']));
            }
            if(isset($filters['deleted_from']) && $filters['deleted_from'] !=null){
                $deleted_from=date('Y-m-d',strtotime($filters['deleted_from']));
            }
            if($deleted_from != null && $deleted_to != null) {
                $result->whereBetween( 'char_cases.deleted_at', [ $deleted_from, $deleted_to]);
            }elseif($deleted_from != null && $deleted_to == null) {
                $result->whereDate('char_cases.deleted_at', '>=', $deleted_from);
            }elseif($deleted_from == null && $deleted_to != null) {
                $result->whereDate('char_cases.deleted_at', '<=', $deleted_to);
            }


        }
        if((isset($filters['date_to']) && $filters['date_to'] !=null) || (isset($filters['date_from']) && $filters['date_from'] !=null)){

            $date_to=null;
            $date_from=null;

            if(isset($filters['date_to']) && $filters['date_to'] !=null){
                $date_to=date('Y-m-d',strtotime($filters['date_to']));
            }
            if(isset($filters['date_from']) && $filters['date_from'] !=null){
                $date_from=date('Y-m-d',strtotime($filters['date_from']));
            }
            if($date_from != null && $date_to != null) {
                $result->whereBetween( 'char_cases.created_at', [ $date_from, $date_to]);
            }elseif($date_from != null && $date_to == null) {
                $result->whereDate('char_cases.created_at', '>=', $date_from);
            }elseif($date_from == null && $date_to != null) {
                $result->whereDate('char_cases.created_at', '<=', $date_to);
            }


        }
        if((isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null) || (isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null)){
            $visited_at_to=null;
            $visited_at_from=null;

            if(isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null){
                $visited_at_to=date('Y-m-d',strtotime($filters['visited_at_to']));
            }
            if(isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null){
                $visited_at_from=date('Y-m-d',strtotime($filters['visited_at_from']));
            }

            if($visited_at_from != null && $visited_at_to != null) {
                $result->whereBetween( 'char_cases.visited_at', [ $visited_at_from, $visited_at_to]);
            }elseif($visited_at_from != null && $visited_at_to == null) {
                $result->whereDate('char_cases.visited_at', '>=', $visited_at_from);
            }elseif($visited_at_from == null && $visited_at_to != null) {
                $result->whereDate('char_cases.visited_at', '<=', $visited_at_to);
            }

        }
        if((isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !="")|| (isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !="")){

            $max_rank=null;
            $min_rank=null;

            if(isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !=""){
                $max_rank=$filters['max_rank'];
            }
            if(isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !=""){
                $min_rank=$filters['min_rank'];
            }

            if($min_rank != null && $max_rank != null) {
                $result->whereRaw(" ( char_cases.rank between ? and  ?)", array($min_rank, $max_rank));
            }
            elseif($max_rank != null && $min_rank == null) {
                $result->whereRaw(" $max_rank >= char_cases.rank");
            }
            elseif($max_rank == null && $min_rank != null) {
                $result->whereRaw(" $min_rank <= char_cases.rank");
            }
        }
        if((isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !="")||
            (isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !="")){

            $monthly_income_from=null;
            $monthly_income_to=null;
            if(isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !=""){
                $monthly_income_to=$filters['monthly_income_to'];
            }
            if(isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !=""){
                $monthly_income_from=$filters['monthly_income_from'];
            }

            if($monthly_income_from != null && $monthly_income_to != null) {
                $result->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
                $result->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
            elseif($monthly_income_to != null && $monthly_income_from == null) {
                $result->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
            }
            elseif($monthly_income_to == null && $monthly_income_from != null) {
                $result->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
        }

        if($category_type !=1) {
            if((isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !="")||
                (isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !="")){

                $max_family_count=null;
                $min_family_count=null;

                if(isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !=""){
                    $max_family_count=$filters['max_family_count'];
                }
                if(isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !=""){
                    $min_family_count=$filters['min_family_count'];
                }
                if($min_family_count != null && $max_family_count != null) {
                    $result = $result->whereRaw(" ( char_persons.family_cnt between ? and  ?)", array($min_family_count, $max_family_count));
                }elseif($max_family_count != null && $min_family_count == null) {
                    $result = $result->whereRaw(" $max_family_count >= char_persons.family_cnt");
                }elseif($max_family_count == null && $min_family_count != null) {
                    $result = $result->whereRaw(" $min_family_count <= char_persons.family_cnt");
                }
            }

              if(isset($filters['Properties'])) {
                for ($x = 0; $x < sizeof($filters['Properties']); $x++) {
                    $item=$filters['Properties'][$x];
                    if(isset($item['exist'])){
                        if($item['exist'] != "") {
                            $has_property=(int)$item['exist'];
                            
                            if($has_property == 1){
                                $op = '=';
                                $has_property_val = 1;

                                $result->whereNotIn('char_cases.person_id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_properties')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('has_property', 1)
                                                       ->where('property_id', $item['id']);
                                             });
                                   });
                            }else{
                                $op = '!=';
                                $has_property_val = 1;

                                $result->whereNotIn('char_cases.person_id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_properties')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('has_property', 1)
                                                       ->where('property_id', $item['id']);
                                             });
                                   });
                            }
                            
//                            $id=(int)$item['id'];
//                            $table='char_persons_properties as prop'.$x;
//                            $result=$result ->join($table, function($q) use($x,$id,$has_property,$op,$has_property_val){
//                                $q->on('prop'.$x.'.person_id', '=', 'char_persons.id');
//                                $q->where('prop'.$x.'.property_id', '=', $id);
//                                $q->where('prop'.$x.'.has_property', '=', $has_property_val);
//                            });
                        }
                    }

                }
            }
            if(isset($filters['AidSources'])) {
                for ($x = 0; $x < sizeof($filters['AidSources']); $x++) {
                    $item=$filters['AidSources'][$x];
                    if(isset($item['aid_take'])){
                        if($item['aid_take'] != "") {
                            $aid_take=(int)$item['aid_take'];
                            if($aid_take == 1){
                                $op = '=';
                                $aid_take_val = 1;
                                $result->whereIn('char_persons.id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_aids')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('aid_take','=', 1)
                                                       ->where('aid_source_id', $item['id']);
                                             });
                                   });
                                
                            }else{
                                $op = '!=';
                                $aid_take_val = 1;  
                                 $result->whereNotIn('char_persons.id', function ($query) use ($item) {
                                       $query->select('person_id')
                                             ->from('char_persons_aids')
                                             ->where(function ($sq) use ($item){
                                                   $sq->where('aid_take','=', 1)
                                                       ->where('aid_source_id', $item['id']);
                                             });
                                   });
                                
                            }
//                            $id=(int)$item['id'];
//                            $table='char_persons_aids as aid'.$x;
//                            $result=$result ->join($table, function($q) use($x,$id,$aid_take,$op,$aid_take_val){
//                                $q->on('aid'.$x.'.person_id', '=', 'char_persons.id');
//                                $q->where('aid'.$x.'.aid_source_id', '=', $id);
//                                $q->where('aid'.$x.'.aid_take', $op, $aid_take_val);
//                            });
                        }
                    }

                }
            }
            if(isset($filters['Essential'])) {
                $custom=[];
                foreach($filters['Essential'] as $item){
                    if((isset($item['max']) && $item['max'] !=null && $item['max'] !="" ) ||
                        isset($item['min']) && $item['min'] !=null && $item['min'] !="" ){
                        array_push($custom,$item);
                    }
                }

                foreach($custom as $k => $value) {
                    $max=null;
                    $min=null;
                    $id=(int)$value['id'];
                    $table='char_cases_essentials as essent'.$k;
                    $need='essent'.$k.'.needs';

                    if(isset($value['max']) && $value['max'] !=null && $value['max'] !=""){
                        $max=(int)$value['max'];
                    }
                    if(isset($value['min']) && $value['min'] !=null && $value['min'] !=""){
                        $min=(int)$value['min'];
                    }

                    $result=$result ->join($table, function($q) use($k,$id,$min,$max) {
                        $q->on('essent' . $k . '.case_id', '=', 'char_cases.id');
                        $q->where('essent' . $k . '.essential_id', '=', $id);

                        if($min != null && $max != null) {
                            $q->where('essent'.$k.'.needs', '>=', $min);
                            $q->where('essent'.$k.'.needs', '<=', $max);
                        }elseif($max == null && $min != null) {
                            $q->where('essent'.$k.'.needs', '>=', $min);
                        }elseif($max != null && $min == null) {
                            $q->where('essent'.$k.'.needs', '<=', $max);
                        }
                    });
                }
            }


            if((isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !="")||
                (isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !="")){

                $max_total_vouchers=null;
                $min_total_vouchers=null;
                if(isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !=""){
                    $max_total_vouchers=$filters['max_total_vouchers'];
                }
                if(isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !=""){
                    $min_total_vouchers=$filters['min_total_vouchers'];
                }

                if($min_total_vouchers != null && $max_total_vouchers != null) {
                    $result->whereRaw(" ( char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') between ? and  ?)", array($min_total_vouchers, $max_total_vouchers));
                }
                elseif($max_total_vouchers != null && $min_total_vouchers == null) {
                    $result->whereRaw(" $max_total_vouchers >= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
                }
                elseif($max_total_vouchers == null && $min_total_vouchers != null) {
                    $result->whereRaw(" $min_total_vouchers <= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
                }
            }

            if(isset($filters['voucher_sponsors']) || isset($filters['voucher_organization']) || isset($filters['voucher_ids']) ||
                isset($filters['got_voucher']) || isset($filters['voucher_to']) || isset($filters['voucher_from'])){

                $voucher_organization=[];
//                if($master == true){
                    if(isset($filters['voucher_organization']) && $filters['voucher_organization'] !=null &&
                        $filters['voucher_organization'] !="" && $filters['voucher_organization'] !=[] && sizeof($filters['voucher_organization'] )!= 0) {
                        if($filters['voucher_organization'][0]==""){
                            unset($filters['voucher_organization'][0]);
                        }
                        $voucher_organization =$filters['voucher_organization'];
                    }
//                }else{
//                    $voucher_organization =[$organization_id];
//                }

                $voucher_sponsors=[];
                if(isset($filters['voucher_sponsors']) && $filters['voucher_sponsors'] !=null && $filters['voucher_sponsors'] !="" &&
                    $filters['voucher_sponsors'] !=[] && sizeof($filters['voucher_sponsors'] ) != 0) {
                    if($filters['voucher_sponsors'][0]==""){
                        unset($filters['voucher_sponsors'][0]);
                    }
                    $voucher_sponsors =$filters['voucher_sponsors'];
                }

                $voucher_ids=[];
                if(isset($filters['voucher_ids']) && $filters['voucher_ids'] !=null && $filters['voucher_ids'] !="" &&
                    $filters['voucher_ids'] !=[] && sizeof($filters['voucher_ids'] )!= 0) {
                    if($filters['voucher_ids'][0]==""){
                        unset($filters['voucher_ids'][0]);
                    }
                    $voucher_ids =$filters['voucher_ids'];
                }

                $voucher_to=null;
                $voucher_from=null;
                if(isset($filters['voucher_to']) && $filters['voucher_to'] !=null){
                    $voucher_to=date('Y-m-d',strtotime($filters['voucher_to']));
                }
                if(isset($filters['voucher_from']) && $filters['voucher_from'] !=null){
                    $voucher_from=date('Y-m-d',strtotime($filters['voucher_from']));
                }
                if(isset($filters['got_voucher']) && $filters['got_voucher'] !=null  && $filters['got_voucher'] !="") {
                    if($filters['got_voucher']==0 ){
                        $result->whereNotIn('char_persons.id', function($query)use($voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                            $query->select('person_id')->from('char_vouchers_persons');

                            if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0 ){

                                $query->join('char_vouchers', function($q) use($voucher_organization,$voucher_ids,$voucher_sponsors){
                                    $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                                    $q->whereNull('char_vouchers.deleted_at');

                                    if(sizeof($voucher_organization) !=0){
                                        $q->wherein('char_vouchers.organization_id', $voucher_organization);
                                    }
                                    if(sizeof($voucher_sponsors) !=0){
                                        $q->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                    }

                                    if(sizeof($voucher_ids) !=0){
                                        $q->wherein('char_vouchers.id', $voucher_ids);
                                    }

                                });
                            }


                            if($voucher_from != null && $voucher_to != null) {
                                $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                            }elseif($voucher_from != null && $voucher_to == null) {
                                $query->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                            }elseif($voucher_from == null && $voucher_to != null) {
                                $query->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                            }




                        });
                    }else{
                        $result->whereIn('char_persons.id', function($query) use($voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                            $query->select('person_id')->from('char_vouchers_persons');

                            if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0){

                                $query->join('char_vouchers', function($q) use($voucher_organization,$voucher_ids,$voucher_sponsors){
                                    $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                                    $q->whereNull('char_vouchers.deleted_at');

                                    if(sizeof($voucher_organization) !=0){
                                        $q->wherein('char_vouchers.organization_id', $voucher_organization);
                                    }
                                    if(sizeof($voucher_sponsors) !=0){
                                        $q->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                    }

                                    if(sizeof($voucher_ids) !=0){
                                        $q->wherein('char_vouchers.id', $voucher_ids);
                                    }

                                });
                            }
                            if($voucher_from != null && $voucher_to != null) {
                                $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                            }elseif($voucher_from != null && $voucher_to == null) {
                                $query->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                            }elseif($voucher_from == null && $voucher_to != null) {
                                $query->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                            }
                        });
                    }
                }
            }

        }
        else{


            if((isset($filters['max_brother_number']) && $filters['max_brother_number'] !=null && $filters['max_brother_number'] !="")||
                (isset($filters['min_brother_number']) && $filters['min_brother_number'] !=null && $filters['min_brother_number'] !="")){

                $max_brother_number=null;
                $min_brother_number=null;

                if(isset($filters['max_brother_number']) && $filters['max_brother_number'] !=null && $filters['max_brother_number'] !=""){
                    $max_brother_number=$filters['max_brother_number'];
                }
                if(isset($filters['min_brother_number']) && $filters['min_brother_number'] !=null && $filters['min_brother_number'] !=""){
                    $min_brother_number=$filters['min_brother_number'];
                }

                if($min_brother_number != null && $max_brother_number != null) {
                    $result->whereRaw(' char_categories.case_is = 1 and char_categories.family_structure= 2 and ( char_get_family_count ( "left",char_persons.father_id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                    $result->orwhereRaw(' char_categories.case_is = 1 and char_categories.family_structure=1 and ( char_get_family_count ( "left",char_persons.id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                    $result->orwhereRaw(' char_categories.case_is = 2 and char_categories.family_structure= 2 and ( char_get_family_count ( "left",char_guardians.guardian_id,null,char_persons.id ) between ? and  ?)', array($min_brother_number, $max_brother_number));
                }elseif($max_brother_number != null && $min_brother_number == null) {
                    $result->whereRaw(" char_categories.case_is = 1 and char_categories.family_structure= 2 and ( $max_brother_number >= char_get_family_count('left',char_persons.father_id,null,char_persons.id))");
                    $result->orwhereRaw(" char_categories.case_is = 1 and char_categories.family_structure=1 and ( $max_brother_number >= char_get_family_count('left',char_persons.id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 2 and char_categories.family_structure= 2 and ( $max_brother_number >= char_get_family_count('left',char_guardians.guardian_id,null,char_persons.id ))");

                }elseif($max_brother_number == null && $min_brother_number != null) {
                    $result->whereRaw(" char_categories.case_is = 1 and char_categories.family_structure= 2 and ( $min_brother_number <= char_get_family_count('left',char_persons.father_id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 1 and char_categories.family_structure=1 and ( $min_brother_number <= char_get_family_count('left',char_persons.id,null,char_persons.id ))");
                    $result->orwhereRaw(" char_categories.case_is = 2 and char_categories.family_structure= 2 and ( $min_brother_number <= char_get_family_count('left',char_guardians.guardian_id,null,char_persons.id ))");
                }
            }
            if((isset($filters['max_monthy_payments_average']) && $filters['max_monthy_payments_average'] !=null && $filters['max_monthy_payments_average'] !="")||
                (isset($filters['min_monthy_payments_average']) && $filters['min_monthy_payments_average'] !=null && $filters['min_monthy_payments_average'] !="")){
                $max_monthy_payments_average=null;
                $min_monthy_payments_average=null;
                if(isset($filters['max_monthy_payments_average']) && $filters['max_monthy_payments_average'] !=null && $filters['max_monthy_payments_average'] !=""){
                    $max_monthy_payments_average=$filters['max_monthy_payments_average'];
                }
                if(isset($filters['min_monthy_payments_average']) && $filters['min_monthy_payments_average'] !=null && $filters['min_monthy_payments_average'] !=""){
                    $min_monthy_payments_average=$filters['min_monthy_payments_average'];
                }

                if($min_monthy_payments_average != null && $max_monthy_payments_average != null) {
                    $result->whereRaw(" ( char_get_person_average_payments ( c.id ,'$first', '$last') between ? and  ?)", array($min_monthy_payments_average, $max_monthy_payments_average));
                }elseif($max_monthy_payments_average != null && $min_monthy_payments_average == null) {
                    $result->whereRaw(" $max_monthy_payments_average >= char_get_person_average_payments ( c.id ,'$first', '$last')");
                }elseif($max_monthy_payments_average == null && $min_monthy_payments_average != null) {
                    $result->whereRaw(" $min_monthy_payments_average <= char_get_person_average_payments ( c.id ,'$first', '$last')");
                }

            }

            if((isset($filters['max_total_amount_of_payments']) && $filters['max_total_amount_of_payments'] !=null && $filters['max_total_amount_of_payments'] !="") ||
                (isset($filters['min_total_amount_of_payments']) && $filters['min_total_amount_of_payments'] !=null && $filters['min_total_amount_of_payments'] !="")){


                $max_total_amount_of_payments=null;
                $min_total_amount_of_payments=null;
                if(isset($filters['max_total_amount_of_payments']) && $filters['max_total_amount_of_payments'] !=null && $filters['max_total_amount_of_payments'] !=""){
                    $max_total_amount_of_payments=$filters['max_total_amount_of_payments'];
                }
                if(isset($filters['min_total_amount_of_payments']) && $filters['min_total_amount_of_payments'] !=null && $filters['min_total_amount_of_payments'] !=""){
                    $min_total_amount_of_payments=$filters['min_total_amount_of_payments'];
                }

                if($min_total_amount_of_payments != null && $max_total_amount_of_payments != null) {
                    $result->whereRaw(" ( char_get_person_total_payments ( c.id ,'$first', '$last') between ? and  ?)", array($min_total_amount_of_payments, $max_total_amount_of_payments));
                }
                elseif($max_total_amount_of_payments != null && $min_total_amount_of_payments == null) {
                    $result->whereRaw(" $max_total_amount_of_payments >= char_get_person_total_payments ( c.id ,'$first', '$last')");
                }
                elseif($max_total_amount_of_payments == null && $min_total_amount_of_payments != null) {
                    $result->whereRaw(" $min_total_amount_of_payments <= char_get_person_total_payments ( c.id ,'$first', '$last')");
                }

            }


            $sponsor=[];
            if(isset($filters['sponsor']) && $filters['sponsor'] !=null && $filters['sponsor'] !=[] && sizeof($filters['sponsor']) != 0) {
                if($filters['sponsor'][0]==""){
                    unset($filters['sponsor'][0]);
                }
                $sponsor =$filters['sponsor'];
            }
            if(isset($filters['guaranteed_status']) && $filters['guaranteed_status'] !=null && $filters['guaranteed_status'] !=""){
                $guaranteed_status=$filters['guaranteed_status'];

                if(in_array($guaranteed_status,[1,2,3,4,5])){
                    $result->whereIn('char_cases.id', function($query)use($guaranteed_status,$sponsor){
                        if(sizeof($sponsor) ==0){
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status', $guaranteed_status);

                        }else{
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status', $guaranteed_status)
                                ->wherein('char_sponsorship_cases.sponsor_id', $sponsor);
                        }
                    });
                }
                if(in_array($guaranteed_status,[-1,-2,-3,-4,-5])){
                    $guaranteed_status=$guaranteed_status * -1;
                    $result->whereNotIn('char_cases.id', function($query)use($guaranteed_status,$sponsor){
                        if(sizeof($sponsor) ==0){
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status' ,'!=',$guaranteed_status);

                        }else{
                            $query->
                            select('case_id')
                                ->from('char_sponsorship_cases')
                                ->where('char_sponsorship_cases.status','!=', $guaranteed_status)
                                ->wherein('char_sponsorship_cases.sponsor_id', $sponsor);
                        }
                    });
                }
            }

            if((isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null) ||
                (isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null)) {

                $mother_birthday_to=null;
                $mother_birthday_from=null;

                if(isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null){
                    $mother_birthday_to=date('Y-m-d',strtotime($filters['mother_birthday_to']));
                }
                if(isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null){
                    $mother_birthday_from=date('Y-m-d',strtotime($filters['mother_birthday_from']));
                }

                if($mother_birthday_from != null && $mother_birthday_to != null) {
                    $result->whereBetween( 'm.birthday', [ $mother_birthday_from, $mother_birthday_to]);
                }elseif($mother_birthday_from != null && $mother_birthday_to == null) {
                    $result->whereDate('m.birthday', '>=', $mother_birthday_from);
                }elseif($mother_birthday_from == null && $mother_birthday_to != null) {
                    $result->whereDate('m.birthday', '<=', $mother_birthday_to);
                }

            }

            if((isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null) ||
                (isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null)) {
                $mother_death_date_to=null;
                $mother_death_date_from=null;

                if(isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null){
                    $mother_death_date_to=date('Y-m-d',strtotime($filters['mother_death_date_to']));
                }
                if(isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null){
                    $mother_death_date_from=date('Y-m-d',strtotime($filters['mother_death_date_from']));
                }
                if($mother_death_date_from != null && $mother_death_date_to != null) {
                    $result->whereBetween( 'm.death_date', [ $mother_death_date_from, $mother_death_date_to]);
                }elseif($mother_death_date_from != null && $mother_death_date_to == null) {
                    $result->whereDate('m.death_date', '>=', $mother_death_date_from);
                }elseif($mother_death_date_from == null && $mother_death_date_to != null) {
                    $result->whereDate('m.death_date', '<=', $mother_death_date_to);
                }
            }

            if((isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null) ||
                (isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null)) {

                $father_death_date_to=null;
                $father_death_date_from=null;

                if(isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null){
                    $father_death_date_to=date('Y-m-d',strtotime($filters['father_death_date_to']));
                }
                if(isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null){
                    $father_death_date_from=date('Y-m-d',strtotime($filters['father_death_date_from']));
                }
                if($father_death_date_from != null && $father_death_date_to != null) {
                    $result->whereBetween( 'f.death_date', [ $father_death_date_from, $father_death_date_to]);
                }elseif($father_death_date_from != null && $father_death_date_to == null) {
                    $result->whereDate('f.death_date', '>=', $father_death_date_from);
                }elseif($father_death_date_from == null && $father_death_date_to != null) {
                    $result->whereDate('f.death_date', '<=', $father_death_date_to);
                }
            }

            if((isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null)||
                (isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null)){

                $father_birthday_to=null;
                $father_birthday_from=null;

                if(isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null){
                    $father_birthday_to=date('Y-m-d',strtotime($filters['father_birthday_to']));
                }
                if(isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null){
                    $father_birthday_from=date('Y-m-d',strtotime($filters['father_birthday_from']));
                }

                if($father_birthday_from != null && $father_birthday_to != null) {
                    $result->whereBetween( 'f.birthday', [ $father_birthday_from, $father_birthday_to]);
                }
                elseif($father_birthday_from != null && $father_birthday_to == null) {
                    $result->whereDate('f.birthday', '>=', $father_birthday_from);
                }
                elseif($father_birthday_from == null && $father_birthday_to != null) {
                    $result->whereDate('f.birthday', '<=', $father_birthday_to);
                }

            }

        }

        
        if(isset($filters['persons'])){
            if(sizeof($filters['persons']) > 0 ){
               $result->whereIn('char_cases.id',$filters['persons']);
            } 
       }
        $map = [
            "organization_name" => 'org.name',
            "name" => 'char_persons.first_name',
            "guardian_name" => 'g.first_name',
            "id_card_number" => 'char_persons.id_card_number',
            "gender" => 'char_persons.gender',
            "case_gender" => 'char_persons.gender',

            "marital_status" => 'char_marital_status.name',

            "category_name" => 'char_categories.name',
            "rank" => 'char_cases.rank',
            "status" => 'char_cases.status',
            "cases_status" => 'char_cases.status',
            "visitor" => 'char_cases.visitor',
            "delete_reason" => 'char_cases.reason',
            "deleted_at" => 'char_cases.reason',

        ];

        if($category_type == 2){
            $map['primary_mobile'] = 'primary_num.contact_value';
            $map['region_name'] = 'region_name.name';
            $map['governarate'] = 'district_name.name';
        }else{
            $map['primary_mobile'] = 'guardian_cont2.contact_value';
            $map['region_name'] = 'L2.name';
            $map['governarate'] = 'L3.name';
        }

        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','desc');
                    $result->orderBy('char_persons.second_name','desc');
                    $result->orderBy('char_persons.third_name','desc');
                    $result->orderBy('char_persons.last_name','desc');
                }elseif($key_ == 'guardian_name'){
                    $result->orderBy('g.first_name','desc');
                    $result->orderBy('g.second_name','desc');
                    $result->orderBy('g.third_name','desc');
                    $result->orderBy('g.last_name','desc');
                }else{
                    $result->orderBy($map[$key_],'desc');
                }
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','asc');
                    $result->orderBy('char_persons.second_name','asc');
                    $result->orderBy('char_persons.third_name','asc');
                    $result->orderBy('char_persons.last_name','asc');
                }elseif($key_ == 'guardian_name'){
                    $result->orderBy('g.first_name','asc');
                    $result->orderBy('g.second_name','asc');
                    $result->orderBy('g.third_name','asc');
                    $result->orderBy('g.last_name','asc');
                }else{
                    $result->orderBy($map[$key_],'asc');
                }
            }
        }


        if(!$order){
            $result->orderBy('char_cases.rank','char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');
        }

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $result->selectRaw("char_cases.id as case_id,
                                 char_persons.id as person_id,
                                 char_cases.created_at as created_at_case,
                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                 org.name as organization_name,
                                 CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                 CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                 CASE
                                           WHEN char_persons.card_type is null THEN ' '
                                           WHEN char_persons.card_type = 1 THEN '$id_card'
                                           WHEN char_persons.card_type = 2 THEN '$id_number'
                                           WHEN char_persons.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type
                                ");
        if($category_type ==1) {
            $result->selectRaw("
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))  END  AS mother_name,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.id_card_number  END  AS mother_id_card_number,
                                 CASE WHEN char_persons.mother_id is null THEN ' ' Else m.old_id_card_number  END  AS mother_old_id_card_number,
                                 CASE
                                           WHEN m.card_type is null THEN ' '
                                           WHEN m.card_type = 1 THEN '$id_card'
                                           WHEN m.card_type = 2 THEN '$id_number'
                                           WHEN m.card_type = 3 THEN '$passport'
                                     END
                                 AS mother_card_type,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' '))  END  AS father_name,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.id_card_number  END  AS father_id_card_number,
                                 CASE WHEN char_persons.father_id is null THEN ' ' Else f.old_id_card_number  END  AS father_old_id_card_number,
                                  CASE
                                           WHEN f.card_type is null THEN ' '
                                           WHEN f.card_type = 1 THEN '$id_card'
                                           WHEN f.card_type = 2 THEN '$id_number'
                                           WHEN f.card_type = 3 THEN '$passport'
                                     END
                                     AS father_card_type,
                                     CASE WHEN g.id is null THEN ' ' Else CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END  AS guardian_name,
                                 CASE WHEN g.id is null THEN ' ' Else g.id_card_number  END  AS guardian_id_card_number,
                                 CASE WHEN g.id is null THEN ' ' Else g.old_id_card_number  END  AS guardian_old_id_card_number,
                                  CASE
                                           WHEN g.card_type is null THEN ' '
                                           WHEN g.card_type = 1 THEN '$id_card'
                                           WHEN g.card_type = 2 THEN '$id_number'
                                           WHEN g.card_type = 3 THEN '$passport'
                                     END
                                     AS guardian_card_type
                                ");
        }

        $result->selectRaw("CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                            CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  
                                     CASE
                                           WHEN char_persons.gender is null THEN ' '
                                           WHEN char_persons.gender = 1 THEN '$male'
                                           WHEN char_persons.gender = 2 THEN '$female'
                                     END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status_id ");

        if($category_type == 2) {
            $result->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted");
        }
        $result->selectRaw("               char_persons.family_cnt AS family_count,
                                           char_persons.spouses AS spouses,
                                           char_persons.male_live AS male_count,
                                           char_persons.female_live AS female_count
                                          ");
        if($category_type == 2){

            $result->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                        CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                        CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                        CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address");

            $result->selectRaw("CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS primary_mobile,
                                     CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS wataniya,
                                     CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS phone");


        }
        else{
            $result->selectRaw("CASE WHEN char_persons.country  is null THEN ' ' Else L4.name END   AS country,
                                        CASE WHEN char_persons.governarate  is null THEN ' ' Else L3.name END   AS governarate ,
                                        CASE WHEN char_persons.city  is null THEN ' ' Else L2.name END   AS region_name,
                                        CASE WHEN char_persons.location_id is null THEN ' ' Else L1.name END   AS nearLocation,
                                        CASE WHEN char_persons.mosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ',
                                                  ifnull(mosques_name.name,' '),' ',
                                       ifnull(char_persons.street_address,' '))
                                     AS address,
                                                   CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,                                                    
                                                     CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   AS guardian_phone,
                                                     CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                                     CASE WHEN guardian_cont4.contact_value is null THEN ' ' Else guardian_cont4.contact_value END   AS guardian_wataniya
                                                    ");
        }
        $result=$result->get();



        return array('statistic'=>$result, 'elements'=>[],
            'essentials'=>[] ,'properties'=>[] , 'aid_source'=>[]);
    }

    public static function getCaseVouchersForSponsor($filters,$organization_id)
    {
        $user = \Auth::user();
        
        $filters['sponsor_id'] = $user->organization_id;
        $char_vouchers=['voucher_type','title','content','notes','organization_id','category_id','currency_id'];
        $char_vouchers_persons=['voucher_id','receipt_location'];
        $char_cases=['category_id','case_id','person_id'];
        $condition=[];
        $char_cases_condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $char_cases_condition [] = ['char_cases.'. $key => $value];
                }
            }
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {

                    if($key == 'voucher_type'){
                        $key='type';
                    }
                    array_push($condition, ['v.' . $key => $value]);
                }
            }
            if(in_array($key, $char_vouchers_persons)) {
                if ( $value != "" ) {
                    array_push($condition, ['char_vouchers_persons.' . $key => $value]);
                }
            }
        }

        $query = \DB::table('char_persons')
            ->join('char_vouchers_persons', 'char_persons.id', '=', 'char_vouchers_persons.person_id')
            ->leftjoin('char_vouchers AS v', function($q){
                $q->on('v.id', '=', 'char_vouchers_persons.voucher_id');
                $q->whereNull('v.deleted_at');
            })
            ->leftjoin('char_vouchers_categories', 'v.category_id', '=', 'char_vouchers_categories.id')
            ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
            ->leftjoin('char_currencies', 'v.currency_id', '=', 'char_currencies.id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'v.transfer_company_id');


        if (count($char_cases_condition) != 0) {
            $query =  $query->whereIn('char_persons.id',function ($q) use ($char_cases_condition) {
                $q->select('person_id')->from('char_cases');

                for ($i = 0; $i < count($char_cases_condition); $i++) {
                    foreach ($char_cases_condition[$i] as $key => $value) {
                        $q->where($key, '=', $value);
                    }
                }
            });
        }


        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['v.content', 'v.title', 'v.notes','char_vouchers_persons.receipt_location'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        $language_id =  \App\Http\Helpers::getLocale();
        $query->orderBy('v.title')
            ->selectRaw("CASE  WHEN v.title is null THEN ' ' Else v.title END AS voucher_name,
                                   CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name ");

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('char_vouchers_persons.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('char_vouchers_persons.receipt_date', '<=', $receipt_date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $date_to=null;
        $date_from=null;

        if(isset($filters['voucher_source']) && $filters['voucher_source'] !=null){
            if($filters['voucher_source'] == 1){
                $query = $query->whereNull('v.project_id');
            }else{
                $query = $query->whereNotNull('v.project_id');
            }
        }

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }


        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $manually_ = trans('aid::application.manually_');

        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');

        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $receipt = trans('aid::application.receipt');
        $not_receipt = trans('aid::application.not_receipt');


        if($filters['action'] =='export'){

            $query->selectRaw("CASE  WHEN char_vouchers_categories.name is null THEN ' ' Else char_vouchers_categories.name END AS category_type,
                           CASE  WHEN v.value is null THEN ' ' Else v.value END AS voucher_value,
                           CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name_,
                           CASE  WHEN v.type = 1 THEN '$non_financial' WHEN v.type = 2 THEN '$financial' END AS voucher_type,
                           CASE  WHEN char_vouchers_persons.status = 1 THEN '$receipt'  WHEN char_vouchers_persons.status = 2 THEN '$not_receipt' END AS receipt_status,
                           CASE  WHEN char_vouchers_persons.receipt_date is null THEN ' ' Else char_vouchers_persons.receipt_date END AS receipt_date,
                           CASE WHEN char_vouchers_persons.receipt_time is null THEN ' ' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time,
                           CASE  WHEN char_vouchers_persons.receipt_location is null THEN ' ' Else char_vouchers_persons.receipt_location END AS receipt_location,
                           CASE  WHEN v.notes is null THEN ' ' Else v.notes END AS voucher_note
                          ");
            return $query->get();
        }

        $query->selectRaw("CASE  WHEN char_vouchers_categories.name is null THEN ' ' Else char_vouchers_categories.name END AS category_type,
                           CASE  WHEN v.value is null THEN ' ' Else v.value END AS voucher_value,
                           CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name_,
                           CASE  WHEN v.type = 1 THEN '$non_financial' WHEN v.type = 2 THEN '$financial' END AS voucher_type,
                           CASE  WHEN v.urgent = 0 THEN '$not_urgent' WHEN v.urgent = 1 THEN '$urgent' END  AS voucher_urgent,
                           CASE  WHEN v.project_id is null THEN '$from_project' Else '$manually_' END  AS voucher_source,     
                           CASE  WHEN char_vouchers_persons.status = 1 THEN '$receipt'  WHEN char_vouchers_persons.status = 2 THEN '$not_receipt' END AS receipt_status,
                           CASE  WHEN char_vouchers_persons.receipt_date is null THEN ' ' Else char_vouchers_persons.receipt_date END AS receipt_date,
                           CASE WHEN char_vouchers_persons.receipt_time is null THEN ' ' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time,
                           CASE  WHEN char_vouchers_persons.receipt_location is null THEN ' ' Else char_vouchers_persons.receipt_location END AS receipt_location,
                           CASE  WHEN v.notes is null THEN ' ' Else v.notes END AS voucher_note
                          ");
        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->paginate()->total());
        $response['data'] = $query->paginate($records_per_page);

        $amount = 0;
        $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
        if($total){
            $amount = $total->total;
        }

        $response['total'] =round($amount,0);

        return $response;
    }

    public static function getSponsorshipsForSponsor($filters){
        
        
        $user = \Auth::user();
        $filters['sponsor_id'] = $user->organization_id;
        $char_cases=['category_id','case_id','person_id'];
        $char_sponsorship_cases=['status','sponsor_id'];
//        $char_sponsorship=['category_id']; // category of sponsorship

        $condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()
            ->join('char_sponsorship_cases','char_sponsorship_cases.case_id',  '=', 'char_cases.id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->join('char_organizations as org', 'org.id', '=', 'char_cases.organization_id')
            ->join('char_categories as ca','ca.id', '=', 'char_cases.category_id');


        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        $q->where($key, '=', $value);
                    }
                }
            });
        }

        $to_sponsorship_date=null;
        $from_sponsorship_date=null;

        if(isset($filters['to_sponsorship_date']) && $filters['to_sponsorship_date'] !=null){
            $to_sponsorship_date=date('Y-m-d',strtotime($filters['to_sponsorship_date']));
        }
        if(isset($filters['from_sponsorship_date']) && $filters['from_sponsorship_date'] !=null){
            $from_sponsorship_date=date('Y-m-d',strtotime($filters['from_sponsorship_date']));
        }

        if($from_sponsorship_date != null && $to_sponsorship_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.created_at', [ $from_sponsorship_date, $to_sponsorship_date]);
        }elseif($from_sponsorship_date != null && $to_sponsorship_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '>=', $from_sponsorship_date);
        }elseif($from_sponsorship_date == null && $to_sponsorship_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '<=', $to_sponsorship_date);
        }


        $to_guaranteed_date=null;
        $from_guaranteed_date=null;


        if(isset($filters['to_guaranteed_date']) && $filters['to_guaranteed_date'] !=null){
            $to_guaranteed_date=date('Y-m-d',strtotime($filters['to_guaranteed_date']));
        }
        if(isset($filters['from_guaranteed_date']) && $filters['from_guaranteed_date'] !=null){
            $from_guaranteed_date=date('Y-m-d',strtotime($filters['from_guaranteed_date']));
        }

        if($from_guaranteed_date != null && $to_guaranteed_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.sponsorship_date', [ $from_guaranteed_date, $to_guaranteed_date]);
        }elseif($from_guaranteed_date != null && $to_guaranteed_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '>=', $from_guaranteed_date);
        }elseif($from_guaranteed_date == null && $to_guaranteed_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '<=', $to_guaranteed_date);
        }
        $language_id =  \App\Http\Helpers::getLocale();
        $query->selectRaw("CONCAT(ifnull(char_persons.first_name,' '), ' ' ,ifnull(char_persons.second_name, ' '), ' ',ifnull(char_persons.third_name, ' '), ' ', ifnull(char_persons.last_name,' ')) AS name,
                           char_persons.id_card_number,char_persons.old_id_card_number,
                           CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name,
                            CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name");

         
                $query->selectRaw("
                                     char_sponsorship_cases.status as sponsorship_status,
                                     CASE  WHEN char_sponsorship_cases.sponsor_number is null THEN ' ' Else char_sponsorship_cases.sponsor_number END AS sponsor_number
                                     ");

        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');

        if($filters['action'] =='filters'){
            $query->selectRaw("CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                              WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                              WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                              WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                              WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                         END AS  status");
        }else{
            $query->selectRaw("CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                              WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                              WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                              WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                               WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                          END AS  sponsorship_status");
        }

        $query->selectRaw("DATE_FORMAT(char_sponsorship_cases.created_at,'%Y-%m-%d') as created_at,
                           CASE WHEN char_sponsorship_cases.sponsorship_date is null THEN ' ' Else char_sponsorship_cases.sponsorship_date
                                END AS guaranteed_date,
                             char_sponsorship_cases.id , char_sponsorship_cases.*
                        ");

        if($filters['action'] =='filters'){
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $query=$query->paginate($records_per_page);
            return $query;

        }
        return $query->get();

    }

    public static function getPaymentForSponsor($target,$filters,$id,$father_id,$mother_id,$page,$organization_id,$sponsor_id){
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $char_cases=['category_id','case_id'];
//        $char_payments_cases = ['case_id','guardian_id'];
        $char_payments_int = ['sponsorship_id','organization_id','sponsor_id', 'status'];
        $c = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $user = \Auth::user();

        $filters['sponsor_id'] = $user->organization_id;
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {

                    if($key == 'case_id'){
                        $key='id';
                    }
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_payments_int)) {
                if ($value != "" ) {
                    $data = ['char_payments.' . $key => (int) $value];
                    array_push($condition, $data);
                }
            }
//            if(in_array($key, $char_payments_cases)) {
//                if ($value != "" ) {
//                    $data = ['char_payments_cases.' . $key => (int) $value];
//                    array_push($condition, $data);
//                }
//            }
            if(in_array($key, $c)) {
                if ($value != "" ) {
                    $data = ['c.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $query = self::query()
            ->join('char_payments_cases','char_payments_cases.case_id',  '=', 'char_cases.id')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments', function($q){
                $q->on('char_payments.id','=','char_payments_cases.payment_id');
                $q->on('char_payments.organization_id','=','char_cases.organization_id');
            })
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->leftjoin('char_persons AS g','g.id','=','char_payments_cases.guardian_id')
            ->whereIn('char_payments_cases.status',[1,2]);

        if($target =='guardian' || $target =='guardians'){
            $query->join('char_persons as c','c.id',  '=', 'char_cases.person_id');
        }else{
            $query->join('char_persons as c','c.id',  '=', 'char_payments_cases.guardian_id');
        }

        if($target =='family'){
            $query=$query->wherein('char_cases.person_id', function($q)use($father_id,$mother_id,$id){
                $q->select('char_persons.id')->from('char_persons');
                if($father_id != null){
                    $q->where('char_persons.father_id', $father_id);
                }

                if($mother_id != null){
                    $q->where('char_persons.mother_id', $mother_id);
                }

                if($father_id == null && $mother_id ==null){
                    $q->where('char_persons.id', $id);
                }
            });
        }

        elseif($target =='guardian' || $target =='guardians'){
            $query=$query->where('char_payments_cases.guardian_id','=',$id);
        }else{
            $query=$query->where('char_cases.person_id','=',$id);
        }

        if($sponsor_id != null){
            $query=$query->where('char_payments.sponsor_id','=',$sponsor_id);
        }

        $end_date_from=null;
        $begin_date_from=null;

        if(isset($filters['end_date_from']) && $filters['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($filters['end_date_from']));
        }
        if(isset($filters['begin_date_from']) && $filters['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($filters['begin_date_from']));
        }
        if($begin_date_from != null && $end_date_from != null) {
            $query = $query->whereBetween( 'char_payments_cases.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $query = $query->whereDate('char_payments_cases.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $query = $query->whereDate('char_payments_cases.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($filters['begin_date_to']) && $filters['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($filters['begin_date_to']));
        }

        if(isset($filters['end_date_to']) && $filters['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($filters['end_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $query = $query->whereBetween( 'char_payments_cases.date_to', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $query = $query->whereDate('char_payments_cases.date_to', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $query = $query->whereDate('char_payments_cases.date_to', '<=', $end_date_to);
        }

        $end_payment_date=null;
        $start_payment_date=null;

        if($start_payment_date != null && $end_payment_date != null) {
            $query = $query->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $query = $query->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($start_payment_date == null && $end_payment_date != null) {
            $query = $query->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            $names = ['g.first_name', 'g.second_name', 'g.third_name', 'g.last_name'];
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $query->selectRaw("CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                           CASE WHEN c.id_card_number is null THEN ' ' Else c.id_card_number END AS id_card_number,
                           CASE WHEN c.old_id_card_number is null THEN ' ' Else c.old_id_card_number END AS old_id_card_number,
                           CASE WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name,
                           CASE WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organizations_name ");


        $deposit = trans('common::application.Bank deposit');
        $check = trans('common::application.Bank check');
        $card = trans('common::application.ID card');
        $Internal_check = trans('common::application.Internal check');

        $amount = 0;
        if($filters['action'] =='filters'){
            
             $query->selectRaw("char_payments_cases.case_id,
        CASE WHEN char_payments_cases.sponsor_number is null THEN ' ' 
                                      Else char_payments_cases.sponsor_number END AS sponsor_number,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else char_payments_cases.amount  END  AS amount,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else (char_payments_cases.shekel_amount)  END  AS amount_in_shekel,
                           CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                           CASE WHEN char_payments_cases.recipient is null THEN ' '
                                WHEN char_payments_cases.recipient = 0 THEN CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                WHEN char_payments_cases.recipient = 1 THEN 
                                CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                           END  AS recipients_name,
                           CASE WHEN char_payments_cases.date_to is null THEN ' ' Else char_payments_cases.date_to END  AS date_to,
                           CASE WHEN char_payments_cases.date_from is null THEN ' ' Else char_payments_cases.date_from  END  AS date_from,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.exchange_date is null THEN ' 'Else char_payments.exchange_date END  AS exchange_date,
                           CASE WHEN char_payments.exchange_type is  null THEN ' '
                                WHEN char_payments.exchange_type = 1 THEN '$deposit'
                                WHEN char_payments.exchange_type = 2 THEN '$check'
                                WHEN char_payments.exchange_type = 4 THEN '$card'
                                WHEN char_payments.exchange_type = 3 THEN '$Internal_check'
                           END  AS exchange_type");
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $paginate=$query->paginate($records_per_page);
            if($page == 1){
                $total    = $query->selectRaw("sum((char_payments_cases.shekel_amount)) as total")->first();
                $amount = $total->total;
            }
            return ['payment' => $paginate, 'total' =>round($amount,0)];
        }
        
         $query->selectRaw("CASE WHEN char_payments_cases.sponsor_number is null THEN ' ' 
                                      Else char_payments_cases.sponsor_number END AS sponsor_number,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else char_payments_cases.amount  END  AS amount,
                           CASE WHEN char_payments_cases.amount is null THEN ' ' Else (char_payments_cases.shekel_amount)  END  AS amount_in_shekel,
                           CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                           CASE WHEN char_payments_cases.recipient is null THEN ' '
                                WHEN char_payments_cases.recipient = 0 THEN CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                WHEN char_payments_cases.recipient = 1 THEN 
                                CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                           END  AS recipients_name,
                           CASE WHEN char_payments_cases.date_to is null THEN ' ' Else char_payments_cases.date_to END  AS date_to,
                           CASE WHEN char_payments_cases.date_from is null THEN ' ' Else char_payments_cases.date_from  END  AS date_from,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.payment_date is null THEN ' ' Else char_payments.payment_date END  AS payment_date,
                           CASE WHEN char_payments.exchange_date is null THEN ' 'Else char_payments.exchange_date END  AS exchange_date,
                           CASE WHEN char_payments.exchange_type is  null THEN ' '
                                WHEN char_payments.exchange_type = 1 THEN '$deposit'
                                WHEN char_payments.exchange_type = 2 THEN '$check'
                                WHEN char_payments.exchange_type = 4 THEN '$card'
                                WHEN char_payments.exchange_type = 3 THEN '$Internal_check'
                           END  AS exchange_type");
        return $query->get();
    }

}

