<?php

namespace Common\Model;
use App\Http\Helpers;

abstract class AbstractVisitorNotes extends \Illuminate\Database\Eloquent\Model
{

    const LEVEL_MALE = 1;
    const LEVEL_FEMALE = 2;

    const TARGET_SPONSORSHIPS = 1;
    const TARGET_AIDS = 2;

    protected $table = 'char_visitor_notes';
//    protected $fillable = ['logo', 'type', 'country', 'name'];
    public $timestamps = false;
    protected $appends = ['target_name', 'target_options'];

    public function isValid($attributes, $rules) {
        $this->validator = \Validator::make($attributes, $rules);
        return $this->validator->passes();
    }

    public function getMessages() {
        return $this->validator->messages();
    }

    public static function getData($target,$organization_id,$itemsCount) {
        $language_id =  \App\Http\Helpers::getLocale();
        $itemsCount = isset($itemsCount)? $itemsCount:0;
        $query = \DB::table('char_visitor_notes')
            ->join('char_categories as ca', function($q) use($target){
                $q->on('ca.id', '=', 'char_visitor_notes.category_id');
                $q->where('ca.type',$target);
            })
            ->where('char_visitor_notes.organization_id',$organization_id)
            ->orderBy('ca.name')
            ->selectRaw("CASE WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name,
                         char_visitor_notes.*");
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
             return $query->paginate($records_per_page);
    }


    public static function target($value = null)
    {
        $options = array(
            self::TARGET_AIDS => trans('application::application.aids'),
            self::TARGET_SPONSORSHIPS => trans('application::application.sponsorship'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getTarget()
    {
        return self::target($this->target);
    }

    public function getTargetNameAttribute()
    {
        return $this->getTarget();
    }

    public function getTargetOptionsAttribute()
    {
        return self::target();
    }

    public static function fetch($category_id,$organization_id,$gender,$lang = 'ar')
    {
        $notes=\DB::table('char_visitor_notes')
            ->where('char_visitor_notes.category_id','=',$category_id)
            ->where('char_visitor_notes.organization_id','=',$organization_id);

        if($gender == 2){
            if($lang == 'ar'){
                $notes=$notes->selectRaw("char_visitor_notes.for_female as note ")->first();
            }else{
                $notes=$notes->selectRaw("char_visitor_notes.en_for_female as note ")->first();
            }
        }else{
            if($lang == 'ar'){
                $notes=$notes->selectRaw("char_visitor_notes.for_male as note")->first();
            }else{
                $notes=$notes->selectRaw("char_visitor_notes.en_for_male as note")->first();
            }
        }

        return $notes;

    }




}
