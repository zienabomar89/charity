<?php

namespace Common\Model;

class ValidatorModel
{

    public  function isValid($attributes,$rules){
        $this->validator = \Validator::make($attributes,$rules) ;
        return $this->validator->passes();
    }

    public  function getMessages()
    {
        return $this->validator->messages();
    }

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name'  => 'required|max:255',
            );
        }

        return self::$rules;
    }

    public static function getPersonValidatorRules($curPriority,$options)
    {
        $cont= '|';
        $integer = 'integer';

        $IntegerInput =  ['habitable','house_condition','rooms','area','rent_value' ,'card_type','is_qualified','qualified_card_number',
                          'indoor_condition','residence_condition','roof_material_id','property_type_id',
                          'kinship_id','stage','condition','health_condition','marital_status_id',
                          'WhoIsGuardian','can_work','working','nationality','category_id' ,'gender','study','prayer','save_quran','quran_center',
                          'birth_place', 'marital_status_id' , 'refugee','alive','spouses','health_disease_id','deserted','family_cnt',
                          'has_health_insurance', 'health_insurance_number', 'has_device','receivables','has_other_work_resources'];

        $Con =  ['marital_status','property_type','roof_material','disease','death_cause',
            'work_status','work_job','work_wage','work_reason','kinship'];
        $edu =  ['type','authority', 'stage','grade','year','school','points','level','degree'];
        $working_arr= ['work_status_id','work_job_id','work_wage_id','work_location'];
        $substr =  [ 'guardian' => ['guardian_address','guardian_banks','guardian_indoor_condition',
            'guardian_marital_status','guardian_phone','guardian_kinship',
            'guardian_primary_mobile','guardian_wataniya','guardian_secondary_mobile',
            'guardian_property_type','guardian_residence_condition','guardian_roof_material',
            'guardian_rooms','guardian_stage',],
            'mother'    =>['mother_marital_status','mother_death_date','mother_death_cause','mother_condition','mother_disease',
                           'mother_details','mother_nationality','mother_stage','mother_prev_family_name'],
                        'health'=>['health_condition'],
                        'father'=>['father_condition'],
                        'fm'=>['fm_monthly_income','fm_work_job_id','fm_working'],
                        'case'=>['case_needs'],
                        'reconstructions'=>['reconstructions_promised','reconstructions_organization_name']
                    ];
        $others=['street_address','prev_family_name','essential_condition','notes',
                 'visitor','needs','promised','organization_name','financial_aid_condition',
                 'non_financial_aid_condition','properties_condition',
                 'monthly_income','actual_monthly_income','gov_commercial_records_details','gov_health_details',  'gov_work_details'


        ];
        $rules=[];


        foreach($curPriority as $key => $value) {
            $priority = null;
            if($value != 1){
                if ($value == 4) {
                    $priority = 'required';
                }else{
                    $priority = ' ';
                }
            }
            if($priority != null && $priority != ' ') {
                if(in_array($key, $substr['reconstructions'])){
                    $key=substr($key,16);
                }
                if(in_array($key, $substr['case'])){
                    $key=substr($key,5);
                }
                if(in_array($key, $substr['fm'])){
                    $key=substr($key,3);
                }
                if(in_array($key, $substr['guardian'])){
                    $key=substr($key,9);
                }
                if(in_array($key, $substr['mother']) || in_array($key, $substr['health'])|| in_array($key, $substr['father'])){
                    $key=substr($key,7);
                }
                if(in_array($key, $Con)){
                    $key=$key.'_id';
                }
                if ($key == 'name') {
                    $rules['first_name'] = $priority;
                    $rules['second_name'] = $priority;
                    $rules['third_name'] = $priority;
                    $rules['last_name'] = $priority;

//                    if(!isset($options['no_en'])){
//                        $rules['en_first_name'] = $priority;
//                        $rules['en_second_name'] = $priority;
//                        $rules['en_third_name'] = $priority;
//                        $rules['en_last_name'] = $priority;
//                    }
                }

                elseif($key == 'en_name') {
                    if(!isset($options['no_en'])){
                        $rules['en_first_name'] = $priority;
                        $rules['en_second_name'] = $priority;
                        $rules['en_third_name'] = $priority;
                        $rules['en_last_name'] = $priority;
                    }
                }
                elseif ($key == 'birthday') {
                    $rules['birthday'] = $priority . '|date|before:' . date("Y-m-d", strtotime('tomorrow'));
                }
                elseif ($key == 'death_cause_id') {
                        $rules['death_cause_id'] = $priority;
                }
                elseif ($key == 'visited_at') {
                    $rules[$key]= $priority.$cont.'date';
                }
                elseif ($key == 'death_date') {
                    if ($priority != null && $priority != ' ' ) {
                        $rules['death_date'] = $priority . '|date|before:' . date("Y-m-d", strtotime('tomorrow'));
                    }
                }
                elseif($key == 'location' || $key == 'address' ){

                    if(isset($options['aidsLocations'])){
                        $rules['adscountry_id']=$priority . $cont .$integer;
                        $rules['adsdistrict_id']=$priority . $cont .$integer;
                        $rules['adsregion_id']=$priority . $cont .$integer;
                        $rules['adsneighborhood_id']=$priority . $cont .$integer;
                        $rules['adssquare_id']=$priority . $cont .$integer;
                        $rules['adsmosques_id']=$priority . $cont .$integer;
                    }else{
                        $rules['city']=$priority . $cont .$integer;
                        $rules['country']=$priority . $cont .$integer;
                        $rules['governarate']=$priority . $cont .$integer;
                        $rules['location_id']=$priority . $cont .$integer;
                        $rules['mosques_id']=$priority . $cont .$integer;

                    }

//                    $rules['street_address']=$priority ;
                }
                elseif($key =='unrwa_card_number'){
                    $rules['unrwa_card_number']= 'required_if:refugee,2|max:45';
                }
                elseif(in_array($key,$working_arr)){
                    $rules[$key]= 'required_if:working,1';
                }
                elseif($key =='work_reason_id'){
                    $rules['work_reason_id']= 'required_if:can_work,2|integer';
                }
                elseif($key =='details' || $key =='health_details'){
                    $rules[$key] = 'required_if:condition,2|required_if:condition,3';
//                    $rules['details'] = 'required_if:health_condition,2|required_if:health_condition,3';
                }
                elseif($key =='disease_id' || $key =='health_disease_id'){
//                    $rules[$key] = 'required_if:health_condition,2|required_if:health_condition,3';
                    $rules[$key] = 'required_if:condition,2|required_if:condition,3';
                }
                elseif($key =='health_insurance_number' || $key =='health_insurance_type'){
                    $rules[$key] = 'required_if:has_health_insurance,1';
                }
                elseif($key =='used_device_name'){
                    $rules[$key] = 'required_if:has_device,1';
                }
                elseif($key =='receivables_sk_amount'){
                    $rules[$key] = 'required_if:receivables,1';
                }
                elseif($key =='qualified_card_number'){
                    $rules[$key] = 'required_if:is_qualified,1';
                }
                elseif($key =='quran_parts' || $key =='quran_chapters'){
                    $rules[$key] = 'required_if:save_quran,0';
                }
                elseif($key=='rent_value'){
                    $rules['rent_value'] = 'required_if:property_type_id,1|integer';
                }
                elseif($key =='prayer_reason'){
                    $rules['prayer_reason'] = 'required_if:prayer,1';
                }
                elseif($key =='quran_reason'){
                    $rules['quran_reason'] = 'required_if:quran_center,0';
                }
                else if($key=='rent_value'){
                    $rules[$key] = 'required_if:property_type_id,1|integer';
                }
                else if($key=='organization_name'){
                    $rules[$key] = 'required_if:promised,2';
                }
                elseif(in_array($key, ['phone','primary_mobile','secondary_mobile','secondery_mobile','wataniya','banks','kinship'])){
                    $rules[$key] =$priority;
                }
                elseif(in_array($key, $edu)) {
                    $rules[$key]= 'required_if:study,1';
                }
                elseif(in_array($key, $IntegerInput)){
                    $rules[$key]=$priority . $cont .$integer;
                }
                elseif(in_array($key, $others)){
                    $rules[$key]=$priority ;
                }
            }
        }

        if(!isset($options['sub'])) {
            if($options['mode'] =='update'){
                $rules['id_card_number']= 'required|integer|unique:char_persons,id_card_number,'.$options['person_id'];
            }else{
                $rules['id_card_number']= 'required|integer|unique:char_persons';
            }
        }
        return $rules;
    }
}
