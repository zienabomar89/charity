<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;

class CaseFile extends Model
{
    protected $table = 'char_cases_files';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('case_id', '=', $this->case_id)
              ->where('document_type_id', '=', $this->document_type_id);

        return $query;
    }
    
    public static function fetch($options = array())
    {
        $query = static::query();
        $query->join('doc_files', 'char_cases_files.file_id', '=', 'doc_files.id')
              ->addSelect([
                  'doc_files.filepath',
                  'doc_files.size',
                  'doc_files.mimetype',
              ]);
        
        return $query->get();
    }

    public static function createNew($attributes)
    {
        return \Illuminate\Support\Facades\DB::transaction(function() use ($attributes) {
            $file = \Document\Model\File::forceCreate([
                'name' => $attributes['name'],
                'filepath' => $attributes['filepath'],
                'size' => $attributes['size'],
                'mimetype' => $attributes['mimetype'],
                'file_status_id' => 1,
                'created_by' => $attributes['user_id'],
            ]);
            if ($file->id) {
                $caseFile = self::forceCreate([
                    'case_id' => $attributes['case_id'],
                    'document_type_id' => $attributes['document_type_id'],
                    'file_id' => $file->id,
                    'person_id' => $attributes['person_id'],
                ]);
                
                return $caseFile;
            }
        });
    }
}