<?php

namespace Common\Model;
use App\Http\Helpers;

abstract class AbstractCategoriespolicy extends \Illuminate\Database\Eloquent\Model
{

    const STATUS_ACTIVE   = 0;
    const STATUS_INACTIVE = 1;

    const LEVEL_PERSON = 1;
    const LEVEL_FAMILY = 2;

    const TARGET_SPONSORSHIPS = 1;
    const TARGET_AIDS = 2;

    protected $table = 'char_categories_policy';
//    protected $fillable = ['logo', 'type', 'country', 'name'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $appends = ['target_name', 'target_options','status_name', 'status_options', 'level_name', 'level_options'];

    public function isValid($attributes, $rules) {
        $this->validator = \Validator::make($attributes, $rules);
        return $this->validator->passes();
    }

    public function getMessages() {
        return $this->validator->messages();
    }

    public static function getData($target,$filters) {

        $language_id =  \App\Http\Helpers::getLocale();
        $char_categories_policy=["level", "category_id"];

        $condition = [];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_categories_policy)) {
                if ($value != "") {
                    $data = ['char_categories_policy.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $policy = \DB::table('char_categories_policy')
            ->join('char_categories as ca','ca.id',  '=', 'char_categories_policy.category_id')
            ->where('ca.type','=',$target)
            ->where('char_categories_policy.deleted_at','=',null);


        if (count($condition) != 0) {
            $policy =  $policy
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                                $q->where($key, '=', $value);
                        }
                    }
                });

        }


        $max_amount=null;
        $min_amount=null;

        if(isset($filters['min_amount']) && $filters['min_amount'] !=null && $filters['min_amount'] !=""){
            $min_amount=(int)$filters['min_amount'];
        }

        if(isset($filters['max_amount']) && $filters['max_amount'] !=null && $filters['max_amount'] !=""){
            $max_amount=(int)$filters['max_amount'];
        }

        if($max_amount != null && $min_amount != null) {
            $policy = $policy->whereRaw(" $min_amount <= char_categories_policy.amount");
            $policy = $policy->whereRaw(" $max_amount >= char_categories_policy.amount");
        }elseif($min_amount != null && $max_amount == null) {
            $policy = $policy->whereRaw(" $min_amount <= char_categories_policy.amount");
        }elseif($min_amount == null && $max_amount != null) {
            $policy = $policy->whereRaw(" $max_amount >= char_categories_policy.amount");
        }

        $max_count_of_family=null;
        $min_count_of_family=null;

        if(isset($filters['min_count_of_family']) && $filters['min_count_of_family'] !=null && $filters['min_count_of_family'] !=""){
            $min_count_of_family=(int)$filters['min_count_of_family'];
        }

        if(isset($filters['max_count_of_family']) && $filters['max_count_of_family'] !=null && $filters['max_count_of_family'] !=""){
            $max_count_of_family=(int)$filters['max_count_of_family'];
        }

        if($max_count_of_family != null && $min_count_of_family != null) {
            $policy = $policy->whereRaw(" $min_count_of_family <= char_categories_policy.count_of_family");
            $policy = $policy->whereRaw(" $max_count_of_family >= char_categories_policy.count_of_family");
        }elseif($min_count_of_family != null && $max_count_of_family == null) {
            $policy = $policy->whereRaw(" $min_count_of_family <= char_categories_policy.count_of_family");
        }elseif($min_count_of_family == null && $max_count_of_family != null) {
            $policy = $policy->whereRaw(" $max_count_of_family >= char_categories_policy.count_of_family");
        }


        $started_at_to=null;
        $started_at_from=null;

        if(isset($filters['started_at_to']) && $filters['started_at_to'] !=null){
            $started_at_to=date('Y-m-d',strtotime($filters['started_at_to']));
        }
        if(isset($filters['started_at_from']) && $filters['started_at_from'] !=null){
            $started_at_from=date('Y-m-d',strtotime($filters['started_at_from']));
        }
        if($started_at_from != null && $started_at_to != null) {
            $policy = $policy->whereBetween( 'char_categories_policy.started_at', [ $started_at_from, $started_at_to]);
        }elseif($started_at_from != null && $started_at_to == null) {
            $policy = $policy->whereDate('char_categories_policy.started_at', '>=', $started_at_from);
        }elseif($started_at_from == null && $started_at_to != null) {
            $policy = $policy->whereDate('char_categories_policy.started_at', '<=', $started_at_to);
        }

        $ends_at_to=null;
        $ends_at_from=null;

        if(isset($filters['ends_at_to']) && $filters['ends_at_to'] !=null){
            $ends_at_to=date('Y-m-d',strtotime($filters['ends_at_to']));
        }
        if(isset($filters['ends_at_from']) && $filters['ends_at_from'] !=null){
            $ends_at_from=date('Y-m-d',strtotime($filters['ends_at_from']));
        }

        if($ends_at_from != null && $ends_at_to != null) {
            $policy = $policy->whereBetween( 'char_categories_policy.ends_at', [ $ends_at_from, $ends_at_to]);
        }elseif($ends_at_from != null && $ends_at_to == null) {
            $policy = $policy->whereDate('char_categories_policy.ends_at', '>=', $ends_at_from);
        }elseif($ends_at_from == null && $ends_at_to != null) {
            $policy = $policy->whereDate('char_categories_policy.ends_at', '<=', $ends_at_to);
        }

        $created_at_to=null;
        $created_at_from=null;

        if(isset($filters['created_at_to']) && $filters['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($filters['created_at_to']));
        }
        if(isset($filters['created_at_from']) && $filters['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($filters['created_at_from']));
        }
        if($created_at_from != null && $created_at_to != null) {
            $policy = $policy->whereBetween( 'char_categories_policy.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $policy = $policy->whereDate('char_categories_policy.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $policy = $policy->whereDate('char_categories_policy.created_at', '<=', $created_at_to);
        }


        $policy ->orderBy('ca.name')
                ->selectRaw("CASE WHEN $language_id = 1 THEN ca.name 
                                  Else ca.en_name END  
                                  AS category_name,
                                 char_categories_policy.*,
                                  CASE WHEN char_categories_policy.ends_at is null
                                       THEN '-'
                                       Else char_categories_policy.ends_at
                                  END
                                  AS ends_at,
                                  char_categories_policy.created_at,
                                  char_categories_policy.level
                                 ");
    $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
    $records_per_page = Helpers::recordsPerPage($itemsCount,$policy->count());
    return $policy->paginate($records_per_page);
    }

    public static function target($value = null)
    {
        $options = array(
            self::TARGET_AIDS => trans('application::application.aids'),
            self::TARGET_SPONSORSHIPS => trans('application::application.sponsorship'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getTarget()
    {
        return self::target($this->target);
    }

    public function getTargetNameAttribute()
    {
        return $this->getTarget();
    }

    public function getTargetOptionsAttribute()
    {
        return self::target();
    }

    public static function status($value = null)
    {
        $options = array(
            self::STATUS_ACTIVE => trans('aid::application.active'),
            self::STATUS_INACTIVE => trans('aid::application.inactive')
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getStatus()
    {
        return self::status($this->status);
    }

    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }

    public function getStatusOptionsAttribute()
    {
        return self::status();
    }


    public static function level($value = null)
    {
        $options = array(
            self::LEVEL_PERSON => trans('aid::application.person'),
            self::LEVEL_FAMILY => trans('aid::application.family'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getLevel()
    {
        return self::level($this->level);
    }

    public function getLevelNameAttribute()
    {
        return $this->getLevel();
    }


    public function getLevelOptionsAttribute()
    {
        return self::level();
    }



}
