<?php
namespace Common\Model;

class CasesStatusLog  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_cases_status_log';
    protected $primaryKey = 'id';
    protected $fillable = ['case_id', 'date','status','user_id','reason'];
    protected $hidden = ['created_at','updated_at'];

    public static function getLogs($id)
    {
        $active = trans('auth::application.active');
        $inactive = trans('auth::application.inactive');

        return  \DB::table('char_cases_status_log')
                    ->leftjoin('char_users','char_users.id',  '=', 'char_cases_status_log.user_id')
                    ->where('char_cases_status_log.case_id',$id)
                    ->selectRaw("char_cases_status_log.*,CONCAT(char_users.firstname,' ',char_users.lastname)as username,
                             char_cases_status_log.created_at,
                             CASE WHEN char_cases_status_log.status = 0 THEN '$active'
                                  WHEN char_cases_status_log.status = 1 THEN '$inactive'
                             END
                             AS falg

                        ")
                ->paginate(config('constants.records_per_page'));
    }
}