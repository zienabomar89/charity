<?php
namespace Common\Model;
use App\Http\Helpers;

class UpdateDataSetting  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_update_data_setting';
    protected $primaryKey = 'id';
    protected $fillable = ['type', 'organization_id','target_id','updated_period','allowed_period'];
    public $timestamps = false;

    public static function  getDate($organization_id,$type,$page,$itemsCount){

        $return=\DB::table('char_update_data_setting');


        if($type ==0){
            $return=$return->join('char_organizations As target', function($q){
                    $q->on('target.id', '=', 'char_update_data_setting.target_id');
                });
        }elseif($type ==1){
            $return=$return->join('char_locations_i18n As target', function($q){
                    $q->on('target.location_id', '=', 'char_update_data_setting.target_id');
                });
       }else{
            $return=$return->join('char_categories As target', function($q){
                            $q->on('target.id', '=', 'char_update_data_setting.target_id');
            });

        }

       $return->orderBy('target.name')
            ->where('char_update_data_setting.type','=',$type)
            ->where('char_update_data_setting.organization_id','=',$organization_id)
            ->selectRaw("char_update_data_setting.id,char_update_data_setting.type,target.name,char_update_data_setting.updated_period,char_update_data_setting.allowed_period");

        $itemsCount = ($itemsCount !== null)?$itemsCount:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$return->count());
        return $return->paginate($records_per_page);

    }

}