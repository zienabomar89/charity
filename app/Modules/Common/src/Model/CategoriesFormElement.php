<?php

namespace Common\Model;

class CategoriesFormElement extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_categories_form_element_priority';
    protected $fillable = ['element_id', 'category_id','priority'];
    public $timestamps = false;
    public $incrementing = false;


    public static function getElements($id){
            return  \DB::table('char_categories_form_element_priority')
                ->join('char_forms_elements as elements', function($q) {
                    $q->on('elements.id', '=', 'char_categories_form_element_priority.element_id');
                })
                ->where('char_categories_form_element_priority.category_id',$id)
                ->orderBy('elements.label')
                ->selectRaw('char_categories_form_element_priority.category_id,
                             char_categories_form_element_priority.priority,
                             elements.id,
                             elements.form_id,
                             elements.label,
                             elements.type,
                             elements.name')
                ->get();
    }
}
