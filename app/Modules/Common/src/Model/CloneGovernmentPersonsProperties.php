<?php

namespace Common\Model;

class CloneGovernmentPersonsProperties  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_properties';
    protected $fillable = ['IDNO','DOC_NO','REAL_OWNER_AREA'];
    public $timestamps = false;
}

