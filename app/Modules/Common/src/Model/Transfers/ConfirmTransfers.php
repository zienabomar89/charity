<?php
namespace Common\Model\Transfers;
use App\Http\Helpers;

use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use Common\Model\Person;
use Common\Model\AidsCases;

class ConfirmTransfers  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_transfers_log';
    protected $fillable = [ 'transfer_id','user_id','organization_id','status','reason','confirm_user_id','confirm_date','confirm','confirm_reason'];

    protected $primaryKey = 'id';
//    protected $hidden = ['created_at','updated_at'];

    public function transfer()
    {
        return $this->belongsTo('Common\Model\Transfers\Transfers','transfer_id','id');
    }

    public static function filter($page,$filters)
    {

        $category_type = 2;
        $user = \Illuminate\Support\Facades\Auth::user();
        $user = \Auth::user();
        $UserType=$user->type;
        $UserOrg=$user->organization;
        $organization_id = $user->organization_id;
        $UserOrgLevel =$UserOrg->level;
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];

        $int_ =  ['city', 'country','governarate', 'location_id','mosques_id',
            'adscountry_id', 'adsdistrict_id','adsmosques_id', 'adsneighborhood_id',
            'adsregion_id', 'adssquare_id','user_id'];

        $viewFilters=['category_id','first_name', 'second_name', 'third_name', 'last_name','id_card_number'];

        foreach ($filters as $key => $value) {
            if(in_array($key,$viewFilters)) {
                if($value != ""){
                    if(in_array($key, $int_)) {
                        $data = ['char_transfers_view.'.$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, ['char_transfers_view.'.$key => $value]);
                    }
                }
            }elseif(in_array($key, $int_)) {
                if($value != ""){
                    if(in_array($key, $int_)) {
                        $data = [$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, [$key => $value]);
                    }
                }
            }

        }

        $query=\DB::table('char_transfers_log')
                   ->join('char_transfers_view', function($join)  use ($organization_id) {
                        $join->on('char_transfers_view.id', 'char_transfers_log.transfer_id')
                             ->where('char_transfers_view.organization_id', $organization_id);
                   })
                   ->join('char_users As reqUer','reqUer.id',  '=', 'char_transfers_view.user_id')
                   ->leftjoin('char_cases', function($join) {
                        $join->on('char_cases.person_id','char_transfers_view.person_id' );
                        $join->on('char_cases.category_id','char_transfers_view.category_id' );
                        $join->on('char_cases.organization_id','char_transfers_log.organization_id' );
                        $join->whereRaw("date(char_cases.created_at) >  date(char_transfers_view.created_at)");
                    })
                   ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                        $join->on('char_transfers_view.adscountry_id', '=','country_name.location_id' )
                            ->where('country_name.language_id', $language_id);
                   })
                   ->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                        $join->on('char_transfers_view.adsdistrict_id', '=','district_name.location_id' )
                            ->where('district_name.language_id', $language_id);
                   })
                   ->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.adsregion_id', '=','region_name.location_id' )
                            ->where('region_name.language_id', $language_id);
                   })
                   ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.adsneighborhood_id', '=','location_name.location_id' )
                            ->where('location_name.language_id', $language_id);
                   })
                   ->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.adssquare_id', '=','square_name.location_id' )
                            ->where('square_name.language_id', $language_id);
                   })
                   ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.adsmosques_id', '=','mosques_name.location_id' )
                            ->where('mosques_name.language_id', $language_id);
                   })
                    ->leftjoin('char_aids_locations_i18n As prev_country_name', function($join)  use ($language_id) {
                        $join->on('char_transfers_view.prev_adscountry_id', '=','prev_country_name.location_id' )
                            ->where('country_name.language_id', $language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As prev_district_name', function($join)  use ($language_id) {
                        $join->on('char_transfers_view.prev_adsdistrict_id', '=','prev_district_name.location_id' )
                            ->where('district_name.language_id', $language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As prev_region_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.prev_adsregion_id', '=','prev_region_name.location_id' )
                            ->where('region_name.language_id', $language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As prev_location_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.prev_adsneighborhood_id', '=','prev_location_name.location_id' )
                            ->where('location_name.language_id', $language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As prev_square_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.prev_adssquare_id', '=','prev_square_name.location_id' )
                            ->where('square_name.language_id', $language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As prev_mosques_name', function($join) use ($language_id)  {
                        $join->on('char_transfers_view.prev_adsmosques_id', '=','prev_mosques_name.location_id' )
                            ->where('mosques_name.language_id', $language_id);
                    })
                   ->leftjoin('char_users As AccUser','AccUser.id',  '=', 'char_transfers_log.user_id')
                   ->leftjoin('char_users As confUser','confUser.id',  '=', 'char_transfers_log.confirm_user_id')
                ;


        $get_by_status = null;
        if(isset($filters['get_by_status']) && $filters['get_by_status'] !=null){
            $get_by_status= $filters['get_by_status'];
        }

        $get_by_status = (int)$get_by_status;

        if ($get_by_status == 1 || $get_by_status == 2 || $get_by_status == 3){
            $query->where('char_transfers_log.status',$get_by_status);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['first_name', 'second_name', 'third_name', 'last_name','street_address'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
//                                $q->where($key ,'like'  ,'%' . $value . '%');
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $created_at_to=null;
        $created_at_from=null;

        if(isset($filters['created_at_to']) && $filters['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($filters['created_at_to']));
        }
        if(isset($filters['created_at_from']) && $filters['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($filters['created_at_from']));
        }
        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween( 'char_transfers_view.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('char_transfers_view.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('char_transfers_view.created_at', '<=', $created_at_to);
        }

        $accept_date_to=null;
        $accept_date_from=null;

        if(isset($filters['accept_date_to']) && $filters['accept_date_to'] !=null){
            $accept_date_to=date('Y-m-d',strtotime($filters['accept_date_to']));
        }
        if(isset($filters['accept_date_from']) && $filters['accept_date_from'] !=null){
            $accept_date_from=date('Y-m-d',strtotime($filters['accept_date_from']));
        }
        if($accept_date_from != null && $accept_date_to != null) {
            $query->whereBetween( 'char_transfers_log.created_at', [ $accept_date_from, $accept_date_to]);
        }elseif($accept_date_from != null && $accept_date_to == null) {
            $query->whereDate('char_transfers_log.created_at', '>=', $accept_date_from);
        }elseif($accept_date_from == null && $accept_date_to != null) {
            $query->whereDate('char_transfers_log.created_at', '<=', $accept_date_to);
        }

        $confirm_date_to=null;
        $confirm_date_from=null;

        if(isset($filters['confirm_date_to']) && $filters['confirm_date_to'] !=null){
            $confirm_date_to=date('Y-m-d',strtotime($filters['confirm_date_to']));
        }
        if(isset($filters['confirm_date_from']) && $filters['confirm_date_from'] !=null){
            $confirm_date_from=date('Y-m-d',strtotime($filters['confirm_date_from']));
        }
        if($confirm_date_from != null && $confirm_date_to != null) {
            $query->whereBetween( 'char_transfers_log.confirm_date', [ $confirm_date_from, $confirm_date_to]);
        }elseif($confirm_date_from != null && $confirm_date_to == null) {
            $query->whereDate('char_transfers_log.confirm_date', '>=', $confirm_date_from);
        }elseif($confirm_date_from == null && $confirm_date_to != null) {
            $query->whereDate('char_transfers_log.confirm_date', '<=', $confirm_date_to);
        }

        $order = false;


        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'desc');
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'asc');
            }
        }

        $query->selectRaw("char_transfers_view.* ,
                         CASE WHEN char_cases.id is null THEN '0' Else '1' END   AS has_case,
                         char_cases.id as case_id ,      
                         CASE WHEN (char_cases.id is not null and char_cases.organization_id = '$organization_id') THEN 1 Else 0  END  AS is_mine,                  
                         char_transfers_log.id as transfer_log_id ,                        
                         CASE WHEN char_transfers_log.id is null THEN '0' Else '1' END   AS has_transfer,
                         CASE WHEN char_transfers_log.status is null THEN '0'
                              WHEN char_transfers_log.status is not null THEN  char_transfers_log.status
                         END
                         as status_id,                       
                         CASE WHEN reqUer.firstname is null THEN '-' Else CONCAT(reqUer.firstname,' ',reqUer.lastname) END as user_name,

                         char_transfers_log.confirm_reason ,
                         CASE WHEN char_transfers_log.created_at is null THEN '-' Else char_transfers_log.created_at END   AS transfer_date,
                         CASE WHEN AccUser.firstname is null THEN '-' Else CONCAT(AccUser.firstname,' ',AccUser.lastname) END as transfer_user_name,

                         CASE WHEN char_transfers_log.confirm_date is null THEN '-' Else char_transfers_log.confirm_date END   AS confirm_date,
                         CASE WHEN char_transfers_log.created_at is null THEN '-' Else char_transfers_log.created_at  END  AS confirm_transfer_date,
                         CASE WHEN confUser.firstname is null THEN '-' Else CONCAT(confUser.firstname,' ',confUser.lastname) END as confirm_user_name,

                         CASE WHEN char_transfers_view.prev_adscountry_id  is null THEN ' ' Else prev_country_name.name END   AS prev_country,
                         CASE WHEN char_transfers_view.prev_adsdistrict_id  is null THEN ' ' Else prev_district_name.name END   AS prev_district_,
                         CASE WHEN char_transfers_view.prev_adsregion_id is null THEN ' ' Else prev_region_name.name END   AS prev_region_,
                         CASE WHEN char_transfers_view.prev_adsneighborhood_id is null THEN ' ' Else prev_location_name.name END   AS prev_nearLocation,
                         CASE WHEN char_transfers_view.prev_adssquare_id is null THEN ' ' Else prev_square_name.name END   AS prev_square_,
                         CASE WHEN char_transfers_view.prev_adsmosques_id is null THEN ' ' Else prev_mosques_name.name END   AS prev_mosque_,
                         
                         CASE WHEN char_transfers_view.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                         CASE WHEN char_transfers_view.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS district_,
                         CASE WHEN char_transfers_view.adsregion_id is null THEN ' ' Else region_name.name END   AS region_,
                         CASE WHEN char_transfers_view.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                         CASE WHEN char_transfers_view.adssquare_id is null THEN ' ' Else square_name.name END   AS square_,
                         CASE WHEN char_transfers_view.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_,
                         
                         CONCAT(ifnull(country_name.name,' '),' _ ',ifnull(district_name.name,' '),' _ ',
                                ifnull(region_name.name,' '),' _ ',ifnull(location_name.name,' '),' _ ',
                                ifnull(square_name.name,' '),' _ ',ifnull(mosques_name.name,' '))
                         AS address");

        if(!$order){
            $query->orderBy('char_transfers_log.created_at','desc');
        }


        $query->groupBy('char_transfers_log.id');

        if($filters['action'] =='xlsx'){
            return  $query->get();
        }

        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page);
    }

}
