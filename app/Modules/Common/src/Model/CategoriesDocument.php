<?php

namespace Common\Model;

class CategoriesDocument extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_categories_document_types';
    protected $fillable = ['category_id', 'document_type_id','scope'];
    public $timestamps = false;

    public static function getCategoryDocument($id)
    {
       return  \DB::table('char_categories_document_types')
           ->join('char_document_types_i18n as doc', function($q) {
               $q->on('doc.document_type_id', '=', 'char_categories_document_types.document_type_id');
               $q->where('doc.language_id',1);
           })
           ->where('char_categories_document_types.category_id',$id)
           ->orderBy('doc.name')
           ->select('doc.name as name','doc.document_type_id as document_type_id','char_categories_document_types.scope as scope')
           ->get();
    }

    public static function getCategoryDocumentsMap()
    {
        $map = array();
        $query= \DB::table('char_categories_document_types')
               ->selectRaw("char_categories_document_types.*")->get();
        
        
        foreach($query as $key =>$value) {
                if(!isset($map[$value->category_id])){
                    $map[$value->category_id] = [];
                }
                
                $map[$value->category_id][]=(int)$value->document_type_id;
            }
            
        return $map;
    }
    public static function getCategoryDocuments($id,$target)
    {

        $query= \DB::table('char_categories_document_types')
            ->join('char_document_types_i18n as doc', function($q) {
                $q->on('doc.document_type_id', '=', 'char_categories_document_types.document_type_id');
                $q->where('doc.language_id',1);
            })
            ->where('char_categories_document_types.category_id',$id);
            if($target){

                if($target == 'family'){
                    $query->where('char_categories_document_types.scope','!=',3);
                }elseif($target == 'case'){
                    $query->where('char_categories_document_types.scope',3);
                }
            }

        return $query ->orderBy('doc.name')
                       ->select('doc.name as name','doc.document_type_id as document_type_id','char_categories_document_types.scope as scope')
                       ->get();
    }

    public static function getCategoryRequiredDocument($id)
    {

        return \DB::table('char_document_types_i18n')
            ->leftjoin('char_categories_document_types', function($q) use($id){
                $q->on('char_document_types_i18n.document_type_id','=','char_categories_document_types.document_type_id');
                $q->where('char_categories_document_types.category_id','=',$id);

            })
            ->where('char_document_types_i18n.language_id','=',1)
            ->groupBy('char_document_types_i18n.document_type_id')
            ->selectRaw('char_document_types_i18n.name as name,char_document_types_i18n.document_type_id,
                         CASE WHEN char_categories_document_types.scope is null THEN "-" Else  char_categories_document_types.scope END AS owner,
                         CASE WHEN char_categories_document_types.scope is null THEN "false" Else "true" END AS checked
                       ')
            ->get();
    }

    public static function getPersonCategoryDocuments($category_id,$person_id,$scope,$action,$language_id)
    {

        $query= \DB::table('char_categories_document_types')
            ->join('char_document_types_i18n', function($q) use ($language_id) {
                $q->on('char_document_types_i18n.document_type_id', '=', 'char_categories_document_types.document_type_id');
                $q->where('char_document_types_i18n.language_id',$language_id);
            })
            ->where('char_categories_document_types.category_id',$category_id)
            ->where('char_categories_document_types.scope',$scope);

        if($action=='show') {
            $query->join('char_persons_documents', function($q) use($person_id){
                $q->on('char_persons_documents.document_type_id', '=', 'char_categories_document_types.document_type_id');
                $q->where('char_persons_documents.person_id',$person_id);
            })
                ->join('doc_files','doc_files.id','=','char_persons_documents.document_id')
                ->selectRaw('doc_files.filepath');

        }else{
            $query->leftjoin('char_persons_documents', function($q) use($person_id){
                $q->on('char_persons_documents.document_type_id', '=', 'char_categories_document_types.document_type_id');
                $q->where('char_persons_documents.person_id',$person_id);
            });
        }

        if($action!='show') {
            $query->selectRaw("char_document_types_i18n.document_type_id as document_type_id,char_persons_documents.document_id as file_id");
        }

        return $query->orderBy('char_document_types_i18n.name')
                     ->selectRaw("'$person_id' as person_id,
                               char_document_types_i18n.name,
                               char_document_types_i18n.document_type_id,
                               char_categories_document_types.scope")
                    ->get();
    }

}
