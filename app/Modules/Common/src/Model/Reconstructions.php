<?php

namespace Common\Model;

class Reconstructions  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_reconstructions';
    protected $primaryKey='case_id';
    protected $fillable = [ 'case_id','promised','organization_name'];
    public $timestamps = false;

    public static function saveReconstruction($case_id,$inputs){

        if(\Common\Model\Reconstructions::where(["case_id"=>$case_id])->first()){
            \Common\Model\Reconstructions::where("case_id",$case_id)->update($inputs);
        }else{
            $inputs['case_id']=$case_id;
            \Common\Model\Reconstructions::create($inputs);
        }
        return true;
    }
}
