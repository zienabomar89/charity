<?php


namespace Common\Model;

class Template extends \Illuminate\Database\Eloquent\Model
{
    const TPL_CANDIDATE = 'candidate';
    const TPL_REPORTS = 'reports';

    protected $table = 'char_templates';
    protected $fillable = ['organization_id','filename','template'];
    protected $hidden = ['created_at','updated_at'];

    public function getCategoriesTemplate()
    {
        return
            \DB::table('char_categories')
                ->join('char_templates as temp','temp.category_id','=','char_categories.id')
                ->where('char_categories.type',1)
                ->select('char_categories.name as name','char_templates.* as scope')
                ->get();

    }
}
