<?php
namespace Common\Model\Relays;

use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use App\Http\Helpers;


class Relays  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_relays';
    protected $fillable = ['person_id','category_id','user_id','organization_id'];

    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];

    public static function filter($page,$filters)
    {
        $condition = [];
        $int_ =  ['category_id',
            'city', 'country',
                  'governarate', 'location_id','mosques_id', 'adscountry_id', 'adsdistrict_id', 
                 'adsmosques_id', 'adsneighborhood_id', 'adsregion_id', 'adssquare_id','category_id','user_id','organization_id'];

        $filters_=['category_type','first_name', 'second_name', 'third_name', 'last_name','id_card_number','street_address'];
        foreach ($filters as $key => $value) {
            if(in_array($key,$filters_) || in_array($key, $int_)) {
                if($value != ""){
                    if(in_array($key, $int_)) {
                        $data = [$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, [$key => $value]);
                    }
               }
            }
        }
        $category_type = $filters['category_type'];
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;
        $organization_id = $user->organization_id;
        $UserOrgLevel =$UserOrg->level;

        $relays =\DB::table('char_relays')
            ->join('char_cases', function($join)  use ($organization_id) {
                $join->on('char_cases.person_id','char_relays.person_id' );
                $join->on('char_cases.category_id','char_relays.category_id' );
                $join->where('char_cases.organization_id', '=',$organization_id );
            })
            ->join('char_categories', function ($join) use($category_type){
                $join->on('char_categories.id', '=', 'char_cases.category_id');
                $join->where('char_categories.type', $category_type);
            })
            ->selectRaw('char_relays.id,char_cases.person_id')->get();

        $notIn = [];
        $whereRaw =null;

        foreach ($relays as $k=>$v){
            $notIn[]=$v->id;
        }

        $query=\DB::table('char_relays_view');

        if($category_type == 2) {
            if ($UserOrgLevel == Organization::LEVEL_BRANCH_CENTER) {
                $mainConnector = OrgLocations::getConnected($organization_id);
                $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
                $whereRaw = "(
                                                adsdistrict_id is null   OR
                                                ( adsdistrict_id  is not null  AND adsdistrict_id IN (" . join(",", $districts) . ") )
                                              )";
            }
            elseif ($UserOrgLevel == Organization::LEVEL_BRANCHES) {
                $mainConnector = OrgLocations::getConnected($organization_id);
                $mosques = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_MOSQUES, $mainConnector, true);
                $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
                $whereRaw ="(
                                          (
                                              adsdistrict_id  is not null AND
                                              adsmosques_id IN (" . join(",", $mosques) . ")
                                          )
                                           or
           
           (
                                             adsdistrict_id   IN (" . join(",", $districts) . ")  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
             or
           
           (
               adsdistrict_id   is  null  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
                                         
                                          )";

            }
        }

        if(sizeof($notIn) >0){
            $query->whereNotIn('char_relays_view.id',$notIn );
        }

        if(!is_null($whereRaw)){
            $query->whereRaw($whereRaw);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['first_name', 'second_name', 'third_name', 'last_name','street_address'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
//                                $q->where($key ,'like'  ,'%' . $value . '%');
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $created_at_to=null;
        $created_at_from=null;

        if(isset($filters['created_at_to']) && $filters['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($filters['created_at_to']));
        }
        if(isset($filters['created_at_from']) && $filters['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($filters['created_at_from']));
        }
        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween( 'char_relays_view.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('char_relays_view.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('char_relays_view.created_at', '<=', $created_at_to);
        }

        $query->selectRaw("char_relays_view.*");

        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'desc');
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'asc');
            }
        }


        if(!$order){
            $query->orderBy('char_relays_view.created_at','desc');
        }


        if($filters['action'] =='xlsx'){
            return  $query->get();
        }

        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page);
    }

}
