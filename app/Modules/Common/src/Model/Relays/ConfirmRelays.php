<?php
namespace Common\Model\Relays;
use App\Http\Helpers;


class ConfirmRelays  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_relays_log';
    protected $fillable = [ 'relay_id','user_id','organization_id'];

    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];

    public static function filter($page,$organization_id,$filters)
    {
        $condition = [];


        $int_ =  ['city', 'country',
            'governarate', 'location_id','mosques_id', 'adscountry_id', 'adsdistrict_id',
            'adsmosques_id', 'adsneighborhood_id', 'adsregion_id', 'adssquare_id','category_id','user_id','organization_id'];

        $relaysFilters =  ['category_id','user_id','organization_id','street_address'];
        $personFilters =['city', 'country',
            'governarate', 'location_id','mosques_id', 'adscountry_id', 'adsdistrict_id',
            'adsmosques_id', 'adsneighborhood_id', 'adsregion_id', 'adssquare_id','first_name', 'second_name', 'third_name', 'last_name','id_card_number'];
        foreach ($filters as $key => $value) {

            if(in_array($key,$relaysFilters)){
                if(in_array($key, $int_)&& $value != "" ) {
                    if(in_array($key, $int_)) {
                        $data = ['char_relays_log.'.$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, ['char_relays_log.'.$key => $value]);
                    }
                }
            }elseif(in_array($key,$personFilters)){
                if ($value != "" ) {
                    if(in_array($key, $int_)) {
                        $data = ['char_persons.'.$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, ['char_persons.'.$key => $value]);
                    }
                }
            }

        }

        $category_type= 1;
        if(isset($filters['category_type'])){
            $category_type = $filters['category_type'];
        }
        $query=\DB::table('char_relays_log')
                ->join('char_relays','char_relays.id',  '=', 'char_relays_log.relay_id')
                ->join('char_cases', function($join)  use ($organization_id) {
                    $join->on('char_cases.person_id','char_relays.person_id' );
                    $join->on('char_cases.category_id','char_relays.category_id' );
                    $join->on('char_cases.organization_id','char_relays_log.organization_id' );
                })
                ->join('char_categories', function($join) use ($category_type)  {
                    $join->on('char_categories.id',  '=', 'char_relays.category_id');
                    $join->where('char_categories.type',$category_type);
               })
               ->join('char_persons', 'char_persons.id', '=', 'char_relays.person_id')
               ->leftjoin('char_persons_contact as mobNo', function($join)  {
                    $join->on('mobNo.person_id',  '=', 'char_relays.person_id');
                    $join->where('mobNo.contact_type','primary_mobile');
               })
               ->leftjoin('char_persons_contact as phoNo', function($join)  {
                    $join->on('phoNo.person_id',  '=', 'char_relays.person_id');
                    $join->where('phoNo.contact_type','phone');
                })
                ->join('char_users As conUser','conUser.id',  '=', 'char_relays_log.user_id')
                ->join('char_users','char_users.id',  '=', 'char_relays.user_id');

//              ->where('char_relays_log.organization_id', '=',$organization_id )

                $user = \Auth::user();
                $UserType=$user->type;

                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_relays_log.organization_id',$organization_id);
                        $anq->orwherein('char_relays_log.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }else{

                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_relays_log.organization_id', function($qudery) use($organization_id) {
                            $qudery->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }


              $query->selectRaw(" char_relays_log.*,
                              char_persons.first_name, 
                              char_persons.second_name,
                              char_persons.third_name,
                              char_persons.last_name,
                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                   ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                              char_cases.category_id,
                              char_cases.person_id,
                              char_cases.id as case_id,
                              char_persons.id_card_number,char_persons.old_id_card_number,
                              char_categories.name AS category_name,
                              ifnull(`mobNo`.`contact_value`,'-') as primary_mobile,
                               ifnull(`phoNo`.`contact_value`,'-') as phone,
                              CASE WHEN char_relays.created_at is null THEN '-' Else char_relays.created_at  END  AS relay_date,
                              CASE WHEN char_relays_log.created_at is null THEN '-' Else char_relays_log.created_at  END  AS confirm_relay_date,
                              CONCAT(conUser.firstname,' ',conUser.lastname)as confirm_user_name,
                              CONCAT(char_users.firstname,' ',char_users.lastname)as user_name")

        ;


        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $created_at_to=null;
        $created_at_from=null;

        if(isset($filters['created_at_to']) && $filters['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($filters['created_at_to']));
        }
        if(isset($filters['created_at_from']) && $filters['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($filters['created_at_from']));
        }
        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween( 'char_relays_log.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('char_relays_log.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('char_relays_log.created_at', '<=', $created_at_to);
        }

        if($filters['action'] =='xlsx'){
            
            
            $query->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
                ->addSelect('char_organizations.name AS organization_name');

            $language_id =1;

            if($category_type == 1) {
                $query->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.mosques_id is null THEN '-' Else mosques_name.name END   AS mosque_name_");

                $query->leftjoin('char_locations_i18n As location_name', function($join) use ($language_id)  {
                    $join->on('char_persons.location_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.location_id is null THEN '-' Else location_name.name END   AS nearLocation_");

                $query->leftjoin('char_locations_i18n As city_name', function($join) use ($language_id)  {
                    $join->on('char_persons.city', '=','city_name.location_id' )
                        ->where('city_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN city_name.name  is null THEN '-' Else city_name.name END   AS city_");

                $query->leftjoin('char_locations_i18n As district_name', function($join)  use ($language_id) {
                    $join->on('char_persons.governarate', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN district_name.name  is null THEN '-' Else district_name.name END   AS governarate_");

                $query->leftjoin('char_locations_i18n As country_name', function($join)  use ($language_id) {
                    $join->on('char_persons.country', '=','country_name.location_id' )
                        ->where('country_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN country_name.name  is null THEN '-' Else country_name.name END   AS country_");

                $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' ',
                        ifnull(district_name.name,' '),' ',
                        ifnull(city_name.name,' '),' ', 
                        ifnull(location_name.name,' '),' ',
                        ifnull(mosques_name.name,' '),' ',
                        ifnull(char_persons.street_address,' '))
                                            AS address");

            }else{

                $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                        ->where('country_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN '-' Else country_name.name END   AS country_");


                $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                    $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adsdistrict_id  is null THEN '-' Else district_name.name END   AS governarate_");

                $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adsregion_id is null THEN '-' Else region_name.name END   AS region_name_");

                $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearLocation_");

                $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adssquare_id is null THEN '-' Else square_name.name END   AS square_name_");

                $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                    ->selectRaw("CASE WHEN char_persons.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosque_name_");

                $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' - ',
                                                            ifnull(district_name.name,' '),' - ',
                                                            ifnull(region_name.name,' '),' - ',
                                                            ifnull(location_name.name,' '),' - ',
                                                            ifnull(square_name.name,' '),' - ',
                                                            ifnull(mosques_name.name,' '),' - ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address");

            }

            $query->selectRaw("CASE WHEN char_persons.street_address  is null THEN '-' Else char_persons.street_address END AS street_address");

        }

        $query->groupBy('char_relays_log.relay_id');

        $map = [
            "id_card_number" => 'char_payments.administration_fees',
            "full_name" => 'char_persons.first_name',
            "category_name" => 'char_categories.name',
            "primary_mobile" => 'mobNo.contact_value',
            "created_at" => 'char_relays_log.created_at',
            "user_name" => 'char_users.firstname',
            "confirm_user_name" => 'conUser.firstname'
        ];
        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                if($key_ == 'full_name'){
                    $query->orderBy('char_persons.first_name','desc');
                    $query->orderBy('char_persons.second_name','desc');
                    $query->orderBy('char_persons.third_name','desc');
                    $query->orderBy('char_persons.last_name','desc');
                }elseif ($key_ == 'user_name'){
                    $query->orderBy('char_users.firstname','desc');
                    $query->orderBy('char_users.lastname','desc');
                }elseif ($key_ == 'confirm_user_name'){
                    $query->orderBy('conUser.firstname','desc');
                    $query->orderBy('conUser.lastname','desc');
                }else{
                    $query->orderBy($map[$key_],'desc');
                }
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                if($key_ == 'full_name'){
                    $query->orderBy('char_persons.first_name','asc');
                    $query->orderBy('char_persons.second_name','asc');
                    $query->orderBy('char_persons.third_name','asc');
                    $query->orderBy('char_persons.last_name','asc');
                }elseif ($key_ == 'user_name'){
                    $query->orderBy('char_users.firstname','asc');
                    $query->orderBy('char_users.lastname','asc');
                }elseif ($key_ == 'confirm_user_name'){
                    $query->orderBy('conUser.firstname','asc');
                    $query->orderBy('conUser.lastname','asc');
                }else{
                    $query->orderBy($map[$key_],'asc');
                }
            }
        }

        if(!$order){
            $query->orderBy('char_relays_log.created_at','desc');
        }


        if($filters['action'] =='xlsx'){
            return  $query->get();
        }

        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->paginate()->total());
        return $query->paginate($records_per_page);
    }

}
