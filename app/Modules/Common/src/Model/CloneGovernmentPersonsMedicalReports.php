<?php

namespace Common\Model;

class CloneGovernmentPersonsMedicalReports  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_medical_reports';
    protected $fillable = ['IDNO','MR_CODE','LOC_NAME_AR','MR_PATIENT_CD','DOCTOR_NAME',
                           'MR_CREATED_ON','DREF_NAME_AR','MR_DIAGNOSIS_AR','MR_DIAGNOSIS_EN',
                           'MR_COMPLAINT','MR_EXAMINATION'];
    public $timestamps = false;

}
