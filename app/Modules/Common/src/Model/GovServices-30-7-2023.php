<?php

namespace Common\Model;

use Log\Model\Log;

class GovServices_
{

    // check card number
    public static function checkCard($card)
    {
        $card = str_replace(' ', '', $card);
        $card = trim($card);
        if (strlen($card) != 9) {
            return false;
        }
        $identity = $card;
        $lastDig = $identity[8];
        $digit1 = $identity[0] * 1;
        $digit2 = $identity[1] * 2;
        $digit3 = $identity[2] * 1;
        $digit4 = $identity[3] * 2;
        $digit5 = $identity[4] * 1;
        $digit6 = $identity[5] * 2;
        $digit7 = $identity[6] * 1;
        $digit8 = $identity[7] * 2;

        $checkLast = (int) $digit1 + (int) $digit3  + (int) $digit5 + (int) $digit7;
        if (strlen($digit2) == 2) {
            $digit22 = (string) $digit2;
            $checkLast += (int) $digit22[0];
            $checkLast += (int) $digit22[1];
        } else {
            $checkLast += (int)$digit2;
        }

        if (strlen($digit4) == 2) {
            $digit44 = (string) $digit4;
            $checkLast += (int) $digit44[0];
            $checkLast += (int) $digit44[1];
        } else {
            $checkLast += (int)$digit4;
        }
        if (strlen($digit6) == 2) {
            $digit66 =  (string)$digit6;
            $checkLast += (int) $digit66[0];
            $checkLast += (int) $digit66[1];
        } else {
            $checkLast += (int)$digit6;
        }

        if (strlen($digit8) == 2) {
            $digit88 =  (string) $digit8;
            $checkLast += (int) $digit88[0];
            $checkLast += (int) $digit88[1];
        } else {
            $checkLast += (int) $digit8;
        }

        $checkLast = (string)$checkLast;
        $checkLast_length = strlen($checkLast);

        if($checkLast[$checkLast_length - 1]==0)
            $lastOne=10;
        else
            $lastOne =  $checkLast[$checkLast_length - 1];

        $lastDigit_inCheck = 10 - $lastOne;

        if ($lastDig != $lastDigit_inCheck) {
            return false;
        } else {
            return true;
        }
    }

    // procedure to get citizen data using card number
    public static function getPersonByCard($card)
    {

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its basic data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $card = (int)$card;
        $result = ['     row'=> [] ,'status' => false];
        $jsonData = [
            "WB_USER_NAME_IN" => "AIDCOMM4GOVDATA",
            "WB_USER_PASS_IN" => "DBDFB0FAD032E0518471E658FED26FED",
            "DATA_IN" => [
                "package" => "MOI_GENERAL_PKG",
                "procedure" => "CITZN_INFO",
                "ID" => $card
            ],
            "WB_AUDIT_IN" => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        $result['message'] = trans('common::application.not register card');
        return $result;
    }

    // procedure to get citizen data using name
    public static function getPersonByName($options = array())
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $defaults = array(
            'first_name' => "",
            'second_name' => "",
            'third_name' => "",
            'last_name' => "",
        );

        $name = $defaults['first_name'] .' ' .$defaults['second_name'].' '. $defaults['third_name'] .' ' .$defaults['last_name'];
        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for name') . ' ('. $name . ') '.
            trans('common::application.To view its basic data'));


        $options = array_merge($defaults, $options);

        $result = ['row'=> [] ,'status' => false];

        $jsonData = array(
            "WB_USER_NAME_IN"=>"AIDCOMM4GOVDATA",
            "WB_USER_PASS_IN"=>"DBDFB0FAD032E0518471E658FED26FED",
            "DATA_IN"=> array(
                "package"=>"MOI_BYNAME_PKG",
                "procedure"=>"CTZN_INFO",
                "FNAME" => $options['first_name'],
                "SNAME" => $options['second_name'],
                "TNAME" => $options['third_name'],
                "LNAME" => $options['last_name']
            ),
            "WB_AUDIT_IN"=> array(
                "ip"=>"10.12.0.32",
                "pc"=>"hima-pc"
            )
        );

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $result = ['row'=> $response->DATA ,'status' => true];
            }else{
                $result['message'] = trans('common::application.there is no data to show');
            }
        }

        return $result;
    }

    // procedure to get citizen photo  using card number
    public static function getPhoto($card)
    {

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its photo'));

        $card = (int)$card;
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            "WB_USER_NAME_IN" => "AIDCOMM4GOVDATA",
            "WB_USER_PASS_IN" => "DBDFB0FAD032E0518471E658FED26FED",
            "DATA_IN" => [
                "package"=>"MOI_PERSONALPHOTO_PKG",
                "procedure"=>"PERSONAL_PHOTO_BYID",
                "ID"=>$card
            ],
            "WB_AUDIT_IN" => [
                "ip"=>"10.12.0.32",
                "pc"=>"hima-pc"
            ]
        ];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getPhoto";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            if ($response) {
                $result['status'] =  true;
                $result['image'] =  $response;
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    // procedure to get citizen contact data  using card number
    public static function getPersonContactInfo($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its contact info'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MTIT_SSO_CONTACT_PKG",
                "procedure" => "GET_CONTACT_INFO_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_ENCODING, '');
        //curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response){
                if ($response->DATA && @count($response->DATA)) {
                    $row = $response->DATA;
                    $result = ['row'=> $row[0] ,'status' => true];
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }else{
                $result['message'] = trans('common::application.not register card');
            }

        }
        return $result;
    }

    // procedure to get citizen financial data to government employee using card number
    public static function getPersonFinancialInfo($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its employee data'));


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "LAB_EMP_FILE_PKG",
                "procedure" => "GET_EMP_INFO_BYID_PR",

//                "package" => "MOF_EMP_FILE_PKG",
//                "procedure" => "GET_EMP_FILE_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response){
                if ($response->DATA && @count($response->DATA)) {
                    $row = $response->DATA;
                    $result = ['row'=> $row[0] ,'status' => true];
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }else{
                $result['message'] = trans('common::application.not register card');
            }

        }
        return $result;
    }

    // procedure to get citizen financial data to non government employee using card number
    public static function getPersonNoGovFinancialInfo($card)
    {

        $card = (int)$card;

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');


        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "LAB_FILE_PKG",
                "procedure" => "GET_WORKERS_DATA_BYID_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    // procedure to get citizen commercial data using card number
    public static function getPersonCommercialRecords($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its commercial data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package"=>"MNE_PERSONS_GENERAL_PKG",
                "procedure"=>"GET_COMMERCIAL_REC_PR",
                "PERSON_ID"=>$card,
                "REC_CODE"=>""
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    public static function getPersonCommercialRecords_($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its commercial data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package"=>"MNE_PERSONS_GENERAL_PKG",
                "procedure"=>"GET_COMMERCIAL_REC_PR",
                "PERSON_ID"=>$card,
                "REC_CODE"=>""
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row ,'status' => true];
            }else{
                $result = ['row'=> [] ,'status' => false];
            }
        }
        return $result;
    }

    // procedure to get company commercial data using card number
    public static function getCommCommercialInfo($REC_CODE)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package"=>"MNE_COMM_GENERAL_PKG",
                "procedure"=>"GET_COMMERCIAL_REC_PR",
                "REGISTER_NO"=>"",
                "REC_CODE"=>$REC_CODE
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    // procedure to get citizen health data to using card number
    public static function getPersonHealthInfo($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its health data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOH_GENERAL_PKG",
                "procedure" => "MEDICAL_REPORTS_GET_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $size_ = sizeof($row);
                if($size_ > 0)
                    $result = ['row'=> $row[($size_-1)] ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    public static function getPersonMedicalReports($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its health data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOH_GENERAL_PKG",
                "procedure" => "MEDICAL_REPORTS_GET_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row ,'status' => true];
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }
        return $result;
    }

    // procedure to get citizen social relations list to using card number
    public static function getPersonRelations($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its relations'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_PKG",
                "procedure" => "REL_CTZN_INFO_DET",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $childs  = $response->DATA;
                $result = ['row'=> $childs ,'status' => true];
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }
        }
        return $result;
    }

    public static function getPersonRelated($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its relations'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');


        $result = ['row'=> [] ,'status' => false];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            "WB_USER_NAME_IN"=>"AIDCOMM4GOVDATA",
            "WB_USER_PASS_IN"=>"DBDFB0FAD032E0518471E658FED26FED",
            'DATA_IN' => [
                "package" => "MOI_ALL_REL_PKG",
                "procedure" => "MOI_CTZN_REL_GET",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if($response){
                if ($response->DATA && @count($response->DATA)) {
                    $result = ['rows'=> $response->DATA ,'status' => true];
                }else{
                    $result['message'] = trans('common::application.There are no registered children');
                }
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }

        }
        return $result;
    }

    public static function getPersonRelatedCard($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its relations'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'kinship'=> [] ,'SEX_CD'=> [] ,'status' => false ];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            "WB_USER_NAME_IN"=>"AIDCOMM4GOVDATA",
            "WB_USER_PASS_IN"=>"DBDFB0FAD032E0518471E658FED26FED",
            'DATA_IN' => [
                "package" => "MOI_ALL_REL_PKG",
                "procedure" => "MOI_CTZN_REL_GET",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

//        REL_CI_ID_NUM
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if($response){
                if ($response->DATA && @count($response->DATA)) {
                    $related  = $response->DATA;
                    $result['status'] =true;
                    $result['row'] =[];

                    if(sizeof($related) > 0){
                        foreach ($related as $key=>$value){
                            if(!in_array($value->REL_CI_ID_NUM ,$result['row'])){
                                if((is_null($value->REL_CI_DEAD_DT) || $value->REL_CI_DEAD_DT == 'null')){
                                    if($value->REL_DESC == 'wife'){
                                        $result['row'][]= $value->REL_CI_ID_NUM;
                                        $result['kinship'][$value->REL_CI_ID_NUM]= $value->REL_DESC;
                                        $result['SEX_CD'][$value->REL_CI_ID_NUM]= $value->REL_CI_SEX_CD;
                                    }else{
                                        if($value->REL_CI_PERSONAL_CD == '10' || $value->REL_CI_PERSONAL_CD == "10" || $value->REL_CI_PERSONAL_CD == 10){
                                            if($value->REL_CI_PERSONAL_CD != 20 ){
                                                $result['row'][]= $value->REL_CI_ID_NUM;
                                                $result['kinship'][$value->REL_CI_ID_NUM]= $value->REL_DESC;
                                                $result['SEX_CD'][$value->REL_CI_ID_NUM]= $value->REL_CI_SEX_CD;
                                            }else{
                                                if((is_null($value->REL_CI_DEAD_DT) || $value->REL_CI_DEAD_DT == 'null')){
                                                    $result['row'][]= $value->REL_CI_ID_NUM;
                                                    $result['kinship'][$value->REL_CI_ID_NUM]= $value->REL_DESC;
                                                    $result['SEX_CD'][$value->REL_CI_ID_NUM]= $value->REL_CI_SEX_CD;
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }

                    if(sizeof($result['row']) == 0 ){
                        $result['status'] =false;
                        $result['message'] = trans('common::application.There are no registered children');
                    }
                }else{
                    $result['message'] = trans('common::application.There are no registered children');
                }
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }

        }
        return $result;
    }

    // procedure to all citizen social relations list to using card number (parent , wives , husband , child)
    public static function getAllPersonRelations($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its relations'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_NEW_PKG",
                "procedure" => "MOI_REL_CTZN_INFO_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $childs  = $response->DATA;
                $result = ['row'=> $childs ,'status' => true];
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }
        }
        return $result;
    }

    // procedure to get citizen children list to using card number
    public static function getPersonChild($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its children'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_PKG",
                "procedure" => "SON_REL_CTZN_INFO",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $childs  = $response->DATA;
                $rows =[];
                if(sizeof($childs) > 0){
                    foreach ($childs as $key=>$value){
                        $rows[]= $value->IDNO_RELATIVE;
                    }
                }

                $result = ['row'=> $rows ,'status' => true];
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }
        }
        return $result;
    }

    // procedure to get citizen single children list to using card number
    public static function getSinglePersonChild($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its children'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_PKG",
                "procedure" => "SON_REL_CTZN_INFO",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $childs  = $response->DATA;
                $rows =[];
                if(sizeof($childs) > 0){
                    foreach ($childs as $key=>$value){
                        $rows[]= $value->IDNO_RELATIVE;
                    }
                }

                $result = ['row'=> $rows ,'status' => true];
            }else{
                $result['message'] = trans('common::application.There are no registered children');
            }
        }
        return $result;
    }

    // procedure to get citizen wives list to using card number
    public static function getPersonWives($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its wives'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_MPWH_GENERAL_PKG",
                "procedure" => "GET_WIFES_DETAIL",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $result = ['row'=> $response->DATA ,'status' => true];
            }else{
                $result['message'] = trans('common::application.There are no registered wives');
            }
        }
        return $result;
    }

    // procedure to get citizen wives card to using card number
    public static function getPersonWivesCard($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its wives'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_MPWH_GENERAL_PKG",
                "procedure" => "GET_WIFES_DETAIL",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $wives  = $response->DATA;
                $result['status'] =true;
                $result['row'] =[];
                if(sizeof($wives) > 0){
                    foreach ($wives as $key=>$value){
                        if(!in_array($value->IDNO_RELATIVE ,$result['row'])){
                            $result['row'][]= $value->IDNO_RELATIVE;
                        }
                    }
                }
            }else{
                $result['message'] = trans('common::application.There are no registered wives');
            }
        }
        return $result;
    }

    // procedure to get citizen aid Committee status to using card number
    public static function aidsCommitteeStatus($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its aid committee status'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $url = "http://newzakat.atyafco.com/api/payee?id=".$card;

        $jsonData = array(
            'key: MviUlMacEbUxQAWK2txepa9SSoqTCrs6tVjavqagLLBUhTeSJ3TxjrLTe84siDTZ',
            'Content-Type: application/x-www-form-urlencoded',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
        } else {
            $response = json_decode($response);

            if ($response) {
                if ($response->status) {
                    $result = ['row'=> $response->data,'status' => true];
                }else{
                    $result = ['status' => false , 'message' => trans('common::application.The person is not beneficiary')];
                }
            }else{
                $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
            }

        }
        return $result;
    }

    // procedure to get citizen social affairs status to using card number
    public static function socialAffairsStatus($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its social affairs receipt'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOSA_GENERAL_PKG",
                "procedure" => "GET_AID_INFO_BYID",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result = ['status' => false , 'message' => trans('common::application.The person is not beneficiary')];
            }
        }
        return $result;
    }

    // procedure to get citizen social affairs receipt list to using card number
    public static function socialAffairsReceipt($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its social affairs receipt'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOSA_GENERAL_PKG",
                "procedure" => "GET_AID_RECIP_INFO_BYID",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $result = ['row'=> $response->DATA ,'status' => true];
            }else{
                $result = ['status' => false , 'message' => trans('common::application.The person is not beneficiary')];
            }
        }
        return $result;
    }

    public static function getPersonMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }
        return $row;
    }

    public static function mapToUpdate($data)
    {

        $row = [];
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }
        return $row;
    }

    public static function MapBasicToUpdate($data)
    {


        $row = [];
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date"
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }
        return $row;
    }

    public static function personRelatedMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "REL_CI_ID_NUM" =>  "id_card_number",
            "REL_CI_FIRST_ARB" =>  "first_name",
            "REL_CI_FATHER_ARB" =>  "second_name",
            "REL_CI_GRAND_FATHER_ARB" => "third_name",
            "REL_CI_FAMILY_ARB" =>  "last_name",
            "REL_CI_PRV_FAMILY_ARB" =>  "prev_family_name",
            "REL_CI_SEX_CD" => "gender",
            "REL_CI_PERSONAL_CD" =>  "marital_status_id",
            "REL_CI_PERSONAL_DESC" =>  "marital_status",
            "REL_CI_BIRTH_DT" =>  "birthday",
            "REL_CI_DEAD_DT" =>  "death_date"
        );

        $dates = ['birthday' , 'death_date'];
        $row['kinship_id'] =null;
        $row['kinship_name']='-';

        foreach ($data as $k=>$v) {
            if($k == 'REL_DESC'){
                if($v =='wife'){
                    $wife_kinship=\Setting\Model\Setting::where('id','wife_kinship')->first();
                    if($wife_kinship){
                        if(!is_null($wife_kinship->value) and $wife_kinship->value != "") {
                            $row['kinship_id'] =$wife_kinship->value;
                            $row['kinship_name']=  trans('common::application.wife_');
                        }
                    }
                }
                else if($v =='son' && $data->REL_CI_SEX_CD == 2){
                    $daughter_kinship = \Setting\Model\Setting::where('id', 'daughter_kinship')->first();
                    if($daughter_kinship){
                        if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
                            $row['kinship_id'] =$daughter_kinship->value;
                            $row['kinship_name']=  trans('common::application.daughter_');
                        }
                    }
                }
                else {
                    $son_kinship = \Setting\Model\Setting::where('id', 'son_kinship')->first();
                    if($son_kinship){
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $row['kinship_id'] =$son_kinship->value;
                            $row['kinship_name']=  trans('common::application.son_');
                        }
                    }
                }
            }
            else{
                if(isset($_translator[$k])){
                    if(in_array($_translator[$k],$dates)){
                        if(!is_null($v) && $v != " "){
                            $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                        }
                    }
                    else{
                        $row [$_translator[$k]]=$v;
                    }
                }
            }
        }

        return $row;
    }

    public static function inputsMap($data){

        $row = array("death_date"=>null,'card_type'=>"1");
        $_translator =array(
            "IDNO_RELATIVE" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
            "CI_CITY" =>  "governarate_name",
            "RELATIVE_CD" =>  "kinship",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        $SEX_CD_MAP = ['531'=> 1 ,'532'=> 2];
        $SOCIAL_STATUS_MAP = ['526'=> 20 ,'525'=> 10 ,'528' => 30 , '529'=>40 , '527' =>21];

        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif($k == 'SEX_CD'){
                    if(isset($SEX_CD_MAP[$v])){
                        $row [$_translator[$k]]=$SEX_CD_MAP[$v];
                    }else{
                        $row [$_translator[$k]]=$v;
                    }
                }
                elseif($k == 'SOCIAL_STATUS_CD'){
                    $row [$_translator[$k]]=$SOCIAL_STATUS_MAP[$v];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        if(isset($row['DETH_DT'])){
            if(!is_null($row['DETH_DT'])){
                $row ['marital_status_id']=4;
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }

        return $row;}

}