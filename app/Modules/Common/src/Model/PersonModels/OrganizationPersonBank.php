<?php

namespace Common\Model\PersonModels;

class OrganizationPersonBank  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_persons_banks';
    protected $fillable = [ 'person_id','bank_id','organization_id'];
    public $timestamps = false;
}
