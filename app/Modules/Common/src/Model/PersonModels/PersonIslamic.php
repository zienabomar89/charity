<?php

namespace Common\Model\PersonModels;

use Organization\Model\Organization;

class PersonIslamic  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_islamic_commitment';
    protected $primaryKey='person_id';
    protected $fillable = ['person_id', 'prayer','quran_parts', 'quran_chapters','prayer_reason','quran_reason','quran_center'];
    public $timestamps = false;


    public static function get_org_islamic_statistic($options,$Org)
    {
        $final=array();
        $age_8=date('Y-m-d', strtotime('-8 years'));
        $age_12=date('Y-m-d', strtotime('-12 years'));
        $age_15=date('Y-m-d', strtotime('-15 years'));

        foreach($Org as $key => $value){
            $sub=[];
            $total = 0;
            foreach($value as $k => $v) {
                $sub[$k]=$v;
            }

            $target=['enrollment','mosque','uncommitted'];
            foreach($target as $k => $v) {
                $sub[$v.'_total']         =self::char_get_boy_count($value->id,null,null,$k);
                $sub[$v.'_less_then_8']   =self::char_get_boy_count($value->id,$age_8,null,$k);
                $sub[$v.'_from_8_to_12']  =self::char_get_boy_count($value->id,$age_12,$age_8,$k);
                $sub[$v.'_from_12_to_15'] =self::char_get_boy_count($value->id,$age_15,$age_12,$k);
                $sub[$v.'_more_then_15']  =self::char_get_boy_count($value->id,null,$age_15,$k);
                $total +=  $sub[$v.'_total']  + $sub[$v.'_less_then_8']  + $sub[$v.'_from_8_to_12']  + $sub[$v.'_from_12_to_15'] + $sub[$v.'_more_then_15'] ;
            }
            $sub['total'] =$total;
//            $sub['total'] =self::char_get_boy_count($value->id,null,null,-1);
            $final[]=$sub;
        }
        return $final;
    }

    public static function char_get_boy_count($organization_id,$birth_date_from,$birth_date_to,$status)
    {

        $cases =\DB::table('char_cases')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->join('char_persons_islamic_commitment as i','i.person_id',  '=', 'char_cases.person_id')
            ->where('ca.type','=',1)
            ->where('char_cases.organization_id','=',$organization_id)
            ->where('c.gender','=',1);


        if($birth_date_from != null && $birth_date_to == null){
            $cases = $cases->whereDate('c.birthday', '>', $birth_date_from);
        }else if($birth_date_from == null && $birth_date_to != null){
            $cases = $cases->whereDate('c.birthday', '<', $birth_date_to);
        } else if($birth_date_from != null && $birth_date_to != null){
            $cases = $cases->whereBetween( 'c.birthday', [ $birth_date_from, $birth_date_to]);
        }

        switch ($status) {
            case 0: $cases = $cases->where('i.quran_center','==',1); break;
            case 1: $cases = $cases->where('i.prayer','=',0)->where('i.quran_center','!=',1);  break;
            case 2: $cases = $cases->where('i.prayer','!=',0)->where('i.quran_center','!=',1); break;

//                case 0: $cases = $cases->where('i.prayer','=',0)->where('i.quran_parts','!=',null); break;
//                case 1: $cases = $cases->where('i.prayer','=',0)->where('i.quran_parts','=',null);  break;
//                case 2: $cases = $cases->where('i.prayer','!=',0)->where('i.quran_parts','=',null); break;
        }

        $count=$cases->selectRaw("count(c.id) as count")->first();
        return $count->count;
    }

}

