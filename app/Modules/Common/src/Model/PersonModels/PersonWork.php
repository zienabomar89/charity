<?php
namespace Common\Model\PersonModels;

class PersonWork  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_work';
    protected $primaryKey='person_id';
    protected $fillable = ['person_id','working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location','has_other_work_resources'];
    public $timestamps = false;


}
