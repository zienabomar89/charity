<?php
namespace Common\Model\PersonModels;

class PersonResidence  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_residence';
    protected $primaryKey='person_id';
    protected $fillable = ['person_id','property_type_id','rent_value','roof_material_id','residence_condition',
                           'habitable','indoor_condition','house_condition','rooms','area','need_repair','repair_notes'];

    public $timestamps = false;

    public static function saveResidence($person_id,$inputs){

       if(\Common\Model\PersonModels\PersonResidence::where(["person_id"=>$person_id])->first()){
            \Common\Model\PersonModels\PersonResidence::where("person_id",$person_id)->update($inputs);
       }else{
           $inputs['person_id']=$person_id;
             \Common\Model\PersonModels\PersonResidence::create($inputs);
       }
        return true;
    }
}
