<?php

namespace Common\Model\PersonModels;

class PersonHealth  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_health';
    protected $primaryKey='person_id';
    protected $fillable = [ 'person_id','condition','details','disease_id', 'has_health_insurance',
                            'health_insurance_number', 'health_insurance_type',
                            'has_device', 'used_device_name', 'gov_health_details'];
    public $timestamps = false;

}
