<?php

namespace Common\Model\PersonModels;
use App\Http\Helpers;
use Common\Model\CaseModel;


class PersonKinship  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_kinship';
//    protected $primaryKey='person_id';
    protected $fillable = ['l_person_id','r_person_id','kinship_id'];
    public $timestamps = false;

    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('r_person_id', '=', $this->getAttribute($this->r_person_id))
              ->where('l_person_id', '=', $this->getAttribute($this->l_person_id));

        return $query;
    }


    public static function fetch($via,$edge,$edge_id,$kinship_id)
    {

        $person_id=null;
        $query = \DB::table('char_persons_kinship')
                    ->leftjoin('char_persons', function ($q) use ($edge){
                        if ($edge == 'l_person_id') {
                            $q->on('char_persons.id', '=', 'char_persons_kinship.r_person_id');
                        }else{
                            $q->on('char_persons.id', '=', 'char_persons_kinship.l_person_id');
                        }
                    });

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');


        if ($via == 'father') {
            $query->where('char_persons.gender', 1);
        }elseif ($via == 'mother') {
            $query->where('char_persons.gender', 2);
        } elseif ($via == 'wives') {
            $query->leftjoin('char_persons_health', function ($q) {
                    $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                })
             ->selectRaw("
                             char_persons.id,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                             CASE WHEN  char_persons.birthday is null THEN '-' Else  DATE_FORMAT(char_persons.birthday,'%Y/%m/%d') END AS birthday,
                             CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number,
                             CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                             CASE WHEN char_persons_health.condition is null THEN '-'
                                  WHEN char_persons_health.condition = 1 THEN '$perfect'
                                  WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                  WHEN char_persons_health.condition = 3 THEN '$special_needs'
                             END AS health_status "
                );
        }

        $query->where('char_persons_kinship.kinship_id', $kinship_id);

        if ($edge == 'l_person_id') {
            $query->where('char_persons_kinship.l_person_id', $edge_id)->selectRaw("char_persons.gender,char_persons_kinship.r_person_id as person_id");

        }
        if ($edge == 'r_person_id') {
            $query->where('char_persons_kinship.r_person_id', $edge_id)->selectRaw("char_persons.gender,char_persons_kinship.l_person_id as person_id");
        }

        if ($via == 'wives') {
            return $query->get();
        }
        $return=$query->first();

        if($return){
            $person_id=$return->person_id;
        }

        return $person_id;
    }

    public static function individual($filters)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $UserType=$user->type;
        $condition =array();
        $output=array();
        $all_organization=1;

        $char_cases = ['category_id','visitor','status'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id",
            "adsdistrict_id",
            "adsmosques_id",
            "card_type",
            "adsneighborhood_id",
            "adsregion_id",
            "adssquare_id" ];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition','house_condition','habitable','rent_value'];
        $char_persons_i18n = ['en_first_name', 'en_second_name', 'en_third_name', 'en_last_name'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location'];
        $char_persons_health = ['condition','disease_id'];
        $char_persons_education = ['grade','currently_study', 'authority','type','stage','level','year','school','points'];

        $reconstructions=['promised'];
        $secondary_mobile=['secondary_mobile'];
        $primary_mobile=['primary_mobile'];
        $phone=['phone'];
        $char_organization_persons_banks = ['bank_id'];
        $char_persons_banks = ['bank_id','branch_name','account_number','account_owner'];

        $char_persons_individual=['individual_id_card_number','individual_card_type','individual_first_name', 'individual_second_name','individual_third_name',
            'individual_gender','individual_marital_status_id','individual_location_id','individual_country','individual_governarate','individual_city',
            'individual_monthly_income','individual_birth_place','individual_nationality', 'individual_last_name','individual_mosques_id'];
        $individual_work = ['individual_working','individual_work_job_id','individual_work_location'];
        $individual_education = ['individual_grade', 'individual_authority','individual_type','individual_stage','individual_level','individual_year','individual_school','individual_points'];


        $char_persons_health_cnt =
        $char_persons_work_cnt  =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt =
        $primary_num_cnt = $secondary_num_cnt =  $phone_num_cnt = $reconstruct_cnt=
        $char_organization_persons_banks_cnt=
        $char_persons_banks_cnt =
        $char_persons_individual_cnt =
        $individual_work_cnt =
        $individual_education_cnt =  0;

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    if($key =='case_status'){
                        $data = ['char_cases.status'=> $value];
                    }else{
                        $data = ['char_cases.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $reconstructions)) {
                if ( $value != "" ) {

                    $data = ['char_reconstructions.'. $key => $value];
                    $reconstruct_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_i18n)) {
                if ( $value != "" ) {
                    $data = ['char_persons_i18n.' . substr($key,3)=>$value ];
                    $char_persons_i18n_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_education)) {
                if ( $value != "" ) {
                    $data = ['char_persons_education.' . $key => $value];
                    $char_persons_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $phone)) {
                if ( $value != "" ) {
                    $data = ['phone_num.contact_value' => $value]; $phone_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $primary_mobile)) {
                if ( $value != "" ) {
                    $data = ['primary_num.contact_value' => $value];
                    $primary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $secondary_mobile)) {
                if ( $value != "" ) {
                    $data = ['secondary_num.contact_value' => $value];
                    $secondary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_organization_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_organization_persons_banks.'. $key => $value];
                    $char_organization_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_persons_banks.'. $key => $value];
                    $char_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_individual)) {
                if ( $value != "" ) {
                    $data = ['individual.' . substr($key, 11) =>$value];
                    $char_persons_individual_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $individual_work)) {

                if ( $value != "" ) {
                    $data = ['individual_work.' . substr($key, 15) => $value];
                    $individual_work_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $individual_education)) {

                if ( $value != "" ) {
                    $data = ['individual_education.' . substr($key, 20) => $value];
                    $individual_education_cnt++;
                    array_push($condition, $data);
                }
            }
        }

        $query = \DB::table('char_cases')
            ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
            ->join('char_categories', function($q){
                $q->on('char_categories.id', '=', 'char_cases.category_id');
                $q->where('char_categories.type',2);
            })
            ->join('char_persons_kinship', 'char_cases.person_id', '=', 'char_persons_kinship.l_person_id')
            ->join('char_persons','char_persons.id','=','char_cases.person_id')
            ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
            ->leftjoin('char_kinship_i18n', function($q)use($language_id) {
                $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                $q->where('char_kinship_i18n.language_id',$language_id);
            })
            ->leftjoin('char_marital_status_i18n', function($q) use($language_id){
                $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                $q->where('char_marital_status_i18n.language_id',$language_id);
            })
            ->leftjoin('char_marital_status_i18n as ind_marital_status', function($q) use($language_id){
                $q->on('ind_marital_status.marital_status_id', '=', 'individual.marital_status_id');
                $q->where('ind_marital_status.language_id',$language_id);
            });

        $query->whereNull('char_cases.deleted_at');

        if($filters['action'] =="filter") {

            if($char_persons_i18n_cnt != 0){
                $query->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id');
            }
            if($char_residence_cnt != 0) {
                $query->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
            }
            if($char_persons_work_cnt != 0){
                $query ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
            }
            if($char_persons_health_cnt!= 0 ){
                $query->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
            }
            if($char_persons_education_cnt!= 0 ){
                $query->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
            }

            if($char_organization_persons_banks_cnt!= 0  || $char_persons_banks_cnt != 0) {
                $query->leftjoin('char_organization_persons_banks', function ($q) {
                    $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                    $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                })
                    ->leftjoin('char_persons_banks', function ($q) {
                        $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                    });

            }
            if($primary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as primary_num', function($q) {
                    $q->on('primary_num.person_id', '=', 'char_persons.id');
                    $q->where('primary_num.contact_type','primary_mobile');
                });
            }
            if($secondary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as secondary_num', function($q) {
                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
                    $q->where('secondary_num.contact_type','secondary_mobile');
                });
            }
            if($phone_num_cnt != 0){
                $query->leftjoin('char_persons_contact as phone_num', function($q) {
                    $q->on('phone_num.person_id', '=', 'char_persons.id');
                    $q->where('phone_num.contact_type','phone');
                });
            }
            if($reconstruct_cnt != 0){
                $query->leftjoin('char_reconstructions', function($q){
                    $q->on('char_reconstructions.case_id', '=', 'char_cases.id');
                });
            }

            if($individual_education_cnt!= 0 ){
                $query->leftjoin('char_persons_education as individual_education','individual_education.person_id','=','individual.id');
            }
            if($individual_work_cnt!= 0 ){
                $query->leftjoin('char_persons_work as individual_work','individual_work.person_id','=','individual.id')  ;
            }


        }
        else{

            if(isset($filters['persons'])){
                if(sizeof($filters['persons']) > 0 ){
                    $query->whereIn('char_persons_kinship.l_person_id',$filters['persons']);
                }
            }
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name',
                        'char_persons_i18n.first_name', 'char_persons_i18n.second_name', 'char_persons_i18n.third_name',
                        'char_persons_i18n.last_name','char_cases.visitor','char_persons_work.work_location','char_persons_banks.account_owner',
                        'individual.first_name', 'individual.second_name', 'individual.third_name', 'individual.last_name'];

                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            }else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }

        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true || $filters['all_organization'] ==='true'){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false || $filters['all_organization'] ==='false'){
                $all_organization=1;

            }

        }

        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null &&
                $filters['organization_ids'] !="" && $filters['organization_ids'] !=[]) {
                if(is_array($filters['organization_ids'])){
                    if(sizeof($filters['organization_ids']) != 0){
                        if($filters['organization_ids'][0]==""){
                            unset($filters['organization_ids'][0]);
                        }
                        $organizations =$filters['organization_ids'];
                    }
                }
            }

            $organization_category=[];
            if(isset($filters['organization_category']) && $filters['organization_category'] !=null &&
                $filters['organization_category'] !="" && $filters['organization_category'] !=[] && sizeof($filters['organization_category']) != 0) {
                if($filters['organization_category'][0]==""){
                    unset($filters['organization_category'][0]);
                }
                $organization_category = $filters['organization_category'];
            }

            if(!empty($organizations)){
                $query->where(function ($anq) use ($organizations,$organization_category) {
                    $anq->wherein('char_cases.organization_id',$organizations);
                    if(!empty($organization_category)){
                        $anq->wherein('org.category_id',$organization_category);
                    }
                });

            }else{
                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user,$organization_category) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });
                }
                else{
                    $query->where(function ($anq) use ($organization_id,$organization_category) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });
                }
            }



        }
        elseif($all_organization ==1){
            $query= $query->where('char_cases.organization_id',$organization_id);
        }

        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){


            $birthday_to=null;
            $birthday_from=null;


            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $query->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $query->whereDate('char_persons.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $query->whereDate('char_persons.birthday', '<=', $birthday_to);
            }

        }
        if((isset($filters['date_to']) && $filters['date_to'] !=null) || (isset($filters['date_from']) && $filters['date_from'] !=null)){


            $date_to=null;
            $date_from=null;

            if(isset($filters['date_to']) && $filters['date_to'] !=null){
                $date_to=date('Y-m-d',strtotime($filters['date_to']));
            }
            if(isset($filters['date_from']) && $filters['date_from'] !=null){
                $date_from=date('Y-m-d',strtotime($filters['date_from']));
            }
            if($date_from != null && $date_to != null) {
                $query->whereBetween( 'char_cases.created_at', [ $date_from, $date_to]);
            }elseif($date_from != null && $date_to == null) {
                $query->whereDate('char_cases.created_at', '>=', $date_from);
            }elseif($date_from == null && $date_to != null) {
                $query->whereDate('char_cases.created_at', '<=', $date_to);
            }


        }
        if((isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null) || (isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null)){
            $visited_at_to=null;
            $visited_at_from=null;

            if(isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null){
                $visited_at_to=date('Y-m-d',strtotime($filters['visited_at_to']));
            }
            if(isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null){
                $visited_at_from=date('Y-m-d',strtotime($filters['visited_at_from']));
            }

            if($visited_at_from != null && $visited_at_to != null) {
                $query->whereBetween( 'char_cases.visited_at', [ $visited_at_from, $visited_at_to]);
            }elseif($visited_at_from != null && $visited_at_to == null) {
                $query->whereDate('char_cases.visited_at', '>=', $visited_at_from);
            }elseif($visited_at_from == null && $visited_at_to != null) {
                $query->whereDate('char_cases.visited_at', '<=', $visited_at_to);
            }

        }
        if((isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !="")|| (isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !="")){

            $max_rank=null;
            $min_rank=null;

            if(isset($filters['max_rank']) && $filters['max_rank'] !=null && $filters['max_rank'] !=""){
                $max_rank=$filters['max_rank'];
            }
            if(isset($filters['min_rank']) && $filters['min_rank'] !=null && $filters['min_rank'] !=""){
                $min_rank=$filters['min_rank'];
            }

            if($min_rank != null && $max_rank != null) {
                $query->whereRaw(" ( char_cases.rank between ? and  ?)", array($min_rank, $max_rank));
            }
            elseif($max_rank != null && $min_rank == null) {
                $query->whereRaw(" $max_rank >= char_cases.rank");
            }
            elseif($max_rank == null && $min_rank != null) {
                $query->whereRaw(" $min_rank <= char_cases.rank");
            }
        }
        if((isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !="")||
            (isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !="")){

            $monthly_income_from=null;
            $monthly_income_to=null;
            if(isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !=""){
                $monthly_income_to=$filters['monthly_income_to'];
            }
            if(isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !=""){
                $monthly_income_from=$filters['monthly_income_from'];
            }

            if($monthly_income_from != null && $monthly_income_to != null) {
                $query->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
                $query->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
            elseif($monthly_income_to != null && $monthly_income_from == null) {
                $query->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
            }
            elseif($monthly_income_to == null && $monthly_income_from != null) {
                $query->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
            }
        }

        if((isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !="")||
            (isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !="")){

            $max_family_count=null;
            $min_family_count=null;

            if(isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !=""){
                $max_family_count=$filters['max_family_count'];
            }
            if(isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !=""){
                $min_family_count=$filters['min_family_count'];
            }
            if($min_family_count != null && $max_family_count != null) {
                $query = $query->whereRaw(" ( char_get_family_count ( 'left',char_persons.id,null,char_persons.id ) between ? and  ?)", array($min_family_count, $max_family_count));
            }elseif($max_family_count != null && $min_family_count == null) {
                $query = $query->whereRaw(" $max_family_count >= char_get_family_count ( 'left',char_persons.id,null,char_persons.id )");
            }elseif($max_family_count == null && $min_family_count != null) {
                $query = $query->whereRaw(" $min_family_count <= char_get_family_count ( 'left',char_persons.id,null,char_persons.id )");
            }
        }

        if(isset($filters['Properties'])) {
            for ($x = 0; $x < sizeof($filters['Properties']); $x++) {
                $item=$filters['Properties'][$x];
                if(isset($item['exist'])){
                    if($item['exist'] != "") {
                        $has_property=(int)$item['exist'];
                        $id=(int)$item['id'];
                        $table='char_persons_properties as prop'.$x;
                        $query=$query ->join($table, function($q) use($x,$id,$has_property){
                            $q->on('prop'.$x.'.person_id', '=', 'char_persons.id');
                            $q->where('prop'.$x.'.property_id', '=', $id);
                            $q->where('prop'.$x.'.has_property', '=', $has_property);
                        });
                    }
                }

            }
        }
        if(isset($filters['AidSources'])) {
            for ($x = 0; $x < sizeof($filters['AidSources']); $x++) {
                $item=$filters['AidSources'][$x];
                if(isset($item['aid_take'])){
                    if($item['aid_take'] != "") {
                        $aid_take=(int)$item['aid_take'];
                        $id=(int)$item['id'];
                        $table='char_persons_aids as aid'.$x;
                        $query=$query ->join($table, function($q) use($x,$id,$aid_take){
                            $q->on('aid'.$x.'.person_id', '=', 'char_persons.id');
                            $q->where('aid'.$x.'.aid_source_id', '=', $id);
                            $q->where('aid'.$x.'.aid_take', '=', $aid_take);
                        });
                    }
                }

            }
        }
        if(isset($filters['Essential'])) {
            $custom=[];
            foreach($filters['Essential'] as $item){
                if((isset($item['max']) && $item['max'] !=null && $item['max'] !="" ) ||
                    isset($item['min']) && $item['min'] !=null && $item['min'] !="" ){
                    array_push($custom,$item);
                }
            }

            foreach($custom as $k => $value) {
                $max=null;
                $min=null;
                $id=(int)$value['id'];
                $table='char_cases_essentials as essent'.$k;
                $need='essent'.$k.'.needs';

                if(isset($value['max']) && $value['max'] !=null && $value['max'] !=""){
                    $max=(int)$value['max'];
                }
                if(isset($value['min']) && $value['min'] !=null && $value['min'] !=""){
                    $min=(int)$value['min'];
                }

                $query=$query ->join($table, function($q) use($k,$id,$min,$max) {
                    $q->on('essent' . $k . '.case_id', '=', 'char_cases.id');
                    $q->where('essent' . $k . '.essential_id', '=', $id);

                    if($min != null && $max != null) {
                        $q->where('essent'.$k.'.needs', '>=', $min);
                        $q->where('essent'.$k.'.needs', '<=', $max);
                    }elseif($max == null && $min != null) {
                        $q->where('essent'.$k.'.needs', '>=', $min);
                    }elseif($max != null && $min == null) {
                        $q->where('essent'.$k.'.needs', '<=', $max);
                    }
                });
            }
        }

        if((isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !="")||
            (isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !="")){

            $max_total_vouchers=null;
            $min_total_vouchers=null;
            if(isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !=""){
                $max_total_vouchers=$filters['max_total_vouchers'];
            }
            if(isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !=""){
                $min_total_vouchers=$filters['min_total_vouchers'];
            }

            if($min_total_vouchers != null && $max_total_vouchers != null) {
                $query->whereRaw(" ( char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') between ? and  ?)", array($min_total_vouchers, $max_total_vouchers));
            }
            elseif($max_total_vouchers != null && $min_total_vouchers == null) {
                $query->whereRaw(" $max_total_vouchers >= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
            }
            elseif($max_total_vouchers == null && $min_total_vouchers != null) {
                $query->whereRaw(" $min_total_vouchers <= char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')");
            }
        }

        if(isset($filters['voucher_sponsors']) || isset($filters['voucher_organization']) || isset($filters['voucher_ids']) ||
            isset($filters['got_voucher']) || isset($filters['voucher_to']) || isset($filters['voucher_from'])){

            $voucher_organization =[];
//            $voucher_organization =[$organization_id];

            if(isset($filters['voucher_organization']) && $filters['voucher_organization'] !=null &&
                $filters['voucher_organization'] !="" && $filters['voucher_organization'] !=[] && sizeof($filters['voucher_organization'] != 0)) {
                if($filters['voucher_organization'][0]==""){
                    unset($filters['voucher_organization'][0]);
                }
                $voucher_organization =$filters['voucher_organization'];
            }

            $voucher_sponsors=[];
            if(isset($filters['voucher_sponsors']) && $filters['voucher_sponsors'] !=null && $filters['voucher_sponsors'] !="" &&
                $filters['voucher_sponsors'] !=[] && sizeof($filters['voucher_sponsors'] != 0)) {
                if($filters['voucher_sponsors'][0]==""){
                    unset($filters['voucher_sponsors'][0]);
                }
                $voucher_sponsors =$filters['voucher_sponsors'];
            }

            $voucher_ids=[];
            if(isset($filters['voucher_ids']) && $filters['voucher_ids'] !=null && $filters['voucher_ids'] !="" &&
                $filters['voucher_ids'] !=[] && sizeof($filters['voucher_ids'] != 0)) {
                if($filters['voucher_ids'][0]==""){
                    unset($filters['voucher_ids'][0]);
                }
                $voucher_ids =$filters['voucher_ids'];
            }

            $voucher_to=null;
            $voucher_from=null;
            if(isset($filters['voucher_to']) && $filters['voucher_to'] !=null){
                $voucher_to=date('Y-m-d',strtotime($filters['voucher_to']));
            }
            if(isset($filters['voucher_from']) && $filters['voucher_from'] !=null){
                $voucher_from=date('Y-m-d',strtotime($filters['voucher_from']));
            }
            if(isset($filters['got_voucher']) && $filters['got_voucher'] !=null  && $filters['got_voucher'] !="") {
                if($filters['got_voucher']==0 ){
                    $query->whereNotIn('char_persons.id', function($query)use($organization_id,$user,$voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                        $query->select('person_id')->from('char_vouchers_persons');

                        $query->join('char_vouchers', function($q) use($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors){
                            $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                            $q->whereNull('char_vouchers.deleted_at');
                            if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0 ){

                                if(sizeof($voucher_organization) !=0){
                                    $q->wherein('char_vouchers.organization_id', $voucher_organization);
                                }
                                if(sizeof($voucher_sponsors) !=0){
                                    $q->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                }

                                if(sizeof($voucher_ids) !=0){
                                    $q->wherein('char_vouchers.id', $voucher_ids);
                                }
                            }else{
                                if($user->type == 2) {
                                    $q->where(function ($anq) use ($organization_id,$user) {
                                        $anq->where('char_vouchers.organization_id',$organization_id);
                                        $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                            $quer->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });

                                }
                                else{

                                    $q->wherein('char_vouchers.organization_id', function($quer) use($organization_id) {
                                        $quer->select('descendant_id')
                                            ->from('char_organizations_closure')
                                            ->where('ancestor_id', '=', $organization_id);
                                    });
                                }
                            }
                        });

                        if($voucher_from != null && $voucher_to != null) {
                            $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                        }elseif($voucher_from != null && $voucher_to == null) {
                            $query->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                        }elseif($voucher_from == null && $voucher_to != null) {
                            $query->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                        }



                    });
                }else{
                    $query->whereIn('char_persons.id', function($query)use($organization_id,$user,$voucher_organization,$voucher_to,$voucher_from,$voucher_ids,$voucher_sponsors){

                        $query->select('person_id')->from('char_vouchers_persons');

                        $query->join('char_vouchers', function($q) use($organization_id,$user,$voucher_organization,$voucher_ids,$voucher_sponsors){
                            $q->on('char_vouchers.id', '=', 'char_vouchers_persons.voucher_id');
                            $q->whereNull('char_vouchers.deleted_at');

                            if(sizeof($voucher_organization) !=0 || sizeof($voucher_sponsors) !=0 || sizeof($voucher_ids) !=0 ){

                                if(sizeof($voucher_organization) !=0){
                                    $q->wherein('char_vouchers.organization_id', $voucher_organization);
                                }
                                if(sizeof($voucher_sponsors) !=0){
                                    $q->wherein('char_vouchers.sponsor_id', $voucher_sponsors);
                                }

                                if(sizeof($voucher_ids) !=0){
                                    $q->wherein('char_vouchers.id', $voucher_ids);
                                }
                            }else{
                                if($user->type == 2) {
                                    $q->where(function ($anq) use ($organization_id,$user) {
                                        $anq->where('char_vouchers.organization_id',$organization_id);
                                        $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                            $quer->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });

                                }
                                else{

                                    $q->wherein('char_vouchers.organization_id', function($quer) use($organization_id) {
                                        $quer->select('descendant_id')
                                            ->from('char_organizations_closure')
                                            ->where('ancestor_id', '=', $organization_id);
                                    });
                                }
                            }
                        });

                        if($voucher_from != null && $voucher_to != null) {
                            $query->whereBetween( 'char_vouchers_persons.receipt_date', [ $voucher_from, $voucher_to]);
                        }elseif($voucher_from != null && $voucher_to == null) {
                            $query->whereDate('char_vouchers_persons.receipt_date', '>=', $voucher_from);
                        }elseif($voucher_from == null && $voucher_to != null) {
                            $query->whereDate('char_vouchers_persons.receipt_date', '<=', $voucher_to);
                        }
                    });
                }
            }
        }

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        if($filters['action'] =="filter") {

            $query->selectRaw("char_persons_kinship.*,
                              CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship_name,
                              CASE WHEN char_cases.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                               CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', 
                               ifnull(char_persons.last_name,' ')) AS full_name,
                               CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                               CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                               CASE WHEN char_persons.gender is null THEN ' '
                                   WHEN char_persons.gender = 1 THEN '$male'
                                   WHEN char_persons.gender = 2 THEN '$female'
                               END AS case_gender,char_persons.gender,
                                CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status_name,
                               CONCAT(ifnull(individual.first_name, ' '), ' ' ,ifnull(individual.second_name, ' '),' ',
                               ifnull(individual.third_name, ' '),' ', ifnull(individual.last_name,' ')) AS individual_name,
                               CASE WHEN individual.id_card_number is null THEN ' ' Else individual.id_card_number  END  AS individual_id_card_number,
                               CASE WHEN individual.marital_status_id is null THEN ' ' Else ind_marital_status.name  END  AS individual_marital_status,
                               CASE WHEN individual.gender is null THEN ' '
                                   WHEN individual.gender = 1 THEN '$male'
                                   WHEN individual.gender = 2 THEN '$female'
                               END AS individual_gender,
                              char_cases.id as case_id,
                              char_cases.category_id ,
                              char_cases.status ,
                              char_categories.name as category_name,
                              char_cases.organization_id,org.name as organization_name,
                              CASE WHEN  individual.birthday is null THEN ' ' Else 
                               EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), individual.birthday)))) END AS individual_age,
                               char_persons.family_cnt AS family_count,
                               char_persons.male_live AS male_count,
                               char_persons.female_live AS female_count,                                       
                               CASE WHEN char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') is null THEN 0
                               Else char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')END AS voucher_count");

            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            $output=$query->paginate($records_per_page);
        }
        else{
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', 
                               ifnull(char_persons.last_name,' ')) AS full_name,
                               CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                               CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                               char_categories.name as category_name,
                              org.name as organization_name,
                               CASE WHEN char_persons.gender is null THEN ' '
                                   WHEN char_persons.gender = 1 THEN '$male'
                                   WHEN char_persons.gender = 2 THEN '$female'
                               END AS case_gender,char_persons.gender,
                                CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status,
                               CONCAT(ifnull(individual.first_name, ' '), ' ' ,ifnull(individual.second_name, ' '),' ',
                               ifnull(individual.third_name, ' '),' ', ifnull(individual.last_name,' ')) AS individual_name,
                               CASE WHEN individual.id_card_number is null THEN ' ' Else individual.id_card_number  END  AS individual_id_card_number,
                               CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship_name,
                               CASE WHEN individual.marital_status_id is null THEN ' ' Else ind_marital_status.name  END  AS individual_marital_status,
                               CASE WHEN individual.gender is null THEN ' '
                                   WHEN individual.gender = 1 THEN '$male'
                                   WHEN individual.gender = 2 THEN '$female'
                               END AS individual_gender,
                              CASE WHEN  individual.birthday is null THEN ' ' Else 
                               EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), individual.birthday)))) END AS individual_age,
                               char_persons.family_cnt AS family_count,
                               char_persons.male_live AS male_count,
                               char_persons.female_live AS female_count,
                               CASE WHEN char_get_voucher_persons_count ( char_persons.id ,'$first', '$last') is null THEN 0
                               Else char_get_voucher_persons_count ( char_persons.id ,'$first', '$last')END AS voucher_count");
            $output = $query->get();
        }

        return $output;
    }

}
