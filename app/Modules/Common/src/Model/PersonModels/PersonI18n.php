<?php

namespace Common\Model\PersonModels;

class PersonI18n  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_i18n';
    protected $fillable = ['person_id','language_id','first_name','second_name','third_name','last_name'];
   public $timestamps = false;

    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('person_id', '=', $this->getAttribute($this->person_id))
              ->where('language_id', '=', $this->getAttribute($this->language_id));
       return $query;
    }


    public static function CreateOrUpdate($person_id,$language_id,$inputs){

        $exist = self::where(['person_id'=>$person_id ,'language_id' =>$language_id])->first();
        if(is_null($exist)){
            \DB::query('INSERT INTO char_persons_i18n (person_id,language_id,first_name,second_name,third_name,last_name) VALUES('
                .$person_id ."," .$language_id .","."'first_name"."','" . "first_name"."','" ."first_name"."','" . "first_name"."')");
        } else{
            self::where(['person_id'=>$person_id ,'aid_type' =>$language_id])->update($inputs);
        }

        return true;
    }

}
