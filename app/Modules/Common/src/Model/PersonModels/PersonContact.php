<?php

namespace Common\Model\PersonModels;

class PersonContact  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_contact';
    protected $fillable = [ 'person_id','contact_type','contact_value'];
    public $timestamps = false;

    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('person_id', '=', $this->getAttribute($this->person_id))
              ->where('contact_type', '=', $this->getAttribute($this->contact_type));

        return $query;
    }

    public static function getMobileNumbers($contact_type ,$receipt ,$source)
    {

        $user = \Auth::user();

        $query =\DB::table('char_persons')
            ->leftjoin('char_persons_contact', function($q) use($contact_type,$receipt,$user) {
                $q->on('char_persons_contact.person_id','=','char_persons.id');
                $q->where('contact_type',$contact_type);
            });

            $query->where(function ($q) use ($user,$source,$receipt) {
//                     $q->where('type', 1);
                     $q->where('id', '!=', $user->organization_id);

                        
                    if($source == 'project'){

                        $q->whereIn('id', function ($q) use ($receipt) {
                            $q->select('person_id')
                                ->from('aid_projects_persons')
                                ->where( function ($qi) use ($receipt) {
                                    $qi->where('status', '=', 3)
                                       ->whereIn('project_id', $receipt);
                                });
                       });
                       
                    }elseif($source == 'central_project'){
                        
                        if($user->type == 2){
                            $q->whereIn('id', function ($q_) use ($receipt,$user) {
                                $q_->select('person_id')
                                    ->from('aid_projects_persons')
                                    ->where( function ($qi) use ($receipt,$user) {
                                        $qi->where('status', '=', 3)
                                           ->whereIn('project_id', $receipt)
                                           ->whereIn('organization_id', function($quer) use($user) {
                                                    $quer->select('organization_id')
                                                         ->from('char_user_organizations')
                                                         ->where('user_id', '=', $user->id);
                                          });
                                    });
                           });
                         }
                         else{
                             $q->whereIn('id', function ($q_) use ($receipt,$user) {
                                 $q_->select('person_id')
                                    ->from('aid_projects_persons')
                                    ->where( function ($qi) use ($receipt,$user) {
                                        $qi->where('status', '=', 3)
                                           ->whereIn('project_id', $receipt)
                                           ->whereIn('organization_id', function($q_) use($user) {
                                                    $q_->select('descendant_id')
                                                          ->from('char_organizations_closure')
                                                          ->where('ancestor_id', '=', $user->organization_id);
                                           });
                                    });
                           });
                            
                         }
                         
                    }else{
                        if(is_array($receipt)){
                            $q->whereIn('id',$receipt);
                            }else{
                            $q->where('id',$receipt);
                            }
                    }
         });
         


        $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                               ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ',
                                                ifnull(char_persons.last_name,' ')) AS name,
                                                char_persons.id as person_id,
                                                char_persons.id_card_number,char_persons.old_id_card_number,char_persons_contact.contact_value as mobile
                                                ");

        return $query->get();
    }

}
