<?php
namespace Common\Model\PersonModels;

class PersonProperties  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_properties';
    protected $fillable = ['person_id','property_id','has_property','quantity'];
    public $timestamps = false;

    public static function getPersonProperties($id,$action){

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $query=\DB::table('char_properties_i18n')
                         ->leftjoin('char_persons_properties', function($join) use($id){
                             $join->on('char_properties_i18n.property_id', '=', 'char_persons_properties.property_id')
                                 ->where('char_persons_properties.person_id', '=', $id);
                         })
                         ->where('char_properties_i18n.language_id', '=', 1)
                         ->orderBy('char_properties_i18n.name')
                         ->selectRaw("char_properties_i18n.*");

                if($action=='show') {
                    $query->selectRaw("CASE WHEN char_persons_properties.has_property is null THEN '$not_exist' 
                                                     WHEN char_persons_properties.has_property = 0 THEN '$not_exist'  
                                                     Else '$exist' END 
                                                AS  has_property,
                                                CASE WHEN char_persons_properties.has_property is null THEN '-' 
                                                     WHEN char_persons_properties.has_property = 0 THEN 0  
                                                     WHEN char_persons_properties.quantity is null THEN 0  
                                                     Else char_persons_properties.quantity END
                                                AS  quantity
                                               ");
                }else{
                    $query->selectRaw("char_persons_properties.has_property,char_persons_properties.quantity");
                }

                return $query->get();
    }


    public static function saveProperties($person_id,$items){

        \Common\Model\PersonModels\PersonProperties::where(['person_id'=>$person_id ])->delete();
        foreach ($items as $item) {
            if($item['has_property'] == 1){
                \Common\Model\PersonModels\PersonProperties::insert($item);
            }
        }
        return array('status' => true);
    }
    public static function getItems($id,$items){
        $input = [];

        $item = json_decode(json_encode($items));
        $records = compact('item');

        foreach ($records['item'] as $record) {
            $sub = [
                'property_id' => $record->property_id, 'has_property' => $record->has_property,
                'person_id' => $id, 'quantity' => null
            ];

            if($record->has_property == 1){
                if(isset($record->quantity)){
                    $sub['quantity'] = strip_tags($record->quantity);
                }
            }
            array_push($input, $sub);

        }
        return $input;

    }

}
