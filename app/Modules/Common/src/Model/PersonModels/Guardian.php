<?php

namespace Common\Model\PersonModels;

class Guardian extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_guardians';
    protected $fillable = ['guardian_id','individual_id','kinship_id','organization_id','status','created_at'];
    public $timestamps = false;
    protected $dates = ['created_at'];


}
