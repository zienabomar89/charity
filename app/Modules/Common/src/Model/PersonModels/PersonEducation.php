<?php

namespace Common\Model\PersonModels;

class PersonEducation  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_education';
    protected $primaryKey='person_id';
    protected $fillable = ['person_id','study','currently_study','type','authority', 'stage','specialization','degree','grade','year','school','level','points'];

   public $timestamps = false;

//    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
//    {
//        $query->where('person_id', '=', $this->getAttribute($this->person_id));
//        return $query;
//    }

}
