<?php

namespace Common\Model\PersonModels;

use Aid\Model\SocialSearch\Cases;
use Aid\Model\SocialSearch\CasesBanks;
use Common\Model\Person;

class PersonBank  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_banks';
    protected $fillable = [ 'person_id','bank_id','account_number','account_owner','branch_name'];
    public $timestamps = false;

    public static function getPersonBanks($id,$organization_id){

        $banks=self::query()->leftjoin('char_banks','char_persons_banks.bank_id',  '=', 'char_banks.id')
                            ->leftjoin('char_branches','char_persons_banks.branch_name',  '=', 'char_branches.id')
                            ->where('char_persons_banks.person_id','=',$id)
                            ->selectRaw("char_persons_banks.id,
                                                   char_persons_banks.bank_id,
                                                   char_persons_banks.branch_name,
                                                   CASE WHEN char_banks.name is null THEN '-' Else char_banks.name END AS bank_name,
                                                   CASE WHEN char_branches.name is null THEN '-' Else char_branches.name END AS branch,
                                                   CASE WHEN char_persons_banks.account_number is null THEN '-' Else char_persons_banks.account_number END AS account_number,
                                                   CASE WHEN char_persons_banks.account_owner is null THEN '-' Else char_persons_banks.account_owner END AS account_owner
                                                  ")
                            ->get();

        if($organization_id){

            $orgBank=\DB::table('char_organization_persons_banks')
                ->where('char_organization_persons_banks.organization_id','=',$organization_id)
                ->where('char_organization_persons_banks.person_id','=',$id)
                ->selectRaw("char_organization_persons_banks.*
                                  ")->first();

            $default=null;
            if($orgBank != null){
                $default=$orgBank->bank_id;
            }

            if($banks != null){
                foreach($banks as $k =>$v){
                    if($default){
                        if($v->id == $default){
                            $v->check=true;
                        }else{
                            $v->check=false;
                        }
                    }else{
                        $v->check=false;
                    }
                }
            }


        }

        return $banks;
    }

    public static function saveAccount($data){


        $person = Person::where(function ($q) use ($data) {
            $q->where('id_card_number', '=', $data['id_card_number']);
        })
            ->first();

        if(is_null($person)){
            return ['status'=>false , 'reason' => 'person_not_register'];
        }

        $entry = self::query()
            ->where(function ($q) use ($data) {
                $q->where('bank_id', '=', $data['bank_id']);
                $q->where('account_number', '=', $data['account_number']);
            })
            ->first();

        if(!is_null($entry)){
            if($entry->person_id == $person->id){
                return ['status'=>false , 'reason' => 'account_number_previously_add_to_you'];
            } else if($entry->person_id != $person->id){
                return ['status'=>false , 'reason' => 'account_number_owned_by_other'];
            }
        }

        $insert = ['bank_id','account_number','account_owner','branch_name'];

        $account = new PersonBank();
        $account->person_id = $person->id;
        foreach ($insert as $key) {
            if(isset($data[$key])){
                $account->$key = $data[$key];
            }
        }
        $account->save();

        return ['status'=>true ];
    }

}
