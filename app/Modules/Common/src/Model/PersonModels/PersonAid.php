<?php

namespace Common\Model\PersonModels;

class PersonAid  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons_aids';
    protected $fillable = [ 'aid_source_id','aid_type','aid_value','aid_take','person_id','currency_id'];
    public $timestamps = false;

    public static function getPersonAids($id,$aid_type,$action){

        $query= \DB::table('char_aid_sources')
            ->leftjoin('char_persons_aids', function($join) use($id,$aid_type){
                $join->on('char_persons_aids.aid_source_id', '=', 'char_aid_sources.id')
                    ->where('char_persons_aids.person_id', '=', $id)
                    ->where('char_persons_aids.aid_type', '=', $aid_type);
            })
            ->orderBy('char_aid_sources.name')
            ->selectRaw("char_persons_aids.*, char_aid_sources.name,char_aid_sources.id as aid_source_id");

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        if($action=='show') {
            if($aid_type==1) {
                $query->leftjoin('char_currencies','char_persons_aids.currency_id' , '=','char_currencies.id')
                    ->selectRaw("CASE WHEN char_persons_aids.aid_value is null THEN '-'
                                    WHEN char_persons_aids.aid_take = 0 THEN '-'
                                    WHEN char_currencies.name is null THEN '-'  
                               Else char_currencies.name END AS  currency_name");
            }

            $query->selectRaw(" CASE WHEN char_persons_aids.aid_value is null THEN '$no' 
                                     WHEN char_persons_aids.aid_take = 0 THEN '$no'  Else '$yes' END AS  aid_take,
                                CASE WHEN char_persons_aids.aid_value is null THEN '-' 
                                     WHEN char_persons_aids.aid_take = 0 THEN '-'
                                    WHEN char_persons_aids.aid_value is null THEN '-'  
                                    Else char_persons_aids.aid_value END AS  aid_value
                                     ");
        }
        return $query->get();
    }


    public static function getItems($person_id,$aid_type,$items){

        $input = [];

        $item = json_decode(json_encode($items));
        $records = compact('item');

        foreach ($records['item'] as $record) {

            $sub = ['aid_source_id' => $record->aid_source_id,
                    'aid_take'  => $record->aid_take,
                    'person_id' => $person_id,
                    'aid_type'  => $aid_type];

            if($record->aid_take == 1){
               if(isset($record->aid_value)){
                    if($record->aid_value != "" && $record->aid_value != null){
                        $sub['aid_value'] = strip_tags($record->aid_value);
                    }
                }
                if($aid_type==1){
                    if(isset($record->currency_id)){
                        if($record->currency_id != "" && $record->currency_id != null){
                            $sub['currency_id'] = strip_tags($record->currency_id);
                        }
                    }

                }
           }
            array_push($input, $sub);
        }

        return $input;

    }

    public static function saveAids($person_id,$aid_type,$items){

       \Common\Model\PersonModels\PersonAid::where(['person_id'=>$person_id ,'aid_type' =>$aid_type])->delete();
        foreach ($items as $item) {
            if($item['aid_take'] == 1){
                \Common\Model\PersonModels\PersonAid::insert($item);
            }
        }
        return array('status' => true);
    }


    public static function CreateOrUpdate($person_id,$aid_type,$input,$aid_source_id){

        $exist = self::where(['person_id'=>$person_id ,'aid_type' =>$aid_type,'aid_source_id'=>$aid_source_id])->first();
        if(is_null($exist)){

            $input['person_id'] = $person_id;
            $input['aid_type'] = $aid_type;
            $input['aid_source_id'] = $aid_source_id;

            self::create($input);
        } else{
            self::where(['person_id'=>$person_id ,'aid_type' =>$aid_type])->update($input);
        }

        return true;
    }

}
