<?php

namespace Common\Model;

use Auth\Model\Setting;
use Common\Model\CaseModel;
use Log\Model\Log;
use Common\Model\CloneGovernmentPersons;
class GovServices
{

    // procedure to reset authorization
    public static function resetApiToken($client_id,$client_secret)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $url = "https://ssoidp.gov.ps/sso/module.php/sspoauth2/token.php";
        $dt = "client_id=$client_id&client_secret=$client_secret";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_VERBOSE => 1,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $dt,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        ));

      $response = curl_exec($curl);
      curl_close($curl);
      if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                $result = ['client_authorization'=> $response->access_token ,
                           'expires_in'=> $response->expires_in ,
                           'status' => true];
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // set CURLOPT_HTTPHEADER
    public static function setHttpHeader()
    {

        $user = \Auth::user();
        $expires_in= Setting::where('id', 'gov_service_expires_in')->where('organization_id', 0)->first();

        $expires_in_dt=date('Y-m-d H:i:s',strtotime($expires_in->value));
        $st_dt = new \DateTime($expires_in_dt);
        $end_dt = new \DateTime();

        $sso_auth = '';
        if (($end_dt < $st_dt)){
            $gov_service_client_id= Setting::where('id', 'gov_service_client_id')->where('organization_id', 0)->first();
            $gov_service_client_secret= Setting::where('id', 'gov_service_client_secret')->where('organization_id', 0)->first();
            $access_token = GovServices::resetApiToken($gov_service_client_id->value,$gov_service_client_secret->value);

            $settings = array('gov_service_client_authorization','gov_service_expires_in');
            if($access_token['status'] != false){

                $sso_auth = $access_token['client_authorization'];
                Setting::where('id', 'gov_service_client_authorization')
                       ->where('organization_id', 0)
                       ->update(['value'=>$access_token['client_authorization']]);

                Setting::where('id', 'gov_service_expires_in')
                    ->where('organization_id', 0)
                    ->update(['value'=>$access_token['expires_in']]);

            }
        }
        else{
            $authorization = Setting::where(function ($q){
                $q->where('id', 'gov_service_client_authorization');
                $q->where('organization_id', 0);
            })
                ->first();
            if (!is_null($authorization)){
                $sso_auth = $authorization->value;
            }
        }

        return array(
                    'Content-type: text/plain',
                    "x-sso-authorization:$sso_auth",
                    'x-user-id:'.$user->id_card_number,
                    'x-user-ip:'.request()->ip(),
                    'x-user-agent:'.request()->userAgent(),);
    }

    // set object property
    public static function ObjectValue($row,$ref)
    {
        $value = null;
        if(isset($row->$ref)){
            return $row->$ref;
        }

        return $value;
    }

    // set object property dashif null
    public static function refDashIfNull($row,$ref)
    {
        $value = '-';
        if(isset($row->$ref)){
            return (is_null($row->$ref) ||$row->$ref == ' ' ) ? '-' :$row->$ref;
        }

        return $value;
    }

    // set object property dashif null
    public static function refNullIfNotIsset($row,$ref)
    {
        $value = '-';
        if(isset($row->$ref)){
            return (is_null($row->$ref) ||$row->$ref == ' ' ) ? '-' :$row->$ref;
        }

        return $value;
    }

    // set object (array) property
    public static function ObjectArrayValue($row,$ref)
    {
        $value = [];
        if(isset($row->$ref)){
            return $row->$ref;
        }

        return $value;
    }

    // push rows to main array
    public static function pushToArray($main_array,$sub_array,$card)
    {

        foreach ($sub_array as $key=>$value){
            $value->MAIN_IDO = $card;
            $main_array [] = $value;
        }

        return $main_array;
    }

    // check card number
    public static function checkCard($card)
    {
        $card = str_replace(' ', '', $card);
        $card = trim($card);
        if (strlen($card) != 9) {
            return false;
        }
        $identity = $card;
        $lastDig = $identity[8];
        $digit1 = $identity[0] * 1;
        $digit2 = $identity[1] * 2;
        $digit3 = $identity[2] * 1;
        $digit4 = $identity[3] * 2;
        $digit5 = $identity[4] * 1;
        $digit6 = $identity[5] * 2;
        $digit7 = $identity[6] * 1;
        $digit8 = $identity[7] * 2;

        $checkLast = (int) $digit1 + (int) $digit3  + (int) $digit5 + (int) $digit7;
        if (strlen($digit2) == 2) {
            $digit22 = (string) $digit2;
            $checkLast += (int) $digit22[0];
            $checkLast += (int) $digit22[1];
        } else {
            $checkLast += (int)$digit2;
        }

        if (strlen($digit4) == 2) {
            $digit44 = (string) $digit4;
            $checkLast += (int) $digit44[0];
            $checkLast += (int) $digit44[1];
        } else {
            $checkLast += (int)$digit4;
        }
        if (strlen($digit6) == 2) {
            $digit66 =  (string)$digit6;
            $checkLast += (int) $digit66[0];
            $checkLast += (int) $digit66[1];
        } else {
            $checkLast += (int)$digit6;
        }

        if (strlen($digit8) == 2) {
            $digit88 =  (string) $digit8;
            $checkLast += (int) $digit88[0];
            $checkLast += (int) $digit88[1];
        } else {
            $checkLast += (int) $digit8;
        }

        $checkLast = (string)$checkLast;
        $checkLast_length = strlen($checkLast);

        if($checkLast[$checkLast_length - 1]==0)
            $lastOne=10;
        else
            $lastOne =  $checkLast[$checkLast_length - 1];

        $lastDigit_inCheck = 10 - $lastOne;

        if ($lastDig != $lastDigit_inCheck) {
            return false;
        } else {
            return true;
        }
    }

    // procedure to get citizen data using card number
    public static function byCard($card,$with_details = false)
    {

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its basic data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $card = (int)$card;

        $result = ['row'=> [] ,'status' => false];

        $url = "https://ws.gov.ps/citizen/id/".$card;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),
        ));

        $response = curl_exec($curl);
        curl_close($curl);

        if ($result === false) {
            $result['message'] = curl_error($curl);
            return $result;
        }
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                $result = ['row'=> self::setRaw($response->data,$card,$with_details) ,'status' => true];
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

       return $result;
    }

    // procedure to get citizen data using card number
    public static function byCardRelated($card,$relation)
    {

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its basic data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $card = (int)$card;

        $result = ['row'=> [] ,'status' => false];

        $url = "https://ws.gov.ps/citizen/id/".$card;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                $result = ['row'=> self::setRelatedRaw($response->data,$relation) ,'status' => true];
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen data using name
    public static function byName($options = array())
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $defaults = array(
            'first_name' => "",
            'second_name' => "",
            'third_name' => "",
            'last_name' => "",
        );

        $name_array = array_merge($defaults, $options);
        $name = $name_array['first_name'] .' ' .$name_array['second_name'].' '. $name_array['third_name'] .' ' .$name_array['last_name'];

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for name') . ' ('. $name . ') '.
            trans('common::application.To view its basic data'));

        $result = ['row'=> [] ,'status' => false];

        $fn =  (is_null($options['first_name']) || $options['first_name'] == '' || $options['first_name'] == ' ' ) ? '0' :$options['first_name'];
        $sn =  (is_null($options['second_name']) || $options['second_name'] == '' || $options['second_name'] == ' ' ) ? '0' :$options['second_name'];
        $tn =  (is_null($options['third_name']) || $options['third_name'] == '' || $options['third_name'] == ' ' ) ? '0' :$options['third_name'];
        $ln =  (is_null($options['last_name']) || $options['last_name'] == '' || $options['last_name'] == ' ' ) ? '0' :$options['last_name'];

        $ffn = urlencode($fn);
        $fsn = urlencode($sn);
        $ftn = urlencode($tn);
        $fln = urlencode($ln);

        $url = "https://ws.gov.ps/citizen/name/".$ffn .'/' .$fsn.'/'. $ftn.'/' .$fln;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        } else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if (isset($response->data->basic)){
                    $result = ['row'=> $response->data->basic  ,'status' => true];
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen data using name
    public static function setRaw($data,$card,$with_details){

        $row = $data->basic;
        $row->IDNO=$card;
        $age =  self::age($row->BIRTH_DT,$row->DETH_DT);
        $row->AGE = $age['age'];
        $row->AGE_YEAR = $age['year'];
        $row->AGE_MONTH = $age['month'];
        $row->AGE_DAYS = $age['days'];

        $row->CTZN_STATUS=null;
        $row->CTZN_TRANS_DT=null;
        $row->VISIT_PURPOSE_DESC=null;

        if(isset($data->place)){
            $row->CTZN_STATUS = $data->place->CTZN_STATUS;
            $row->CTZN_TRANS_DT = $data->place->CTZN_TRANS_DT;
            $row->VISIT_PURPOSE_DESC = $data->place->VISIT_PURPOSE_DESC;
        }

        $row->photo_url=null;
        $row->photo_update_date=null;
        if(isset($data->photo)){
            if(!is_null($data->photo->update_date)){
                $row->photo_url = $data->photo->photo_url;
                $row->photo_update_date = $data->photo->update_date;
            }
        }

        $unsetKeys = ['CI_CITY','MOTHER_ARB','JOB_DESC'];
        foreach ($unsetKeys as $unsetKey){
            if(!isset($row->$unsetKey)){
                $row->$unsetKey =null;
            }
        }

        $row->MOBILE=null;
        $row->TEL=null;
        $row->GOV_NAME=null;
        $row->CITY_NAME=null;
        $row->PART_NAME=null;
        $row->ADDRESS_DET=null;
        $row->MAMRK_TYPE_NAME=null;
        $row->EMAIL=null;

        $row->travel=[];
        $row->social_affairs_status= trans('common::application.un_beneficiary');

        $row->social_affairs=null;

        $row->cases_list=[];
        $row->marriage=[];
        $row->divorce=[];
        $row->health=[];
        $row->vehicles=[];
        $row->commercial_data=[];
        $row->employment=[];
        $row->properties=[];
        $row->reg48_license=[];
        $row->social_affairs_receipt=[];

        if (CaseModel::hasPermission('reports.case.MinistryOfCommunications')) {
            $contact_data = self::ssoInfo($row->IDNO);
            if($contact_data['status'] != false){
                $row->MOBILE= self::ObjectValue($contact_data['row'],'USERMOBILE');
                $row->TEL= self::ObjectValue($contact_data['row'],'USERTELEPHONE');
                $row->GOV_NAME= self::ObjectValue($contact_data['row'],'GOV_NAME');
                $row->CITY_NAME= self::ObjectValue($contact_data['row'],'CITY_NAME');
                $row->PART_NAME= self::ObjectValue($contact_data['row'],'PART_NAME');
                $row->ADDRESS_DET= self::ObjectValue($contact_data['row'],'ADDRESS_DET');
                $row->MAMRK_TYPE_NAME= self::ObjectValue($contact_data['row'],'MAMRK_TYPE_NAME');
                $row->EMAIL= self::ObjectValue($contact_data['row'],'USEREMAIL');
            }
        }


        $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
        $SPONSES_RELATIVE_DESC = ["زوج/ة"];
        $CHILD_RELATIVE_DESC = ["ابن/ة"];
        $SINGLE_SOCIAL_STATUS_CD = ["10"];

        $father_kinship_id = 1;
        $mother_kinship_id = 5;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;
        $daughter_kinship_id = 22;
        $son_kinship_id = 2;

        $parent = [];
        $childrens = [];
        $wives = [];

        $row->family_cnt=1;
        $row->female_live=0;
        $row->male_live=0;
        $row->spouses=0;
        $row->all_relatives=[];

        if (isset($data->relatives)){
            $relatives = $data->relatives;
            foreach ($relatives as $key =>$relative){

                if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                    in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                    in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {

                    $relative->IDNO= $relative->IDNO_RELATIVE;
                    $r_age =  self::age($relative->BIRTH_DT,$relative->DETH_DT);
                    $relative->AGE = $r_age['age'];
                    $relative->AGE_YEAR = $r_age['year'];
                    $relative->AGE_MONTH = $r_age['month'];
                    $relative->AGE_DAYS = $r_age['days'];

                    $relative->kinship_name= $relative->RELATIVE_DESC;
                    $relative->MOBILE=null;
                    $relative->TEL=null;
                    $relative->GOV_NAME=null;
                    $relative->CITY_NAME=null;
                    $relative->PART_NAME=null;
                    $relative->ADDRESS_DET=null;
                    $relative->MAMRK_TYPE_NAME=null;
                    $relative->EMAIL=null;

                    if (CaseModel::hasPermission('reports.case.MinistryOfCommunications')) {
                        $contact_info = self::ssoInfo($relative->IDNO_RELATIVE);
                        if($contact_info['status'] != false){
                            $relative->MOBILE=self::ObjectValue($contact_info['row'],'USERMOBILE');
                            $relative->TEL=self::ObjectValue($contact_info['row'],'USERTELEPHONE');
                            $relative->GOV_NAME= self::ObjectValue($contact_info['row'],'GOV_NAME');
                            $relative->CITY_NAME= self::ObjectValue($contact_info['row'],'CITY_NAME');
                            $relative->PART_NAME= self::ObjectValue($contact_info['row'],'PART_NAME');
                            $relative->ADDRESS_DET= self::ObjectValue($contact_info['row'],'ADDRESS_DET');
                            $relative->MAMRK_TYPE_NAME= self::ObjectValue($contact_info['row'],'MAMRK_TYPE_NAME');
                            $relative->EMAIL= self::ObjectValue($contact_info['row'],'USEREMAIL');
                        }
                    }

                    if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                        if($relative->SEX_CD == 1){
                            $relative->kinship_id =$father_kinship_id;
                        }else{
                            $relative->kinship_id =$mother_kinship_id;
                        }
                        $parent[]= $relative;
                    }

                    if(in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                        if((is_null($relative->DETH_DT) || $relative->DETH_DT == null )){
                            $row->family_cnt++;
                            $row->spouses++;
                        }

                        if($relative->SEX_CD == 1){
                            $relative->kinship_id =$wife_kinship_id;
                        }else{
                            $relative->kinship_id =$husband_kinship_id;
                        }
                        $wives[]= $relative;
                    }

                    if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                        if($relative->SEX_CD == 1){
                            $relative->kinship_id =$son_kinship_id;
                        }else{
                            $relative->kinship_id =$daughter_kinship_id;
                        }

                        if((is_null($relative->DETH_DT) || $relative->DETH_DT == null ) &&
                            in_array($relative->SOCIAL_STATUS_CD ,$SINGLE_SOCIAL_STATUS_CD)){
                            if ($relative->SEX_CD == 1 ){ $row->male_live++ ; } else{ $row->female_live++; }
                            $row->family_cnt++;
                        }
                        $childrens[]= $relative;
                    }

                    $row->all_relatives[]=$relative;

                }
            }
        }


        $row->family_cnt = ( $row->spouses + $row->male_live +  $row->female_live) + 1;

        $row->parent = $parent;
        $row->childrens = $childrens;
        $row->wives = $wives;

        return $row;
    }

    // procedure to get some relatives card using id - relation
    public static function setRelatedRaw($data,$ref){

        $RELATIVE_DESC = ["ابن/ة"];
        if ($ref == 'wives'){
            $RELATIVE_DESC = ["زوج/ة"];
        }elseif ($ref == 'parent'){
            $RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
        }

        $cards = [];
        $relatives = [];
        foreach ($data->relatives as $key =>$relative){
            if(is_null($relative->DETH_DT)){
                if(in_array($relative->RELATIVE_DESC ,$RELATIVE_DESC)){
                    $cards[]= $relative->IDNO_RELATIVE;
                    $relatives[]= $relative;
                }
            }
        }

        return ['cards' => $cards , 'rows' => $relatives];
    }

    // get all citizen data using card
    public static function allByCard($card){

        $row = self::byCard($card);
        if($row['status'] == true) {
            $record = $row['row'];
            $record->passport=null;
            $record->travel=[];
            $record->vehicles=[];
            $record->health_insurance_data=null;
            $record->health=[];
            $record->commercial_data=[];
            $record->employment=[];
            $record->marriage_divorce=[ 'marriage' =>[] ,  'divorce' =>[]];
            $record->properties=[];
            $record->reg48_license=[];
            $record->aids_committee=null;
            $record->CASE_STATUS=false;
            $record->cases_list=[];
            $record->person_id = Person::getPersonId([$card]);
            $record->social_affairs=null;
            $record->social_affairs_request=null;
            $record->social_affairs_status= trans('common::application.un_beneficiary');
            $record->social_affairs_receipt=[];

            $passport_data = self::passportInfo($card);
            if($passport_data['status'] != false){
                if(isset($passport_data['row']->ISSUED_ON)){
                    $passport_data['row']->ISSUED_ON = date('d/m/Y',strtotime($passport_data['row']->ISSUED_ON));
                }
                $record->passport=$passport_data['row'];
            }

            $travel_data = self::travelRecords($card);
            if($travel_data['status'] != false){
                $record->travel=$travel_data['row'];
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfJustice')) {
                $marriageDivorce = self::marriageDivorce($card);
                if($marriageDivorce['status'] != false){
                    $record->marriage=self::ObjectValue($marriageDivorce['row'],'marriage');
                    $record->divorce=self::ObjectValue($marriageDivorce['row'],'divorce');
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfHealth')) {
                $health_insurance_data = self::healthInsurance($card);
                if($health_insurance_data['status'] != false){
                    if(count((array)$health_insurance_data['row'])) {
                        $record->health_insurance_data=$health_insurance_data['row'];
                    }
                }
                $health_data = self::medicalReports($card);
                if($health_data['status'] != false){
                    $record->health=$health_data['row'];
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfTransportation')){
                $vehicle_data = self::vehicle($card);
                if($vehicle_data['status'] != false){
                    $record->vehicles=$vehicle_data['row'];
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfLabor')) {
                $reg48License = self::reg48License($card);
                if($reg48License['status'] != false){
                    $record->reg48_license=$reg48License['row'];
                }
            }

            if (CaseModel::hasPermission('reports.case.LandAuthority')) {
                $properties = self::properties($card);
                if($properties['status'] != false){
                    $record->properties=$properties['row'];
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfFinance')) {
                $employment_data = self::workDetails($card);
                if($employment_data['status'] != false){
                    $record->employment=$employment_data['row'];
                    foreach ($record->employment as $ke => $v_){
                        if(isset($v_->JOB_START_DT)){
                            $v_->JOB_START_DT = date('d/m/Y',strtotime($v_->JOB_START_DT));
                        }
                    }
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfEconomy')){
                $commercial_data = self::commercialRecords($card);
                if($commercial_data['status'] != false){
                    $record->commercial_data=$commercial_data['row'];
                    foreach ($record->commercial_data as $ke => $v_){
                        if(isset($v_->START_DATE)){
                            $v_->START_DATE = date('d/m/Y',strtotime($v_->START_DATE));
                        }
                    }
                }
            }

            if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment')){
                $socialAffairs=self::socialAffairsStatus($card);
                if($socialAffairs['status'] != false){
                    $socialAffairs_=$socialAffairs['row'];
                    $socialAffairs_->social_affairs= true;
                    $socialAffairs_->social_affairs_status=  trans('common::application.beneficiary_');
                    $record->social_affairs_status= trans('common::application.beneficiary_');
                    $record->social_affairs=$socialAffairs_;
                }

                $socialAffairsRecipt=self::socialAffairsReceipt($card);
                if($socialAffairsRecipt['status'] == true) {
                    $record->social_affairs_receipt = $socialAffairsRecipt['row'];
                }
            }

//            $aidsCommittee=self::aidsCommitteeStatus($card);
//            if($aidsCommittee['status'] == true){
//                $record->aids_committee=$aidsCommittee['row'];
//            }

            if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
                $record->cases_list = Person::getPersonCardCases([$card]);
            }

            return ['row'=> $record ,'status' => true];
        }

        return ['row'=> [] ,'status' => false];
    }

    // get some citizen data using card
    public static function tabByCard($card,$tab){

        $record = [];
        if ($tab == 'Justice') {
            $marriageDivorce = self::marriageDivorce($card);
            $record['marriage_divorce']=[ 'marriage' =>[] ,  'divorce' =>[]];
            if($marriageDivorce['status'] != false){
                $record['marriage']=self::ObjectValue($marriageDivorce['row'],'marriage');
                $record['divorce']=self::ObjectValue($marriageDivorce['row'],'divorce');

                $record['marriage_divorce']['marriage']=self::ObjectValue($marriageDivorce['row'],'marriage');
                $record['marriage_divorce']['divorce']=self::ObjectValue($marriageDivorce['row'],'divorce');
            }
        }
        else if ($tab== 'Health')  {
            $health_insurance_data = self::healthInsurance($card);
            $record['health_insurance_data']=null;
            if($health_insurance_data['status'] != false){
                if(count((array)$health_insurance_data['row'])) {
                    $record['health_insurance_data']=$health_insurance_data['row'];
                }
            }

            $health_data = self::medicalReports($card);
            $record['health']=[];
            if($health_data['status'] != false){
                $record['health']=$health_data['row'];
            }
        }
        else if ($tab== 'Transportation') {
            $vehicle_data = self::vehicle($card);
            $record['vehicles']=[];
            if($vehicle_data['status'] != false){
                $record['vehicles']=$vehicle_data['row'];
            }
        }
        else if ($tab== 'Communications') {
            $record['MOBILE'] = null;
            $record['TEL'] = null;
            $record['GOV_NAME'] = null;
            $record['CITY_NAME'] = null;
            $record['PART_NAME'] = null;
            $record['ADDRESS_DET'] = null;
            $record['MAMRK_TYPE_NAME'] = null;
            $record['EMAIL'] = null;

            $contact_data = self::ssoInfo($card);
            if($contact_data['status'] != false){
                $record['MOBILE']= self::ObjectValue($contact_data['row'],'USERMOBILE');
                $record['TEL']= self::ObjectValue($contact_data['row'],'USERTELEPHONE');
                $record['GOV_NAME']= self::ObjectValue($contact_data['row'],'GOV_NAME');
                $record['CITY_NAME']= self::ObjectValue($contact_data['row'],'CITY_NAME');
                $record['PART_NAME']= self::ObjectValue($contact_data['row'],'PART_NAME');
                $record['ADDRESS_DET']= self::ObjectValue($contact_data['row'],'ADDRESS_DET');
                $record['MAMRK_TYPE_NAME']= self::ObjectValue($contact_data['row'],'MAMRK_TYPE_NAME');
                $record['EMAIL']= self::ObjectValue($contact_data['row'],'USEREMAIL');
            }

        }
        else if ($tab== 'Finance') {
            $employment_data = self::workDetails($card);
            $record['employment']=[];
            if($employment_data['status'] != false){
                $record['employment']=$employment_data['row'];
                foreach ($record['employment'] as $ke => $v_){
                    if(isset($v_->JOB_START_DT)){
                        $v_->JOB_START_DT = date('d/m/Y',strtotime($v_->JOB_START_DT));
                    }
                }
            }

        }
        else if ($tab== 'Economy') {
            $commercial_data = self::commercialRecords($card);
            $record['commercial_data']=[];
            if($commercial_data['status'] != false){
                $record['commercial_data']=$commercial_data['row'];
                foreach ($record['commercial_data'] as $ke => $v_){
                    if(isset($v_->START_DATE)){
                        $v_->START_DATE = date('d/m/Y',strtotime($v_->START_DATE));
                    }
                }
            }
        }
        else if ($tab== 'Land') {
            $properties = self::properties($card);
            $record['properties']=[];
            if($properties['status'] != false){
                $record['properties']=$properties['row'];
            }
        }
        else if ($tab== 'Labor') {
            $reg48License = self::reg48License($card);
            $record['reg48_license']=[];
            if($reg48License['status'] != false){
                $record['reg48_license']=$reg48License['row'];
            }

        }
        else if ($tab== 'Social') {
            $socialAffairs=self::socialAffairsStatus($card);
            $record['social_affairs']=null;
            $record['social_affairs_request']=null;
            $record['social_affairs_status']= trans('common::application.un_beneficiary');
            if($socialAffairs['status'] != false){
                $socialAffairs_=$socialAffairs['row'];
                $socialAffairs_->social_affairs= true;
                $socialAffairs_->social_affairs_status=  trans('common::application.beneficiary_');
                $record['social_affairs_status']= trans('common::application.beneficiary_');
                $record['social_affairs']=$socialAffairs_;
            }

            $socialAffairsRecipt=self::socialAffairsReceipt($card);
            $record['social_affairs_receipt']=[];
            if($socialAffairsRecipt['status'] == true) {
                $record['social_affairs_receipt'] = $socialAffairsRecipt['row'];
            }
        }

        return ['row'=> $record ,'status' => true];
    }

    public static function setMain($record,$card){


        $passport_data = self::passportInfo($card);
        $record->passport=null;
        if($passport_data['status'] != false){
            if(isset($passport_data['row']->ISSUED_ON)){
                $passport_data['row']->ISSUED_ON = date('d/m/Y',strtotime($passport_data['row']->ISSUED_ON));
            }
            $record->passport=$passport_data['row'];
        }


        $record->health_insurance_data=null;
        $record->social_affairs=null;
        $record->social_affairs_request=null;
        $record->social_affairs_status= trans('common::application.un_beneficiary');

        if (CaseModel::hasPermission('reports.case.MinistryOfHealth')) {
            $health_insurance_data = self::healthInsurance($card);
            if($health_insurance_data['status'] != false){
                if(count((array)$health_insurance_data['row'])) {
                    $record->health_insurance_data=$health_insurance_data['row'];
                }
            }
        }

        if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment')) {
            $socialAffairs=self::socialAffairsStatus($card);
            if($socialAffairs['status'] != false){
                $socialAffairs_=$socialAffairs['row'];
                $socialAffairs_->social_affairs= true;
                $socialAffairs_->social_affairs_status=  trans('common::application.beneficiary_');
                $record->social_affairs_status= trans('common::application.beneficiary_');
                $record->social_affairs=$socialAffairs_;
            }
        }


//        $aidsCommittee=self::aidsCommitteeStatus($card);
//        $record->aids_committee=null;
//        if($aidsCommittee['status'] == true){
//            $record->aids_committee=$aidsCommittee['row'];
//        }

        return ['row'=> $record ,'status' => true];
    }

    public static function setCaseFormDetails($record,$person,$card){

        $commercial_data = self::commercialRecords($card);
        $record->commercial_data=[];
        $person['has_commercial_records']='0';
        $person['active_commercial_records']= 0 ;
        $person['gov_commercial_records_details']='';

        if($commercial_data['status'] != false){
            $record->commercial_data=$commercial_data['row'];
            $person['has_commercial_records']= '1' ;
            foreach ($record->commercial_data as $ke_ => $v_){
                if(isset($v_->START_DATE)){
                    $v_->START_DATE = date('d/m/Y',strtotime($v_->START_DATE));
                }
                if($v_->IS_VALID_DESC == "فعال"){ $person['active_commercial_records']++; }
                $person['gov_commercial_records_details'] .= '  ( ' .
                    self::refDashIfNull($v_,'REGISTER_NO')   . '  -  ' .
                    self::refDashIfNull($v_,'COMP_NAME')     . '  -  ' .
                    self::refDashIfNull($v_,'START_DATE')    . '  -  ' .
                    self::refDashIfNull($v_,'IS_VALID_DESC') . ' )  ';
            }
        }

        $record->has_commercial_records =  $person['has_commercial_records'] ;
        $record->active_commercial_records =  $person['active_commercial_records'] ;
        $record->gov_commercial_records_details =  $person['gov_commercial_records_details'] ;

        $person['has_health_insurance']='0';
        $person['health_insurance_number']= null ;
        $person['health_insurance_type']=null;

        $record->CARD_NO= null;
        $record->EXP_DATE= null;
        $record->INS_STATUS_DESC= null;
        $record->INS_TYPE_DESC= null;
        $record->WORK_SITE_DESC= null;

        $health_insurance_data = self::healthInsurance($card);
        if($health_insurance_data['status'] != false){
            if(count((array)$health_insurance_data['row'])) {
                $health_insurance_data=$health_insurance_data['row'];
                $record->CARD_NO= self::refNullIfNotIsset($health_insurance_data,'CARD_NO');
                $record->EXP_DATE= self::refNullIfNotIsset($health_insurance_data,'EXP_DATE');
                $record->INS_STATUS_DESC= self::refNullIfNotIsset($health_insurance_data,'INS_STATUS_DESC');
                $record->INS_TYPE_DESC= self::refNullIfNotIsset($health_insurance_data,'INS_TYPE_DESC');
                $record->WORK_SITE_DESC= self::refNullIfNotIsset($health_insurance_data,'WORK_SITE_DESC');

                $person['has_health_insurance']='1';
                $person['health_insurance_number']= self::refNullIfNotIsset($health_insurance_data,'CARD_NO');
                $person['health_insurance_type']= self::refNullIfNotIsset($health_insurance_data,'INS_TYPE_DESC'); ;
            }
        }

        $record->PASSPORT_NO= null;
        $record->PASSPORT_ISSUED_ON= null;
        $record->PASSPORT_TYPE= null;
        $passport_data = self::passportInfo($card);
        if($passport_data['status'] != false){
            if(isset($passport_data['row']->ISSUED_ON)){
                $passport_data['row']->ISSUED_ON = date('d/m/Y',strtotime($passport_data['row']->ISSUED_ON));
            }

            $record->PASSPORT_TYPE= self::refNullIfNotIsset($passport_data['row'],'PASS_TYPE');
            $record->PASSPORT_NO= self::refNullIfNotIsset($passport_data['row'],'PASSPORT_NO');
            $record->PASSPORT_ISSUED_ON= self::refNullIfNotIsset($passport_data['row'],'ISSUED_ON');
        }

        $person['gov_health_details']='';

        $health_data = self::medicalReports($card);
        $record->health=[];
        if($health_data['status'] != false){
            $health =$health_data['row'];
            $record->health=$health;
            foreach ($health as $ke_ => $v_){
                $person['gov_health_details'] .= ' ( ' .
                    self::refDashIfNull($v_,'MR_CODE')   . '  -  ' .
                    self::refDashIfNull($v_,'LOC_NAME_AR')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_PATIENT_CD')   . '  -  ' .
                    self::refDashIfNull($v_,'DOCTOR_NAME')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_CREATED_ON')   . '  -  ' .
                    self::refDashIfNull($v_,'DREF_NAME_AR')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_DIAGNOSIS_AR')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_DIAGNOSIS_EN')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_COMPLAINT')   . '  -  ' .
                    self::refDashIfNull($v_,'MR_EXAMINATION') . ' ) ';
            }
        }
        $record->gov_health_details = $person['gov_health_details'] ;

        $person['gov_work_details']='';

        $employment_data = self::workDetails($card);
        $record->employment=[];
        if($employment_data['status'] != false){
            foreach ($employment_data['row']as $ke => $v_){
                if(isset($v_->JOB_START_DT)){
                    $v_->JOB_START_DT = date('d/m/Y',strtotime($v_->JOB_START_DT));
                }

                $person['gov_work_details'] .= '( ' .
                    self::refDashIfNull($v_,'MINISTRY_NAME')   . '  -  ' .
                    self::refDashIfNull($v_,'JOB_START_DT')     . '  -  ' .
                    self::refDashIfNull($v_,'DEGREE_NAME')     . '  -  ' .
                    self::refDashIfNull($v_,'EMP_STATE_DESC')    . '  -  ' .
                    self::refDashIfNull($v_,'EMP_WORK_STATUS')    . '  -  ' .
                    self::refDashIfNull($v_,'JOB_DESC') .' )';

            }
        }

        $record->gov_work_details = $person['gov_work_details'] ;

        $vehicle_data = self::vehicle($card);
        $record->vehicles=[];
        if($vehicle_data['status'] != false){
            $record->vehicles=$vehicle_data['row'];
        }

        $reg48License = self::reg48License($card);
        $record->reg48_license=[];
        if($reg48License['status'] != false){
            $record->reg48_license=$reg48License['row'];
        }

        $properties = self::properties($card);
        $record->properties=[];
        if($properties['status'] != false){
            $record->properties=$properties['row'];
        }

        $marriageDivorce = self::marriageDivorce($card);
        $record->marriage= [];
        $record->divorce= [];
        if($marriageDivorce['status'] != false){
            $record->marriage=self::ObjectValue($marriageDivorce['row'],'marriage');
            $record->divorce=self::ObjectValue($marriageDivorce['row'],'divorce');
        }

        $travel_data = self::travelRecords($card);
        $record->travel=[];
        if($travel_data['status'] != false){
            $record->travel=$travel_data['row'];
        }

        $employment_data = self::workDetails($card);
        $record->employment=[];
        if($employment_data['status'] != false){
            $record->employment=$employment_data['row'];
            foreach ($record->employment as $ke => $v_){
                if(isset($v_->JOB_START_DT)){
                    $v_->JOB_START_DT = date('d/m/Y',strtotime($v_->JOB_START_DT));
                }
            }
        }




        $socialAffairs=self::socialAffairsStatus($card);
        $record->social_affairs=null;
        $record->social_affairs_request=null;
        $record->social_affairs_status= trans('common::application.un_beneficiary');
        if($socialAffairs['status'] != false){
            $socialAffairs_=$socialAffairs['row'];
            $socialAffairs_->social_affairs= true;
            $socialAffairs_->social_affairs_status=  trans('common::application.beneficiary_');
            $record->social_affairs_status= trans('common::application.beneficiary_');
            $record->social_affairs=$socialAffairs_;
        }

        $socialAffairsRecipt=self::socialAffairsReceipt($card);
        $record->social_affairs_receipt=[];
        if($socialAffairsRecipt['status'] == true) {
            $record->social_affairs_receipt = $socialAffairsRecipt['row'];
        }

//        $aidsCommittee=self::aidsCommitteeStatus($card);
//        $record->aids_committee=null;
//        if($aidsCommittee['status'] == true){
//            $record->aids_committee=$aidsCommittee['row'];
//        }


        $person['parent'] = [] ;
        $person['childrens'] = [] ;
        $person['wives'] = [] ;

        if (sizeof($record->all_relatives) > 0){
            $rel_keys = ['parent','childrens','wives'];
            foreach ($rel_keys as $key){
                foreach ($record->$key as $k=>$v){
                    if ($key == 'childrens'){
                        if ($record->SEX_CD == 1){
                            $v->FATHER_IDNO = $card;
                        }else{
                            $v->MOTHER_IDNO = $card;
                        }
                    }
                    $v->IDNO = $v->IDNO_RELATIVE;
                    CloneGovernmentPersons::saveNewWithRelation($card,$v);
                }
                $person[$key] = $record->$key ;
            }
        }

        return ['record'=> $record ,'person' => $person];
    }

    // procedure to get sso data  using card number (contact data)
    public static function ssoInfo($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its contact info'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/detailed-sso-info/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }

        }

        return $result;
    }

    // procedure to get citizen work details data using card number
    public static function workDetails($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its employee data'));


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/work/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen health-insurance data using card number
    public static function healthInsurance($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view health insurance'));


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/health-insurance/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen medical-reports data using card number
    public static function medicalReports($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its health data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/medical-reports/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get commercial-records data using card number
    public static function commercialRecords($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its commercial data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/commercial-records/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->commercial_records)){
                        $result = ['row'=> $response->data->commercial_records  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen social affairs status to using card number
    public static function socialAffairsStatus($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its social affairs receipt'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/aid/social-affairs/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen social affairs receipt list to using card number
    public static function socialAffairsReceipt($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its social affairs receipt'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $curl = curl_init();

        $url = "https://ws.gov.ps/aid/records/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get citizen passport  using card number
    public static function passportInfo($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view passport info'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];

        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/passport/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        if (sizeof($response->data->basic) > 0){
                            $result = ['row'=> $response->data->basic[0]  ,'status' => true];
                        }else{
                            $result['message'] = trans('common::application.not register card');
                        }
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get travel data using card number (from to)
    public static function travelRecords($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its commercial data'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $from = '1-1-1970';
        $to = date('d-m-Y');
        $url = "https://ws.gov.ps/citizen/travel/id/".$card."/from/".$from."/to/".$to;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get Vehicle data using card number
    public static function vehicle($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view vehicle'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/vehicle/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data)){
                        if (isset($response->data->basic)){
                            $result = ['row'=> $response->data->basic  ,'status' => true];
                        }else{
                            $result['message'] = trans('common::application.not register card');
                        }
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get marriage-divorce data using card number
    public static function marriageDivorce($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view marriage-divorce'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/marriage-divorce/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data)){
                        $result = ['row'=> $response->data  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get reg48-license data using card number
    public static function reg48License($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view reg48-license'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/reg48-license/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->basic)){
                        $result = ['row'=> $response->data->basic  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // procedure to get properties data using card number
    public static function properties($card)
    {

        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view reg48-license'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $curl = curl_init();

        $url = "https://ws.gov.ps/citizen/properties/id/".$card;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => self::setHttpHeader(),

        ));

        $response = curl_exec($curl);
        curl_close($curl);
        if (FALSE === $response) {
            $result['message'] = trans('common::application.can not connected to api service');
        }
        else {
            $response = json_decode($response);
            if ($response->status == "success" ){
                if ($response->data){
                    if (isset($response->data->properties)){
                        $result = ['row'=> $response->data->properties  ,'status' => true];
                    }else{
                        $result['message'] = trans('common::application.not register card');
                    }
                }else{
                    $result['message'] = trans('common::application.not register card');
                }
            }elseif ($response->status == "access_denied" || $response->status == "invalid_client" ){
                $result['message'] = trans('common::application.'.$response->status);
            }else{
                $result['message'] = trans('common::application.not register card');
            }
        }

        return $result;
    }

    // ***********************************************************************************************
    // ***********************************************************************************************
    // ***********************************************************************************************
    // procedure to get citizen aid Committee status to using card number
    public static function aidsCommitteeStatus($card)
    {
        $card = (int)$card;

        Log::saveNewLog('CARD_CKECK',
            trans('common::application.The user searched for the ID number') . ' ('.$card . ') '.
            trans('common::application.To view its aid committee status'));

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $url = "http://newzakat.atyafco.com/api/payee?id=".$card;

        $jsonData = array(
            'key: MviUlMacEbUxQAWK2txepa9SSoqTCrs6tVjavqagLLBUhTeSJ3TxjrLTe84siDTZ',
            'Content-Type: application/x-www-form-urlencoded',
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$jsonData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $headers = array('Content-type: text/plain');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
        } else {
            $response = json_decode($response);

            if ($response) {
                if ($response->status) {
                    $result = ['row'=> $response->data,'status' => true];
                }else{
                    $result = ['status' => false , 'message' => trans('common::application.The person is not beneficiary')];
                }
            }else{
                $result = ['status' => false , 'message' => trans('common::application.can not connected to api service')];
            }

        }
        return $result;
    }

    public static function age($birthday,$death_date){

        $age = 0;
        $year = 0;
        $month = 0;
        $days = 0;

        if (!is_null($birthday)) {
            $dob = \DateTime::createFromFormat('d/m/Y', $birthday);
            $dod = new \DateTime();
            if (!is_null($death_date)) {
                $dod = \DateTime::createFromFormat('d/m/Y', $death_date);
            }
            $diff = $dod->diff($dob);

            $year = $diff->y;
            $month = $diff->m;
            $days = $diff->d;

            $age = $diff->y;

            if ($diff->m > 0) {
                $age += ($diff->m / 12);
            }

            if ($diff->d > 0) {
                $age += ($diff->d / 365);
            }
        }
        return ['age' => round($age,1) , 'year' => $year  , 'diff' => $diff  ,'month' => $month  , 'days' => $days];
    }

    // ----------------------- Mapping -------------------- //
    public static function personDataMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "FULLNAME" =>  "fullname",
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "spouses" =>  "spouses",
            "male_live" =>  "male_live",
            "female_live" =>  "female_live",
    );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'FULLNAME'){
                    if (!isset($data->FNAME_ARB)){
                        $parts = explode(" ",$v);
                        $row['first_name'] = $row['first_name'] = $row['first_name'] =$row['first_name'] = '';
                        if (isset($parts[0])){ $row['first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                        if (isset($parts[1])){ $row['second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                        if (isset($parts[2])){ $row['third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                        if (isset($parts[3])){ $row['last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }

                        if (sizeof($parts) > 4){
                            $length = sizeof($parts) - 1;
                            for ($x = 4; $x < $length; $x++) {
                                $row['last_name'] .= (is_null($parts[$x]) ||$parts[$x] == ' ' ) ? '-' :$parts[$x] ;
                            }
                        }
                    }

                    $row['fullname'] = $v;
                }
                elseif($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $row['en_second_name'] = $row['en_third_name'] =$row['en_last_name'] = '';
                    if (isset($parts[0])){ $row['en_first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                    if (isset($parts[1])){ $row['en_second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                    if (isset($parts[2])){ $row['en_third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                    if (isset($parts[3])){ $row['en_last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }
        return $row;
    }

    public static function mapToUpdate($data)
    {

        $row = [];
        $_translator =array(
            "FULLNAME" =>  "fullname",
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "spouses" =>  "spouses",
            "male_live" =>  "male_live",
            "female_live" =>  "female_live",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'FULLNAME'){

                    if (!isset($data->FNAME_ARB)){
                        $parts = explode(" ",$v);
                        $row['first_name'] = $row['first_name'] = $row['first_name'] =$row['first_name'] = '';
                        if (isset($parts[0])){ $row['first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                        if (isset($parts[1])){ $row['second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                        if (isset($parts[2])){ $row['third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                        if (isset($parts[3])){ $row['last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }

                        if (sizeof($parts) > 4){
                            $length = sizeof($parts) - 1;
                            for ($x = 4; $x < $length; $x++) {
                                $row['last_name'] .= (is_null($parts[$x]) ||$parts[$x] == ' ' ) ? '-' :$parts[$x] ;
                            }
                        }
                    }

                    $row['fullname'] = $v;
                }
                elseif($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $row['en_second_name'] = $row['en_third_name'] =$row['en_last_name'] = '';
                    if (isset($parts[0])){ $row['en_first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                    if (isset($parts[1])){ $row['en_second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                    if (isset($parts[2])){ $row['en_third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                    if (isset($parts[3])){ $row['en_last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }
        return $row;
    }

    public static function mapBasicToUpdate($data)
    {


        $row = [];
        $_translator =array(
            "FULLNAME" =>  "fullname",
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "spouses" =>  "spouses",
            "male_live" =>  "male_live",
            "female_live" =>  "female_live",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }
        return $row;
    }

    public static function personRelatedMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "REL_CI_ID_NUM" =>  "id_card_number",
            "REL_CI_FIRST_ARB" =>  "first_name",
            "REL_CI_FATHER_ARB" =>  "second_name",
            "REL_CI_GRAND_FATHER_ARB" => "third_name",
            "REL_CI_FAMILY_ARB" =>  "last_name",
            "REL_CI_PRV_FAMILY_ARB" =>  "prev_family_name",
            "REL_CI_SEX_CD" => "gender",
            "REL_CI_PERSONAL_CD" =>  "marital_status_id",
            "REL_CI_PERSONAL_DESC" =>  "marital_status",
            "REL_CI_BIRTH_DT" =>  "birthday",
            "REL_CI_DEAD_DT" =>  "death_date"
        );

        $dates = ['birthday' , 'death_date'];
        $row['kinship_id'] =null;
        $row['kinship_name']='-';

        foreach ($data as $k=>$v) {
            if($k == 'REL_DESC'){
                if($v =='wife'){
                    $wife_kinship=\Setting\Model\Setting::where('id','wife_kinship')->first();
                    if($wife_kinship){
                        if(!is_null($wife_kinship->value) and $wife_kinship->value != "") {
                            $row['kinship_id'] =$wife_kinship->value;
                            $row['kinship_name']=  trans('common::application.wife_');
                        }
                    }
                }
                else if($v =='son' && $data->REL_CI_SEX_CD == 2){
                    $daughter_kinship = \Setting\Model\Setting::where('id', 'daughter_kinship')->first();
                    if($daughter_kinship){
                        if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
                            $row['kinship_id'] =$daughter_kinship->value;
                            $row['kinship_name']=  trans('common::application.daughter_');
                        }
                    }
                }
                else {
                    $son_kinship = \Setting\Model\Setting::where('id', 'son_kinship')->first();
                    if($son_kinship){
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $row['kinship_id'] =$son_kinship->value;
                            $row['kinship_name']=  trans('common::application.son_');
                        }
                    }
                }
            }
            else{
                if(isset($_translator[$k])){
                    if(in_array($_translator[$k],$dates)){
                        if(!is_null($v) && $v != " "){
                            $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                        }
                    }
                    else{
                        $row [$_translator[$k]]=$v;
                    }
                }
            }
        }

        return $row;
    }

    public static function inputsMap($data){

        $row = array("death_date"=>null,'card_type'=>"1");
        $_translator =array(
            "FULLNAME" =>  "fullname",
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "spouses" =>  "spouses",
            "male_live" =>  "male_live",
            "female_live" =>  "female_live",
        );

        $dates = ['birthday' , 'death_date'];
        $SEX_CD_MAP = ['1'=> 1 ,'2'=> 2];
        $SOCIAL_STATUS_MAP = ['20'=> 20 ,'10'=> 10 ,'30' => 30 , '40'=>40 , '21' =>21];

        foreach ($data as $k=>$v) {
            if(isset($_translator[$k])){
                if($k == 'FULLNAME'){

                    if (!isset($data->FNAME_ARB)){
                        $parts = explode(" ",$v);
                        $row['first_name'] = $row['first_name'] = $row['first_name'] =$row['first_name'] = '';
                        if (isset($parts[0])){ $row['first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                        if (isset($parts[1])){ $row['second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                        if (isset($parts[2])){ $row['third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                        if (isset($parts[3])){ $row['last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }

                        if (sizeof($parts) > 4){
                            $length = sizeof($parts) - 1;
                            for ($x = 4; $x < $length; $x++) {
                                $row['last_name'] .= (is_null($parts[$x]) ||$parts[$x] == ' ' ) ? '-' :$parts[$x] ;
                            }
                        }
                    }

                    $row['fullname'] = $v;
                }
                elseif($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $row['en_second_name'] = $row['en_third_name'] =$row['en_last_name'] = '';
                    if (isset($parts[0])){ $row['en_first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                    if (isset($parts[1])){ $row['en_second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                    if (isset($parts[2])){ $row['en_third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                    if (isset($parts[3])){ $row['en_last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }
                }
                elseif($k == 'SEX_CD'){
                    if(isset($SEX_CD_MAP[$v])){
                        $row [$_translator[$k]]=$SEX_CD_MAP[$v];
                    }else{
                        $row [$_translator[$k]]=$v;
                    }
                }
                elseif($k == 'SOCIAL_STATUS_CD'){
                    $row [$_translator[$k]]=$SOCIAL_STATUS_MAP[$v];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        if(isset($row['DETH_DT'])){
            if(!is_null($row['DETH_DT'])){
                $row ['marital_status_id']=4;
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }

        return $row;}

    // ---------------------------------------------------- //

}