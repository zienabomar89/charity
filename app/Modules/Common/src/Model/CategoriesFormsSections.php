<?php

namespace Common\Model;

class CategoriesFormsSections extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_categories_forms_sections';
    protected $fillable = ['id', 'category_id'];
    public $timestamps = false;
    protected $keyType = 'string';
    public $incrementing = false;

    public static function getCategoryRequiredSections($id,$type)
    {
        $sections= self::query()->where('category_id',$id)->get();
        if($type == 2){
            $all= ['personalInfo','residenceData','homeIndoorData','otherData','familyStructure','CaseDocumentData', 'recommendations'];
        }else{
            $all= ['CaseData','ParentData','providerData','FamilyData','familyDocuments','caseDocuments'];
        }
        $return=[];

        if(sizeof($sections)==0){
            foreach($all as $key) {
                $return[$key]=false;
            }
        }else{

            $Exist=[];
            foreach($sections as $item) {
                array_push($Exist,$item->id);
            }
            foreach($all as $key) {
                if(in_array($key, $Exist)) {
                    $return[$key]=true;
                }else{
                    $return[$key]=false;
                }
            }
        }

        return $return;
    }

}
