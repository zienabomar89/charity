<?php

namespace Common\Model;
use App\Http\Helpers;


class BlockCategories extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_block_categories';
    public $fillable = ['block_id','category_id'];
    protected $hidden = ['created_at','updated_at'];
    public $primaryKey='block_id';

    public function Category()
    {
        return $this->belongsTo('Aid\Model\Categories','category_id','id');
    }

    public function Block()
    {
        return $this->belongsTo('Aid\Model\BlockIDCard','block_id','id');
    }

    public static function getBlocked($page,$type,$filter){

        $language_id =  \App\Http\Helpers::getLocale();
        if($type == "3"){

            $char_block_id_card_number= ['id_card_number'];
            $char_block_categories= ['category_id'];
            $condition = [];

            foreach ($filter as $key => $value) {
                if(in_array($key, $char_block_id_card_number)) {
                    if ($value != "") {
                        $data = ['char_block_id_card_number.' . $key => $value];
                        array_push($condition, $data);
                    }
                }else{
                    if(in_array($key, $char_block_categories)) {
                        if ($value != "") {
                            $data = ['blk.' . $key => $value];
                            array_push($condition, $data);
                        }
                    }
                }
            }

            $return= \DB::table('char_block_id_card_number')
                ->leftjoin('char_block_categories as blk','blk.block_id',  '=', 'char_block_id_card_number.id')
                ->leftjoin('char_categories as ca','ca.id',  '=', 'blk.category_id')
                ->leftjoin('char_organizations','char_organizations.id',  '=', 'char_block_id_card_number.id_card_number')
                ->where('char_block_id_card_number.type','=',$type);

            if (count($condition) != 0) {
                $return =  $return
                    ->where(function ($q) use ($condition) {
                        for ($i = 0; $i < count($condition); $i++) {
                            foreach ($condition[$i] as $key => $value) {
                                $q->where($key, '=', $value);
                            }
                        }
                    });
            }

            $blocked_to=null;
            $blocked_from=null;

            if(isset($filter['blocked_to']) && $filter['blocked_to'] !=null){
                $blocked_to=date('Y-m-d',strtotime($filter['blocked_to']));
            }
            if(isset($filter['blocked_from']) && $filter['blocked_from'] !=null){
                $blocked_from=date('Y-m-d',strtotime($filter['blocked_from']));
            }

            if($blocked_from != null && $blocked_to != null) {
                $return = $return->whereBetween( 'blk.created_at', [ $blocked_from, $blocked_to]);
            }elseif($blocked_from != null && $blocked_to == null) {
                $return = $return->whereDate('blk.created_at', '>=', $blocked_from);
            }elseif($blocked_from == null && $blocked_to != null) {
                $return = $return->whereDate('blk.created_at', '<=', $blocked_to);
            }


            if($filter['status'] =='export') {
                return $return = $return->selectRaw("
                              CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS sponsor_name,
                              blk.created_at as block_date,
                              CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_block
                              "
                )
                    ->get();
            }else{
              $return = $return->selectRaw("
                              char_block_id_card_number.id,
                              blk.created_at as block_date,
                              char_organizations.id as sponsor_id,
                              ca.id as category_id,
                              ca.name as category_block,
                              CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_block,
                              CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS sponsor_name"
                );
              $itemsCount = isset($filter['itemsCount'])?$filter['itemsCount']:0;
              $records_per_page = Helpers::recordsPerPage($itemsCount, $return->count());
              return  $return->paginate($records_per_page);
            }
        }


         $char_block_id_card_number= ['type','id_card_number'];
         $char_block_categories= ['category_id'];
        $condition = [];

        foreach ($filter as $key => $value) {
            if(in_array($key, $char_block_id_card_number)) {
                if ($value != "") {
                    $data = ['char_block_id_card_number.' . $key => $value];
                    array_push($condition, $data);
                }
            }else{
                if(in_array($key, $char_block_categories)) {
                    if ($value != "") {
                        $data = ['blk.' . $key => $value];
                        array_push($condition, $data);
                    }
                }
            }
        }

        $category_type = null;
        if(isset($filter['category_type'])){
            $category_type = $filter['category_type'];
        }

        $return =\DB::table('char_block_id_card_number')
            ->leftjoin('char_block_categories as blk','blk.block_id',  '=', 'char_block_id_card_number.id')
            ->leftjoin('char_categories as ca', function($join) use ($category_type){
                $join->on('ca.id',  '=', 'blk.category_id');
            })
            ->leftjoin('char_persons','char_persons.id_card_number',  '=', 'char_block_id_card_number.id_card_number');

        if(!is_null($category_type)){
            $return->where('ca.type',$category_type);
        }
        if (count($condition) != 0) {
            $return =  $return
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                                $q->where($key, '=', $value);
                        }
                    }
                });
        }

        $blocked_to=null;
        $blocked_from=null;

        if(isset($filter['blocked_to']) && $filter['blocked_to'] !=null){
            $blocked_to=date('Y-m-d',strtotime($filter['blocked_to']));
        }
        if(isset($filter['blocked_from']) && $filter['blocked_from'] !=null){
            $blocked_from=date('Y-m-d',strtotime($filter['blocked_from']));
        }

        if($blocked_from != null && $blocked_to != null) {
            $return = $return->whereBetween( 'blk.created_at', [ $blocked_from, $blocked_to]);
        }elseif($blocked_from != null && $blocked_to == null) {
            $return = $return->whereDate('blk.created_at', '>=', $blocked_from);
        }elseif($blocked_from == null && $blocked_to != null) {
            $return = $return->whereDate('blk.created_at', '<=', $blocked_to);
        }

        $language_id =  \App\Http\Helpers::getLocale();
        $Not_registered = trans('common::application.Not registered card');

        if($filter['status'] =='export'){
            return $return     ->selectRaw("
                              char_block_id_card_number.id_card_number,
                              CASE
                                  WHEN char_persons.first_name is null THEN '$Not_registered'
                                  Else CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))
                              END
                              as name,
                              blk.created_at as block_date,
                             CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_block
                          "
            )->get();

        }else{
            $return->selectRaw("
                              char_block_id_card_number.id,
                              char_block_id_card_number.id_card_number,
                              blk.created_at as block_date,
                              ca.id as category_id,
                              CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_block,
                              CASE
                                  WHEN char_persons.first_name is null THEN '$Not_registered'
                                  Else CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))
                              END
                              as name
                          "
            ); 
            $itemsCount = isset($filter['itemsCount'])?$filter['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount, $return->count());
            return $return->paginate($records_per_page);
        }



    }

    public static function getBlockedList($type){

        $Not_registered = trans('common::application.Not registered card');
        $nomination = trans('common::application.nomination');
        $addition = trans('common::application.addition');

        $return =\DB::table('char_block_id_card_number')
            ->leftjoin('char_block_categories as blk','blk.block_id',  '=', 'char_block_id_card_number.id')
            ->leftjoin('char_categories as ca','ca.id',  '=', 'blk.category_id')
            ->leftjoin('char_persons','char_persons.id_card_number',  '=', 'char_block_id_card_number.id_card_number');

        if($type != -1) {
            return $return->where('char_block_id_card_number.type', '=', $type)
                ->selectRaw("
                              char_block_id_card_number.id_card_number,
                                CASE
                                  WHEN char_persons.first_name is null THEN '$Not_registered'
                                  Else CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))
                              END
                              as name,
                              blk.created_at as block_date,
                              ca.name as category_block"
                )
                ->get();

        }
        else{
            $id_list = [1,2];

            return $return->whereIn('char_block_id_card_number.type', $id_list)
                           ->selectRaw("
                              char_block_id_card_number.id_card_number,
                              CASE
                                  WHEN char_persons.gender is null THEN '$Not_registered'
                                  Else CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))
                              END
                              as name,
                              CASE
                                     WHEN char_block_id_card_number.type = 1 THEN '$addition'
                                    WHEN char_block_id_card_number.type = 2 THEN '$nomination'
                              END
                              as block_type,
                              ca.name as category_block,
                              blk.created_at as block_date

                         "
            )
                ->get();

        }

    }

    public static function isBlock($id_card_number,$category_id){

        $block=\DB::table('char_block_id_card_number')
               ->leftjoin('char_block_categories', function ($q) use($category_id) {
                   $q->on('char_block_id_card_number.id','=','char_block_categories.block_id');
                   $q->where('char_block_categories.category_id', $category_id);
               })
                ->where(function ($q) use($id_card_number) {
                    $q->where('char_block_id_card_number.type','=',2);
                    $q->where('char_block_id_card_number.id_card_number',$id_card_number);
                })
                ->first();


        if($block){
            return true;
        }

        return false;
    }

}

