<?php

namespace Common\Model;

class CloneGovernmentPersonsWorks  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_works';
    protected $fillable = ['IDNO','EMP_DOC_ID','MINISTRY_NAME','JOB_START_DT','DEGREE_NAME',
                           'EMP_STATE_DESC','EMP_WORK_STATUS','JOB_DESC','MIN_DATA_SOURCE'];
    public $timestamps = false;

}
