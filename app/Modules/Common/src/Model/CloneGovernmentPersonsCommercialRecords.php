<?php

namespace Common\Model;

class CloneGovernmentPersonsCommercialRecords  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_commercial_records';
    protected $fillable = ['IDNO','COMP_NAME','REC_CODE','PERSON_TYPE_DESC','IS_VALID_DESC',
                           'STATUS_ID_DESC','COMP_TYPE_DESC','REC_TYPE_DESC','REGISTER_NO' ,
                           'START_DATE','WORK_CLASS_DESC','BRAND_NAME','CITY_DESC'];
    public $timestamps = false;

}
