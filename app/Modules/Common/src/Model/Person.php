<?php

namespace Common\Model;

use Common\Model\PersonModels\PersonI18n;
use Common\Model\PersonModels\PersonAid;
use Common\Model\PersonModels\PersonBank;
use Common\Model\PersonModels\PersonContact;
use Common\Model\PersonModels\PersonEducation;
use Common\Model\PersonModels\PersonHealth;
use Common\Model\PersonModels\PersonProperties;
use Common\Model\PersonModels\PersonResidence;
use Common\Model\PersonModels\PersonWork;
use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers;
use Illuminate\Database\Eloquent\SoftDeletes;

class Person extends Model
{
    protected $table = 'char_persons';
    protected $hidden = ['created_at', 'updated_at'];
    protected $primaryKey='id';
    protected static $rules;
    protected $fillable = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','old_id_card_number',
        'gender', 'birthday', 'birth_place', 'refugee', 'unrwa_card_number','card_type',
        'marital_status_id', 'nationality', 'father_id', 'mother_id','is_qualified','qualified_card_number',
        'death_date', 'death_cause_id', 'city', 'country', 'governarate', 'location_id','mosques_id',
        'street_address', 'disease_id', 'monthly_income',"adscountry_id",'main',
        "adsdistrict_id", "adsneighborhood_id", "adsregion_id", "adssquare_id", "adsmosques_id",'actual_monthly_income', 'receivables',  'receivables_sk_amount',
        'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function country()
    {
        return $this->belongsTo('Setting\Model\LocationI18n', 'country', 'location_id');
    }

    public function city()
    {
        return $this->belongsTo('Setting\Model\LocationI18n', 'city', 'location_id');
    }

    public function mosque()
    {
        return $this->belongsTo('Setting\Model\LocationI18n', 'mosques_id', 'location_id');
    }

    public function district()
    {
        return $this->belongsTo('Setting\Model\LocationI18n', 'governarate', 'location_id');
    }

    public function location()
    {
        return $this->belongsTo('Setting\Model\LocationI18n', 'location_id', 'location_id');
    }

    public function contacts()
    {
        return $this->hasMany('Common\Model\PersonModels\PersonContact','person_id', 'id');
    }

    public function PersonsEducation()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonEducation','person_id', 'id');

    }

    public function c_education()
    {
        return $this->morphMany('Common\Model\PersonModels\PersonEducation', 'EducationCases');
    }

    public function PersonsWork()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonWork','person_id', 'id');
    }

    public function DeathCauses()
    {
        return $this->belongsTo('Setting\Model\DeathCauseI18n','death_cause_id','death_cause_id');
    }

    public function MaritalStatus()
    {
        return $this->belongsTo('Setting\Model\MaritalStatusI18n','marital_status_id','marital_status_id');
    }

    public function PersonsHealth()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonHealth','person_id', 'id');
    }

    public function PersonResidence()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonResidence','person_id', 'id');
    }

    public function Kinship()
    {
        return $this->belongsToMany('Setting\Model\KinshipI18n','char_persons_kinship','r_person_id','kinship_id');
    }

    public function Guardians()
    {
        return $this->hasMany('Sponsorship\Model\Guardians','individual_id','id');
    }

    public function PersonIslamic()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonIslamic','person_id', 'id');
    }

    public function PersonCases()
    {
        return $this->hasMany('Sponsorship\Model\Cases', 'person_id', 'id');
    }

    public function PersonsI18n()
    {
        return $this->hasMany('Common\Model\PersonModels\PersonI18n', 'person_id', 'id');
    }

    public function maritalStatusId()
    {
        return $this->belongsTo('Setting\Model\MaritalStatusI18n','marital_status_id','marital_status_id');
    }

    public function SponsorshipsPerson()
    {
        return $this->belongsTo('Sponsorship\Model\SponsorshipsPerson', 'id', 'person_id');
    }

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'first_name' => 'required',
                'second_name' => 'required',
                'third_name' => 'required',
                'last_name' => 'required',
                'id_card_number' => 'required',
                'card_type' => 'required',
                'gender' => 'required',
                'marital_status_id' => 'required',
                'birthday' => 'required',
                'birth_place' => 'required',
                'nationality' => 'required',
                'death_date' => 'required',
                'death_cause_id' => 'required',
                'father_id' => 'required',
                'mother_id' => 'required',
                'location_id' => 'required',
                'mosques_id' => 'required',
                'street_address' => 'required',
                'refugee' => 'required',
                'unrwa_card_number' => 'required',
                'spouses' => 'required',
            );
        }

        return self::$rules;
    }

    public static function oldfetch($options = array())
    {
        $language_id = 1;

        $defaults = array(
            'marital_status_name' => false,
            'birth_place_name' => false,
            'nationality_name' => false,
            'death_cause_name' => false,
            'father_death_cause' => false,
            'father_name' => false,
            'mother_name' => false,
            'mother_death_cause' => false,
            'location_name' => false,

            'id' => null,
            'marital_status_id' => null,
            'death_cause_id' => null,
            'father_id' => null,
            'father_death_cause_id' => null,
            'mother_id' => null,
            'mother_death_cause_id' => null,

            'paginate' => false,
        );

        $options = array_merge($defaults, $options);


        $query = self::query()
            ->addSelect(array(
                'char_persons.id',
                'char_persons.first_name',
                'char_persons.second_name',
                'char_persons.third_name',
                'char_persons.last_name',
            ))
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name');

        if ($options['marital_status_name']) {
            $query->join('char_marital_status_i18n', function ($join) use ($language_id) {
                $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                    ->where('char_marital_status_i18n.language_id', '=', $language_id);
            })
                ->addSelect(array(
                    'char_marital_status_i18n.name AS marital_status_name',
                ));
        }
        if ($options['death_cause_name']) {
            $query->join('char_death_causes_i18n', function ($join) use ($language_id) {
                $join->on('char_persons.death_cause_id', '=', 'char_death_causes_i18n.death_cause_id')
                    ->where('char_death_causes_i18n.language_id', '=', $language_id);
            })
                ->addSelect(array(
                    'char_marital_status_i18n.name AS death_cause_name',
                ));
        }
        if ($options['father_name'] || $options['father_death_cause']) {
            $query->join('char_persons AS father', 'char_persons.father_id', '=', 'father.id');
            if ($options['father_name']) {
                $query->addSelect(array(
                    'father.first_name AS father_first_name',
                    'father.second_name AS father_second_name',
                    'father.third_name AS father_third_name',
                    'father.last_name AS father_last_name',
                ));
            }
            if ($options['father_death_cause']) {
                $query->join('char_death_causes_i18n AS father_death', function ($join) use ($language_id) {
                    $join->on('father.death_cause_id', '=', 'father_death.death_cause_id')
                        ->where('father_death.language_id', '=', $language_id);
                })
                    ->addSelect(array(
                        'father_death.name AS father_death_cause',
                    ));
            }
        }
        if ($options['mother_name'] || $options['mother_death_cause']) {
            $query->join('char_persons AS mother', 'char_persons.mother_id', '=', 'mother.id');
            if ($options['mother_name']) {
                $query->addSelect(array(
                    'mother.first_name AS mother_first_name',
                    'mother.second_name AS mother_second_name',
                    'mother.third_name AS mother_third_name',
                    'mother.last_name AS mother_last_name',
                ));
            }
            if ($options['mother_death_cause']) {
                $query->join('char_death_causes_i18n AS mother_death', function ($join) use ($language_id) {
                    $join->on('mother.death_cause_id', '=', 'mother_death.death_cause_id')
                        ->where('mother_death.language_id', '=', $language_id);
                })
                    ->addSelect(array(
                        'mother_death.name AS mother_death_cause',
                    ));
            }
        }

        if ($options['id']) {
            return $query->where('char_persons.id', '=', $options['id'])->first();
        }
        if ($options['paginate']) {
            return $query->paginate($options['paginate']);
        }

        return $query->get();
    }

    public static function fetch($options = array())
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $en_language_id= 2;
        $defaults = array(
            'action' => null,
            'person_id' => null,
            'id' => null,
            'id_card_number' => null,
            'r_person_id' => null ,
            'l_person_id' => null ,
            'individual_id' => null ,
            'organization_id' => null ,
            'category_id' => null ,

            'full_name' => false,
            'marital_status_name' => false,
            'gender_name' => false,
            'person' => false,
            'persons_i18n' => false,
            'contacts' => false,
            'work' => false,
            'residence' => false,
            'education' => false,
            'islamic' => false,
            'health' => false,
            'banks' => false,
            'home_indoor' => false,
            'financial_aid_source' => false,
            'non_financial_aid_source' => false,
            'persons_properties' => false,
            'wives' => false,
            'persons_documents' => false
        );

        $options = array_merge($defaults, $options);
        $organization_id=$options['organization_id'];

        $query = self::query()->addSelect('char_persons.*')->selectRaw("char_persons.id as person_id");

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $is_can_work = trans('common::application.is_can_work');
        $not_can_work = trans('common::application.not_can_work');

        if ($options['action']=='show') {
            $query->selectRaw("CASE WHEN char_get_siblings_count(char_persons.id,'father',null) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'father',null) END AS father_brothers,
                                     CASE WHEN char_get_siblings_count(char_persons.id,'father',1) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'father',1) END AS male_father_brothers,                                                                            CASE WHEN char_get_siblings_count(char_persons.id,'father',2) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'father',2) END AS female_father_brothers,
                                     CASE WHEN char_get_siblings_count(char_persons.id,'mother',null) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'mother',null) END AS mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_persons.id,'mother',1) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'mother',1) END AS male_mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_persons.id,'mother',2) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'mother',3) END AS female_mother_brothers,
                                     CASE WHEN char_get_siblings_count(char_persons.id,'both',null) is null THEN 0
                                          Else char_get_siblings_count(char_persons.id,'both',null) END AS brothers,
                                     CASE WHEN char_get_children_count(char_persons.id,null) is null THEN 0
                                          Else char_get_children_count(char_persons.id,null) END AS children,
                                     CASE WHEN char_get_children_count(char_persons.id,1) is null THEN 0
                                          Else char_get_children_count(char_persons.id,1) END AS male_children,
                                     CASE WHEN char_get_children_count(char_persons.id,2) is null THEN 0
                                          Else char_get_children_count(char_persons.id,2) END AS female_children,
                                     CASE WHEN char_persons.spouses is null THEN '-' Else char_persons.spouses  END  AS spouses,
                                     CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                     CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                     CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                     CASE WHEN char_persons.death_date is null THEN '-' Else char_persons.death_date  END  AS death_date,
                                     CASE WHEN char_persons.death_date is null THEN '$yes' Else '$no'  END  AS is_alive,
                                     CASE WHEN char_persons.death_date is null THEN '$is_alive' Else '$deceased'  END  AS alive,
                                     CASE WHEN char_persons.monthly_income is null THEN '-' Else char_persons.monthly_income  END  AS monthly_income,
                                     CASE
                                               WHEN char_persons.refugee is null THEN '-'
                                               WHEN char_persons.refugee = 0 THEN '-'
                                               WHEN char_persons.refugee = 1 THEN ' $refugee '
                                               WHEN char_persons.refugee = 2 THEN '$citizen'
                                         END
                                     AS refugee,
                                     CASE WHEN char_persons.refugee = 2 THEN '$yes' Else '$no' END AS is_refugee,
                                     CASE WHEN char_persons.refugee = 2 THEN '*' Else ' ' END AS per_refugee,
                                     CASE WHEN char_persons.refugee = 1 THEN '$yes' Else '$no' END AS is_citizen,
                                     CASE WHEN char_persons.refugee = 1 THEN '*' Else ' ' END AS per_citizen,
                                     CASE
                                               WHEN char_persons.refugee is null THEN '-'
                                               WHEN char_persons.refugee = 0 THEN '-'
                                               WHEN char_persons.refugee = 1 THEN '-'
                                               WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                         END
                                         AS unrwa_card_number,
                                         CASE
                                               WHEN char_persons.gender is null THEN '-'
                                               WHEN char_persons.gender = 1 THEN '$male'
                                               WHEN char_persons.gender = 2 THEN '$female'
                                         END
                                         AS gender,
                                        CASE WHEN char_persons.gender = 1 THEN '*' Else ' ' END AS gender_male,         
                                        CASE WHEN char_persons.gender = 2 THEN '*' Else ' ' END AS gender_female,
                                        CASE
                                               WHEN char_persons.deserted is null THEN '-'
                                               WHEN char_persons.deserted = 1 THEN '$yes'
                                               WHEN char_persons.deserted = 0 THEN '$no'
                                         END
                                         AS deserted,
                                         CASE WHEN char_persons.deserted = 1 THEN '*' Else ' ' END AS is_deserted,         
                                         CASE WHEN char_persons.deserted = 0 THEN '*' Else ' ' END AS not_deserted,
                                         CASE WHEN char_persons.is_qualified = 1 THEN '$qualified' Else '$not_qualified' END AS is_qualified,
                                         CASE WHEN char_persons.is_qualified = 1 THEN '*' Else ' ' END AS qualified,         
                                         CASE WHEN char_persons.is_qualified != 1 THEN '*' Else ' ' END AS not_qualified,
                                         CASE WHEN char_persons.qualified_card_number is null THEN '-' Else char_persons.qualified_card_number  END  AS qualified_card_number,

                                     
                                        CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                                        CASE WHEN char_persons.first_name is null THEN '-' Else char_persons.first_name  END  AS first_name,
                                        CASE WHEN char_persons.second_name is null THEN '-' Else char_persons.second_name  END  AS second_name,
                                        CASE WHEN char_persons.third_name is null THEN '-' Else char_persons.third_name  END  AS third_name,
                                        CASE WHEN char_persons.last_name is null THEN '-' Else char_persons.last_name  END  AS last_name
                                     ");

            $query->leftjoin('char_death_causes_i18n', function($join) use ($language_id){
                $join->on('char_persons.death_cause_id', '=', 'char_death_causes_i18n.death_cause_id')
                    ->where('char_death_causes_i18n.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.death_cause_id is null THEN '-' Else char_death_causes_i18n.name  END  AS death_cause_name");

            $query->leftjoin('char_marital_status_i18n', function($join) use ($language_id) {
                $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                    ->where('char_marital_status_i18n.language_id', '=', $language_id);
            })
                ->selectRaw("CASE WHEN char_persons.marital_status_id is null THEN '-' Else char_marital_status_i18n.name  END  AS marital_status_name");

            $query->leftjoin('char_locations_i18n As birth_place_name', function($join) use ($language_id) {
                $join->on('char_persons.birth_place', '=','birth_place_name.location_id' )
                    ->where('birth_place_name.language_id',$language_id);
            })->selectRaw("CASE WHEN char_persons.birth_place is null THEN '-' Else birth_place_name.name  END  AS birth_place");

            $query->leftjoin('char_locations_i18n As nationality_name', function($join) use ($language_id)  {
                $join->on('char_persons.nationality', '=','nationality_name.location_id' )
                    ->where('nationality_name.language_id',$language_id);
            })->selectRaw("CASE WHEN char_persons.nationality is null THEN '-' Else nationality_name.name  END  AS nationality");

            $query->selectRaw("CASE WHEN char_persons.street_address  is null THEN '-' Else char_persons.street_address END AS street_address");

            $query->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id',$language_id);
            })->selectRaw("CASE WHEN char_persons.mosques_id is null THEN '-' Else mosques_name.name END   AS mosque_name");

            $query->leftjoin('char_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('char_persons.location_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',$language_id);
            })->selectRaw("CASE WHEN char_persons.location_id is null THEN '-' Else location_name.name END   AS nearLocation");

            $query->leftjoin('char_locations_i18n As city_name', function($join) use ($language_id)  {
                $join->on('char_persons.city', '=','city_name.location_id' )
                    ->where('city_name.language_id',$language_id);
            })->selectRaw("CASE WHEN city_name.name  is null THEN '-' Else city_name.name END   AS city");

            $query->leftjoin('char_locations_i18n As district_name', function($join)  use ($language_id) {
                $join->on('char_persons.governarate', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            })->selectRaw("CASE WHEN district_name.name  is null THEN '-' Else district_name.name END   AS governarate");

            $query->leftjoin('char_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.country', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })->selectRaw("CASE WHEN country_name.name  is null THEN '-' Else country_name.name END   AS country");

            $query->selectRaw("CONCAT(ifnull(country_name.name, ' '), ' ' ,ifnull(district_name.name, ' '),' ',ifnull(city_name.name, ' ')
                                     ,' ', ifnull(location_name.name,' '),' ', ifnull(mosques_name.name,' ') , ifnull(char_persons.street_address, ' ')) AS address");


            $query->leftjoin('char_aids_locations_i18n As ads_country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','ads_country_name.location_id' )
                    ->where('ads_country_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adscountry_id  is null THEN '-' Else ads_country_name.name END   AS aids_country");


            $query->leftjoin('char_aids_locations_i18n As ads_district_name', function($join)  use ($language_id) {
                $join->on('char_persons.adsdistrict_id', '=','ads_district_name.location_id' )
                    ->where('ads_district_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adsdistrict_id  is null THEN '-' Else ads_district_name.name END   AS aids_governarate");

            $query->leftjoin('char_aids_locations_i18n As ads_region_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsregion_id', '=','ads_region_name.location_id' )
                    ->where('ads_region_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adsregion_id is null THEN '-' Else ads_region_name.name END   AS aids_region_name");

            $query->leftjoin('char_aids_locations_i18n As ads_location_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsneighborhood_id', '=','ads_location_name.location_id' )
                    ->where('ads_location_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adsneighborhood_id is null THEN '-' Else ads_location_name.name END   AS aids_nearLocation");

            $query->leftjoin('char_aids_locations_i18n As ads_square_name', function($join) use ($language_id)  {
                $join->on('char_persons.adssquare_id', '=','ads_square_name.location_id' )
                    ->where('ads_square_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adssquare_id is null THEN '-' Else ads_square_name.name END   AS aids_square_name");

            $query->leftjoin('char_aids_locations_i18n As ads_mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsmosques_id', '=','ads_mosques_name.location_id' )
                    ->where('ads_mosques_name.language_id',$language_id);
            })
                ->selectRaw("CASE WHEN char_persons.adsmosques_id is null THEN '-' Else ads_mosques_name.name END   AS aids_mosque_name");

            $query->selectRaw("CONCAT(ifnull(ads_country_name.name,' '),' - ',
                                                            ifnull(ads_district_name.name,' '),' - ',
                                                            ifnull(ads_region_name.name,' '),' - ',
                                                            ifnull(ads_location_name.name,' '),' - ',
                                                            ifnull(ads_square_name.name,' '),' - ',
                                                            ifnull(ads_mosques_name.name,' '),' - ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS aids_address");

        }
        else{
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name");
        }
        if ($options['full_name']) {
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name");
        }

        if ($options['persons_i18n']) {
            $query->leftjoin('char_persons_i18n', function($join)  use ($en_language_id) {
                $join->on('char_persons.id', '=','char_persons_i18n.person_id')
                    ->where('char_persons_i18n.language_id',$en_language_id);
            });
            if ($options['action']=='show') {

                $query->selectRaw("CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                   CASE WHEN char_persons_i18n.first_name  is null THEN '-' Else char_persons_i18n.first_name  END  AS en_first_name,
                                   CASE WHEN char_persons_i18n.second_name is null THEN '-' Else char_persons_i18n.second_name END  AS en_second_name,
                                   CASE WHEN char_persons_i18n.third_name  is null THEN '-' Else char_persons_i18n.third_name  END  AS en_third_name,
                                   CASE WHEN char_persons_i18n.last_name   is null THEN '-' Else char_persons_i18n.last_name   END  AS en_last_name
                                 ");
            }
            else{
                $query->selectRaw("char_persons_i18n.first_name   AS en_first_name,
                                   char_persons_i18n.second_name  AS en_second_name,
                                   char_persons_i18n.third_name   AS en_third_name,
                                   char_persons_i18n.last_name    AS en_last_name
                                 ");
            }
        }
        if ($options['contacts']) {
            $query->leftjoin('char_persons_contact as wataniya_mob_number', function($join) {
                $join->on('char_persons.id', '=','wataniya_mob_number.person_id')
                    ->where('wataniya_mob_number.contact_type','wataniya');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN wataniya_mob_number.contact_value is null THEN '-' Else wataniya_mob_number.contact_value  END  AS wataniya");
            }else{
                $query->selectRaw("wataniya_mob_number.contact_value  AS wataniya");
            }

            $query->leftjoin('char_persons_contact as primary_mob_number', function($join) {
                $join->on('char_persons.id', '=','primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type','primary_mobile');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN primary_mob_number.contact_value is null THEN '-' Else primary_mob_number.contact_value  END  AS primary_mobile");
            }else{
                $query->selectRaw("primary_mob_number.contact_value  AS primary_mobile");
            }

            $query->leftjoin('char_persons_contact as secondary_mob_number', function($join) {
                $join->on('char_persons.id', '=','secondary_mob_number.person_id')
                    ->where('secondary_mob_number.contact_type','secondary_mobile');
            });

            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN secondary_mob_number.contact_value is null THEN '-' Else secondary_mob_number.contact_value  END  AS secondary_mobile");
            }else{
                $query->selectRaw("secondary_mob_number.contact_value  AS secondary_mobile");
            }

            $query->leftjoin('char_persons_contact as phone_number', function($join) {
                $join->on('char_persons.id', '=','phone_number.person_id')
                    ->where('phone_number.contact_type','phone');
            });
            if ($options['action']=='show') {
                $query->selectRaw("CASE WHEN phone_number.contact_value is null THEN '-' Else phone_number.contact_value  END  AS phone");
            }else{
                $query->selectRaw("phone_number.contact_value  AS phone");
            }

        }
        if ($options['work']) {
            $query->leftjoin('char_persons_work','char_persons.id', '=','char_persons_work.person_id');

            if($options['action']=='show'){

                $query->selectRaw("CASE WHEN char_persons_work.working is null THEN '-'
                                        WHEN char_persons_work.working = 1 THEN '$work'
                                        WHEN char_persons_work.working = 2 THEN '$not_work'
                                        END
                                  AS working,
                                  CASE WHEN char_persons_work.working is null THEN '-'
                                        WHEN char_persons_work.working = 1 THEN '$yes'
                                        WHEN char_persons_work.working = 2 THEN '$no'
                                        END
                                  AS works,
                                  CASE WHEN char_persons_work.working = 1 THEN '*' Else ' ' END AS is_working,         
                                  CASE WHEN char_persons_work.working is null THEN '*'
                                       WHEN char_persons_work.working = 3 THEN '*' Else ' ' END AS not_working         
                                  ");


                $query->selectRaw("CASE WHEN char_persons_work.can_work is null THEN '-'
                                       WHEN char_persons_work.can_work = 1 THEN '$is_can_work'
                                       WHEN char_persons_work.can_work = 2 THEN '$not_can_work'
                                       END 
                                  AS can_work,
                                  CASE WHEN char_persons_work.can_work is null THEN '-'
                                       WHEN char_persons_work.can_work = 1 THEN '$yes'
                                       WHEN char_persons_work.can_work = 2 THEN '$no'
                                       END 
                                  AS can_works,
                                   CASE WHEN char_persons_work.can_work = 1 THEN '*' Else ' ' END AS can_working,         
                                  CASE WHEN char_persons_work.can_work is null THEN '*'
                                       WHEN char_persons_work.can_work = 2 THEN '*' Else ' ' END AS can_not_working         
                                  ");

                $query->selectRaw("CASE WHEN char_persons_work.working is null THEN '-'
                                        WHEN char_persons_work.work_location is null THEN '-'
                                         WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                          WHEN char_persons_work.working = 2 THEN '-'
                                  END AS work_location");

                $query->leftjoin('char_work_jobs_i18n', function($join) use ($language_id){
                    $join->on('char_persons_work.work_job_id', '=',  'char_work_jobs_i18n.work_job_id')
                        ->where('char_work_jobs_i18n.language_id',$language_id);
                })
                    ->selectRaw("CASE  WHEN char_persons_work.working is null THEN '-'
                                       WHEN char_persons_work.work_job_id is null THEN '-'
                                       WHEN char_persons_work.working = 1 THEN char_work_jobs_i18n.name
                                       WHEN char_persons_work.working = 2 THEN '-'
                                    END
                               AS work_job");

                $query->leftjoin('char_work_status_i18n', function($join) use ($language_id) {
                    $join->on('char_persons_work.work_status_id', '=',  'char_work_status_i18n.work_status_id')
                        ->where('char_work_status_i18n.language_id',$language_id);
                })
                    ->selectRaw("CASE  WHEN char_persons_work.working is null THEN '-'
                                       WHEN char_persons_work.work_status_id is null THEN '-'
                                       WHEN char_persons_work.working = 1 THEN char_work_status_i18n.name
                                                   WHEN char_persons_work.working = 2 THEN '-'
                                                   END
                                             AS work_status");

                $query->leftjoin('char_work_wages',function($join) use ($language_id) {
                    $join->on('char_persons_work.work_wage_id', '=', 'char_work_wages.id');
                })->selectRaw("CASE  WHEN char_persons_work.working is null THEN '-'
                                     WHEN char_persons_work.work_wage_id is null THEN '-'
                                                       WHEN char_persons_work.working = 1 THEN char_work_wages.name
                                                       WHEN char_persons_work.working = 2 THEN '-'
                                                 END
                                                 AS work_wage");

                $query ->leftjoin('char_work_reasons_i18n', function($join) use ($language_id)  {
                    $join->on('char_persons_work.work_reason_id', '=', 'char_work_reasons_i18n.work_reason_id')
                        ->where('char_work_reasons_i18n.language_id',$language_id);
                })->selectRaw("CASE
                                           WHEN char_persons_work.can_work is null THEN '-'
                                           WHEN char_persons_work.work_reason_id is null THEN '-'
                                           WHEN char_persons_work.can_work = 1 THEN '-'
                                           WHEN char_persons_work.can_work = 2 THEN char_work_reasons_i18n.name
                                       END AS work_reason");



            }else{
                $query->addSelect('char_persons_work.*');
            }

        }
        if ($options['residence']) {
            $query->leftjoin('char_residence', 'char_persons.id', '=','char_residence.person_id');

            if($options['action']=='show'){

                $query ->leftjoin('char_roof_materials_i18n', function($join) use ($language_id){
                    $join->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id')
                        ->where('char_roof_materials_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_roof_materials_i18n.name is null THEN '-' Else char_roof_materials_i18n.name END   AS roof_materials");

                $query->leftjoin('char_property_types_i18n', function ($join) use ($language_id){
                    $join->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id')
                        ->where('char_property_types_i18n.language_id', $language_id);
                })->selectRaw("CASE WHEN char_property_types_i18n.name is null THEN '-' Else char_property_types_i18n.name END   AS property_types");

                $query->leftjoin('char_building_status_i18n', function($join) use ($language_id){
                    $join->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition')
                        ->where('char_building_status_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_building_status_i18n.name is null THEN '-' Else char_building_status_i18n.name  END  AS house_condition");

                $query->leftjoin('char_house_status_i18n', function($join) use ($language_id){
                    $join->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition')
                        ->where('char_house_status_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_house_status_i18n.name is null     THEN '-' Else char_house_status_i18n.name  END  AS residence_condition");

                $query->leftjoin('char_furniture_status_i18n', function($join) use ($language_id){
                    $join->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition')
                        ->where('char_furniture_status_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_furniture_status_i18n.name is null THEN '-' Else char_furniture_status_i18n.name  END  AS indoor_condition");

                $query ->leftjoin('char_habitable_status_i18n', function($join) use ($language_id){
                    $join->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable')
                        ->where('char_habitable_status_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_habitable_status_i18n.name is null THEN '-' Else char_habitable_status_i18n.name  END  AS habitable");

                $query ->selectRaw("CASE WHEN char_residence.rooms is null THEN '-' Else char_residence.rooms  END  AS rooms");
                $query->selectRaw("CASE WHEN char_residence.area is null THEN '-' Else char_residence.area  END  AS area");
                $query->selectRaw("CASE WHEN char_residence.rent_value is null       THEN '-' Else char_residence.rent_value  END 
                                             AS rent_value ,
                                              CASE WHEN char_residence.repair_notes is null       THEN ' ' 
                                                                            Else char_residence.repair_notes  END  AS repair_notes ,
                                                                        CASE WHEN char_residence.need_repair is null THEN ' '
                                                                            WHEN char_residence.need_repair = 1 THEN '$yes'
                                                                            WHEN char_residence.need_repair = 2 THEN '$no'
                                                                            END
                                                                        AS need_repair");

            }else{
                $query->addSelect('char_residence.*');
            }
        }
        if ($options['education']) {
            $query->leftjoin('char_persons_education', 'char_persons.id', '=','char_persons_education.person_id');

            if($options['action']=='show'){
                $query->selectRaw("        CASE
                                                          WHEN char_persons_education.study is null THEN '-'
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                          WHEN char_persons_education.currently_study is null THEN '-'
                                                          WHEN char_persons_education.currently_study = 1 THEN '$yes'
                                                          WHEN char_persons_education.currently_study = 0 THEN '$no'
                                                     END
                                                     AS currently_study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                                       CASE
                                                               WHEN char_persons_education.authority = 1 THEN ' '
                                                               WHEN char_persons_education.authority = 2 THEN 'X'
                                                               WHEN char_persons_education.authority = 3 THEN ' '
                                                                Else ' '
                                                     END
                                                     AS authority_is_paid, 
                                                     CASE
                                                               WHEN char_persons_education.authority = 1 THEN 'X'
                                                               WHEN char_persons_education.authority = 2 THEN ' '
                                                               WHEN char_persons_education.authority = 3 THEN 'X'
                                                                Else ' '
                                                     END
                                                     AS authority_is_not__paid,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university'                                         
                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.specialization is null THEN '-' Else char_persons_education.specialization END  AS specialization,
                                                     CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school
                                             ");
                $query->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                    $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                    $q->where('char_edu_stages.language_id',$language_id);
                })->selectRaw("CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage");

                $query->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                    $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                    $q->where('char_edu_authorities.language_id',$language_id);
                })->selectRaw("CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority");

                $query->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                    $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                    $q->where('char_edu_degrees.language_id',$language_id);
                })->selectRaw("CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree");


            }else{
                $query->addSelect('char_persons_education.*');
            }
        }
        if($options['islamic']) {
            $query->leftjoin('char_persons_islamic_commitment', 'char_persons.id', '=', 'char_persons_islamic_commitment.person_id');
            if($options['action']=='show') {
                $query->SelectRaw("
                                CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                     WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                     WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                     WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                               END AS prayer,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                               END AS save_quran,
                                CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN 'X'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN 'X'
                                    Else ' '
                               END AS is_not_save_quran,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN ' '
                                    Else 'X'
                               END AS is_save_quran,
                               CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' 
                                    Else char_persons_islamic_commitment.prayer_reason 
                               END AS prayer_reason,
                               CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' 
                                    Else char_persons_islamic_commitment.quran_reason 
                               END AS quran_reason,
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' 
                                    Else char_persons_islamic_commitment.quran_parts
                               END  AS quran_parts,
                               CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' 
                                    Else char_persons_islamic_commitment.quran_chapters 
                               END AS quran_chapters,
                               CASE WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    WHEN char_persons_islamic_commitment.quran_center = 1 THEN '$yes'
                                    Else '$yes'
                               END AS quran_center
                              ");
            }else{
                $query->addSelect('char_persons_islamic_commitment.*');
            }
        }
        if($options['health']){
            $query->leftjoin('char_persons_health', 'char_persons.id', '=','char_persons_health.person_id');

            if($options['action']=='show') {
                $query->leftjoin('char_diseases_i18n', function ($q) {
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id', 1);
                })->selectRaw("CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS diseases_name");

                $query->selectRaw("CASE WHEN char_persons_health.condition is null THEN '-'
                                                  WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                  WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                  WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                END AS health_status,
                                  CASE WHEN char_persons_health.condition = 1 THEN '*' Else ' ' END AS good_health_status,
                                  CASE WHEN char_persons_health.condition = 2 THEN '*' Else ' ' END AS chronic_disease,
                                  CASE WHEN char_persons_health.condition = 3 THEN '*' Else ' ' END AS disabled,
                                  CASE WHEN char_persons_health.condition = 1 THEN 'X' Else ' ' END AS has_good_health,
                                          CASE WHEN char_persons_health.condition is null THEN '-'
                                                  WHEN char_persons_health.condition = 1 THEN ' '
                                                  WHEN char_persons_health.condition = 2 THEN 'X'
                                                  WHEN char_persons_health.condition = 3 THEN 'X'
                                           END AS has_disease,
                                  CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS health_details         
                              ");
            }else{
                $query->addSelect('char_persons_health.*');
            }
        }
        if($options['category_id']){
            $category_id=$options['category_id'];
            $query->leftjoin('char_cases', function ($q) use($category_id,$organization_id) {
                $q->on('char_cases.person_id', '=', 'char_persons.id');
                $q->where('char_cases.category_id', $category_id);
                $q->where('char_cases.organization_id', $organization_id);
            });

            $query->selectRaw('char_get_active_case_count(char_persons.id) as active_case_count,
                                         char_cases.id as case_id,char_cases.status as case_status,char_cases.deleted_at');
        }
        if ($options['r_person_id']) {
            $r_person_id=$options['r_person_id'];
            $query->leftjoin('char_persons_kinship', function ($q) use($r_person_id) {
                $q->on('char_persons_kinship.l_person_id', '=', 'char_persons.id');
                $q->where('char_persons_kinship.r_person_id', '=', $r_person_id);
            });
            if ($options['action']=='show'){
                $query->join('char_kinship_i18n', function($q) use ($language_id) {
                    $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                    $q->where('char_kinship_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name END   AS kinship");
            }else{
                $query->addSelect('char_persons_kinship.*')->selectRaw("char_persons_kinship.kinship_id  AS kinship");
            }
        }
        if ($options['l_person_id']) {
            $l_person_id=$options['l_person_id'];
            $query->leftjoin('char_persons_kinship as l_kinship', function ($q) use($l_person_id) {
                $q->on('l_kinship.r_person_id', '=', 'char_persons.id');
                $q->where('l_kinship.l_person_id', '=', $l_person_id);
            })->addSelect('l_kinship.*');

            if ($options['action']=='show'){
                $query->join('char_kinship_i18n as l_kinship_i18n', function($q) use ($language_id) {
                    $q->on('l_kinship_i18n.kinship_id', '=', 'l_kinship.kinship_id');
                    $q->where('l_kinship_i18n.language_id',$language_id);
                })->selectRaw("CASE WHEN l_kinship_i18n.kinship_id is null THEN '-' Else l_kinship_i18n.name END   AS kinship");
            }else{
                $query->addSelect('l_kinship.*')->selectRaw("l_kinship.kinship_id  AS kinship");
            }


        }
        if ($options['individual_id']) {
            $individual_id=$options['individual_id'];
            $query->leftjoin('char_guardians', function ($q) use($individual_id,$organization_id) {
                $q->on('char_guardians.guardian_id', '=', 'char_persons.id');
                $q->where('char_guardians.individual_id', '=', $individual_id);
                $q->where('char_guardians.organization_id', $organization_id);
            })->addSelect('char_guardians.*')->selectRaw("char_guardians.kinship_id  AS kinship");
        }

        if($options['person_id']){
            $query->where('char_persons.id', '=', $options['person_id']);
        }
        if($options['id']){
            $query->where('char_persons.id', '=', $options['id']);
        }
        if($options['id_card_number']){
            $query->where('char_persons.id_card_number', '=', $options['id_card_number']);
        }

        $return =$query->first();

        if($return){
            $return->wives =[];
            $return->banks =[];
            $return->financial_aid_source =[];
            $return->non_financial_aid_source =[];
            $return->persons_properties =[];
            $return->persons_documents =[];

            if ($options['wives']) {
                $husband_kinship_id = 15; // null
                $husband_kinship_id=\Setting\Model\Setting::where('id','husband_kinship')->first();
                if($husband_kinship_id){
                    if(!is_null($husband_kinship_id->value) and $husband_kinship_id->value != "") {
                        $husband_kinship_id=$husband_kinship_id->value;
                    }
                }

                $return->wives=\Common\Model\PersonModels\PersonKinship::fetch('wives','l_person_id',$return->id,$husband_kinship_id);
            }
            if ($options['banks']) {
                $return->banks=\Common\Model\PersonModels\PersonBank::getPersonBanks($return->id,$organization_id);
            }
            if ($options['financial_aid_source']) {
                $return->financial_aid_source=\Common\Model\PersonModels\PersonAid::getPersonAids($return->id,1,$options['action']);
            }
            if ($options['non_financial_aid_source']) {
                $return->non_financial_aid_source=\Common\Model\PersonModels\PersonAid::getPersonAids($return->id,2,$options['action']);
            }
            if ($options['persons_properties']) {
                $return->persons_properties=\Common\Model\PersonModels\PersonProperties::getPersonProperties($return->id,$options['action']);
            }
            if ($options['persons_documents']) {
                $return->persons_documents=\Common\Model\PersonDocument::getPersonDocuments($return->id);
            }

        }
        return $return;
    }

    public static function voucherFilter($filters)
    {
        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $language_id =  \App\Http\Helpers::getLocale();
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');

        $condition =[];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $char_vouchers =['title','content','header','footer','notes','category_id','center',
                        'case_category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' ,
                        'serial','un_serial','collected', 'transfer','allow_day'];


        foreach ($filters as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['v.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()
             ->join('char_vouchers_persons AS vp', 'char_persons.id', '=', 'vp.person_id')
             ->leftjoin('char_vouchers AS v', function($q){
                 $q->on('v.id', '=', 'vp.voucher_id');
                 $q->whereNull('v.deleted_at');
             })
             ->leftjoin('char_vouchers_categories AS vc', 'v.category_id', '=', 'vc.id')
             ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
             ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
             ->leftjoin('char_currencies AS c', 'v.currency_id', '=', 'c.id')
//            ->where('char_persons.id_card_number', '=', $filters['id_card_number'])
            ->selectRaw( "char_persons.*, vp.*, v.*,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS fullname,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS nm,
                            TIME_FORMAT(vp.receipt_time, '%r') as receipt_time_,
                            CASE   WHEN vp.status = 1 THEN '$receipt' Else ' $not_receipt' END as receipt_status,
                            v.un_serial as un_serial,
                            v.serial as serial,
                            (v.value) as value,
                            round((v.value * v.exchange_rate),0) as sh_value,
                            vp.status AS receipt_status_id,
                            vc.name AS category_name,
                            org.name AS organization_name,
                            c.name AS currency_name");

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $str = ['v.title','v.serial','v.content','v.header','v.footer','v.notes',
                        'char_persons.first_name', 'char_persons.second_name',
                        'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($filters['target'])){

            if($user->type == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('v.organization_id',$organization_id);
                    $anq->orwherein('v.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $query->where(function ($anq) use ($organization_id) {
                    /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                    $anq->wherein('v.organization_id', function($quer) use($organization_id) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        if(isset($filters['codes'])){
            if(is_array($filters['codes'])){
                if(sizeof($filters['codes'])  > 0){
                    $query->whereIn('v.un_serial' ,$filters['codes']);
                }
            }
        }

        if(isset($filters['vouchers_code'])){
            if(is_array($filters['vouchers_code'])){
                if(sizeof($filters['vouchers_code'])  > 0){
                    $query->whereIn('v.serial' ,$filters['vouchers_code']);
                }
            }
        }

        $min_value=null;
        $max_value=null;

        if(isset($filters['max_value']) && $filters['max_value'] !=null && $filters['max_value'] !=""){
            $max_value=$filters['max_value'];
        }

        if(isset($filters['min_value']) && $filters['min_value'] !=null && $filters['min_value'] !=""){
            $min_value=$filters['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $query = $query->whereRaw(" $max_value >= char_vouchers.value");
            $query = $query->whereRaw(" $min_value <= char_vouchers.value");
        }elseif($max_value != null && $min_value == null) {
            $query = $query->whereRaw(" $max_value >= char_vouchers.value");
        }elseif($max_value == null && $min_value != null) {
            $query = $query->whereRaw(" $min_value <= char_vouchers.value");
        }


        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'vp.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('vp.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('vp.receipt_date', '<=', $receipt_date_to);
        }

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN sponsor.name is null THEN ' ' Else sponsor.name END AS sponsor_name");
        }else{
            $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
        }


        $paginate = $query->get();

        if(sizeof($paginate)){
            $response['data'] = $paginate;
            $response['full_name'] = $paginate[0]->fullname;
            $response['id_card_number'] = $paginate[0]->id_card_number;

            $amount = 0;
            $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
            if($total){
                $amount = $total->total;
            }

            $response['total'] =round($amount,0);

            $response['success'] = true;
        }else{
            $response['msg'] = trans('common::application.There is no vouchers to show');
            $response['success'] = false;
        }
        return $response;
    }

    public static function voucherFilterDetails($filters){


        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $daughter_kinship_id = 22;
        $son_kinship_id = 2;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;

        $return = [
            'success' =>  false,
            'total' =>  0, 'data' =>  [],
            'parents' =>  [], 'parents_total' =>  0,
            'wives' =>  [], 'wives_total' =>  0,
            'childrens' =>  [], 'childrens_total' =>  0,
        ];

        if($filters['for_person'] == true){
           $vouchers = self::voucherFilterGroup($filters,$user,$organization_id);
           if (sizeof($vouchers['data']) > 0){
               $return['data'] = $vouchers['data'];
               $return['total'] = $vouchers['total'];
           }
        }

        if($filters['for_parent'] == true){
            $fathers_vouchers = self::VoucherFilterFathers($filters,$user,$organization_id);
            $return['parents_total'] = $fathers_vouchers['total'];
            $kinship_name = trans('aid::application.father');
            foreach ($fathers_vouchers['data'] as $key=>$value){
                $value['kinship_name']=$kinship_name;
                $return['parents'][]=$value;
            }

            $mothers_vouchers = self::VoucherFilterMothers($filters,$user,$organization_id);
            $return['parents_total'] += $mothers_vouchers['total'];
            $kinship_name = trans('aid::application.mother');
            foreach ($mothers_vouchers['data'] as $key=>$value){
                $value['kinship_name']=$kinship_name;
                $return['parents'][]=$value;
            }
        }

        if($filters['for_children'] == true){
            $childrens_filters = $filters;
            $childrens_filters['single'] = true;
            $childrens_filters['kinship_ids'] = [$daughter_kinship_id,$son_kinship_id];
            $vouchers = self::VoucherFilterRelations($childrens_filters,$user,$organization_id);
            if (sizeof($vouchers['data']) > 0){
                $return['childrens'] = $vouchers['data'];
                $return['childrens_total'] = $vouchers['total'];
            }
        }

        if($filters['for_wife'] == true){
            $wives_filters = $filters;
            $wives_filters['kinship_ids'] = [$husband_kinship_id,$wife_kinship_id];
            $wives_filters['single'] = false;
            $vouchers = self::VoucherFilterRelations($wives_filters,$user,$organization_id);
            if (sizeof($vouchers['data']) > 0){
                $return['wives'] = $vouchers['data'];
                $return['wives_total'] = $vouchers['total'];
            }
        }

        return $return;

    }

    public static function voucherFilterGroup($filters,$user,$organization_id)
    {

        $condition =[];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name'];
        $char_vouchers =['title','content','header','footer','notes','un_serial',
            'category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' , 'transfer','allow_day'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['v.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()
            ->join('char_vouchers_persons AS vp', 'char_persons.id', '=', 'vp.person_id')
            ->leftjoin('char_vouchers AS v', function($q){
                $q->on('v.id', '=', 'vp.voucher_id');
                $q->whereNull('v.deleted_at');
            })
            ->leftjoin('char_vouchers_categories AS vc', 'v.category_id', '=', 'vc.id')
            ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
            ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
            ->leftjoin('char_currencies AS c', 'v.currency_id', '=', 'c.id')
//            ->where('char_persons.id_card_number', '=', $filters['id_card_number'])
            ->selectRaw( "char_persons.*, vp.*, v.*,
                                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS fullname,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS nm,
                            TIME_FORMAT(vp.receipt_time, '%r') as receipt_time_,
                            (v.value) as value,
                             round((v.value * v.exchange_rate),0) as sh_value,
                            vc.name AS category_name,
                            org.name AS organization_name,
                            c.name AS currency_name");

//        id_card_number
        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $str = ['v.title','v.content','v.header','v.footer','v.notes',
                    'char_persons.first_name', 'char_persons.second_name',
                    'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($filters['id_card_number'])){
            if(is_array($filters['id_card_number'])){
                if(sizeof($filters['id_card_number'])  > 0){
                    $query->whereIn('char_persons.id_card_number' ,$filters['id_card_number']);
                }
            }
        }

        if(isset($filters['titles'])){
            $titles = explode(',', $filters['titles']);
            if(is_array($titles)){
                if(sizeof($titles)  > 0){
                    $query->whereIn('v.title' ,$titles);
                }
            }
        }

        if(isset($filters['codes'])){
            $codes = explode(',', $filters['codes']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.un_serial' ,$codes);
                }
            }
        }

        if(isset($filters['vouchers_code'])){
            $codes = explode(',', $filters['vouchers_code']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.serial' ,$codes);
                }
            }
        }

        if(isset($filters['target'])){

            if($user->type == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('v.organization_id',$organization_id);
                    $anq->orwherein('v.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $query->where(function ($anq) use ($organization_id) {
                    $anq->wherein('v.organization_id', function($quer) use($organization_id) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        $min_value=null;
        $max_value=null;

        if(isset($filters['max_value']) && $filters['max_value'] !=null && $filters['max_value'] !=""){
            $max_value=$filters['max_value'];
        }

        if(isset($filters['min_value']) && $filters['min_value'] !=null && $filters['min_value'] !=""){
            $min_value=$filters['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $query = $query->whereRaw(" $max_value >= value");
            $query = $query->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $query = $query->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $query = $query->whereRaw(" $min_value <= value");
        }


        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'vp.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('vp.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('vp.receipt_date', '<=', $receipt_date_to);
        }

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN sponsor.name is null THEN ' ' Else sponsor.name END AS sponsor_name");
        }else{
            $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
        }

        $paginage = $query->get();
        $response['data'] = [];
        $response['total'] = [];
        $response['success'] = false;

        if(sizeof($paginage)){
            $response['data'] = $paginage;
            $amount = 0;
            $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
            if($total){
                $amount = $total->total;
            }

            $response['total'] =round($amount,0);
            $response['success'] = true;
        }else{
            $response['msg'] = trans('common::application.There is no vouchers to this card number');
        }
        return $response;
    }

    public static function VoucherFilterFathers($filters,$user,$organization_id)
    {


        $condition =[];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name'];
        $char_vouchers =['title','content','header','footer','notes','un_serial',
            'category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' , 'transfer','allow_day'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['v.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = \DB::table("char_persons")
                    ->join('char_persons AS father', function($q){
                        $q->on('father.id', '=', 'char_persons.father_id');
                        $q->whereNull('char_persons.father_id');
                    })
                    ->join('char_vouchers_persons AS vp', 'vp.person_id', '=','char_persons.father_id')
                    ->leftjoin('char_vouchers AS v', function($q){
                        $q->on('v.id', '=', 'vp.voucher_id');
                        $q->whereNull('v.deleted_at');
                    })
                    ->leftjoin('char_vouchers_categories AS vc', 'v.category_id', '=', 'vc.id')
                    ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
                    ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
                    ->leftjoin('char_currencies AS c', 'v.currency_id', '=', 'c.id')
                    ->selectRaw( "father.*, vp.*, v.*,
                                  char_persons.id_card_number AS main_id_card_number,
                                  CONCAT(ifnull(father.first_name, ' '), ' ' ,ifnull(father.second_name, ' '),' ',ifnull(father.third_name, ' '),' ', ifnull(father.last_name,' ')) AS fullname,
                                  CONCAT(ifnull(father.first_name, ' '), ' ' ,ifnull(father.second_name, ' '),' ',ifnull(father.third_name, ' '),' ', ifnull(father.last_name,' ')) AS nm,
                                  TIME_FORMAT(vp.receipt_time, '%r') as receipt_time_,
                                  (v.value) as value,
                                  round((v.value * v.exchange_rate),0) as sh_value,
                                  vc.name AS category_name,
                                  org.name AS organization_name,
                                  c.name AS currency_name");

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $str = ['v.title','v.content','v.header','v.footer','v.notes',
                    'char_persons.first_name', 'char_persons.second_name',
                    'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($filters['id_card_number'])){
            if(is_array($filters['id_card_number'])){
                if(sizeof($filters['id_card_number'])  > 0){
                    $query->whereIn('char_persons.id_card_number' ,$filters['id_card_number']);
                }
            }
        }

        if(isset($filters['titles'])){
            $titles = explode(',', $filters['titles']);
            if(is_array($titles)){
                if(sizeof($titles)  > 0){
                    $query->whereIn('v.title' ,$titles);
                }
            }
        }

        if(isset($filters['codes'])){
            $codes = explode(',', $filters['codes']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.un_serial' ,$codes);
                }
            }
        }

        if(isset($filters['vouchers_code'])){
            $codes = explode(',', $filters['vouchers_code']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.serial' ,$codes);
                }
            }
        }

        if(isset($filters['target'])){

            if($user->type == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('v.organization_id',$organization_id);
                    $anq->orwherein('v.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $query->where(function ($anq) use ($organization_id) {
                    $anq->wherein('v.organization_id', function($quer) use($organization_id) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        $min_value=null;
        $max_value=null;

        if(isset($filters['max_value']) && $filters['max_value'] !=null && $filters['max_value'] !=""){
            $max_value=$filters['max_value'];
        }

        if(isset($filters['min_value']) && $filters['min_value'] !=null && $filters['min_value'] !=""){
            $min_value=$filters['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $query = $query->whereRaw(" $max_value >= value");
            $query = $query->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $query = $query->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $query = $query->whereRaw(" $min_value <= value");
        }


        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'vp.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('vp.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('vp.receipt_date', '<=', $receipt_date_to);
        }

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN sponsor.name is null THEN ' ' Else sponsor.name END AS sponsor_name");
        }else{
            $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
        }

        $paginage = $query->get();
        $response['data'] = [];
        $response['total'] = [];
        $response['success'] = false;

        if(sizeof($paginage)){
            $response['data'] = $paginage;
            $amount = 0;
            $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
            if($total){
                $amount = $total->total;
            }

            $response['total'] =round($amount,0);
            $response['success'] = true;
        }else{
            $response['msg'] = trans('common::application.There is no vouchers to this card number');
        }
        return $response;
    }

    public static function VoucherFilterMothers($filters,$user,$organization_id)
    {

        $condition =[];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name'];
        $char_vouchers =['title','content','header','footer','notes','un_serial',
            'category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' , 'transfer','allow_day'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['v.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = \DB::table("char_persons")
                    ->join('char_persons AS mother', function($q){
                        $q->on('mother.id', '=', 'char_persons.mother_id');
                        $q->whereNull('char_persons.mother_id');
                    })
                    ->join('char_vouchers_persons AS vp', 'vp.person_id', '=','char_persons.mother_id')
                    ->leftjoin('char_vouchers AS v', function($q){
                        $q->on('v.id', '=', 'vp.voucher_id');
                        $q->whereNull('v.deleted_at');
                    })
                    ->leftjoin('char_vouchers_categories AS vc', 'v.category_id', '=', 'vc.id')
                    ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
                    ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
                    ->leftjoin('char_currencies AS c', 'v.currency_id', '=', 'c.id')
                    ->selectRaw( "mother.*, vp.*, v.*,
                                  char_persons.id_card_number AS main_id_card_number,
                                  CONCAT(ifnull(mother.first_name, ' '), ' ' ,ifnull(mother.second_name, ' '),' ',ifnull(mother.third_name, ' '),' ', ifnull(mother.last_name,' ')) AS fullname,
                                  CONCAT(ifnull(mother.first_name, ' '), ' ' ,ifnull(mother.second_name, ' '),' ',ifnull(mother.third_name, ' '),' ', ifnull(mother.last_name,' ')) AS nm,
                                  TIME_FORMAT(vp.receipt_time, '%r') as receipt_time_,
                                  (v.value) as value,
                                  round((v.value * v.exchange_rate),0) as sh_value,
                                  vc.name AS category_name,
                                  org.name AS organization_name,
                                  c.name AS currency_name");

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $str = ['v.title','v.content','v.header','v.footer','v.notes',
                    'char_persons.first_name', 'char_persons.second_name',
                    'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($filters['id_card_number'])){
            if(is_array($filters['id_card_number'])){
                if(sizeof($filters['id_card_number'])  > 0){
                    $query->whereIn('char_persons.id_card_number' ,$filters['id_card_number']);
                }
            }
        }

        if(isset($filters['titles'])){
            $titles = explode(',', $filters['titles']);
            if(is_array($titles)){
                if(sizeof($titles)  > 0){
                    $query->whereIn('v.title' ,$titles);
                }
            }
        }

        if(isset($filters['codes'])){
            $codes = explode(',', $filters['codes']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.un_serial' ,$codes);
                }
            }
        }

        if(isset($filters['vouchers_code'])){
            $codes = explode(',', $filters['vouchers_code']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.serial' ,$codes);
                }
            }
        }

        if(isset($filters['target'])){

            if($user->type == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('v.organization_id',$organization_id);
                    $anq->orwherein('v.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $query->where(function ($anq) use ($organization_id) {
                    $anq->wherein('v.organization_id', function($quer) use($organization_id) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        $min_value=null;
        $max_value=null;

        if(isset($filters['max_value']) && $filters['max_value'] !=null && $filters['max_value'] !=""){
            $max_value=$filters['max_value'];
        }

        if(isset($filters['min_value']) && $filters['min_value'] !=null && $filters['min_value'] !=""){
            $min_value=$filters['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $query = $query->whereRaw(" $max_value >= value");
            $query = $query->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $query = $query->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $query = $query->whereRaw(" $min_value <= value");
        }


        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'vp.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('vp.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('vp.receipt_date', '<=', $receipt_date_to);
        }

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN sponsor.name is null THEN ' ' Else sponsor.name END AS sponsor_name");
        }else{
            $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
        }

        $paginage = $query->get();
        $response['data'] = [];
        $response['total'] = [];
        $response['success'] = false;

        if(sizeof($paginage)){
            $response['data'] = $paginage;
            $amount = 0;
            $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
            if($total){
                $amount = $total->total;
            }

            $response['total'] =round($amount,0);
            $response['success'] = true;
        }else{
            $response['msg'] = trans('common::application.There is no vouchers to this card number');
        }
        return $response;
    }

    public static function VoucherFilterRelations($filters,$user,$organization_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $condition =[];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name'];
        $char_vouchers =['title','content','header','footer','notes','un_serial',
            'category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' , 'transfer','allow_day'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['v.'. $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = \DB::table("char_persons")
                    ->join('char_persons_kinship', function($q) use($filters){
                        $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                        $q->whereIn('char_persons_kinship.kinship_id', $filters['kinship_ids']);
                    })
                    ->join('char_persons AS left_person', function($q) use($filters){
                        $q->on('left_person.id', '=', 'char_persons_kinship.l_person_id');
                        if ($filters['single']){
                            $q->where('left_person.marital_status_id', 10);
                        }

                    })
                    ->join('char_kinship_i18n', function($q)use ($language_id){
                        $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                        $q->where('char_kinship_i18n.language_id', $language_id);
                    })
                    ->join('char_vouchers_persons AS vp', 'vp.person_id', '=','left_person.id')
                    ->leftjoin('char_vouchers AS v', function($q){
                        $q->on('v.id', '=', 'vp.voucher_id');
                        $q->whereNull('v.deleted_at');
                    })
                    ->leftjoin('char_vouchers_categories AS vc', 'v.category_id', '=', 'vc.id')
                    ->leftjoin('char_organizations as sponsor', 'v.sponsor_id', '=', 'sponsor.id')
                    ->leftjoin('char_organizations as org', 'v.organization_id', '=', 'org.id')
                    ->leftjoin('char_currencies AS c', 'v.currency_id', '=', 'c.id')
            ->selectRaw( "left_person.*, vp.*, v.*,
                                char_kinship_i18n.name as kinship_name,
                                  char_persons.id_card_number AS main_id_card_number,
                                  CONCAT(ifnull(left_person.first_name, ' '), ' ' ,ifnull(left_person.second_name, ' '),' ',ifnull(left_person.third_name, ' '),' ', ifnull(left_person.last_name,' ')) AS fullname,
                                  CONCAT(ifnull(left_person.first_name, ' '), ' ' ,ifnull(left_person.second_name, ' '),' ',ifnull(left_person.third_name, ' '),' ', ifnull(left_person.last_name,' ')) AS nm,
                                  TIME_FORMAT(vp.receipt_time, '%r') as receipt_time_,
                                  (v.value) as value,
                                  round((v.value * v.exchange_rate),0) as sh_value,
                                  vc.name AS category_name,
                                  org.name AS organization_name,
                                  c.name AS currency_name");

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $str = ['v.title','v.content','v.header','v.footer','v.notes',
                    'char_persons.first_name', 'char_persons.second_name',
                    'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($filters['id_card_number'])){
            if(is_array($filters['id_card_number'])){
                if(sizeof($filters['id_card_number'])  > 0){
                    $query->whereIn('char_persons.id_card_number' ,$filters['id_card_number']);
                }
            }
        }

        if(isset($filters['titles'])){
            $titles = explode(',', $filters['titles']);
            if(is_array($titles)){
                if(sizeof($titles)  > 0){
                    $query->whereIn('v.title' ,$titles);
                }
            }
        }

        if(isset($filters['codes'])){
            $codes = explode(',', $filters['codes']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.un_serial' ,$codes);
                }
            }
        }

        if(isset($filters['vouchers_code'])){
            $codes = explode(',', $filters['vouchers_code']);
            if(is_array($codes)){
                if(sizeof($codes)  > 0){
                    $query->whereIn('v.serial' ,$codes);
                }
            }
        }

        if(isset($filters['target'])){

            if($user->type == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('v.organization_id',$organization_id);
                    $anq->orwherein('v.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $query->where(function ($anq) use ($organization_id) {
                    $anq->wherein('v.organization_id', function($quer) use($organization_id) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }
        }

        $min_value=null;
        $max_value=null;

        if(isset($filters['max_value']) && $filters['max_value'] !=null && $filters['max_value'] !=""){
            $max_value=$filters['max_value'];
        }

        if(isset($filters['min_value']) && $filters['min_value'] !=null && $filters['min_value'] !=""){
            $min_value=$filters['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $query = $query->whereRaw(" $max_value >= value");
            $query = $query->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $query = $query->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $query = $query->whereRaw(" $min_value <= value");
        }


        $date_to=null;
        $date_from=null;

        if(isset($filters['date_to']) && $filters['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($filters['date_to']));
        }
        if(isset($filters['date_from']) && $filters['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($filters['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query = $query->whereDate('v.created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query = $query->whereDate('v.created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($filters['voucher_date_to']) && $filters['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($filters['voucher_date_to']));
        }
        if(isset($filters['voucher_date_from']) && $filters['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($filters['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $query = $query->whereDate('v.voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $query = $query->whereDate('v.voucher_date', '<=', $voucher_date_to);
        }

        $receipt_date_to=null;
        $receipt_date_from=null;

        if(isset($filters['receipt_date_to']) && $filters['receipt_date_to'] !=null){
            $receipt_date_to=date('Y-m-d',strtotime($filters['receipt_date_to']));
        }
        if(isset($filters['receipt_date_from']) && $filters['receipt_date_from'] !=null){
            $receipt_date_from=date('Y-m-d',strtotime($filters['receipt_date_from']));
        }
        if($receipt_date_from != null && $receipt_date_to != null) {
            $query = $query->whereBetween( 'vp.receipt_date', [ $receipt_date_from, $receipt_date_to]);
        }
        elseif($receipt_date_from != null && $receipt_date_to == null) {
            $query = $query->whereDate('vp.receipt_date', '>=', $receipt_date_from);
        }
        elseif($receipt_date_from == null && $receipt_date_to != null) {
            $query = $query->whereDate('vp.receipt_date', '<=', $receipt_date_to);
        }

        if($user->super_admin){
            $query->selectRaw("CASE  WHEN sponsor.name is null THEN ' ' Else sponsor.name END AS sponsor_name");
        }else{
            $query->selectRaw("CASE  WHEN (sponsor.name is null or v.organization_id != '$organization_id') THEN ' ' Else 
                                     sponsor.name END AS sponsor_name");
        }

        $paginage = $query->get();
        $response['data'] = [];
        $response['total'] = [];
        $response['success'] = false;

        if(sizeof($paginage)){
            $response['data'] = $paginage;
            $amount = 0;
            $total    = $query->selectRaw("sum((v.value * v.exchange_rate)) as total")->first();
            if($total){
                $amount = $total->total;
            }

            $response['total'] =round($amount,0);
            $response['success'] = true;
        }else{
            $response['msg'] = trans('common::application.There is no vouchers to this card number');
        }
        return $response;
    }

    public static function groupDetails($cards)
    {

        return self::query()
            ->whereIn('char_persons.id_card_number', $cards)
            ->selectRaw( "char_persons.id_card_number,char_persons.old_id_card_number,
                          CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                 ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS fullname,
                          CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                 ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS nm")
            ->get();

    }


    public static function descendants($id)
    {
        return self::query()->where('father_id', '=', $id)
            ->orWhere('mother_id', '=', $id)
            ->get();
    }
    public function getDescendants()
    {
        return self::descendants($this->id);
    }
    public static function father($id)
    {
        return self::query()->join('char_persons AS d', 'char_persons.id', '=', 'd.father_id')
            ->where('d.id', '=', $id)
            ->first();
    }
    public function getFather()
    {
        return self::father($this->father_id);
    }
    public static function mother($id)
    {
        return self::query()->join('char_persons AS d', 'char_persons.id', '=', 'd.mother_id')
            ->where('d.id', '=', $id)
            ->first();
    }
    public function getMother()
    {
        return self::mother($this->mother_id);
    }
    public static function siblings($id)
    {
        return self::query()->join('char_persons AS d', 'char_persons.father_id', '=', 'd.father_id')
            ->where('d.id', '=', $id)
            ->get();
    }
    public function getSiblings()
    {
        return self::siblings($this->id);
    }
    public static function brothers($id)
    {
        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        return self::query()
            ->join('char_persons AS d', function($q) {
                $q->on( 'char_persons.father_id', '=', 'd.father_id');
                $q->on( 'char_persons.mother_id', '=', 'd.mother_id');
            })
            ->leftjoin('char_marital_status_i18n', function($join)  {
                $join->on('d.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                    ->where('char_marital_status_i18n.language_id', '=', 1);
            })
            ->leftjoin('char_persons_education','d.id', '=', 'char_persons_education.person_id')
            ->leftjoin('char_persons_health','d.id', '=', 'char_persons_health.person_id')
            ->where('d.id', '=', $id)
            ->selectRaw("char_persons.id,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                         CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number,
                         CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                         CASE WHEN  char_persons.birthday is null THEN '-' Else  DATE_FORMAT(char_persons.birthday,'%Y/%m/%d') END AS birthday,
                         CASE WHEN  char_persons.marital_status_id is null THEN '-' Else  char_marital_status_i18n.name END AS marital_status,
                         CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number,
                         CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                         CASE WHEN char_persons.gender is null THEN '-'
                              WHEN char_persons.gender = 1 THEN '$male'
                              WHEN char_persons.gender = 2 THEN '$female'
                         END AS gender,
                         CASE WHEN char_persons_health.condition is null THEN '-'
                               WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                  WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                  WHEN char_persons_health.condition = 3 THEN '$special_needs'
                         END AS health_status                               
                       ") ->get();
    }
    public function getBrothers()
    {
        return self::brothers($this->id);
    }
    public static function cases($id, $organization_id = null)
    {
        $query = self::query()->join('char_cases', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
            ->where('char_persons.id', '=', $id)
            ->select(array(
                'char_cases.*',
                'char_categories.name AS category_name'
            ));
        if (null !== $organization_id) {
            $query->where('char_cases.organization_id', '=', $organization_id);
        } else {
            $query->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
                ->addSelect('char_organizations.name AS organization_name');
        }

        return $query->get();
    }
    public function getCases()
    {
        return self::cases($this->id);
    }
    public function getCaseWithContact($id)
    {
        $query = self::query()
            ->join('char_persons_contact', 'char_persons.id', '=', 'char_persons_contact.person_id')
            ->where(['char_persons.id'=> $id,'char_persons_contact.contact_type'=>'primary_mobile'])
            ->select(array(
                'char_persons.*',
                'char_persons_contact.*'
            ));
        return $query->first();
    }
    public static function filterPersons($filters){

        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'prev_family_name', 'id_card_number',
                         'old_id_card_number'];
        $condition = [];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {

                    if($key =='id_card_number'){
//                        if(strlen($value) == 9){
                        $data = ['char_persons.' . $key => (int)$value];
//                        }
                    }else{
                        $data = ['char_persons.' . $key =>$value];
                    }
                    if(!empty($data)){
                        array_push($condition, $data);
                    }
                }
            }
        }


        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $query = \DB::table('char_persons')
                    ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) {
                        $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                        $q->where('char_marital_status.language_id',1);
                    })
                    ->leftjoin('char_persons_contact as cont2', function($q) {
                        $q->on('cont2.person_id', '=', 'char_persons.id');
                        $q->where('cont2.contact_type','primary_mobile');
                    })
                    ->selectRaw(" char_persons.id,char_persons.family_cnt,char_persons.male_live,
                                  char_persons.female_live,char_persons.spouses,
                                  CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                  CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number,
                                  CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                                  CASE WHEN  char_persons.birthday is null THEN '-' Else  DATE_FORMAT(char_persons.birthday,'%Y/%m/%d') END AS birthday,
                                  CASE WHEN  char_persons.marital_status_id is null THEN '-' Else  char_marital_status.name END AS marital_status,
                                  CASE WHEN  char_persons.old_id_card_number is null THEN '-' Else  char_persons.old_id_card_number END AS old_id_card_number,
                                  CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,  
                                  CASE
                                               WHEN char_persons.gender is null THEN '-'
                                               WHEN char_persons.gender = 1 THEN '$male'
                                               WHEN char_persons.gender = 2 THEN '$female'
                                         END
                                         AS gender
                                         
                                   ") ;

        $language_id = 1;
        if($filters['source'] =='guardians'){
            $query->join('char_guardians', function($q) {
                $q->on('char_guardians.guardian_id',  '=', 'char_persons.id');
//                    $q->where('char_guardians.status','=',1);
            });

            $query->leftjoin('char_locations_i18n As gmL1', function ($q) use ($language_id) {
                $q->on('gmL1.location_id', '=', 'char_persons.mosques_id')
                    ->where('gmL1.language_id', $language_id);
            })->leftjoin('char_locations_i18n As gL1', function ($q) use ($language_id) {
                $q->on('gL1.location_id', '=', 'char_persons.location_id')
                    ->where('gL1.language_id', $language_id);
            })
                ->leftjoin('char_locations_i18n As gL2', function ($q) use ($language_id) {
                    $q->on('gL2.location_id', '=', 'char_persons.city');
                    $q->where('gL2.language_id', $language_id);
                })
                ->leftjoin('char_locations_i18n As gL3', function ($q) use ($language_id) {
                    $q->on('gL3.location_id', '=', 'char_persons.governarate');
                    $q->where('gL3.language_id', $language_id);
                })
                ->leftjoin('char_locations_i18n As gL4', function ($q) use ($language_id) {
                    $q->on('gL4.location_id', '=', 'char_persons.country');
                    $q->where('gL4.language_id', $language_id);
                })
                ->selectRaw("CONCAT(ifnull(gL4.name, ' '), ' ' ,ifnull(gL3.name, ' '),' ',ifnull(gL2.name, ' '),' ',
                                    ifnull(gL1.name,' '),' ', ifnull(gmL1.name,' '),' ', ifnull(char_persons.street_address,' '))
                             AS address ");
        }
        else{

            $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id', $language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                    ->where('district_name.language_id', $language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                    ->where('region_name.language_id', $language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                    ->where('location_name.language_id', $language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                    ->where('square_name.language_id', $language_id);
            });
            $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id', $language_id);
            });

            $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),' _ ',
                                                            ifnull(region_name.name,' '),' _ ',
                                                            ifnull(location_name.name,' '),' _ ',
                                                            ifnull(square_name.name,' '),' _ ',
                                                            ifnull(mosques_name.name,' '),' _ ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address");

        }

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name',
                          'char_persons.prev_family_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }


        $query->groupBy('char_persons.id');
        if($filters['action'] == 'export'){
            return $query->get();
        }

        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->paginate()->total());
        return $query->paginate($records_per_page);

    }
    public static function filterPersonCases($id, $filters)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $char_cases = ['category_id','visitor','status','organization_id'];
        $condition=[];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    $data = ['char_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()->join('char_cases', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
            ->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
            ->where('char_persons.id', '=', $id);

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = ['char_cases.visitor'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        $visited_at_to=null;
        $visited_at_from=null;

        if(isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null){
            $visited_at_to=date('Y-m-d',strtotime($filters['visited_at_to']));
        }
        if(isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null){
            $visited_at_from=date('Y-m-d',strtotime($filters['visited_at_from']));
        }

        if($visited_at_from != null && $visited_at_to != null) {
            $query->whereBetween( 'char_cases.visited_at', [ $visited_at_from, $visited_at_to]);
        }elseif($visited_at_from != null && $visited_at_to == null) {
            $query->whereDate('char_cases.visited_at', '>=', $visited_at_from);
        }elseif($visited_at_from == null && $visited_at_to != null) {
            $query->whereDate('char_cases.visited_at', '<=', $visited_at_to);
        }

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $query->selectRaw("
                           CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name ,
                           CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name ,
                           CASE WHEN char_cases.status = 1 THEN '$inactive' WHEN char_cases.status = 0 THEN '$active'   END  AS status,
                           CASE WHEN char_cases.created_at is null THEN '-' Else char_cases.created_at  END  AS created_date,
                           CASE WHEN char_cases.visitor is null THEN '-' Else char_cases.visitor  END  AS visitor,
                           CASE WHEN char_cases.visited_at is null THEN '-' Else char_cases.visited_at  END  AS visited_at,
                           CASE WHEN char_cases.notes is null THEN '-' Else char_cases.notes  END  AS notes,
                           CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                           CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                           CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                           ");

        if($filters['action'] =='filters'){
            return $query->paginate(config('constants.records_per_page'));
        }else{
            return $query->get();
        }
    }
    public static function savePerson($input = array())
    {

        $language_id = 1;
        $en_language_id = 2;
        $person_Input = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','birthday', 'gender', 'marital_status_id','deserted','family_cnt',
            'birth_place', 'nationality', 'city', 'country','mosques_id','governarate', 'location_id', 'street_address', 'refugee',
            'unrwa_card_number', 'monthly_income', 'spouses', 'prev_family_name', 'father_id', 'mother_id', 'death_date', 'death_cause_id','spouses'
            ,"adscountry_id", "adsdistrict_id", "adsmosques_id","family_cnt","female_live","male_live","spouses",
            "card_type",'is_qualified','qualified_card_number',
            "adsneighborhood_id","adsregion_id","has_commercial_records","active_commercial_records","gov_commercial_records_details",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount'
        ];

        $IntegerInput =  ['habitable','house_condition','rooms','area','rent_value' ,'receivables',
            'card_type','is_qualified','qualified_card_number',
            'indoor_condition','residence_condition','roof_material_id','property_type_id',
            'kinship_id','stage','condition','health_condition','marital_status_id',
            'WhoIsGuardian','can_work','working','nationality','category_id' ,'gender','study','prayer','save_quran','quran_center',
            'birth_place', 'marital_status_id' , 'refugee','alive','spouses','health_disease_id','deserted','family_cnt'];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{

                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }

        $return=array();
        $person=null;
        $rank_updated=false;

        if (!isset($insert['card_type'])){
            $insert['card_type'] = '1';
            if (isset($insert['id_card_number'])){
                if (substr($insert['id_card_number'], 0, 1) === '7') {
                    $insert['card_type'] = '2';
                }
            }
        }

//        return ['d_' => $insert];
        $founded = false;
        if(isset($input['person_id'])) {
            if(sizeof($insert) != 0){
                $person = self::findorfail($input['person_id']);
                $return['prev_adscountry_id']= $person->adscountry_id;
                $return['prev_adsregion_id']= $person->adsregion_id;
                $return['prev_adsdistrict_id']= $person->adsdistrict_id;
                $return['prev_adsneighborhood_id']= $person->adsneighborhood_id;
                $return['prev_adssquare_id']= $person->adssquare_id;
                $return['prev_adsmosques_id']= $person->adsmosques_id;
                foreach ($insert as $key => $value) {
                    $person->$key = $value;
                }
//                return $person->save();
                self::where(['id' =>$input['person_id']])->update($insert);
            }
            $person =self::findorfail($input['person_id']);
            $founded = true;
        }
        else{
            if(sizeof($insert) > 0){
                $exist = self::query()->where(['id_card_number' => (int) $insert['id_card_number']])->first();

                if(is_null($exist)){
                    $person = new Person();
                    foreach ($insert as $key => $value) {
                        $person->$key = $value;
                    }
                    $person->save();
                }
                else{
                    $return['prev_adscountry_id']= $exist->adscountry_id;
                    $return['prev_adsregion_id']= $exist->adsregion_id;
                    $return['prev_adsdistrict_id']= $exist->adsdistrict_id;
                    $return['prev_adsneighborhood_id']= $exist->adsneighborhood_id;
                    $return['prev_adssquare_id']= $exist->adssquare_id;
                    $return['prev_adsmosques_id']= $exist->adsmosques_id;
                    $founded = true;
                    self::where(['id' =>$exist->id])->update($insert);
                    $person = self::findorfail($exist->id);
                }


            }
        }
        \DB::beginTransaction();

            if ($person) {
            $return['id']= $person->id;
            $return['gender']= $person->gender;
            $return['death_date']= $person->death_date;
            $return['id_card_number']= $person->id_card_number;
            $return['card_type']= $person->card_type;
            $return['first_name']= (is_null($person->first_name) || $person->first_name == ' ' ) ? ' ' : $person->first_name;
            $return['second_name']= (is_null($person->second_name) || $person->second_name == ' ' ) ? ' ' : $person->second_name;
            $return['third_name']= (is_null($person->third_name) || $person->third_name == ' ' ) ? ' ' : $person->third_name;
            $return['last_name']= (is_null($person->last_name) || $person->last_name == ' ' ) ? ' ' : $person->last_name;
            $return['full_name']=  $return['first_name']. ' ' . $return['second_name'] .' ' .$return['third_name']. ' ' . $return['last_name'];
            $return['adscountry_id']= $person->adscountry_id;
            $return['adsregion_id']= $person->adsregion_id;
            $return['adsdistrict_id']= $person->adsdistrict_id;
            $return['adsneighborhood_id']= $person->adsneighborhood_id;
            $return['adssquare_id']= $person->adssquare_id;
            $return['adsmosques_id']= $person->adsmosques_id;

            $person_id = $person->id;
            $father_id=null;
            $mother_id=null;

            if($person->father_id != null){

                $father_kinship=\Setting\Model\Setting::where('id','father_kinship')->first();
                if($father_kinship){
                    if(!is_null($father_kinship->value) and $father_kinship->value != ""){
                        if(\Common\Model\PersonModels\PersonKinship::where(["r_person_id"=> $person->father_id,"l_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["r_person_id"=>(int) $person->father_id,"l_person_id"=>(int)$person_id])
                                ->update(['kinship_id'=>(int)$father_kinship->value]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["r_person_id"=>(int)$person->father_id,"l_person_id"=>(int)$person_id,'kinship_id'=>(int)$father_kinship->value]);
                        }
                    }
                }

                if($person->gender== 2){
                    $son_kinship=\Setting\Model\Setting::where('id','daughter_kinship')->first();
                }else{
                    $son_kinship=\Setting\Model\Setting::where('id','son_kinship')->first();
                }

                if($son_kinship){
                    if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person->father_id,"r_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person->father_id,"r_person_id"=>$person_id])
                                ->update(['kinship_id'=>$son_kinship->value]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $person->father_id,"r_person_id"=>$person_id,'kinship_id'=>$son_kinship->value]);
                        }
                    }
                }
            }
            else{

                if($person->gender == 2){
                    $daughter_kinship=\Setting\Model\Setting::where('id','daughter_kinship')->first();
                    if($daughter_kinship) {
                        if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
                            $father_id=\Common\Model\PersonModels\PersonKinship::fetch('father','r_person_id',$person->id,$daughter_kinship->value); //female
                        }
                    }
                }else{
                    $son_kinship=\Setting\Model\Setting::where('id','son_kinship')->first();
                    if($son_kinship) {
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $father_id=\Common\Model\PersonModels\PersonKinship::fetch('father','r_person_id',$person->id,$son_kinship->value); //female
                        }}
                }
                if($father_id != null){
                    \Common\Model\Person::where('id',$person_id)->update(['father_id' => $father_id]);
                }
            }

            if($person->mother_id != null){
                $mother_kinship=\Setting\Model\Setting::where('id','mother_kinship')->first();
                if($mother_kinship){
                    if(!is_null($mother_kinship->value) and $mother_kinship->value != "") {
                        if(\Common\Model\PersonModels\PersonKinship::where(["r_person_id"=> $person->mother_id,"l_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["r_person_id"=> $person->mother_id,"l_person_id"=>$person_id])
                                ->update(['kinship_id'=>$mother_kinship->value]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["r_person_id"=> $person->mother_id,"l_person_id"=>$person_id,'kinship_id'=>$mother_kinship->value]);
                        }
                    }

                }
                if($person->gender== 2){
                    $son_kinship=\Setting\Model\Setting::where('id','daughter_kinship')->first();
                }else{
                    $son_kinship=\Setting\Model\Setting::where('id','son_kinship')->first();
                }
                if($son_kinship){
                    if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person->mother_id,"r_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person->mother_id,"r_person_id"=>$person_id])
                                ->update(['kinship_id'=>$son_kinship->value]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $person->mother_id,"r_person_id"=>$person_id,'kinship_id'=>$son_kinship->value]);
                        }
                    }

                }
            }
            else{

                if($person->gender == 2){
                    $daughter_kinship=\Setting\Model\Setting::where('id','daughter_kinship')->first();
                    if($daughter_kinship) {
                        if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
                            $mother_id = \Common\Model\PersonModels\PersonKinship::fetch('mother', 'r_person_id', $person->id, $daughter_kinship->value); //female
                        }
                    }
                }else{
                    $son_kinship=\Setting\Model\Setting::where('id','son_kinship')->first();
                    if($son_kinship) {
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $mother_id=\Common\Model\PersonModels\PersonKinship::fetch('mother','r_person_id',$person->id,$son_kinship->value); //female
                        }
                    }
                }
                if($mother_id!= null){
                    \Common\Model\Person::where('id',$person_id)->update(['mother_id' => $mother_id]);
                }

            }

            if (isset($input['husband_id'])) {
                    if (($input['husband_id'] != null && $input['husband_id'] != "")) {

                        $husband_kinship=\Setting\Model\Setting::where('id','husband_kinship')->first();

                        if($husband_kinship){
                            if(!is_null($husband_kinship->value) and $husband_kinship->value != "") {
                                $husband_kinship_id=$husband_kinship->value;
                                if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['husband_id'],"r_person_id"=>$person_id])->first()){
                                    \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['husband_id'],"r_person_id"=>$person_id])
                                        ->update(['kinship_id'=>$husband_kinship_id]);
                                }else{
                                        \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $input['husband_id'],
                                        "r_person_id"=>$person_id,
                                        'kinship_id'=>$husband_kinship_id]);
                                }
                            }
                        }

                        $wife_kinship=\Setting\Model\Setting::where('id','wife_kinship')->first();
                        if($wife_kinship){
                            if(!is_null($wife_kinship->value) and $wife_kinship->value != "") {
                                $wife_kinship_id=$wife_kinship->value;

                                if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person_id,"r_person_id"=>$input['husband_id']])->first()){
                                        \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person_id,"r_person_id"=>$input['husband_id']])
                                        ->update(['kinship_id'=>$wife_kinship_id]);
                                }else{
                                        \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $person_id,
                                        "r_person_id"=>$input['husband_id'],
                                        'kinship_id'=>$wife_kinship_id]);
                                }
                            }
                        }
                    }
                }

            if(isset($input['category_id']) || isset($input['case_id'])) {

                $case_attributes=[];

                if(isset($input['category_id'])) {
                    if($input['category_id'] != null){
                        $case_attributes['category_id']=$input['category_id'];
                    }
                }
                if(isset($input['target'])){
                    if($input['target']=='case'){
                        $caseInput=['organization_id','notes','visited_at','visitor',
                                   'user_id','rank','visitor_card', 'visitor_opinion', 'visitor_evaluation'];
                        foreach ($caseInput as $var) {
                            if(isset($input[$var])){
                                if(($input[$var] != null && $input[$var] != 'null' && $input[$var] != "")) {

                                    if ($var == 'visited_at') {
                                        $case_attributes[$var] = date('Y-m-d', strtotime($input[$var]));
                                    }else{
                                        $case_attributes[$var] = $input[$var];
                                    }

                                }else{
                                    $case_attributes[$var] = null;
                                }
                            }
                        }

                        if(sizeof($case_attributes) !=0){
                            $case=\Common\Model\CaseModel::where(['person_id' => $person_id,
                                'category_id'=>$input['category_id'],'organization_id'=>$input['organization_id'] ])->first();

                            $return['old_case']= false;

                            if(!is_null($case)){
                                $return['old_case']= true;
                                $return['case_id']= $case->id;
                                \Common\Model\CaseModel::where(['id' => $return['case_id']])->update($case_attributes);
                            }else{
                                $case_attributes['person_id']=$person_id;
                                $case_attributes['created_at']=date('Y-m-d H:i:s');
                                $case_attributes['status']=0;

                                if(!isset($input['category_type'])){
                                    $input['category_type'] = 1;
                                }


                                if($input['category_type'] == 2){

                                $save_ = true ;

                                $active= \DB::select("select char_get_active_case_count ( ? ) AS active_case_count", [$person_id]);
                                if($active){
                                    if($active[0]->active_case_count > 0 ){
                                        $save_ = false ;

                                    }
                                }

                                if($save_ == true){
                                    $case=  \Common\Model\AidsCases::create($case_attributes);
                                    $return['case_id']= $case->id;
                                }else{
                                    $return['reason']= 'active_case_count';
                                    $case = null;
                                }

                                }else{
                                    $case=  \Common\Model\SponsorshipsCases::create($case_attributes);
                                    $return['case_id']= $case->id;
                                }

                                if(!is_null($case)){
                                    \Common\Model\CasesStatusLog::create([
                                        'case_id'=>$case->id, 'user_id'=>$input['user_id'],
                                        'reason'=>trans('common::application.case was created'),
                                        'status'=>0, 'date'=>date("Y-m-d")]);
                                }
                            }

                            $rank_updated=true;
                            if(isset($input['needs'])){
                                if($input['needs'] != null && $input['needs'] !="") {
                                    if(!is_null($case)){
                                        \Common\Model\CaseNeeds::saveNeeds($case->id,['needs' => $input['needs']]);
                                    }
                                }
                            }

                            if(isset($input['promised']) && isset($input['organization_name'])){
                                if($input['promised'] != null && $input['promised'] !="" && $input['organization_name'] != null && $input['organization_name'] !="") {
                                    if(!is_null($case)){
                                        \Common\Model\Reconstructions::saveReconstruction($case->id,['promised' => $input['promised'] , 'organization_name' => $input['organization_name']]);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(isset($input['en_first_name']) && isset($input['en_second_name']) && isset($input['en_third_name']) && isset($input['en_last_name'])) {
//                PersonI18n::updateOrCreate(['person_id' => $person_id,'language_id' => $en_language_id],
//                                                                       ['first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
//                                                                        'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
//                                                                     ]);

                if(isset($input['update_en'])){

                    $exist_ = PersonI18n::where(['person_id' => $person_id,'language_id' => $en_language_id])->first();
                    if(is_null($exist_)){

                        PersonI18n::create(['person_id' => $person_id,'language_id' => $en_language_id,'first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                            'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                        ]);
                    }
                }else{
                    if(PersonI18n::where(['person_id' => $person_id,'language_id' => $en_language_id])->first()){
                        PersonI18n::where(['person_id' => $person_id,'language_id' => $en_language_id])
                            ->update(['first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                                'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                            ]);
                    }else{
                        PersonI18n::create(['person_id' => $person_id,'language_id' => $en_language_id,'first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                            'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                        ]);
                    }
                }


            }

            if(isset($input['phone'])){
                if($input['phone'] != null && $input['phone'] !=""){
//                    \Common\Model\PersonModels\PersonContact::updateOrCreate(['person_id' => $person_id, 'contact_type' => 'phone'],
//                                                                                 ['contact_value' => $input['phone']]);

                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])
                            ->update(['contact_value' => $input['phone']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'phone','contact_value' => $input['phone']]);
                    }
                }
            }
            if(isset($input['secondary_mobile'])){
                if($input['secondary_mobile'] != null && $input['secondary_mobile'] !=""){
//                       \Common\Model\PersonModels\PersonContact::updateOrCreate(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'],
//                                                                                    ['contact_value' => $input['secondary_mobile']]);
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])
                            ->update(['contact_value' => $input['secondary_mobile']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'secondary_mobile','contact_value' => $input['secondary_mobile']]);
                    }
                }
            }
            if(isset($input['primary_mobile'])){
                if($input['primary_mobile'] != null && $input['primary_mobile'] !=""){
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])
                            ->update(['contact_value' => $input['primary_mobile']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'primary_mobile','contact_value' => $input['primary_mobile']]);
                    }

                }
            }
            if(isset($input['wataniya'])){
                if($input['wataniya'] != null && $input['wataniya'] !=""){
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])
                            ->update(['contact_value' => $input['wataniya']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'wataniya','contact_value' => $input['wataniya']]);
                    }

                }
            }

            if(isset($input['working']) || isset($input['can_work']) ||isset($input['work_job_id']) || isset($input['work_status_id']) ||
                isset($input['work_wage_id']) || isset($input['work_location']) || isset($input['work_reason_id'])|| isset($input['has_other_work_resources'])) {

                $worksInput=['working','can_work','work_job_id','work_status_id','work_wage_id','work_reason_id','work_location'];
                $CreateWork = [];
                if (isset($input['working']) || isset($input['can_work'])) {
                    foreach ($input as $key => $value) {
                        if(in_array($key, $worksInput) &&  !(!$value || $value == "")) {
                            if($key == 'work_location'){
                                $CreateWork[$key] = $value;
                            }else{
                                $CreateWork[$key] = (int)$value;
                            }
                        }
                    }



                    $CreateWork['has_other_work_resources'] = 0;
                    if (isset($input['has_other_work_resources'])) {
                        if (($input['has_other_work_resources'] != null && $input['has_other_work_resources'] != "")) {
                            $CreateWork['has_other_work_resources'] = $input['has_other_work_resources'];
                        }
                    }

                    if (isset($CreateWork['working'])){
                        if($CreateWork['working'] !=1){
                            $CreateWork['work_job_id']=$CreateWork['work_status_id']=$CreateWork['work_wage_id']=$CreateWork['work_location']=null;
                        }
                    }
                    if ( isset($CreateWork['can_work'])){
                        if($CreateWork['can_work'] !=1){
                            $CreateWork['work_reason_id']=null;
                        }
                    }

                    if (sizeof($CreateWork) != 0) {
                        \Common\Model\PersonModels\PersonWork::updateOrCreate(['person_id' => $person_id],$CreateWork);
                    }
                }

            }

            if(isset($input['study']) ||isset($input['currently_study']) || isset($input['degree']) ||isset($input['specialization']) || isset($input['stage']) ||
                isset($input['type']) || isset($input['authority']) || isset($input['grade']) || isset($input['year']) ||
                isset($input['school']) || isset($input['level']) || isset($input['points']) ) {

                $CreateEducation = [];
                $EduInput=['degree','specialization','stage','type','authority','grade','year','school','level','points'];
                $EduInputIn=['degree','specialization','stage','type','authority','grade','year','level'];

                if (isset($input['currently_study'])&&  ($input['currently_study']  != null && $input['currently_study'] != "")) {
                    $CreateEducation['currently_study'] = $input['currently_study'];
                }

                if (isset($input['study'])&&  ($input['study']  != null && $input['study'] != "")) {
                    $CreateEducation['study'] = (int) $input['study'];
                    foreach ($EduInput as $var) {
                        if(isset($input[$var])){
                            if($CreateEducation['study'] == 1 && ($input[$var] != null && $input[$var] != "")) {
                                if(!in_array($var,$EduInputIn)){
                                    $CreateEducation[$var] = $input[$var];
                                }else{
                                    $CreateEducation[$var] = (int)$input[$var];
                                }

                            }else{
                                $CreateEducation[$var] = null;
                            }
                        }
                    }
                }

                if (sizeof($CreateEducation) != 0) {
                    PersonEducation::updateOrCreate(['person_id' => $person_id],$CreateEducation);
                }

            }

            if (isset($input['property_type_id']) || isset($input['roof_material_id']) || isset($input['residence_condition']) ||
                isset($input['indoor_condition']) || isset($input['habitable']) || isset($input['house_condition']) || isset($input['rooms']) ||
                isset($input['area']) || isset($input['rent_value']) || isset($input['need_repair']) || isset($input['repair_notes'])) {

                $residenceInput=['property_type_id','roof_material_id','residence_condition','need_repair',
                    'repair_notes','indoor_condition','habitable','house_condition','rooms','area','rent_value'];
                $residenceInputInt=['property_type_id','roof_material_id','residence_condition','indoor_condition',
                    'habitable','house_condition','rooms','rent_value','need_repair'];
                $CreateResidence = [];

                foreach ($residenceInput as $var) {
                    if(isset($input[$var])){
                        if($input[$var] != null && $input[$var] != "") {
                            if(!in_array($var,$residenceInputInt)){
                                $CreateResidence[$var] = $input[$var];
                            }else{
                                $CreateResidence[$var] = (int)$input[$var];
                            }
                        }else{
                            $CreateResidence[$var] = null;
                        }
                    }
                }
                if (sizeof($CreateResidence) != 0) {
//                        \Common\Model\PersonModels\PersonResidence::updateOrCreate(['person_id' => $person_id],$CreateResidence);
                    \Common\Model\PersonModels\PersonResidence::saveResidence($person_id,$CreateResidence);
//                            if(\Common\Model\PersonModels\PersonResidence::where(['person_id' => $person_id])->first()){
//                                \Common\Model\PersonModels\PersonResidence::where(['person_id' => $person_id])->update($CreateResidence);
//                            }else{
//                                $CreateResidence['person_id']=$person_id;
//                                \Common\Model\PersonModels\PersonResidence::create($CreateResidence);
//                            }
                }
            }

            if (isset($input['condition']) || isset($input['details']) || isset($input['disease_id'])  ||
                isset($input['has_health_insurance']) || isset($input['health_insurance_number']) ||
                isset($input['health_insurance_type']) || isset($input['has_device'])|| isset($input['used_device_name'])) {

                $CreateHealth = [];

                if (isset($input['has_device'])) {
                    if (($input['has_device'] != null && $input['has_device'] != "")) {
                        $CreateHealth['has_device'] = $input['has_device'];

                        if ($CreateHealth['has_device'] == 0) {
                            $CreateHealth['used_device_name'] =  null;
                        } else {

                            if (isset($input['used_device_name'])) {
                                if (($input['used_device_name'] != null && $input['used_device_name'] != "")) {
                                    $CreateHealth['used_device_name'] = $input['used_device_name'];
                                }
                            }
                        }
                    }
                }

                if (isset($input['has_health_insurance'])) {
                    if (($input['has_health_insurance'] != null && $input['has_health_insurance'] != "")) {
                        $CreateHealth['has_health_insurance'] = $input['has_health_insurance'];

                        if ($CreateHealth['has_health_insurance'] == 0) {
                            $CreateHealth['health_insurance_number'] =  null;
                            $CreateHealth['health_insurance_type'] = null;

                        } else {

                            if (isset($input['health_insurance_number'])) {
                                if (($input['health_insurance_number'] != null && $input['health_insurance_number'] != "")) {
                                    $CreateHealth['health_insurance_number'] = $input['health_insurance_number'];
                                }
                            }
                            if (isset($input['health_insurance_type'])) {
                                if (($input['health_insurance_type'] != null && $input['health_insurance_type'] != "")) {
                                    $CreateHealth['health_insurance_type'] = $input['health_insurance_type'];
                                }
                            }

                        }
                    }
                }

                if (isset($input['condition'])) {
                    if (($input['condition'] != null && $input['condition'] != "")) {
                        $CreateHealth['condition'] = $input['condition'];

                        if ($CreateHealth['condition'] == 1) {
                            $CreateHealth['disease_id'] =  null;
                            $CreateHealth['details'] = null;
                        } else {

                            if (isset($input['details'])) {
                                if (($input['details'] != null && $input['details'] != "")) {
                                    $CreateHealth['details'] = $input['details'];
                                }
                            }
                            if (isset($input['disease_id'])) {
                                if (($input['disease_id'] != null && $input['disease_id'] != "")) {
                                    $CreateHealth['disease_id'] = $input['disease_id'];
                                }
                            }
                        }

                    }
                }

                if (sizeof($CreateHealth) != 0) {
                    \Common\Model\PersonModels\PersonHealth::updateOrCreate(['person_id' => $person_id],$CreateHealth);
//                            if(\Common\Model\PersonModels\PersonHealth::where(['person_id' => $person_id])->first()){
//                                return     \Common\Model\PersonModels\PersonHealth::where(['person_id' => $person_id])->update($CreateHealth);
//                            }else{
//                                $CreateHealth['person_id']=$person_id;
//                                return    \Common\Model\PersonModels\PersonHealth::create($CreateHealth);
//                            }
                }

            }
            if (isset($input['kinship_id'])) {
                if (isset($input['guardian_id'])   && isset($input['guardian_update']) ) {
                    if (($input['guardian_id'] != null && $input['guardian_id'] != "")) {
                        \Common\Model\PersonModels\Guardian::where(['individual_id'=>$person_id,'organization_id'=>$input['organization_id']])->update(['status'=>0]);

                        if(\Common\Model\PersonModels\Guardian::where(["guardian_id"=> $input['guardian_id'],"individual_id"=>$person_id ,'organization_id' => $input['organization_id']])->first()){
                            \Common\Model\PersonModels\Guardian::where(["guardian_id"=> $input['guardian_id'],"individual_id"=>$person_id, 'organization_id'=>$input['organization_id']])->update(['status'=>1,'kinship_id'=>$input['kinship_id']]);

                        }else{
                             \Common\Model\PersonModels\Guardian::create(['status'=>1,"guardian_id"=> $input['guardian_id'], "individual_id"=>$person_id,
                                'kinship_id'=>$input['kinship_id'], 'organization_id'=>$input['organization_id'], 'created_at'=>date('Y-m-d H:i:s')]);
                        }

                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['guardian_id'],"r_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=>$input['guardian_id'],"r_person_id"=>$person_id])->update(['kinship_id'=>$input['kinship_id']]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $input['guardian_id'],"r_person_id"=>$person_id,'kinship_id'=>$input['kinship_id']]);
                        }
                    }
                }
                if (isset($input['individual_id']) && isset($input['individual_update'])) {
                    if (($input['individual_id'] != null && $input['individual_id'] != "") ) {
                        \Common\Model\PersonModels\Guardian::where(['individual_id'=> $input['individual_id'],'organization_id'=>$input['organization_id']])->update(['status'=>0]);
                        if(\Common\Model\PersonModels\Guardian::where(["individual_id"=> $input['individual_id'],"guardian_id"=>$person_id ,'organization_id' => $input['organization_id']])->first()){
                            \Common\Model\PersonModels\Guardian::where(["individual_id"=> $input['individual_id'],"guardian_id"=>$person_id, 'organization_id'=>$input['organization_id']])->update(['status'=>1,'kinship_id'=>$input['kinship_id']]);
                        }else{
                            \Common\Model\PersonModels\Guardian::create(["individual_id"=> $input['individual_id'],"guardian_id"=>$person_id,
                                'kinship_id'=>$input['kinship_id'], 'organization_id'=>$input['organization_id'], 'created_at'=>date('Y-m-d H:i:s')]);
                        }


                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['individual_id'],"r_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=>$input['individual_id'],"r_person_id"=>$person_id])->update(['kinship_id'=>$input['kinship_id']]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $input['individual_id'],"r_person_id"=>$person_id,'kinship_id'=>$input['kinship_id']]);
                        }


                    }
                }
                if (isset($input['l_person_id'])  && isset($input['l_person_update'])) {
                    if (($input['l_person_id'] != null && $input['l_person_id'] != "")) {

                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id])->update(['kinship_id'=>$input['kinship_id']]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id,'kinship_id'=>$input['kinship_id']]);
                        }
//                        \Common\Model\PersonModels\PersonKinship::updateOrCreate(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id],
//                              ['kinship_id'=>$input['kinship_id']]);
                    }
                }
                if (isset($input['r_person_id'])  && isset($input['r_person_update'])) {
                    if (($input['r_person_id'] != null && $input['r_person_id'] != "")) {
//                        \Common\Model\PersonModels\PersonKinship::updateOrCreate(["r_person_id"=> $input['r_person_id'],"l_person_id"=>$person_id],['kinship_id'=>$input['kinship_id']]);
                        if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person_id,"r_person_id"=>$input['r_person_id']])->first()){
                            \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $person_id,"r_person_id"=>$input['r_person_id']])->update(['kinship_id'=>$input['kinship_id']]);
                        }else{
                            \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $person_id,"r_person_id"=>$input['r_person_id'],'kinship_id'=>$input['kinship_id']]);
                        }
                    }
                }
            }

            if (isset($input['save_quran']) || isset($input['prayer']) || isset($input['prayer_reason']) || isset($input['quran_parts']) ||
                isset($input['quran_chapters']) || isset($input['quran_center'])|| isset($input['quran_reason'])) {
                $CreateIslamic = [];

                if (isset($input['save_quran'])) {
                    if ($input['save_quran'] != null && $input['save_quran'] != "") {
                        if($input['save_quran'] ==0 ){

                            if(isset($input['quran_parts'])){
                                if($input['quran_parts'] != null && $input['quran_parts'] != "") {
                                    $CreateIslamic['quran_parts'] = $input['quran_parts'];
                                }
                            }
                            if(isset($input['quran_chapters'])){
                                if($input['quran_chapters'] != null && $input['quran_chapters'] != "") {
                                    $CreateIslamic['quran_chapters'] = $input['quran_chapters'];
                                }
                            }
                        }else{
                            $CreateIslamic['quran_parts'] = null;
                            $CreateIslamic['quran_chapters'] = null;

                        }
                    }
                }

                if (isset($input['prayer'])) {
                    if ($input['prayer'] != null && $input['prayer'] != "") {
                        $CreateIslamic['prayer'] = $input['prayer'];
                        if($input['prayer'] ==1 ) {
                            if(isset($input['prayer_reason'])){
                                if($input['prayer_reason'] != null && $input['prayer_reason'] != "") {
                                    $CreateIslamic['prayer_reason'] = $input['prayer_reason'];
                                }
                            }
                        }else{
                            $CreateIslamic['prayer_reason'] = null;

                        }
                    }
                }

                if (isset($input['quran_center'])) {
                    if ($input['quran_center'] != null && $input['quran_center'] != "") {
                        $CreateIslamic['quran_center'] = $input['quran_center'];

                        if($input['quran_center'] ==0 ){
                            if(isset($input['quran_reason'])){
                                if($input['quran_reason'] != null && $input['quran_reason'] != "") {
                                    $CreateIslamic['quran_reason'] = $input['quran_reason'];
                                }
                            }
                        }else{
                            $CreateIslamic['quran_reason'] = null;
                        }
                    }
                }

                if (sizeof($CreateIslamic) != 0) {

                  \Common\Model\PersonModels\PersonIslamic::updateOrCreate(['person_id' => $person_id],$CreateIslamic);
//                    if(\Common\Model\PersonModels\PersonIslamic::where(['person_id' => $person_id])->first()){
//                        return    \Common\Model\PersonModels\PersonIslamic::where(['person_id' => $person_id])->update($CreateIslamic);
//                    }else{
//                        $CreateIslamic['person_id']=$person_id;
//                        return    \Common\Model\PersonModels\PersonIslamic::create($CreateIslamic);
//                    }

                }

            }

            if (isset($input['banks'])) {
                $where = ['person_id' => $person_id, 'organization_id' => $input['organization_id']];

                \Common\Model\PersonModels\OrganizationPersonBank::where($where)->delete();
                \Common\Model\PersonModels\PersonBank::where(['person_id' => $person_id])->delete();
                if (sizeof($input['banks']) != 0) {
                    $where = ['person_id' => $person_id, 'organization_id' => $input['organization_id']];

                    \Common\Model\PersonModels\OrganizationPersonBank::where($where)->delete();
                    \Common\Model\PersonModels\PersonBank::where(['person_id' => $person_id])->delete();
                    if (sizeof($input['banks']) != 0) {
                        foreach ($input['banks'] as $key=>$value) {

                            if(isset($input['outSource'])){

                                $foundedAcount= \Common\Model\PersonModels\PersonBank::
                                 where(['bank_id'   => $value['bank_id'], 'account_number' => $value['account_number']])->first();

                                if(!$foundedAcount){
                                    $person_bank_account = \Common\Model\PersonModels\PersonBank::create(['person_id' => $person_id,
                                        'bank_id'   => $value['bank_id'],
                                        'branch_name' => $value['branch_name'],
                                        'account_number' => $value['account_number'],
                                        'account_owner'  => $value['account_owner']]);

                                    if ($value['check'] == true) {
                                        \Common\Model\PersonModels\OrganizationPersonBank::create(['person_id' => $person_id,
                                            'organization_id' => $input['organization_id'] ,
                                            'bank_id' => $person_bank_account->id]);
                                    }
                                }


                            }else{
                                $person_bank_account = \Common\Model\PersonModels\PersonBank::create(['person_id' => $person_id,
                                    'bank_id'   => $value['bank_id'],
                                    'branch_name' => $value['branch_name'],
                                    'account_number' => $value['account_number'],
                                    'account_owner'  => $value['account_owner']]);

                                if ($value['check'] == true) {
                                    \Common\Model\PersonModels\OrganizationPersonBank::create(['person_id' => $person_id,
                                        'organization_id' => $input['organization_id'] ,
                                        'bank_id' => $person_bank_account->id]);
                                }
                            }
                        }
                    }
                }
            }

            if (isset($input['save'])) {
                if($input['save'] != null &&$input['save'] != '' ){

                    if($input['child_gender']==1){
                        $son_kinship=\Setting\Model\Setting::where('id','son_kinship')->first();
                    }else{
                        $son_kinship=\Setting\Model\Setting::where('id','daughter_kinship')->first();
                    }

                    if($son_kinship){
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $child_kinship = $son_kinship->value;
                            if ($input['save'] == 'father') {
                                $father_kinship = \Setting\Model\Setting::where('id', 'father_kinship')->first();
                                if ($father_kinship) {
                                    if (\Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])->first()) {
                                        \Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])
                                            ->update(['kinship_id' => $father_kinship->value]);
                                    } else {
                                        \Common\Model\PersonModels\PersonKinship::create(["r_person_id" => $person_id, "l_person_id" => $input['child_id'], 'kinship_id' => $father_kinship->value]);
                                    }
                                }
                                if (\Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])->first()) {
                                    \Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])
                                        ->update(['kinship_id' => $child_kinship]);
                                } else {
                                    \Common\Model\PersonModels\PersonKinship::create(["r_person_id" => $person_id, "l_person_id" => $input['child_id'], 'kinship_id' => $child_kinship]);
                                }

                                \Common\Model\Person::where('id', $input['child_id'])->update(['father_id' => $person_id]);

                            }
                            if ($input['save'] == 'mother') {

                                $mother_kinship = \Setting\Model\Setting::where('id', 'mother_kinship')->first();
                                if ($mother_kinship) {
                                    if (\Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])->first()) {
                                        \Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])
                                            ->update(['kinship_id' => $mother_kinship->value]);
                                    } else {
                                        \Common\Model\PersonModels\PersonKinship::create(["r_person_id" => $person_id,
                                            "l_person_id" => $input['child_id'],
                                            'kinship_id' => $mother_kinship->value]);

                                    }
                                }
                                if (\Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])->first()) {
                                    \Common\Model\PersonModels\PersonKinship::where(["r_person_id" => $person_id, "l_person_id" => $input['child_id']])
                                        ->update(['kinship_id' => $child_kinship]);
                                } else {
                                    \Common\Model\PersonModels\PersonKinship::create(["r_person_id" => $person_id, "l_person_id" => $input['child_id'], 'kinship_id' => $child_kinship]);
                                }
                                \Common\Model\Person::where('id', $input['child_id'])->update(['mother_id' => $person_id]);

                            }
                        }
                    }
                }
            }

            if($rank_updated == true){
                if(isset($return['case_id'])){
                    \Common\Model\CaseModel::where(['id' => $return['case_id'] ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($return['case_id'])]);
                }
            }
            }
        \DB::commit();
        return $return;

    }
    public static function isUnique($id_card_number)
    {
        $person = self::query()->where('id_card_number', '=', $id_card_number)->first();
        return (is_null($person))? ['status' => false] : array('status' => true , 'row' => $person);
    }
    public static function getId($id_card_number)
    {
        $person = self::query()->where('id_card_number', '=', $id_card_number)->first();
        return (is_null($person))? null : $person->id;
    }
    public static function getFullName($id){

        $person = \DB::table('char_persons')
            ->where('id', '=', $id)
            ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', 
                                ifnull(char_persons.last_name,' ')) AS name") ->first();

        return (is_null($person))? '--' :$person->name;

    }
    public static function reMapInputs($inputs){

        $outputs=array('mother'=>[],'father'=>[],'guardian'=>[],'person'=>[]);

        foreach ($inputs as $k=>$v) {
            if(!is_null($inputs[$k]) && $inputs[$k] != " "){
                if($k == 'hl_alam_maayl'){
                    $outputs['mother'][$k] = $v;
                }
                elseif($k == 'sl_alkrab'){
                    $outputs['guardian'][$k] = $v;
                }
                elseif (substr($k, -5) === "_alam") {
                    $outputs['mother'][$k] = $v;
                } elseif (substr($k, -5) === "_alab") {
                    $outputs['father'][$k] = $v;
                } elseif (substr($k, -9) === "_rb_alasr") {
                    $outputs['father'][$k] = $v;
                } elseif (substr($k, -8) === "_almaayl") {
                    $outputs['guardian'][$k]= $v;
                }else{
                    $outputs['person'][$k] = $v;
                }
            }
        }
        return $outputs;
    }
    public static function filterInputs($data){

        $dates = ['birthday','visited_at','death_date'];
        $constant =  ['birth_place', 'nationality', 'marital_status_id','death_cause_id',
            'country', 'governarate', 'city','mosques_id','location_id','kinship_id',
            'bank_id', 'branch_name','disease_id','authority', 'stage','degree',
            'work_reason_id', 'work_status_id', 'work_wage_id', 'work_job_id',
            'property_type_id', 'roof_material_id',
            'residence_condition', 'indoor_condition', 'habitable', 'house_condition'];
        $hasParent =  ['governarate', 'city', 'location_id','mosques_id', 'branch_name'];
        $Parents = ['governarate'=>'country', 'mosques_id'=>'location_id','city' => 'governarate', 'location_id' => 'city', 'branch_name' => 'bank_id'];
        $ParentsNames = ['governarate'=>'aldol', 'city' => 'almhafth','mosques_id' => 'almasjed', 'location_id' => 'almdyn', 'branch_name' => 'asm_albnk'];

        $numeric =  ['id_card_number', 'spouses', 'unrwa_card_number',
            'primary_mobile', 'wataniya', 'secondary_mobile', 'phone',
            'monthly_income', 'rooms','area','rent_value',"points" ,"quran_parts","quran_chapters",
        ];

        $row=[];
        $row['card_type'] = 1;
        $row['errors']=[];

        foreach ($data as $k=>$v) {

            $key = null;
            if($k == 'sl_alkrab'){
                if(isset($_translator[$k])){
                    $key=CaseModel::$_translator[$k];
                }
            }
            elseif($k == 'hl_alam_maayl'){
                if(isset($_translator[$k])){
                    $key=CaseModel::$_translator[$k];
                }
            }
            elseif(substr( $k, -5 ) === "_alam"){
                $key=CaseModel::$_translator[substr($k, 0, -5)];
            }
            elseif(substr( $k,  -5 ) === "_alab"){
                $key=CaseModel::$_translator[substr($k, 0, -5)];
            }
            elseif(substr( $k, -9 ) === "_rb_alasr"){
                $key=CaseModel::$_translator[substr($k, 0, -9)];
            }
            elseif(substr( $k, -8 ) === "_almaayl"){
                $key=CaseModel::$_translator[substr($k, 0, -8)];
            }
            else{
                if(isset($_translator[$k])){
                    $key=CaseModel::$_translator[$k];
                }
            }

            if(!is_null($key)){

                if(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(!is_numeric($v)){
                            $row['errors'][$key]=trans('common::application.invalid_number');
                        }
                    }
                }

                if(in_array($key,$dates)){
                    if(!is_null($v) && $v != " "){
                        $date_ = Helpers::getFormatedDate($v);
                        if (!is_null($date_)) {
                            $row[$key]=$date_;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_date');
                        }
                    }
                }
                elseif($key =='gender'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ø°ÙƒØ±') ? 1 : 2;
                    }
                }
                elseif($key =='deserted'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 1 : 2;
                    }
                }
                elseif($key =='card_type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        if ($vlu == 'Ø¨Ø·Ø§Ù‚Ø© ØªØ¹Ø±ÙŠÙ�'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'Ø¬ÙˆØ§Ø² Ø³Ù�Ø±'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'ØªÙ‚Ù†ÙŠ') ? 2 : 1;
                    }
                }
                elseif($key =='level'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'Ù…Ù…ØªØ§Ø²'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'Ø¬ÙŠØ¯ Ø¬Ø¯Ø§'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'Ø¬ÙŠØ¯'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'Ù…Ù‚Ø¨ÙˆÙ„'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'Ø¶Ø¹ÙŠÙ�'){
                            $row[$key] = 5;
                        }
                    }
                }
                elseif($key =='grade'){
                    $vlu = trim($v);
                    if(!is_null($vlu) && $vlu != " "){
                        if($vlu == 'Ø§ÙˆÙ„ Ø§Ø¨ØªØ¯Ø§Ø¦ÙŠ'){ $row[$key] = 1;}
                        elseif($vlu == 'Ø«Ø§Ù†ÙŠ Ø§Ø¨ØªØ¯Ø§Ø¦ÙŠ'){$row[$key] = 2;}
                        elseif ($vlu == 'Ø«Ø§Ù„Ø« Ø§Ø¨ØªØ¯Ø¦ÙŠ'){$row[$key] = 3;}
                        elseif ($vlu == 'Ø±Ø§Ø¨Ø¹ Ø§Ø¨ØªØ¯Ø¦ÙŠ'){$row[$key] = 4;}
                        elseif ($vlu == 'Ø®Ø§Ù…Ø³ Ø§Ø¨ØªØ¯Ø¦ÙŠ'){$row[$key] = 5;}
                        elseif ($vlu == 'Ø³Ø§Ø¯Ø³ Ø§Ø¨ØªØ¯Ø¦ÙŠ'){$row[$key] = 6;}
                        elseif ($vlu == 'Ø§Ù„Ø³Ø§Ø¨Ø¹'){$row[$key] = 7;}
                        elseif ($vlu == 'Ø§Ù„Ø«Ø§Ù…Ù†'){$row[$key] = 8;}
                        elseif ($vlu == 'Ø§Ù„ØªØ§Ø³Ø¹'){$row[$key] = 9;}
                        elseif ($vlu == 'Ø§Ù„Ø¹Ø§Ø´Ø±'){$row[$key] = 10;}
                        elseif ($vlu == 'Ø§Ù„Ø­Ø§Ø¯ÙŠ Ø¹Ø´Ø±'){$row[$key] = 11;}
                        elseif ($vlu == 'Ø§Ù„Ø«Ø§Ù†ÙŠ Ø¹Ø´Ø±'){$row[$key] = 12;}
                        elseif ($vlu == 'Ø¯Ø±Ø§Ø³Ø§Øª Ø®Ø§ØµØ©'){$row[$key] = 13;}
                        elseif ($vlu == 'ØªØ¯Ø±ÙŠØ¨ Ù…Ù‡Ù†Ù‰'){$row[$key] = 14;}
                        elseif ($vlu == 'Ø³Ù†Ø© Ø£ÙˆÙ„Ù‰ Ø¬Ø§Ù…Ø¹Ø©'){$row[$key] = 15;}
                        elseif ($vlu == 'Ø³Ù†Ø© Ø«Ø§Ù†ÙŠØ© Ø¬Ø§Ù…Ø¹Ø©'){$row[$key] = 16;}
                        elseif ($vlu == 'Ø³Ù†Ø© Ø«Ø§Ù„Ø«Ø© Ø¬Ø§Ù…Ø¹Ø©'){$row[$key] = 17;}
                        elseif ($vlu == 'Ø³Ù†Ø© Ø±Ø§Ø¨Ø¹Ø© Ø¬Ø§Ù…Ø¹Ø©'){$row[$key] = 18;}
                        elseif ($vlu == 'Ø³Ù†Ø© Ø®Ø§Ù…Ø³Ø© Ø¬Ø§Ù…Ø¹Ø©'){$row[$key] = 19;}
                    }
                }
                elseif($key =='condition'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if ($vlu == 'Ù…Ø±Ø¶ Ù…Ø²Ù…Ù†'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'Ø°ÙˆÙŠ Ø§Ø­ØªÙŠØ§Ø¬Ø§Øª Ø®Ø§ØµØ©'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='refugee'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù…ÙˆØ§Ø·Ù†') ? 1 : 2;
                    }
                }
                elseif($key =='working'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 1 : 2;
                    }
                }
                elseif($key =='mother_is_guardian'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 1 : 2;
                    }
                }
                elseif($key =='study'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 1 : 0;
                    }
                }
                elseif($key =='can_work'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 2 : 1;
                    }
                }
                elseif($key =='promised'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'Ù†Ø¹Ù…') ? 2 : 1;
                    }
                }
                elseif(in_array($key,$constant)){
                    $parent =null;
                    $isChild =false;
                    if(in_array($key,$hasParent)){
                        $isChild =true;
                        $parentKey=$Parents[$key];
                        $parentName=$ParentsNames[$key];
                        if(isset($row[$parentKey])){
                            $parent=$row[$parentKey];
                        }
//                                                else{
//                                                    $parent=$value[$parentName];
//                                                }
                    }
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != '') {
                        $cons_val = CaseModel::constantId($key,$vlu,$isChild,$parent);
                        if(!is_null($cons_val)) {
                            $row[$key] = $cons_val;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_constant');
                        }
                    }
                }
                elseif(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(is_numeric($v)){
                            $row[$key]=(int)$v;
                        }
                    }
                }
                else{
                    if(!is_null($v) && $v != " "){
                        $row[$key]=$v;
                    }
                }
            }
        }

        $row['outSource']=true;
        if(isset($row['bank_id']) && isset($row['account_number'])&& isset($row['branch_name'])&& isset($row['branch_name'])){
            $row['banks'][]=[
                'bank_id' => $row['bank_id'],
                'account_owner' => $row['account_owner'],
                'account_number' => $row['account_number'],
                'branch_name' => $row['branch_name'],
                'check' => true
            ];

        }

        return $row;
    }

    // input card
    //  get person cases list

    public static function getPersonCardCases($cards)
    {

        $active = trans('auth::application.active');
        $inactive = trans('auth::application.inactive');

        return \DB::table('char_persons')
            ->join('char_cases', 'char_cases.person_id', '=', 'char_persons.id')
            ->join('char_categories', 'char_categories.id', '=', 'char_cases.category_id')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
            ->whereIn('char_persons.id_card_number',  $cards)
            ->selectRaw("
                            char_cases.id AS case_id,
                            char_persons.id_card_number,char_persons.old_id_card_number,
                            char_cases.category_id AS category_id,
                            char_categories.name AS category_name,
                            char_categories.type AS category_type,
                           char_organizations.name AS organization_name,
                           CASE WHEN char_cases.status = 1 THEN '$inactive'
                                WHEN char_cases.status = 0 THEN '$active'
                           END  AS status,
                           CASE WHEN char_cases.created_at is null THEN '-' Else char_cases.created_at  END  AS created_date,
                           DATE_FORMAT(char_cases.created_at,'%d-%m-%Y') as created_dt,
                           CASE WHEN char_cases.visitor is null THEN '-' Else char_cases.visitor  END  AS visitor,
                           CASE WHEN char_cases.visited_at is null THEN '-' Else char_cases.visited_at  END  AS visited_at,
                           CASE WHEN char_cases.notes is null THEN '-' Else char_cases.notes  END  AS notes,                           
                           CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                           CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                           CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                           ")->get();
    }

    public static function getCardCases($cards,$category_type)
    {

        $active = trans('auth::application.active');
        $inactive = trans('auth::application.inactive');

        return self::query()
                ->join('char_cases', 'char_cases.person_id', '=', 'char_persons.id')
                ->join('char_categories', function ($join) use($category_type){
                    $join->on('char_categories.id', '=', 'char_cases.category_id');
                    $join->where('char_categories.type', $category_type);
                })
                ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
                ->selectRaw("
                            char_organizations.name AS organization_name,
                            char_categories.name AS category_name,
                            char_categories.type AS category_type,
                            CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                   ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                            char_persons.id_card_number,char_persons.old_id_card_number,

                           CASE WHEN char_cases.status = 1 THEN '$inactive'
                                WHEN char_cases.status = 0 THEN '$active'
                           END  AS status,
                           CASE WHEN char_cases.created_at is null THEN '-' Else char_cases.created_at  END  AS created_date,
                           CASE WHEN char_cases.visitor is null THEN '-' Else char_cases.visitor  END  AS visitor,
                           CASE WHEN char_cases.visited_at is null THEN '-' Else char_cases.visited_at  END  AS visited_at,
                           CASE WHEN char_cases.notes is null THEN '-' Else char_cases.notes  END  AS notes,
                           CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                           CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                           CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                           ")
              ->whereIn('id_card_number',$cards)
              ->get();

    }

    public static function getPersonId($cards)
    {


        $person =  self::query()

            ->wherein('char_persons.id_card_number',  $cards)
            ->selectRaw("char_persons.id")->first();

        if(!is_null($person)){
            return $person->id;
        }

        return null;
    }

    // input card
    public static function PersonMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array(),"death_date"=>null,'card_type'=>"1");
        $_translator =array(
            "FULLNAME" =>  "fullname",
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "female_live" =>  "female_live",
            "male_live" =>  "male_live",
            "spouses" =>  "spouses",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'FULLNAME'){
                    if (!isset($data->FNAME_ARB)){
                        $parts = explode(" ",$v);
                        $row['first_name'] = $row['first_name'] = $row['first_name'] =$row['first_name'] = '';
                        if (isset($parts[0])){ $row['first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                        if (isset($parts[1])){ $row['second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                        if (isset($parts[2])){ $row['third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                        if (isset($parts[3])){ $row['last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }

                        if (sizeof($parts) > 4){
                            $length = sizeof($parts) - 1;
                            for ($x = 4; $x < $length; $x++) {
                                $row['last_name'] .= (is_null($parts[$x]) ||$parts[$x] == ' ' ) ? '-' :$parts[$x] ;
                            }
                        }
                    }

                    $row['fullname'] = $v;
                }
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }

        return $row;
    }

    public static function mapToUpdate($data)
    {

        $row = [];
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
//            "MOTHER_ARB" =>  "mother_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
//            "BIRTH_MAIN_CD" =>  "birth_place_country_id",
//            "BIRTH_PMAIN" =>  "birth_place_country",
//            "BIRTH_SUB_CD" =>  "birth_place_city_id",
//            "BIRTH_PSUB" =>  "birth_place_city",
//            "CITY_CD" => "governarate_id",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
//            "REGION_CD" =>  "city_id",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }
        return $row;
    }
    public static function mapBasicToUpdate($data)
    {


        $row = [];
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",

            "FULLNAME" =>  "fullname",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
            "STREET_ARB" =>  "street_address",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
            "family_cnt" =>  "family_cnt",
            "spouses" =>  "spouses",
            "male_live" =>  "male_live",
            "female_live" =>  "female_live",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if($k == 'FULLNAME'){

                if (!isset($data->FNAME_ARB)){
                    $parts = explode(" ",$v);
                    $row['first_name'] = $row['first_name'] = $row['first_name'] =$row['first_name'] = '';
                    if (isset($parts[0])){ $row['first_name'] = (is_null($parts[0]) ||$parts[0] == ' ' ) ? '-' :$parts[0] ; }
                    if (isset($parts[1])){ $row['second_name'] = (is_null($parts[1]) ||$parts[1] == ' ' ) ? '-' :$parts[1] ; }
                    if (isset($parts[2])){ $row['third_name'] = (is_null($parts[2]) ||$parts[2] == ' ' ) ? '-' :$parts[2] ; }
                    if (isset($parts[3])){ $row['last_name'] = (is_null($parts[3]) ||$parts[3] == ' ' ) ? '-' :$parts[3] ; }

                    if (sizeof($parts) > 4){
                        $length = sizeof($parts) - 1;
                        for ($x = 4; $x < $length; $x++) {
                            $row['last_name'] .= (is_null($parts[$x]) ||$parts[$x] == ' ' ) ? '-' :$parts[$x] ;
                        }
                    }
                }

                $row['fullname'] = $v;
            }
            elseif($k == 'ENG_NAME'){
                if(!is_null($v) && $v != " "){
                    $ENG_NAME_AR = explode(" ", $v);

                    if(is_array($ENG_NAME_AR)){
                        if(sizeof($ENG_NAME_AR) > 0){
                            if(sizeof($ENG_NAME_AR) == 4){
                                $row['en_first_name']=$ENG_NAME_AR[0];
                                $row['en_second_name']=$ENG_NAME_AR[1];
                                $row['en_third_name']=$ENG_NAME_AR[2];
                                $row['en_last_name']=$ENG_NAME_AR[3];
                            }else if(sizeof($ENG_NAME_AR) == 3){
                                $row['en_first_name']=$ENG_NAME_AR[0];
                                $row['en_second_name']=$ENG_NAME_AR[1];
                                $row['en_third_name']= ' ';
                                $row['en_last_name']=$ENG_NAME_AR[3];
                            }else if(sizeof($ENG_NAME_AR) == 2){
                                $row['en_first_name']=$ENG_NAME_AR[0];
                                $row['en_second_name']=' ';
                                $row['en_third_name']= ' ';
                                $row['en_last_name']=$ENG_NAME_AR[3];
                             }
                        }
                    }
                }

            }
            else{
                if(isset($_translator[$k])){
                    if(in_array($_translator[$k],$dates)){
                        if(!is_null($v) && $v != " "){
                            //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                            $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                        }
                    }
                    else{
                        $row [$_translator[$k]]=$v;
                    }
                }
            }

        }
        return $row;
    }

    // input card
    // gov services to get social affairs status
    public static function aidsCommitteeStatus($card)
    {
        return ['row'=> [] ,'status' => false,'msg' => trans('aid::application.not_person')];
    }

    // input card
    public static function personRelatedMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "REL_CI_ID_NUM" =>  "id_card_number",
            "REL_CI_FIRST_ARB" =>  "first_name",
            "REL_CI_FATHER_ARB" =>  "second_name",
            "REL_CI_GRAND_FATHER_ARB" => "third_name",
            "REL_CI_FAMILY_ARB" =>  "last_name",
            "REL_CI_PRV_FAMILY_ARB" =>  "prev_family_name",
            "REL_CI_SEX_CD" => "gender",
            "REL_CI_PERSONAL_CD" =>  "marital_status_id",
            "REL_CI_PERSONAL_DESC" =>  "marital_status",
            "REL_CI_BIRTH_DT" =>  "birthday",
            "REL_CI_DEAD_DT" =>  "death_date"
        );

        $dates = ['birthday' , 'death_date'];
        $row['kinship_id'] =null;
        $row['kinship_name']='-';

        foreach ($data as $k=>$v) {
            if($k == 'REL_DESC'){
                if($v =='wife'){
                    $wife_kinship=\Setting\Model\Setting::where('id','wife_kinship')->first();
                    if($wife_kinship){
                        if(!is_null($wife_kinship->value) and $wife_kinship->value != "") {
                            $row['kinship_id'] =$wife_kinship->value;
                            $row['kinship_name']=trans('common::application.wife_');
                        }
                    }
                }
                else if($v =='son' && $data->REL_CI_SEX_CD == 2){
                    $daughter_kinship = \Setting\Model\Setting::where('id', 'daughter_kinship')->first();
                    if($daughter_kinship){
                        if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
                            $row['kinship_id'] =$daughter_kinship->value;
                            $row['kinship_name']=trans('common::application.daughter_');
                        }
                    }
                }
                else {
                    $son_kinship = \Setting\Model\Setting::where('id', 'son_kinship')->first();
                    if($son_kinship){
                        if(!is_null($son_kinship->value) and $son_kinship->value != "") {
                            $row['kinship_id'] =$son_kinship->value;
                            $row['kinship_name']= trans('common::application.son_');
                        }
                    }
                }
            }
            else{
                if(isset($_translator[$k])){
                    if(in_array($_translator[$k],$dates)){
                        if(!is_null($v) && $v != " "){
                            $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                        }
                    }
                    else{
                        $row [$_translator[$k]]=$v;
                    }
                }
            }
        }

        return $row;
    }

    // input card
    // get id & name  by card

    public static function HasRaw($card)
    {
        $person = self::query()
                 ->where('id_card_number', '=', $card)
                 ->selectRaw("char_persons.id,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', 
                                ifnull(char_persons.last_name,' ')) AS name") ->first();

        return (is_null($person))? null : $person->id;
    }

    public static function cardCasesList($id_card_number){

        $language_id =  \App\Http\Helpers::getLocale();

        $query = self::query()
            ->join('char_cases', 'char_persons.id', '=', 'char_cases.person_id')
            ->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
            ->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
            ->where('char_persons.id_card_number', '=', $id_card_number);

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $query->selectRaw("
                           char_cases.id as case_id,
                           char_cases.person_id ,
                           char_cases.category_id ,
                           char_categories.type as category_type,
                           CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name ,
                           CASE WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name ,
                           CASE WHEN char_cases.status = 1 THEN '$inactive' WHEN char_cases.status = 0 THEN '$active'   END  AS status,
                           CASE WHEN char_cases.created_at is null THEN '-' Else char_cases.created_at  END  AS created_date,
                           CASE WHEN char_cases.visitor is null THEN '-' Else char_cases.visitor  END  AS visitor,
                           CASE WHEN char_cases.visited_at is null THEN '-' Else char_cases.visited_at  END  AS visited_at,
                           CASE WHEN char_cases.notes is null THEN '-' Else char_cases.notes  END  AS notes,
                           CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS visitor_evaluation,
                           CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS visitor_opinion,
                           CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS visitor_card
                           ");

        return $query->get();
    }

    public static function getByCards($cards)
    {

        return \DB::table('char_persons')
                ->leftjoin('char_persons_i18n As i18n', function($join) {
                    $join->on('char_persons.id', '=','i18n.person_id')
                        ->where('i18n.language_id',2);
                })
            ->whereIn('id_card_number',$cards)
            ->selectRaw("char_persons.id, char_persons.first_name,char_persons.second_name,char_persons.third_name,char_persons.last_name, 
                                   prev_family_name, id_card_number,card_type,gender,marital_status_id,birthday,death_date,
                                   father_id, mother_id,spouses, family_cnt,male_live,female_live ,
                                   i18n.language_id ,i18n.first_name as en_first_name ,i18n.second_name  as en_second_name ,
                                   i18n.third_name as en_third_name ,i18n.last_name  as en_last_name 
                                 ")->get();
    }

    public static function savePerson_($person_id,$input = array())
    {
        $language_id = 1;
        $en_language_id = 2;
        \DB::beginTransaction();
        PersonI18n::updateOrCreate(['person_id' => $person_id,'language_id' => $en_language_id],
            ['first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
            ]);
        \DB::commit();
        return true;

    }


    public static function saveFromSocialSearch($row)
    {
        $input = (array) $row;
        $person_Input = ["card_type", "deserted","birth_place", "death_cause_id","nationality" ,
                         "adscountry_id","adsdistrict_id","adsregion_id","adsneighborhood_id","adssquare_id","adsmosques_id" ,
                         "street_address","refugee","unrwa_card_number",
                         "is_qualified", "receivables","receivables_sk_amount",
                         "qualified_card_number","monthly_income","actual_monthly_income"];

        $IntegerInput =  ['habitable','house_condition','rooms','area','rent_value' ,'receivables',
                            'card_type','is_qualified','qualified_card_number',
                            'indoor_condition','residence_condition','roof_material_id','property_type_id',
                            'kinship_id','stage','condition','health_condition','marital_status_id',
                            'WhoIsGuardian','can_work','working','nationality','category_id' ,
                            'gender','study','prayer','save_quran','quran_center',
                            'birth_place' , 'refugee','alive','spouses','health_disease_id','deserted','family_cnt'];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{
                        $insert[$var] =$input[$var];
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }

        $person=null;

        $person = self::query()->where(['id_card_number' => (int) $input['id_card_number']])->first();
        if(!is_null($person)){
            $insert['adscountry_id'] =1;

            \DB::beginTransaction();

            if(sizeof($insert) > 0){
                self::where(['id' =>$person->id])->update($insert);
            }

            $person_id = $person->id;

            if(isset($input['phone'])){
                if($input['phone'] != null && $input['phone'] !=""){
                    if(PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])->first()){
                        PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])
                            ->update(['contact_value' => $input['phone']]);
                    }else{
                        PersonContact::create(['person_id' => $person_id, 'contact_type' => 'phone','contact_value' => $input['phone']]);
                    }
                }
            }

            if(isset($input['secondary_mobile'])){
                if($input['secondary_mobile'] != null && $input['secondary_mobile'] !=""){
                    if(PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])->first()){
                        PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])
                            ->update(['contact_value' => $input['secondary_mobile']]);
                    }else{
                        PersonContact::create(['person_id' => $person_id, 'contact_type' => 'secondary_mobile','contact_value' => $input['secondary_mobile']]);
                    }
                }
            }

            if(isset($input['primary_mobile'])){
                if($input['primary_mobile'] != null && $input['primary_mobile'] !=""){
                    if(PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])->first()){
                        PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])
                            ->update(['contact_value' => $input['primary_mobile']]);
                    }else{
                        PersonContact::create(['person_id' => $person_id, 'contact_type' => 'primary_mobile','contact_value' => $input['primary_mobile']]);
                    }

                }
            }

            if(isset($input['wataniya'])){
                if($input['wataniya'] != null && $input['wataniya'] !=""){
                    if(PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])->first()){
                        PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])
                            ->update(['contact_value' => $input['wataniya']]);
                    }else{
                        PersonContact::create(['person_id' => $person_id, 'contact_type' => 'wataniya','contact_value' => $input['wataniya']]);
                    }

                }
            }

            if(isset($input['working']) || isset($input['can_work']) ||isset($input['work_job_id']) || isset($input['work_status_id']) ||
                isset($input['work_wage_id']) || isset($input['work_location']) || isset($input['work_reason_id'])|| isset($input['has_other_work_resources'])) {

                $worksInput=['working','can_work','work_job_id','work_status_id','work_wage_id','work_reason_id','work_location'];
                $CreateWork = [];
                if (isset($input['working']) || isset($input['can_work'])) {
                    foreach ($input as $key => $value) {
                        if(in_array($key, $worksInput) &&  !(!$value || $value == "")) {
                            if($key == 'work_location'){
                                $CreateWork[$key] = $value;
                            }else{
                                $CreateWork[$key] = (int)$value;
                            }
                        }
                    }



                    $CreateWork['has_other_work_resources'] = 0;
                    if (isset($input['has_other_work_resources'])) {
                        if (($input['has_other_work_resources'] != null && $input['has_other_work_resources'] != "")) {
                            $CreateWork['has_other_work_resources'] = $input['has_other_work_resources'];
                        }
                    }

                    if (isset($CreateWork['working'])){
                        if($CreateWork['working'] !=1){
                            $CreateWork['work_job_id']=$CreateWork['work_status_id']=$CreateWork['work_wage_id']=$CreateWork['work_location']=null;
                        }
                    }
                    if ( isset($CreateWork['can_work'])){
                        if($CreateWork['can_work'] !=1){
                            $CreateWork['work_reason_id']=null;
                        }
                    }

                    if (sizeof($CreateWork) != 0) {
                        PersonWork::updateOrCreate(['person_id' => $person_id],$CreateWork);
                    }
                }

            }

            if (isset($input['property_type_id']) || isset($input['roof_material_id']) || isset($input['residence_condition']) ||
                isset($input['indoor_condition']) || isset($input['habitable']) || isset($input['house_condition']) || isset($input['rooms']) ||
                isset($input['area']) || isset($input['rent_value']) || isset($input['need_repair']) || isset($input['repair_notes'])) {

                $residenceInput=['property_type_id','roof_material_id','residence_condition','need_repair',
                    'repair_notes','indoor_condition','habitable','house_condition','rooms','area','rent_value'];

                $residenceInputInt=['property_type_id','roof_material_id','residence_condition','indoor_condition',
                    'habitable','house_condition','rooms','rent_value','need_repair'];
                $CreateResidence = [];

                foreach ($residenceInput as $var) {
                    if(isset($input[$var])){
                        if($input[$var] != null && $input[$var] != "") {
                            if(!in_array($var,$residenceInputInt)){
                                $CreateResidence[$var] = $input[$var];
                            }else{
                                $CreateResidence[$var] = (int)$input[$var];
                            }
                        }else{
                            $CreateResidence[$var] = null;
                        }
                    }
                }
                if (sizeof($CreateResidence) != 0) {
                    PersonResidence::saveResidence($person_id,$CreateResidence);
                }
            }

            if (isset($input['condition']) || isset($input['details']) || isset($input['disease_id'])  ||
                isset($input['has_health_insurance']) || isset($input['health_insurance_number']) ||
                isset($input['health_insurance_type']) || isset($input['has_device'])|| isset($input['used_device_name'])) {

                $CreateHealth = [];
                if (isset($input['has_device'])) {
                    if (($input['has_device'] != null && $input['has_device'] != "")) {
                        $CreateHealth['has_device'] = $input['has_device'];

                        if ($CreateHealth['has_device'] == 0) {
                            $CreateHealth['used_device_name'] =  null;
                        } else {

                            if (isset($input['used_device_name'])) {
                                if (($input['used_device_name'] != null && $input['used_device_name'] != "")) {
                                    $CreateHealth['used_device_name'] = $input['used_device_name'];
                                }
                            }
                        }
                    }
                }
                if (isset($input['has_health_insurance'])) {
                    if (($input['has_health_insurance'] != null && $input['has_health_insurance'] != "")) {
                        $CreateHealth['has_health_insurance'] = $input['has_health_insurance'];

                        if ($CreateHealth['has_health_insurance'] == 0) {
                            $CreateHealth['health_insurance_number'] =  null;
                            $CreateHealth['health_insurance_type'] = null;

                        } else {

                            if (isset($input['health_insurance_number'])) {
                                if (($input['health_insurance_number'] != null && $input['health_insurance_number'] != "")) {
                                    $CreateHealth['health_insurance_number'] = $input['health_insurance_number'];
                                }
                            }
                            if (isset($input['health_insurance_type'])) {
                                if (($input['health_insurance_type'] != null && $input['health_insurance_type'] != "")) {
                                    $CreateHealth['health_insurance_type'] = $input['health_insurance_type'];
                                }
                            }

                        }
                    }
                }
                if (isset($input['condition'])) {
                    if (($input['condition'] != null && $input['condition'] != "")) {
                        $CreateHealth['condition'] = $input['condition'];

                        if ($CreateHealth['condition'] == 1) {
                            $CreateHealth['disease_id'] =  null;
                            $CreateHealth['details'] = null;
                        } else {

                            if (isset($input['details'])) {
                                if (($input['details'] != null && $input['details'] != "")) {
                                    $CreateHealth['details'] = $input['details'];
                                }
                            }
                            if (isset($input['disease_id'])) {
                                if (($input['disease_id'] != null && $input['disease_id'] != "")) {
                                    $CreateHealth['disease_id'] = $input['disease_id'];
                                }
                            }
                        }

                    }
                }

                if (sizeof($CreateHealth) != 0) {
                    PersonHealth::updateOrCreate(['person_id' => $person_id],$CreateHealth);
                }
            }

            if( sizeof($input['aids_source'])> 0 ){

                $aids_source = [];
                foreach ($input['aids_source'] as $k=>$v){
                    $aids_source[] =[ 'aid_source_id' => $v->aid_source_id,'aid_type' => $v->aid_type,
                        'aid_value' => $v->aid_value, 'aid_take' => $v->aid_take,
                        'person_id' => $person_id, 'currency_id'=>4];
                }

                PersonAid::where(['person_id' => $person_id])->delete();
                PersonAid::insert($aids_source);

            }

            if( sizeof($input['properties'])> 0 ){

                $properties = [];
                foreach ($input['properties'] as $k=>$v){
                    $properties[] =['person_id'=>$person_id,'property_id'=>$v->property_id,
                        'has_property' =>$v->has_property, 'quantity'=>$v->quantity];
                }

                PersonProperties::where(['person_id' => $person_id])->delete();
                PersonProperties::insert($properties);

            }

            if( sizeof($input['banks'])> 0 ){
                foreach ($input['banks'] as $k=>$v){
                    $bank = PersonBank::where(['bank_id' => $v->bank_id,'account_number' => $v->account_number])->first();
                    if(!is_null($bank)){
                        if($bank->person_id == $person_id){
                            PersonBank::where(['bank_id' => $v->bank_id,'person_id'  => $person_id,
                                'account_number' => $v->account_number])
                                ->update(['account_owner' => $v->account_owner,'branch_name' => $v->branch_name]);
                        }
                    }else{
                        PersonBank::insert([ 'person_id'  => $person_id,
                            'bank_id' => $v->bank_id,'account_number' => $v->account_number,
                            'account_owner' => $v->account_owner,'branch_name' => $v->branch_name]);
                    }
                }
            }

            if(sizeof($input['family_members']) > 0 ){
                foreach ($input['family_members'] as $k=>$v) {
                 self::saveFamilySocialSearch(['id_card_number' => $v->id_card_number,'card_type' => $v->card_type,
                        'kinship_id' => $v->kinship_id, 'l_person_id' => $person_id,
                        'primary_mobile' => $v->primary_mobile, 'wataniya' => $v->watanya,
                        'condition' => $v->condition, 'disease_id' => $v->disease_id,
                        'details' => $v->details, 'currently_study' => $v->currently_study,
                        'school' => $v->school, 'grade' => $v->grade]);
                }
            }

            if(isset($input['category_id'])) {

                $caseInput=['notes','visited_at','visitor','visitor_card', 'visitor_opinion', 'visitor_evaluation'];

                $case_attributes=[];
                $category_id=$input['category_id'];

                foreach ($caseInput as $var) {
                    if(isset($input[$var])){
                        if(($input[$var] != null && $input[$var] != 'null' && $input[$var] != "")) {
                            if ($var == 'visited_at') {
                                $case_attributes[$var] = date('Y-m-d', strtotime($input[$var]));
                            }else{
                                $case_attributes[$var] = $input[$var];
                            }
                        }else{
                            $case_attributes[$var] = null;
                        }
                    }
                }

                if(sizeof($case_attributes) !=0){

                    CaseModel::where(['person_id' => $person_id,'category_id'=>$input['category_id']])
                        ->update($case_attributes);

                    if(isset($input['needs'])){
                        if($input['needs'] != null && $input['needs'] !="") {
                            CaseNeeds::whereIn('case_id',function ($q)use ($person_id,$category_id){
                                $q->select('id')->from('char_cases')
                                    ->where('person_id',$person_id)
                                    ->where('category_id' ,$category_id);
                            })->update(['needs'=>$input['needs']]);
                        }
                    }

                    if(isset($input['promised']) && isset($input['organization_name'])){
                        if($input['promised'] != null && $input['promised'] !="" && $input['organization_name'] != null && $input['organization_name'] !="") {
                            Reconstructions::whereIn('case_id',function ($q)use ($person_id,$category_id){
                                $q->select('id')->from('char_cases')
                                    ->where('person_id',$person_id)
                                    ->where('category_id' ,$category_id);
                            })->update(['promised' => $input['promised'] , 'organization_name' => $input['organization_name']]);

                        }
                    }
                }

                if( sizeof($input['essentials'])> 0 ){

                    $cases =  CaseModel::where(['person_id' => $person_id,'category_id'=>$input['category_id']])->get();
                    foreach ($cases as $k_=>$v_) {
                        $essentials = [];
                        foreach ($input['essentials'] as $k=>$v) {
                            $essentials[] = ['case_id'   => $v_->id ,'essential_id' => $v->essential_id,
                                'condition' => $v->condition ,'needs'=> $v->needs ];
                        }
                        CasesEssentials::where('case_id',$v_->id)->delete();
                        CasesEssentials::insert($essentials);
                    }
                }
            }

            \DB::commit();
        }

        return true;
    }

    public static function saveFamilySocialSearch($input = array()){

        $person_Input = ['card_type'];
        $IntegerInput = ['card_type'];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{

                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }

        \DB::beginTransaction();
        $person = self::query()->where(['id_card_number' => (int) $input['id_card_number']])->first();

        if(!is_null($person)){
            $person_id = $person->id;
            if(isset($input['card_type'])){
                if($input['card_type'] != null && $input['card_type'] !=""){
                    self::where(['id' =>$person->id])->update(['card_type' => $input['card_type']]);
                }
            }

            if(isset($input['phone'])){
                if($input['phone'] != null && $input['phone'] !=""){
//                    \Common\Model\PersonModels\PersonContact::updateOrCreate(['person_id' => $person_id, 'contact_type' => 'phone'],
//                                                                                 ['contact_value' => $input['phone']]);

                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'phone'])
                            ->update(['contact_value' => $input['phone']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'phone','contact_value' => $input['phone']]);
                    }
                }
            }
            if(isset($input['secondary_mobile'])){
                if($input['secondary_mobile'] != null && $input['secondary_mobile'] !=""){
//                       \Common\Model\PersonModels\PersonContact::updateOrCreate(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'],
//                                                                                    ['contact_value' => $input['secondary_mobile']]);
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'secondary_mobile'])
                            ->update(['contact_value' => $input['secondary_mobile']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'secondary_mobile','contact_value' => $input['secondary_mobile']]);
                    }
                }
            }
            if(isset($input['primary_mobile'])){
                if($input['primary_mobile'] != null && $input['primary_mobile'] !=""){
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'primary_mobile'])
                            ->update(['contact_value' => $input['primary_mobile']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'primary_mobile','contact_value' => $input['primary_mobile']]);
                    }

                }
            }
            if(isset($input['wataniya'])){
                if($input['wataniya'] != null && $input['wataniya'] !=""){
                    if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])->first()){
                        \Common\Model\PersonModels\PersonContact::where(['person_id' => $person_id, 'contact_type' => 'wataniya'])
                            ->update(['contact_value' => $input['wataniya']]);
                    }else{
                        \Common\Model\PersonModels\PersonContact::create(['person_id' => $person_id, 'contact_type' => 'wataniya','contact_value' => $input['wataniya']]);
                    }

                }
            }

            if(isset($input['working']) || isset($input['can_work']) ||isset($input['work_job_id']) || isset($input['work_status_id']) ||
                isset($input['work_wage_id']) || isset($input['work_location']) || isset($input['work_reason_id'])|| isset($input['has_other_work_resources'])) {

                $worksInput=['working','can_work','work_job_id','work_status_id','work_wage_id','work_reason_id','work_location'];
                $CreateWork = [];
                if (isset($input['working']) || isset($input['can_work'])) {
                    foreach ($input as $key => $value) {
                        if(in_array($key, $worksInput) &&  !(!$value || $value == "")) {
                            if($key == 'work_location'){
                                $CreateWork[$key] = $value;
                            }else{
                                $CreateWork[$key] = (int)$value;
                            }
                        }
                    }



                    $CreateWork['has_other_work_resources'] = 0;
                    if (isset($input['has_other_work_resources'])) {
                        if (($input['has_other_work_resources'] != null && $input['has_other_work_resources'] != "")) {
                            $CreateWork['has_other_work_resources'] = $input['has_other_work_resources'];
                        }
                    }

                    if (isset($CreateWork['working'])){
                        if($CreateWork['working'] !=1){
                            $CreateWork['work_job_id']=$CreateWork['work_status_id']=$CreateWork['work_wage_id']=$CreateWork['work_location']=null;
                        }
                    }
                    if ( isset($CreateWork['can_work'])){
                        if($CreateWork['can_work'] !=1){
                            $CreateWork['work_reason_id']=null;
                        }
                    }

                    if (sizeof($CreateWork) != 0) {
                        \Common\Model\PersonModels\PersonWork::updateOrCreate(['person_id' => $person_id],$CreateWork);
                    }
                }

            }

            if(isset($input['currently_study']) || isset($input['grade']) || isset($input['school'])) {

                $CreateEducation = [];
                $EduInput=['grade','school','study','currently_study'];
                $EduInputIn=['grade','study','currently_study'];

                if (isset($input['currently_study'])&&  ($input['currently_study']  != null && $input['currently_study'] != "")) {
                    $CreateEducation['currently_study'] = $input['currently_study'];
                    $CreateEducation['study'] = $input['currently_study'];
                }
                $input['study'] = $input['currently_study'] ;
                if (isset($input['study'])&&  ($input['study']  != null && $input['study'] != "")) {
                    $CreateEducation['study'] = (int) $input['study'];
                    foreach ($EduInput as $var) {
                        if(isset($input[$var])){
                            if($CreateEducation['study'] == 1 && ($input[$var] != null && $input[$var] != "")) {
                                if(!in_array($var,$EduInputIn)){
                                    $CreateEducation[$var] = $input[$var];
                                }else{
                                    $CreateEducation[$var] = (int)$input[$var];
                                }

                            }else{
                                $CreateEducation[$var] = null;
                            }
                        }
                    }
                }
                if (sizeof($CreateEducation) != 0) {
                    PersonEducation::updateOrCreate(['person_id' => $person_id],$CreateEducation);
//                    if(PersonEducation::where(['person_id' => $person_id])->first()){
//                       return PersonEducation::where(['person_id' => $person_id])->update($CreateEducation);
//                    }else{
//                        $CreateEducation['person_id']=$person_id;
//                        return PersonEducation::create($CreateEducation);
//                    }
                }
            }

            if (isset($input['condition']) || isset($input['details']) || isset($input['disease_id'])) {

                $CreateHealth = [];
                if (isset($input['condition'])) {
                    if (($input['condition'] != null && $input['condition'] != "")) {
                        $CreateHealth['condition'] = $input['condition'];

                        if ($CreateHealth['condition'] == 1) {
                            $CreateHealth['disease_id'] =  null;
                            $CreateHealth['details'] = null;
                        } else {

                            if (isset($input['details'])) {
                                if (($input['details'] != null && $input['details'] != "")) {
                                    $CreateHealth['details'] = $input['details'];
                                }
                            }
                            if (isset($input['disease_id'])) {
                                if (($input['disease_id'] != null && $input['disease_id'] != "")) {
                                    $CreateHealth['disease_id'] = $input['disease_id'];
                                }
                            }
                        }

                    }
                }

                if (sizeof($CreateHealth) != 0) {
                    \Common\Model\PersonModels\PersonHealth::updateOrCreate(['person_id' => $person_id],$CreateHealth);
                }

            }

            if ( isset($input['kinship_id']) && isset($input['l_person_id']) ) {
                if (($input['l_person_id'] != null && $input['l_person_id'] != "")) {
                    if(\Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id])->first()){
                        \Common\Model\PersonModels\PersonKinship::where(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id])->update(['kinship_id'=>$input['kinship_id']]);
                    }else{
                        \Common\Model\PersonModels\PersonKinship::create(["l_person_id"=> $input['l_person_id'],"r_person_id"=>$person_id,'kinship_id'=>$input['kinship_id']]);
                    }
                }
            }
        }
        \DB::commit();

        return true ;

    }

    public static function insertPerson($input = array())
    {

        $en_language_id = 2;
        $person_Input = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','birthday', 'gender',
                         'marital_status_id','deserted', 'birth_place', 'nationality', 'city', 'country','mosques_id',
                         'governarate', 'location_id', 'street_address', 'refugee','unrwa_card_number',
                         'monthly_income', 'prev_family_name', 'father_id', 'mother_id', 'death_date', 'death_cause_id',
                        "family_cnt", "spouses", "male_live", "female_live"];
        $IntegerInput =  ['card_type','marital_status_id','marital_status_id',"family_cnt", "spouses", "male_live", "female_live"];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{

                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }

        $return=array();
        $person=null;
        $person = new Person();
        foreach ($insert as $key => $value) {
            $person->$key = $value;
        }

        if (!isset($person->card_type)){
            $person->card_type = '1';

            if (isset($person->id_card_number)){
                if (substr($person->id_card_number, 0, 1) === '7') {
                    $person->card_type = '2';
                }
            }

        }

        $person->save();

        if ($person) {
            $return['id']= $person->id;
            $return['id']= $person->id;
            $return['gender']= $person->gender;
            $return['death_date']= $person->death_date;
            $return['id_card_number']= $person->id_card_number;
            $return['card_type']= $person->card_type;
            $return['first_name']= (is_null($person->first_name) || $person->first_name == ' ' ) ? ' ' : $person->first_name;
            $return['second_name']= (is_null($person->second_name) || $person->second_name == ' ' ) ? ' ' : $person->second_name;
            $return['third_name']= (is_null($person->third_name) || $person->third_name == ' ' ) ? ' ' : $person->third_name;
            $return['last_name']= (is_null($person->last_name) || $person->last_name == ' ' ) ? ' ' : $person->last_name;
            $return['full_name']=  $return['first_name']. ' ' . $return['second_name'] .' ' .$return['third_name']. ' ' . $return['last_name'];
            $return['adscountry_id']= $person->adscountry_id;
            $return['adsregion_id']= $person->adsregion_id;
            $return['adsdistrict_id']= $person->adsdistrict_id;
            $return['adsneighborhood_id']= $person->adsneighborhood_id;
            $return['adssquare_id']= $person->adssquare_id;
            $return['adsmosques_id']= $person->adsmosques_id;

            $person_id = $person->id;
            if(isset($input['en_first_name']) && isset($input['en_second_name']) && isset($input['en_third_name']) && isset($input['en_last_name'])) {
                if(PersonI18n::where(['person_id' => $person_id,'language_id' => $en_language_id])->first()){
                    PersonI18n::where(['person_id' => $person_id,'language_id' => $en_language_id])
                        ->update(['first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                            'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                        ]);
                }else{
                    PersonI18n::create(['person_id' => $person_id,'language_id' => $en_language_id,'first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                        'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                    ]);
                }
            }

        }
        return $return;

    }

    public static function oldCardCheck($cards)
    {

        return \DB::table('char_persons')
                  ->where(function ($q) use ($cards){
                      $q->whereRaw('(id_card_number != old_id_card_number)');
                      $q->whereNotNull('old_id_card_number');
                      $q->whereIn('old_id_card_number',$cards);
                  })
                  ->selectRaw("char_persons.id_card_number , char_persons.old_id_card_number")
                  ->get();

    }

}

