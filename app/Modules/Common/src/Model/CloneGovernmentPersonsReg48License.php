<?php

namespace Common\Model;

class CloneGovernmentPersonsReg48License  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_reg48_license';
    protected $fillable = ['WORKER_ID','STATUS_NAME','DATE_FROM','DATE_TO','LICENSE_NAME',
                           'LIC_REC_STATUS_NAME','LIC_REC_DATE'];
    public $timestamps = false;
}

