<?php

namespace Common\Model;

use Illuminate\Database\Eloquent\Model;
use Setting\Model\Setting;

class PersonDocument extends Model
{
    protected $table = 'char_persons_documents';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('person_id', '=', $this->person_id)
              ->where('document_type_id', '=', $this->document_type_id);

        return $query;
    }
    
    public static function fetch($options = array())
    {
        $query = static::query();
        $query->join('doc_files', 'char_persons_documents.document_id', '=', 'doc_files.id')
              ->addSelect([
                  'doc_files.filepath',
                  'doc_files.size',
                  'doc_files.mimetype',
              ]);
        
        return $query->get();
    }

    public static function createNew($attributes)
    {
        return \Illuminate\Support\Facades\DB::transaction(function() use ($attributes) {
            $file = \Document\Model\File::forceCreate([
                'name' => $attributes['name'],
                'filepath' => $attributes['filepath'],
                'size' => $attributes['size'],
                'mimetype' => $attributes['mimetype'],
                'file_status_id' => 1,
                'created_by' => $attributes['user_id'],
            ]);
            if ($file->id) {
                $caseFile = self::forceCreate([
                    'person_id' => $attributes['person_id'],
                    'document_type_id' => $attributes['document_type_id'],
                    'document_id' => $file->id,
                    'status' => 1,
                ]);
                
                return $caseFile;
            }
        });
    }

    public static function getPersonDocuments($id){
        return  self::query()->join('char_document_types_i18n', function($q) {
                                    $q->on('char_persons_documents.document_type_id', '=', 'char_document_types_i18n.document_type_id');
                                    $q->where('char_document_types_i18n.language_id',1);
                              })
                              ->where('char_persons_documents.person_id',$id)
                              ->select('char_document_types_i18n.name as name',
                                       'char_document_types_i18n.document_type_id as document_type_id',
                                       'char_persons_documents.document_id as file_id')
                              ->get();
    }
    public static function personalImagePath($person_id,$type){

        $key = ($type == 1) ? 'image' : 'aid_image';

        $document = Setting::where('id',$key)->first();
        if(is_null($document)){
            return null;
        }

        $filePath = static::query()
            ->join('doc_files', 'char_persons_documents.document_id', '=', 'doc_files.id')
            ->where('char_persons_documents.person_id',$person_id)
            ->where('char_persons_documents.document_type_id',$document->value)
            ->selectRaw('doc_files.filepath')
            ->first();

        return is_null($filePath) ? null : base_path('storage/app/').$filePath->filepath;

    }

    public static function hasPersonalImagePath($person_id,$type){

        $key = ($type == 1) ? 'image' : 'aid_image';

        $document = Setting::where('id',$key)->first();
        if(is_null($document)){
            return null;
        }

        $filePath = static::query()
            ->join('doc_files', 'char_persons_documents.document_id', '=', 'doc_files.id')
            ->where('char_persons_documents.person_id',$person_id)
            ->where('char_persons_documents.document_type_id',$document->value)
            ->selectRaw('doc_files.filepath')
            ->first();

        return is_null($filePath) ? false : true;

    }

    public static function attachmentImagePath($person_id,$document_type_id){

        $file = static::query()
            ->join('doc_files', 'char_persons_documents.document_id', '=', 'doc_files.id')
            ->where('char_persons_documents.person_id',$person_id)
            ->where('char_persons_documents.document_type_id',$document_type_id)
            ->selectRaw('doc_files.filepath,doc_files.mimetype')
            ->first();

        return $file;

    }

}