<?php

namespace Common\Model\Categories;

abstract class AbstractCategories extends \Illuminate\Database\Eloquent\Model
{

    const TYPE_SPONSORSHIPS = 1;
    const TYPE_AIDS = 2;


    protected $table = 'char_categories';
//    protected $fillable = ['name','type'];
    public $timestamps = false;

    protected $appends = ['type_name', 'type_options'];


    public function documents()
    {
        return $this->hasMany('Common\Model\CategoriesDocument','category_id','id');
    }

    public static function type($value = null)
    {
        $options = array(
            self::TYPE_AIDS => trans('application::application.aids'),
            self::TYPE_SPONSORSHIPS => trans('application::application.sponsorship'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getCategoriesFormSections($id,$has_custom,$mode)
    {
        $Sections= $this->Sections($has_custom,$mode);
        $CategoriesFormsSections=CategoriesFormsSections::where('category_id',$id)->get();
        $Formsections=array();
        $count=1;
        foreach($Sections as $key) {

            if($key =='vouchersData' && $mode=='show') {
                array_push($Formsections,[ 'id' =>$key ,'StepNumber' => $count]);
                $count++;
            }

            if($key =='customFormData' && $has_custom) {
                array_push($Formsections,[ 'id' =>$key ,'StepNumber' => $count]);
                $count++;
            }

            foreach($CategoriesFormsSections as $k =>$v) {
                if($v->id == $key){
                    array_push($Formsections,[ 'id' =>$key ,'StepNumber' => $count]);
                    $count++;
                }
            }

        }
        return $Formsections;
    }

    public function getType()
    {
        return self::type($this->type);
    }

    public function getTypeNameAttribute()
    {
        return $this->getType();
    }

    public function getTypeOptionsAttribute()
    {
        return self::type();
    }

    public static function getTemplates($organization_id,$type){
        $language_id =  \App\Http\Helpers::getLocale();

        return
            \DB::table('char_categories')
                ->leftjoin('char_templates', function($q) use($organization_id){
                    $q->on('char_templates.category_id','=','char_categories.id');
                    $q->where('char_templates.organization_id','=',$organization_id);
                })
                ->where('char_categories.type',$type)
                ->selectRaw("CASE WHEN $language_id = 1 THEN char_categories.name 
                                  Else char_categories.en_name END  
                                  AS name,
                             char_templates.id as id , char_categories.id as category_id , char_templates.filename as template")
                ->get();
    }

    public static function whoIsCase($id)
    {
        $category= \DB::table('char_categories')
                        ->where('char_categories.id',$id)
                        ->select('char_categories.case_is')
                        ->first();

        if($category->case_is == 2){
            return 'guardian';
        }

        return 'person';

    }

    public static function CatType($id)
    {
        $category= \DB::table('char_categories')
                        ->where('char_categories.id',$id)
                        ->first();

        return $category->type;

    }
}
