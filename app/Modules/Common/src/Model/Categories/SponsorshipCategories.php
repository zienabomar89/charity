<?php
namespace Common\Model\Categories;

use Forms\Model\CustomForms;
use Common\Model\Categories\CategoriesForm;

class SponsorshipCategories extends \Common\Model\Categories\AbstractCategories
{
    protected $target = AbstractCategories::TYPE_SPONSORSHIPS;

    public function getSectionForEachCategory($has_custom)
    {

        $user = \Auth::user();
        $query = self::query()->where('type',1)->selectRaw('char_categories.*');
        $Categories = $query->get();

        $CategoriesSections=[];
        foreach($Categories as $item) {
            
            $has_custom = false;
            $id = $item->id;
            $org_form = CategoriesForm::where(['category_id'=>$id,'status'=>0,'organization_id'=>$user->organization_id])->first();
        
            if($item->family_structure == 1){

                if(!is_null($org_form)){

                    if($org_form->status == 0){
                        $has_custom=true;
                    }

                }
                else{
                    $form = \Setting\Model\Setting::where(['id'=>'sponsorship_custom_form','organization_id'=>$user->organization_id])->first();

                    if($form){
                        $ActiveForm=CustomForms::where(function ($q) use($form){
                            $q->where('id','=',$form->value);
                            $q->where('status','=',1);
                        })->first();

                        if($ActiveForm){
                            $has_custom=true;
                        }
                    }
                }

            }  
            
            $target_sections=$this->getCategoriesFormSections($item->id,$has_custom,'add');
            if(sizeof($target_sections) >0){
                $target_state=$this->getTargetSections($target_sections[0]['id']);
                array_push($CategoriesSections,['id'=> $item->id,'name' => $item->name,'en_name' => $item->en_name, 'FirstStep' => $target_state]);
            }

        }
        return $CategoriesSections;
    }
    public function Sections($has_custom,$mode){

        if($has_custom){
            if($mode != 'show') {
                return ['CaseData','ParentData','providerData','familyDocuments','caseDocuments','FamilyData','customFormData'];
            }else{
                return ['CaseData','ParentData','providerData','familyDocuments','customFormData','caseSponsorshipList','casePaymentList','FamilyData','caseDocuments'];
            }
        }else{
            if($mode != 'show') {
                return ['CaseData','ParentData','providerData','familyDocuments','caseDocuments','FamilyData'];
            }else{
                return ['CaseData','ParentData','providerData','familyDocuments','caseSponsorshipList','casePaymentList','FamilyData','caseDocuments'];
            }

        }

    }
    public function getCategoriesFormSections($id,$has_custom,$mode)
    {
        $Sections= $this->Sections($has_custom,$mode);
        $CategoriesFormsSections=\Common\Model\CategoriesFormsSections::where('category_id',$id)->get();
        $FormSections=array();
        $count=1;
        foreach($Sections as $key) {
            if( ($key =='caseSponsorshipList' ||$key =='casePaymentList') && $mode=='show') {
                array_push($FormSections,[ 'id' =>$key ,'StepNumber' => $count]);
                $count++;
            }

            foreach($CategoriesFormsSections as $k =>$v) {
                if($v->id == $key){
                    array_push($FormSections,[ 'id' =>$key ,'StepNumber' => $count]);
                    $count++;
                }

                if(sizeof($FormSections) == sizeof($CategoriesFormsSections)){
                    if($has_custom) {
                        array_push($FormSections,[ 'id' =>'customFormData','StepNumber' => $count ]);
                        $count++;
                    }
                }

            }
        }

        return $FormSections;

    }
    public function getTargetSections($id)
    {
        switch ($id) {
            case 'CaseData':
                $target_state='case-info';
                break;
            case 'ParentData':
                $target_state='parent-info';
                break;
            case 'providerData'  :
                $target_state='guardian-info';
                break;
            case 'customFormData':
                $target_state='custom-form-data';
                break;
            case 'caseSponsorshipList' :
                $target_state='sponsorship-data';
                break;
            case 'casePaymentList' :
                $target_state='payment-data';
                break;
            case 'FamilyData' :
                $target_state='family-info';
                break;
            case 'caseDocuments' :
                $target_state='case-documents';
                break;
            case 'familyDocuments' :
                $target_state='family-documents';
                break;
        }
        return $target_state;
    }
    public static function getSponsorTemplates($id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        return
            \DB::table('char_categories')
                ->leftjoin('char_templates', function($q) use($id){
                    $q->on('char_categories.id','=','char_templates.category_id')
                        ->where('char_templates.organization_id','=',$id)
                        ->where('char_templates.template','candidate');
                })
                ->where('char_categories.type',1)
                ->orderBy('char_categories.name')
                ->selectRaw("char_templates.id as id,
                             CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS name,
                             char_categories.id as category_id,
                             char_templates.filename as template,
                             char_templates.en_filename as en_template")
                ->get();
    }

    public static function getSponsorReports($id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        return
            \DB::table('char_categories')
                ->leftjoin('char_templates', function($q) use($id){
                    $q->on('char_categories.id','=','char_templates.category_id')
                        ->where('char_templates.organization_id','=',$id)
                        ->where('char_templates.template','reports');
                })
                ->where('char_categories.type',1)
                ->orderBy('char_categories.name')
                ->selectRaw("char_templates.id as id,
                             CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS name,
                            char_categories.id as category_id,
                            char_templates.filename as template,
                            char_templates.other_filename as other_template,
                            char_templates.en_filename as en_template,
                            char_templates.en_other_filename as en_other_template")
                ->get();
    }

}

