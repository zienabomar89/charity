<?php

namespace Common\Model\Categories;

use Forms\Model\CustomForms;
use Common\Model\Categories\CategoriesForm;


class AidsCategories extends \Common\Model\Categories\AbstractCategories
{
    protected $target = AbstractCategories::TYPE_AIDS;

    public function getSectionForEachCategory($has_custom)
    {
        
        $user = \Auth::user();

        $query = self::query()->where('type',2)->selectRaw('char_categories.*');
        $Categories = $query->get();

        $CategoriesSections=[];
        foreach($Categories as $item) {
            $has_custom = false;
            $id = $item->id;
            $org_form = CategoriesForm::where(['category_id'=>$id,'organization_id'=>$user->organization_id])->first();
        
            if(!is_null($org_form)){

                if($org_form->status == 0){
                    $has_custom=true;
                }

            }else{
                $form = \Setting\Model\Setting::where(['id'=>'aid_custom_form','organization_id'=>$user->organization_id])->first();

                if($form){
                    $ActiveForm=CustomForms::where(function ($q) use($form){
                        $q->where('id','=',$form->value);
                        $q->where('status','=',1);
                    })->first();

                    if($ActiveForm){
                        $has_custom=true;
                    }
                }
            }  
            $target_sections=$this->getCategoriesFormSections($item->id,$has_custom,'add');
            if(sizeof($target_sections) >0){
                $target_state=$this->getTargetSections($target_sections[0]['id']);
                array_push($CategoriesSections,['id'=> $item->id,'name' => $item->name,'en_name' => $item->en_name, 'FirstStep' => $target_state]);
            }
        }
        return $CategoriesSections;
    }
    public function getCategoriesFormSections($id,$has_custom,$mode)
    {
        $Sections= $this->Sections($has_custom,$mode);
        $CategoriesFormsSections=\Common\Model\CategoriesFormsSections::where('category_id',$id)->get();
        $FormSections=array();
        $count=1;
        foreach($Sections as $key) {

            if($key =='vouchersData' && $mode=='show') {
                array_push($FormSections,[ 'id' =>$key ,'StepNumber' => $count]);
                $count++;
            }

            if($key =='customFormData' && $has_custom) {
                array_push($FormSections,[ 'id' =>$key ,'StepNumber' => $count]);
                $count++;
            }

            foreach($CategoriesFormsSections as $k =>$v) {
                if($v->id == $key){
                    array_push($FormSections,[ 'id' =>$key ,'StepNumber' => $count]);
                    $count++;
                }
            }

        }
        return $FormSections;
    }
    public function getTargetSections($id)
    {
        $target_state='';
        switch ($id) {
            case 'personalInfo' :
                $target_state='personal-info';
                break;
            case 'residenceData' :
                $target_state='residence-data';
                break;
            case  'homeIndoorData' :
                $target_state='home-indoor-data';
                break;
            case 'otherData' :
                $target_state='other-data';
                break;
            case 'vouchersData' :
                $target_state='vouchers-data';
                break;
            case 'familyStructure' :
                $target_state='family-structure';
                break;
            case 'CaseDocumentData' :
                $target_state='case-DocumentData';
                break;
            case  'customFormData' :
                $target_state='custom-form';
                break;
            case 'recommendations' :
                $target_state='recommendations';
                break;
        }
        return $target_state;
    }
    public function Sections($has_custom,$mode){
        if($has_custom){
            if($mode == 'show') {
                return ['personalInfo','residenceData','homeIndoorData','otherData','vouchersData','familyStructure','CaseDocumentData','customFormData','recommendations'];
            }else{
                return ['personalInfo','residenceData','homeIndoorData','otherData','familyStructure','CaseDocumentData','customFormData','recommendations'];
            }
        }else{
            if($mode == 'show') {
                return ['personalInfo','residenceData','homeIndoorData','otherData','vouchersData','familyStructure','CaseDocumentData','recommendations'];
            }else{
                return ['personalInfo','residenceData','homeIndoorData','otherData','familyStructure','CaseDocumentData','recommendations'];
            }
        }

    }

}
