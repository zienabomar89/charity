<?php

namespace Common\Model\Categories;

use App\Http\Helpers;

class CategoriesForm extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_categories_form';
    public $fillable = ['category_id','form_id','organization_id','status'];
    protected $hidden = ['created_at','updated_at'];


    public static function forms($type,$organization_id){

        $language_id =  \App\Http\Helpers::getLocale();

        return \DB::table('char_categories')
                  ->leftjoin('char_categories_form', function($q) use ($organization_id) {
                        $q->on('char_categories_form.category_id', '=', 'char_categories.id');
                        $q->where('char_categories_form.organization_id',$organization_id);
                  })
                 ->leftjoin('char_forms','char_forms.id', '=', 'char_categories_form.form_id')
                 ->where('char_categories.type','=',$type)
                 ->orderBy('char_categories.name')
                 ->selectRaw("CASE WHEN $language_id = 1 THEN char_categories.name 
                                  Else char_categories.en_name END  
                                  AS category_name,
                                 char_categories_form.organization_id as organization_id ,
                                 char_categories_form.form_id as form_id ,
                                 char_categories_form.status as status ,
                                 char_forms.name as form_name ,
                                 char_categories.id as category_id
                                 ")->get();

    }

}
