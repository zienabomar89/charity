<?php
namespace Common\Model;

use App\Http\Helpers;

class Citizen  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_citizens';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ["first_name", "second_name", "third_name", "last_name", "prev_family_name", "id_card_number",
        "card_type", "gender", "deserted", "mobile", "wataniya", "phone", "marital_status_id", "birthday", "spouses",
        "refugee", "unrwa_card_number", "monthly_income", "family_cnt", "male_live", "female_live",
        "adscountry_id", "adsdistrict_id", "adsregion_id", "adsneighborhood_id",
        "adssquare_id", "adsmosques_id", "street_address"];

}
