<?php

namespace Common\Model;

class CloneGovernmentPersonsVehicles  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_vehicles';
    protected $fillable = ['IDNO','MODEL_YEAR','OWNER_TYPE_NAME'];
    public $timestamps = false;

}
