<?php

namespace Common\Model;

class CloneGovernmentPersonsMarriage  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_marriage';
    protected $fillable = ['IDNO','CONTRACT_NO','CONTRACT_DT','HUSBAND_SSN','HUSBAND_NAME',
        'WIFE_SSN','WIFE_NAME'];
    public $timestamps = false;

}

