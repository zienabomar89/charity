<?php

namespace Common\Model;

class CloneGovernmentPersonsTravel  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_travel';
    protected $fillable = ['IDNO','CTZN_TRANS_DT','CTZN_TRANS_TYPE','CTZN_TRANS_BORDER'];
    public $timestamps = false;
}
