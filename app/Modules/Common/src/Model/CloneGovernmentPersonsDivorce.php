<?php

namespace Common\Model;

class CloneGovernmentPersonsDivorce  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_divorce';
    protected $fillable = ['IDNO','DIV_CERTIFIED_NO','CONTRACT_DT','DIV_TYPE_NAME','HUSBAND_SSN','HUSBAND_NAME',
                          'WIFE_SSN','WIFE_NAME'];
    public $timestamps = false;

}
