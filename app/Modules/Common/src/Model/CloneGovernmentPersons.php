<?php

namespace Common\Model;

class CloneGovernmentPersons  extends \Illuminate\Database\Eloquent\Model
{
    protected $table ='clone_government_persons';
    protected $fillable = ['IDNO','FATHER_IDNO','MOTHER_IDNO','FULLNAME',
                            'FNAME_ARB','SNAME_ARB','TNAME_ARB','LNAME_ARB'  ,
                            'PREV_LNAME_ARB','MOTHER_ARB','ENG_NAME'  ,
                            'SEX','SOCIAL_STATUS','CI_RELIGION'  ,
                            'MOBILE','TEL'  ,
                            'BIRTH_PMAIN'  ,'BIRTH_PSUB'  ,'BIRTH_DT'  ,'DETH_DT'  ,'IS_DEAD_DESC'  ,
                            'CI_CITY'  ,'CI_REGION'  ,'STREET_ARB'  ,
                            'CTZN_STATUS'  ,'CTZN_TRANS_DT'  ,'VISIT_PURPOSE_DESC'  ,
                            'photo_url'  ,'photo_update_date'  ,
                            'PASSPORT_NO'  ,'PASSPORT_ISSUED_ON'  ,'PASSPORT_TYPE'  ,
                            'CARD_NO'  ,'EXP_DATE'  ,
                            'INS_STATUS_DESC'  ,'INS_TYPE_DESC'  ,'WORK_SITE_DESC' ,
                            'family_cnt', 'spouses', 'male_live   ', 'female_live  ',
                            'GOV_NAME','CITY_NAME','PART_NAME ','ADDRESS_DET',
                            'social_affairs_status'   ,'AID_CLASS'  ,'AID_TYPE'  ,'AID_AMOUNT'   ,'AID_SOURCE'   ,
                            'AID_PERIODIC'   ,'ST_BENEFIT_DATE' ,'END_BENEFIT_DATE' ,'home_address' ,
                            'near_mosque' ,'paterfamilias_mobile' ,'telephone' ,'current_career' ,
                            'father_death_reason' ,'mother_death_reason' ,
                            'building_type' ,'home_type' ,'furniture_type' ,
                            'home_status' ,'home_description' ,'total_family_income' ,'health_status' ,
                            'wife_mobile' ,'affected_by_wars'];

    public static function fillableKeys (){

        return [
            'IDNO',
            'FATHER_IDNO','MOTHER_IDNO','FULLNAME',
            'FNAME_ARB','SNAME_ARB','TNAME_ARB','LNAME_ARB'  ,
            'PREV_LNAME_ARB','MOTHER_ARB','ENG_NAME'  ,
            'SEX','SOCIAL_STATUS','CI_RELIGION'  ,
            'MOBILE','TEL'  ,
            'BIRTH_PMAIN'  ,'BIRTH_PSUB'  ,'BIRTH_DT'  ,'DETH_DT'  ,'IS_DEAD_DESC'  ,
            'CI_CITY'  ,'CI_REGION'  ,'STREET_ARB'  ,
            'CTZN_STATUS'  ,'CTZN_TRANS_DT'  ,'VISIT_PURPOSE_DESC'  ,
            'photo_url'  ,'photo_update_date'  ,
            'PASSPORT_NO'  ,'PASSPORT_ISSUED_ON'  ,'PASSPORT_TYPE'  ,
            'CARD_NO'  ,'EXP_DATE'  ,
            'INS_STATUS_DESC'  ,'INS_TYPE_DESC'  ,'WORK_SITE_DESC' ,
            'family_cnt', 'spouses', 'male_live   ', 'female_live  ',
            'GOV_NAME','CITY_NAME','PART_NAME ','ADDRESS_DET',
            'social_affairs_status'   ,'AID_CLASS'  ,'AID_TYPE'  ,'AID_AMOUNT'   ,'AID_SOURCE'   ,
            'AID_PERIODIC'   ,'ST_BENEFIT_DATE' ,'END_BENEFIT_DATE' ,'home_address' ,
            'near_mosque' ,'paterfamilias_mobile' ,'telephone' ,'current_career' ,
            'father_death_reason' ,'mother_death_reason' ,
            'building_type' ,'home_type' ,'furniture_type' ,
            'home_status' ,'home_description' ,'total_family_income' ,'health_status' ,
            'wife_mobile' ,'affected_by_wars'];
    }

    public static function saveNew ($input){
/*
        $IDNO = $input->IDNO ;
        $keys = self::fillableKeys();
        $create = [];

        foreach ($input as $key => $value){
            if(in_array($key, $keys)) {
                $create[$key] =$value;
            }
        }
        $main = null ;
        if (sizeof($create) > 0){
            $main = self::CreateOrUpdate($create,$IDNO);

            if ($main){
                if (isset($input->employment)){
                    if (sizeof($input->employment) > 0){
                        CloneGovernmentPersonsWorks::where('IDNO',$IDNO)->delete();
                        foreach ($input->employment as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsWorks::create($dt);
                        }
                    }
                }

                if (isset($input->commercial_data)){
                    if (sizeof($input->commercial_data) > 0){
                        CloneGovernmentPersonsCommercialRecords::where('IDNO',$IDNO)->delete();
                        foreach ($input->commercial_data as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsCommercialRecords::create($dt);
                        }
                    }
                }

                if (isset($input->health)){
                    if (sizeof($input->health) > 0){
                        CloneGovernmentPersonsMedicalReports::where('IDNO',$IDNO)->delete();
                        foreach ($input->health as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsMedicalReports::create($dt);
                        }
                    }
                }

                if (isset($input->travel)){
                    if (sizeof($input->travel) > 0){
                        CloneGovernmentPersonsTravel::where('IDNO',$IDNO)->delete();
                        foreach ($input->travel as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsTravel::create($dt);
                        }
                    }
                }

                if (isset($input->vehicles)){
                    if (sizeof($input->vehicles) > 0){
                        CloneGovernmentPersonsVehicles::where('IDNO',$IDNO)->delete();
                        foreach ($input->vehicles as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsVehicles::create($dt);
                        }
                    }
                }

                if (isset($input->properties)){
                    if (sizeof($input->properties) > 0){
                        CloneGovernmentPersonsProperties::where('IDNO',$IDNO)->delete();
                        foreach ($input->properties as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsProperties::create($dt);
                        }
                    }
                }

                if (isset($input->reg48_license)){
                    if (sizeof($input->reg48_license) > 0){
                        CloneGovernmentPersonsReg48License::where('WORKER_ID',$IDNO)->delete();
                        foreach ($input->reg48_license as $ke => $v_){
                            $v_->WORKER_ID = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsReg48License::create($dt);
                        }
                    }
                }

                if (isset($input->social_affairs_receipt)){
                    if (sizeof($input->social_affairs_receipt) > 0){
                        CloneGovernmentPersonsSocialAffairsReceipt::where('IDNO',$IDNO)->delete();
                        foreach ($input->social_affairs_receipt as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsSocialAffairsReceipt::create($dt);
                        }
                    }
                }

                if (isset($input->marriage)){
                    if (sizeof($input->marriage) > 0){
                        CloneGovernmentPersonsMarriage::where('IDNO',$IDNO)->delete();
                        foreach ($input->marriage as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsMarriage::create($dt);
                        }
                    }
                }

                if (isset($input->divorce)){
                    if (sizeof($input->divorce) > 0){
                        CloneGovernmentPersonsDivorce::where('IDNO',$IDNO)->delete();
                        foreach ($input->divorce as $ke => $v_){
                            $v_->IDNO = $IDNO;
                            $dt = (array)$v_;
                            CloneGovernmentPersonsDivorce::create($dt);
                        }
                    }
                }

                if (isset($input->wives)){
                    if (sizeof($input->wives) > 0){
                        self::saveWives($IDNO , $input->wives , $keys);
                    }
                }

                if (isset($input->parent)){
                    if (sizeof($input->parent) > 0){
                        self::saveParents($IDNO , $input->parent , $keys);
                    }
                }

                if (isset($input->childrens)){
                    if (sizeof($input->childrens) > 0){
                        self::saveChildrens($input->SEX_CD ,$IDNO, $input->childrens , $keys);
                    }
                }


            }
        }

        return $main;
*/
        return ;

    }

    public static function saveNewWithRelation ($IDNO ,$input){

        /*
        $keys = self::fillableKeys();
        $create = [];
//        foreach ($keys as $key){
//            if (isset($input->$key)){
//                $create[$key] =$input->$key;
//            }
//        }

        foreach ($input as $key => $value){
            if(in_array($key, $keys)) {
                $create[$key] =$value;
            }
        }

        $main = null ;

        if (sizeof($create) > 0){
            $main = self::CreateOrUpdate($create,$input->IDNO_RELATIVE);
            if ($main){
                CloneGovernmentPersonsRelations::CreateOrUpdate(['IDNO' => $IDNO ,'IDNO_RELATIVE' => $input->IDNO,
                'RELATIVE' => $input->RELATIVE_DESC,]);
            }
        }

        return $main;

        */

        return ;


    }

    public static function CreateOrUpdate($data,$IDNO){

        $row = self::where('IDNO',$data['IDNO'])->first();
        if(is_null($row)){
            $data['IDNO'] = $IDNO;
            $row = self::create($data);
        } else{
            self::where('IDNO',$data['IDNO'])->update($data);
        }

        return $data;
    }

    public static function saveWives($IDNO,$wives,$keys){

        foreach ($wives as $wife){
            $wifeCreate = ['IDNO' =>  $wife->IDNO_RELATIVE ];
            foreach ($keys as $key){
                if (isset($wife->$key)){
                    $wifeCreate[$key] =$wife->$key;
                }
            }

            self::CreateOrUpdate($wifeCreate,$wife->IDNO_RELATIVE);
            CloneGovernmentPersonsRelations::CreateOrUpdate(['IDNO' => $IDNO ,'IDNO_RELATIVE' => $wife->IDNO_RELATIVE,
                                                 'RELATIVE' => $wife->RELATIVE_DESC,]);

        }

        return null ;
    }

    public static function saveChildrens($SEX_CD,$IDNO,$childrens,$keys){

        foreach ($childrens as $child){
            $create = ['IDNO' =>  $child->IDNO_RELATIVE ];
            foreach ($keys as $key){
                if (isset($child->$key)){
                    $create[$key] =$child->$key;
                }
            }

            if ($SEX_CD == 1){
                $create['FATHER_IDNO'] = $IDNO ;
            }else{
                $create['MOTHER_IDNO'] = $IDNO ;
            }

            self::CreateOrUpdate($create,$child->IDNO_RELATIVE);
            CloneGovernmentPersonsRelations::CreateOrUpdate(['IDNO' => $IDNO ,'IDNO_RELATIVE' => $child->IDNO_RELATIVE,
                                                 'RELATIVE' => $child->RELATIVE_DESC]);

        }

        return null ;
    }

    public static function saveParents($IDNO,$parents,$keys){

        $update = [];
        foreach ($parents as $parent){
            $create = ['IDNO' =>  $parent->IDNO_RELATIVE ];
            foreach ($keys as $key){
                if (isset($parent->$key)){
                    $create[$key] =$parent->$key;
                }
            }

            if ($parent->SEX_CD == 1){
                $update['FATHER_IDNO'] = $parent->IDNO_RELATIVE ;
            }else{
                $update['MOTHER_IDNO'] = $parent->IDNO_RELATIVE ;
            }

            self::CreateOrUpdate($create,$parent->IDNO_RELATIVE);

            CloneGovernmentPersonsRelations::CreateOrUpdate(['IDNO' => $IDNO ,'IDNO_RELATIVE' => $parent->IDNO_RELATIVE,
            'RELATIVE' => $parent->RELATIVE_DESC,]);

        }

        self::where(['IDNO'=>$IDNO ])->update($update);
        return null ;
    }

}
