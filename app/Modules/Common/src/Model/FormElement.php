<?php

namespace Common\Model;

class FormElement extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_forms_elements';
    protected $fillable = ['form_id','label', 'description','type','priority'];
    public $timestamps = false;


    public static function getPriority($target,$category_id)
    {
        $element=self::getTargetSections($target);
        $list=[];

        $category_element = \DB::table('char_categories_form_element_priority')
                            ->join('char_forms_elements As E', function($q) use($element){
                                $q->on('E.id', '=', 'char_categories_form_element_priority.element_id');
                                $q->whereIn('E.name',$element);
                            })
                            ->orderBy('E.name')
                            ->where('char_categories_form_element_priority.category_id','=',$category_id)
                            ->selectRaw("E.name,char_categories_form_element_priority.priority")
                            ->get();

          foreach ($category_element as $item) {
              $list[$item->name]= $item->priority;
          }
            return $list;
    }

    public static function getTargetSections($target)
    {
        $element=array();

        switch ($target) {
            case 'personalInfo' :
                $element=[
                    'name','en_name','card_type','id_card_number','gender','birthday' ,'birth_place','marital_status_id','refugee',
                    'nationality','location','street_address','monthly_income','wataniya','','secondary_mobile',
                    'phone','working','can_work','unrwa_card_number','work_reason_id','work_job_id','work_wage_id','work_status_id',
                    'work_location','banks','condition','disease_id','details','deserted','family_cnt',
                    'actual_monthly_income', 'receivables',  'receivables_sk_amount','is_qualified','qualified_card_number',
                    'has_commercial_records','active_commercial_records', 'gov_commercial_records_details',
                    'has_health_insurance', 'health_insurance_number', 'health_insurance_type',
                    'has_device', 'used_device_name', 'gov_health_details',  'gov_work_details','has_other_work_resources'
                ];

                break;
            case 'residenceData' :
                $element=[ 'property_type_id','rent_value','roof_material_id','residence_condition' ,
                    'indoor_condition','habitable','house_condition','rooms','area','need_repair','repair_notes'];

                break;
            case 'homeIndoorData' :
                $element=[ 'essential_condition'];
                break;
            case 'otherData' :
                $element=[ 'financial_aid_condition','non_financial_aid_condition','properties_condition',
                    'reconstructions_promised','reconstructions_organization_name','case_needs' ];
                break;
            case 'familyStructure' :
                $element=[
                    'name','en_name','card_type','id_card_number','gender','birthday' ,'birth_place','marital_status_id','fm_monthly_income','study',
                    'kinship_id','authority','degree', 'stage','grade','school','fm_working','fm_work_job_id','condition',
                    'disease_id','details','fm_currently_study'
                ];
                break;
            case 'recommendations' :
                $element=[ 'notes','visitor','visited_at','visitor_card', 'visitor_opinion', 'visitor_evaluation'];
                break;
            case 'father':
                $element =['name','en_name','card_type','id_card_number','birthday','working','work_job','work_location','monthly_income','specialization',
                    'study','degree','death_date','death_cause','spouses','father_condition','disease_id','details'];
                break;
            case 'mother':
                $element=['name','en_name','card_type','id_card_number','birthday','working','work_job','work_location','monthly_income','specialization',
                    'study','alive','mother_marital_status','mother_death_date','mother_death_cause','mother_condition','mother_disease',
                    'mother_details','mother_nationality','mother_stage','mother_prev_family_name'];
                break;
            case 'fm-case':
                $element=['name','en_name','card_type','id_card_number','birthday','gender','nationality','birth_place',
                        'health_condition','disease_id','details','quran_center',
                        'prayer','prayer_reason','save_quran','quran_parts','quran_chapters','quran_reason',
                        'banks','type','authority','study',
                        'stage','grade','year','school','points','level','kinship'];
                break;
            case 'fm' :
                $element=['name','en_name','card_type','id_card_number','birthday','study','gender','marital_status','health_condition','fm_working','work_job','kinship','stage'];
                break;
            case 'guardian':
                $element=['WhoIsGuardian','name','en_name','card_type','id_card_number','birthday','working','work_job','work_location','monthly_income',
                    'guardian_kinship','guardian_marital_status','nationality','study',
                    'guardian_stage', 'guardian_address', 'guardian_phone','guardian_primary_mobile','guardian_wataniya', 'guardian_secondary_mobile',
                    'guardian_property_type', 'guardian_roof_material','guardian_residence_condition', 'guardian_indoor_condition',
                    'guardian_rooms', 'guardian_banks'];
                break;
        }
        return $element;
    }



}

