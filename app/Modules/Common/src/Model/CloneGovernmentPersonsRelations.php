<?php

namespace Common\Model;

class CloneGovernmentPersonsRelations  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_relations';
    protected $fillable = ['IDNO','IDNO_RELATIVE','RELATIVE'];
    public $timestamps = false;


    public static function CreateOrUpdate($data){

        $row = self::where(['IDNO'=>$data['IDNO'],'IDNO_RELATIVE'=>$data['IDNO_RELATIVE']])->first();
        if(is_null($row)){

            $row = self::create($data);

        } else{
            self::where(['IDNO'=>$data['IDNO'],'IDNO_RELATIVE'=>$data['IDNO_RELATIVE']])
                ->update(['RELATIVE'=>$data['RELATIVE']]);
        }

        return $row;
    }
}
