<?php

namespace Common\Model;
use App\Http\Helpers;

class SponsorshipsCases extends \Common\Model\AbstractCases
{
    protected $type = AbstractCases::TYPE_SPONSORSHIPS;

    public static function getVisitorNotes($id,$category_id,$organization_id,$gender,$lang){
        $notes=\Common\Model\AbstractVisitorNotes::fetch($category_id,$organization_id,$gender,$lang);
        if($notes){

            $language_id =  \App\Http\Helpers::getLocale();

            $active = trans('common::application.active');
            $inactive = trans('common::application.inactive');

            $yes = trans('common::application.yes');
            $no = trans('common::application.no');

            $id_card = trans('common::application.id_card');
            $id_number = trans('common::application.id_number');
            $passport = trans('common::application.passport');

            $male = trans('common::application.male');
            $female = trans('common::application.female');

            $refugee = trans('common::application.refugee');
            $citizen = trans('common::application.citizen');

            $not_exist = trans('common::application.not_exist');
            $exist = trans('common::application.exist');

            $not_work = trans('common::application.not_work');
            $work = trans('common::application.work');

            $is_alive = trans('common::application.is_alive');
            $deceased = trans('common::application.deceased');

            $vocational = trans('common::application.vocational');
            $academic = trans('common::application.academic');

            $perfect = trans('common::application.perfect');
            $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
            $special_needs = trans('common::application.special_needs');

            $excellent = trans('common::application.excellent');
            $acceptable = trans('common::application.acceptable');

            $good = trans('common::application.good');
            $very_good = trans('common::application.very_good');
            $weak = trans('common::application.weak');
            $very_bad_condition = trans('common::application.very_bad_condition');
            $bad_condition = trans('common::application.bad_condition');

            $the_first  = trans('common::application.the_first') ;
            $the_second = trans('common::application.the_second') ;
            $the_third  = trans('common::application.the_third') ;
            $the_fourth = trans('common::application.the_fourth') ;
            $the_Fifth  = trans('common::application.the_Fifth');
            $the_Sixth  = trans('common::application.the_Sixth');
            $the_Seventh = trans('common::application.the_Seventh');
            $the_Eighth= trans('common::application.the_Eighth');
            $the_Ninth = trans('common::application.the_Ninth');
            $the_tenth = trans('common::application.the_tenth');
            $the_eleventh = trans('common::application.the_eleventh');
            $the_twelveth= trans('common::application.the_twelveth');

            $special_studies= trans('common::application.special_studies');
            $vocational_training= trans('common::application.vocational_training');
            $first_year_of_university= trans('common::application.first_year_of_university');
            $second_year_of_university= trans('common::application.second_year_of_university');
            $third_year_of_university= trans('common::application.third_year_of_university');
            $fourth_year_of_university= trans('common::application.fourth_year_of_university');
            $fifth_year_of_university= trans('common::application.fifth_year_of_university');


            $regularly = trans('common::application.regularly');
            $does_not_pray = trans('common::application.does_not_pray');
            $sometimes = trans('common::application.sometimes');

            $map=['category_name','birth_place','nationality','gender','name','id_card_number','birthday','address','brothers',
                    'phone','primary_mobile','wataniya','secondery_mobile','prayer_status','prayer_reason','quran_reason','quran_parts',
                    'quran_chapters','quran','the_health_status','health_details','health_diseases','account_number','account_owner','bank_name',
                    'branch_name','rooms','area','property_types','roof_materials','residence_condition','indoor_condition',
                    'edu_stages','edu_currently_study','edu_authorities','edu_degrees','edu_year','edu_average','edu_school','education_type','education_level','education_grade',
                    'guardian_name','guardian_birthday','guardian_id_card_number','guardian_birth_place','guardian_nationality','guardian_kinship',
                    'guardian_gender','guardian_address','guardian_phone','guardian_primary_mobile','guardian_wataniya','guardian_secondery_mobile','guardian_rooms',
                    'guardian_area','guardian_property_types','guardian_roof_materials','guardian_residence_condition','guardian_indoor_condition',
                    'guardian_account_number','guardian_account_owner','guardian_bank_name','guardian_branch_name',
                    'father_name','father_id_card_number','father_birthday','father_spouses','father_death_status','father_death_date',
                    'father_death_causes',
                    'mother_name','mother_Work_Location','mother_work_job_id','mother_birthday','mother_id_card_number','mother_monthly_income',
                    'mother_marital_status','mother_working','mother_death_status','mother_death_date','mother_death_causes'];
            $query = \DB::table('char_cases')->addSelect('char_cases.*');
            if (strrpos($notes->note,'category_name')) {
                    $query->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
                        ->addSelect(array(
                            'char_categories.name AS category_name'
                        ));
                }

            if (strrpos($notes->note,'brothers')) {
                $query->selectRaw("CASE WHEN char_get_siblings_count(char_cases.person_id,'both',null) is null THEN 0
                                          Else char_get_siblings_count(char_cases.person_id,'both',null) END AS brothers");
            }

            if (strrpos($notes->note, 'family_count')) {
                $query->selectRaw("char_get_family_count( 'left',char_cases.person_id,null,char_cases.person_id ) as family_count");
            }

            if (strrpos($notes->note,'birth_place') || strrpos($notes->note,'nationality') || strrpos($notes->note,'gender') ||
                    strrpos($notes->note,'name') ||  strrpos($notes->note,'id_card_number') || strrpos($notes->note,'address') ||
                    strrpos($notes->note,'birthday') ||
                    strrpos($notes->note,'birthday') ||
                    strrpos($notes->note,'phone')|| strrpos($notes->note,'wataniya')|| strrpos($notes->note,'primary_mobile')|| strrpos($notes->note,'secondery_mobile') ||
                    strrpos($notes->note,'the_health_status')|| strrpos($notes->note,'health_details')|| strrpos($notes->note,'health_diseases')||
                    strrpos($notes->note,'rooms')|| strrpos($notes->note,'area')|| strrpos($notes->note,'property_types')||
                    strrpos($notes->note,'roof_materials')|| strrpos($notes->note,'residence_condition')|| strrpos($notes->note,'indoor_condition') ||
                    strrpos($notes->note,'mother_name') || strrpos($notes->note,'mother_id_card_number') || strrpos($notes->note,'mother_birthday') ||
                    strrpos($notes->note,'mother_marital_status') ||  strrpos($notes->note,'mother_death_status') || strrpos($notes->note,'mother_death_date') ||
                    strrpos($notes->note,'mother_death_causes') ||  strrpos($notes->note,'mother_working') || strrpos($notes->note,'mother_Work_Location') ||
                    strrpos($notes->note,'mother_work_job_id')||strrpos($notes->note,'mother_monthly_income')||
                    strrpos($notes->note,'father_name') || strrpos($notes->note,'father_id_card_number') || strrpos($notes->note,'father_birthday') ||
                    strrpos($notes->note,'father_spouses') ||  strrpos($notes->note,'father_death_status') || strrpos($notes->note,'father_death_date') ||
                    strrpos($notes->note,'father_death_causes')) {

                    $query->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id');

                    if (strrpos($notes->note, 'name')) {
                        $query->selectRaw("char_persons.first_name,char_persons.second_name,char_persons.third_name,char_persons.last_name,
                                            CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name")
                            ->orderBy('char_persons.first_name')
                            ->orderBy('char_persons.second_name')
                            ->orderBy('char_persons.third_name')
                            ->orderBy('char_persons.last_name');
                    }
                    if (strrpos($notes->note, 'id_card_number')) {
                        $query->addSelect(array('char_persons.id_card_number'));
                    }
                    if (strrpos($notes->note, 'gender')) {
                        $query->selectRaw("CASE
                                                      WHEN char_persons.gender is null THEN '-'
                                                      WHEN char_persons.gender = 1 THEN '$male'
                                                      WHEN char_persons.gender = 2 THEN '$female'
                                                  END
                                                  AS gender");
                    }
                    if (strrpos($notes->note, 'birthday')) {
                        $query->selectRaw("CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday");
                    }
                    if (strrpos($notes->note, 'birth_place')) {
                        $query->join('char_locations_i18n As L', function ($q) {
                            $q->on('L.location_id', '=', 'char_persons.nationality')
                                ->where('L.language_id', 1);
                        })->selectRaw("CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place");
                    }
                    if (strrpos($notes->note, 'nationality')) {
                        $query->join('char_locations_i18n As L0', function ($q) {
                            $q->on('L0.location_id', '=', 'char_persons.nationality')
                                ->where('L0.language_id', 1);
                        })->selectRaw("CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality");
                    }
                    if (strrpos($notes->note, 'address')) {

                        $query
                            ->leftjoin('char_locations_i18n As mL1', function ($q) {
                            $q->on('mL1.location_id', '=', 'char_persons.mosques_id')
                                ->where('mL1.language_id', 1);
                        })->leftjoin('char_locations_i18n As L1', function ($q) {
                            $q->on('L1.location_id', '=', 'char_persons.location_id')
                                ->where('L1.language_id', 1);
                        })
                            ->leftjoin('char_locations_i18n As L2', function ($q) {
                                $q->on('L2.location_id', '=', 'char_persons.city');
                                $q->where('L2.language_id', 1);
                            })
                            ->leftjoin('char_locations_i18n As L3', function ($q) {
                                $q->on('L3.location_id', '=', 'char_persons.governarate');
                                $q->where('L3.language_id', 1);
                            })
                            ->leftjoin('char_locations_i18n As L4', function ($q) {
                                $q->on('L4.location_id', '=', 'char_persons.country');
                                $q->where('L4.language_id', 1);
                            })
                                    ->selectRaw("CASE WHEN L4.name  is null THEN '-' Else L4.name END   AS country,
                                                 CASE WHEN L3.name  is null THEN '-' Else L3.name END   AS governarate,
                                                 CASE WHEN L2.name  is null THEN '-' Else L2.name END   AS city,
                                                 CASE WHEN char_persons.location_id is null THEN '-' Else L1.name END   AS location_id,
                                                 CASE WHEN char_persons.mosques_id is null THEN '-' Else mL1.name END   AS mosque_name,
                                                 CASE WHEN char_persons.street_address is null THEN '-' Else char_persons.street_address END   AS street_address,
                                                 CONCAT(ifnull(L4.name, ' '), ' ' ,ifnull(L3.name, ' '),' ',ifnull(L2.name, ' '),' ', ifnull(L1.name,' '),' ', ifnull(mL1.name,' '),' ', ifnull(char_persons.street_address,' '))
                                                 AS address
                                               ");
                    }
                    if (strrpos($notes->note, 'phone')) {
                        $query->leftjoin('char_persons_contact as cont1', function ($q) {
                            $q->on('cont1.person_id', '=', 'char_persons.id');
                            $q->where('cont1.contact_type', 'phone');
                        })->selectRaw("CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone");
                    }
                    if (strrpos($notes->note, 'wataniya')) {
                        $query->leftjoin('char_persons_contact as cont2_', function ($q) {
                            $q->on('cont2_.person_id', '=', 'char_persons.id');
                            $q->where('cont2_.contact_type', 'wataniya');
                        })->selectRaw("CASE WHEN cont2_.contact_value is null THEN '-' Else cont2_.contact_value END   AS wataniya");
                    }
                    if (strrpos($notes->note, 'primary_mobile')) {
                        $query->leftjoin('char_persons_contact as cont2', function ($q) {
                            $q->on('cont2.person_id', '=', 'char_persons.id');
                            $q->where('cont2.contact_type', 'primary_mobile');
                        })->selectRaw("CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile");
                    }
                    if (strrpos($notes->note, 'secondery_mobile')) {
                        $query->leftjoin('char_persons_contact as cont3', function ($q) {
                            $q->on('cont3.person_id', '=', 'char_persons.id');
                            $q->where('cont3.contact_type', 'secondery_mobile');
                        })->selectRaw("CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondery_mobile");
                    }

                    if (strrpos($notes->note, 'the_health_status') || strrpos($notes->note, 'health_details') || strrpos($notes->note, 'health_diseases')) {
                        $query->leftjoin('char_persons_health', function ($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })->selectRaw("CASE WHEN char_persons_health.condition is null THEN '-'
                                            WHEN char_persons_health.condition = 1 THEN '$perfect'
                                            WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                            WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                      END  AS the_health_status,
                                      CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS health_diseases
                                      ");


                        if (strrpos($notes->note, 'health_diseases')) {
                            $query->leftjoin('char_diseases_i18n', function ($q) {
                                $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                                $q->where('char_diseases_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END  AS  health_diseases");
                        }


                    }
                    if (strrpos($notes->note, 'rooms') || strrpos($notes->note, 'area') || strrpos($notes->note, 'property_types') ||
                        strrpos($notes->note, 'roof_materials') || strrpos($notes->note, 'residence_condition') || strrpos($notes->note, 'indoor_condition')) {

                        $query->leftjoin('char_residence', function ($q) {
                            $q->on('char_residence.person_id', '=', 'char_persons.id');
                        })->selectRaw("CASE WHEN char_residence.area is null THEN '-' Else char_residence.area  END  AS area,
                                               CASE WHEN char_residence.rooms is null THEN '-' Else char_residence.rooms  END  AS rooms
                                           ");

                        if (strrpos($notes->note, 'residence_condition')) {
                            $query  ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                                $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                                $q->where('char_house_status_i18n.language_id',$language_id);
                            })->selectRaw("CASE WHEN char_house_status_i18n.name is null     THEN '-' Else char_house_status_i18n.name  END  AS residence_condition");
                        }

                        if (strrpos($notes->note, 'indoor_condition')) {
                            $query  ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                                $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                                $q->where('char_furniture_status_i18n.language_id',$language_id);
                            })->selectRaw("CASE WHEN char_furniture_status_i18n.name is null THEN '-' Else char_furniture_status_i18n.name  END  AS indoor_condition");
                        }

                        if (strrpos($notes->note, 'roof_materials')) {
                            $query->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                                $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                                $q->where('char_roof_materials_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN char_roof_materials_i18n.name is null THEN '-' Else char_roof_materials_i18n.name END   AS roof_materials");
                        }

                        if (strrpos($notes->note, 'property_types')) {
                            $query->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                                $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                                $q->where('char_property_types_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN char_property_types_i18n.name is null THEN '-' Else char_property_types_i18n.name END   AS property_types");
                        }
                    }
                    if (strrpos($notes->note, 'prayer_status') || strrpos($notes->note, 'prayer_reason') || strrpos($notes->note, 'quran_reason') ||
                        strrpos($notes->note, 'quran_center') || strrpos($notes->note, 'quran_parts')|| strrpos($notes->note, 'quran_chapters')|| strrpos($notes->note, 'quran')) {

                        $query->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        });

                        if (strrpos($notes->note, 'prayer_status')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                                     WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                     WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                     WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                               END   AS prayer");
                        }

                        if (strrpos($notes->note, 'prayer_reason')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason");
                        }

                        if (strrpos($notes->note, 'quran_reason')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason");
                        }

                        if (strrpos($notes->note, 'quran_center')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.quran_center is null THEN '-'  Else  char_persons_islamic_commitment.quran_center END AS quran_center");
                        }

                        if (strrpos($notes->note, 'quran_parts')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts");
                        }

                        if (strrpos($notes->note, 'quran_chapters')) {
                            $query ->selectRaw("CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters");
                        }

                        if (strrpos($notes->note, 'quran')) {
                            $query ->selectRaw(" CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                                      WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                      Else '$yes'
                                                 END  AS save_quran");
                        }


                    }
                    if (strrpos($notes->note, 'account_number') || strrpos($notes->note, 'account_owner') || strrpos($notes->note, 'bank_name') || strrpos($notes->note, 'branch_name')) {

                        $query->leftjoin('char_organization_persons_banks', function($q) {
                            $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                            $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                        })->leftjoin('char_persons_banks', function($q) {
                            $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                        });

                        if (strrpos($notes->note, 'account_number')) {
                            $query ->selectRaw("CASE WHEN char_persons_banks.account_number is null THEN '-' Else char_persons_banks.account_number END AS account_number");
                        }

                        if (strrpos($notes->note, 'account_owner')) {
                            $query ->selectRaw("CASE WHEN char_persons_banks.account_owner is null THEN '-' Else char_persons_banks.account_owner END AS account_owner");
                        }

                        if (strrpos($notes->note, 'bank_name')) {
                            $query->leftjoin('char_banks', function($q) {
                                $q->on('char_banks.id', '=', 'char_persons_banks.bank_id');
                            })->selectRaw("CASE WHEN char_banks.name is null THEN '-' Else char_banks.name END AS bank_name");
                        }

                        if (strrpos($notes->note, 'branch_name')) {
                            $query->leftjoin('char_branches', function($q) {
                                $q->on('char_branches.id', '=', 'char_persons_banks.branch_name');
                            })->selectRaw("CASE WHEN char_branches.name is null THEN '-' Else char_branches.name END AS branch_name");
                        }

                    }

                    if (strrpos($notes->note, 'edu_stages') ||strrpos($notes->note, 'edu_currently_study') || strrpos($notes->note, 'edu_authorities') || strrpos($notes->note, 'edu_degrees') ||
                        strrpos($notes->note, 'edu_year')|| strrpos($notes->note, 'edu_average')|| strrpos($notes->note, 'edu_school')||
                        strrpos($notes->note, 'education_type') || strrpos($notes->note, 'education_level') || strrpos($notes->note, 'education_grade')) {


                        $query->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        });


                        if (strrpos($notes->note, 'edu_stages')) {
                            $query->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                                $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                                $q->where('char_edu_stages.language_id',$language_id);
                            })->selectRaw("CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as edu_stages");
                        }

                        if (strrpos($notes->note, 'edu_authorities')) {
                            $query->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                                $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                                $q->where('char_edu_authorities.language_id',$language_id);
                            })->selectRaw("CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END edu_authorities");
                        }

                        if (strrpos($notes->note, 'edu_degrees')) {
                            $query->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                                $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                                $q->where('char_edu_degrees.language_id',$language_id);
                            }) ->selectRaw("CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as edu_degrees");
                        }

                        if (strrpos($notes->note, 'edu_study')) {
                            $query ->selectRaw("CASE WHEN char_persons_education.study is null THEN '-'
                                                                 WHEN char_persons_education.study = 1 THEN '$yes'
                                                                 WHEN char_persons_education.study = 0 THEN '$no'
                                                            END
                                                            AS edu_study");
                        }

                        if (strrpos($notes->note, 'edu_currently_study')) {
                            $query ->selectRaw("CASE WHEN char_persons_education.currently_study is null THEN '-'
                                                                 WHEN char_persons_education.currently_study = 1 THEN '$yes'
                                                                 WHEN char_persons_education.currently_study = 0 THEN '$no'
                                                            END
                                                            AS edu_currently_study");
                        }

                        if (strrpos($notes->note, 'edu_year')) {
                            $query ->selectRaw("CASE WHEN char_persons_education.year is null THEN '-' Else char_persons_education.year END as edu_year");
                        }

                        if (strrpos($notes->note, 'edu_average')) {
                            $query ->selectRaw("CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS edu_average");
                        }

                        if (strrpos($notes->note, 'edu_school')) {
                            $query ->selectRaw("CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END as edu_school");
                        }

                        if (strrpos($notes->note, 'education_type')) {
                            $query ->selectRaw("CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                            END
                                                            AS education_type");
                        }

                        if (strrpos($notes->note, 'education_level')) {
                            $query ->selectRaw("CASE
                                                               WHEN char_persons_education.level is null THEN '-'
                                                                WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                            END
                                                            AS education_level");
                        }

                        if (strrpos($notes->note, 'education_grade')) {
                            $query ->selectRaw("CASE
                                                               WHEN char_persons_education.grade is null THEN '-'
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                             END
                                                             AS education_grade");
                        }



                    }

                    if (strrpos($notes->note,'father_name') || strrpos($notes->note,'father_id_card_number') || strrpos($notes->note,'father_birthday') ||
                        strrpos($notes->note,'father_spouses') ||  strrpos($notes->note,'father_death_status') || strrpos($notes->note,'father_death_date') ||
                        strrpos($notes->note,'father_death_causes') ) {
                        $query->leftjoin('char_persons AS f','f.id','=','char_persons.father_id');

                        if (strrpos($notes->note, 'father_name')) {
                            $query->selectRaw("f.first_name AS father_first_name,f.second_name AS father_second_name,
                                                   f.third_name AS father_third_name,f.last_name AS father_last_name,
                                                   CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' ')) AS father_name");
                        }
                        if (strrpos($notes->note, 'father_id_card_number')) {
                            $query->addSelect(array('f.id_card_number AS father_id_card_number'));
                        }
                        if (strrpos($notes->note, 'father_birthday')) {
                            $query->selectRaw("CASE WHEN f.birthday is null THEN '-' Else DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday");
                        }
                        if (strrpos($notes->note, 'father_spouses')) {
                            $query->selectRaw("CASE WHEN f.spouses is null THEN '-' Else f.spouses  END  AS father_spouses");
                        }
                        if (strrpos($notes->note, 'father_death_date')) {
                            $query->selectRaw("CASE WHEN f.death_date is null THEN '-'  Else f.death_date END AS father_death_date");
                        }
                        if (strrpos($notes->note, 'father_death_status')) {
                            $query->selectRaw("CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_death_status");
                        }
                        if (strrpos($notes->note, 'father_death_causes')) {
                            $query->leftjoin('char_death_causes_i18n AS father_death_cause', function($q) use ($language_id){
                                $q->on('father_death_cause.death_cause_id', '=', 'f.death_cause_id');
                                $q->where('father_death_cause.language_id',$language_id);
                            })->selectRaw("CASE WHEN f.death_date is null THEN '-'  Else father_death_cause.name END AS father_death_causes");
                        }
                    }

                    if (strrpos($notes->note,'mother_name') || strrpos($notes->note,'mother_id_card_number') || strrpos($notes->note,'mother_birthday') ||
                        strrpos($notes->note,'mother_marital_status') ||  strrpos($notes->note,'mother_death_status') || strrpos($notes->note,'mother_death_date') ||
                        strrpos($notes->note,'mother_death_causes') ||  strrpos($notes->note,'mother_working') || strrpos($notes->note,'mother_Work_Location') ||
                        strrpos($notes->note,'mother_work_job_id')||strrpos($notes->note,'mother_monthly_income') ){
                        $query->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id');

                        if (strrpos($notes->note, 'mother_name')) {
                            $query->selectRaw("m.first_name AS mother_first_name,m.second_name AS mother_second_name,
                                               m.third_name AS mother_third_name,m.last_name AS mother_last_name,
                                               CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' '))
                                               AS mother_name");
                        }
                        if (strrpos($notes->note, 'mother_id_card_number')) {
                            $query->addSelect(array('m.id_card_number AS mother_id_card_number'));
                        }
                        if (strrpos($notes->note, 'mother_birthday')) {
                            $query->selectRaw("CASE WHEN m.birthday is null THEN '-' Else DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday");
                        }
                        if (strrpos($notes->note, 'mother_marital_status')) {
                            $query->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id){
                                $q->on('char_marital_status.marital_status_id', '=', 'm.marital_status_id');
                                $q->where('char_marital_status.language_id',$language_id);
                            })
                                ->selectRaw("CASE WHEN m.marital_status_id is null THEN '-'  Else char_marital_status.name END AS mother_marital_status");
                        }
                        if (strrpos($notes->note, 'mother_death_date')) {
                            $query->selectRaw("CASE WHEN m.death_date is null THEN '-'  Else m.death_date END AS mother_death_date");
                        }
                        if (strrpos($notes->note, 'mother_death_status')) {
                            $query->selectRaw("CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_death_status");
                        }
                        if (strrpos($notes->note, 'mother_death_causes')) {
                            $query->leftjoin('char_death_causes_i18n AS mother_death_cause', function($q) use ($language_id){
                                $q->on('mother_death_cause.death_cause_id', '=', 'm.death_cause_id');
                                $q->where('mother_death_cause.language_id',$language_id);
                            })->selectRaw("CASE WHEN m.death_date is null THEN '-'  Else mother_death_cause.name END AS mother_death_causes");
                        }

                    }


                }

                if (strrpos($notes->note,'guardian_name') || strrpos($notes->note,'guardian_id_card_number') || strrpos($notes->note,'guardian_birthday') ||
                    strrpos($notes->note,'guardian_address') ||  strrpos($notes->note,'guardian_wataniya') ||strrpos($notes->note,'guardian_phone') || strrpos($notes->note,'guardian_primary_mobile') ||
                    strrpos($notes->note,'guardian_secondery_mobile') ||  strrpos($notes->note,'guardian_rooms') || strrpos($notes->note,'guardian_area') ||
                    strrpos($notes->note,'guardian_property_types')||strrpos($notes->note,'guardian_roof_materials')||strrpos($notes->note,'guardian_residence_condition') ||
                    strrpos($notes->note,'guardian_indoor_condition')||strrpos($notes->note,'guardian_account_number')||strrpos($notes->note,'guardian_account_owner') ||
                    strrpos($notes->note,'guardian_bank_name')||strrpos($notes->note,'guardian_branch_name')) {


                    $query->leftjoin('char_guardians', function($q)  {
                        $q->on('char_guardians.individual_id','=','char_cases.person_id')
                            ->on('char_guardians.organization_id','=','char_cases.organization_id')
                            ->where('char_guardians.status','=',1);
                    })->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id');


                    if (strrpos($notes->note, 'guardian_name')) {
                        $query->selectRaw("g.first_name AS guardian_first_name,g.second_name AS guardian_second_name,
                                                   g.third_name AS guardian_third_name,g.last_name AS guardian_last_name,
                                                   CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name");
                    }
                    if (strrpos($notes->note, 'guardian_id_card_number')) {
                        $query->addSelect(array('g.id_card_number AS guardian_id_card_number'));
                    }
                    if (strrpos($notes->note, 'guardian_birthday')) {
                        $query->selectRaw("CASE WHEN g.birthday is null THEN '-' Else DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS guardian_birthday");
                    }
                    if (strrpos($notes->note, 'guardian_birth_place')) {
                        $query->join('char_locations_i18n As gL', function ($q) {
                            $q->on('gL.location_id', '=', 'g.nationality')
                                ->where('gL.language_id', 1);
                        })->selectRaw("CASE WHEN g.birth_place is null THEN '-' Else gL.name END AS guardian_birth_place");
                    }
                    if (strrpos($notes->note, 'guardian_nationality')) {
                        $query->join('char_locations_i18n As gL0', function ($q) {
                            $q->on('gL0.location_id', '=', 'g.nationality')
                                ->where('gL0.language_id', 1);
                        })->selectRaw("CASE WHEN g.nationality is null THEN '-' Else gL0.name END AS guardian_nationality");
                    }
                    if (strrpos($notes->note, 'guardian_address')) {

                        $query->leftjoin('char_locations_i18n As gmL1', function ($q) {
                            $q->on('gmL1.location_id', '=', 'g.mosques_id')
                                ->where('gmL1.language_id', 1);
                        })->leftjoin('char_locations_i18n As gL1', function ($q) {
                            $q->on('gL1.location_id', '=', 'g.location_id')
                                ->where('gL1.language_id', 1);
                        })
                            ->leftjoin('char_locations_i18n As gL2', function ($q) {
                                $q->on('gL2.location_id', '=', 'g.city');
                                $q->where('gL2.language_id', 1);
                            })
                            ->leftjoin('char_locations_i18n As gL3', function ($q) {
                                $q->on('gL3.location_id', '=', 'g.governarate');
                                $q->where('gL3.language_id', 1);
                            })
                            ->leftjoin('char_locations_i18n As gL4', function ($q) {
                                $q->on('gL4.location_id', '=', 'g.country');
                                $q->where('gL4.language_id', 1);
                            })
                            ->selectRaw("CASE WHEN gL4.name  is null THEN '-' Else gL4.name END   AS guardian_country,
                                         CASE WHEN gL3.name  is null THEN '-' Else gL3.name END   AS guardian_governarate,
                                         CASE WHEN gL2.name  is null THEN '-' Else gL2.name END   AS guardian_city,
                                         CASE WHEN g.location_id is null THEN '-' Else gL1.name END   AS guardian_location_id,
                                         CASE WHEN g.mosques_id is null THEN '-' Else gmL1.name END   AS guardian_mosque_id,
                                         CASE WHEN g.street_address is null THEN '-' Else g.street_address END   AS guardian_street_address,
                                         CONCAT(ifnull(gL4.name, ' '), ' ' ,ifnull(gL3.name, ' '),' ',ifnull(gL2.name, ' '),' ',
                                                ifnull(gL1.name,' '),' ', ifnull(gmL1.name,' '),' ', ifnull(g.street_address,' '))
                                        AS guardian_address ");
                    }
                    if (strrpos($notes->note, 'guardian_phone')) {
                        $query->leftjoin('char_persons_contact as gcont1', function ($q) {
                            $q->on('gcont1.person_id', '=', 'g.id');
                            $q->where('gcont1.contact_type', 'phone');
                        })->selectRaw("CASE WHEN gcont1.contact_value is null THEN '-' Else gcont1.contact_value END   AS guardian_phone");
                    }
                    if (strrpos($notes->note, 'guardian_primary_mobile')) {
                        $query->leftjoin('char_persons_contact as gcont2', function ($q) {
                            $q->on('gcont2.person_id', '=', 'g.id');
                            $q->where('gcont2.contact_type', 'primary_mobile');
                        })->selectRaw("CASE WHEN gcont2.contact_value is null THEN '-' Else gcont2.contact_value END   AS guardian_primary_mobile");
                    }
                    if (strrpos($notes->note, 'guardian_wataniya')) {
                        $query->leftjoin('char_persons_contact as gcont2_', function ($q) {
                            $q->on('gcont2_.person_id', '=', 'g.id');
                            $q->where('gcont2_.contact_type', 'wataniya');
                        })->selectRaw("CASE WHEN gcont2_.contact_value is null THEN '-' Else gcont2_.contact_value END   AS guardian_wataniya");
                    }
                    if (strrpos($notes->note, 'guardian_secondery_mobile')) {
                        $query->leftjoin('char_persons_contact as gcont3', function ($q) {
                            $q->on('gcont3.person_id', '=', 'char_persons.id');
                            $q->where('gcont3.contact_type', 'secondery_mobile');
                        })->selectRaw("CASE WHEN gcont3.contact_value is null THEN '-' Else gcont3.contact_value END   AS guardian_secondery_mobile");
                    }



                    if (strrpos($notes->note, 'guardian_rooms') || strrpos($notes->note, 'guardian_area') || strrpos($notes->note, 'guardian_property_types') ||
                        strrpos($notes->note, 'guardian_roof_materials') || strrpos($notes->note, 'guardian_residence_condition') ||
                        strrpos($notes->note, 'guardian_indoor_condition')) {

                        $query->leftjoin('char_residence as guardian_residence', function ($q) {
                            $q->on('guardian_residence.person_id', '=', 'char_persons.id');
                        });

                        if (strrpos($notes->note,'guardian_rooms')) {
                            $query->selectRaw("CASE WHEN guardian_residence.rooms is null THEN '-' Else guardian_residence.rooms  END  AS guardian_rooms");
                        }
                        if (strrpos($notes->note,'guardian_area')) {
                            $query->selectRaw("CASE WHEN guardian_residence.area is null THEN '-' Else guardian_residence.area  END  AS guardian_area");
                        }

                        if (strrpos($notes->note, 'guardian_residence_condition')) {
                            $query  ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                                $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                                $q->where('guardian_house_status_i18n.language_id',$language_id);
                            })->selectRaw("CASE WHEN guardian_house_status_i18n.name is null     THEN '-' Else guardian_house_status_i18n.name  END  AS residence_condition");
                        }

                        if (strrpos($notes->note, 'guardian_indoor_condition')) {
                            $query  ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                                $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                                $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                            })->selectRaw("CASE WHEN guardian_furniture_status_i18n.name is null THEN '-' Else guardian_furniture_status_i18n.name  END  AS indoor_condition");
                        }

                        if (strrpos($notes->note, 'guardian_roof_materials')) {
                            $query->leftjoin('char_roof_materials_i18n as guardian_roof_materials_i18n', function ($q) {
                                $q->on('guardian_roof_materials_i18n.roof_material_id', '=', 'guardian_residence.roof_material_id');
                                $q->where('guardian_roof_materials_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN guardian_roof_materials_i18n.name is null THEN '-' Else guardian_roof_materials_i18n.name END   AS guardian_roof_materials");
                        }
                        if (strrpos($notes->note, 'guardian_property_types')) {
                            $query->leftjoin('char_property_types_i18n as guardian_property_types_i18n', function ($q) {
                                $q->on('guardian_property_types_i18n.property_type_id', '=', 'guardian_residence.property_type_id');
                                $q->where('guardian_property_types_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN guardian_property_types_i18n.name is null THEN '-' Else guardian_property_types_i18n.name END   AS guardian_property_types");
                        }
                    }

                    if (strrpos($notes->note, 'guardian_account_number') || strrpos($notes->note, 'guardian_account_owner') || strrpos($notes->note, 'guardian_bank_name') || strrpos($notes->note, 'guardian_branch_name')) {

                        $query->leftjoin('char_organization_persons_banks as guardian_organization_banks', function($q) {
                            $q->on('guardian_organization_banks.person_id', '=', 'g.id');
                            $q->on('guardian_organization_banks.organization_id', '=', 'char_cases.organization_id');
                        })->leftjoin('char_persons_banks as guardian_persons_banks', function($q) {
                            $q->on('guardian_persons_banks.id', '=', 'guardian_organization_banks.bank_id');
                        });

                        if (strrpos($notes->note, 'guardian_account_number')) {
                            $query ->selectRaw("CASE WHEN guardian_persons_banks.account_number is null THEN '-' Else guardian_persons_banks.account_number END AS guardian_account_number");
                        }

                        if (strrpos($notes->note, 'guardian_account_owner')) {
                            $query ->selectRaw("CASE WHEN guardian_persons_banks.account_owner is null THEN '-' Else guardian_persons_banks.account_owner END AS guardian_account_owner");
                        }

                        if (strrpos($notes->note, 'guardian_bank_name')) {
                            $query->leftjoin('char_banks as guardian_banks', function($q) {
                                $q->on('guardian_banks.id', '=', 'guardian_persons_banks.bank_id');
                            })->selectRaw("CASE WHEN guardian_banks.name is null THEN '-' Else guardian_banks.name END AS guardian_bank_name");
                        }

                        if (strrpos($notes->note, 'guardian_branch_name')) {
                            $query->leftjoin('char_branches as guardian_branches', function($q) {
                                $q->on('guardian_branches.id', '=', 'guardian_persons_banks.branch_name');
                            })->selectRaw("CASE WHEN guardian_branches.name is null THEN '-' Else guardian_branches.name END AS guardian_branch_name");
                        }

                    }
                }

            $case=$query->where('char_cases.id', '=', $id)->first();
            $case=(object)$case;
            $final=$notes->note;

            foreach($map as $k =>$v){
                if(property_exists((object)$case, $v)){
                    $final = str_replace(' '.$v.' ', $case->$v, $final);
                }
            }

            return ['status' => true,'note'=>$final,'case' =>$case];
        }
        return ['status' => false];
    }

    public static function getFamilies($page,$organization_id,$filter){

        $first =date('Y-01-01');
        $last = date('Y-m-d', strtotime('12/31'));
        $all_organization=null;


        $father=['father_id_card_number','father_last_name','father_second_name','father_third_name'];
        $guardian=['guardian_id_card_number','guardian_last_name','guardian_second_name','guardian_third_name'];
        $mother=['mother_id_card_number','mother_last_name','mother_second_name','mother_third_name'];

        $condition=[];
        foreach ($filter as $key => $value) {

            if(in_array($key, $father)) {
                if ( $value != "" ) {
                    $data = ['f.' . substr($key,7) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian)) {
                if ( $value != "" ) {
                    $data = ['g.' . substr($key,9) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother)) {
                if ( $value != "" ) {
                    $data = ['m.' . substr($key,7) => $value];
                    array_push($condition, $data);
                }
            }


        }

        $families =\DB::table('char_cases')
            ->join('char_categories as ca', function($q)  {
                $q->on('ca.id',  '=', 'char_cases.category_id');
                $q->where('ca.type','=',1);
            })
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
//            ->leftjoin('char_persons AS f','f.id','=','c.father_id')
            ->join('char_persons AS f','f.id','=','c.father_id')
            ->leftjoin('char_persons AS m','m.id','=','c.mother_id')
            ->leftjoin('char_guardians', function($q)  {
                $q->on('char_guardians.individual_id','=','char_cases.person_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id');


        $all_organization = null;
        if (isset($filter['all_organization'])) {
            if ($filter['all_organization'] === true || $filter['all_organization'] === 'true') {

                $organizations = [];
                if (isset($filter['organization_ids']) && $filter['organization_ids'] != null && $filter['organization_ids'] != "" &&
                    $filter['organization_id'] != [] && sizeof($filter['organization_ids'])!= 0
                ) {

                    if ($filter['organization_ids'][0] == "") {
                        unset($filter['organization_ids'][0]);
                    }
                    $organizations = $filter['organization_ids'];
                }

                if (!empty($organizations)) {
                    $families->wherein('char_cases.organization_id', $organizations);
                } else {

                    $user = \Auth::user();
                    $UserType=$user->type;

                    if($UserType == 2) {
                        $families->where(function ($anq) use ($organization_id,$user) {
                            $anq->where('char_cases.organization_id',$organization_id);
                            $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                                $decq->select('organization_id')
                                    ->from('char_user_organizations')
                                    ->where('user_id', '=', $user->id);
                            });
                        });

                    }else{

                        $families->where(function ($anq) use ($organization_id) {
                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                            $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                                $decq->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                        });
                    }
                }

            }
            elseif ($filter['all_organization'] == false) {
                $families->where('char_cases.organization_id',$organization_id);
            }

        }else{
            $families->where('char_cases.organization_id',$organization_id);
        }

      if (count($condition) != 0) {
            $families =  $families
                ->where(function ($q) use ($condition) {
                    $names = ['f.first_name', 'f.second_name', 'f.third_name', 'f.last_name',
                        'm.first_name', 'm.second_name', 'm.third_name', 'm.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }


        $max_children_number=null;
        $min_children_number=null;

        if(isset($filter['max_children_number']) && $filter['max_children_number'] !=null && $filter['max_children_number'] !=""){
            $max_children_number=$filter['max_children_number'];
        }

        if(isset($filter['min_children_number']) && $filter['min_children_number'] !=null && $filter['min_children_number'] !=""){
            $min_children_number=$filter['min_children_number'];
        }

        if($min_children_number != null && $max_children_number != null) {
            $families->whereRaw(' ca.case_is = 1 and ca.family_structure= 2 and ( char_get_family_count ( "left",c.father_id,null,c.id ) between ? and  ?)', array($min_children_number, $max_children_number));
            $families->orwhereRaw(' ca.case_is = 1 and ca.family_structure=1 and ( char_get_family_count ( "left",c.id,null,c.id ) between ? and  ?)', array($min_children_number, $max_children_number));
            $families->orwhereRaw(' ca.case_is = 2 and ca.family_structure= 2 and ( char_get_family_count ( "left",char_guardians.guardian_id,null,c.id ) between ? and  ?)', array($min_children_number, $max_children_number));
        }elseif($max_children_number != null && $min_children_number == null) {
            $families->whereRaw(" ca.case_is = 1 and ca.family_structure= 2 and ( $max_children_number >= char_get_family_count('left',c.father_id,null,c.id))");
            $families->orwhereRaw(" ca.case_is = 1 and ca.family_structure=1 and ( $max_children_number >= char_get_family_count('left',c.id,null,c.id ))");
            $families->orwhereRaw(" ca.case_is = 2 and ca.family_structure= 2 and ( $max_children_number >= char_get_family_count('left',char_guardians.guardian_id,null,c.id ))");

        }elseif($max_children_number == null && $min_children_number != null) {
            $families->whereRaw(" ca.case_is = 1 and ca.family_structure= 2 and ( $min_children_number <= char_get_family_count('left',c.father_id,null,c.id ))");
            $families->orwhereRaw(" ca.case_is = 1 and ca.family_structure=1 and ( $min_children_number <= char_get_family_count('left',c.id,null,c.id ))");
            $families->orwhereRaw(" ca.case_is = 2 and ca.family_structure= 2 and ( $min_children_number <= char_get_family_count('left',char_guardians.guardian_id,null,c.id ))");
        }

        $category_id=null;
        if(isset($filter['category_id']) && $filter['category_id'] !=null && $filter['category_id'] !=""){
            $category_id=$filter['category_id'];
        }

        $max_total_amount_of_payments=null;
        $min_total_amount_of_payments=null;

        if(isset($filter['max_total_amount_of_payments']) && $filter['max_total_amount_of_payments'] !=null && $filter['max_total_amount_of_payments'] !=""){
            $max_total_amount_of_payments=$filter['max_total_amount_of_payments'];
        }

        if(isset($filter['min_total_amount_of_payments']) && $filter['min_total_amount_of_payments'] !=null && $filter['min_total_amount_of_payments'] !=""){
            $min_total_amount_of_payments=$filter['min_total_amount_of_payments'];
        }

        $max_total_this_year=null;
        $min_total_this_year=null;

        if(isset($filter['max_total_this_year']) && $filter['max_total_this_year'] !=null && $filter['max_total_this_year'] !=""){
            $max_total_this_year=$filter['max_total_this_year'];
        }

        if(isset($filter['min_total_this_year']) && $filter['min_total_this_year'] !=null && $filter['min_total_this_year'] !=""){
            $min_total_this_year=$filter['min_total_this_year'];
        }

        $exceed=null;

        if($category_id ==null){

            if($min_total_this_year != null && $max_total_this_year != null) {
                $families = $families->whereRaw(" ( char_get_family_total_category_payments ( f.id ,'$first','$last',Null) between ? and  ?)", array($min_total_this_year, $max_total_this_year));
            }elseif($max_total_this_year != null && $min_total_this_year == null) {
                $families = $families->whereRaw(" $max_total_this_year >= char_get_family_total_category_payments ( f.id ,'$first','$last',Null)");
            }elseif($max_total_this_year == null && $min_total_this_year != null) {
                $families = $families->whereRaw(" $min_total_this_year <= char_get_family_total_category_payments ( f.id ,'$first','$last',Null)");
            }

            if($min_total_this_year != null && $max_total_amount_of_payments != null) {
                $families = $families->whereRaw(" ( char_get_family_total_category_payments ( f.id ,Null,Null,Null) between ? and  ?)", array($min_total_amount_of_payments, $max_total_amount_of_payments));
            }elseif($max_total_amount_of_payments != null && $min_total_amount_of_payments == null) {
                $families = $families->whereRaw(" $max_total_amount_of_payments >= char_get_family_total_category_payments ( f.id ,Null,Null,Null)");
            }elseif($max_total_amount_of_payments == null && $min_total_amount_of_payments != null) {
                $families = $families->whereRaw(" $min_total_amount_of_payments <= char_get_family_total_category_payments ( f.id ,Null,Null,Null)");
            }


            /*
CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',
                    ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS person_name,
                                                       c.id_card_number as id_card_number,
                                                       c.id as p_id,                                                       
                                                    
             *              */
            $families=  $families->groupBy('f.id')
                ->selectRaw("f.id as id,
                                                    CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' ')) AS father,
                                                    f.id_card_number as father_id_card_number,
                                                   CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' ')) AS mother,
                                                   CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian,
                                                    CASE WHEN char_get_family_total_category_payments ( f.id ,Null,Null,Null) is null
                                                       THEN 0
                                                       Else char_get_family_total_category_payments ( f.id ,Null,Null,Null)
                                                    END
                                                    AS total,
                                                    CASE WHEN char_get_family_total_category_payments ( f.id ,'$first','$last',Null) is null
                                                       THEN 0
                                                       Else char_get_family_total_category_payments ( f.id ,'$first','$last',Null)
                                                    END
                                                    AS totalIn,
                                                      CASE WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN char_get_family_count ( 'left',c.father_id,null,c.id )
                                                           WHEN (ca.case_is = 1 and ca.family_structure=1) THEN char_get_family_count ( 'left',c.id,null,c.id )
                                                           WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,c.id )
                                                     END AS family_count");
        }else{

            if($min_total_this_year != null && $max_total_this_year != null) {
                $families = $families->whereRaw(" ( char_get_family_total_category_payments ( f.id ,'$first','$last','$category_id') between ? and  ?)", array($min_total_this_year, $max_total_this_year));
            }elseif($max_total_this_year != null && $min_total_this_year == null) {
                $families = $families->whereRaw(" $max_total_this_year >= char_get_family_total_category_payments ( f.id ,'$first','$last','$category_id')");
            }elseif($max_total_this_year == null && $min_total_this_year != null) {
                $families = $families->whereRaw(" $max_total_this_year <= char_get_family_total_category_payments ( f.id ,'$first','$last','$category_id')");
            }

            if($min_total_amount_of_payments != null && $max_total_amount_of_payments != null) {
                $families = $families->whereRaw(" ( char_get_family_total_category_payments ( f.id ,Null,Null,'$category_id') between ? and  ?)", array($min_total_amount_of_payments, $max_total_amount_of_payments));
            }elseif($max_total_amount_of_payments != null && $min_total_amount_of_payments == null) {
                $families = $families->whereRaw(" $max_total_amount_of_payments >= char_get_family_total_category_payments ( f.id ,Null,Null,'$category_id')");
            }elseif($max_total_amount_of_payments == null && $min_total_amount_of_payments != null) {
                $families = $families->whereRaw(" $min_total_amount_of_payments <= char_get_family_total_category_payments ( f.id ,Null,Null,'$category_id')");
            }

            /*
CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',
                    ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS person_name,
                                                       c.id_card_number as id_card_number,             */
            $families= $families->groupBy('f.id')
                ->selectRaw("
                                                       f.id as father_id,
                                                       m.id as mother_id,
                                                       g.id as guardian_id,
                                                       CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' ')) AS father,
                                                       f.id_card_number as father_id_card_number,
                                                       CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' ')) AS mother,
                                                       CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian,
                                                        CASE WHEN char_get_family_total_category_payments ( f.id ,Null,Null,'$category_id') is null
                                                           THEN 0
                                                           Else char_get_family_total_category_payments ( f.id ,Null,Null,'$category_id')
                                                        END
                                                        AS total,
                                                        CASE WHEN char_get_family_total_category_payments ( f.id ,'$first','$last','$category_id') is null
                                                           THEN 0
                                                           Else char_get_family_total_category_payments ( f.id ,'$first','$last','$category_id')
                                                        END
                                                        AS totalIn,
                                                        CASE WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN char_get_family_count ( 'left',c.father_id,null,c.id )
                                                             WHEN (ca.case_is = 1 and ca.family_structure=1) THEN char_get_family_count ( 'left',c.id,null,c.id )
                                                             WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,c.id )
                                                        END AS family_count
                                      ");
        }

        if($filter['action'] =="filters"){
            $itemsCount = isset($filter['itemsCount'])? $filter['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$families->count());
            $families=$families->paginate($records_per_page);
            return $families;
        }else{
            return $families->get();
        }

    }

}

