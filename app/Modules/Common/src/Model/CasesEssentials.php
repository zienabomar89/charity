<?php

namespace Common\Model;

class CasesEssentials  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_cases_essentials';
    protected $fillable = [ 'case_id','essential_id','condition','needs'];
    protected $primaryKey='case_id';
    public $timestamps = false;
    public function Essential()
    {
        return $this->belongsToMany('Setting\Model\Essential','essential_id','id');
    }

    public static function saveEssentials($id,$items,$priority){

        \Common\Model\CasesEssentials::where('case_id',$id)->delete();

        if($priority != 1){
            $inputs= \Common\Model\CasesEssentials::getItems($id,$items);
                     \Common\Model\CasesEssentials::where('case_id',$id)->delete();

            if(sizeof($inputs) != 0){
                \Common\Model\CasesEssentials::insert($inputs);
                return array('status' => true,'inserted' =>true);

            }
            else{
                if($priority == 4){
                    return array('status'=>false,'msg' => trans('aid::application.please enter home indoor status'));
                }
            }
        }

        return array('status' => true);
    }

    public static function getItems($id,$items){
        $input = [];

        $item = json_decode(json_encode($items));
        $records = compact('item');

        foreach ($records['item'] as $record) {
            if($record != null){
                if(isset($record->condition) && $record->condition != null && $record->condition !=""){
                    $sub = ['essential_id'=> $record->essential_id ,
                        'condition'=> $record->condition ,
                        'case_id'=> $id ,
                        'needs' =>0];

                    if(isset($record->needs)){
                        $sub['needs']= strip_tags($record->needs);
                    }
                    array_push($input, $sub);
                }
            }
        }
        return $input;

    }

}
