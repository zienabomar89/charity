<?php

namespace Common\Model;

class AidsCases extends \Common\Model\AbstractCases
{
    protected $type = AbstractCases::TYPE_AIDS;

    public static function getVisitorNotes($id,$category_id,$organization_id,$gender){

      $notes=\Common\Model\AbstractVisitorNotes::fetch($category_id,$organization_id,$gender);

      if($notes){

          $have_health_insurance = trans('aid::application.have_health_insurance');
          $not_have_health_insurance = trans('aid::application.not_have_health_insurance');

          $qualified = trans('common::application.qualified');
          $not_qualified = trans('common::application.not qualified');

          $_need_repair = trans('aid::application._need_repair');
          $not_need_repair = trans('aid::application.not_need_repair');

          $deserted = trans('aid::application.deserted');
          $not_deserted = trans('aid::application.not_deserted');

          $have_other_work_resources = trans('aid::application.have_other_work_resources');
          $not_have_other_work_resources = trans('aid::application.not_have_other_work_resources');

          $have_commercial_records = trans('aid::application.have_commercial_records');
          $not_have_commercial_records = trans('aid::application.not_have_commercial_records');

          $have_receivables = trans('aid::application.have_receivables');
          $not_have_receivables = trans('aid::application.not_have_receivables');

          $male = trans('common::application.male');
          $female = trans('common::application.female');

          $refugee = trans('common::application.refugee');
          $citizen = trans('common::application.citizen');

          $perfect = trans('common::application.perfect');
          $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
          $special_needs = trans('common::application.special_needs');

          $use_device = trans('common::application.use_device');
          $not_use_device = trans('common::application.not_use_device');


          $very_bad = trans('common::application.very_bad');
          $bad = trans('common::application.bad');
          $good = trans('common::application.good');
          $very_good = trans('common::application.very_good');
          $excellent = trans('common::application.excellent');

          $map=[ 'category_name','visited_at','visitor','notes','age', 'name','id_card_number','the_health_status',
                 'birthday','monthly_income','marital_status','refugee','gender','address','male_live','health_details',
                 'female_live','spouses','device_name','needs','country','district','street_address','has_commercial_records',
                 'region','nearlocation','square','mosque','unrwa_card_number','nationality','birth_place','family_count',
                 'bank_name','account_number','account_owner','branch_name','health_diseases','primary_mobile',
                 'secondary_mobile','phone','wataniya','use_device', 'property_types','roof_materials','qualified_card_number',
                 'area','rooms','residence_condition','rent_value','indoor_condition','house_condition','is_qualified',
                 'habitable', 'working','work_status', 'work_job','work_wage','work_location','has_other_work_resources',
                 'can_work','work_reason','promised','reconstruction','persons_properties','essentials_need','aid_sources',
                 'deserted','family_cnt','need_repair','repair_notes','visited_at','visitor_card',
                 'visitor_opinion', 'visitor_evaluation','actual_monthly_income', 'receivables',  'receivables_sk_amount',
                 'has_commercial_records','active_commercial_records', 'gov_commercial_records_details',
                 'has_health_insurance', 'health_insurance_number', 'health_insurance_type',
                 'has_device', 'used_device_name', 'gov_health_details',  'gov_work_details'];

                $query = \DB::table('char_cases')->addSelect('char_cases.*');

                if (strrpos($notes->note,'category_name')) {
                    $query->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
                        ->addSelect(array(
                            'char_categories.name AS category_name'
                        ));
                }

                if (strrpos($notes->note, 'visitor_evaluation')) {
                    $query->selectRaw("CASE WHEN char_cases.visitor_evaluation is null THEN '-'
                                            WHEN char_cases.visitor_evaluation = 0 THEN '$very_bad'
                                            WHEN char_cases.visitor_evaluation = 1 THEN '$bad'
                                            WHEN char_cases.visitor_evaluation = 2 THEN '$good'
                                            WHEN char_cases.visitor_evaluation = 3 THEN '$very_good'
                                            WHEN char_cases.visitor_evaluation = 4 THEN '$excellent'
                                       END AS visitor_evaluation");
                }

                if (strrpos($notes->note, 'essentials_need')) {
                    $query->selectRaw("CASE WHEN char_get_case_essentials_need(char_cases.id) is null THEN '-' Else char_get_case_essentials_need(char_cases.id)  END  AS essentials_need");
                }

                if (strrpos($notes->note, 'reconstruction') ||strrpos($notes->note, 'promised')) {
                    $query->leftjoin('char_reconstructions', function($q){
                        $q->on('char_reconstructions.case_id', '=', 'char_cases.id');
                    });
                    if (strrpos($notes->note, 'promised')) {
                        $not_promise = trans('common::application.not_promise');
                        $promise = trans('common::application.promise');

                        $query ->selectRaw(" CASE WHEN char_reconstructions.promised is null THEN '-'
                                                  WHEN char_reconstructions.promised = 1 THEN '$not_promise'
                                                  WHEN char_reconstructions.promised = 2 THEN '$promise'
                                             END  AS promised");
                    }
                    if (strrpos($notes->note, 'reconstruction')) {
                        $query ->selectRaw("CASE WHEN char_reconstructions.promised = 1 THEN '-'
                                                 WHEN char_reconstructions.promised is null THEN '-'
                                                 WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name
                                           END  AS reconstruction");
                    }
                }

                if (strrpos($notes->note, 'needs')) {
                    $query->leftjoin('char_case_needs', function($q){
                        $q->on('char_case_needs.case_id', '=', 'char_cases.id');
                    });
                    if (strrpos($notes->note, 'needs')) {
                        $query ->selectRaw("CASE WHEN char_case_needs.needs is null THEN '-'
                                                 Else char_case_needs.needs
                                           END  AS needs");
                    }
                }

                if (strrpos($notes->note,'name') || strrpos($notes->note,'id_card_number') ||
                    strrpos($notes->note,'gender') || strrpos($notes->note,'gov_commercial_records_details') ||
                    strrpos($notes->note,'has_commercial_records') || strrpos($notes->note,'active_commercial_records') ||
                    strrpos($notes->note,'birthday') || strrpos($notes->note,'birth_place') ||
                    strrpos($notes->note,'need_repair')  ||
                    strrpos($notes->note,'birth_place') ||
                    strrpos($notes->note,'is_qualified ') ||
                    strrpos($notes->note,'qualified ') ||
                    strrpos($notes->note,'not_qualified ') ||
                    strrpos($notes->note,'qualified_card_number ') ||
                    strrpos($notes->note,'health_details ') ||
                    strrpos($notes->note,'has_health_insurance ') ||
                    strrpos($notes->note,'marital_status')  || strrpos($notes->note,'refugee') ||
                    strrpos($notes->note,'unrwa_card_number') || strrpos($notes->note,'nationality') ||
                    strrpos($notes->note, 'country') || strrpos($notes->note, 'district') ||
                    strrpos($notes->note, 'region') || strrpos($notes->note, 'nearlocation') ||
                    strrpos($notes->note, 'square') || strrpos($notes->note, 'mosque')  ||
                    strrpos($notes->note, 'street_address') || strrpos($notes->note, 'address') ||
                    strrpos($notes->note,'monthly_income')||strrpos($notes->note,'family_count')||
                    strrpos($notes->note,'phone')|| strrpos($notes->note,'wataniya')|| strrpos($notes->note,'primary_mobile')||
                    strrpos($notes->note,'secondary_mobile') || strrpos($notes->note,'property_types')||
                    strrpos($notes->note,'roof_materials')|| strrpos($notes->note,'has_other_work_resources')||
                    strrpos($notes->note,'area')|| strrpos($notes->note,'rooms')||
                    strrpos($notes->note,'residence_condition')|| strrpos($notes->note,'rent_value')||
                    strrpos($notes->note,'indoor_condition')|| strrpos($notes->note,'house_condition')||
                    strrpos($notes->note,'habitable')|| strrpos($notes->note,'persons_properties')||
                    strrpos($notes->note,'aid_sources') || strrpos($notes->note,'receivables')||
                    strrpos($notes->note,'receivables_sk_amount')||
                    strrpos($notes->note,'spouses')|| strrpos($notes->note,'female_live')|| strrpos($notes->note,'male_live')
                ) {

                    $query->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id');

                    if (strrpos($notes->note, 'persons_properties')) {
                        $query->selectRaw("CASE WHEN char_get_persons_properties(char_persons.id) is null THEN '-' Else char_get_persons_properties(char_persons.id)  END  AS persons_properties");
                    }

                    if (strrpos($notes->note, 'aid_sources')) {
                        $query->selectRaw("CASE WHEN char_get_case_aid_sources(char_persons.id) is null THEN '-' Else char_get_case_aid_sources(char_persons.id)  END  AS aid_sources");
                    }

                    if (strrpos($notes->note, 'family_count')) {
                        $query->selectRaw("char_persons.family_cnt as family_count");
                    }

                    if (strrpos($notes->note, 'spouses')) {
                        $query->selectRaw("char_persons.spouses as spouses");
                    }

                    if (strrpos($notes->note, 'spouses')) {
                        $query->selectRaw("CASE WHEN char_persons.has_commercial_records = 1 THEN '$have_commercial_records' 
                                                Else '$not_have_commercial_records' END AS has_commercial_records");
                    }

                    if (strrpos($notes->note, 'female_live')) {
                        $query->selectRaw("char_persons.family_cnt as female_live");
                    }

                    if (strrpos($notes->note, 'male_live')) {
                        $query->selectRaw("char_persons.family_cnt as male_live");
                    }

                    if (strrpos($notes->note, 'name')) {
                        $query->selectRaw("char_persons.first_name,char_persons.second_name,char_persons.third_name,char_persons.last_name,
                                           CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name")
                            ->orderBy('char_persons.first_name')
                            ->orderBy('char_persons.second_name')
                            ->orderBy('char_persons.third_name')
                            ->orderBy('char_persons.last_name');
                    }

                    if (strrpos($notes->note, 'id_card_number')) {
                        $query->addSelect(array('char_persons.id_card_number'));
                    }

                    if (strrpos($notes->note, 'gender')) {
                        $query->selectRaw("CASE WHEN char_persons.gender is null THEN '-'
                                                WHEN char_persons.gender = 1 THEN '$male'
                                               WHEN char_persons.gender = 2 THEN '$female'
                                           END AS gender");
                    }

                    if (strrpos($notes->note, 'receivables')) {
                        $query->selectRaw("CASE WHEN char_persons.receivables = 1 THEN '$have_receivables' 
                                                Else '$not_have_receivables' END AS receivables");
                    }


                    if (strrpos($notes->note, 'receivables_sk_amount')) {
                        $query->selectRaw("CASE  WHEN char_persons.receivables is null THEN '0'
                                                 WHEN char_persons.receivables = 0 THEN '0'
                                                 WHEN char_persons.receivables = 1 THEN char_persons.receivables_sk_amount
                                          END AS receivables_sk_amount");
                    }

                    if (strrpos($notes->note, 'is_qualified')) {
                        $query->selectRaw("CASE WHEN char_persons.is_qualified = 1 THEN '$qualified' 
                                                Else '$not_qualified' END AS is_qualified");
                    }


                    if (strrpos($notes->note, 'qualified_card_number')) {
                        $query->selectRaw("CASE WHEN char_persons.qualified_card_number is null THEN '-' 
                                                Else char_persons.qualified_card_number
                                           END  
                                           AS qualified_card_number");

                    }

                    if (strrpos($notes->note, 'unrwa_card_number')) {
                        $query->selectRaw(" CASE WHEN char_persons.unrwa_card_number is null THEN '-'
                                                 WHEN char_persons.refugee is null THEN '-'
                                                 WHEN char_persons.refugee = 0 THEN '-'
                                                 WHEN char_persons.refugee = 1 THEN '-'
                                                 WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                            END  AS unrwa_card_number");
                    }

                    if (strrpos($notes->note, 'birthday')) {
                        $query->selectRaw("CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday");
                    }

                    if (strrpos($notes->note, 'age')) {
                        $query->selectRaw("CASE WHEN  char_persons.birthday is null THEN ' ' Else 
                                                EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age");
                    }

                    if (strrpos($notes->note, 'deserted')) {
                        $query->selectRaw("CASE WHEN char_persons.deserted is null THEN ' '
                                                WHEN char_persons.deserted = 1 THEN '$deserted'
                                                WHEN char_persons.deserted = 0 THEN '$not_deserted'
                                           END
                                           AS deserted");
                    }

                    if (strrpos($notes->note, 'monthly_income')) {
                        $query->selectRaw("CASE WHEN char_persons.monthly_income is null THEN '-' Else char_persons.monthly_income  END  AS monthly_income");
                    }

                    if (strrpos($notes->note, 'actual_monthly_income')) {
                        $query->selectRaw("CASE WHEN char_persons.actual_monthly_income is null THEN '-' Else char_persons.actual_monthly_income  END  AS actual_monthly_income");
                    }

                    if (strrpos($notes->note, 'gov_commercial_records_details')) {
                        $query->selectRaw("CASE WHEN char_persons.gov_commercial_records_details is null THEN '0' Else char_persons.gov_commercial_records_details  END  AS gov_commercial_records_details");
                    }

                    if (strrpos($notes->note, 'active_commercial_records')) {
                        $query->selectRaw("CASE WHEN char_persons.active_commercial_records is null THEN '0' Else char_persons.active_commercial_records  END  AS active_commercial_records");
                    }

                    if (strrpos($notes->note, 'birth_place')) {
                        $query->join('char_locations_i18n As L', function ($q) {
                            $q->on('L.location_id', '=', 'char_persons.nationality')
                                ->where('L.language_id', 1);
                        })->selectRaw("CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place");
                    }

                    if (strrpos($notes->note, 'marital_status')) {
                        $query->leftjoin('char_marital_status_i18n as marital_status', function($q) {
                            $q->on('marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                            $q->where('marital_status.language_id',1);
                        })->selectRaw("CASE WHEN char_persons.marital_status_id is null THEN '-'  Else marital_status.name END AS marital_status");
                    }

                    if (strrpos($notes->note, 'refugee')) {
                        $query->selectRaw("CASE
                                           WHEN char_persons.refugee is null THEN '-'
                                           WHEN char_persons.refugee = 0 THEN '-'
                                           WHEN char_persons.refugee = 1 THEN ' $citizen '
                                           WHEN char_persons.refugee = 2 THEN '$refugee'
                                     END
                                     AS refugee");
                    }

                    if (strrpos($notes->note, 'nationality')) {
                        $query->join('char_locations_i18n As L0', function ($q) {
                            $q->on('L0.location_id', '=', 'char_persons.nationality')
                                ->where('L0.language_id', 1);
                        })->selectRaw("CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality");
                    }

                    if (strrpos($notes->note, 'country') || strrpos($notes->note, 'district') ||
                        strrpos($notes->note, 'region') || strrpos($notes->note, 'nearlocation') ||
                        strrpos($notes->note, 'square') ||strrpos($notes->note, 'mosque') ||
                        strrpos($notes->note, 'street_address') || strrpos($notes->note, 'address')){

                        $query->leftjoin('char_aids_locations_i18n As country_i18n', function ($q) {
                            $q->on('country_i18n.location_id', '=', 'char_persons.adscountry_id');
                            $q->where('country_i18n.language_id', 1);
                        })
                            ->leftjoin('char_aids_locations_i18n As district_i18n', function ($q) {
                                $q->on('district_i18n.location_id', '=', 'char_persons.adsdistrict_id');
                                $q->where('district_i18n.language_id', 1);
                            })
                            ->leftjoin('char_aids_locations_i18n As region_i18n', function ($q) {
                                $q->on('region_i18n.location_id', '=', 'char_persons.adsregion_id');
                                $q->where('region_i18n.language_id', 1);
                            })
                            ->leftjoin('char_aids_locations_i18n As neighborhood_i18n', function ($q) {
                                $q->on('neighborhood_i18n.location_id', '=', 'char_persons.adsneighborhood_id')
                                    ->where('neighborhood_i18n.language_id', 1);
                            })
                            ->leftjoin('char_aids_locations_i18n As square_i18n', function ($q) {
                                $q->on('square_i18n.location_id', '=', 'char_persons.adssquare_id')
                                    ->where('square_i18n.language_id', 1);
                            })
                            ->leftjoin('char_aids_locations_i18n As mosque_i18n', function ($q) {
                                $q->on('mosque_i18n.location_id', '=', 'char_persons.adsmosques_id')
                                    ->where('mosque_i18n.language_id', 1);
                            });

                        $query->selectRaw("CASE WHEN char_persons.adsmosques_id is null THEN '-' Else '11' END   AS mosque");
                        if (strrpos($notes->note, 'address')) {
                            $query->selectRaw("CONCAT(ifnull(country_i18n.name, ' '), ' ' ,ifnull(district_i18n.name, ' '),' ',
                                                     ifnull(region_i18n.name, ' '),' ', ifnull(neighborhood_i18n.name,' '),' ',
                                                      ifnull(square_i18n.name,' '),' ',
                                                      ifnull(mosque_i18n.name,' '),' ',
                                                     ifnull(char_persons.street_address,' ')) 
                                               AS address");
                        }
                        if (strrpos($notes->note, 'country')) {
                            $query->selectRaw("CASE WHEN country_i18n.name  is null THEN '-' Else country_i18n.name END   AS country");
                        }
                        if (strrpos($notes->note, 'district')) {
                            $query->selectRaw("CASE WHEN district_i18n.name  is null THEN '-' Else district_i18n.name END   AS district");
                        }
                        if (strrpos($notes->note, 'region')) {
                            $query->selectRaw("CASE WHEN region_i18n.name  is null THEN '-' Else region_i18n.name END   AS region");
                        }
                        if (strrpos($notes->note, 'nearlocation')) {
                            $query->selectRaw("CASE WHEN neighborhood_i18n.name is null THEN '-' Else neighborhood_i18n.name END   AS nearlocation");
                        }
                        if (strrpos($notes->note, 'square')) {
                            $query->selectRaw("CASE WHEN square_i18n.name is null THEN '-' Else square_i18n.name END   AS square");
                        }
                        if (strrpos($notes->note, 'mosque')) {
                            $query->selectRaw("CASE WHEN mosque_i18n.name is null THEN '-' Else mosque_i18n.name END   AS mosque");
                        }
                        if (strrpos($notes->note, 'street_address')) {
                            $query->selectRaw("CASE WHEN char_persons.street_address  is null THEN '-' Else char_persons.street_address END AS street_address");
                        }
                    }

                    if (strrpos($notes->note, 'the_health_status') || strrpos($notes->note, 'health_details') ||
                        strrpos($notes->note, 'use_device') || strrpos($notes->note, 'has_health_insurance') ||
                        strrpos($notes->note, 'device_name') ||
                        strrpos($notes->note, 'gov_health_details') || strrpos($notes->note, 'health_insurance_number') ||
                        strrpos($notes->note, 'health_diseases')) {

                        $query->leftjoin('char_persons_health', function ($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })->selectRaw("CASE WHEN char_persons_health.condition is null THEN '-'
                                            WHEN char_persons_health.condition = 1 THEN '$perfect'
                                            WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                            WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                      END  AS the_health_status,
                                      CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS health_diseases
                                      ");

                        if (strrpos($notes->note, 'health_insurance_number')) {
                            $query->selectRaw("CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' 
                                                    Else char_persons_health.health_insurance_number END 
                                               AS health_insurance_number");

                        }
                        if (strrpos($notes->note, 'health_insurance_type')) {
                            $query->selectRaw("CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' 
                                                    Else char_persons_health.health_insurance_type END 
                                               AS health_insurance_type");

                        }

                        if (strrpos($notes->note, 'health_details')) {
                            $query->selectRaw("CASE WHEN char_persons_health.details is null THEN ' ' 
                                                    Else char_persons_health.details END 
                                               AS health_details");
                        }

                        if (strrpos($notes->note, 'has_health_insurance')) {
                            $query->selectRaw("CASE
                                                  WHEN char_persons_health.has_health_insurance is null THEN '$not_have_health_insurance'
                                                  WHEN char_persons_health.has_health_insurance = 1 THEN '$have_health_insurance'
                                                  WHEN char_persons_health.has_health_insurance = 0 THEN '$not_have_health_insurance'
                                          END  AS has_health_insurance");
                        }

                        if (strrpos($notes->note, 'use_device')) {
                            $query->selectRaw(" CASE
                                                          WHEN char_persons_health.has_device is null THEN ' '
                                                          WHEN char_persons_health.has_device = 1 THEN '$use_device'
                                                          WHEN char_persons_health.has_device = 0 THEN '$not_use_device'
                                                     END
                                                     AS use_device");
                        }

                        if (strrpos($notes->note, 'device_name')) {
                             $query->selectRaw("CASE WHEN char_persons_health.used_device_name is null THEN ' '
                                                     Else char_persons_health.used_device_name END 
                                                AS device_name");
                        }

                        if (strrpos($notes->note, 'gov_health_details')) {
                             $query->selectRaw("CASE WHEN char_persons_health.gov_health_details is null THEN ' '
                                                     Else char_persons_health.gov_health_details END 
                                                AS gov_health_details");
                        }


                        if (strrpos($notes->note, 'health_diseases')) {
                            $query->leftjoin('char_diseases_i18n', function ($q) {
                                $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                                $q->where('char_diseases_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END  AS  health_diseases");
                        }


                    }

                    if (strrpos($notes->note, 'phone')) {
                        $query->leftjoin('char_persons_contact as cont1', function ($q) {
                            $q->on('cont1.person_id', '=', 'char_persons.id');
                            $q->where('cont1.contact_type', 'phone');
                        })->selectRaw("CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone");
                    }

                    if (strrpos($notes->note, 'primary_mobile')) {
                        $query->leftjoin('char_persons_contact as cont2', function ($q) {
                            $q->on('cont2.person_id', '=', 'char_persons.id');
                            $q->where('cont2.contact_type', 'primary_mobile');
                        })->selectRaw("CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile");
                    }

                    if (strrpos($notes->note, 'wataniya')) {
                        $query->leftjoin('char_persons_contact as cont2_', function ($q) {
                            $q->on('cont2_.person_id', '=', 'char_persons.id');
                            $q->where('cont2_.contact_type', 'wataniya');
                        })->selectRaw("CASE WHEN cont2_.contact_value is null THEN '-' Else cont2_.contact_value END   AS wataniya");
                    }

                    if (strrpos($notes->note, 'secondary_mobile')) {
                        $query->leftjoin('char_persons_contact as cont3', function ($q) {
                            $q->on('cont3.person_id', '=', 'char_persons.id');
                            $q->where('cont3.contact_type', 'secondery_mobile');
                        })->selectRaw("CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondary_mobile");
                    }

                    if (strrpos($notes->note, 'rooms') || strrpos($notes->note, 'area') ||
                        strrpos($notes->note, 'repair_notes') ||
                        strrpos($notes->note, 'need_repair') ||
                        strrpos($notes->note, 'property_types') || strrpos($notes->note, 'rent_value') || strrpos($notes->note, 'roof_materials') ||
                        strrpos($notes->note, 'house_condition') || strrpos($notes->note, 'residence_condition') ||
                        strrpos($notes->note, 'indoor_condition') || strrpos($notes->note, 'habitable')) {

                        $query->leftjoin('char_residence', function ($q) {
                            $q->on('char_residence.person_id', '=', 'char_persons.id');
                        });

                        if (strrpos($notes->note, 'roof_materials')) {
                            $query ->leftjoin('char_roof_materials_i18n', function($q) {
                                $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                                $q->where('char_roof_materials_i18n.language_id',1);
                            })->selectRaw("CASE WHEN char_roof_materials_i18n.name is null THEN '-' Else char_roof_materials_i18n.name END   AS roof_materials");
                        }

                        if (strrpos($notes->note, 'property_types')) {
                            $query->leftjoin('char_property_types_i18n', function ($q) {
                                $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                                $q->where('char_property_types_i18n.language_id', 1);
                            })->selectRaw("CASE WHEN char_property_types_i18n.name is null THEN '-' Else char_property_types_i18n.name END   AS property_types");
                        }

                        if (strrpos($notes->note, 'rooms')) {
                            $query ->selectRaw("CASE WHEN char_residence.rooms is null THEN '-' Else char_residence.rooms  END  AS rooms");
                        }

                        if (strrpos($notes->note, 'area')) {
                            $query->selectRaw("CASE WHEN char_residence.area is null THEN '-' Else char_residence.area  END  AS area");
                        }

                        if (strrpos($notes->note, 'rent_value')) {
                            $query->selectRaw("CASE WHEN char_residence.rent_value is null  THEN '-' Else char_residence.rent_value  END  AS rent_value");
                        }
                        if (strrpos($notes->note, 'need_repair')) {
                            $query->selectRaw("CASE WHEN char_residence.need_repair is null THEN ' '
                                                WHEN char_residence.need_repair = 1 THEN '$_need_repair'
                                                WHEN char_residence.need_repair = 2 THEN '$not_need_repair'
                                                END
                                            AS need_repair");
                        }
                        if (strrpos($notes->note, 'repair_notes')) {
                            $query->selectRaw("CASE WHEN char_residence.repair_notes is null THEN '-' Else char_residence.repair_notes  END  AS repair_notes");
                        }

                        if (strrpos($notes->note, 'house_condition')) {
                            $query ->leftjoin('char_building_status_i18n', function($q) {
                                $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                                $q->where('char_building_status_i18n.language_id',1);
                            })->selectRaw("CASE WHEN char_building_status_i18n.name is null THEN '-' Else char_building_status_i18n.name  END  AS house_condition");
                        }

                        if (strrpos($notes->note, 'residence_condition')) {
                            $query  ->leftjoin('char_house_status_i18n', function($q) {
                                $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                                $q->where('char_house_status_i18n.language_id',1);
                            })->selectRaw("CASE WHEN char_house_status_i18n.name is null     THEN '-' Else char_house_status_i18n.name  END  AS residence_condition");
                        }

                        if (strrpos($notes->note, 'indoor_condition')) {
                            $query  ->leftjoin('char_furniture_status_i18n', function($q) {
                                $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                                $q->where('char_furniture_status_i18n.language_id',1);
                            })->selectRaw("CASE WHEN char_furniture_status_i18n.name is null THEN '-' Else char_furniture_status_i18n.name  END  AS indoor_condition");
                        }

                        if (strrpos($notes->note, 'habitable')) {
                            $query ->leftjoin('char_habitable_status_i18n', function($q) {
                                $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                                $q->where('char_habitable_status_i18n.language_id',1);
                            })->selectRaw("CASE WHEN char_habitable_status_i18n.name is null THEN '-' Else char_habitable_status_i18n.name  END  AS habitable");
                        }

                    }

                    if (strrpos($notes->note, 'account_number') || strrpos($notes->note, 'account_owner') || strrpos($notes->note, 'bank_name') || strrpos($notes->note, 'branch_name')) {
                        $query->leftjoin('char_organization_persons_banks', function($q) {
                            $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                            $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                        })->leftjoin('char_persons_banks', function($q) {
                            $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                        });
                        if (strrpos($notes->note, 'account_number')) {
                            $query ->selectRaw("CASE WHEN char_persons_banks.account_number is null THEN '-' Else char_persons_banks.account_number END AS account_number");
                        }
                        if (strrpos($notes->note, 'account_owner')) {
                            $query ->selectRaw("CASE WHEN char_persons_banks.account_owner is null THEN '-' Else char_persons_banks.account_owner END AS account_owner");
                        }
                        if (strrpos($notes->note, 'bank_name')) {
                            $query->leftjoin('char_banks', function($q) {
                                $q->on('char_banks.id', '=', 'char_persons_banks.bank_id');
                            })->selectRaw("CASE WHEN char_banks.name is null THEN '-' Else char_banks.name END AS bank_name");
                        }
                        if (strrpos($notes->note, 'branch_name')) {
                            $query->leftjoin('char_branches', function($q) {
                                $q->on('char_branches.id', '=', 'char_persons_banks.branch_name');
                            })->selectRaw("CASE WHEN char_branches.name is null THEN '-' Else char_branches.name END AS branch_name");
                        }
                    }

                    if (strrpos($notes->note, 'working') || strrpos($notes->note, 'work_status') ||
                        strrpos($notes->note, 'gov_work_details') ||
                        strrpos($notes->note, 'has_other_work_resources') ||
                        strrpos($notes->note, 'work_job') || strrpos($notes->note, 'work_wage') ||
                        strrpos($notes->note, 'work_location') || strrpos($notes->note, 'can_work') ||
                        strrpos($notes->note, 'work_reason')) {

                        $query->leftjoin('char_persons_work', function($q) {
                            $q->on('char_persons_work.person_id', '=', 'char_persons.id');
                        });

                        if (strrpos($notes->note, 'has_other_work_resources')) {
                            $query->selectRaw("CASE WHEN char_persons_work.has_other_work_resources is null THEN '$not_have_other_work_resources'
                                                    WHEN char_persons_work.has_other_work_resources = 1 THEN '$have_other_work_resources'
                                                    WHEN char_persons_work.has_other_work_resources = 0 THEN '$not_have_other_work_resources'
                                                    END
                                              AS has_other_work_resources");
                        }

                        if (strrpos($notes->note, 'gov_work_details')) {
                            $query->selectRaw("CASE WHEN char_persons_work.gov_work_details is null THEN ' ' 
                                                    Else char_persons_work.gov_work_details  END  
                                               AS gov_work_details");

                        }
                        if (strrpos($notes->note, 'working')) {
                            $not_work = trans('common::application.not_work');
                            $work = trans('common::application.work');

                            $query->selectRaw("CASE WHEN char_persons_work.working is null THEN '-'
                                                WHEN char_persons_work.working = 1 THEN '$work'
                                                WHEN char_persons_work.working = 2 THEN '$not_work'
                                             END
                                           AS working");
                        }
                        if (strrpos($notes->note, 'can_work')) {

                            $is_can_work = trans('common::application.is_can_work');
                            $not_can_work = trans('common::application.not_can_work');

                            $query->selectRaw("CASE WHEN char_persons_work.can_work is null THEN '-'
                                                    WHEN char_persons_work.can_work = 1 THEN '$is_can_work '
                                                    WHEN char_persons_work.can_work = 2 THEN '$not_can_work'
                                               END AS can_work");
                        }
                        if (strrpos($notes->note, 'work_location')){
                            $query->selectRaw("CASE WHEN char_persons_work.working is null THEN '-'
                                                     WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                                    WHEN char_persons_work.working = 2 THEN '-'
                                                END AS work_location");
                        }
                        if (strrpos($notes->note, 'work_job')){
                            $query->leftjoin('char_work_jobs_i18n as work_job', function($q) {
                                $q->on('work_job.work_job_id', '=', 'char_persons_work.work_job_id');
                                $q->where('work_job.language_id',1);
                            })->selectRaw("CASE
                                           WHEN char_persons_work.working is null THEN '-'
                                           WHEN char_persons_work.working = 1 THEN work_job.name
                                           WHEN char_persons_work.working = 2 THEN '-'
                                     END
                                     AS work_job");

                        }
                        if (strrpos($notes->note, 'work_status')){
                            $query->leftjoin('char_work_status_i18n as work_status', function($q) {
                                $q->on('work_status.work_status_id', '=', 'char_persons_work.work_status_id');
                                $q->where('work_status.language_id',1);
                            })->selectRaw("CASE
                                           WHEN char_persons_work.working is null THEN '-'
                                           WHEN char_persons_work.working = 1 THEN work_status.name
                                           WHEN char_persons_work.working = 2 THEN '-'
                                       END  AS work_status");
                        }
                        if (strrpos($notes->note, 'work_wage')){
                            $query->leftjoin('char_work_wages as work_wage', function($q) {
                                $q->on('work_wage.id', '=', 'char_persons_work.work_wage_id');
                            })->selectRaw("CASE
                                           WHEN char_persons_work.working is null THEN '-'
                                           WHEN char_persons_work.working = 1 THEN work_wage.name
                                           WHEN char_persons_work.working = 2 THEN '-'
                                     END
                                     AS work_wage");
                        }
                        if (strrpos($notes->note, 'work_reason')){
                            $query ->leftjoin('char_work_reasons_i18n as work_reason', function($q) {
                                $q->on('work_reason.work_reason_id', '=', 'char_persons_work.work_reason_id');
                                $q->where('work_reason.language_id',1);
                            })->selectRaw("CASE
                                           WHEN char_persons_work.can_work is null THEN '-'
                                           WHEN char_persons_work.can_work = 1 THEN '-'
                                           WHEN char_persons_work.can_work = 2 THEN work_reason.name
                                       END AS work_reason");
                        }

                    }

                }


               $case=$query->where('char_cases.id', '=', $id)->first();
                $case=(object)$case;


                $final=$notes->note;

                  foreach($map as $k =>$v){
                      if(property_exists((object)$case, $v)){
                          $final = str_replace(' '.$v.' ', $case->$v, $final);
                      }
                  }

                  return ['status' => true,'note'=>$final,'case' =>$case];
            }
      return ['status' => false];
    }

}
