<?php

namespace Common\Model;

class CloneGovernmentPersonsSocialAffairsReceipt  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'clone_government_persons_social_affairs_receipt';
    protected $fillable = ['IDNO','SRV_INF_NAME','ORG_NM_MON','SRV_TYPE_MAIN_NM',
                           'CURRENCY','RECP_AID_AMOUNT','RECP_DELV_DT'];
    public $timestamps = false;

}
