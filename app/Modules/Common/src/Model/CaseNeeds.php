<?php

namespace Common\Model;
class CaseNeeds  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_case_needs';
    protected $primaryKey='case_id';
    protected $fillable = [ 'case_id','needs'];
    public $timestamps = false;

    public static function saveNeeds($case_id,$inputs){

        if($inputs['needs'] == ""){
            $inputs['needs']= trans("common::application.There's no needs to family");
        }

        if(\Common\Model\CaseNeeds::where(["case_id"=>$case_id])->first()){
            \Common\Model\CaseNeeds::where("case_id",$case_id)->update($inputs);
        }else{
            $inputs['case_id']=$case_id;
            \Common\Model\CaseNeeds::create($inputs);
        }
        return true;
    }
}
