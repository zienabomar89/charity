<?php

namespace Common\Model;

class BlockIDCard extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_block_id_card_number';
    public $fillable = ['id_card_number','type'];
    protected $hidden = ['created_at','updated_at'];


    public function BlockCategories()
    {
        return $this->hasMany('Aid\Model\BlockCategories','block_id','id');
    }
    public function getSponsorStatus($id,$category_id)
    {
        $is_blocked=BlockIDCard::where(['id_card_number'  => $id])->first();
        if($is_blocked){
            $target_category=BlockCategories::where(['block_id'  => $is_blocked->id,'category_id'=>$category_id])->count();
            if($target_category!=0){
                return array('status'=>'false');
            }
        }
        return array('status'=>'true');
    }

    public static function isBlock($id,$category_id,$type)
    {
        $is_blocked=BlockIDCard::where(['id_card_number'  => $id ,'type'  => $type])->first();
        if($is_blocked){
            $target_category=BlockCategories::where(['block_id'  => $is_blocked->id,'category_id'=>$category_id])->count();
            if($target_category!=0){
                return true;
            }
        }
        return false;
    }
}
