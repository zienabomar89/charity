<?php

namespace Common\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

abstract class AbstractCases extends \Illuminate\Database\Eloquent\Model
{

    const TYPE_SPONSORSHIPS = 1;
    const TYPE_AIDS = 2;

    protected $table = 'char_cases';
    protected $primaryKey='id';
    public $timestamps = false;

    protected $appends = ['type_name', 'type_options'];

    protected $fillable = ['person_id','organization_id','category_id','notes','visited_at','visitor','status','user_id','rank','reason'
        ,'visitor_card', 'visitor_opinion', 'visitor_evaluation'];
    protected $hidden = ['created_at','updated_at'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;


    public static function type($value = null)
    {
        $options = array(
            self::TYPE_AIDS => trans('application::application.aids'),
            self::TYPE_SPONSORSHIPS => trans('application::application.sponsorship'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getType()
    {
        return self::type($this->type);
    }

    public function getTypeNameAttribute()
    {
        return $this->getType();
    }

    public function getTypeOptionsAttribute()
    {
        return self::type();
    }

    public static function fatchCasesIds($person_id,$organization_id,$category_id){

        $ids=[];
        $query =\DB::table('char_cases')
                ->where('char_cases.person_id', '=', $person_id);

        if($organization_id != null){
            $query->where('char_cases.organization_id', '=', $organization_id);
        }

        if($category_id != null){
            $query->where('char_cases.category_id', '=', $category_id);
        }
        $result=$query->selectRaw('char_cases.id')->get();

        if(sizeof($result) != 0){
            foreach ($result as $key => $value) {
                $ids[]=$value->id;
            }
        }

        return $ids;
    }

    public static function updateRank($id)
    {
        $rank=   \DB::table('char_cases')->where('char_cases.id',$id)
            ->selectRaw("char_cases.id,char_get_case_rank ( char_cases.id ) AS rank")->first();

        return $rank->rank;
    }

    public static function saveCase($input = array())
    {

        $language_id = 1;
        $en_language_id = 2;
        $person_Input = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'birthday', 'gender', 'marital_status_id',
            'birth_place', 'nationality', 'city', 'country', 'governarate', 'location_id', 'street_address', 'refugee','mosques_id',
            'unrwa_card_number', 'monthly_income', 'spouses', 'prev_family_name', 'father_id', 'mother_id', 'death_date', 'death_cause_id'
        ];


        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if ($var == 'birthday' || $var == 'death_date') {
                        $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                    }else{
                        $insert[$var] =$input[$var];
                    }
                }else{
                    $insert[$var] = null;
                }
            }
        }

        $return=array();

        if(isset($input['person_id'])) {
            \Common\Model\Person::where(['id' =>$input['person_id']])->update($insert);
            $person =\Common\Model\Person::findorfail($input['person_id']);
        }else{
            $person = \Common\Model\Person::create($insert);
        }

        if ($person) {
            $return['id']= $person->id;
            $return['first_name']= $person->first_name;
            $return['second_name']= $person->second_name;
            $return['third_name']= $person->third_name;
            $return['last_name']= $person->last_name;
            $person_id = $person->id;

            if(isset($input['en_first_name']) && isset($input['en_second_name']) && isset($input['en_third_name']) && isset($input['en_last_name'])) {
                \Common\Model\PersonModels\PersonI18n::updateOrCreate(['person_id' => $person_id,'language_id' => $en_language_id],
                    ['first_name'  => $input['en_first_name'], 'second_name' => $input['en_second_name'],
                        'third_name'  => $input['en_third_name'], 'last_name'   => $input['en_last_name']
                    ]);
            }

        }

        return $return;

    }

}
