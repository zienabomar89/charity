<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\UpdateDataSetting;

class RelaysPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('reports.relays.manage')) {
            return true;
        }

        return false;
    }

    public function manageConfirm(User $user)
    {
        if ($user->hasPermission('reports.relays.manageConfirm')) {
            return true;
        }

        return false;
    }

    public function confirm(User $user)
    {
        if ($user->hasPermission('reports.relays.confirm')) {
            return true;
        }

        return false;
    }


}

