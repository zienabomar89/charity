<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\SponsorshipCategoriespolicy;

class CategoriesSponsorshipspolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCategoriesPolicy.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCategoriesPolicy.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipCategoriespolicy $sponsorshipCategoriesPolicy = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCategoriesPolicy.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipCategoriespolicy $sponsorshipCategoriesPolicy = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCategoriesPolicy.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipCategoriespolicy $sponsorshipCategoriesPolicy = null)
    {
        if ($user->hasPermission(['sponsorship.sponsorshipCategoriesPolicy.view','sponsorship.sponsorshipCategoriesPolicy.update'])) {
            return true;
        }

        return false;
    }
}

