<?php

namespace Common\Policy;

use Auth\Model\User;
use Common\Model\SponsorshipVisitorNotes;

class SponsorshipVisitorNotesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipVisitorNotes.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipVisitorNotes.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipVisitorNotes $sponsorshipvisitornotes = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipVisitorNotes.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipVisitorNotes $sponsorshipvisitornotes = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipVisitorNotes.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipVisitorNotes $sponsorshipvisitornotes = null)
    {
        if ($user->hasPermission(['sponsorship.sponsorshipVisitorNotes.view','sponsorship.sponsorshipVisitorNotes.update'])) {
            return true;
        }

        return false;
    }
}

