<?php

namespace Common\Policy;

use Auth\Model\User;
use Common\Model\AidsVisitorNotes;

class AidsVisitorNotespolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.aidsVisitorNotes.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.aidsVisitorNotes.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AidsVisitorNotes $aidsvisitornotes = null)
    {
        if ($user->hasPermission('aid.aidsVisitorNotes.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AidsVisitorNotes $aidsvisitornotes = null)
    {
        if ($user->hasPermission('aid.aidsVisitorNotes.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AidsVisitorNotes $aidsvisitornotes = null)
    {
        if ($user->hasPermission(['aid.aidsVisitorNotes.view','aid.aidsVisitorNotes.update'])) {
            return true;
        }

        return false;
    }
}

