<?php

namespace Common\Policy\Categories;
use Auth\Model\User;
use Common\Model\Categories\AidsCategories;

class AidCategoriesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.category.manage')) {
            return true;
        }

        return false;
    }

    public function manageCategoriesFormElement(User $user)
    {
        if ($user->hasPermission('aid.categoriesFormElement.manage')) {
            return true;
        }

        return false;
    }

    public function updateCategoriesFormElement(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.categoriesFormElement.update')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.category.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.category.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.category.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission(['aid.category.view','aid.category.update'])) {
            return true;
        }

        return false;
    }

    public function upload(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.category.upload')) {
            return true;
        }

        return false;
    }

    public function download(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.category.view')) {
            return true;
        }

        return false;
    }

    public function forms(User $user, AidsCategories $category = null)
    {
        if ($user->hasPermission('aid.category.forms')) {
            return true;
        }

        return false;
    }

}

