<?php

namespace Common\Policy\Categories;

use Auth\Model\User;
use Common\Model\Categories\SponsorshipCategories;

class SponsorshipsCategoriesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.category.manage')) {
            return true;
        }

        return false;
    }

    public function manageCategoriesFormElement(User $user)
    {
        if ($user->hasPermission('sponsorship.categoriesFormElement.manage')) {
            return true;
        }

        return false;
    }

    public function updateCategoriesFormElement(User $user, SponsorshipCategories $category = null)
    {
        if ($user->hasPermission('sponsorship.categoriesFormElement.update')) {
            return true;
        }

        return false;
    }


    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.category.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipCategories $category = null)
    {
        if ($user->hasPermission('sponsorship.category.update') ) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipCategories $category = null ){
        if ($user->hasPermission('sponsorship.category.delete') ) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipCategories $category = null)  {
        if ($user->hasPermission(['sponsorship.category.view','sponsorship.category.update'])) {
            return true;
        }

        return false;
    }

    public function upload(User $user, SponsorshipCategories $category = null)
    {
        if ($user->hasPermission('sponsorship.category.upload')) {
            return true;
        }

        return false;
    }
    public function download(User $user, SponsorshipCategories $category = null)
    {
        if ($user->hasPermission('sponsorship.category.view')) {
            return true;
        }

        return false;
    }

    public function forms(User $user, SponsorshipCategories $category = null)
    {
        if ($user->hasPermission('sponsorship.category.forms')) {
            return true;
        }

        return false;
    }
}

