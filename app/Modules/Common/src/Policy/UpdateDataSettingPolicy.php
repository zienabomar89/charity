<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\UpdateDataSetting;

class UpdateDataSettingPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.updateSetting.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.updateSetting.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, UpdateDataSetting $updatesetting = null)
    {
        if ($user->hasPermission('sponsorship.updateSetting.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, UpdateDataSetting $updatesetting = null)
    {
        if ($user->hasPermission('sponsorship.updateSetting.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, UpdateDataSetting $updatesetting = null)
    {
        if ($user->hasPermission(['sponsorship.updateSetting.view','sponsorship.updateSetting.update'])) {
            return true;
        }

        return false;
    }


}

