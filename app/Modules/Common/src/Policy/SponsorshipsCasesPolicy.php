<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\SponsorshipsCases;

class SponsorshipsCasesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.case.manage')) {
            return true;
        }

        return false;
    }
    public function search(User $user)
    {
        if ($user->hasPermission('sponsorship.case.search')) {
            return true;
        }

        return false;
    }
    public function searchIncomplete(User $user)
    {
        if ($user->hasPermission('sponsorship.case.searchIncomplete')) {
            return true;
        }

        return false;
    }

    public function searchAttachment(User $user)
    {
        if ($user->hasPermission('sponsorship.case.searchAttachment')) {
            return true;
        }

        return false;
    }

    public function searchFamilies(User $user)
    {
        if ($user->hasPermission('sponsorship.case.searchFamilies')) {
            return true;
        }

        return false;
    }
      public function manageSponsor(User $user)
    {
        if ($user->hasPermission('sponsorship.SponsorCase.manage')) {
            return true;
        }

        return false;
    }

    public function customForm(User $user)
    {
        if ($user->hasPermission('sponsorship.case.selectCustomForm')) {
            return true;
        }

        return false;
    }
    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.case.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission(['sponsorship.case.view','sponsorship.case.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.export')) {
            return true;
        }

        return false;
    }
    public function import(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.import')) {
            return true;
        }

        return false;
    }

    public function exportSponsor(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.export')) {
            return true;
        }

        return false;
    }

    public function changeCategory(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.changeCategory')) {
            return true;
        }

        return false;
    }
    public function changeStatus(User $user, SponsorshipsCases $cases = null)
    {
        if ($user->hasPermission('sponsorship.case.changeStatus')) {
            return true;
        }

        return false;
    }
    public function islamicCommitmentStatistic(User $user)
    {
        if ($user->hasPermission('reports.case.islamicCommitmentStatistic')) {
            return true;
        }

        return false;
    }

    public function sponsorshipsStatistic(User $user)
    {
        if ($user->hasPermission('reports.case.sponsorshipsStatistic')) {
            return true;
        }

        return false;
    }
    public function paymentsStatistic(User $user)
    {
        if ($user->hasPermission('reports.case.paymentsStatistic')) {
            return true;
        }

        return false;
    }

    public function guardiansSearch(User $user)
    {
        if ($user->hasPermission('reports.case.guardiansSearch')) {
            return true;
        }

        return false;
    }

    public function sponsorshipSearch(User $user)
    {
        if ($user->hasPermission('reports.case.sponsorshipSearch')) {
            return true;
        }

        return false;
    }
}

