<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\BlockIDCard;

class BlockIDCardPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.blockIDCard.manage') || $user->hasPermission('sponsorship.blockIDCard.manage')) {
            return true;
        }

        return false;
    }

    public function manageSponsorshipBlock(User $user)
    {
        if ($user->hasPermission('sponsorship.blockIDCard.manage')) {
            return true;
        }

        return false;
    }
    public function manageAidsBlock(User $user)
    {
        if ($user->hasPermission('aid.blockIDCard.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.blockIDCard.create') || $user->hasPermission('sponsorship.blockIDCard.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, BlockIDCard $blockIDCard = null)
    {
        if ($user->hasPermission('aid.blockIDCard.update') || $user->hasPermission('sponsorship.blockIDCard.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, BlockIDCard $blockIDCard = null)
    {
        if ($user->hasPermission('aid.blockIDCard.delete') || $user->hasPermission('sponsorship.blockIDCard.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, BlockIDCard $blockIDCard = null)
    {
        if ($user->hasPermission(['aid.blockIDCard.view','aid.blockIDCard.update']) ||
            $user->hasPermission(['sponsorship.blockIDCard.view','sponsorship.blockIDCard.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, BlockIDCard $blockIDCard = null)
    {
        if ($user->hasPermission('aid.blockIDCard.export') || $user->hasPermission('sponsorship.blockIDCard.export')) {
            return true;
        }

        return false;
    }

}

