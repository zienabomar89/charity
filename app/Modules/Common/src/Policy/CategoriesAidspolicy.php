<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\AidsCategoriespolicy;

class CategoriesAidspolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.aidsCategoriesPolicy.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.aidsCategoriesPolicy.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AidsCategoriespolicy $aidsCategoriesPolicy = null)
    {
        if ($user->hasPermission('aid.aidsCategoriesPolicy.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AidsCategoriespolicy $aidsCategoriesPolicy = null)
    {
        if ($user->hasPermission('aid.aidsCategoriesPolicy.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AidsCategoriespolicy $aidsCategoriesPolicy = null)
    {
        if ($user->hasPermission(['aid.aidsCategoriesPolicy.view','aid.aidsCategoriesPolicy.update'])) {
            return true;
        }

        return false;
    }


}

