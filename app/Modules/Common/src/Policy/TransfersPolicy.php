<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\UpdateDataSetting;

class TransfersPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('reports.transfers.manage')) {
            return true;
        }

        return false;
    }

    public function manageConfirm(User $user)
    {
        if ($user->hasPermission('reports.transfers.manageConfirm')) {
            return true;
        }

        return false;
    }

    public function confirm(User $user)
    {
        if ($user->hasPermission('reports.transfers.confirm')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('reports.transfers.create')) {
            return true;
        }

        return false;
    }


}

