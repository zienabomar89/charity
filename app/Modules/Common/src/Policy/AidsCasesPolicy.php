<?php

namespace Common\Policy;
use Auth\Model\User;
use Common\Model\AidsCases;

class AidsCasesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.case.manage')) {
            return true;
        }

        return false;
    }

    public function search(User $user)
    {
        if ($user->hasPermission('aid.case.search')) {
            return true;
        }

        return false;
    }
     public function searchIncomplete(User $user)
    {
        if ($user->hasPermission('aid.case.searchIncomplete')) {
            return true;
        }

        return false;
    }
    public function searchAttachment(User $user)
    {
        if ($user->hasPermission('aid.case.searchAttachment')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.case.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission(['aid.case.view','aid.case.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.export')) {
            return true;
        }

        return false;
    }
    public function import(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.import')) {
            return true;
        }

        return false;
    }

    public function changeCategory(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.changeCategory')) {
            return true;
        }

        return false;
    }
  public function changeStatus(User $user, AidsCases $cases = null)
    {
        if ($user->hasPermission('aid.case.changeStatus')) {
            return true;
        }

        return false;
    }


    public function customForm(User $user)
    {
        if ($user->hasPermission('aid.case.selectCustomForm')) {
            return true;
        }

        return false;
    }
    public function vouchersStatistic(User $user)
    {
        if ($user->hasPermission('reports.case.vouchersStatistic')) {
            return true;
        }

        return false;
    }


    public function charityActStatistic(User $user)
    {
        if ($user->hasPermission('reports.case.charityAct')) {
            return true;
        }

        return false;
    }

    public function generalSearch(User $user)
    {
        if ($user->hasPermission('reports.case.generalSearch')) {
            return true;
        }

        return false;
    }
    public function voucherSearch(User $user)
    {
        if ($user->hasPermission('reports.case.voucherSearch')) {
            return true;
        }

        return false;
    }

    public function ExportCitizenRepository(User $user)
    {
        if ($user->hasPermission('reports.case.ExportCitizenRepository')) {
            return true;
        }

        return false;
    }

    public function RelayCitizenRepository(User $user)
    {
        if ($user->hasPermission('reports.case.RelayCitizenRepository')) {
            return true;
        }

        return false;
    }

    public function citizenRepository(User $user)
    {
        if ($user->hasPermission('reports.case.citizenRepository')) {
            return true;
        }

        return false;
    }

    public function socialAffairs(User $user)
    {
        if ($user->hasPermission('reports.case.socialAffairs')) {
            return true;
        }

        return false;
    }
    public function ExportSocialAffairs(User $user)
    {
        if ($user->hasPermission('reports.case.ExportSocialAffairs')) {
            return true;
        }

        return false;
    }

    public function aidsCommittee(User $user)
    {
        if ($user->hasPermission('reports.case.aidsCommittee')) {
            return true;
        }

        return false;
    }

    public function ExportAidsCommittee(User $user)
    {
        if ($user->hasPermission('reports.case.ExportAidsCommittee')) {
            return true;
        }

        return false;
    }


    public function photo(User $user)
    {
        if ($user->hasPermission('reports.case.photo')) {
            return true;
        }

        return false;
    }

}

