<?php

namespace App\Modules\Common;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */

    protected $policies = [
        \Common\Model\Transfers\Transfers::class => \Common\Policy\TransfersPolicy::class,
        \Common\Model\Relays\Relays::class => \Common\Policy\RelaysPolicy::class,
        \Common\Model\BlockIDCard::class => \Common\Policy\BlockIDCardPolicy::class,
        \Common\Model\UpdateDataSetting::class => \Common\Policy\UpdateDataSettingPolicy::class,

        \Common\Model\AidsCases::class => \Common\Policy\AidsCasesPolicy::class,
        \Common\Model\SponsorshipsCases::class => \Common\Policy\SponsorshipsCasesPolicy::class,

        \Common\Model\Categories\AidsCategories::class => \Common\Policy\Categories\AidCategoriesPolicy::class,
        \Common\Model\Categories\SponsorshipCategories::class => \Common\Policy\Categories\SponsorshipsCategoriesPolicy::class,

        \Common\Model\AidsCategoriespolicy::class => \Common\Policy\CategoriesAidspolicy::class,
        \Common\Model\SponsorshipCategoriespolicy::class => \Common\Policy\CategoriesSponsorshipspolicy::class,

        \Common\Model\AidsVisitorNotes::class => \Common\Policy\AidsVisitorNotespolicy::class,
        \Common\Model\SponsorshipVisitorNotes::class => \Common\Policy\SponsorshipVisitorNotesPolicy::class
  ];
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'common');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'common');

        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
