<?php
return array(
            'forms.customForms.manage'=>'إدارة النماذج المخصصة',
            'forms.customForms.create'=>'إنشاء نموذج مخصص جديد',
            'forms.customForms.update'=>'تحرير بيانات نموذج مخصص',
            'forms.customForms.delete'=>'حذف نموذج مخصص',
            'forms.customForms.view'=>'عرض بيانات نموذج مخصص',
            'forms.elementOption.manage'=>'إدارة خيارات العناصر',
            'forms.elementOption.create'=>'إنشاء خيار عنصر جديد',
            'forms.elementOption.update'=>'تحرير بيانات خيار عنصر',
            'forms.elementOption.delete'=>'حذف خيار عنصر',
            'forms.elementOption.view'=>'عرض بيانات خيار عنصر',
            'forms.formElement.manage'=>'إدارة عناصر النموذج',
            'forms.formElement.create'=>'إنشاء عنصر نموذج جديد',
            'forms.formElement.update'=>'تحرير بيانات عنصر نموذج',
            'forms.formElement.delete'=>'حذف عنصر نموذج',
            'forms.formElement.view'=>'عرض بيانات عنصر نموذج',
            'forms.formsCasesData.manage'=>'إدارة بيانات النماذج المخصصة ',
            'forms.formsCasesData.create'=>'إنشاءسحل بيانات نموذج مخصص جديد',
            'forms.formsCasesData.update'=>'نحرير سجل بيانات نموذج مخصص',
            'forms.formsCasesData.delete'=>'حذف سجل بيانات نموذج مخصص',
            'forms.formsCasesData.view'=>'عرض سجل بيانات نموذج مخصص'
        );