<?php

namespace Forms\Controller;

use App\Http\Controllers\Controller;
use Common\Model\Categories\CategoriesForm;
use Forms\Model\CustomForms;
use Illuminate\Http\Request;
use Forms\Model\FormsCasesData;
use Forms\Model\FormElement;
use Setting\Model\Setting;

class FormCaseDataController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(){
    }

    // save caseForm data of selected case
    public function store(Request $request)
    {
        $this->authorize('create', FormsCasesData::class);

        $response = array();
        $case_id = $request->get('case_id');


        $person = \Common\Model\CaseModel::fetch(array('full_name'=>true),$case_id);
        $name=$person->full_name;

        $fieldsData = $request->get('fieldsData');
        $Casedata =$rules= $input=[];

        foreach($fieldsData as $key => $value) {
            if ($value['priority'] == 4) {
                $priority = 'required';
                $rules[$value['name']] = $priority;
                $input[$value['name']] = "";
                if(isset($value['data'])){
                    $input[$value['name']] = $value['data'];
                }
            } else if ($value['priority'] == 3 || $value['priority'] == 2) {
                $priority = '';
                $rules[$value['name']] = $priority;
                $input[$value['name']] = "";
                if(isset($value['data'])){
                    $input[$value['name']] = $value['data'];
                }
            }
        }

        if( sizeof($input) == 0){
            $response["status"]= 'success';
            \Log\Model\Log::saveNewLog('FORM_CASE_DATA_UPDATED',trans('forms::application.He edited the custom form data for the case') . ' "'.$name. '" ');
            return response()->json($response);
        }else{
            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            foreach ($fieldsData as $key=> $value) {

                if(isset($value['data'])){
                    $temp = [
                        'element_id' => $value['element_id'],
                        'case_id' => $case_id,
                        'value'=>$value['data']
                    ];
                    if($value['type'] == 2){
                        $temp['option_id']=$value['data'];
                    }else{
                        $temp['option_id']=null;
                    }

                    array_push($Casedata, $temp);
                }

            }

            FormsCasesData::where(['case_id'=>$case_id])->delete();
            FormsCasesData::insert($Casedata);
            $response["status"]= 'success';
            \Log\Model\Log::saveNewLog('FORM_CASE_DATA_UPDATED',trans('forms::application.He edited the custom form data for the case')  . ' "'.$name. '" ');
            return response()->json($response);
        }

    }

    // get caseForm data of selected case
    public function show($id,Request $request)
    {
         $user = \Auth::user();
         $organization_id=$user->organization_id;

         $form_id=null;

         $category_form = CategoriesForm::where(['category_id'=>$request->category_id,
                                                 'status'=>0,
                                                 'organization_id'=>$organization_id])->first();

         if(!is_null($category_form)){
            if($category_form->form_id != '' && $category_form->form_id != ' ' || !is_null($category_form->form_id)){
                $form_id = $category_form->form_id;
            }
         }
        
         if(is_null($form_id)){
            if($category_type == 1){
                $form=Setting::where(['id' => 'sponsorship_custom_form' ,'organization_id' => $case_organization_id])->first();
            }else{
                $form=Setting::where(['id' => 'aid_custom_form' ,'organization_id' => $case_organization_id])->first();
            }
            
            if(!is_null($form)){
               if($form->value != '' && $form->value != ' ' || !is_null($form->value)){
                    $form_id = $form->value;
                }
            }
        }

        if(!is_null($form_id)){

            $ActiveForm=CustomForms::where(function ($q) use($form_id){
                $q->where('id','=',$form_id);
                $q->where('status','=',1);
            })->first();

            if (!$ActiveForm) {
                return response()->json(['status'=>false]);
            }
            $elements =FormElement::with('options')->where('form_id','=',$form_id)->count();

            if ($elements==0) {
                return response()->json(['status_2'=>false]);
            }

            return response()->json(['data'=>FormsCasesData::getData($id,$form_id)]);
        }
        return response()->json(['status'=>false]);
    }

    // get caseForm data of selected case
    public function showFormCaseData(Request $request,$id)
    {
        $form_element=FormElement::where('form_id','=',$request->form_id)->get(['id']);
        $elements=[];
        foreach ($form_element as $key=> $value) {
            array_push($elements, $value->id);
        }

        $data = FormsCasesData::with('options')->where('case_id','=',$id)
            ->whereIn('element_id', $elements )->get();
        return response()->json(['caseData' => $data]);
    }

}
