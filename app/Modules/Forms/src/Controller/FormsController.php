<?php

namespace Forms\Controller;

use App\Http\Controllers\Controller;
use Common\Model\Categories\CategoriesForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Forms\Model\CustomForms;
use Organization\Model\Organization;
use App\Http\Helpers;

class FormsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get Forms model searching by name
    public function index(Request $request){

        $this->authorize('manage', CustomForms::class);

        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        $deleted = false;
        if($request->type =='trash'){
            $deleted = true;
        }
        return response()->json(['master'=>$master,'result'  =>
            CustomForms::getList($user->organization_id,$request->page,$request->itemsCount,$deleted)] );

    }

    // paginate organization forms model
    public function filter(Request $request){

        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        $deleted = false;
        if($request->type =='trash'){
            $deleted = true;
        }
        return response()->json(['master'=>$master,'result'  =>
            CustomForms::getList($user->organization_id,$request->page,$request->itemsCount,$deleted)] );

    }

    public function getCustomForms(Request $request){
        $user = \Auth::user();
        $forms= new CustomForms();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        return response()->json(['master'=>$master,'result'  =>
            $forms->getList($user->organization_id,$request->page,$request->itemsCount)] );
    }

    // list all organization forms model
    public function getCustomFormsList(){
        $user = \Auth::user();
        return response()->json(CustomForms::getActiveList($user->organization_id,null));
    }

    // create new Forms model
    public function store(Request $request)
    {
        $this->authorize('create', CustomForms::class);

        $response = array();
        $input=[
            'name' => strip_tags($request->get('name')),
            'status' => $request->get('status')
        ];

        $rules= [
            'name' => 'required',
//                     'name' => 'required|max:255|unique:char_forms',
            'status' => 'required|integer'
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $user = \Auth::user();
        $founded=CustomForms::where(['name' =>$input['name'],'organization_id' =>$user->organization_id])->first();

        if(!is_null($founded)){
            $response["status"]= 'failed_valid';
            $response["msg"]['name']=[trans('forms::application.This value is already in use')];
            return response()->json($response);
        }
        $input['type']= 2;
        $input['organization_id']= $user->organization_id;

        $inserted=CustomForms::create($input);
        if($inserted)
        {
            \Log\Model\Log::saveNewLog('CUSTOM_FORM_CREATED', trans('forms::application.He added a new custom form') . ' "' . $input['name'] . '" ');
            $response["status"]= 'success';
            $response["form_id"]= $inserted->id;
            $response["msg"]= trans('forms::application.The row is inserted to db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not insert to db');
        }


        return response()->json($response);

    }

    // get existed object model by id
    public function show($id)
    {
        $form = CustomForms::findOrFail($id);
        $this->authorize('view', $form);
        return response()->json($form);

    }

    // get existed object model by id (name)
    public function getFormName($id)
    {
        $form = CustomForms::findOrFail($id);
        $this->authorize('view', $form);
        return response()->json($form);
    }

    // update data of Forms object model by id
    public function update(Request $request,$id)
    {

        $form = CustomForms::findOrFail($id);
        $this->authorize('update', $form);

        $response = array();

        $input=[
            'name' => strip_tags($request->get('name')),
            'status' => $request->get('status'),
        ];

        $rules= [
            'name' => 'required',
//                'name' => 'required|max:255|unique:char_forms,name,'.$id,
            'status' => 'required|integer'
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        $user = \Auth::user();
        $founded=CustomForms::where(['name' =>$input['name'],'organization_id' =>$user->organization_id])->first();

        if(!is_null($founded)){
            if(!($founded->id == $id)){
                $response["status"]= 'failed_valid';
                $response["msg"]['name']=[trans('forms::application.This value is already in use')];
                return response()->json($response);
            }
        }
        if($form->update($input)){
            \Log\Model\Log::saveNewLog('CUSTOM_FORM_UPDATED', trans('forms::application.He edited the properties of the custom form') . ' "' . $input['name'] . '" ');
            return response()->json(['status' => 'success','msg'=>trans('forms::application.The row is updated')]);
        }

        return response()->json(['failed' => 'success','msg'=>trans('forms::application.There is no update')]);

    }

    // delete exist Forms model by id
    public function destroy($id,Request $request)
    {
        $forms = CustomForms::findOrFail($id);
        $this->authorize('delete', $forms);

        $response = array();


        $scm= \Setting\Model\Setting::where(['id'=>'sponsorship_custom_form','value'=>$id])->count();
        $acm= \Setting\Model\Setting::where(['id'=>'aid_custom_form','value'=>$id])->count();

        $category_form = CategoriesForm::where('form_id',$id)->count();

        if($scm > 0 || $acm > 0 || $category_form > 0){
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The form in can not  delete because it is in use on aids or sponsorship forms');
            return response()->json($response);

        }

        $forms->status = 3;
        if($forms->save())
        {
            \Log\Model\Log::saveNewLog('CUSTOM_FORM_DELETED',trans('forms::application.He deleted the custom form')  . ' "' . $forms->name . '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('forms::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }


    // restore exist Forms model by id
    public function restore($id,Request $request)
    {
        $forms = CustomForms::findOrFail($id);
        $this->authorize('delete', $forms);
        $response = array();
        $forms->status = 1;
        $forms->deleted_at = NULL;
        if($forms->save())
        {
            \Log\Model\Log::saveNewLog('CUSTOM_FORM_RESTORED',trans('forms::application.He restore the custom form')  . ' "' . $forms->name . '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('forms::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }
}
