<?php

namespace Forms\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Forms\Model\FormElement;
use Forms\Model\Elementoption;
use App\Http\Helpers;

class ElementsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(){
    }

    // get elements of form by id
    public function getElements($id){
        $fields = FormElement::with('options')->where('form_id','=',$id)->get();
        return response()->json(['fields'=>$fields]);

    }

    // get elements of form by id
    public function getFormElements($id){
        $this->authorize('manage', FormElement::class);
        $fields =  FormElement::with('options')->where('form_id','=',$id)->get() ;
        return response()->json(['fields'=>$fields]);
    }

    // get element of form by id
    public function getElementName($id)
    {
        $element = FormElement::with('options')->where('id','=',$id)->first();
        return response()->json(['Element' =>$element]);
    }

    // get element of form by id
    public function show($id)
    {
        $entry = FormElement::findOrFail($id);
        $this->authorize('view', $entry);
        return response()->json(['data'=>$entry]);
    }

    // create new form element model
    public function store(Request $request)
    {
        $this->authorize('create', FormElement::class);

        $element = $request->get('Element');
        $option = $request->get('Element_Options');
        $response = array();

        $input=[
            'name' => (isset($element['name']) ) ? $element['name'] :null,
            'label' => (isset($element['label']) ) ? $element['label'] :null,
            'priority' => (isset($element['priority']) ) ? $element['priority'] :null,
            'type' => (isset($element['type']) ) ? $element['type'] :null,
            'description' => (isset($element['description']) ) ? $element['description'] :null
        ];

        $rules= [
            'name' => 'required|max:255|unique:char_forms_elements',
            'label' => 'required',
            'description' => 'required',
            'priority' => 'required|integer',
            'type' => 'required|integer',
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $input['form_id']=$element['form_id'];
        $inserted=FormElement::create($input);
        if($inserted)
        {
            if(sizeof($option) !=0){
                $options =[];
                $element_id= $inserted->id;
                foreach ($option as $item) {
                    $temp = [
                        'element_id' => $element_id,
                        'label' => $item['label'],
                        'value' => $item['value'],
                        'weight' => $item['weight']
                    ];
                    array_push($options, $temp);
                }

                if(Elementoption::insert($options)){
                    $response["status"]= 'success';
                    $response["msg"]= trans('forms::application.The row is inserted to db');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('forms::application.The row is not insert to db');
                }

            }else{
                $response["status"]= 'success';
                $response["msg"]= trans('forms::application.The row is inserted to db');
            }
            \Log\Model\Log::saveNewLog('FORM_ELEMENTS_CREATED',trans('forms::application.Inserts an item') . ' "' .  $input['label'] . '" ');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not insert to db');
        }
        return response()->json($response);

    }

    // update orm element model by id
    public function update(Request $request,$id)
    {

            $entry = FormElement::findOrFail($id);
            $this->authorize('update', $entry);

            $response = array();
            $element = $request->get('Element');
            $option = $request->get('Element_Options');
            $Removedoption = $request->get('RemovedOptions');

            $input=[
                'name' => (isset($element['name']) ) ? $element['name'] :null,
                'label' => (isset($element['label']) ) ? $element['label'] :null,
                'priority' => (isset($element['priority']) ) ? $element['priority'] :null,
                'type' => (isset($element['type']) ) ? $element['type'] :null,
                'description' => (isset($element['description']) ) ? $element['description'] :null
            ];

            $rules= [
                'name' => 'required|max:255|unique:char_forms_elements,name,'.$id,
                'label' => 'required',
                'description' => 'required',
                'priority' => 'required|integer',
                'type' => 'required|integer',
            ];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


            $action_1=false;
            $action_2=false;

            if($entry->update($input)) {
                $action_1=true;
            }

            if(sizeof($option) !=0) {

                    if($input['type'] != 2){
                        $options = Elementoption::findOrFail($id);
                        Elementoption::where(['element_id' =>$id] )->delete(1);
                        $action_2=true;
                    }else{
                        $count = Elementoption::where('element_id', '=' , $id)->count();

                         if($count == 0){
                             $options =[];
                             $element_id= $id;
                             foreach ($option as $item) {
                                 $temp = [
                                     'element_id' => $element_id,
                                     'label' => $item['label'],
                                     'value' => $item['value'],
                                     'weight' => $item['weight']
                                 ];
                                 array_push($options, $temp);
                             }

                             $this->authorize('create', Elementoption::class);
                             if(Elementoption::insert($options)){
                                 $action_2=true;
                             }
                         }else{
                             $newoptions=array();
                             if(sizeof($Removedoption) ==0 ){
                                 foreach ($option as $item) {
                                     if($item != null){

                                         if(isset($item['id'])){
                                             $temp = [
                                                 'label' => $item['label'],
                                                 'value' => $item['value'],
                                                 'weight' => $item['weight']
                                             ];

                                             $target = Elementoption::findOrFail($item['id']);
                                             $this->authorize('update', $target);

                                             Elementoption::where(['id' => $item['id'] , 'element_id' =>$id ])->update($temp);
                                             $action_2=true;
                                         }else{
                                             $temp = [
                                                 'element_id' => $id,
                                                 'label' => $item['label'],
                                                 'value' => $item['value'],
                                                 'weight' => $item['weight']
                                             ];
                                             array_push($newoptions, $temp);

                                         }
                                    }
                                 }


                             }else
                             {
                                 foreach ($Removedoption as $item) {
                                     if($item != null){
                                         $target1 = Elementoption::findOrFail($item['id']);
                                         $this->authorize('delete', $target1);
                                         Elementoption::destroy($item['id']);
                                     }
                                 }
                                 foreach ($option as $item) {
                                     if($item != null){

                                         if(isset($item['id'])){
                                             $temp = [
                                                 'label' => $item['label'],
                                                 'value' => $item['value'],
                                                 'weight' => $item['weight']
                                             ];
                                             $Elementoption = Elementoption::findOrFail($item['id']);
                                             $this->authorize('update', $Elementoption);
                                             Elementoption::where(['id' => $item['id'] , 'element_id' =>$id ])->update($temp);
                                         }else{
                                             $temp = [
                                                 'element_id' => $id,
                                                 'label' => $item['label'],
                                                 'value' => $item['value'],
                                                 'weight' => $item['weight']
                                             ];
                                             array_push($newoptions, $temp);
                                         }
                                     }
                                 }
                             }

                             if(sizeof($newoptions) !=0){
                                 $this->authorize('create', Elementoption::class);
                                 if(Elementoption::insert($newoptions)){
                                     $action_2=true;
                                 }
                             }

                             $action_2=true;
                         }

                    }
             }
            if( $action_2 || $action_1){
                $response["status"]= 'success';
                $response["msg"]= trans('forms::application.The row is updated');

                \Log\Model\Log::saveNewLog('FORM_ELEMENTS_UPDATED',trans('forms::application.He edited an item') . ' "' .  $input['label'] . '" ');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('forms::application.There is no update');
            }

            return response()->json($response);

    }

    // delete option of element model by id
    public function deleteOption($id)
    {
        $obj = Elementoption::findOrFail($id);
        $this->authorize('delete', $obj);

        $response = array();
        $element = (object)Elementoption::where('id', '=', $id)->get(['element_id']);
        $element_id= $element[0]->element_id;
        $label=$obj->label;

        if($obj->delete())
        {
            $response["status"]= 'success';
            $response["Options"] = Elementoption::where('element_id', '=', $element_id)->get();
            \Log\Model\Log::saveNewLog('ELEMENT_OPTION_DELETED',trans('forms::application.He deleted the option') . ' "' . $label . '" ');
            $response["msg"]= trans('forms::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }

    // delete exist element model by id
    public function destroy($id)
    {
        $form = FormElement::findOrFail($id);
        $this->authorize('delete', $form);

        $response = array();
        $forms = (object)FormElement::where('id', '=', $id)->get(['form_id']);
        $form_id= $forms[0]->form_id;
        $label=$form->label;

        if($form->delete())
        {
            $response["status"]= 'success';
            \Log\Model\Log::saveNewLog('FORM_ELEMENTS_DELETED',trans('forms::application.Deleted item') . ' "' . $label . '" ');
            $response["FormElement"]= FormElement::with('options')->where('form_id','=',$form_id)->get();
            $response["msg"]= trans('forms::application.The row is deleted from db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('forms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }
}
