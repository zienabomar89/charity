<?php

namespace Forms\Model;

class Elementoption extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_forms_elements_options';
    protected $fillable = ['element_id','label', 'value','weight'];
    public $timestamps = false;

    public function element()
    {
        return $this->belongsTo('Forms\Model\FormElement','element_id','id');
    }



}

