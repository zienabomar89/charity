<?php
namespace Forms\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers;

class CustomForms  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_forms';
    protected $fillable = ['organization_id','name','type','status'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at','updated_at'];
//    public $timestamps = false;
    use SoftDeletes;

    public function organization()
    {
        return $this->belongsTo('Organization\Model\Organization','organization_id','id');
    }

    public static function getList($organization_id,$page,$itemsCount=0,$deleted)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $return = \DB::table('char_forms')
                      ->join('char_organizations as Org','Org.id','=','char_forms.organization_id')
                      ->leftJoin('char_forms_elements', 'char_forms.id', '=', 'char_forms_elements.form_id');

        if($deleted == true){
//            $return->where('char_forms.deleted_at','!=',null);
            $return->where('char_forms.status',3);
        }else{
            $return->where('char_forms.status','!=',3);
        }

        $return->where('char_forms.type','=',2)
                      ->where('char_forms.organization_id','=',$organization_id)
                     ->groupBy('char_forms.id')
                     ->orderBy('char_forms.name')
                     ->selectRaw("char_forms.*,
                                 count(char_forms_elements.form_id) as form_elements, 
                                 CASE  WHEN $language_id = 1 THEN Org.name Else Org.en_name END  AS organization_name ");

        if($page != null){
            $records_per_page = Helpers::recordsPerPage($itemsCount,$return->paginate()->total());
            $return=$return->paginate($records_per_page);
        }else{
            $return=$return->where('char_forms.status' ,'!=' ,3)->get();
        }
        return $return;
    }


    public static function getActiveList($organization_id,$page,$itemsCount=0)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $return = \DB::table('char_forms')
            ->join('char_organizations as Org','Org.id','=','char_forms.organization_id')
            ->leftJoin('char_forms_elements', 'char_forms.id', '=', 'char_forms_elements.form_id')
            ->where('char_forms.deleted_at','=',null)
            ->where('char_forms.type','=',2)
            ->where('char_forms.organization_id','=',$organization_id)
            ->groupBy('char_forms.id')
            ->orderBy('char_forms.name')
            ->selectRaw("char_forms.*,
                                 count(char_forms_elements.form_id) as form_elements, 
                                 CASE  WHEN $language_id = 1 THEN Org.name Else Org.en_name END  AS organization_name ");

        if($page != null){
            $records_per_page = Helpers::recordsPerPage($itemsCount,$return->paginate()->total());
            $return=$return->paginate($records_per_page);
        }else{
            $return=$return->where('char_forms.status' ,'!=' ,3)->get();
        }
        return $return;
    }
}
