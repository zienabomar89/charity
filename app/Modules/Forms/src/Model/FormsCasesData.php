<?php
namespace Forms\Model;

use Common\Model\Categories\CategoriesForm;
use Setting\Model\Setting;
class FormsCasesData extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_forms_cases_data';
    protected $fillable = ['element_id','option_id', 'case_id','weight'];
    public $timestamps = false;
    public function options()
    {
        return $this->belongsTo('Forms\Model\Elementoption','option_id','id');
    }


    public static function getData($case_id,$form_id)
    {
        $return=[];

        $data=\DB::table('char_forms_elements')
                ->leftjoin('char_forms_cases_data as fcd', function($q) use($case_id){
                    $q->on('fcd.element_id', '=', 'char_forms_elements.id');
                    $q->where('fcd.case_id',$case_id);
                })
                ->where('char_forms_elements.form_id','=',$form_id)
                ->selectRaw( "fcd.value,
                             char_forms_elements.id as element_id,
                             char_forms_elements.label,
                             char_forms_elements.priority,
                             char_forms_elements.name,
                             char_forms_elements.type")
                ->get();

        foreach($data as $key => $value) {
           $t=$value;
            $t->options
                =\DB::table('char_forms_elements_options')
                ->where('char_forms_elements_options.element_id','=',$value->element_id)
                ->get();

            $return[]=$t;
        }

        return $return;

    }

    public static function getCaseCustomData($case_id,$category_id,$case_organization_id,$category_type)
    {

        $return=[];

        $user = \Auth::user();
        $user_organization_id=$user->organization_id;

        $form_id = null;

        $category_form = CategoriesForm::where(['category_id'=>$category_id,
                                                'status'=>0,
                                                'organization_id'=>$case_organization_id])->first();

        if(!is_null($category_form)){
            if($category_form->form_id != '' && $category_form->form_id != ' ' || !is_null($category_form->form_id)){
                $form_id = $category_form->form_id;
            }
        }
        
        if(is_null($form_id)){
            if($category_type == 1){
                $form=Setting::where(['id' => 'sponsorship_custom_form' ,'organization_id' => $case_organization_id])->first();
            }else{
                $form=Setting::where(['id' => 'aid_custom_form' ,'organization_id' => $case_organization_id])->first();
            }
            
            if(!is_null($form)){
               if($form->value != '' && $form->value != ' ' || !is_null($form->value)){
                    $form_id = $form->value;
                }
            }
        }
        

        if(!is_null($form_id)){

            $ActiveForm=CustomForms::where(function ($q) use($form_id){
                $q->where('id','=',$form_id);
                $q->where('status','=',1);
            })->first();

            if (!is_null($ActiveForm)) {

                $elements =FormElement::with('options')->where('form_id','=',$form_id)->count();
                if ($elements!=0) {
                    $data=\DB::table('char_forms_elements')
                             ->leftjoin('char_forms_cases_data as fcd', function($q) use($case_id){
                                $q->on('fcd.element_id', '=', 'char_forms_elements.id');
                                $q->where('fcd.case_id',$case_id);
                             })
                             ->where('char_forms_elements.form_id','=',$form_id)
                             ->selectRaw( "fcd.value,
                                         char_forms_elements.id as element_id,
                                         char_forms_elements.label,
                                         char_forms_elements.priority,
                                         char_forms_elements.name,
                                         char_forms_elements.type")
                             ->get();

                    foreach($data as $key => $value) {

                        $t['label']=$value->label;
                        $t['type']=$value->type;
                        $t['value']='-';

                        if(!is_null($value->value)){
                            if($value->type == 2){
                                $t['value']=$value->value;
                                $option
                                    =\DB::table('char_forms_elements_options')
                                    ->where('char_forms_elements_options.element_id','=',$value->element_id)
                                    ->where('char_forms_elements_options.value','=',$value->value)
                                    ->first();
                                if($option){
                                    $t['value']=$option->value;
                                }else{
                                    $t['value']='-';
                                }
                            }else{
                                $t['value']=$value->value;
                            }
                        }

                        $return[]=$t;
                    }
                }

            }

        }

        return $return;

    }

}

