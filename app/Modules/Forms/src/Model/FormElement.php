<?php
namespace Forms\Model;

class FormElement extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_forms_elements';
    protected $fillable = ['form_id','label', 'description','type','priority','name'];
    public $timestamps = false;

    public function form()
    {
        return $this->belongsTo('Forms\Model\CustomForms','form_id','id');
    }

    public function options()
    {
        return $this->hasMany('Forms\Model\Elementoption','element_id','id');
    }

}

