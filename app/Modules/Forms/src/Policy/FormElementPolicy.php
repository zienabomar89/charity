<?php
namespace Forms\Policy;
use Auth\Model\User;
use Forms\Model\FormElement;

class FormElementPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('forms.formElement.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('forms.formElement.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, FormElement $formelement = null)
    {
        if ($user->hasPermission('forms.formElement.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, FormElement $formelement = null)
    {
        if ($user->hasPermission('forms.formElement.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, FormElement $formelement = null)
    {
        if ($user->hasPermission('forms.formElement.view')) {
            return true;
        }

        return false;
    }
}

