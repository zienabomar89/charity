<?php

namespace Forms\Policy;
use Auth\Model\User;
use Forms\Model\Elementoption;

class ElementoptionPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('forms.elementOption.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('forms.elementOption.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Elementoption $elementoption = null)
    {
        if ($user->hasPermission('forms.elementOption.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Elementoption $elementoption = null)
    {
        if ($user->hasPermission('forms.elementOption.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Elementoption $elementoption = null)
    {
        if ($user->hasPermission('forms.elementOption.view')) {
            return true;
        }

        return false;
    }
}

