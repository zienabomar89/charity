<?php

namespace Forms\Policy;
use Auth\Model\User;
use Forms\Model\FormsCasesData;

class FormsCasesDataPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('forms.formsCasesData.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('forms.formsCasesData.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, FormsCasesData $formscasesdata = null)
    {
        if ($user->hasPermission('forms.formsCasesData.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, FormsCasesData $formscasesdata = null)
    {
        if ($user->hasPermission('forms.formsCasesData.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, FormsCasesData $formscasesdata = null)
    {
        if ($user->hasPermission('forms.formsCasesData.view')) {
            return true;
        }

        return false;
    }
}

