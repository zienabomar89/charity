<?php

namespace Forms\Policy;
use Auth\Model\User;
use Forms\Model\CustomForms;

class CustomFormsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('forms.customForms.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('forms.customForms.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, CustomForms $customforms = null)
    {
        if ($user->hasPermission('forms.customForms.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, CustomForms $customforms = null)
    {
        if ($user->hasPermission('forms.customForms.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, CustomForms $customforms = null)
    {
        if ($user->hasPermission('forms.customForms.view')) {
            return true;
        }

        return false;
    }
}

