<?php

namespace App\Modules\Forms;

use Forms\Model\CustomForms;
use Forms\Model\Elementoption;
use Forms\Model\FormElement;
use Forms\Model\FormsCasesData;
use Forms\Policy\CustomFormsPolicy;
use Forms\Policy\ElementoptionPolicy;
use Forms\Policy\FormElementPolicy;
use Forms\Policy\FormsCasesDataPolicy;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        CustomForms::class => CustomFormsPolicy::class,
        Elementoption::class => ElementoptionPolicy::class,
        FormElement::class => FormElementPolicy::class,
        FormsCasesData::class => FormsCasesDataPolicy::class,
    ];

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'forms');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'forms');
        
        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();

    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
