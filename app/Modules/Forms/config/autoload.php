<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Forms\\', __DIR__ . '/../src');
$loader->register();