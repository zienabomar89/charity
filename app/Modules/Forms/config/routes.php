<?php

Route::group(['middleware' => ['api','lang','auth.check'],'prefix' => 'api/v1.0/forms' ,'namespace' => 'Forms\Controller'], function() {

    Route::group(['prefix' => 'form_elements'], function() {
        Route::get('getElements/{id}','ElementsController@getElements');
        Route::delete('deleteOption/{id}','ElementsController@deleteOption');
        Route::get ('getElementName/{id}','ElementsController@getElementName');
        Route::get ('getFormElements/{id}','ElementsController@getFormElements');
    });
    Route::resource('form_elements', 'ElementsController');
    //----------------------------------------------------------------------------------------
    Route::group(['prefix' => 'forms_case_data'], function() {
        Route::get ('showFormCaseData/{id}','FormCaseDataController@showFormCaseData');
        Route::post ('saveCaseData/','FormCaseDataController@saveCaseData');
    });
    Route::resource('forms_case_data', 'FormCaseDataController');
    //----------------------------------------------------------------------------------------
    Route::group(['prefix' => 'custom_forms'], function() {
        Route::get ('restore/{id}','FormsController@restore');
        Route::get ('getFormName/{id}','FormsController@getFormName');
        Route::get ('getCustomFormsList/','FormsController@getCustomFormsList');
        Route::get ('getCustomForms/','FormsController@getCustomForms');
        Route::post ('filter','FormsController@filter');
    });
    Route::resource('custom_forms', 'FormsController');
});

?>