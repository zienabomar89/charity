<?php

namespace Auth\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ChangePassword extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     *
     * @var \Auth\Model\User
     */
    public $user;
    
    /**
     *
     * @var string
     */
    public $password;
    
    public function __construct(\Auth\Model\User $user)
    {
        $this->user = $user;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $url = url('/account/login');

        return (new MailMessage)
                        ->subject('تغيير كلمة المرور')
                        ->greeting(sprintf('السيد، %s %s', $this->user->firstname, $this->user->lastname))
                        ->line('لقد قمت بتغيير كلمة المرور الخاصة بك')
                        ->action('تسجيل الدخول', $url)
                        ->line('شكرا لك!');
    }

}