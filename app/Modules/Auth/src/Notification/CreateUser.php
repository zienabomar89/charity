<?php

namespace Auth\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateUser extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     *
     * @var \Auth\Model\User
     */
    public $user;
    
    /**
     *
     * @var string
     */
    public $password;
    
    public function __construct(\Auth\Model\User $user, $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
    
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $url = url('/account/login');

        return (new MailMessage)
                        ->subject('حساب مستخدم جديد')
                        ->greeting(sprintf('السيد، %s %s', $this->user->firstname, $this->user->lastname))
                        ->line('تم إنشاء حساب خاص بك على النظام المحوسب لإدارة الجمعيات. فيما يلي بيانات الدخول الخاصة بك:')
                        ->line(sprintf('اسم المستخدم: %s', $this->user->username))
                        ->line(sprintf('كلمة المرور: %s', $this->password))
                        ->action('تسجيل الدخول', $url)
                        ->line('شكرا لك!');
    }

}