<?php

namespace Auth\Policy;

use Auth\Model\User;

class UserPolicy
{
    public function manage(User $authUser)
    {
        if ($authUser->hasPermission('auth.users.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function trashed(User $authUser)
    {
        if ($authUser->hasPermission('auth.users.trashed')) {
            return true;
        }
        
        return false;
    }
    
    public function restore(User $authUser)
    {
        if ($authUser->hasPermission('auth.users.restore')) {
            return true;
        }
        
        return false;
    }
    
    public function create($authUser)
    {
        if ($authUser->hasPermission('auth.users.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//        if ($authUser->hasPermission('auth.users.update')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission('auth.users.update')) {
            return true;
        }

        return false;
    }
    
    public function delete($authUser, User $user = null)
    {
        if ($authUser->id == $user->id) {
            return false;
        }
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission('auth.users.delete')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission('auth.users.delete')) {
            return true;
        }

        return false;
    }
    
    public function view($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission(['auth.users.view', 'auth.users.update', 'auth.users.changePassword'])
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission(['auth.users.view', 'auth.users.update', 'auth.users.changePassword'])) {
            return true;
        }
        
        return false;
    }
    
    public function status($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission('auth.users.status')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission('auth.users.status')) {
            return true;
        }
        return false;
    }
    
    public function permissions($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission('auth.users.permissions')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission('auth.users.permissions')) {
            return true;
        }

        return false;
    }
    
    public function permissionsUpdate($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission('auth.users.permissionsUpdate')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }


        if ($authUser->hasPermission('auth.users.permissionsUpdate')) {
            return true;
        }
        return false;
    }
    
    public function changePassword($authUser, User $user = null)
    {
//        if ($user->super_admin && !$authUser->super_admin) {
//            return false;
//        }
//
//        if ($authUser->hasPermission('auth.users.changePassword')
//                && ($authUser->super_admin || $authUser->organization_id == $user->organization_id)) {
//            return true;
//        }

        if ($authUser->hasPermission('auth.users.changePassword')) {
            return true;
        }

        return false;
    }
}

