<?php

namespace Auth\Policy;

use Auth\Model\User;
use Auth\Model\Role;

class RolePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('auth.roles.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('auth.roles.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Role $role = null)
    {
        if ($user->organization_id == $role->organization_id
                && $user->hasPermission('auth.roles.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Role $role = null)
    {
        if ($user->organization_id == $role->organization_id
                && $user->hasPermission('auth.roles.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Role $role = null)
    {
        if ($user->organization_id == $role->organization_id
                && $user->hasPermission(['auth.roles.view', 'auth.roles.update'])) {
            return true;
        }
        
        return false;
    }
    
    public function copy(User $user, Role $role = null)
    {
        if ($user->organization_id == $role->organization_id
                && $user->hasPermission('auth.roles.copy')) {
            return true;
        }
        
        return false;
    }
    
    public function users(User $user, Role $role = null)
    {
        if ($user->organization_id == $role->organization_id
                && $user->hasPermission('auth.roles.users')) {
            return true;
        }
        
        return false;
    }
}

