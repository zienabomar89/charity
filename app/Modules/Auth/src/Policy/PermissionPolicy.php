<?php

namespace Auth\Policy;

use Auth\Model\User;

class PermissionPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission(['auth.roles.create', 'auth.roles.update'])) {
            return true;
        }
        
        return false;
    }
}

