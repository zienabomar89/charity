<?php

namespace Auth\Policy;

use Auth\Model\User;
use Auth\Model\Setting;

class SettingPolicy
{
    public function manage(User $user, Setting $setting = null)
    {
        if ($user->hasPermission('auth.settings.manage')) {
            /*if (null !== $setting) {
                if ($setting->organization_id && $setting->organization_id != $user->organization_id) {
                    return false;
                }
            }*/
            return true;
        }

        return false;
    }

    public function backup(User $user, Setting $setting = null)
    {
        if ($user->hasPermission('auth.settings.backup')) {
            /*if (null !== $setting) {
                if ($setting->organization_id && $setting->organization_id != $user->organization_id) {
                    return false;
                }
            }*/
            return true;
        }

        return false;
    }
}

