<?php

namespace Auth\Database\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('char_users')->insert([
            'username' => 'nepras',
            'password' => bcrypt('sarpen'),
            'email' => 'info@nepras.com',
            'firstname' => 'Nepras',
            'lastname' => 'Admin',
            'job_title' => 'Administrator',
            'mobile' => '0599885474',
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }
}