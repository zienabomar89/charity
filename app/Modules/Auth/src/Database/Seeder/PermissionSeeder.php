<?php

namespace Auth\Database\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $module_id = DB::table('app_modules')->where('code', 'auth')->value('id');
        if (!$module_id) {
            $module_id = DB::table('app_modules')->insertGetId([
                'code' => 'auth',
                'name' => 'المستخدمين والصلاحيات',
                'description' => '',
                'status' => 1,
            ]);
        }
        
        $permissions = require dirname(__DIR__) . '/../../languages/ar/permission.php';
        foreach ($permissions as $key => $value) {
            DB::table('char_permissions')->insert([
                'module_id' => $module_id,
                'code' => $key,
                'name' => $value,
            ]);
        }
    }
}
