<?php

namespace Auth\Controller;

use Auth\Model\RolePermission;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth\Model\Role;

class RolesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get user role according user_id if not super admin or organization_id if super admin
    public function index()
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $this->authorize('manage', Role::class);
        $user = Auth::user();

        $options= ['users' => true];
        if (!$user->super_admin) {
            $options['user_id'] = $user->id;
        }else{
            $options['organization_id'] = $user->organization_id;
        }
        return response()->json(Role::fetch($options));
    }

    // store new user role according (user_id , organization_id )
    public function store(Request $request)
    {
        $this->authorize('create', Role::class);
        $this->validate($request, Role::getValidatorRules());
        
        $user = Auth::user();
        $role = new Role();
        $role->name            = $request->input('name');
        $role->user_id = $user->id;
        $role->organization_id = $user->organization_id;
        $allow = $request->input('allow');
        $deny = $request->input('deny');
        $role->saveWithPermissions($allow, $deny);
        return response()->json($role);
    }

   //  get role by id
    public function show($id)
    {
        $role = Role::findOrFail($id);
//        $this->authorize('view', $role);
        return response()->json($role);
    }

    //  update role by id
    public function update(Request $request, $id)
    {
        $role = Role::findOrFail($id);
//        $this->authorize('view', $role);
        
        $this->validate($request, Role::getValidatorRules());
        $role->name = $request->input('name');
        $allow = $request->input('allow');
        $deny = $request->input('deny');
        $role->saveWithPermissions($allow, $deny);
        return response()->json($role);
    }

    //  destroy role by id
    public function destroy(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('view', $role);
        
        $role->delete();
        return response()->json($role);
    }

    //  copy role by id
    public function copy(Request $request, $id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('copy', $role);
        
        $organization_id = $request->input('organization_id');
        $copy = $role->copy($organization_id);
        
        return response()->json(['data' => $copy]);
    }

    //  get users that has role by id
    public function users($id)
    {
        $role = Role::findOrFail($id);
        $this->authorize('users', $role);
        
        return response()->json($role->users);
    }
}
