<?php

namespace Auth\Controller;

use Laravel\Passport\Http\Controllers\AccessTokenController;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response as Psr7Response;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use Setting\Model\Setting;

class LoginController extends AccessTokenController
{
    use ThrottlesLogins;
    
    protected $settings;

    /**
     * Authorize a client to access the user's account.
     *
     * @param  ServerRequestInterface  $request
     * @return Response
     */
    public function issueToken(ServerRequestInterface $request)
    {
        $settings = $this->getSettings();
        
        $httpRequest = request();
        if ($settings->decayMinutes && $this->hasTooManyLoginAttempts($httpRequest)) {
            $this->fireLockoutEvent($httpRequest);
            return $this->sendLockoutResponse($httpRequest);
        }

        $username = $httpRequest->username;

        $user =\DB::table('char_users')
                ->where('username', '=', $username)
                ->first() ;

       if ($user){
           $user_id = $user->id;

           if ($user->deleted_at){
               return response()->json([
                   'error' => 'throttle',
                   'message' => trans('auth::application.deleted user'),
               ], 401);
           }
           else{

               if ($user->status == 2){
                   return response()->json([
                       'error' => 'throttle',
                       'message' => trans('auth::application.inactive user'),
                   ], 401);
               }
               else{

                   if (!( $user->super_admin || $username = 'charity' )){

                       $no_sessions_at_time = $user->no_sessions_at_time;
                       $tokens =\DB::table('oauth_access_tokens')
                           ->where(function($q) use ($username,$user_id){
                               $q->where('revoked',0);
                               $q->where('user_id',$user_id);
//                           $q->whereIn('user_id',function($q_) use ($username){
//                               $q_->select('id')
//                                   ->from('char_users')
//                                   ->where('username', '=', $username);
//                           });
                           })->count() ;


                       if ($tokens > 1){
                           if ($tokens != $no_sessions_at_time){
                               return response()->json([
                                   'error' => 'throttle',
                                   'message' => trans('auth::application.You are not allowed to log in') .' '.trans('auth::application.no of allowed sessions') .
                                       ' : ( ' . sizeof($no_sessions_at_time) . ' ) ، ' . trans('auth::application.no of sessions') . ' : ( ' .$tokens . ' )',
                               ], 401);
                           }
                       }

                   }

               }
           }

       }else{
           return response()->json([
               'error' => 'throttle',
               'message' => trans('auth::application.not registered user'),
           ], 401);
       }

        $response = $this->withErrorHandling(function () use ($request) {
            return $this->server->respondToAccessTokenRequest($request, new Psr7Response);
        });

        if ($response->getStatusCode() < 200 || $response->getStatusCode() > 299) {
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            if ($settings->decayMinutes) {
                $this->incrementLoginAttempts($httpRequest);
            }
            return $response;
        }

        $payload = json_decode($response->getBody()->__toString(), true);

        if (isset($payload['access_token'])) {
//             $this->revokeOtherAccessTokens($payload);
        }

        return $response;
    }
    
    public function username()
    {
        return 'username';
    }
    
    protected function hasTooManyLoginAttempts(Request $request)
    {
        
        $settings = $this->getSettings();
        
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), $settings->maxAttempts, $settings->decayMinutes
        );
    }
    
    /**
     * Redirect the user after determining they are locked out.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendLockoutResponse(Request $request)
    {
        $seconds = $this->limiter()->availableIn(
            $this->throttleKey($request)
        );

        $message = Lang::get('auth.throttle', ['seconds' => $seconds]);

        return response()->json([
            'error' => 'throttle',
            'message' => $message,
        ], 401);
    }
    
    protected function getSettings()
    {
        if (null === $this->settings) {
            $this->settings = new \stdClass();
            $entries = Setting::whereIn('id', array(
                'auth_throttlesLogins_maxAttempts',
                'auth_throttlesLogins_decayMinutes',
            ))->where('organization_id', '=', 0)->get();
            
            foreach ($entries as $entry) {
                $id = str_replace('auth_throttlesLogins_', '', $entry->id);
                $this->settings->{$id} = $entry->value;
            }
        }
        
        return $this->settings;
    }

}
