<?php

namespace Auth\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Permission;
use Organization\Model\Organization;

class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get user permissions according (user_id , module_id , role_id if isset)
    public function index()
    {

        $this->authorize('manage', Permission::class);
        
        $user = \Illuminate\Support\Facades\Auth::user();
        $organization = Organization::find($user->organization_id);
        $options['crossed_permission'] = false;

        $request = request();
        $user_id = $request->input('user_id');
        $role_id = $request->input('role_id');
        $module_id = $request->input('module_id');
        
        $options = array(
            'with_module_name' => true,
            'group_by_module' => true,
        );

        if (!$user->super_admin) {
            $options['crossed'] = true;
            $pIds = [];
            $except = [];
            $allowed = $user->permissions();

            $allowed_map = [];
            $last_update = [];
            if ($allowed) {
                foreach ($allowed as $entry) {
                    if(!isset($allowed_map[$entry->permission_id])){
                        $allowed_map[$entry->permission_id] = [];
                    }
                    $allowed_map[$entry->permission_id][]=$entry;
                }

                foreach ($allowed_map as $permission_id => $entry) {
                    if(sizeof($entry) > 1){
                        $max = $entry[0];
                        foreach ($entry as $key => $value) {
                            if (new \DateTime($max->created_at) < new \DateTime($value->created_at)) {
                                $max = $value;
                            }
                        }
                        $last_update[]= $max;
                    }else{
                        $last_update[]= $entry[0];
                    }

                }
                foreach ($last_update as $entry) {
                    if($entry->allow == 1 || $entry->allow == '1'){
                        if(!in_array($entry->permission_id,$pIds)){
                            $pIds[] = $entry->permission_id;
                        }
                    }else{
                        $except[]=$entry;
                    }
                }
            }
            $options['allowed'] = $pIds;
        }

        if (!$user->super_admin && $organization && $organization->parent_id) {
            //return response()->json(Permission::allForUser($user->id, $module_id, $role_id));
            $options['user_id'] = $user->id;
        }

        $permissions = [];
        if ($user_id) {
            $options['user_role'] = true;
            $permissions = Permission::withUserPermissions($user_id, $options);
        } else if ($role_id) {
            $permissions = Permission::withRolePermissions($role_id, $options);
        } else {
            $permissions = Permission::fetch($options);
        }

        return response()->json($permissions);
    }
    
    public function user($id)
    {
        
    }
    
    public function role($id)
    {
        
    }
}
