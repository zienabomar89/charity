<?php

namespace Auth\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Auth\Model\User;
use Organization\Model\Organization;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // update data of logged user (email , mobile , wataniya , nick_name , position )
    public function update(Request $request)
    {
        $user = Auth::user();
//        $this->validate($request, array(
//            'email' => 'required|email|unique:char_users,email,'. $user->id .'|max:255',
////            'id_card_number' => 'required|id_card_number|unique:char_users,id_card_number,'. $user->id
//        ));

        $error = \App\Http\Helpers::isValid($request->all(),array(
            'email' => 'required|email|unique:char_users,email,'. $user->id .'|max:255',
//            'id_card_number' => 'required|id_card_number|unique:char_users,id_card_number,'. $user->id
        ));
        if($error)
            return response()->json($error);

        $user->id_card_number = $request->input('id_card_number');
        $user->email = $request->input('email');
        $user->wataniya = $request->input('wataniya');
        $user->mobile = $request->input('mobile');
        $user->nick_name = $request->input('nick_name');
        $user->position = $request->input('position');

        $user->save();
        return response()->json($user);
    }

    // set image of logged user
    public function setImage(Request $request)
    {

        $user = Auth::user();
        $document_id = $user->document_id;
        $user->document_id = $request->input('id');
        $user->save();
        if ($document_id != $user->document_id){
            $user->status = "success";
            $user->msg = trans('auth::application.Saved');
        }else {
            $user->status = "error";
        }
        return response()->json($user);
    }

    // return details of logged user (basic data , permissions)
    public function user()
    {
        $user = Auth::user();
        $user->organization = $user->organizationName();

        $permissions = array();

        if ($user->super_admin) {
            $entries = \Auth\Model\Permission::all(array('code'));
            if ($entries) {

                $options['crossed'] = true;
                $permissions = [];
                $except = [];
                $allowed = $user->permissions();

                $allowed_map = [];
                $last_update = [];
                if ($allowed) {
                    foreach ($allowed as $entry) {
                        if(!isset($allowed_map[$entry->permission_id])){
                            $allowed_map[$entry->permission_id] = [];
                        }
                        $allowed_map[$entry->permission_id][]=$entry;
                    }

                    foreach ($allowed_map as $permission_id => $entry) {
                        if(sizeof($entry) > 1){
                            $max = $entry[0];
                            foreach ($entry as $key => $value) {
                                if (new \DateTime($max->created_at) < new \DateTime($value->created_at)) {
                                    $max = $value;
                                }
                            }
                            $last_update[]= $max;
                        }else{
                            $last_update[]= $entry[0];
                        }

                    }
                    foreach ($last_update as $entry) {
                        if($entry->allow == 1 || $entry->allow == '1'){
                            if(!in_array($entry->permission_id,$permissions)){
                                $permissions[] = $entry->code;
                            }
                        }else{
                            $except[]=$entry;
                        }
                    }
                }
//            foreach ($entries as $entry) {
//                $permissions[] = $entry->code;
//            }
            }
        } else {
            $permissions = Permission::allowedUserPermissionCode($user->id);
        }
        $user->permissions = $permissions;
        return response()->json($user);
    }
    
    public function notifications()
    {
        $notifications = [];
//        \Carbon\Carbon::setLocale('ar');
//        $user = Auth::user();
//        foreach ($user->unreadNotifications as $entry) {
//            $entry->since = $entry->created_at->diffForHumans(null, true);
//            $notifications[] = $entry;
//        }
//        return response()->json($notifications);
    }

    // log out user and destroy revoke token
    public function logout(\Illuminate\Http\Request $request)
    {
        $request->user()->token()->revoke();
        
        if ($request->expectsJson()) {
            return response()->json([]);
        }

        return redirect()->guest('account/login');
    }
}
