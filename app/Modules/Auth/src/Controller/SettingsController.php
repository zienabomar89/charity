<?php

namespace Auth\Controller;

use App\Http\Controllers\Controller;
use Common\Model\GovServices;
use Illuminate\Http\Request;
use Auth\Model\Setting;
use Illuminate\Support\Facades\Auth;

class SettingsController extends Controller
{
    protected $settings = array(
        'auth_throttlesLogins_maxAttempts',
        'auth_throttlesLogins_decayMinutes',
        'gov_service',
        'gov_service_client_id',
        'gov_service_client_secret',
        'gov_service_client_authorization',
        'gov_service_expires_in',
    );
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get general system setting
    public function index()
    {
        $this->authorize('manage', Setting::class);
        $settings = Setting::whereIn('id', $this->settings)->where('organization_id', 0)->get();

        $expires_in = null;
        $client_id = null;
        $client_secret = null;
        foreach ($settings as $key => $setting) {
            if ($setting->id === 'gov_service_expires_in') {
                $expires_in = $setting->value;
            }
            if ($setting->id === 'gov_service_client_id') {
                $client_id = $setting->value;
            }
            if ($setting->id === 'gov_service_client_secret') {
                $client_secret = $setting->value;
            }
        }

        if (!is_null($expires_in)){
            $expires_in_dt=date('Y-m-d H:i:s',strtotime($expires_in));
            $st_dt = new \DateTime($expires_in_dt);
            $end_dt = new \DateTime();

            if ($end_dt < $st_dt){
                $access_token = GovServices::resetApiToken($client_id,$client_secret);
                if($access_token['status'] != false){

                    Setting::where('id', 'gov_service_client_authorization')
                        ->where('organization_id', 0)
                        ->update(['value'=>$access_token['client_authorization']]);

                    Setting::where('id', 'gov_service_expires_in')
                        ->where('organization_id', 0)
                        ->update(['value'=>$access_token['expires_in']]);

                }
            }
        }

        return response()->json($settings);
    }

    // save or update general system setting
    public function store(Request $request)
    {
        $this->authorize('manage', Setting::class);
        $settings = $map = array();
        
        $entries = Setting::whereIn('id', $this->settings)->where('organization_id', 0)->get();
        foreach ($entries as $entry) {
            $map[$entry->id] = $entry;
        }
        
        foreach ($this->settings as $id) {
            $value = $request->input($id);
            if ($value !== null) {
                if(array_key_exists($id, $map)) {
                    $map[$id]->update(array(
                        'value' => $value
                    ));
                    $settings[] = $map[$id];
                } else {
                    $settings[] = Setting::create(array(
                        'id' => $id,
                        'organization_id' => null,
                        'value' => $value,
                    ));
                }
            }
        }
        
        return response()->json($settings);
    }

    // update gov_service_client_authorization on system setting
    public function resetApiToken(Request $request)
    {
//        $this->authorize('manage', Setting::class);
         $map = array();

        $settings = array('gov_service_client_id', 'gov_service_client_secret', 'gov_service_client_authorization', 'gov_service_expires_in');
        $entries = Setting::whereIn('id', $settings)->where('organization_id', 0)->get();
        foreach ($entries as $entry) {
            $map[$entry->id] = $entry;
        }

        $access_token = GovServices::resetApiToken($map['gov_service_client_id']->value,$map['gov_service_client_secret']->value);

        $settings = array('gov_service_client_authorization','gov_service_expires_in');
        if($access_token['status'] != false){
            foreach ($this->settings as $id) {
                $value = $request->input($id);
                if ($value !== null) {
                    $keyValue = substr($id, 12); ;
                    if(array_key_exists($id, $map)) {
                        $map[$id]->update(array(
                            'value' => $access_token[$keyValue]
                        ));
                        $settings[] = $map[$id];
                    } else {
                        $settings[] = Setting::create(array(
                            'id' => $id,
                            'organization_id' => null,
                            'value' => $access_token[$keyValue]
                        ));
                    }
                }
            }
            return response()->json(['status'=>'success','token'=>$access_token['client_authorization']]);
        }else{
            foreach ($settings as $id) {
                $value = $request->input($id);
                if ($value !== null) {
                    if(array_key_exists($id, $map)) {
                        $map[$id]->update(array(
                            'value' => ''
                        ));
                        $settings[] = $map[$id];
                    } else {
                        $settings[] = Setting::create(array(
                            'id' => $id,
                            'organization_id' => null,
                            'value' => ''
                        ));
                    }
                }
            }
        }

        return response()->json(['status'=>'failed','msg'=>$access_token['message']]);
    }

}
