<?php

namespace Auth\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Notification;
use Auth\Notification\ChangePassword as ChangePasswordNotification;

class PasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // update user password
    public function update(Request $request, $id = null)
    {
        $rules = array(
            'password' => 'required|confirmed',
            'password_confirmation' => 'required',
        );
        
        if (null === $id) {
            $user = Auth::user();

            if($user->username === $request->password) {
                return response()->json(['status' => 'error',
                                         'msg'=> trans('common::application.the username must not be the password')]);

            }

            $user->setUpdateContext(User::UPDATE_CONTEXT_PASSWORD_RESET);
            Validator::extend('currentPassword', function($attribute, $value, $parameters, $validator) use ($user) {
                return User::checkHash($value, $user->password);
            });
            $rules['old_password'] = 'required|currentPassword';
        } else {
            $user = User::findOrFail($id);
            $this->authorize('changePassword', $user);
            $user->setUpdateContext(User::UPDATE_CONTEXT_PASSWORD_CHANGE);
        }
        
        $this->validate($request, $rules, array(
            'current_password' => trans('auth::application.validation_current_password'),
        ));
        
        $password = $request->input('password');
        $user->updatePassword($password);
        
        Notification::send([$user], new ChangePasswordNotification($user));

        return response()->json(['status' => 'success','user' => $user,
                                 'msg'=> trans('common::application.The row is updated')]);
        return response()->json();
    }
}