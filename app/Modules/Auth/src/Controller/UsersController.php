<?php

namespace Auth\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Auth\Model\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Auth\Notification\CreateUser;
use \App\Http\Helpers;
use Laravel\Passport\RefreshTokenRepository;
use Log\Model\Log;
use Sms\Model\SmsProviders;
use Maatwebsite\Excel\Facades\Excel;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get users descendants according organization_id
    public function index()
    {
        $this->authorize('manage', User::class);
        $user = Auth::user();

        $request = request();
        $filters = array(
                            'organization_id' => $request->query('organization_id'),
                            'status'          => $request->query('status'),
                            'username'        => $request->query('username'),
                            'email'           => $request->query('email'),
                            'name'            => $request->query('name'),
                            'organization_name' => true,
                        );

        $users = User::descendants($user->organization_id);
        if (!$user->super_admin) {
            $users->where('id', '<>', $user->id);
        }


        return response()->json($users->filter($filters)->orderBy('firstname')->orderBy('lastname')->paginate());
    }

    // filter users according user type ( organization_id , type sub (connected location)  , descendant )
    public function filter(Request $request)
    {
        $this->authorize('manage', User::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        return response()->json(User::filters($request->all()));
    }

    public function online(Request $request)
    {
        $this->authorize('manage', User::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $users  = User::onlineFilter($request->all());
//            $users->map(function ($q) {
////                $q->last_action =\DB::statment("`last_action_date`($q->id)");
////                $q->last_action =\DB::table('app_logs')
////                    ->where(function($sq) use ($q){
////                        $sq->where('user_id',$q->id);
////                    })
////                    ->orderBy('created_at', 'desc')
////                    ->selectRaw('created_at')->first();
////            $q->last_action = Log::where('user_id',$q->id)
////                                 ->orderBy('created_at', 'desc')
////                                 ->selectRaw('created_at')->first();
//                return $q;
//            });

        return response()->json($users);
    }

    // filter trashed users according user type ( organization_id , type sub (connected location)  , descendant )
    public function trashed(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('trashed', User::class);
        $user = Auth::user();
        
        $users = User::onlyTrashed()->descendants($user->organization_id);
        if (!$user->super_admin) {
            $users->where(function ($q) use ($user) {
                $q->where('parent_id', $user->id);
                $q->where('id', '<>', $user->id);
            });
        }
        
        $itemsCount = $request->input('itemsCount');
        $records_per_page = Helpers::recordsPerPage($itemsCount,$users->count());

        return response()->json($users->orderBy('firstname')->orderBy('lastname')->paginate($records_per_page));
    }

    // restore trashed user using user id
    public function restore(Request $request, $id)
    {
        $user = User::onlyTrashed()->findOrFail($id);
        $this->authorize('restore', $user);
        $user->setUpdateContext(User::UPDATE_CONTEXT_RESTORE);
        if ($user->trashed()) {
            $user->restore();
        }
        return response()->json($user);
    }

    // save new user on db according inputs and  organization_id of auth user
    public function store(Request $request)
    {
        $this->authorize('create', User::class);
        $rules = User::getValidatorRules();
        
        $authUser = Auth::user();
        $organization = \Organization\Model\Organization::find($authUser->organization_id);
        if (!$organization || !$organization->parent_id) {
            $rules['organization_id'] = 'required';
        }
//        $this->validate($request, $rules);

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $password = $request->input('password');
        
        $user = new User();
        $user->password        = User::hashPassword($password);
        $user->parent_id       = $authUser->id;
        $user->status          = User::STATUS_ACTIVE;
//        if ($organization && $organization->parent_id) {
//            $user->organization_id = $authUser->organization_id;
//        } else {
        $user->organization_id = $request->input('organization_id');
//        }

        $inputs =array("username",'email', "firstname","lastname", "job_title", "mobile", "type","connect_with_sponsor","wataniya","id_card_number");

        foreach ($inputs as $key){
            if(isset($request->$key)){
                $user->$key = $request->$key;
            }
        }

        $role_id = $request->input('role_id');

        if(!$authUser->connect_with_sponsor){
            $user->connect_with_sponsor = 0;
        }

        $user->saveWithRoles($role_id);

        if($user){
            if($user->type == 2){
                UserOrg::resetOrgs($request->user_range,$user->id);
            }
        }
        Notification::send([$user], new CreateUser($user, $password));
        return response()->json($user);
    }

    // get user details using user id
    public function show($id)
    {
        $user = User::findWithRoleId($id);
//        $this->authorize('view', $user);
        if($user){
            if($user->type == 2){
                $user->user_range= UserOrg::getUserOrg($user->id);
            }
        }
        return response()->json($user);
    }

    // update user details using user id
    public function update(Request $request, $id)
    {

        $user = User::findOrFail($id);
        $this->authorize('update', $user);
        
        $rules = User::getValidatorRules();
        unset($rules['password'], $rules['password_confirmation'], $rules['username']);
        $rules['id_card_number'] = 'required|unique:char_users,id_card_number,' . $user->id ;
        $rules['email'] = 'required|unique:char_users,email,' . $user->id ;

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $inputs =array('email', "firstname","lastname", "job_title", "mobile", "type","connect_with_sponsor","wataniya","id_card_number");

        foreach ($inputs as $key){
            if(isset($request->$key)){
                $user->$key = $request->$key;
            }
        }
        $role_id = $request->input('role_id');

        $authUser = Auth::user();
        if(!$authUser->connect_with_sponsor){
            $user->connect_with_sponsor = 0;
        }

        $user->saveWithRoles($role_id);
        if($user){
            if($user->type == 2){
                UserOrg::resetOrgs($request->user_range,$user->id);
            }
        }
        return response()->json($user);
    }

    // update user status  using user id
    public function status(Request $request, $action, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('status', $user);
        
        $user->status = ($action == 'activate')? User::STATUS_ACTIVE : User::STATUS_INACTIVE;
        if ($user->status === User::STATUS_ACTIVE) {
            $user->setUpdateContext(User::UPDATE_CONTEXT_ACTIVATE);
        } else {
            $user->setUpdateContext(User::UPDATE_CONTEXT_DEACTIVATE);
        }
        $user->saveOrFail();
        return response()->json($user);
    }

    // update user permissions using user id
    public function permissionsUpdate(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('permissionsUpdate', $user);
        
        $allow = $request->input('allow');
        $deny = $request->input('deny');
        $user->savePermissions($allow, $deny);
        return response()->json($user);
    }

    // get user permissions using user id
    public function permissions($id)
    {
        $user = User::findOrFail($id);
        $this->authorize('permissions', $user);
        
        return response()->json($user->permissions());
    }

    // destroy user using user id
    public function destroy(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->authorize('delete', $user);
        
        $user->delete();
        return response()->json($user);
    }

    // automatic logout user using user id
    public function logout(Request $request, $id)
    {

        $user = User::findOrFail($id);
        $user_name =  $user->firstname .' ' .$user->lastname;

        $tokens =\DB::table('oauth_access_tokens')
            ->where(function($q) use ($id){
                $q->where('user_id',$id);
                $q->where('revoked',0);
            })->orderBy('updated_at','asc')->get() ;


        $tokens_list =[];
        foreach($tokens as $token) {
            $token_id = "'".$token->id."'";
            $tokens_list[] = $token_id;
            \DB::table('oauth_access_tokens')->where('id',$token->id)->update(['revoked'=>1]);
            \DB::table('oauth_refresh_tokens')
                ->where(function($q) use ($token){
                    $q->where('access_token_id',$token->id);
                    $q->orwhere('id',$token->id);
                })->update(['revoked'=>1]);

        }

        \Log\Model\Log::saveNewLog('AUTOMATIC_LOGOUT',trans('common::application.Automatic logout') . ' : ' . $user_name);
        return response()->json($user);
    }

    // automatic logout users using user id (group)
    public function logOutGroup(Request $request)
    {

        $users = [] ;
        $selected = $request->selected ;

        if ($selected == true || $selected == 'true'){
            $users = $request->users ;
        }else{
            $users = User::onlineFilter(['action' =>'list']);
        }

        foreach ($users as $key =>$user){

            $user_name =  $user->firstname .' ' .$user->lastname;

            $tokens =\DB::table('oauth_access_tokens')
                ->where(function($q) use ($user){
                    $q->where('user_id',$user->id);
                    $q->where('revoked',0);
                })->orderBy('updated_at','asc')->get() ;


            $tokens_list =[];
            foreach($tokens as $token) {
                $token_id = "'".$token->id."'";
                $tokens_list[] = $token_id;
                \DB::table('oauth_access_tokens')->where('id',$token->id)->update(['revoked'=>1]);
                \DB::table('oauth_refresh_tokens')
                    ->where(function($q) use ($token){
                        $q->where('access_token_id',$token->id);
                        $q->orwhere('id',$token->id);
                    })->update(['revoked'=>1]);

            }

            \Log\Model\Log::saveNewLog('AUTOMATIC_LOGOUT',trans('common::application.Automatic logout') . ' : ' . $user_name);
        }

        return response()->json($user);
    }
}
