<?php

namespace Auth\Observer;

use Log\Model\Log;

abstract class AbstractObserver
{
    public function newLogModel()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $id = $user? $user->id : 0;
        $log = new Log();
        $log->user_id = $id;
        $log->ip = request()->ip();
        $log->created_at = date('Y-m-d H:i:s');
        
        return $log;
    }
}