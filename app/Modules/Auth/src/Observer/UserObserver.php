<?php

namespace Auth\Observer;

use Auth\Model\User;

class UserObserver extends AbstractObserver
{
    const ACTION_USER_CREATED = 'USER_CREATED';
    const ACTION_USER_UPDATED = 'USER_UPDATED';
    const ACTION_USER_DELETED = 'USER_DELETED';
    const ACTION_USER_PASSWORD_CHANGED = 'USER_PASSWORD_CHANGED';
    const ACTION_USER_PASSWORD_RESET = 'USER_PASSWORD_RESET';
    const ACTION_USER_ACTIVATED = 'USER_ACTIVATED';
    const ACTION_USER_DEACTIVATED = 'USER_DEACTIVATED';
    const ACTION_USER_RESTORED = 'USER_RESTORED';
    
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {
        $log = $this->newLogModel();
        $log->action = self::ACTION_USER_CREATED;
        $log->message = sprintf('قام بإنشاء المستخدم "%s %s"', $user->firstname, $user->lastname);
        $log->save();
    }
    
    /**
     * Listen to the User deleted event.
     *
     * @param  User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $log = $this->newLogModel();
        
        switch($user->getUpdateContext()) {
            case User::UPDATE_CONTEXT_PASSWORD_CHANGE:
                $log->action = self::ACTION_USER_PASSWORD_CHANGED;
                $log->message = sprintf('قام بتغيير كلمة المرور الخاصة بالمستخدم "%s %s"', $user->firstname, $user->lastname);
                break;
            case User::UPDATE_CONTEXT_PASSWORD_RESET:
                $log->user_id = $user->id;
                $log->action = self::ACTION_USER_PASSWORD_RESET;
                $log->message = 'قام بإعادة تعيين كلمة المرور الخاصة به';
                break;
            case User::UPDATE_CONTEXT_ACTIVATE:
                $log->action = self::ACTION_USER_ACTIVATED;
                $log->message = sprintf('قام بتفعيل حساب المستخدم "%s %s"', $user->firstname, $user->lastname);
                break;
            case User::UPDATE_CONTEXT_DEACTIVATE:
                $log->action = self::ACTION_USER_DEACTIVATED;
                $log->message = sprintf('قام بتعطيل حساب المستخدم "%s %s"', $user->firstname, $user->lastname);
                break;
            case User::UPDATE_CONTEXT_RESTORE:
                //$this->restored($user);
                return;
            default:
                $log->action = self::ACTION_USER_UPDATED;
                $log->message = sprintf('قام بتحديث بيانات المستخدم "%s %s"', $user->firstname, $user->lastname);
        }
        
        $log->save();
    }
    
    /**
     * Listen to the User deleted event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        if ($user->trashed()) {
            $user->update(array(
                'status' => User::STATUS_DELETED,
            ));
        }
        
        $log = $this->newLogModel();
        $log->action = self::ACTION_USER_DELETED;
        $log->message = sprintf('قام بحذف المستخدم "%s %s"', $user->firstname, $user->lastname);
        $log->save();
    }
    
    /**
     * Listen to the User restored event.
     *
     * @param  User  $user
     * @return void
     */
    public function restored(User $user)
    {       
        $log = $this->newLogModel();
        $log->action = self::ACTION_USER_RESTORED;
        $log->message = sprintf('قام باستعادة المستخدم "%s %s"', $user->firstname, $user->lastname);
        $log->save();
    }
    
}