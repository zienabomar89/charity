<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserPermission extends Model
{
    const ALLOW = 1;
    const DENY = 0;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_users_permissions';
    
    protected $fillable = ['user_id', 'permission_id', 'allow', 'created_at'];
    
    public $incrementing = false;
    public $timestamps   = false;
    
    public static function deleteUserPermissions($user_id)
    {
        return DB::table('char_users_permissions')->where('user_id', '=', $user_id)->delete();
    }
}