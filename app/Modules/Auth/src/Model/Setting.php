<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_settings';
    
    protected $fillable = ['id', 'organization_id', 'value'];
    
    public $incrementing = false;
    public $timestamps   = false;
    
    protected $ids = [
        'auth_throttlesLogins_maxAttempts',
        'auth_throttlesLogins_decayMinutes',
    ];

    public function getThrottlesLoginsMaxAttempts()
    {
        return self::where('id', 'auth_throttlesLogins_maxAttempts')
                ->where('organization_id', 0)->value('value');
    }
    
    public function setThrottlesLoginsMaxAttempts($value)
    {
        return self::updateOrCreate(array(
            'id' => 'auth_throttlesLogins_maxAttempts',
            'organization_id' => 0,
            'value' => (int) $value,
        ));
    }
    
    public function getThrottlesLoginsDcayMinutes()
    {
        return self::where('id', 'auth_throttlesLogins_decayMinutes')
                ->where('organization_id', 0)->value('value');
    }
    
    public function setThrottlesLoginsDcayMinutes($value)
    {
        return self::updateOrCreate(array(
            'id' => 'auth_throttlesLogins_decayMinutes',
            'organization_id' => 0,
            'value' => (int) $value,
        ));
    }


    public static function getIgnoreGovServices()
    {

        return false;
        $settings= self::where(['id'=>'gov_service'])->first();

        // ignore gov. service
        $gov_service = false;
        if ($settings){
            if ($settings->value == 1){
                $gov_service = true;
            }
        }

        return $gov_service;

    }


}