<?php

namespace Auth\Model;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use \App\Http\Helpers;

class User extends \App\User
{
    use SoftDeletes;
    
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DELETED  = 3;
    
    const UPDATE_CONTEXT_NORMAL          = 1;
    const UPDATE_CONTEXT_PASSWORD_CHANGE = 2;
    const UPDATE_CONTEXT_PASSWORD_RESET  = 3;
    const UPDATE_CONTEXT_ACTIVATE        = 4;
    const UPDATE_CONTEXT_DEACTIVATE      = 5;
    const UPDATE_CONTEXT_RESTORE         = 6;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];   
    
    protected static $rules;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_users';
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    protected $permissions = [];
    
    protected $appends = ['status_name', 'status_options'];
    
    protected $updateContext = self::UPDATE_CONTEXT_NORMAL;
    
    public function getUpdateContext()
    {
        return $this->updateContext;
    }
    
    public function setUpdateContext($context)
    {
        if (!in_array($context, array(
            self::UPDATE_CONTEXT_NORMAL,
            self::UPDATE_CONTEXT_PASSWORD_CHANGE,
            self::UPDATE_CONTEXT_PASSWORD_RESET,
            self::UPDATE_CONTEXT_ACTIVATE,
            self::UPDATE_CONTEXT_DEACTIVATE,
            self::UPDATE_CONTEXT_RESTORE,
        ))) {
            $context = self::UPDATE_CONTEXT_NORMAL;
        }
        $this->updateContext = $context;
        return $this;
    }

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'username'  => 'required|unique:char_users,username|min:3|max:255',
                'id_card_number'  => 'required|unique:char_users,username',
                'password'  => 'required|confirmed',
                'password_confirmation' => 'required',
                'email'     => 'required|email|unique:char_users,email|max:255',
                'firstname' => 'required|max:255',
                'no_sessions_at_time' => 'required|min:1',
                'lastname'  => 'required|max:255',
                //'organization_id' => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public function organization()
    {
        return $this->belongsTo('Organization\Model\Organization', 'organization_id', 'id');
    }
    
    /**
     * Scope a query to only children.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $ancestor_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDescendants($query, $ancestor_id)
    {
        return $query->whereIn('organization_id', function($query) use($ancestor_id) {
            $query->select('descendant_id')
                  ->from('char_organizations_closure')
                  ->where('ancestor_id', '=', $ancestor_id);
        });
    }
    
    /**
     * Scope a query to only children.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, $filters)
    {
        $defaults = array(
            'organization_id' => null,
            'status'          => null,
            'username'        => null,
            'email'           => null,
            'name'            => null,
            'organization_name' => false,
        );
        $options = array_merge($defaults, $filters);
        if ($options['organization_id']) {
            $query->where('organization_id', '=', $options['organization_id']);
        }
        if ($options['status']) {
            $query->where('status', '=', $options['status']);
        }
        if ($options['email']) {
            $query->where('email', '=', $options['email']);
        }
        if ($options['username']) {
            $query->whereRaw("normalize_text_ar(username) like  normalize_text_ar(?)", "%{$options['username']}%");

        }
        if ($options['name']) {
            $query->whereRaw('CONCAT(firstname, lastname) LIKE \'%' . $options['name'] . '%\'');
        }
        if ($options['organization_name']) {
            $query->with('organization');
        }
        return $query;
    }


    public function findForPassport($username)
    {
        $user = self::query()
                ->where('username', $username)
                ->where('status', self::STATUS_ACTIVE)
                ->whereNull('deleted_at')
                // Allow only users from active organizations
                ->whereIn('organization_id', function($query) {
                    $query->select('id')
                          ->from('char_organizations')
                          ->where('status', '=', \Organization\Model\AbstractOrganization::STATUS_ACTIVE)
                          ->whereNull('deleted_at');
                })
                ->first();
        return $user;
    }
    
    public static function hashPassword($password)
    {
        return Hash::make($password);
    }
    
    public static function checkHash($password, $hashedPassword)
    {
        return Hash::check($password, $hashedPassword);
    }
    
    public function updatePassword($password)
    {
        $password = self::hashPassword($password);
        return $this->update(array(
            'password' => $password,
        ));
    }
    
    public function permissions($id = null)
    {
        if (null === $id) {
            $id = $this->id;
        }
        
        if ($id === $this->id && !empty($this->permissions)) {
            return $this->permissions;
        }
        
        /*$permissions = DB::table('char_users_roles')
                ->join('char_roles_permissions', 'char_users_roles.role_id', '=', 'char_roles_permissions.role_id')
                ->join('char_permissions', 'char_roles_permissions.permission_id', '=', 'char_permissions.id')
                ->select('char_permissions.*')*/
        $permissions = DB::table('auth_users_permissions_view')
                ->where('user_id', '=', $id)
                ->orderBy('code')
                ->get();
        
        $this->permissions = $permissions;
        return $this->permissions;
    }

    public function organizationName($id = null)
    {
        if (null === $id) {
            $id = $this->id;
        }
        return $this->organization;


    }
    
    public function hasPermission($code, $id = null)
    {
        if (null === $id) {
            $id = $this->id;
        }
        
        // Super User!
        if ($this->super_admin) {
            return true;
        }
        
        if (!is_array($code)) {
            $code = array($code);
        }
        $set = [];
        $permissions = $this->permissions($id);
        foreach ($permissions as $permission) {
            if(in_array($permission->code, $code)) {
                $set[] = $permission;
            }
        }
        
        foreach ($set as $permission) {
            $allow = (int) $permission->allow;
            if (!$permission->role_id) {
                if ($allow == 0) {
                    return false;
                }
                if ($allow == 1) {
                    return true;
                }
            }
            return ($allow == 1)? true : false;
        }
        
        return false;
        
        /*$permission = DB::table('char_users_roles')
                ->join('char_roles_permissions', 'char_users_roles.role_id', '=', 'char_roles_permissions.role_id')
                ->join('char_permissions', 'char_roles_permissions.permission_id', '=', 'char_permissions.id')
                ->select('char_permissions.*')
                ->where('char_users_roles.user_id', '=', $id);
        $permission = DB::table('auth_users_permissions_view')
                ->where('user_id', '=', $id);
        if (is_array($code)) {
            $permission->whereIn('code', $code);
        } else {
            $permission->where('code', '=', $code);
        }
        
        return $permission->first();*/
    }
    
    public static function findWithRoleId($id)
    {
        $user = DB::table('char_users')
                ->leftJoin('char_users_roles', 'char_users.id', '=', 'char_users_roles.user_id')
                ->select('char_users.*', 'char_users_roles.role_id')
                ->where('char_users.id', '=', $id)
                ->first();

        if (is_null($user)) {
            throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(get_class());
        }
        
        return (new self())->forceFill(get_object_vars($user));
    }
    
    public function saveWithRoles($roles)
    {
        if (!is_array($roles)) {
            $roles = array($roles);
        }
        return $this->getConnection()->transaction(function () use ($roles) {
            $this->save();
            $user_id = $this->id;
            
            UserRole::deleteUserRoles($this->id);
            foreach ($roles as $role_id) {
                $data = [
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                UserRole::create($data);
            }
        });
    }
    
    public function savePermissions($allow, $deny)
    {
        if (!is_array($allow)) {
            $allow = array($allow);
        }
        if (!is_array($deny)) {
            $deny = array($deny);
        }
        return $this->getConnection()->transaction(function () use ($allow, $deny) {
            UserPermission::deleteUserPermissions($this->id);
            foreach ($allow as $permission_id) {
                $data = [
                    'user_id' => $this->id,
                    'permission_id' => $permission_id,
                    'allow'   => UserPermission::ALLOW,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
                ];
                UserPermission::create($data);
            }
            foreach ($deny as $permission_id) {
                $data = [
                    'user_id' => $this->id,
                    'permission_id' => $permission_id,
                    'allow'   => UserPermission::DENY,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
                ];
                UserPermission::create($data);
            }
        });
    }
    
    public static function status($value = null)
    {
        $options = array(
            self::STATUS_ACTIVE => trans('auth::application.active'),
            self::STATUS_INACTIVE => trans('auth::application.inactive'),
            self::STATUS_DELETED => trans('auth::application.deleted'),
        );
        
        if (null === $value) {
            return $options;
        }
        
        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }
    
    public function getStatus()
    {
        return self::status($this->status);
    }
    
    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }
    
    public function getStatusOptionsAttribute()
    {
        return self::status();
    }
    
    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('Auth\Model\Role', 'char_users_roles', 'user_id', 'role_id');
    }


    public static function userList()
    {
        $user = \Auth::user();
        $organization_id  = $user->organization_id;
        $UserType=$user->type;

        $users = DB::table('char_users')
            ->selectRaw("char_users.id, CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name ");

        if($UserType == 2) {

            $users->where(function ($anq) use ($organization_id,$user) {
                $anq->where('char_users.organization_id',$organization_id);
                $anq->orwherein('char_users.organization_id', function($quer) use($user) {
                    $quer->select('organization_id')
                        ->from('char_user_organizations')
                        ->where('user_id', '=', $user->id);
                });
            });
        }else{
            $users->where(function ($anq) use ($organization_id) {
                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                $anq->wherein('char_users.organization_id', function($query) use($organization_id) {
                    $query->select('descendant_id')
                        ->from('char_organizations_closure')
                        ->where('ancestor_id', '=', $organization_id);
                });
            });
        }

        return $users->get();

        
    }

    public static function userListOfTags($except)
    {
        $user = \Auth::user();
//        connect_with_sponsor
        $organization_id  = $user->organization_id;

        $users = DB::table('char_users')
                 ->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name ,char_users.id")
                 ->whereNotIn('id',$except)
                 ->where(function ($query) use ($organization_id,$user , $except) {
                     if(!$user->super_admin) {
//                         $query->where('parent_id',$user->id)
//                                ->orWhere(function($q)use($user,$organization_id) {
//                                  $q->where('parent_id',$user->id)
//                                    ->where('id', $user->parent_id)
//                                    ->where('organization_id', '=', $organization_id);
//                               })
//                               ->orWhere(function($q)use($user,$organization_id) {
//                                  $q->where('type',$user->type)
//                                     ->where('organization_id', '=', $organization_id);
//                             });
                         if($user->type == 1){
                             $query->where(function($q)use($user,$organization_id) {
                                          $q->where('id','!=',$user->id);
                                          $q->where(function ($anq) use ($organization_id,$user) {
                                                 $anq->where('organization_id',$organization_id);
                                                 $anq->orwherein('organization_id', function($decq) use($organization_id) {
                                                     $decq->select('descendant_id')
                                                         ->from('char_organizations_closure')
                                                         ->where('ancestor_id', '=', $organization_id);
                                                 });
                                            });
                             });
                             $query->orWhere('super_admin', '=', 1);
                             $query->orWhere(function($q)use($user) {
                                 $q->where('id','!=',$user->id);
                                 $q->WhereIn('type',[6,1,4]);
                             });
                         }
                         elseif($user->type == 2){

                             $query->where(function($q)use($user,$organization_id) {
                                 $q->where('id','!=',$user->id);
                                 $q->where(function ($anq) use ($organization_id,$user) {
                                     $anq->where('organization_id',$organization_id);
                                     $anq->orwherein('organization_id', function($decq) use($user) {
                                         $decq->select('organization_id')
                                             ->from('char_user_organizations')
                                             ->where('user_id', '=', $user->id);
                                     });
                                 });
                             });

                             $query->orWhere('super_admin', '=', 1);
                             $query->orWhere('id', '=', $user->parent_id);
                             $query->orWhere(function($q)use($user) {
                                 $q->where('id','!=',$user->id);
                                 $q->where('parent_id',$user->parent_id);
                                 $q->where('type',2);
                             });

                         }
                         elseif($user->type == 4){
                             $query->where(function($q)use($user,$organization_id) {
                                 $q->where('id','!=',$user->id);
                                 $q->where('connect_with_sponsor',1);
                                 $q->WhereIn('organization_id', function($qtuery) use($organization_id) {
                                     $qtuery->select('organization_id')
                                         ->from('char_organization_sponsors')
                                         ->where('sponsor_id', '=', $organization_id);
                                 });
                             });
                             $query->orWhere('super_admin', '=', 1);
                         }
                         else{
                             $query->where(function($q)use($user,$organization_id) {
                                 $q->where('id','!=',$user->id);
                                 $q->where('id','=',$user->parent_id);
                                 $q->orWhere('super_admin', '=', 1);
                                 $q->where(function ($anq) use ($organization_id,$user) {
                                     $anq->where('organization_id',$organization_id);
                                     $anq->orwherein('organization_id', function($decq) use($organization_id) {
                                         $decq->select('descendant_id')
                                             ->from('char_organizations_closure')
                                             ->where('ancestor_id', '=', $organization_id);
                                     });
                                     if($user->connect_with_sponsor == 1 ){
                                         $anq->orWhereIn('organization_id', function($qry) use($organization_id) {
                                             $qry->select('sponsor_id')
                                                 ->from('char_organization_sponsors')
                                                 ->where('organization_id', '=', $organization_id);
                                         });
                                     }
                                 });
                             });
                         }
                     }else{
                         $query->where('id','!=',$user->id);
                     }
                 });

        return $users->get();

    }

    public static function filters($options)
    {

        $condition = [];
        $user = \Auth::user();
        $UserType=$user->type;
        $UserOrg = $user->organization_id;

        $filters =array('status','username','email' ,'name' ,'type');

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = [$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $users = self::query()->with(['organization']);

        $organization_id = null;
        if(isset($options['organization_id']) && $options['organization_id'] !=null && $options['organization_id'] !="") {
            $organization_id =$options['organization_id'];
        }

        if(!is_null($organization_id)){
//                $users->where('id', '!=', $user->id);
            $users->where(function ($q) use ($user , $organization_id) {
                $q->where('organization_id',$organization_id);
            });
        }
        else{
            if($UserType == 2) {
//                $users->where('id', '!=', $user->id);
                $users->where(function ($q) use ($user,$UserOrg) {
                    $q->where('organization_id',$UserOrg);
                    $q->orwhereIn('organization_id', function ($query) use ($user) {
                        $query->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }
            else{
//                $users->where('id', '!=', $user->id);
                $users->where(function ($q) use ($user , $UserOrg) {
//                    $ancestor = \Organization\Model\Organization::getAncestor($UserOrg);
                    $q->where('organization_id',$UserOrg);
                    $q->orwhereIn('organization_id', function($decq) use($UserOrg) {
                        $decq->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $UserOrg);
                    });
                });
            }
        }

        if (count($condition) != 0) {
            $users =  $users
                ->where(function ($q) use ($condition,$options) {
                    $str = ['usersname','content','header', 'footer','notes'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            }else if ($key == 'name') {
                                $q->whereRaw('CONCAT(firstname, lastname) LIKE \'%' . $value . '%\'');
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }

                });
        }

        if(isset($options['user_range'])){
            if($options['user_range'] !=null &&
                $options['user_range'] !="" && $options['user_range'] !=[] && sizeof($options['user_range']) != 0) {
                if($options['user_range'][0]==""){
                    unset($options['user_range'][0]);
                }
                $user_range =$options['user_range'];

                if(!empty($user_range)){
                    $users->wherein('id', function ($sQ) use ($user_range) {
                        $sQ->select('user_id')
                            ->from('char_user_organizations')
                            ->wherein('organization_id', $user_range);
                    });
                }
            }
        }

       if(isset($options['online'])){
           $users_ids =[];
           $logged_users = \DB::table('oauth_access_tokens')
                               ->where(function($q) use ($user){
                                   $q->where('revoked',0);
                               })
                               ->get();

           foreach ($logged_users as $k=>$v){
               if(!in_array($v->user_id, $users_ids)) {
                   $users_ids[]=(int)$v->user_id;
               }
           }

           if(!empty($users_ids)){
               $users->wherein('user_id', $users_ids);
           }
       }

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount'] : 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$users->count());
        return $users->orderBy('firstname')->orderBy('lastname')->paginate($records_per_page);

    }

    public static function onlineFilter($options)
    {

        $condition = [];
        $user = \Auth::user();
        $UserType=$user->type;
        $UserOrg = $user->organization_id;

        $filters =array('status','username','email' ,'name' ,'type');
        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = [$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $language_id =  \App\Http\Helpers::getLocale();
        $users = DB::table('char_users_online_view')
                    ->leftjoin('char_organizations_category_i18n', function($join) use ($language_id){
                        $join->on('char_organizations_category_i18n.category_id', '=','char_users_online_view.org_category_id' )
                            ->where('char_organizations_category_i18n.language_id',$language_id);
                    })->selectRaw("char_users_online_view.* ,
                                   CASE WHEN char_users_online_view.org_category_id is null THEN '-'
                                   Else char_organizations_category_i18n.name
                                   END as org_category_name ")
//                   ->where('user_id','!=',$user->id)
        ;


        if (count($condition) != 0) {
            $users =  $users
                ->where(function ($q) use ($condition,$options) {
                    $str = ['usersname','content','header', 'footer','notes'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            }else if ($key == 'name') {
                                $q->whereRaw("CONCAT(firstname, lastname) LIKE '%$value%' ");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }

                });
        }

        $organization_category_id=[];
        if(isset($options['organization_category_id']) && $options['organization_category_id'] !=null && $options['organization_category_id'] !="" &&
            $options['organization_category_id'] !=[] && sizeof($options['organization_category_id']) != 0) {
            if($options['organization_category_id'][0]==""){
                unset($options['organization_category_id'][0]);
            }
            $organization_category_id =$options['organization_category_id'];
        }

        if(!empty($organization_category_id)){
            $users->where(function ($anq) use ($organization_category_id) {
                $anq->wherein('org_category_id',$organization_category_id);
            });

        }

        $organizations=[];
        if(isset($options['organization_id']) && $options['organization_id'] !=null && $options['organization_id'] !="" &&
           $options['organization_id'] !=[] && sizeof($options['organization_id']) != 0) {
            if($options['organization_id'][0]==""){
                unset($options['organization_id'][0]);
            }
            $organizations =$options['organization_id'];
        }

        if(!empty($organizations)){
            $users->where(function ($anq) use ($organizations,$user) {
                $anq->wherein('organization_id',$organizations);
            });

        }else{
            if($UserType == 2) {
                $users->where(function ($anq) use ($UserOrg,$user) {
                    $anq->where('organization_id',$UserOrg);
                    $anq->orwherein('organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }
            else{
                $users->where(function ($anq) use ($UserOrg) {
                    $anq->wherein('organization_id', function($quer) use($UserOrg) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $UserOrg);
                    });
                });
            }
        }

        $user_range = [];
        if(isset($options['user_range'])){
            if($options['user_range'] !=null &&
                $options['user_range'] !="" && $options['user_range'] !=[] && sizeof($options['user_range']) != 0) {
                if($options['user_range'][0]==""){
                    unset($options['user_range'][0]);
                }
                $user_range =$options['user_range'];
            }
        }
        if(!empty($user_range)){
            $users->wherein('user_id', function ($sQ) use ($user_range) {
                $sQ->select('user_id')
                    ->from('char_user_organizations')
                    ->wherein('organization_id', $user_range);
            });
        }
//        return [$condition , $user_range , $organizations , $organization_category_id];

        $users->groupBy('user_id');

//        return $users->orderBy('firstname')->orderBy('lastname')->ToSql();

        if(isset($options['action'])){
            if($options['action'] == 'list') {
                return $users->orderBy('firstname')->orderBy('lastname')->get();
            }
        }

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount'] : 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$users->count());
        return $users->orderBy('firstname')->orderBy('lastname')->paginate($records_per_page);

    }

    public static function userHasPermission($code , $orgs){

        $query = \DB::table('auth_users_permissions_view')
            ->select('user_id')
            ->where('code', '=', $code);

        if(sizeof($orgs) >0){
            $query->whereIn('organization_id', $orgs);
        }

        $return = $query->get();

        $users = array();
        foreach ($return as $key=>$value){
            if(!in_array($value->user_id,$users)){
                $users[]=$value->user_id;
            }
        }
        return $users;
    }

    public static function getMobileNumbers($cont_type,$source ,$target)
    {

        $query = \DB::table('char_users');

        if($cont_type == 0 || $cont_type == '0'){
            $query->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name,char_users.wataniya as mobile");
        }else{
            $query->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name,char_users.mobile as mobile");
        }

        $query->whereIn('id',$target);

        return $query->get();

    }

    public static function getLocale(){

        $language_id = 1;
        $lang = 'ar';

        $user = User::findOrFail(\Illuminate\Support\Facades\Auth::user()->id);

        if(!is_null($user->locale )){
            if($user->locale  == 'en'){
                $lang = 'en';
                $language_id = 2;
            }
        }

        \App::setLocale($lang);
        \Application\Model\I18nableEntity::setGlobalLanguageId($language_id);

        return $language_id;
    }

    public static function OrgManager($organization_id){

        $query = \DB::table('char_users')
                    ->join('char_organizations','char_organizations.id',  '=', 'char_users.organization_id')
                    ->where(function ($q) use ($organization_id) {

                        $q->where('char_users.organization_id', '=', $organization_id);

                        $q->where(function($qr) use($organization_id) {

                            $qr->where(function($q_) use($organization_id) {
                                $q_->where('char_users.type', '=', 3);//organization_manager
                                $q_->whereIn('char_organizations.level', [1,3]); // 1 => master_center , 3 => branches
                            });

                            $qr->orwhere(function($q_) use($organization_id) {
                                $q_->where('char_users.type', '=', 1); // sector_manager
                                $q_->where('char_organizations.level', '=', 2); // branch_center
                            });

                        });

                    })
                    ->select('char_users.id')->first();


        return $query ? $query->id : null;

    }

    public static function OrgUser($organization_id,$except_id){

        return \DB::table('char_users')
                    ->where(function ($q) use ($organization_id,$except_id) {
                        $q->where('organization_id', '=', $organization_id);
                        $q->where('id', '!=', $except_id);
                    })
                    ->selectRaw("char_users.id,
                        CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name")
                    ->get();

    }

}

