<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_roles';
    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name'  => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public static function fetch($options = array())
    {
        $default = array(
            'users' => false,
            'organization_name' => false,
            'user_id' => null,
            'organization_id' => null,
        );
        
        $options = array_merge($default, $options);
        
        $query = self::query()->select('char_roles.id','char_roles.user_id',
                                               'char_roles.name', 'char_roles.organization_id', 'char_roles.deleted', 'char_roles.level',
            'char_roles.created_at', 'char_roles.updated_at', 'char_roles.deleted_at');
        if ($options['users']) {
            $query->leftJoin('char_users_roles', 'char_users_roles.role_id', '=', 'char_roles.id')
                  ->addSelect(DB::raw('COUNT(char_users_roles.user_id) AS users'))
                  ->groupBy('char_roles.id', 'char_roles.name', 'char_roles.organization_id', 'char_roles.created_at', 'char_roles.updated_at', 'char_roles.deleted_at');
        }
        
        if ($options['organization_name']) {
            $query->join('char_organizations', 'char_roles.organization_id', '=', 'char_organizations.id')
                  ->addSelect('char_organizations AS organization_name');
        }
        
        if ($options['organization_id']) {
            $query->where('char_roles.organization_id', $options['organization_id']);
        }
        if ($options['user_id']) {
            $query->where('char_roles.user_id', $options['user_id']);
        }

        return $query->get();
    }

    public function saveWithPermissions($allow, $deny)
    {
        return $this->getConnection()->transaction(function () use ($allow, $deny) {
            $this->save();
            $role_id = $this->id;
            
            RolePermission::deleteRolePermissions($this->id);
            foreach ($allow as $permission_id) {
                $data = [
                    'role_id' => $role_id,
                    'permission_id' => $permission_id,
                    'allow' => 1,
                    'deleted' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
                ];
                RolePermission::create($data);
            }
            foreach ($deny as $permission_id) {
                $data = [
                    'role_id' => $role_id,
                    'permission_id' => $permission_id,
                    'allow' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => \Illuminate\Support\Facades\Auth::user()->id,
                ];
                RolePermission::create($data);
            }
        });
    }
    
    public function copy($organization_id = null)
    {
        $role = $this;
        return $this->getConnection()->transaction(function() use ($role, $organization_id) {
            if (null === $organization_id) {
                $organization_id = $role->organization_id;
            }

            if (!is_array($organization_id)) {
                $organization_id = array($organization_id);
            }
            
            $query = 'INSERT INTO char_roles_permissions
                    (role_id, permission_id, created_at)
                    SELECT :new_role_id, permission_id, now()
                    FROM char_roles_permissions
                    WHERE role_id = :role_id';
            
            foreach ($organization_id as $_id) {
                $copy = $role->replicate(array('organization_id'));
                $copy->organization_id = $_id;
                $copy->save();
                
                DB::statement($query, array(
                    'new_role_id' => $copy->id,
                    'role_id' => $role->id,
                ));
                
                $copies[] = $copy;
            }
            
            return $copies;
        });
    }
    
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('Auth\Model\User', 'char_users_roles', 'role_id', 'user_id');
    }
}