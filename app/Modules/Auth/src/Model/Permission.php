<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Permission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_permissions';
    
    protected $fillable = ['id', 'code', 'name', 'role_id'];
     
    public static function allForUser($id, $module_id = null, $role_id = null)
    {
        $params = ['user_id' => $id];
        $where = '';
        if ($module_id) {
            $where = 'AND p.module_id = :module_id';
            $params['module_id'] = $module_id;
        }
        
        if ($role_id) {
            $query = 'SELECT p.*, m.name AS module_name
                      r.role_id FROM char_permissions p
                      INNER JOIN app_modules m ON p.module_id = m.id
                      INNER JOIN char_roles_permissions rp ON p.id = rp.permission_id
                      INNER JOIN char_users_roles ur ON rp.role_id = ur.role_id 
                      LEFT JOIN char_roles_permissions r
                        ON p.id = r.permission_id
                        AND r.role_id = :role_id
                      WHERE ur.user_id = :user_id
                        ' . $where . '
                      ORDER BY m.name, p.code';
            $params['role_id'] = $role_id;
        } else {
            $query = 'SELECT p.*, m.name AS module_name
                      FROM char_permissions p
                      INNER JOIN app_modules m ON p.module_id = m.id
                      INNER JOIN char_roles_permissions rp ON p.id = rp.permission_id
                      INNER JOIN char_users_roles ur ON rp.role_id = ur.role_id 
                      WHERE ur.user_id = :user_id
                        ' . $where . '
                      ORDER BY m.name, p.code';
        }
        
        $entries = array();
        $results = DB::select($query, $params);

        foreach ($results as $row) {
            if (!isset($entries[$row->module_id])) {
                $entries[$row->module_id] = array(
                    'module_id' => $row->module_id,
                    'module_name' => $row->module_name,
                    'permissions' => [],
                );
            }
            $entries[$row->module_id]['permissions'][] = $row;
        }
        
        $return = [];
        foreach ($entries as $entry) {
            $return[] = $entry;
        }
        
        return $return;
    }
    
    public static function withUserPermissions($id, $options = array())
    {
        $query = self::query()->orderBy('char_permissions.code')->select('char_permissions.*');
        
        $query->leftJoin('char_users_permissions', function($join) use($id) {
            $join->on('char_users_permissions.permission_id', '=', 'char_permissions.id')
                 ->where('char_users_permissions.user_id', '=', $id);
            
        });
        $query->selectRaw('IFNULL(char_users_permissions.allow, -1) as allow');
        
        if (isset($options['with_module_name']) && $options['with_module_name']) {
            $query->join('app_modules', function($join) {
                $join->on( 'char_permissions.module_id', '=', 'app_modules.id')
                    ->where('app_modules.display', '=', 1);
            })->addSelect('app_modules.name as module_name');
        }
        if (isset($options['user_id']) && !empty($options['user_id'])) {
            $user_id = $options['user_id'];
            $query->whereIn('char_permissions.id', function($query) use ($user_id) {
                $query->select('permission_id')
                      ->from('auth_users_permissions_view')
                      ->where('user_id', $user_id);
            });
        }

        if (isset($options['crossed']) && $options['crossed']) {
            $query->whereIn('char_permissions.id',$options['allowed']);
        }


        $results = $query->get();
        
        if (isset($options['user_role']) && $options['user_role']) {
            $query = UserRole::query();
            $query->join('char_roles', 'char_users_roles.role_id', '=', 'char_roles.id')
                  ->join('char_roles_permissions', 'char_users_roles.role_id', '=', 'char_roles_permissions.role_id')
                  ->where('char_users_roles.user_id', '=', $id)
                  ->select('char_roles_permissions.permission_id', 'char_roles_permissions.allow', 'char_roles.name');

            if (isset($options['crossed']) && $options['crossed']) {
                $query->whereIn('char_roles_permissions.permission_id',$options['allowed']);
            }
            $roles_permissions = $query->get();
            foreach ($results as $entry) {
                foreach ($roles_permissions as $entry2) {
                    if ($entry->id == $entry2->permission_id) {
                        $entry->role_name = $entry2->name;
                        $entry->role_allow = $entry2->allow;
                    }
                }
            }
        }
        
        if (isset($options['group_by_module']) && $options['group_by_module']) {
            $return = $entries = array();
            foreach ($results as $row) {
                if (!isset($entries[$row->module_id])) {
                    $entries[$row->module_id] = array(
                        'module_id' => $row->module_id,
                        'module_name' => $row->module_name,
                        'permissions' => [],
                    );
                }
                $entries[$row->module_id]['permissions'][] = $row;
            }

            foreach ($entries as $entry) {
                $return[] = $entry;
            }
            
            return $return;
        }
        
        return $results;
    }

    public static function withRolePermissions($id, $options = array())
    {
        $query = self::query()->orderBy('char_permissions.code')->select('char_permissions.*');
        
        $query->leftJoin('char_roles_permissions', function($join) use($id) {
            $join->on('char_roles_permissions.permission_id', '=', 'char_permissions.id')
                 ->where('char_roles_permissions.role_id', '=', $id);
        });
        $query->selectRaw('IFNULL(char_roles_permissions.allow, -1) as allow');
        
        if (isset($options['with_module_name']) && $options['with_module_name']) {
            $query->join('app_modules', function($join) {
                $join->on( 'char_permissions.module_id', '=', 'app_modules.id')
                    ->where('app_modules.display', '=', 1);
            }) ->addSelect('app_modules.name as module_name');
        }
        if (isset($options['user_id']) && !empty($options['user_id'])) {
            $user_id = $options['user_id'];
            $query->whereIn('char_permissions.id', function($query) use ($user_id) {
                $query->select('permission_id')
                      ->from('auth_users_permissions_view')
                      ->where('user_id', $user_id);
            });
        }

        if (isset($options['crossed']) && $options['crossed']) {
            $query->whereIn('char_permissions.id',$options['allowed']);
        }

        $results = $query->get();
        if (isset($options['group_by_module']) && $options['group_by_module']) {
            $return = $entries = array();
            foreach ($results as $row) {
                if (!isset($entries[$row->module_id])) {
                    $entries[$row->module_id] = array(
                        'module_id' => $row->module_id,
                        'module_name' => $row->module_name,
                        'permissions' => [],
                    );
                }
                $entries[$row->module_id]['permissions'][] = $row;
            }

            foreach ($entries as $entry) {
                $return[] = $entry;
            }
            
            return $return;
        }
        
        return $results;
    }
    
    public static function fetch($options = array())
    {
        $query = self::query()->orderBy('char_permissions.code')->select('char_permissions.*');
        
        if (isset($options['with_module_name']) && $options['with_module_name']) {
            $query->join('app_modules', function($join) {
                $join->on( 'char_permissions.module_id', '=', 'app_modules.id')
                    ->where('app_modules.display', '=', 1);
            })
                  ->addSelect('app_modules.name as module_name');
        }
        if (isset($options['user_id']) && !empty($options['user_id'])) {
            $user_id = $options['user_id'];
            $query->whereIn('char_permissions.id', function($query) use ($user_id) {
                $query->select('permission_id')
                      ->from('auth_users_permissions_view')
                      ->where('user_id', $user_id);
            });
        }

        if (isset($options['crossed']) && $options['crossed']) {
            $query->whereIn('char_permissions.id',$options['allowed']);
        }

        $results = $query->get();
        if (isset($options['group_by_module']) && $options['group_by_module']) {
            $return = $entries = array();
            foreach ($results as $row) {
                if (!isset($entries[$row->module_id])) {
                    $entries[$row->module_id] = array(
                        'module_id' => $row->module_id,
                        'module_name' => $row->module_name,
                        'permissions' => [],
                    );
                }
                $entries[$row->module_id]['permissions'][] = $row;
            }

            foreach ($entries as $entry) {
                $return[] = $entry;
            }
            
            return $return;
        }
        
        return $results;
    }

    public static function allowedUserPermission($user_id)
    {
        $query = \DB::table('auth_users_permissions_view')
          ->selectRaw('permission_id')
         ->where('user_id', $user_id)->get();

        $allowed = [];
        if(sizeof($query) >0){
            foreach($query as $k=>$v){
                if(!in_array($v->permission_id,$allowed)){
                    $allowed[]=$v->permission_id;
               }
            }
        }


        return $allowed;
    }

    public static function allowedUserPermissionCode($user_id){
        $query = \DB::table('auth_users_permissions_view')
                    ->selectRaw('*')
                    ->where('user_id', $user_id)
                    ->orderBy('permission_id')
                    ->orderBy('created_at','desc')
                    ->get();

        $options['crossed'] = true;
        $pIds = [];
        $codes = [];
        $except = [];
        $code_map = [];
        $allowed = $query;

        $allowed_map = [];
        $last_update = [];
        if ($allowed) {
            foreach ($allowed as $entry) {
                if(!isset($allowed_map[$entry->permission_id])){
                    $allowed_map[$entry->permission_id] = [];
                    $code_map[$entry->permission_id] =$entry->code;
                }
                $allowed_map[$entry->permission_id][]=$entry;
            }

            foreach ($allowed_map as $permission_id => $entry) {
                if(sizeof($entry) > 1){
                    $max = $entry[0];
                    foreach ($entry as $key => $value) {
                        if (new \DateTime($max->created_at) < new \DateTime($value->created_at)) {
                            $max = $value;
                        }
                    }
                    $last_update[]= $max;
                }else{
                    $last_update[]= $entry[0];
                }

            }
            foreach ($last_update as $entry) {
                if($entry->allow == 1 || $entry->allow == '1'){
                    if(!in_array($entry->permission_id,$pIds)){
                        $codes[] = $code_map[$entry->permission_id];
                    }
                }else{
                    $except[]=$entry;
                }
            }
        }

        return $codes;

    }
}