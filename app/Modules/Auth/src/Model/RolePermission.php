<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RolePermission extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_roles_permissions';

    protected $fillable = ['role_id', 'permission_id', 'allow', 'created_at', 'created_by'];
    
    public $incrementing = false;
    public $timestamps   = false;
    
    public static function deleteRolePermissions($role_id)
    {
        return DB::table('char_roles_permissions')->where('role_id', '=', $role_id)->delete();
    }
}