<?php

namespace Auth\Model;

use Organization\Model\Organization;

class UserOrg extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_user_organizations';
    protected $fillable = ['organization_id','user_id'];
    public $timestamps = false;

    public static function getUserOrg($user_id)
    {
        return  Organization::where(function ($q) use ($user_id) {
            $q->whereIn('id', function($query) use($user_id) {
                $query->select('organization_id')
                    ->from('char_user_organizations')
                    ->where('user_id', '=', $user_id);
            });
        })->get();
    }

    public static function getUserOrgIds($user_id)
    {
        $return =  Organization::where(function ($q) use ($user_id) {
            $q->whereIn('id', function($query) use($user_id) {
                $query->select('organization_id')
                    ->from('char_user_organizations')
                    ->where('user_id', '=', $user_id);
            });
        })->get();

        $output = [];
        foreach ($return as $key=>$value){
            $output[]=$value->id;
        }

        return $output;
    }

    public static function resetOrgs($organizations,$user_id)
    {
        self::where(['user_id'=>$user_id])->delete();
        foreach ($organizations as $item){
            self::create([ 'organization_id' => $item ,'user_id' => $user_id]);
        }
        return true;
    }
}

