<?php

namespace Auth\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRole extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_users_roles';
    
    protected $fillable = ['user_id', 'role_id', 'created_at'];
    
    public $incrementing = false;
    public $timestamps   = false;
    
    public static function deleteUserRoles($user_id)
    {
        return DB::table('char_users_roles')->where('user_id', '=', $user_id)->delete();
    }
}