<?php

return array(
    'auth.users.manage' => 'إدارة مستخدمي النظام',
    'auth.users.view'   => 'عرض بيانات المستخدم',
    'auth.users.create' => 'إنشاء حساب مستخدم جديد',
    'auth.users.update' => 'تحرير بيانات مستخدم',
    'auth.users.delete' => 'حذف حساب مستخدم',
    'auth.users.changePassword' => 'تغيير كلمة المرور لحساب مستخدم',
    'auth.users.permissions' => 'تحرير الصلاحيات على مستوى المستخدم',
    'auth.users.status' => 'تفعيل وتعطيل حسابات المستخدمين',
    
    'auth.roles.manage' => 'إدارة مجموعات الصلاحيات',
    'auth.roles.view'   => 'عرض معلومات مجموعة الصلاحيات',
    'auth.roles.create' => 'إنشاء مجموعة صلاحيات جديدة',
    'auth.roles.update' => 'تحرير مجموعة صلاحيات',
    'auth.roles.delete' => 'حذف مجموعة صلاحيات',
    'auth.roles.copy'   => 'نسخ مجموعة صلاحيات',
    
    'auth.settings.manage' => 'التحكم في إعدادات الدخول إلى النظام',
);
