<?php

namespace App\Modules\Auth;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Auth\Model\User::class => \Auth\Policy\UserPolicy::class,
        \Auth\Model\Role::class => \Auth\Policy\RolePolicy::class,
        \Auth\Model\Permission::class => \Auth\Policy\PermissionPolicy::class,
        \Auth\Model\Setting::class => \Auth\Policy\SettingPolicy::class,
    ];
    
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'auth');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'auth');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
        $this->registeObservers();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'auth'
        );*/
    }
    
    protected function registeObservers()
    {
        \Auth\Model\User::observe(\Auth\Observer\UserObserver::class);
    }

}
