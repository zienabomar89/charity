<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl" data-ng-app="UserModule">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8">
        <title><?= trans('auth::application.reset_password') ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
        <link href="/themes/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/metronic/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="/themes/metronic/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css">
        <link href="/themes/metronic/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css">
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="/themes/metronic/pages/css/login-2-rtl.min.css" rel="stylesheet" type="text/css">
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico">
    </head>
    <!-- END HEAD -->

    <body class="login" ng-controller="ForgotPasswordController">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <h1>نظام إدارة الجمعيات الموحد</h1>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->

            <div class="alert" ng-class="{'alert-success': success, 'alert-danger': failed}" ng-if="message">
                <p>{{message}}
            </div>

            <form class="forget-form" style="display: block" role="form" method="POST" ng-submit="sendEmail()">
                    <div class="form-title">
                    <span class="form-title"><?= trans('auth::application.forgot_password') ?></span>
                    <span class="form-subtitle">Enter your e-mail to reset it.</span>
                </div>
                
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">{{'E-Mail Address' || translate}}</label>
                    <input ng-model="email" id="email" type="email" class="form-control form-control-solid placeholder-no-fix" name="email" value="" required placeholder="{{'EMail_Address' || translate}}">
                </div>

                <div class="form-actions">
                    <button type="submit" class="btn red uppercase"><span>{{'Send Password Reset Link' || translate}}</span></button>
                </div>
            </form>

        </div>
<!--        <div class="copyright"> --><?//= date('Y') ?><!-- © Nepras IT. </div>-->
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
        <script src="/themes/metronic/global/plugins/respond.min.js"></script>
        <script src="/themes/metronic/global/plugins/excanvas.min.js"></script> 
        <script src="/themes/metronic/global/plugins/ie8.fix.min.js"></script> 
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="/themes/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="/themes/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="/themes/metronic/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- <script src="/themes/metronic/pages/scripts/login.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->

        <script src="/app/scripts/query-string/query-string.js" type="text/javascript"></script>
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="/themes/metronic/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-resource.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/angular-cookies.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-oauth2.min.js" type="text/javascript"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script>
                                var gInLoginPage = true;
        </script>
        <script src="/app/modules/user/Module.js" type="text/javascript"></script>
        <script src="/app/modules/user/controllers/ForgotPasswordController.js" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
    </body>
</html>
