<div ng-controller="AccountController">
    <div class="alert alert-success" ng-show="success" role="alert">
        {{message}}
    </div>
    <div class="alert alert-danger" ng-show="error" role="alert">
        {{message}}
    </div>
    <form name="form" class="form-horizontal" novalidate="" role="form" ng-submit="submit()">
        <fieldset>
            <legend><?= trans('auth::application.account_information') ?></legend>
            <div class="form-group" ng-class="errorClass('email')">
                <label for="email" class="col-md-2 control-label"><?= trans('auth::application.email') ?></label>
                <div class="col-md-4">
                    <input type="email" class="form-control" id="email" name="email" placeholder="<?= trans('auth::application.email') ?>" value="{{email}}" 
                           ng-model="user.email" ng-required="true">
                    <span class="help-block" ng-show="form.email.$invalid && form.email.$dirty">{{errorMessage('email')}}</span>
                </div>
            </div>
            <div class="form-group" ng-class="errorClass('mobile')">
                <label for="mobile" class="col-md-2 control-label"><?= trans('auth::application.mobile') ?></label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="<?= trans('auth::application.mobile') ?>" value="{{mobile}}" 
                           ng-model="user.mobile" ng-required="true">
                    <span class="help-block" ng-show="form.mobile.$invalid && form.mobile.$dirty">{{errorMessage('mobile')}}</span>
                </div>
            </div>
        </fieldset>
        <fieldset>
            <legend><?= trans('auth::application.personal_information') ?></legend>
            <div class="form-group" ng-class="errorClass('firstname')">
                <label for="firstname" class="col-md-2 control-label"><?= trans('auth::application.firstname') ?></label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="firstname" name="firstname" placeholder="<?= trans('auth::application.firstname') ?>" value="{{firstname}}" 
                           ng-model="user.firstname" ng-required="true">
                    <span class="help-block" ng-show="form.firstname.$invalid && form.firstname.$dirty">{{errorMessage('firstname')}}</span>
                </div>
            </div>
            <div class="form-group" ng-class="errorClass('lastname')">
                <label for="lastname" class="col-md-2 control-label"><?= trans('auth::application.lastname') ?></label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="lastname" name="lastname" placeholder="<?= trans('auth::application.lastname') ?>" value="{{lastname}}" 
                           ng-model="user.lastname" ng-required="true">
                    <span class="help-block" ng-show="form.lastname.$invalid && form.lastname.$dirty">{{errorMessage('lastname')}}</span>
                </div>
            </div>
            <div class="form-group" ng-class="errorClass('job')">
                <label for="job" class="col-md-2 control-label"><?= trans('auth::application.job') ?></label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="job" name="job" placeholder="Position" value="{{job}}" 
                           ng-model="user.job_title" ng-required="true">
                    <span class="help-block" ng-show="form.job.$invalid && form.job.$dirty">{{errorMessage('job')}}</span>
                </div>
            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn blue"><span><?= trans('auth::application.save') ?></span></button>
            </div>
        </div>
    </form>
</div>