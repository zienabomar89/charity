<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="ar" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="ar" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="ar" dir="rtl" data-ng-app="UserModule">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8">
    <title> CharityApp |  {{'login'| translate}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <noscript>
        <meta http-equiv="refresh" content="0;URL='/no-javascript.html'" />
    </noscript>
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="/themes/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/bootstrap-toastr/toastr-rtl.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="/themes/metronic/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="/themes/metronic/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/themes/metronic/global/css/components-rounded-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/themes/metronic/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
    <link href="/themes/metronic/pages/css/login-3-rtl.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.4/css/fontawesome.min.css" integrity="sha384-jLKHWM3JRmfMU0A5x5AkjWkw/EYfGUAGagvnfryNV3F9VqM98XiIH7VBGVoxVSc7" crossorigin="anonymous">
    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="/logo.png" />
    <style>
        .login{ background-color:#364150!important}
        /*.login .content ,.login .content .form-actions{ background-color:#bbcdda !important;}*/
        @font-face {
            font-family: 'Droid Arabic Kufi';
            src: url('/app/css/font/DroidKufi-Regular.eot');
            src: url('/app/css/font/DroidKufi-Regular.eot?#iefix') format('embedded-opentype'),
            url('/app/css/font/DroidKufi-Regular.woff') format('woff'),
            url('/app/css/font/DroidKufi-Regular.woff2') format('woff2'),
            url('/app/css/font/DroidKufi-Regular.ttf') format('truetype'),
            url('/app/css/font/DroidKufi-Regular.svg#Droid Arabic Kufi') format('svg');
            font-weight: 400;
            font-style: normal;
        }
        @font-face {
            font-family: 'Droid Arabic Kufi';
            src: url('/app/css/font/DroidKufi-Bold.eot');
            src: url('/app/css/font/DroidKufi-Bold.eot?#iefix') format('embedded-opentype'),
            url('/app/css/font/DroidKufi-Bold.woff') format('woff'),
            url('/app/css/font/DroidKufi-Bold.woff2') format('woff2'),
            url('/app/css/font/DroidKufi-Bold.ttf') format('truetype'),
            url('/app/css/font/DroidKufi-Bold.svg#Droid Arabic Kufi') format('svg');
            font-weight: 700;
            font-style: normal;
        }


        body,h1,h2,h3,h4,h5,h6,p{font-family: Droid Arabic Kufi, serif !important;}
        *,.btn > span { font-size:11px !important; }
        .login .content .forget-password{ font-size:12px !important; }
        h3{font-size: 21px !important;}
        .login .content .input-icon { border-right : 2px solid #67809f!important}

        .login .content .input-icon {
            border-right: 2px solid #b3b334!important;
        }

        .btn.blue-hoki-:not(.btn-outline) {
            color: #FFF;
            background-color: #b3b334;
            border-color: #b3b334;
        }

        .btn.blue-hoki-:not(.btn-outline).active, .btn.blue-hoki-:not(.btn-outline):active,
        .btn.blue-hoki-:not(.btn-outline):hover, .open>.btn.blue-hoki-:not(.btn-outline).dropdown-toggle {
            color: #FFF;
            background-color: #b3b334;
            border-color: #b3b334;
        }
    </style>

</head>
<!-- END HEAD -->

<body class=" login" style="background-color: #e3e3e3 !important;">
<!-- BEGIN LOGO -->
<div class="logo"  style="margin: 120px auto 10px;">
    <!--    <img src="/themes/logo.png" alt="Logo" uib-tooltip="نظام إدارة شئون الجميعات الموحد">-->
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" name="login_form"  id="loginForm" method="post" role="form" action="" style="padding-top: 3%;" ng-controller="UserLoginController" >
        <div class="logo" style="margin: 0px auto 10px;">
            <img src="/themes/logo.png" style="
    width: 100%;" alt="Logo" uib-tooltip="نظام إدارة شئون الجميعات الموحد">
        </div>
        <!--        <h3 class="form-title text-center" style="color: #818ea0 !important;">{{'Please_login'| translate}}</h3>-->
        <div class="alert alert-danger text-danger bold" ng-show="failed">
            <button class="close" data-close="alert"></button>
            <span>{{message}}</span>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">{{'username'| translate}}</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text"
                       ng-enter="login()"
                       autocomplete="off" placeholder="{{'username'| translate}}"
                       name="username" ng-model="username" required>

            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">{{'password'| translate}}</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control form-control-solid placeholder-no-fix"  ng-enter="login()"
                       type="password" autocomplete="off" placeholder="{{'password'| translate}}" name="password" ng-model="password" required>
            </div>
        </div>
        <div class="form-actions">
            <button type="button" class="btn blue-hoki-  btn-block uppercase"
                    ng-click="login()"
                    ng-disabled="login_form.$invalid && !try"><span>{{'login'| translate}}</span></button>
        </div>
        <!--        id="forget-password"-->
        <div class="forget-password">
            <p class="text-right"> <a href="javascript:;" style="color: #73730a !important;" ng-click="forget()"> {{'forgot_password'| translate}} </a>  </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" name="forget_form"  method="post" ng-controller="ForgotPasswordController" ng-submit="sendEmail()"
          style="padding-top:15px;">
        <div class="form-title text-center bold" style="color: #818ea0 !important;">
            <span class="form-title">{{'forgot_password'| translate}}</span>
            <span class="form-subtitle">{{'Enter_your_email_to_reset_it'| translate}}</span>
        </div>
        <div class="alert bold "
             ng-class="{'alert-success text-primary': success, 'alert-danger text-danger': failed}"
             ng-if="message"
             style=" margin-top: 40px !important;">
            <p>{{message}}
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="glyphicon glyphicon-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" autocomplete="off"
                       placeholder="{{'EMail_Address' | translate}}" name="email" ng-model="email" required> </div>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn btn-default"><span>{{'Back' | translate}}</span></button>
            <button type="submit" class="btn blue-hoki- uppercase pull-right" ng-disabled="forget_form.$invalid"><span>{{'Submit' | translate}}</span></button>

            <!--            <button type="button" id="back-btn" class="btn grey-salsa btn-outline"> Back </button>-->
            <!--            <button type="submit" class="btn blue-hoki pull-right"> Submit </button>-->
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->

</div>
<!--<div class="copyright " style=" color: #73730a;">  {{'Nepras for media & IT' | translate}}  © {{Curyear}} </div>-->

<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="/themes/metronic/global/plugins/respond.min.js"></script>
<script src="/themes/metronic/global/plugins/excanvas.min.js"></script>
<script src="/themes/metronic/global/plugins/ie8.fix.min.js"></script>

<![endif]-->

<script src="/themes/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/bootstrap-toastr/toastr.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/themes/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/themes/metronic/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<!--<script src="/themes/metronic/pages/scripts/login.min.js" type="text/javascript"></script>-->
<script>
    //    jQuery('#forget-password').click(function() {
    //        jQuery('.login-form').hide();
    //        jQuery('.forget-form').show();
    //        jQuery('#email').val('');
    //    });

    jQuery('#back-btn').click(function() {
        jQuery('.login-form').show();
        jQuery('.forget-form').hide();
    });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<script src="/app/scripts/query-string/query-string.js" type="text/javascript"></script>
<!-- BEGIN CORE ANGULARJS PLUGINS -->
<script src="/themes/metronic/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-resource.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/angular-cookies.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-oauth2.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate-loader-static-files.min.js" type="text/javascript"></script>
<!-- END CORE ANGULARJS PLUGINS -->
<!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
<script>
    var gInLoginPage = true;
</script>
<script src="/app/modules/user/Module.js" type="text/javascript"></script>
<script src="/app/modules/user/controllers/AuthController.js" type="text/javascript"></script>
<script src="/app/modules/user/controllers/ForgotPasswordController.js" type="text/javascript"></script>
<!-- END APP LEVEL ANGULARJS SCRIPTS -->
</body>

</html>