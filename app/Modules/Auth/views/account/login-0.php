<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" dir="rtl" data-ng-app="UserModule">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8">
    <title><?= trans('auth::application.login') ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <noscript>
        <meta http-equiv="refresh" content="0;URL='/no-javascript.html'" />
    </noscript>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css">
    <link href="/themes/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/themes/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="/themes/metronic/global/plugins/bootstrap/css/bootstrap-rtl.min.css" rel="stylesheet" type="text/css">
    <link href="/themes/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="/themes/metronic/global/css/components-rtl.min.css" rel="stylesheet" id="style_components" type="text/css">
    <link href="/themes/metronic/global/css/plugins-rtl.min.css" rel="stylesheet" type="text/css">
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN PAGE LEVEL STYLES -->
<!--    <link href="/themes/metronic/pages/css/login-2-rtl.min.css" rel="stylesheet" type="text/css">-->
    <link href="/themes/metronic/pages/css/login-3-rtl.min.css" rel="stylesheet" type="text/css" />

    <!-- END PAGE LEVEL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="/favicon.ico">
</head>
<!-- END HEAD -->

<body class="login">
<!-- BEGIN LOGO -->
<div class="logo">
    <img src="/themes/logo.png" alt="Logo" uib-tooltip="نظام إدارة شئون الجميعات الموحد">
</div>
<!-- END LOGO -->
<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" id="loginForm" method="post" role="form" ng-submit="login()" ng-controller="UserLoginController">
        <div class="form-title">
            <span class="form-title">{{'Welcome'| translate}}.</span>
            <span class="form-subtitle">{{'Please_login'| translate}}.</span>
        </div>
        <div class="alert alert-danger" role="alert" ng-show="failed">
            <span>{{message}}</span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">{{'username'| translate}}</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="{{'username'| translate}}" name="username" ng-model="username" required>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">{{'password'| translate}}</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="{{'password'| translate}}" name="password" ng-model="password" required>
        </div>
        <div class="form-actions">
            <button type="submit" class="btn red btn-block uppercase"><span>{{'login'| translate}}</span></button>
        </div>
        <div class="pull-right forget-password-block">
            <a href="javascript:;" id="forget-password" class="forget-password">{{'forgot_password'| translate}}</a>
        </div>
    </form>
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="forget-form" method="post" ng-controller="ForgotPasswordController" ng-submit="sendEmail()">
        <div class="form-title">
            <span class="form-title">{{'forgot_password'| translate}}</span>
            <span class="form-subtitle">{{'Enter_your_email_to_reset_it'| translate}}</span>
        </div>
        <div class="alert" ng-class="{'alert-success': success, 'alert-danger': failed}" ng-if="message">
            <p>{{message}}
        </div>
        <div class="form-group">
            <input ng-model="email" class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="{{'EMail_Address' | translate}}" name="email"> </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn btn-default"><span>{{'Back' | translate}}</span></button>
            <button type="submit" class="btn btn-primary uppercase pull-right"><span>{{'Submit' | translate}}</span></button>
        </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
</div>
<!--<div class="copyright " style="color: #7a8ca5;">  {{'Nepras for media & IT' | translate}}  © {{Curyear}} </div>-->
<!-- END LOGIN -->
<!--[if lt IE 9]>
<script src="/themes/metronic/global/plugins/respond.min.js"></script>
<script src="/themes/metronic/global/plugins/excanvas.min.js"></script>
<script src="/themes/metronic/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="/themes/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/themes/metronic/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/themes/metronic/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script>
    jQuery('#forget-password').click(function() {
        jQuery('.login-form').hide();
        jQuery('.forget-form').show();
    });

    jQuery('#back-btn').click(function() {
        jQuery('.login-form').show();
        jQuery('.forget-form').hide();
    });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<!-- END THEME LAYOUT SCRIPTS -->

<script src="/app/scripts/query-string/query-string.js" type="text/javascript"></script>
<!-- BEGIN CORE ANGULARJS PLUGINS -->
<script src="/themes/metronic/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-resource.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/angular-cookies.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-oauth2.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate.min.js" type="text/javascript"></script>
<script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate-loader-static-files.min.js" type="text/javascript"></script>
<!-- END CORE ANGULARJS PLUGINS -->
<!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
<script>
    var gInLoginPage = true;
</script>
<script src="/app/modules/user/Module.js" type="text/javascript"></script>
<script src="/app/modules/user/controllers/AuthController.js" type="text/javascript"></script>
<script src="/app/modules/user/controllers/ForgotPasswordController.js" type="text/javascript"></script>
<!-- END APP LEVEL ANGULARJS SCRIPTS -->
</body>
</html>