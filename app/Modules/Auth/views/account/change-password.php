<form name="form" class="form-horizontal" novalidate="" role="form" ng-submit="submit()" ng-controller="ChangePasswordController">
    <h1>{{user.firstname}} {{user.lastname}}</h1>
    <fieldset>
        <legend><?= trans('auth::application.change_password') ?></legend>
        <div class="form-group" ng-class="errorClass('password')">
            <label for="password" class="col-md-2 control-label"><?= trans('auth::application.current_password') ?></label>
            <div class="col-md-4">
                <input type="password" class="form-control" id="password" name="password" placeholder="<?= trans('auth::application.current_password') ?>" ng-required="true" ng-model="password">
                <span class="help-inline" 
                      ng-show="form.password.$invalid && form.password.$touched">Password is required</span>
                <span class="help-block" ng-show="form.password.$invalid && form.password.$dirty">{{errorMessage('password')}}</span>
            </div>
        </div>
        <div class="form-group" ng-class="errorClass('new_password')">
            <label for="password" class="col-md-2 control-label"><?= trans('auth::application.new_password') ?></label>
            <div class="col-md-4">
                <input type="password" class="form-control" id="new_password" name="new_password" placeholder="<?= trans('auth::application.new_password') ?>" ng-required="true" ng-model="new_password">
                <span class="help-inline" 
                      ng-show="form.new_password.$invalid && form.new_password.$touched">Password is required</span>
                <span class="help-block" ng-show="form.new_password.$invalid && form.new_password.$dirty">{{errorMessage('new_password')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="confirm_password" class="col-md-2 control-label"><?= trans('auth::application.confirm_password') ?></label>
            <div class="col-md-4">
                <input type="password" class="form-control" id="new_password2" name="confirm_password" placeholder="<?= trans('auth::application.confirm_password') ?>" ng-required="true" ng-model="confirm_password">
                <span class="help-inline" 
                      ng-show="form.confirm_password.$invalid && form.confirm_password.$touched">Password is required</span>
                <span class="help-block" ng-show="form.confirm_password.$invalid && form.confirm_password.$dirty">{{errorMessage('confirm_password')}}</span>
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn blue"><span><?= trans('auth::application.submit') ?></span></button>
        </div>
    </div>
</form>