<?php
Route::get('/account/login', function() {
    return view('auth::account.login');
});
Route::get('/login', function() {
     return view('auth::account.login');
});

Route::get('password/reset', function() { return view('auth::passwords.email'); })->middleware('guest');

Route::get('password/reset/{token}', function(\Illuminate\Http\Request $request, $token = null) {
    return view('auth::passwords.reset')->with(
        ['token' => $token, 'email' => $request->email]
    );
})->middleware('guest');

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0'], function() {

    Route::group(['prefix' => 'users'], function() {
        Route::get('resetApiToken', 'Auth\Controller\SettingsController@resetApiToken');

        Route::put('status/{action}/{id}', 'Auth\Controller\UsersController@status')->where(['action' => 'activate|deactivate']);
        Route::get('permissions/{id}', 'Auth\Controller\UsersController@permissions');
        Route::post('permissions/{id}', 'Auth\Controller\UsersController@permissionsUpdate');
        Route::get('trashed', 'Auth\Controller\UsersController@trashed');
        Route::post('filter/trashed', 'Auth\Controller\UsersController@trashed');
        Route::post('online', 'Auth\Controller\UsersController@online');
        Route::post('filter', 'Auth\Controller\UsersController@filter');
        Route::put('restore/{id}', 'Auth\Controller\UsersController@restore');
        Route::put('logout/{id}', 'Auth\Controller\UsersController@logout');
        Route::get('logOutGroup', 'Auth\Controller\UsersController@logOutGroup');
    });

    Route::resource('users', 'Auth\Controller\UsersController');


    Route::resource('roles', 'Auth\Controller\RolesController');
    Route::post('roles/copy/{id}', 'Auth\Controller\RolesController@copy');
    Route::get('roles/{id}', 'Auth\Controller\RolesController@users');
    Route::get('roles/users/{id}', 'Auth\Controller\RolesController@users');

    Route::resource('permissions', 'Auth\Controller\PermissionsController', [
        'only' => ['index'],
    ]);
    Route::resource('auth/settings', 'Auth\Controller\SettingsController', [
        'only' => ['index', 'store'],
    ]);

    Route::put('password/{id?}', 'Auth\Controller\PasswordController@update');

    Route::group(['prefix' => 'auth/account'], function() {
        Route::get('user', 'Auth\Controller\AccountController@user');
        Route::put('user', 'Auth\Controller\AccountController@update');
        Route::put('setImage', 'Auth\Controller\AccountController@setImage');
        Route::put('Logout', 'Auth\Controller\AccountController@Logout');
        Route::get('user/notifications', 'Auth\Controller\AccountController@notifications');
    });

    Route::get('/account/logout', 'Auth\Controller\AccountController@logout');

    Route::group(['prefix' => 'password'], function() {
        Route::post('email', 'Auth\Controller\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
        Route::post('reset', 'Auth\Controller\ResetPasswordController@reset')->name('password.reset');
    });

});

Route::post('/oauth/token', ['uses' => 'Auth\Controller\LoginController@issueToken']);

