<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Auth\\', __DIR__ . '/../src');
$loader->register();