<?php
Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/aid-repository' ,'namespace' => 'AidRepository\Controller'], function() {

    Route::group(['prefix' => 'repositories'], function() {

        Route::group(['prefix' => 'central'], function() {
            Route::post('MainStatistic', 'CentralRepositoriesController@MainStatistic')->name('api.v1.0.CentralRepositories.MainStatistic');
            Route::post('statistic', 'CentralRepositoriesController@statistic')->name('api.v1.0.CentralRepositories.statistic');
            Route::post('check', 'CentralRepositoriesController@check')->name('api.v1.0.CentralRepositories.check');
            Route::post('filter', 'CentralRepositoriesController@filter')->name('api.v1.0.CentralRepositories.filter');
            Route::get('projectsOptions/{id}', 'CentralRepositoriesController@projectsOptions')->name('api.v1.0.CentralRepositories.projectsOptions');
            Route::get('projects/{id}', 'CentralRepositoriesController@projects')->name('api.v1.0.CentralRepositories.projects');
            Route::put('organization-share/{id}','CentralRepositoriesController@organizationShare')->name('api.v1.0.CentralRepositories.organizationShare');
        });
    
        Route::put('restore/{id}',   'RepositoriesController@restore')->name('api.v1.0.Repositories.restore');
        Route::post('MainStatistic', 'RepositoriesController@MainStatistic')->name('api.v1.0.Repositories.MainStatistic');
        Route::post('statistic', 'RepositoriesController@statistic')->name('api.v1.0.Repositories.statistic');
        Route::post('check', 'RepositoriesController@check')->name('api.v1.0.Repositories.check');
        Route::post('trash', 'RepositoriesController@trash')->name('api.v1.0.Repositories.trash');
        Route::post('filter', 'RepositoriesController@filter')->name('api.v1.0.Repositories.filter');
        Route::get('projectsOptions/{id}', 'RepositoriesController@projectsOptions')->name('api.v1.0.Repositories.projectsOptions');
        Route::get('projects/{id}', 'RepositoriesController@projects')->name('api.v1.0.Repositories.projects');
    });
    Route::resource('repositories', 'RepositoriesController');

    Route::resource('categories', 'CategoriesController');
    Route::post('projects/check', 'ProjectsController@check')->name('api.v1.0.Projects.check');
    Route::resource('projects', 'ProjectsController');

    Route::post('projects/upload_candidates','ProjectsController@upload_candidates');
    Route::put('projects/{id}/rest_candidates', 'ProjectsController@rest_candidates');
    Route::put('projects/{id}/candidates/status','ProjectsController@updateCandidateStatus');
    Route::put('projects/{id}/updateProjectStatus','ProjectsController@updateProjectStatus');
    Route::post('projects/{id}/getMobilesNumbers', 'ProjectsController@getMobilesNumbers');
    Route::post('projects/{id}/organization-share','ProjectsController@organizationShare');
    Route::post('projects/UpdateCandidatesStatus','ProjectsController@updateCandidateStatus_');
    Route::delete('projects/{id}/candidates/delete/{person_id}','ProjectsController@deleteCandidate');

    Route::get('nomination-rules/{id}/results', 'NominationsController@theCandidates');
    Route::get('nomination-rules', 'NominationsController@index');

    Route::group(['prefix' => 'nomination-rules'], function() {
        Route::post('filter', 'NominationsController@filter');
        Route::post('results', 'NominationsController@filter');
        Route::post('statistic', 'NominationsController@statistic');
        Route::delete('deleteNomination/{id}', 'NominationsController@deleteNomination');
        Route::post('saveNomination', 'NominationsController@saveNomination');
        Route::post('rule', 'NominationsController@rule');
        Route::put('rest_candidates/{id}', 'NominationsController@rest_candidates');
        Route::get('groups/{id}', 'NominationsController@groups');
        Route::get('group/{id}', 'NominationsController@group');
        Route::post('candidates', 'NominationsController@candidates');
    });
    Route::post('nomination-rules', 'NominationsController@store');

    Route::post('organization/projects/related', 'ProjectsController@related');
    Route::post('organization/projects/filter', 'ProjectsController@filter');
    Route::post('organization/projects/OrgProjects', 'ProjectsController@listForOrganization');
    Route::get('organization/projects', 'ProjectsController@listForOrganization');
    Route::get('organization/projects/{id}/candidates', 'ProjectsController@organizationCandidates');
});
?>