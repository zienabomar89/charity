<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('AidRepository\\', __DIR__ . '/../src');
$loader->register();