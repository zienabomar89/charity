<?php

return array(
    "#" => "م.",
    "aid_repositories" => "قائمة سلة المشاريع",
    "case_category" => "تصنيف الحالة",
    "organizations" => "كشف الجمعيات",
    "invalid_card"=>"رقم هوية غير صالح",
    "You cannot accredit other people, because the number of approved persons has reached the required number" => "لايمكنك اعتماد أشخاص آخرين ، لان عدد الاشخاص المعتمدين بلغ العدد المطلوب",
    "gender" => "الجنس",
    "active"=>"فعال",
    "voucher_benefit"=>"استفاد خارج الوعاء",
    "nomination"=>"الترشيح",
    "unable update status"=>"لايمكن تعديل حالة المشروع بسبب وجود جمعيات لم تنشأ قسائم مساعدات للمشروع أو مستفيدين لم يستلمو قسائم المشروع",
    "benefit_voucher_value"=>"قيمة الاستفادة",
    "not_benefit_voucher_value"=>"قيمة عدم الاستفادة",
    "not_benefit_voucher_date"=>"تاريخ عدم الاستفادة",
    "benefit_voucher_date"=>"تاريخ الاستفادة",
    "the_same"=>"نفس الحالة",
    "inactive"=>"غير فعال",

    "benefit_projects_date_from" => "استفادة من  داخل الوعاء من تاريخ",
    "benefit_projects_date_to" => "استفادة من  داخل الوعاء حتى تاريخ",
    "not_benefit_projects_date_from" => "عدم الاستفادة من  داخل الوعاء من تاريخ",
    "not_benefit_projects_date_to" => "عدم الاستفادة من  داخل الوعاء حتى تاريخ",
    "However, there are associations whose number of candidates according to the entered conditions did not reach the number required to be nominated by the organization"=>" ولكن يوجد جمعيات عدد مرشحينها حسب الشروط المدخلة لم يصل للعدد المطلوب ترشيحه من قبل الجمعية",
    "However, there are associations whose number of candidates according to the entered conditions did not reach the number required to be nominated by the organization."=>
    " ولكن يوجد جمعيات عدد مرشحينها حسب الشروط المدخلة لم يصل للعدد المطلوب ترشيحه من قبل الجمعية",
    "male" => "ذكر",
    "dependants_birthday" => "تاريخ ميلاد المعالين",
    "female" => "أنثى",
    "Added a new repository" => "تم اضافة وعاء جديد ",
    "Edited the project status" => "تم تعديل حالة مشروع",
    "He added new conditions to a project" => "تم حفظ شروط الترشيح للمشروع",
    "birthday" => "تاريخ الميلاد",
    "marital_status" => "الحالة الإجتماعية",
    "dependants" => "عدد المعالين",
    "required_no" => "العدد المطلوب",
    "monthly_income" => "الدخل الشهري",
    "work" => "يعمل",
    "workable" => "يستطيع أن يعمل",
    "no" => "لا",
    "yes" => "نعم",
    "work_status" => "حالة العمل",
    "work_wage" => "فئة الأجر",
    "house_property" => "نوع ملكية المنزل",
    "roof_material" => "سقف وجدران المنزل",
    "residence_condition" => "حالة المنزل/ المسكن",
    "excellent" => "ممتاز",
    "very_good" => "جيد جدا",
    "good" => "جيد",
    "bad" => "سيء",
    "very_bad" => "سيء جدا",
    "indoor_condition" => "حالة الأثاث",
    "house_condition" =>  "تقييم وضع المنزل ",
    "habitable" => "حاجة المنزل للترميم",
    "health" => "الحالة الصحية",
    "benefit" => "يستفيد من",
    "not_benefit" => "لا يستفيد من",
    "benefit_projects" => "يستفيد من مشروع داخل الوعاء",
    "not_benefit_projects" => "لا يستفيد من مشروع داخل الوعاء",
    "benefit_vouchers" => "يستفيد من مشروع خارج الوعاء",
    "not_benefit_vouchers" => "لا يستفيد من مشروع خارج الوعاء",
    "candidate" => "مرشح",
    "accepted" => "معتمد",
    "sent"=>"مرسل",
    "reject"=>"مرفوض",
    "open" => "مفتوح",
    "closed" => "مغلق",
    "execution"=>"معتمد للتنفيذ",
    "policy restricted"=>"لم يتجاوز القيود",
    "un_register"=>"غير مسجل في النظام",
    "un_register_to_your_organization"=>"غير مرشح من خلال جمعيتك",
    "reason"=>"سبب المنع",
    "reason_"=>"السبب",
    "un_defined_reason"=>"السبب غير مدخل",
    "un_defined_reason_for_all_cases"=>"السبب غير مدخل لكافة الأسماء المدرجة في الكشف",
    "exceed_max_required"=>"تجاوز اجمالي عدد المرا اعتمادهم",
    "nominated"=>"مرشح مسبقا",
    "category_name"=>"التصنيف",
    "new nominate"=>"تم ترشيح",
    "statistic"=>"احصائيات سلة المشاريع",
    "project_statistic"=>"احصائية المشروع",
    "candidates"=>"المرشحين",
    "candidates count"=>"العدد الكلي للمرشحين",
    "no organization"=>"لايوجد جمعيات داخل المشروع لعمل الاحصائية",
    "count"=>"العدد الكلي",
    "success"=>"تم ترشيحهم بنجاح",
    "previously inserted"=>"الأسماء المدرجة سابقا",
    "duplicated"=>"أشخاص مكررين",
    "restricted_"=>"قائمة الممنوع ترشيحهم",
    "blocked"=>"محضور ترشيحه",
    "restricted"=>"الممنوعين بسبب تجاوز العدد المسموح للجمعية",
    "project restricted"=>"الممنوعين بسبب تجاوز العدد المسموح للمشروع",
    "The cases is successfully nomination"=>"تمت عملية الترشيح كافة الاسماء بنجاح",
    "The cases is not successfully nomination"=>"لم تتم عملية الترشيح للكل بنجاح",
    "name"=> "الاسم رباعي",
    "id_card_number"=> "رقم الهوية",
    "old_id_card_number"=> " رقم بطاقة التعريف ",
    "user_name"=> "اسم المستخدم",
    "main_organization_detail"=> "بيانات الجمعيات الرئيسية",
    "candidate_organization_name"=> "اسم الجمعية المرشحة",
    "Organization_Name"=> "اسم الجمعية",
    "status"=> "الحالة",
    "repository_name"=> "اسم سلة المشاريع",
    "repository_date"=> "تاريخ سلة المشاريع",
    "created_at"=> "تاريخ الاضافة",
    "deleted_at"=> "تاريخ الحذف",
    "full_name"=> "الاسم رباعي",
    "notes"=> "ملاحظات",
    "project_name"=> "اسم المشروع",
    "organization_name"=> "الجمعية المنفذة",
    "custom_organization"=> "الجمعية المخصصة",
    "sponsor_name"=> "الجهة الممولة للمشروع",
    "organization_ratio" =>"النسبة التحفيزية",
    "organization_quantity" =>"الكمية التحفيزية",
    "custom_ratio" =>"النسبة التخصيصية",
    "custom_quantity" =>"الكمية التخصيصية",
    "quantity" =>"الكمية",
    "currency" =>"العملة",
    "exchange_rate" =>"سعر الصرف",
    "amount_" =>"القيمة بالعملة الاصلية",
    "amount_sh" =>"القيمة بالشيكل",
    "candidates list" =>"كشف فحص مستفيدين",
    "not candidates to all" =>"غير مستفيد ",
    "invalid_cards" =>"أرقام هوية غير صحيحة  ",
    "undefined_cards" =>"أرقام هوية غير مسجلة",
    "family_count"=> "عدد أفراد العائلة",
    "without_level"=> "من غير التزام",
    "district"=> "المحافظة",
    "city"=> " المدينة",
    "region"=> "المنطقة",
    "nearlocation"=> "الحي",
    "square"=> "المربع",
    "mosques"=> "المسجد",
    "exceed project quantity"=>"تجاوز الكمية المحددة للمشروع",
    "all card are nominated'"=> "كافة ارقام الهوية مرشحة مسبقا",
    "not_case"=> "غير مسجل كحالة",
    'the file is empty' => 'الملف لايحتوي على بيانات',
    "Organization"=> "جمعية",
    "He added a new repository" => "قام بإضافة وعاء جديد",
    "Saved" => "تم الحفظ بنجاح",
    "There was a bug during the save" => "حدث خلل أثناء عملية الحفظ ",
    "Edited the repository data" => "قام بتحرير بيانات وعاء ",
    "All names entered are incorrect or not registered in the system"=>"جميع الأسماء المدخلة غير صحيحة أو غير مسجلة في النظام ",
    "All names entered are incorrect or not registered in the system as A case on You"=>"جميع الأسماء المدخلة غير صحيحة أو غير مسجلة في النظام كحالة لديك ",
    "Not filtered" => "غير مرشح",
    "Downloading the results file,"=> " جاري تنزيل ملف نتائج الفحص ، ",
    "Some numbers were successfully checked, as the total number of digits" => "تم فحص بعض الارقام بنجاح ، حيث أن عدد الارقام الكلي ",
    "Number of names checked" => "عدد الأسماء التي تم فحصها",
    "The number of incorrect or not registered ID numbers in the system"=>"عدد أرقام الهوية الغير صحيحة او غير مسجلة في النظام",
    "Number of duplicate names" => " عدد  الأسماء  المكررة ( موجودة في الملف أكثر من مرة )",
    "All the numbers were checked and their data was fetched" => " تم فحص كافة الارقام وجلب بياناتها لها ",
    "Deleted repository"=> "قام بحذف وعاء",
    "Restored repository" => "قام باستعادة وعاء ",
    "Added new conditions to a project" =>  "قام بإضافة شروط جديدة لمشروع ",
    "Added new candidates to the project" => "قام بإضافة مرشحين جدد لمشروع ",
    "However there are associations whose number of candidates according to the entered conditions did not reach the number required to be nominated by the organization"=>" ولكن يوجد جمعيات عدد مرشحينها حسب الشروط المدخلة لم يصل للعدد المطلوب ترشيحه من قبل الجمعية",
    "Conditions successfully saved but no candidates" => "تم حفظ الشروط بنجاح ولكن لا يوجد مرشحين",
    "Terms successfully saved"=> "تم حفظ الشروط بنجاح ",
    "Nomination for a project approved for implementation, closed or draft cannot be submitted" => "لايمكن الترشيح لمشروع معتمد للتنفيذ أو مغلق أو مسودة",
    "There are no candidates" => "لايوجد مرشحين ",
    "cannot be added project to a closed container" =>  "لا يمكن إضافة مشروع جديد إلى وعاء مغلق",
    "Added a new project" => "قام بإضافة مشروع جديد",
    "Cannot edit project data in a closed repository" => "لا يمكن تحرير بيانات مشروع في وعاء مغلق",
    "Edited the project data"=> "قام بتحرير بيانات مشروع",
    "No numbers for organizations" => "لا يوجد ارقام للجمعيات",
    "cannot be deleted" => "لا يمكن حذف مشروع في وعاء مغلق",
    "The status of persons in a closed or approved project cannot be modified" => "لايمكن تعديل حالة الأشخاص في مشروع مغلق أو معتمد للتنفيذ",
    "No new candidates can be accredited because you have the same number of accredited candidates" => "لايمكن اعتماد أي مرشحين جدد لأن عدد المعتمدين لديك هو نفس عدد المطلوب اعتمادهم",
    "Modified the status of project recipients to" => " قام بتعديل حالة مستفيدي المشروع إلى ",
    "The status of all people has been successfully modified" => "تم تعديل حالة كافة الاشخاص بنجاح",
    "The status of all people has not been modified because you have reached the maximum number of approved people in this project or have the same status" => "لم يتم تعديل حالة كافة الاشخاص لأنك وصلت للحد الأعلى من عدد المعتمدين في هذا المشروع أو انه يمتلك نفس الحالة",
    "The status of some people has been successfully modified, as the total number of people" => "تم تعديل الحالة لبعض الأشخاص بنجاح ، حيث أن عدد الأشخاص الكلي ",
    "The number of names whose status has been modified" => "عدد الأسماء التي تم تعديل الحالة لها" ,
    "Number of names with the same status" => "عدد الأسماء التي يمتلكون نفس الحالة",
    "Number of people whose status has not been modified" =>  "عدد الأشخاص الذين لم يتم تعديل الحالة لهم",
    "Downloading file whose status has not been modified" =>  "جاري تنزيل ملف الأسماء التي لم يتم تعديل الحالة لها",
    "modified the proportions of the associations for a project" => "قام بتعديل نسب الجمعيات لمشروع",
    "cant update on the ratios of associations for a closed or approved project for implementation"=> "لا يمكن التعديل على نسب الجمعيات لمشروع مغلق أو معتمد للتنفيذ",
    "There are no candidates saved within the project to empty" => "لايوجد مرشحين محفوظين ضمن المشروع لتفريغ",
    "occurred during the operation" => "حدث خلل أثناء إجراء العملية",
    "The candidates were successfully emptied from the project" => "تم تفريغ المرشحين من المشروع بنجاح",
    "The candidates were successfully emptied from the nominations" => "تم تفريغ المرشحين من مجموعة الترشيح بنجاح",
    "empties the project from the candidates" => "قام بتفريغ المشروع من المرشحين",
    "You cannot delete people from a closed or approved project for execution" => "لايمكن حذف أشخاص من مشروع مغلق أو معتمد للتنفيذ",
    "The candidate must be the person to be deleted from the list of candidates" => "يجب أن تكون الجهة المرشحة للشخص حتى تتمكن من حذفه من قائة المرشحين",
    "Deleted project filter" => "قام بحذف مرشح مشروع",
    "Not all people have their status modified because they have the same status" => "لم يتم تعديل حالة كافة الاشخاص لأنهم يمتلكون نفس الحالة",
    "Manage_Repository"=> "إدارة سلة المشاريع",
    "Repository "=> "سلات المشاريع",
    "Repositories"=> "سلات المشاريع",
    "Trash_Repositories"=> "سلات المشاريع المحذوفة",
    "Projects"=> "المشاريع",
    "Project"=> "مشروع",
    "Sponsor"=> "الجهة الممولة",
    "Organization_"=> "جمعية",
    "Candidate_Organization_Name"=> "الجمعية المرشحة",
    "rest_roles"=> "تفريغ الشروط",
    "rest_candidates"=> "تفريغ المرشحين",
    "Amount"=> "القيمة",
    "Extra_Quantity"=> "الكمية الإضافية",
    "currency_"=> "العملة",
    "Quantity"=> "الكمية",
    "Percentage"=> "النسبة",
    "child_ratio"=> "نسب الأبناء",
    "restore"=> "استرجاع",
    "Share"=> "الحصة",
    "Total"=> "المجموع",
    "Repository_Project"=> "المشاريع",
    "Repository_Level"=> "مستوى الإلتزام",
    "Add_Project"=> "إضافة مشروع",
    "Category"=> "التصنيف",
    "check_persons"=> "فحص مستفيدين",
    "change status"=> "تعديل الحالة",
    "Save"=> "حفظ",
    "Save And Candidate"=> "حفظ الشروط وترشيح الكل",
    "Cancel"=> "إلغاء الأمر",
    "Notes"=> "ملاحظات",
    "total_required"=> "اجمالي العدد المطلوب",
    "all_candidate"=> "اجمالي المرشحين",
    "using excel"=>" عن طريق اكسل",
    "status update form"=> "نموذج تعديل الحالة",
    "Distribute"=> "توزيع",
    "Unit_Amount"=> "قيمة الوحدة",
    "Unit_Amount_sh"=> "قيمة الوحدة بالشيكل",
    "Total_Quantity"=> "إجمالي الكمية",
    "Total_Quantity_Share"=> "إجمالي نسب الكميات",
    "Total_Amount"=> "إجمالي القيمة",
    "Total_Amount_Share"=> "إجمالي نسب القيمة",
    "Ratio_"=> "النسبة المئوية % ",
    "repository data form"=>  "نموذج بيانات سلة المشاريع",
    "back"=>  "العودة",
    "Ratio"=> "النسبة التحفيزية",
    "Organization_Ratio_"=> "نسبة الجمعية المنفذة",
    "Organization_Ratio"=> "نسبة الجمعية المنفذة",
    "Quantity_Ratio"=> "الكمية بعد النسبة",
    "Candidate_all"=> "ترشيح الكل",
    "Candidates"=> "المرشحون",
    "Candidates_Statistic"=> "احصائية المرشحين",
    "Candidate"=> "ترشيح",
    "Nomination"=> "الترشيح",
    "Adjust"=> "موائمة",
    "Status"=> "الحالة",
    "Created_At"=> "انشئ في",
    "Updated_At"=> "آخر تحديث",
    "Results"=> "النتائج",
    "Search"=> "بحث",
    "No_results"=> "لا يوجد نتائج للبحث الذي قمت به!",
    "No_Candidates"=> "لايوجد مرشحون جدد يطابقون الشروط",
    "ID"=> "رقم الهوية",
    "Birthday"=> "تاريخ الميلاد",
    "Name"=> "الاسم",
    "Rules"=> "شروط الترشيح",
    "Organization_Share"=> "نسبة الجمعية",
    "Rule"=> "الشرط",
    "Value"=> "القيمة",
    "Project_Candidates"=> "المرشحين",
    "Share_Settings"=> "إعدادات النسب",
    "username"=> "إسم المستخدم",
    "Operations"=> "العمليات",
    "Customized_Ratio"=> "النسبة التخصيصية",
    "Open"=> "مفتوح",
    "Close"=> "مغلق",
    "to"=> "ل",
    "To"=> "ل",
    "From"=> "من",
     "To_"=> "إلى",
    "From_"=> "من",
    "new_voucher"=>"إضافة قسيمة",
    "new voucher form"=>"نموذج إنشاء قسيمة",
    "voucher_title"=>"عنوان القسيمة",
    "voucher_category"=>"تصنيف القسيمة",
    "voucher_type"=>"نوع القسيمة",
    "voucher_content"=>"محتوى القسيمة",
    "voucher_note"=>"ملاحظات على القسيمة",
    "voucher_date"=>"تاريخ القسيمة",
    "there is no data to show" => "لايوجد سجلات للعرض",
    "there is no rules to show" => "لايوجد شروط ترشيح للعرض",
    "allowed"=>"العدد المسموح",
    "count before this candidate"=>"عدد المرشحين قبل هذه العملية ",
    "total candidate"=>"اجمالي عدد المرشحين",
    "candidate count"=>"عدد المرشحون ",
    "new candidates"=>"عدد المرشحون الجدد",
    "nominate"=>"مرشح",
    "edit_project"=>"تعديل بيانات المشروع",
    "Reject Reason Form"=>"نموذج إضافة سبب الرفض",
    "Reject Reason"=>"  سبب الرفض" ,
    "receipt_date"=>"تاريخ الاستلام",
    "receipt_time"=>"وقت الاستلام",
    "transfer_company_name"=>"شركة التحويل",
    "transfer"=>"التنفيذ",
    "receipt_location"=>"مكان الاستلام",
    "show_all"=>"عرض الكل",
    "nearlocation_"=> "الحي",
    "allaw_day"=> "عدد أيام السماح",
    "urgent_repository"=> "وعاء طارئ",
    "at least the imported file must be included id card number for all status ,but for rejected should be contain reason"=>"يرجى الإلتزام بالقالب ، حيث أنه يجب أن يحتوي الملف على رقم الهوية على الاقل و السبب في حال كان الاسم مرفوض",
    "at least the imported file must be included id card number"=>"يرجى الإلتزام بالقالب ، حيث أنه يجب أن يحتوي الملف على رقم الهوية ",
    "Organizations"=>"الجمعيات",
    "Persons"=>"الأشخاص",
    "select_organization_persons"=>"ارسال للجمعيات او الاشخاص",
    "The candidates is greater than the specified for this project"=>"عدد المرشحين أكبر من العدد المحدد لهذا المشروع",


    "Org_Name"=> "الجمعية",
    "select status"=> "حدد الحالة",
    "Certified for nomination"=>"معتمد للترشيح",
    "Certified for execution"=>"معتمد للتنفيذ",
    "Draft"=>"مسودة",
    "Done"=>"تم",
    "No nomination conditions"=>"لايوجد شروط للترشيح",
    "There are no new candidates who meet the specified conditions"=>"لايوجد أشخاص مرشحين جدد تنطبق عليه الشروط المحددة",
    "Total ratios exceeded 100" => "تجاوز اجمالي مجموع النسب 100",
    "Total ratio exceeded 1, ratio is zeroed for"=> "تجاوز اجمالي النسبة 1 ، تم تصفير النسبة لـ ",
    ", So you can add the correct ratio, where the remaining ratio=>"=>" ، حتى تتمكن من اضافة النسبة الصحيحة ، حيث أن النسبة المتبقية => ",
    "You have not selected specific people or people who have the same status to edit or you have selected people who are not registered as an organization"=>"لم تقم بتحديد أشخاص أو الاشخاص المحددين يمتلكون نفس الحالة المراد التعديل لها أو أنك حددت أشخاص غير مسجلين لديك كجمعية",
    "No project was selected to send the message"=>"لم يتم تحديد أي مشروع  لارسال الرسالة .",
    "The project cannot be adapted again"=>"لايمكن موائمة المشروع مرة أخرى",
    ", And a file that meets the requirements and has not been nominated is being downloaded"=>"، وجاري تنزيل ملف الذين تنطبق عليهم الشروط ولم يتم ترشيحهم",
    ", And you're being redirected to the Candidates page"=>" ، وجاري تحويلك إلى صفحة المرشحين ",
    "There are no specific people to nominate"=>"لايوجد أشخاص محددين لترشيحهم",
    "Total ratios must be 1 in order to save project"=>"يجب أن يكون اجمالي النسب يساوي 1 حتى يتسنى لك حفظ المشروع",
    "No operation can be do on a project under construction or closed"=>"لايمكن اجراء اي عملية على مشروع قيد التنفيذ أم مغلق"


);
