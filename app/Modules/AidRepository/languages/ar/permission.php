<?php

return array(
    'aidRepository.categories.manage' => 'إدارة تصنيفات مشاريع إدارة سلة المشاريع',
    'aidRepository.categories.create' => 'إنشاء تصنيف جديد لمشاريع إدارة سلة المشاريع',
    'aidRepository.categories.update' => 'تحرير تصنيف مشروع إدارة سلة المشاريع',
    'aidRepository.categories.delete' => 'حذف تصنيف مشروع إدارة سلة المشاريع',
    'aidRepository.categories.view' => 'عرض معلومات تصنيف مشروع إدارة سلة المشاريع',
    
    'aidRepository.repositories.manage' => 'إدارة سلة المشاريع',
    'aidRepository.repositories.create' => 'إنشاء وعاء جديد',
    'aidRepository.repositories.update' => 'تحرير بيانات وعاء',
    'aidRepository.repositories.delete' => 'حذف وعاء',
    'aidRepository.repositories.view' => 'عرض معلومات سلة المشاريع',
    
    'aidRepository.projects.manage' => 'إدارة مشاريع سلة المشاريع',
    'aidRepository.projects.create' => 'إنشاء مشروع جديد في سلة المشاريع',
    'aidRepository.projects.update' => 'تحرير بيانات مشروع',
    'aidRepository.projects.delete' => 'حذف مشروع من سلة المشاريع',
    'aidRepository.projects.view' => 'عرض معلومات المشروع',
    
    'aidRepository.organizationProjects.manage' => 'مشاريع سلة المشاريع الخاصة بالجمعية',
    'aidRepository.organizationProjects.candidates' => 'عرض المرشحين للمشروع في الجمعية',
    'aidRepository.organizationProjects.nominate' => 'ترشيح مستفيدين لمشروع في سلة المشاريع',

);
