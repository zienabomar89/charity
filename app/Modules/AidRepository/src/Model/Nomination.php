<?php

namespace AidRepository\Model;

use App\Http\Helpers;
use Illuminate\Support\Facades\DB;

class Nomination
{

    public static function getVouchersList($project_id){

        $user = \Auth::user();
        $project = Project::with('Repository')->findOrFail($project_id);

        return \DB::table('char_vouchers')
                  ->leftJoin('char_currencies', 'char_currencies.id', '=', 'char_vouchers.currency_id')
                  ->where(function ($q) use ($user ,$project) {
                      $q->whereNull('deleted_at');
                      // $q->where('status','=',0);
                      $q->whereNull('project_id');

                      if($project->date_from != null && $project->date_to != null) {
                          $q->whereDate('voucher_date', '>=', $project->date_from);
                          $q->whereDate('voucher_date', '<=', $project->date_to);
                      }elseif($project->date_from != null && $project->date_to == null) {
                          $q->whereDate('voucher_date', '>=', $project->date_from);
                      }elseif($project->date_from == null && $project->date_to != null) {
                          $q->whereDate('voucher_date', '<=', $project->date_to);
                      }
                      
                      $q->where(function ($anq) use ($user) {
                          if($user->type == 2) {
                              $anq->where('organization_id',$user->organization_id);
                              $anq->orwherein('organization_id', function($qr) use($user) {
                                  $qr->select('organization_id')
                                     ->from('char_user_organizations')
                                     ->where('user_id', '=', $user->id);
                              });
                          }
                          else{
                              $anq->wherein('organization_id', function($qr) use($user) {
                                  $qr->select('descendant_id')
                                       ->from('char_organizations_closure')
                                       ->where('ancestor_id', '=', $user->organization_id);
                              });
                          }
                      });
                  })
                  ->selectRaw(" char_vouchers.id , 
                                CONCAT(
                                       ifnull(char_vouchers.title, ' '),' (' ,
                                       ifnull(char_vouchers.value, ' '), ' ' ,
                                       ifnull(char_currencies.name, ' '),' )' 

                                      )
                                AS title
                               ")
                  ->get();

    }

    public static function getNomination($rules,$project_id,$repository_id,$master)
    {

        $tables = ['char_persons', 'char_cases', 'char_categories', 'char_organizations'];

        $results = DB::table('char_persons')
                ->join('char_cases', 'char_cases.person_id', '=', 'char_persons.id')
                ->join('char_categories', 'char_categories.id', '=', 'char_cases.category_id')
                ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
                ->select([
                    'char_persons.*',
                     'char_cases.id as case_id',
                     'char_cases.organization_id',
                    'char_categories.name as category_name',
                    'char_organizations.name as organization_name',
                ])
                ->orderBy('char_organizations.name','char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name');

         if(!$master){
             if (isset($rules['organization_id']) && $rules['organization_id'] ) {
                 $results->where('char_cases.organization_id', '=', $rules['organization_id']);
             }
         }
        
        $definedRules = self::getRules($project_id,$repository_id);
        foreach ($rules as $name => $value) {
            if (!array_key_exists($name, $definedRules)) {
                continue;
            }
            $select = $definedRules[$name]['select'];
            $hasValue = false;
            if (isset($select['column'])) {
                   if (is_array($value)) {
                       if($name == 'not_benefit' || $name == 'benefit' || $name == 'not_own' || $name == 'own' || $name == 'need' ||
                          $name == 'not_need' || $name == 'not_benefit_projects' || $name == 'benefit_projects'  ||
                           $name == 'benefit_vouchers' || $name == 'not_benefit_vouchers'){
                           if(sizeof($value) > 0){
                               if($name == 'benefit' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_aids')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('aid_take', 1)
                                                   ->whereIn('aid_source_id', $value);
                                           });
                                   });
                                   
//                                 $results->join('char_persons_aids  as aids_take', function($q) use($organization_id,$user,$value){
//                                        $q->on('aids_take.person_id', '=', 'char_cases.person_id')
//                                          ->where('aids_take.aid_take', '=', 1)
//                                                ->whereIn('aids_take.aid_source_id', $value);
//                                 });
                               }
                               else if($name == 'not_benefit' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_aids')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('aid_take', 1)
                                                   ->whereIn('aid_source_id', $value);
                                           });
                                   });
                                   
                               }
                               else if($name == 'own' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_properties')
                                           ->where(function ($sq) use ($value){
                                                   $sq->where('has_property', 1)
                                                       ->whereIn('property_id', $value);
                                           });
                                   });

                               }
                               else if($name == 'not_own' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                             ->from('char_persons_properties')
                                             ->where(function ($sq) use ($value){
                                                   $sq->where('has_property',1)
                                                       ->whereIn('property_id', $value);
                                             });
                                   });
                               }else if($name == 'need' ) {
                                   $results->whereNotIn('char_cases.id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_cases_essentials')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('needs','!=', 0)
                                                   ->whereIn('essential_id', $value);
                                           });
                                   });
                                                                      
                               }else if($name == 'not_need' ) {
                                   $results->whereIn('char_cases.id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_cases_essentials')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('needs', 0)
                                                   ->whereIn('essential_id', $value);
                                           });
                                   });
                                                                      
                               }else if($name == 'benefit_projects' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('aid_projects_persons')
                                           ->where(function ($q) use ($value) {
                                             $q->where('status', 3);
                                             $q->whereIn('project_id', $value);
                                        });
                                        
                                   });
                               }else if($name == 'not_benefit_projects' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('aid_projects_persons')
                                           ->where(function ($q) use ($value) {
                                             $q->where('status', 3);
                                             $q->whereIn('project_id', $value);
                                        });
                                        
                                   });
                               }
                               else if($name == 'benefit_vouchers' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_vouchers_persons')
                                           ->where(function ($q) use ($value) {
                                               $q->where('status', 1);
                                               $q->whereIn('voucher_id', $value);
                                           });

                                   });
                               }
                               else if($name == 'not_benefit_vouchers' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_vouchers_persons')
                                           ->where(function ($q) use ($value) {
                                               $q->where('status', 1);
                                               $q->whereIn('voucher_id', $value);
                                           });

                                   });
                               }
                           }
                       }
                       else{
                           if(array_key_exists('min', $value) || array_key_exists('max', $value)) {
                               if(array_key_exists('min', $value) && $value['min'] !== '') {
                                   $results->where($select['column'], '>=', $value['min']);
                                   $hasValue = true;
                               }
                               if(in_array('max', $value) && $value['max'] !== '') {
                                   $results->where($select['column'], '<=', $value['max']);
                                   $hasValue = true;
                               }
                           }
                           else if (!empty($value)) {
                               $results->whereIn($select['column'], $value);
                               $hasValue = true;
                           }
                       }

                    } else if (!empty($value)) {
                        $results->where($select['column'], $value);
                        $hasValue = true;
                    }
            }
            if ($hasValue && isset($select['table']) && !in_array($select['table'], $tables)) {
                $table = $select['table'];
                $tables[] = $table;
                $fk = $select['fk'];
                $results->join($table, 'char_persons.id', '=', "$table.$fk");
            }
        }

        $results=$results->whereNotIn('char_persons.id', function($query)use($project_id){
                                     $query->select('person_id')->from('aid_projects_persons')->where('aid_projects_persons.project_id', $project_id);
                         });

        return $results->get();
    }

    public static function getRules($project_id,$repository_id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        return [

            'case_status' => [
                'label' => trans('aid-repository::application.status'),
                'type' => 'options',
                'options' => [
                    ['id' => 0, 'name' => trans('aid::application.active')],
                    ['id' => 1, 'name' => trans('aid::application.inactive')],
                ],
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'char_cases.status',
                    'fk' => 'person_id',
                ],
            ],

            'category' => [
                'label' => trans('aid-repository::application.case_category'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\Categories::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'category_id',
                    'fk' => 'person_id',
                ],
            ],

            'visitor_evaluation ' => [
                'label' => trans('aid::application.visitor_evaluation'),
                'type' => 'options',
                'options' => [
                    ['id' => 0, 'name' => trans('aid::application.very_bad')],
                    ['id' => 1, 'name' => trans('aid::application.bad')],
                    ['id' => 2, 'name' => trans('aid::application.good')],
                    ['id' => 3, 'name' => trans('aid::application.very_good')],
                    ['id' => 4, 'name' => trans('aid::application.excellent')],
                ],
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'visitor_evaluation',
                ],
            ],

            'card_type ' => [
                'label' => trans('aid::application.card_type'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.id_card')],
                    ['id' => 2, 'name' => trans('aid::application.id_number')],
                    ['id' => 3, 'name' => trans('aid::application.passport')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'card_type',
                ],
            ],

            'gender' => [
                'label' => trans('aid-repository::application.gender'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.male')],
                    ['id' => 2, 'name' => trans('aid-repository::application.female')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'gender',
                ],
            ],

            'marital_status' => [
                'label' => trans('aid-repository::application.marital_status'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\MaritalStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->marital_status_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'marital_status_id',
                ],
            ],

            'deserted ' => [
                'label' => trans('aid::application.deserted'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'deserted',
                ],
            ],

            'refugee ' => [
                'label' => trans('aid::application.refugee'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.citizen')],
                    ['id' => 2, 'name' => trans('aid::application.refugee')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'refugee',
                ],
            ],

            'birthday' => [
                'label' => trans('aid-repository::application.birthday') .' ( 25-01-2022 ) ',
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'birthday',
                ],
            ],


            'dependants' => [
                'label' => trans('aid-repository::application.dependants'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'family_cnt',
//                    'column' => DB::raw('char_get_family_count ( \'left\',char_persons.,null,char_persons.id )'),
////                    'column' => DB::raw('char_get_dependants_count(char_persons.id, \'father\')'),
                ],
            ],

            'dependants_birthday' => [
                'label' => trans('aid-repository::application.dependants_birthday') .' ( 25-01-2022 ) ',
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'birthday',
                ],
            ],



            'monthly_income' => [
                'label' => trans('aid-repository::application.monthly_income'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'monthly_income',
                ],
            ],

            'actual_monthly_income' => [
                'label' => trans('common::application.actual_monthly_income'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'actual_monthly_income',
                ],
            ],

            'receivables ' => [
                'label' => trans('common::application.receivables'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'receivables',
                ],
            ],

            'receivables_sk_amount' => [
                'label' => trans('common::application.receivables_sk_amount'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'receivables_sk_amount',
                ],
            ],

            'has_commercial_records ' => [
                'label' => trans('common::application.has_commercial_records'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'has_commercial_records',
                ],
            ],

            'active_commercial_records' => [
                'label' => trans('common::application.active_commercial_records'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'active_commercial_records',
                ],
            ],
            'work' => [
                'label' => trans('aid-repository::application.work'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'working',
                    'fk' => 'person_id',
                ],
            ],

            'workable' => [
                'label' => trans('common::application.is_can_work'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'can_work',
                    'fk' => 'person_id',
                ],
            ],

            'work_status' => [
                'label' => trans('aid-repository::application.work_status'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Work\StatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->work_status_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'work_status_id',
                    'fk' => 'person_id',
                ],
            ],

            'work_wage' => [
                'label' => trans('aid-repository::application.work_wage'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\Work\Wage::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'work_wage_id',
                    'fk' => 'person_id',
                ],
            ],

            'house_property' => [
                'label' => trans('aid-repository::application.house_property'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyTypeI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_type_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'property_type_id',
                    'fk' => 'person_id',
                ],
            ],

            'roof_material' => [
                'label' => trans('aid-repository::application.roof_material'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\RoofMaterialI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'roof_material_id',
                    'fk' => 'person_id',
                ],
            ],

            'residence_condition' => [
                'label' => trans('aid-repository::application.residence_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\HouseStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'residence_condition',
                    'fk' => 'person_id',
                ],
            ],

            'indoor_condition' => [
                'label' => trans('aid-repository::application.indoor_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\FurnitureStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'indoor_condition',
                    'fk' => 'person_id',
                ],
            ],

            'house_condition' => [
                'label' => trans('aid-repository::application.house_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\BuildingStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'house_condition',
                    'fk' => 'person_id',
                ],
            ],

            'habitable' => [
                'label' => trans('aid-repository::application.habitable'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\HabitableStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'habitable',
                    'fk' => 'person_id',
                ],
            ],

            'has_health_insurance ' => [
                'label' => trans('aid::application.has_health_insurance'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'has_health_insurance',
                ],
            ],

            'has_device ' => [
                'label' => trans('aid::application.has_device'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'has_device',
                ],
            ],

            'health_condition' => [
                'label' => trans('aid-repository::application.health'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.good_status')],
                    ['id' => 2, 'name' => trans('aid::application.chronic_disease')],
                    ['id' => 2, 'name' => trans('aid::application.special_needs')],
                ],
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'condition',
                    'fk' => 'person_id',
                ],
            ],

            'health' => [
                'label' => trans('aid::application.disease_id'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\DiseaseI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->disease_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'disease_id',
                    'fk' => 'person_id',
                ],
            ],

            'need' => [
                'label' => trans('aid::application.need'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Essential::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases_essentials',
                    'column' => 'needs',
                    'fk' => 'case_id',
                ],
            ],

            'not_need' => [
                'label' => trans('aid::application.not_need'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Essential::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases_essentials',
                    'column' => 'needs',
                    'fk' => 'case_id',
                ],
            ],

            'own' => [
                'label' => trans('aid::application.own'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_properties',
                    'column' => 'property_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_own' => [
                'label' => trans('aid::application.not_own'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_properties',
                    'column' => 'property_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit' => [
                'label' => trans('aid-repository::application.benefit'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\AidSource::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_aids',
                    'column' => 'aid_source_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_benefit' => [
                'label' => trans('aid-repository::application.not_benefit'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\AidSource::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_aids',
                    'column' => 'aid_source_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit_vouchers' => [
                'label' => trans('aid-repository::application.benefit_vouchers'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($project_id) {
                    $entries = [];
                    $list = self::getVouchersList($project_id);
                    foreach ($list as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->title,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_vouchers_persons',
                    'column' => 'voucher_id',
                    'fk' => 'voucher_id',
                ],
            ],

//            'benefit_voucher_date' => [
//                'label' => trans('aid-repository::application.benefit_voucher_date') .' ( 25-01-2022 ) ',
//                'type' => 'range',
//                'select' => [
//                    'table' => 'char_vouchers_persons',
//                    'column' => 'receipt_date',
//                    'fk' => 'receipt_date',
//                ],
//            ],

            'benefit_voucher_value' => [
                'label' => trans('aid-repository::application.benefit_voucher_value'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_vouchers',
                    'column' => 'value',
                    'fk' => 'value',
                ],
            ],

            'not_benefit_vouchers' => [
                'label' => trans('aid-repository::application.not_benefit_vouchers'),
                'type' => 'options',
                'options' => function() use($project_id) {
                    $entries = [];
                    $list = self::getVouchersList($project_id);
                    foreach ($list as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->title,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_vouchers_persons',
                    'column' => 'voucher_id',
                    'fk' => 'voucher_id',
                ],
            ],

//            'not_benefit_voucher_date' => [
//                'label' => trans('aid-repository::application.not_benefit_voucher_date') .' ( 25-01-2022 )',
//                'type' => 'range',
//                'select' => [
//                    'table' => 'char_vouchers_persons',
//                    'column' => 'receipt_date',
//                    'fk' => 'receipt_date',
//                ],
//            ],

            'not_benefit_voucher_value' => [
                'label' => trans('aid-repository::application.not_benefit_voucher_value'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_vouchers',
                    'column' => 'value',
                    'fk' => 'value',
                ],
            ],

            'benefit_projects' => [
                'label' => trans('aid-repository::application.benefit_projects'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($project_id,$repository_id) {
                    $entries = [];
                    $projects = Project::where(function ($q) use ($project_id,$repository_id) {
                        $q->where('id', '!=', $project_id);
                        $q->where('repository_id', '=', $repository_id);
                        $q->whereNull('.deleted_at');
                    })->get();
                    foreach ($projects as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'aid_projects_persons',
                    'column' => 'project_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit_projects_date_from' => [
                'label' => trans('aid-repository::application.benefit_projects_date_from') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_from',
                ],
            ],

            'benefit_projects_date_to' => [
                'label' => trans('aid-repository::application.benefit_projects_date_to') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_to',
                ],
            ],

            'not_benefit_projects' => [
                'label' => trans('aid-repository::application.not_benefit_projects'),
                'type' => 'options',
                'options' => function() use($project_id,$repository_id) {
                    $entries = [];
                    $projects = Project::where(function ($q) use ($project_id,$repository_id) {
                        $q->where('id', '!=', $project_id);
                        $q->where('repository_id', '=', $repository_id);
                        $q->whereNull('.deleted_at');
                    })->get();
                    foreach ($projects as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'aid_projects_persons',
                    'column' => 'project_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_benefit_projects_date_from' => [
                'label' => trans('aid-repository::application.not_benefit_projects_date_from') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_from',
                ],
            ],

            'not_benefit_projects_date_to' => [
                'label' => trans('aid-repository::application.not_benefit_projects_date_to') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_to',
                ],
            ],


            /*'education' => [
                'label' => trans('aid-repository::application.education'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\WorkWages::all() as $entry) {
                        $entries[$entry->id] = $entry->name;
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_education',
                    'column' => 'work_wage_id',
                    'fk' => 'person_id',
                ],
            ],*/

        ];
    }

    public static function filter($project_id,$repository_id,$orderby,$rules,$organization_id,$paginate,$itemsCount)
    {


        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');


        $implode = implode(',', $orderby);
        $tables = ['char_persons', 'char_cases', 'char_categories', 'char_organizations'];
        $user = \Auth::user();
        $UserType=$user->type;

        $results = DB::table('char_persons')
                     ->join('char_cases', function($q) use($organization_id,$user){
                            $q->on('char_cases.person_id', '=', 'char_persons.id')
//                                ->where('char_cases.status', '=', 0)
                                ->whereNull('char_cases.deleted_at')
                            ;
                     })
                     ->join('char_categories', 'char_categories.id', '=', 'char_cases.category_id')
                     ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
                     ->where(function($query_) use($project_id,$orderby) {
                        $query_->whereNull('char_persons.death_date');
                        $query_->whereNotIn('char_cases.person_id', function($query) use($project_id) {
                                $query->select('person_id')
                                    ->from('aid_projects_persons')
                                    ->where('project_id', '=', $project_id);
                         });
                        $query_->whereIn('char_cases.organization_id',$orderby);
                     })
                    ->orderByRaw('FIELD (char_organizations.id, ' . $implode . ') ASC')
                    ->orderBy('char_organizations.name','char_persons.first_name', 'char_persons.second_name',
                              'char_persons.third_name', 'char_persons.last_name');

        $definedRules = self::getRules($project_id,$repository_id);

        $min_not_benefit_dt = null;
        $max_not_benefit_dt = null;
        $min_not_benefit_val = null;
        $max_not_benefit_val = null;

        $min_benefit_dt = null;
        $max_benefit_dt = null;
        $min_benefit_val = null;
        $max_benefit_val = null;

        $max_benefit_projects_date_from_dt = null;
        $min_benefit_projects_date_from_dt = null;
        $max_benefit_projects_date_to_dt = null;
        $min_benefit_projects_date_to_dt = null;

        $max_not_benefit_projects_date_from_dt = null;
        $min_not_benefit_projects_date_from_dt = null;
        $max_not_benefit_projects_date_to_dt = null;
        $min_not_benefit_projects_date_to_dt = null;


        $max_dependants_birthday_dt = null;
        $min_dependants_birthday_dt = null;

        if(isset($rules['dependants_birthday'])) {
            if (isset($rules['dependants_birthday']['max'])) {
                if ($rules['dependants_birthday']['max'] != null) {
                    $max_dependants_birthday_dt = date('Y-m-d', strtotime($rules['dependants_birthday']['max']));
                }
            }

            if (isset($rules['dependants_birthday']['min'])) {
                if ($rules['dependants_birthday']['min'] != null) {
                    $min_dependants_birthday_dt = date('Y-m-d', strtotime($rules['dependants_birthday']['min']));
                }
            }
        }

        if(isset($rules['benefit_projects_date_from'])) {
            if (isset($rules['benefit_projects_date_from']['max'])) {
                if ($rules['benefit_projects_date_from']['max'] != null) {
                    $max_benefit_projects_date_from_dt = date('Y-m-d', strtotime($rules['benefit_projects_date_from']['max']));
                }
            }

            if (isset($rules['benefit_projects_date_from']['min'])) {
                if ($rules['benefit_projects_date_from']['min'] != null) {
                    $min_benefit_projects_date_from_dt = date('Y-m-d', strtotime($rules['benefit_projects_date_from']['min']));
                }
            }
        }

        if(isset($rules['benefit_projects_date_to'])) {
            if (isset($rules['benefit_projects_date_to']['max'])) {
                if ($rules['benefit_projects_date_to']['max'] != null) {
                    $max_benefit_projects_date_to_dt = date('Y-m-d', strtotime($rules['benefit_projects_date_to']['max']));
                }
            }

            if (isset($rules['benefit_projects_date_to']['min'])) {
                if ($rules['benefit_projects_date_to']['min'] != null) {
                    $min_benefit_projects_date_to_dt = date('Y-m-d', strtotime($rules['benefit_projects_date_to']['min']));
                }
            }
        }

        if(isset($rules['not_benefit_projects_date_from'])) {
            if (isset($rules['not_benefit_projects_date_from']['max'])) {
                if ($rules['not_benefit_projects_date_from']['max'] != null) {
                    $max_not_benefit_projects_date_from_dt = date('Y-m-d', strtotime($rules['not_benefit_projects_date_from']['max']));
                }
            }

            if (isset($rules['not_benefit_projects_date_from']['min'])) {
                if ($rules['not_benefit_projects_date_from']['min'] != null) {
                    $min_not_benefit_projects_date_from_dt = date('Y-m-d', strtotime($rules['not_benefit_projects_date_from']['min']));
                }
            }
        }

        if(isset($rules['not_benefit_projects_date_to'])) {
            if (isset($rules['not_benefit_projects_date_to']['max'])) {
                if ($rules['not_benefit_projects_date_to']['max'] != null) {
                    $max_not_benefit_projects_date_to_dt = date('Y-m-d', strtotime($rules['not_benefit_projects_date_to']['max']));
                }
            }

            if (isset($rules['not_benefit_projects_date_to']['min'])) {
                if ($rules['not_benefit_projects_date_to']['min'] != null) {
                    $min_not_benefit_projects_date_to_dt = date('Y-m-d', strtotime($rules['not_benefit_projects_date_to']['min']));
                }
            }
        }

        if(isset($rules['benefit_voucher_value'])) {
            if (isset($rules['benefit_voucher_value']['max'])) {
                if ($rules['benefit_voucher_value']['max'] != null) {
                    $max_benefit_val = $rules['benefit_voucher_value']['max'];
                }
            }

            if (isset($rules['benefit_voucher_value']['min'])) {
                if ($rules['benefit_voucher_value']['min'] != null) {
                    $min_benefit_val = $rules['benefit_voucher_value']['min'];
                }
            }
        }

        if(isset($rules['not_benefit_voucher_value'])) {
            if (isset($rules['not_benefit_voucher_value']['max'])) {
                if ($rules['not_benefit_voucher_value']['max'] != null) {
                    $max_not_benefit_val = $rules['not_benefit_voucher_value']['max'];
                }
            }

            if (isset($rules['not_benefit_voucher_date']['min'])) {
                if ($rules['not_benefit_voucher_date']['min'] != null) {
                    $min_not_benefit_val = $rules['not_benefit_voucher_value']['min'];
                }
            }
        }

        foreach ($rules as $name => $value) {

            if (!array_key_exists($name, $definedRules)) {
                continue;
            }

            $select = $definedRules[$name]['select'];
            $hasValue = false;
            if (isset($select['column'])) {

                $column = $select['column'];
                if ($select['column'] != 'char_cases.status'){
                    $column = $select['table'].'.'.$select['column'];
                }

                if (is_array($value)) {
                       if($name == 'not_benefit' || $name == 'benefit' || $name == 'not_own' || $name == 'own' || $name == 'need' ||
                          $name == 'not_need' || $name == 'dependants_birthday' ||
                          $name == 'not_benefit_projects' || $name == 'benefit_projects' ||
                          $name == 'benefit_projects_date_from' || $name == 'benefit_projects_date_to' ||
                          $name == 'not_benefit_projects_date_from' || $name == 'not_benefit_projects_date_to' ||
                          $name == 'benefit_vouchers' || $name == 'not_benefit_vouchers'||
                          $name == 'benefit_voucher_date'|| $name == 'not_benefit_voucher_date'||
                          $name == 'benefit_voucher_value'|| $name == 'not_benefit_voucher_value' ){
                           if(sizeof($value) > 0){
                               if($name == 'dependants_birthday' ) {
                                   if ($min_dependants_birthday_dt != null or $max_dependants_birthday_dt != null){
                                       $results->whereNotIn('char_cases.person_id', function ($query) use ($min_dependants_birthday_dt,$max_dependants_birthday_dt) {
                                           $query->select('l_person_id')
                                               ->from('char_persons_kinship')
                                               ->whereNotIn('r_person_id', function ($query) use ($min_dependants_birthday_dt,$max_dependants_birthday_dt) {
                                                   $query->select('id')
                                                       ->from('char_persons')
                                                       ->where(function ($q) use ($min_dependants_birthday_dt,$max_dependants_birthday_dt) {
                                                           $q->whereIn('marital_status_id',[10.30]);

                                                           if($min_dependants_birthday_dt != null && $max_dependants_birthday_dt != null) {
                                                               $q->where(function ($q_) use ($min_dependants_birthday_dt,$max_dependants_birthday_dt) {
                                                                   $q_->whereDate('birthday', '>=', $min_dependants_birthday_dt)
                                                                       ->whereDate('birthday', '<=', $min_dependants_birthday_dt);
                                                               });
                                                           }elseif($min_dependants_birthday_dt != null && $min_dependants_birthday_dt == null) {
                                                               $q->whereDate('birthday', '>=', $min_dependants_birthday_dt);
                                                           }elseif($min_dependants_birthday_dt == null && $min_dependants_birthday_dt != null) {
                                                               $q->whereDate('birthday', '<=', $min_dependants_birthday_dt);
                                                           }
                                                       });
                                               });

                                       });
                                   }
                               }
                               else if($name == 'benefit' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_aids')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('aid_take', 1)
                                                   ->whereIn('aid_source_id', $value);
                                           });
                                   });

//                                 $results->join('char_persons_aids  as aids_take', function($q) use($organization_id,$user,$value){
//                                        $q->on('aids_take.person_id', '=', 'char_cases.person_id')
//                                          ->where('aids_take.aid_take', '=', 1)
//                                                ->whereIn('aids_take.aid_source_id', $value);
//                                 });
                               }
                               else if($name == 'not_benefit' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_aids')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('aid_take', 1)
                                                   ->whereIn('aid_source_id', $value);
                                           });
                                   });
                               }
                               else if($name == 'own' ) {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_persons_properties')
                                           ->where(function ($sq) use ($value){
                                                   $sq->where('has_property', 1)
                                                       ->whereIn('property_id', $value);
                                           });
                                   });
                               }
                               else if($name == 'not_own' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value) {
                                       $query->select('person_id')
                                             ->from('char_persons_properties')
                                             ->where(function ($sq) use ($value){
                                                   $sq->where('has_property',1)
                                                       ->whereIn('property_id', $value);
                                             });
                                   });
                               }
                               else if($name == 'need' ) {
                                   $results->whereNotIn('char_cases.id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_cases_essentials')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('needs','!=', 0)
                                                   ->whereIn('essential_id', $value);
                                           });
                                   });
                               }
                               else if($name == 'not_need' ) {
                                   $results->whereIn('char_cases.id', function ($query) use ($value) {
                                       $query->select('person_id')
                                           ->from('char_cases_essentials')
                                           ->where(function ($sq) use ($value){
                                               $sq->where('needs', 0)
                                                   ->whereIn('essential_id', $value);
                                           });
                                   });
                               }
                               else if($name == 'benefit_projects' || $name == 'benefit_projects_date_from' || $name == 'benefit_projects_date_to'  ) {

                                   $results->whereIn('char_cases.person_id', function ($query)
                                    use ($value,$max_benefit_projects_date_from_dt,$min_benefit_projects_date_from_dt,$max_benefit_projects_date_to_dt,$min_benefit_projects_date_to_dt) {
                                       $query->select('person_id')
                                           ->from('aid_projects_persons')
                                           ->where(function ($q) use ($value,$max_benefit_projects_date_from_dt,$min_benefit_projects_date_from_dt,$max_benefit_projects_date_to_dt,$min_benefit_projects_date_to_dt) {
                                             $q->where('status', 3);

                                             $q->whereIn('project_id',
                                                 function ($q_) use ($value,$max_benefit_projects_date_from_dt,$min_benefit_projects_date_from_dt,$max_benefit_projects_date_to_dt,$min_benefit_projects_date_to_dt) {
                                                     $q_->select('id')
                                                     ->from('aid_projects')
                                                     ->whereIn('id', $value);

                                                     if($min_benefit_projects_date_from_dt != null && $max_benefit_projects_date_from_dt != null) {
                                                         $q_->whereDate('aid_projects.date_from', '>=', $min_benefit_projects_date_from_dt);
                                                         $q_->whereDate('aid_projects.date_from', '<=', $max_benefit_projects_date_from_dt);
                                                     }elseif($min_benefit_projects_date_from_dt != null && $max_benefit_projects_date_from_dt == null) {
                                                         $q_->whereDate('aid_projects.date_from', '>=', $min_benefit_projects_date_from_dt);
                                                     }elseif($min_benefit_projects_date_from_dt == null && $max_benefit_projects_date_from_dt != null) {
                                                         $q_->whereDate('aid_projects.date_from', '<=', $max_benefit_projects_date_from_dt);
                                                     }
                                                     if($min_benefit_projects_date_to_dt != null && $max_benefit_projects_date_to_dt != null) {
                                                         $q_->whereDate('aid_projects.date_to', '>=', $min_benefit_projects_date_to_dt);
                                                         $q_->whereDate('aid_projects.date_to', '<=', $max_benefit_projects_date_to_dt);
                                                     }elseif($min_benefit_projects_date_to_dt != null && $max_benefit_projects_date_to_dt == null) {
                                                         $q_->whereDate('aid_projects.date_to', '>=', $min_benefit_projects_date_to_dt);
                                                     }elseif($min_benefit_projects_date_to_dt == null && $max_benefit_projects_date_to_dt != null) {
                                                         $q_->whereDate('aid_projects.date_to', '<=', $max_benefit_projects_date_to_dt);
                                                     }
                                             });
                                        });

                                   });
                               }
                               else if($name == 'not_benefit_projects' || $name == 'not_benefit_projects_date_from' || $name == 'not_benefit_projects_date_to' ) {
                                   $results->whereNotIn('char_cases.person_id', function ($query) use ($value,$max_not_benefit_projects_date_from_dt,$min_not_benefit_projects_date_from_dt,
                                       $max_not_benefit_projects_date_to_dt , $min_not_benefit_projects_date_to_dt) {
                                       $query->select('person_id')
                                           ->from('aid_projects_persons')
                                           ->where(function ($q) use ($value,$max_not_benefit_projects_date_from_dt,$min_not_benefit_projects_date_from_dt,
                                               $max_not_benefit_projects_date_to_dt , $min_not_benefit_projects_date_to_dt) {
                                             $q->where('status', 3);
//                                             $q->whereIn('project_id', $value);

                                               $q->whereIn('project_id',
                                                   function ($q_) use ($value,$max_not_benefit_projects_date_from_dt,$min_not_benefit_projects_date_from_dt,
                                                                              $max_not_benefit_projects_date_to_dt , $min_not_benefit_projects_date_to_dt) {
                                                       $q_->select('id')
                                                           ->from('aid_projects')
                                                           ->whereIn('id', $value);

                                                       if($max_not_benefit_projects_date_from_dt != null && $min_not_benefit_projects_date_from_dt != null) {
                                                           $q_->whereDate('aid_projects.date_from', '>=', $max_not_benefit_projects_date_from_dt);
                                                           $q_->whereDate('aid_projects.date_from', '<=', $min_not_benefit_projects_date_from_dt);
                                                       }elseif($max_not_benefit_projects_date_from_dt != null && $min_not_benefit_projects_date_from_dt == null) {
                                                           $q_->whereDate('aid_projects.date_from', '>=', $max_not_benefit_projects_date_from_dt);
                                                       }elseif($max_not_benefit_projects_date_from_dt == null && $min_not_benefit_projects_date_from_dt != null) {
                                                           $q_->whereDate('aid_projects.date_from', '<=', $min_not_benefit_projects_date_from_dt);
                                                       }
                                                       if($min_not_benefit_projects_date_to_dt != null && $max_not_benefit_projects_date_to_dt != null) {
                                                           $q_->whereDate('aid_projects.date_to', '>=', $min_not_benefit_projects_date_to_dt);
                                                           $q_->whereDate('aid_projects.date_to', '<=', $max_not_benefit_projects_date_to_dt);
                                                       }elseif($min_not_benefit_projects_date_to_dt != null && $max_not_benefit_projects_date_to_dt == null) {
                                                           $q_->whereDate('aid_projects.date_to', '>=', $min_not_benefit_projects_date_to_dt);
                                                       }elseif($min_not_benefit_projects_date_to_dt == null && $max_not_benefit_projects_date_to_dt != null) {
                                                           $q_->whereDate('aid_projects.date_to', '<=', $max_not_benefit_projects_date_to_dt);
                                                       }
                                                   });
                                        });

                                   });
                               }
                               else if($name == 'benefit_vouchers' || $name == 'benefit_voucher_value') {
                                   $results->whereIn('char_cases.person_id', function ($query) use ($value ,$min_benefit_dt,$max_benefit_dt,$min_benefit_val,$max_benefit_val) {
                                       $query->select('person_id')
                                           ->from('char_vouchers_persons')
                                           ->where(function ($q) use ($value ,$min_benefit_dt,$max_benefit_dt,$min_benefit_val,$max_benefit_val) {
                                               $q->where('status', 1);
                                               if(sizeof($value) > 0){
                                                   $q->whereIn('voucher_id', $value);
                                               }

                                               if($min_benefit_dt != null && $max_benefit_dt != null) {
                                                   $q->where(function ($q_) use ($min_benefit_dt,$max_benefit_dt) {
                                                       $q_->whereDate('receipt_date', '>=', $min_benefit_dt)->whereDate('receipt_date', '<=', $max_benefit_dt);
                                                   });
                                               }
                                               elseif($min_benefit_dt != null && $max_benefit_dt == null) {
                                                   $q->whereDate('receipt_date', '>=', $min_benefit_dt);
                                               }
                                               elseif($min_benefit_dt == null && $max_benefit_dt != null) {
                                                   $q->whereDate('receipt_date', '<=', $max_benefit_dt);
                                               }

                                               if($min_benefit_val != null || $max_benefit_val != null) {
                                                   $q->whereIn('voucher_id', function ($qy) use ($min_benefit_val,$max_benefit_val) {
                                                       $qy->select('id')
                                                           ->from('char_vouchers')
                                                           ->where(function ($q) use ($min_benefit_val,$max_benefit_val) {
                                                               $q->whereNull('deleted_at');
                                                               if($min_benefit_val != null && $max_benefit_val != null) {
                                                                   $q->whereRaw(" $max_benefit_val >= char_vouchers.value");
                                                                   $q->whereRaw(" $min_benefit_val <= char_vouchers.value");
                                                               }elseif($max_benefit_val != null && $min_benefit_val == null) {
                                                                   $q->whereRaw(" $max_benefit_val >= char_vouchers.value");
                                                               }elseif($max_benefit_val == null && $min_benefit_val != null) {
                                                                   $q->whereRaw(" $min_benefit_val <= char_vouchers.value");
                                                               }
                                                           });
                                                   });
                                               }
                                           });
                                   });
                               }
                               else if($name == 'not_benefit_vouchers'  || $name == 'not_benefit_voucher_value') {
                                  $results->whereNotIn('char_cases.person_id', function ($query) use ($value ,$min_not_benefit_dt,$max_not_benefit_dt,$min_not_benefit_val,$max_not_benefit_val) {
                                       $query->select('person_id')
                                           ->from('char_vouchers_persons')
                                           ->where(function ($q) use ($value ,$min_not_benefit_dt,$max_not_benefit_dt,$min_not_benefit_val,$max_not_benefit_val) {
                                                   $q->where('status', 1);
                                                   if(sizeof($value) > 0){
                                                       $q->whereIn('voucher_id', $value);
                                                   }
                                                   if($min_not_benefit_dt != null && $max_not_benefit_dt != null) {
                                                       $q->where(function ($q_) use ($min_not_benefit_dt,$max_not_benefit_dt) {
                                                           $q_->whereDate('receipt_date', '>=', $min_not_benefit_dt)->whereDate('receipt_date', '<=', $max_not_benefit_dt);
                                                       });
                                                   }
                                                   elseif($min_not_benefit_dt != null && $max_not_benefit_dt == null) {
                                                       $q->whereDate('receipt_date', '>=', $min_not_benefit_dt);
                                                   }
                                                   elseif($min_not_benefit_dt == null && $max_not_benefit_dt != null) {
                                                       $q->whereDate('receipt_date', '<=', $max_not_benefit_dt);
                                                   }


                                               if($min_not_benefit_val != null || $max_not_benefit_val != null) {
                                                   $q->whereIn('voucher_id', function ($qy) use ($min_not_benefit_val,$max_not_benefit_val) {
                                                       $qy->select('id')
                                                           ->from('char_vouchers')
                                                           ->where(function ($q) use ($min_not_benefit_val,$max_not_benefit_val) {
                                                               $q->whereNull('deleted_at');
                                                               if($min_not_benefit_val != null && $max_not_benefit_val != null) {
                                                                   $q->whereRaw(" $max_not_benefit_val >= char_vouchers.value");
                                                                   $q->whereRaw(" $min_not_benefit_val <= char_vouchers.value");
                                                               }elseif($max_not_benefit_val != null && $min_not_benefit_val == null) {
                                                                   $q->whereRaw(" $max_not_benefit_val >= char_vouchers.value");
                                                               }elseif($max_not_benefit_val == null && $min_not_benefit_val != null) {
                                                                   $q->whereRaw(" $min_not_benefit_val <= char_vouchers.value");
                                                               }
                                                           });
                                                   });
                                               }
                                           });
                                   });
                               }
                           }
                       }
                       else{
                           if(array_key_exists('min', $value) || array_key_exists('max', $value)) {
                               if(array_key_exists('min', $value) && $value['min'] !== '') {
                                   $results->where($column, '>=', $value['min']);
                                   $hasValue = true;
                               }
                               if(in_array('max', $value) && $value['max'] !== '') {
                                   $results->where($column, '<=', $value['max']);
                                   $hasValue = true;
                               }
                           }
                           else if (!empty($value)) {

                               $results->whereIn($column, $value);
                               $hasValue = true;
                           }
                       }

                    } else if (!empty($value)) {
                        $results->where($column, $value);
                        $hasValue = true;
                    }
            }

            if ($hasValue && isset($select['table']) && !in_array($select['table'], $tables) && $select['fk'] != 'receipt_date') {
                $table = $select['table'];
                $tables[] = $table;
                $fk = $select['fk'];
                $results->join($table, 'char_persons.id', '=', "$table.$fk");
            }
        }
        $results->groupBy('char_persons.id');

//                    ->groupBy('char_cases.person_id')
        if ($paginate){
            $results->select([
                'char_persons.*',
                'char_cases.id as case_id',
                'char_cases.organization_id',
                'char_categories.name as category_name',
                'char_organizations.name as organization_name',
            ]);
            $paginate_result = $results;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate_result->count());
         //   $paginate_result = $paginate_result->toSql();
            $paginate_result = $paginate_result->paginate($records_per_page);
            return $paginate_result;

        }

        $list =$results->select([
                                    'char_cases.id as case_id',
                                    'char_cases.person_id',
                                    'char_cases.organization_id'
                                ])->get();

        return $list;
    }

}

