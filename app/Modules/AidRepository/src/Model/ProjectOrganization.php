<?php

namespace AidRepository\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Organization\Model\Organization;

class ProjectOrganization extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aid_projects_organizations';
    
    protected $fillable = ['project_id', 'organization_id', 'percentage', 'amount', 'ratio', 'quantity'];
    
    public $incrementing = false;
    
    public $timestamps = false;
    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'project_id'  => 'required',
                'organization_id'  => 'required',
                'percentage'  => 'required',
                'amount'  => 'required',
                'quantity'  => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public static function saveForProject(Project $project, $organizations)
    {
        $project_id = $project->id;
        DB::beginTransaction();
        $project->update();
        try {
            self::deleteForProject($project_id);
            foreach ($organizations as $organization) {
                $data = [
                    'project_id' => $project_id,
                    'organization_id' => $organization['organization_id'],
                    'ratio' => $organization['ratio'],
                    'amount' => $organization['amount'],
                    'quantity' => $organization['quantity'],
                ];
                $model = new self($data);
                $model->save();


                $parent_id =  $organization['organization_id'];
                $organizations_ = Organization::where(function ($q) use ($parent_id) {
                                        $q->where('type', 1);
                                        $q->where('parent_id', '=', $parent_id);
                                    })->selectRaw("round((char_organizations.container_share/100),2) as container_share , char_organizations.id")
                                        ->get();

                if(sizeof($organizations_) > 0) {
                    foreach ($organizations_ as $k_ => $v_) {
                        $parent_quantity = $model->quantity;
                        $parent_amount = $model->amount;

                        if ($model->organization_id == $project->organization_id) {
                            $parent_quantity = $model->quantity + ($project->organization_quantity);
                            $parent_amount = ( $parent_quantity * $project->amount);
                        }
                        $quantity = floor($parent_quantity * ($v_->container_share));
                        $amount = floor($parent_amount * ($v_->container_share));

                        $child_data = [
                            'project_id' => $project_id,
                            'organization_id' =>  $v_->id,
                            'ratio' => $v_->container_share,
                            'amount' => $amount,
                            'quantity' => $quantity,
                        ];
                        $child = new self($child_data);
                        $child->save();


                    }
                }

            }
            
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
    
    
    public static function saveCentralForProject(Project $project, $organizations)
    {
        $project_id = $project->id;
        DB::beginTransaction();
        $project->update();
        try {
            foreach ($organizations as $organization) {
                
                $org = ProjectOrganization::where(['project_id' => $project_id ,
                                                   'organization_id' => $organization['organization_id'] ])->first();
                
                if(!is_null($org)){
                    $model = (object)[
                        'project_id' => $project_id,
                        'organization_id' => $organization['organization_id'],
                        'ratio' => $organization['ratio'],
                        'amount' => $organization['amount'],
                        'quantity' => $organization['quantity'],
                    ];
                    
                    if($org->quantity != $organization['quantity'] || 
                        $org->quantity != $organization['quantity'] || 
                        $org->quantity != $organization['quantity']){
                        
                        ProjectOrganization::where(['project_id' => $project_id ,
                                                   'organization_id' => $organization['organization_id'] ])
                                            ->update([
                                                        'ratio' => $organization['ratio'],
                                                        'amount' => $organization['amount'],
                                                        'quantity' => $organization['quantity'],
                                                    ]);
                    }
                                        
                }else{
                    $data = [
                        'project_id' => $project_id,
                        'organization_id' => $organization['organization_id'],
                        'ratio' => $organization['ratio'],
                        'amount' => $organization['amount'],
                        'quantity' => $organization['quantity'],
                    ];
                    $model = new self($data);
                    $model->save();
                    
                }

                $parent_id =  $organization['organization_id'];
                $organizations_ = Organization::where(function ($q) use ($parent_id) {
                                        $q->where('type', 1);
                                        $q->where('parent_id', '=', $parent_id);
                                    })->selectRaw("round((char_organizations.container_share/100),2) as container_share , char_organizations.id")
                                        ->get();

                if(sizeof($organizations_) > 0) {
                    foreach ($organizations_ as $k_ => $v_) {
                        $parent_quantity = $model->quantity;
                        $parent_amount = $model->amount;

                        if ($model->organization_id == $project->organization_id) {
                            $parent_quantity = $model->quantity + $project->organization_quantity;
                            $parent_amount =  $parent_quantity * $project->amount ;
                        }
                        $quantity = floor($parent_quantity * ($v_->container_share));
                        $amount = floor($parent_amount * ($v_->container_share));

                        $child_data = [
                            'project_id' => $project_id,
                            'organization_id' =>  $v_->id,
                            'ratio' => $v_->container_share,
                            'amount' => $amount,
                            'quantity' => $quantity,
                        ];
                        $child = new self($child_data);
                        $child->save();


                    }
                }

            }
            
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
    
    public static function deleteForProject($project_id)
    {
        return DB::table('aid_projects_organizations')->where('project_id', '=', $project_id)->delete();
    }

    public static function getOrganizations($project_id,$organization_id,$project_organization_id,$sub)
    {
        if($sub){
            return  \DB::select("select `char_organizations`.`id`, `char_organizations`.`name`,
                                            COALESCE(`aid_projects_organizations`.`amount`,0) as amount,
                                            COALESCE(`aid_projects_organizations`.`quantity`,0) as max_count ,
                                            COALESCE(count(`aid_projects_persons`.`person_id`),0) as count
                                     FROM `char_organizations`
                                     left join `aid_projects_organizations` ON ((`char_organizations`.`id`= `aid_projects_organizations`.`organization_id`)and (`aid_projects_organizations`.`project_id`='$project_id'))
                                     left join `aid_projects_persons`       ON ((`aid_projects_organizations`.`organization_id`= `aid_projects_persons`.`organization_id`)and (`aid_projects_persons`.`status` != 4) and (`aid_projects_persons`.`project_id`=`aid_projects_organizations`.`project_id`))
                                     where `char_organizations`.`id` ='$organization_id'
                                     ORDER BY container_share");

        }else{

            $ancestor_id= \DB::select("SELECT a.ancestor_id FROM char_organizations_closure a
                                       WHERE a.depth = (SELECT max(b.depth) FROM char_organizations_closure b WHERE b.descendant_id = a.descendant_id)
                                       AND(a.descendant_id = '$organization_id')");
            $ancestor=$ancestor_id[0]->ancestor_id;

            if( $project_organization_id == $organization_id || $organization_id == $ancestor_id){
                $ancestor_id=$ancestor;
            }else{
                $ancestor_id=$organization_id;
            }

            return  \DB::select("select `char_organizations`.`id`, `char_organizations`.`name`,
                                            COALESCE(`aid_projects_organizations`.`amount`,0) as amount,
                                            COALESCE(`aid_projects_organizations`.`quantity`,0) as max_count ,
                                            COALESCE(count(`aid_projects_persons`.`person_id`),0) as count
                                     FROM `char_organizations`
                                     left join `aid_projects_organizations` ON ((`char_organizations`.`id`= `aid_projects_organizations`.`organization_id`)and (`aid_projects_organizations`.`project_id`=$project_id))
                                     left join `aid_projects_persons`       ON ((`aid_projects_organizations`.`organization_id`= `aid_projects_persons`.`organization_id`)and (`aid_projects_persons`.`status` != 4) and (`aid_projects_persons`.`project_id`=`aid_projects_organizations`.`project_id`))
                                     where(`char_organizations`.`type`=1) and
                                         (`char_organizations`.`deleted_at`is null) and
                                         (`char_organizations`.`id` IN(select `char_organizations_closure`.`descendant_id` as `id` FROM `char_organizations_closure` where `char_organizations_closure`.`ancestor_id` = $ancestor_id))
                                     GROUP BY  `char_organizations`.`id`
                                     ORDER BY container_share");


        }



    }

    public static function allWithOptions($options = array())
    {

        $defaults = array(
            'with_project_name' => false,
            'with_repository_name' => false,
            'with_category_name' => false,
            'with_sponsor_name' => false,
            'with_organization_name' => false,
            'project_id' => null,
            'master' => false,
            'organization_id' => null,
            'project_status' => null,
        );
        
        $options = array_merge($defaults, $options);
        
        $query = self::query()->select('aid_projects_organizations.*');

        if ($options['with_project_name'] || $options['with_repository_name'] || $options['with_organization_name']
            || $options['with_sponsor_name'] || $options['with_category_name'] || $options['project_status'] ||$options['organization_id']) {

            $DRAFT      = trans('aid-repository::application.Draft');
            $CANDIDATES = trans('aid-repository::application.Certified for nomination');
            $EXECUTION  = trans('aid-repository::application.Certified for execution');
            $CLOSED     = trans('aid-repository::application.closed');

            $query->join('aid_projects', 'aid_projects_organizations.project_id', '=', 'aid_projects.id')
                ->addSelect([
                    'aid_projects.sponsor_id',
                    'aid_projects.name AS project_name',
                    'aid_projects.amount AS project_amount',
                    'aid_projects.quantity AS project_quantity',
                    'aid_projects.organization_quantity',
                    'aid_projects.organization_id',
                    'aid_projects.status',
                ])
                ->selectRaw('
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.quantity,0) + CAST(aid_projects.organization_quantity AS DECIMAL),2)),COALESCE(aid_projects_organizations.quantity,0)) AS total_quantity,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.ratio,0),2) + COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2)),COALESCE(aid_projects_organizations.ratio,0)) AS total_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2),0) AS extra_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (CAST(aid_projects.organization_quantity AS DECIMAL)),0) AS extra_quantity,
                             round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2)  AS organization_ratio,
                             round(COALESCE(aid_projects_organizations.ratio,0),2)  AS ratio
                             ')
                ->selectRaw("CASE  
                                           WHEN aid_projects.status = 1 THEN '$DRAFT' 
                                           WHEN aid_projects.status = 2 THEN '$CANDIDATES' 
                                           WHEN aid_projects.status = 3 THEN '$EXECUTION' 
                                           WHEN aid_projects.status = 4 THEN '$CLOSED' END AS status_name");

            if ($options['with_repository_name']) {
                $query->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id')
                    ->addSelect('aid_repositories.name as repository_name');
            }
            if ($options['with_organization_name']) {
                $query->join('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                    ->addSelect('char_organizations.name as organization_name');
            }
            if ($options['with_sponsor_name']) {
                $query->join('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                    ->addSelect('sponsor.name as sponsor_name');
            }
            if ($options['with_category_name']) {
                $query->leftJoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                    ->addSelect('char_vouchers_categories.name as category_name');
            }
            if (!$options['master'] && $options['project_status']) {
                if (is_array($options['project_status'])) {
                    $query->whereIn('aid_projects.status', $options['project_status']);
                } else {
                    $query->where('aid_projects.status', '=', $options['project_status']);
                }
            }

            if ($options['organization_id']) {
                if($options['master']) {
                    $query->where('aid_projects.organization_id', $options['organization_id']);
                    $query->selectRaw('
                                    round(CAST(aid_projects.organization_quantity AS DECIMAL),2) AS total_quantity,
                                    round(( (aid_projects.organization_quantity * 100) / aid_projects.quantity ,2) AS total_ratio,
                                    aid_projects.amount,
                                    aid_projects.quantity
                             ');
                    $query->groupBy('aid_projects.id');

                }else{
                    $org=$options['organization_id'];
                    $query->whereRaw("( (aid_projects_organizations.organization_id = '$org') and (aid_projects.organization_id = '$org')) or 
                                       ((aid_projects_organizations.organization_id = '$org') and (aid_projects_organizations.ratio > 0))   ");
                }
            }

        }

        if ($options['project_id']) {
            $query->where('aid_projects_organizations.project_id', $options['project_id']);
        }

        if ($options['project_id'] && $options['organization_id']) {
            return $query->first();
        }

        return $query->get();
    }

    public static function getDescendantOrganizations($project_id,$organization_id,$project_organization_id,$sub)
    {
        $ancestor_id=$organization_id;

        return  \DB::select("select `char_organizations`.`id`, `char_organizations`.`name`,
                                            COALESCE(`aid_projects_organizations`.`amount`,0) as amount,
                                            COALESCE(`aid_projects_organizations`.`quantity`,0) as max_count ,
                                            COALESCE(count(`aid_projects_persons`.`person_id`),0) as count
                                     FROM `char_organizations`
                                     left join `aid_projects_organizations` ON ((`char_organizations`.`id`= `aid_projects_organizations`.`organization_id`)and (`aid_projects_organizations`.`project_id`=$project_id))
                                     left join `aid_projects_persons`       ON ((`aid_projects_organizations`.`organization_id`= `aid_projects_persons`.`organization_id`)and (`aid_projects_persons`.`status` != 4) and (`aid_projects_persons`.`project_id`=`aid_projects_organizations`.`project_id`))
                                     where(`char_organizations`.`type`=1) and
                                         (`char_organizations`.`deleted_at`is null) and
                                         (`char_organizations`.`id` IN(select `char_organizations_closure`.`descendant_id` as `id` FROM `char_organizations_closure` where `char_organizations_closure`.`ancestor_id` = $ancestor_id))
                                     GROUP BY  `char_organizations`.`id`
                                     ORDER BY container_share");


    }

    public static function listProjectOrganization($options = array())
    {

        $defaults = array(
            'with_project_name' => false,
            'with_repository_name' => false,
            'with_category_name' => false,
            'with_sponsor_name' => false,
            'with_organization_name' => false,
            'project_id' => null,
            'master' => false,
            'organization_id' => null,
            'parent_id' => null,
            'container_share' => 0,
            'project_status' => null,
        );

        $options = array_merge($defaults, $options);
        $org=$options['organization_id'];
        $parent_id=$options['parent_id'];
        $container_share=$options['container_share'];

        $DRAFT      = trans('aid-repository::application.Draft');
        $CANDIDATES = trans('aid-repository::application.Certified for nomination');
        $EXECUTION  = trans('aid-repository::application.Certified for execution');
        $CLOSED     = trans('aid-repository::application.closed');

        $query = self::query()
                 ->select('aid_projects_organizations.*');

        if ($options['with_project_name'] || $options['with_repository_name'] || $options['with_organization_name']
            || $options['with_sponsor_name'] || $options['with_category_name'] || $options['project_status'] ||$options['organization_id']) {

//            $query->join('aid_projects', 'aid_projects_organizations.project_id', '=', 'aid_projects.id')
            $query->join('aid_projects', function($join) {
                    $join->on('aid_projects_organizations.project_id', '=','aid_projects.id' )
                        ->whereNull('aid_projects.deleted_at');
                })
                ->addSelect([
                    'aid_projects.sponsor_id',
                    'aid_projects.allow_day',

                    'aid_projects.name AS project_name',
                    'aid_projects.amount AS project_amount',
                    'aid_projects.quantity AS project_quantity',
                    'aid_projects.organization_quantity',
                    'aid_projects.organization_id',
                    'aid_projects.status',
                ])
                ->selectRaw("
                             aid_projects.date ,
                             aid_projects.allow_day ,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.quantity,0) + CAST(aid_projects.organization_quantity AS DECIMAL),2)),COALESCE(aid_projects_organizations.quantity,0)) AS total_quantity,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.ratio,0),2) + COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2)),COALESCE(aid_projects_organizations.ratio,0)) AS total_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2),0) AS extra_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (CAST(aid_projects.organization_quantity AS DECIMAL)),0) AS extra_quantity,
                             round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2)  AS organization_ratio,
                             round(COALESCE(aid_projects_organizations.ratio,0),2)  AS ratio,
                             CASE WHEN aid_projects.organization_id = aid_projects_organizations.organization_id 
                                  then round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2) 
                                   Else 0 END as extra_ratio
                             ")
            ->selectRaw("CASE  
                                           WHEN aid_projects.status = 1 THEN '$DRAFT' 
                                           WHEN aid_projects.status = 2 THEN '$CANDIDATES' 
                                           WHEN aid_projects.status = 3 THEN '$EXECUTION' 
                                           WHEN aid_projects.status = 4 THEN '$CLOSED' END AS status_name");
            if ($options['with_repository_name']) {
//                $query->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id');
                     $query->join('aid_repositories', function($join) {
                         $join->on('aid_projects.repository_id', '=','aid_repositories.id' )
                             ->whereNull('aid_repositories.deleted_at');
                     })
                    ->addSelect('aid_repositories.name as repository_name');
            }
            if ($options['with_organization_name']) {
                $query->join('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                    ->addSelect('char_organizations.name as organization_name');
            }
            if ($options['with_sponsor_name']) {
                $query->join('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                    ->addSelect('sponsor.name as sponsor_name');
            }
            if ($options['with_category_name']) {
                $query->leftJoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                    ->addSelect('char_vouchers_categories.name as category_name');
            }
            if (!$options['master'] && $options['project_status']) {
                if (is_array($options['project_status'])) {
                    $query->whereIn('aid_projects.status', $options['project_status']);
                } else {
                    $query->where('aid_projects.status', '=', $options['project_status']);
                }
            }
            if ($options['organization_id']) {
                if($options['master']) {
                    $query->where('aid_projects.organization_id', $options['organization_id']);
                    $query->selectRaw('
                                    round(CAST(aid_projects.organization_quantity AS DECIMAL),2) AS total_quantity,
                                    round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2) AS total_ratio,
                                    aid_projects.amount,
                                    aid_projects.quantity
                             ');
                    $query->groupBy('aid_projects.id');

                }else{
                    $query->whereRaw(" ((aid_projects_organizations.organization_id = '$org') and (aid_projects.organization_id = '$org'))
                                             or 
                                            ((aid_projects_organizations.organization_id = '$org') and (aid_projects_organizations.ratio > 0) ) ");

                }
            }

        }

        $query->selectRaw("
                                      CASE WHEN aid_projects.organization_id = '$org' THEN 1 Else 0  END  AS is_mine,
                                      CASE WHEN aid_projects_organizations.ratio is null THEN  0  Else aid_projects_organizations.ratio  END  AS org_ratio,
                                      CASE WHEN aid_projects_organizations.amount is null THEN  0  Else aid_projects_organizations.amount  END  AS org_amount,
                                      CASE WHEN aid_projects_organizations.quantity is null THEN  0  Else aid_projects_organizations.quantity  END  AS org_quantity
                             ");
        if ($options['project_id']) {
            $query->where('aid_projects_organizations.project_id', $options['project_id']);
        }

        if ($options['project_id'] && $options['organization_id']) {
            return $query->first();
        }

        return $query->get();
    }

    public static function listOrganizationOrganization($options = array())
    {

        $defaults = array(
            'with_project_name' => false,
            'with_repository_name' => false,
            'with_category_name' => false,
            'with_sponsor_name' => false,
            'with_organization_name' => false,
            'project_id' => null,
            'master' => false,
            'organization_id' => null,
            'parent_id' => null,
            'container_share' => 0,
            'project_status' => null,
        );

        $options = array_merge($defaults, $options);
        $org=$options['organization_id'];
        $parent_id=$options['parent_id'];
        $container_share=$options['container_share'];

        $DRAFT      = trans('aid-repository::application.Draft');
        $CANDIDATES = trans('aid-repository::application.Certified for nomination');
        $EXECUTION  = trans('aid-repository::application.Certified for execution');
        $CLOSED     = trans('aid-repository::application.closed');

        $query = self::query()
            ->select('aid_projects_organizations.*');

        if ($options['with_project_name'] || $options['with_repository_name'] || $options['with_organization_name']
            || $options['with_sponsor_name'] || $options['with_category_name'] || $options['project_status'] ||$options['organization_id']) {

//            $query->join('aid_projects', 'aid_projects_organizations.project_id', '=', 'aid_projects.id')
            $query->join('aid_projects', function($join) {
                $join->on('aid_projects_organizations.project_id', '=','aid_projects.id' )
                    ->whereNull('aid_projects.deleted_at');
            })
                ->addSelect([
                    'aid_projects.sponsor_id',
                    'aid_projects.allow_day',

                    'aid_projects.name AS project_name',
                    'aid_projects.amount AS project_amount',
                    'aid_projects.quantity AS project_quantity',
                    'aid_projects.organization_quantity',
                    'aid_projects.organization_id',
                    'aid_projects.status',
                ])
                ->selectRaw("
                             aid_projects.date ,
                             aid_projects.allow_day ,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.quantity,0) + CAST(aid_projects.organization_quantity AS DECIMAL),2)),COALESCE(aid_projects_organizations.quantity,0)) AS total_quantity,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(aid_projects_organizations.ratio,0),2) + COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2)),COALESCE(aid_projects_organizations.ratio,0)) AS total_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ),2),0) AS extra_ratio,
                             IF(aid_projects.organization_id = aid_projects_organizations.organization_id, (CAST(aid_projects.organization_quantity AS DECIMAL)),0) AS extra_quantity,
                             round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2)  AS organization_ratio,
                             round(COALESCE(aid_projects_organizations.ratio,0),2)  AS ratio,
                             CASE WHEN aid_projects.organization_id = aid_projects_organizations.organization_id 
                                  then round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2)
                             Else 0 END as extra_ratio
                             ")
                ->selectRaw("CASE  
                                           WHEN aid_projects.status = 1 THEN '$DRAFT' 
                                           WHEN aid_projects.status = 2 THEN '$CANDIDATES' 
                                           WHEN aid_projects.status = 3 THEN '$EXECUTION' 
                                           WHEN aid_projects.status = 4 THEN '$CLOSED' END AS status_name");
            if ($options['with_repository_name']) {
//                $query->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id');
                $query->join('aid_repositories', function($join) {
                    $join->on('aid_projects.repository_id', '=','aid_repositories.id' )
                        ->whereNull('aid_repositories.deleted_at');
                })
                    ->addSelect('aid_repositories.name as repository_name');
            }
            if ($options['with_organization_name']) {
                $query->join('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                    ->addSelect('char_organizations.name as organization_name');
            }
            if ($options['with_sponsor_name']) {
                $query->join('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                    ->addSelect('sponsor.name as sponsor_name');
            }
            if ($options['with_category_name']) {
                $query->leftJoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                    ->addSelect('char_vouchers_categories.name as category_name');
            }
            if (!$options['master'] && $options['project_status']) {
                if (is_array($options['project_status'])) {
                    $query->whereIn('aid_projects.status', $options['project_status']);
                } else {
                    $query->where('aid_projects.status', '=', $options['project_status']);
                }
            }
            if ($options['organization_id']) {
                if($options['master']) {
                    $query->where('aid_projects.organization_id', $options['organization_id']);
                    $query->selectRaw('round(CAST(aid_projects.organization_quantity AS DECIMAL),2) AS total_quantity,
                                     round(COALESCE(((aid_projects.organization_quantity * 100 ) / aid_projects.quantity ) ,0),2) AS total_ratio,
                                     aid_projects.amount,
                                     aid_projects.quantity');

                    $query->groupBy('aid_projects.id');

                }else{
                    $query->whereRaw(" ((aid_projects_organizations.organization_id = '$org') and (aid_projects.organization_id = '$org'))
                                             or 
                                            ((aid_projects_organizations.organization_id = '$org') and (aid_projects_organizations.ratio > 0) ) ");

                }
            }

        }

        $query->selectRaw("
                                      CASE WHEN aid_projects.organization_id = '$org' THEN 1 Else 0  END  AS is_mine,
                                      CASE WHEN aid_projects_organizations.ratio is null THEN  0  Else aid_projects_organizations.ratio  END  AS org_ratio,
                                      CASE WHEN aid_projects_organizations.amount is null THEN  0  Else aid_projects_organizations.amount  END  AS org_amount,
                                      CASE WHEN aid_projects_organizations.quantity is null THEN  0  Else aid_projects_organizations.quantity  END  AS org_quantity
                             ");
        if ($options['project_id']) {
            $query->where('aid_projects_organizations.project_id', $options['project_id']);
        }

        if ($options['project_id'] && $options['organization_id']) {
            return $query->first();
        }

        return $query->get();
    }

    public static function organizationProjectMobile($project_id,$projects)
    {
        
        if(!isset($projects) && empty($projects))
            $projects[] = $project_id;

        $query = self::query()
            ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
            ->addSelect('char_organizations.mobile')
            ->wherein('aid_projects_organizations.project_id', $projects)
            ->get();

        $return = array();
        if(sizeof($query) > 0){
            foreach ($query as $key=>$value){
                if(!is_null($value->mobile) && $value->mobile != '' && $value->mobile != ' '){
                    $return[]=$value->mobile;
                }
            }
        }
        return $return;
    }

    

}