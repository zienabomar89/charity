<?php

namespace AidRepository\Model;

use Illuminate\Database\Eloquent\Model;

class NominationRules extends Model
{

    protected $table = 'aid_projects_nominations';
    protected $fillable = [ 'project_id','label','user_id'];
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function project()
    {
        return $this->belongsTo('AidRepository\Model\Project','project_id','id');
    }

    public function user()
    {
        return $this->belongsTo('Auth\Model\User', 'user_id', 'id');
    }
}