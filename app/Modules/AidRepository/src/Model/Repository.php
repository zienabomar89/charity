<?php

namespace AidRepository\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers;


class Repository extends Model
{
    use SoftDeletes;

    const STATUS_OPEN = 1;
    const STATUS_CLOSED = 2;

    const WITHOUT_LEVEL = 1;
    const LEVEL_ORGANIZATION = 2;
    const LEVEL_DISTRICT  = 3;
    const LEVEL_CITY  = 4;
    const LEVEL_REGION  = 5;
    const LEVEL_NEARLOCATION = 6;
    const LEVEL_SEQUARE  = 7;
    const LEVEL_MOSQUES = 8;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aid_repositories';
    
    protected $appends = ['status_name', 'status_options','level_name', 'level_options'];
    protected $dates = ['deleted_at'];
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name'  => 'required|max:255',
                'date'  => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public static function status($value = null)
    {
        $options = array(
            self::STATUS_OPEN => trans('aid-repository::application.open'),
            self::STATUS_CLOSED => trans('aid-repository::application.closed'),
        );
        
        if (null === $value) {
            return $options;
        }
        
        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public static function level($value = null)
    {
        $options = array(
            self::WITHOUT_LEVEL => trans('aid-repository::application.without_level'),
            self::LEVEL_ORGANIZATION => trans('aid-repository::application.Organization'),
            self::LEVEL_DISTRICT => trans('aid-repository::application.district'),
            self::LEVEL_CITY => trans('aid-repository::application.city'),
            self::LEVEL_REGION => trans('aid-repository::application.region'),
            self::LEVEL_NEARLOCATION => trans('aid-repository::application.nearlocation'),
            self::LEVEL_SEQUARE => trans('aid-repository::application.square'),
            self::LEVEL_MOSQUES => trans('aid-repository::application.mosques'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getStatus()
    {
        return self::status($this->status);
    }

    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }

    public function getStatusOptionsAttribute()
    {
        return self::level();
    }

    public function getLevel()
    {
        return self::level($this->level);
    }

    public function getLevelNameAttribute()
    {
        return $this->getLevel();
    }

    public function getLevelOptionsAttribute()
    {
        return self::level();
    }

    public static function filter($options)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $filters = ['name','status'];
        $user = \Auth::user();
        $organization_id=  $user->organization_id;

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    array_push($condition, ['aid_repositories.'. $key => $value]);
                }
            }
        }

        $organizations=[];
        if(isset($options['organization_id']) &&
            $options['organization_id'] !=null && $options['organization_id'] !="" && $options['organization_id'] !=[] &&
            sizeof($options['organization_id']) != 0) {
            if($options['organization_id'][0]==""){
                unset($options['organization_id'][0]);
            }
            $organizations =$options['organization_id'];
        }

         $repositories = self::query()
             ->join('char_users','char_users.id',  '=', 'aid_repositories.user_id')
             ->join('char_organizations','char_organizations.id',  '=', 'aid_repositories.organization_id');


        if(sizeof($organizations) > 0 ){
            $repositories->wherein('aid_repositories.organization_id',$organizations);
        }else{

            $user = \Auth::user();
            $UserType=$user->type;

            if($UserType == 2) {
                $repositories->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('aid_repositories.organization_id',$organization_id);
                    $anq->orwherein('aid_repositories.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $repositories->where(function ($anq) use ($organization_id) {
                    /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                    $anq->wherein('aid_repositories.organization_id', function($query) use($organization_id) {
                        $query->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }

        }

        if (count($condition) != 0) {
            $repositories->where(function ($q) use ($condition) {
                    $str = ['aid_repositories.name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $last=null;
        $first=null;

        if(isset($options['last']) && $options['last'] !=null){
            $last=date('Y-m-d',strtotime($options['last']));
        }
        if(isset($options['first']) && $options['first'] !=null){
            $first=date('Y-m-d',strtotime($options['first']));
        }
        if($first != null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
            $repositories =  $repositories->whereDate('aid_repositories.date', '<=', $last);
        }elseif($first != null && $last == null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
        }elseif($first == null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '<=', $last);
        }

        $last_created_at=null;
        $first_created_at=null;


        if(isset($options['first_created_at']) && $options['first_created_at'] !=null){
            $first_created_at=date('Y-m-d',strtotime($options['first_created_at']));
        }
        if(isset($options['last_created_at']) && $options['last_created_at'] !=null){
            $last_created_at=date('Y-m-d',strtotime($options['last_created_at']));
        }
        if($first != null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '>=', $first_created_at);
            $repositories = $repositories->whereDate('aid_repositories.created_at', '<=', $last_created_at);
        }elseif($first != null && $last == null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '>=', $first_created_at);
        }elseif($first == null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '<=', $last_created_at);
        }

        $repositories->orderBy('aid_repositories.created_at','desc');
        $repositories->orderBy('aid_repositories.name','desc');
        $repositories->groupBy('aid_repositories.id');

        if($options['action'] == 'statistic'){
             if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $repositories->whereIn('aid_repositories.id',$options['items']);  
               }      
             }
            $repositories->selectRaw("aid_repositories.id");
            $items=$repositories->get();

            $ids = [];
            foreach ($items as $key => $value) {
                $ids[]=$value->id;
            }
            return $ids;
        }

        $repositories->selectRaw("aid_repositories.*,
                                            CASE WHEN aid_repositories.organization_id = '$organization_id' THEN 1    Else 2  END  AS is_mine,
                                            CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name,
                                            CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name");


        if($options['action'] == 'xlsx'){
            
            if(isset($options['items'])){
              if(sizeof($options['items']) > 0 ){
                $repositories->whereIn('aid_repositories.id',$options['items']);  
             }      
           }  
            return $repositories->get();
        }

        $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount, $repositories->paginate()->total());
        return $repositories->paginate($records_per_page);


    }

    public static function trash($options)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $filters = ['name','status'];
        $user = \Auth::user();
        $organization_id=  $user->organization_id;

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    array_push($condition, ['aid_repositories.'. $key => $value]);
                }
            }
        }

        $organizations=[];
        if(isset($options['organization_id']) &&
            $options['organization_id'] !=null && $options['organization_id'] !="" && $options['organization_id'] !=[] &&
            sizeof($options['organization_id']) != 0) {
            if($options['organization_id'][0]==""){
                unset($options['organization_id'][0]);
            }
            $organizations =$options['organization_id'];
        }

         $repositories = self::query()->onlyTrashed()
             ->join('char_users','char_users.id',  '=', 'aid_repositories.user_id')
             ->join('char_organizations','char_organizations.id',  '=', 'aid_repositories.organization_id');


        if(sizeof($organizations) > 0 ){
            $repositories->wherein('aid_repositories.organization_id',$organizations);
        }else{

            $user = \Auth::user();
            $UserType=$user->type;

            if($UserType == 2) {
                $repositories->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('aid_repositories.organization_id',$organization_id);
                    $anq->orwherein('aid_repositories.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{
                $repositories->where(function ($anq) use ($organization_id) {
                    /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                    $anq->wherein('aid_repositories.organization_id', function($query) use($organization_id) {
                        $query->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $organization_id);
                    });
                });
            }

        }

        if (count($condition) != 0) {
            $repositories->where(function ($q) use ($condition) {
                    $str = ['aid_repositories.name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $last=null;
        $first=null;

        if(isset($options['last']) && $options['last'] !=null){
            $last=date('Y-m-d',strtotime($options['last']));
        }
        if(isset($options['first']) && $options['first'] !=null){
            $first=date('Y-m-d',strtotime($options['first']));
        }
        if($first != null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
            $repositories =  $repositories->whereDate('aid_repositories.date', '<=', $last);
        }elseif($first != null && $last == null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
        }elseif($first == null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '<=', $last);
        }

        $last_created_at=null;
        $first_created_at=null;


        if(isset($options['first_created_at']) && $options['first_created_at'] !=null){
            $first_created_at=date('Y-m-d',strtotime($options['first_created_at']));
        }
        if(isset($options['last_created_at']) && $options['last_created_at'] !=null){
            $last_created_at=date('Y-m-d',strtotime($options['last_created_at']));
        }
        if($first != null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '>=', $first_created_at);
            $repositories = $repositories->whereDate('aid_repositories.created_at', '<=', $last_created_at);
        }elseif($first != null && $last == null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '>=', $first_created_at);
        }elseif($first == null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.created_at', '<=', $last_created_at);
        }

        $repositories->orderBy('aid_repositories.created_at','desc');
        $repositories->orderBy('aid_repositories.name','desc');
        $repositories->groupBy('aid_repositories.id');

        if($options['action'] == 'statistic'){
             if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $repositories->whereIn('aid_repositories.id',$options['items']);  
               }      
             }
            $repositories->selectRaw("aid_repositories.id");
            $items=$repositories->get();

            $ids = [];
            foreach ($items as $key => $value) {
                $ids[]=$value->id;
            }
        return $ids;
        }

        $repositories->selectRaw("aid_repositories.*,
                                 CASE WHEN aid_repositories.organization_id = '$organization_id' THEN 1    Else 2  END  AS is_mine,
                                 CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name,
                                 CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name
                                 ");

        if($options['action'] == 'xlsx'){
            if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $repositories->whereIn('aid_repositories.id',$options['items']);  
               }      
             }
            return $repositories->get();
        }

        $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount, $repositories->paginate()->total());

        return $repositories->paginate($records_per_page);

    }
    
        
    public static function central($options)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $condition = [];
        $filters = ['name','status'];
        $user = \Auth::user();
        $organization_id=  $user->organization_id;
        $UserType=$user->type;
        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    array_push($condition, ['aid_repositories.'. $key => $value]);
                }
            }
        }

         $repositories = self::query()
                             ->join('char_users','char_users.id',  '=', 'aid_repositories.user_id')
                             ->join('char_organizations','char_organizations.id',  '=', 'aid_repositories.organization_id');
         
         
         $repositories->where(function ($rep) use ($organization_id,$user) {

             $rep->where(function ($sq) use ($organization_id,$user) {
                 $sq->where('aid_repositories.level','=',1);
                  if($user->type == 2)
                  {
                      $sq->whereIn('aid_repositories.id',function ($q) use ($organization_id,$user) {
                                          $q->select('repository_id')
                                            ->from('aid_projects')
                                            ->whereIn('aid_projects.id',function ($q) use ($organization_id,$user) {
                                                        $q->select('project_id')
                                                          ->from('aid_projects_persons')
                                                          ->where(function ($anq) use ($organization_id,$user) {
                                                                       $anq->where('organization_id', '!=', $user->organization_id);
                                                                       $anq->whereIn('organization_id', function($quer) use($user) {
                                                                               $quer->select('organization_id')
                                                                                    ->from('char_user_organizations')
                                                                                    ->where('user_id', '=', $user->id);
                                                                             });
                                                                        });
                                            });
                                    });
                  }
                  else
                  {
                      $sq->whereIn('aid_repositories.id',function ($q) use ($organization_id,$user) {
                                          $q->select('repository_id')
                                            ->from('aid_projects')
                                            ->whereIn('aid_projects.id',function ($q) use ($organization_id,$user) {
                                                        $q->select('project_id')
                                                          ->from('aid_projects_persons')
                                                          ->where(function ($anq) use ($organization_id,$user) {
                                                                       $anq->where('organization_id', '!=', $user->organization_id);
                                                                       $anq->whereIn('organization_id', function($quer) use($user) {
                                                                                  $quer->select('descendant_id')
                                                                                    ->from('char_organizations_closure')
                                                                                    ->where('ancestor_id', '=', $user->organization_id);
                                                                             });
                                                                        });
                                            });
                                    });
                  }
             });
             
             $rep->orWhere(function ($sq) use ($organization_id,$user) {
                 $sq->where('aid_repositories.level','!=',1);
                 if($user->type == 2)
                  {
                      $sq->whereIn('aid_repositories.id',function ($q) use ($organization_id,$user) {
                                          $q->select('repository_id')
                                            ->from('aid_projects')
                                            ->whereIn('aid_projects.id',function ($q) use ($organization_id,$user) {
                                                        $q->select('project_id')
                                                          ->from('aid_projects_organizations')
                                                          ->where(function ($anq) use ($organization_id,$user) {
                                                                       $anq->where('ratio','!=',0);
                                                                       $anq->where('organization_id', '!=', $user->organization_id);
                                                                       $anq->whereIn('organization_id', function($quer) use($user) {
                                                                               $quer->select('organization_id')
                                                                                    ->from('char_user_organizations')
                                                                                    ->where('user_id', '=', $user->id);
                                                                             });
                                                                        });
                                            });
                                    });
                  }
                  else
                  {
                      $sq->whereIn('aid_repositories.id',function ($q) use ($organization_id,$user) {
                                          $q->select('repository_id')
                                            ->from('aid_projects')
                                            ->whereIn('aid_projects.id',function ($q) use ($organization_id,$user) {
                                                        $q->select('project_id')
                                                          ->from('aid_projects_organizations')
                                                          ->where(function ($anq) use ($organization_id,$user) {
                                                                       $anq->where('ratio','!=',0);
                                                                       $anq->where('organization_id', '!=', $user->organization_id);
                                                                       $anq->whereIn('organization_id', function($quer) use($user) {
                                                                                  $quer->select('descendant_id')
                                                                                    ->from('char_organizations_closure')
                                                                                    ->where('ancestor_id', '=', $user->organization_id);
                                                                             });
                                                                        });
                                            });
                                    });
                  }
             });
        });    
                          
        $repositories->whereNotIn('user_id', function($quer) use($user) {
                        $quer->select('id')
                             ->from('char_users')
                             ->where('organization_id', '=', $user->organization_id);
                     });


        if (count($condition) != 0) {
            $repositories->where(function ($q) use ($condition) {
                    $str = ['aid_repositories.name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $last=null;
        $first=null;

        if(isset($options['last']) && $options['last'] !=null){
            $last=date('Y-m-d',strtotime($options['last']));
        }
        if(isset($options['first']) && $options['first'] !=null){
            $first=date('Y-m-d',strtotime($options['first']));
        }
        if($first != null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
            $repositories =  $repositories->whereDate('aid_repositories.date', '<=', $last);
        }elseif($first != null && $last == null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '>=', $first);
        }elseif($first == null && $last != null) {
            $repositories = $repositories->whereDate('aid_repositories.date', '<=', $last);
        }

        $repositories->orderBy('aid_repositories.date','desc');

        if($options['action'] == 'statistic'){
             if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $repositories->whereIn('aid_repositories.id',$options['items']);  
               }      
             }
            $repositories->selectRaw("aid_repositories.id");
            $items=$repositories->get();

            $ids = [];
            foreach ($items as $key => $value) {
                $ids[]=$value->id;
            }
        return $ids;
        }

        $repositories->selectRaw("aid_repositories.*,
                                  CASE WHEN aid_repositories.organization_id = '$organization_id' THEN 1    Else 2  END  AS is_mine,
                                  CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name,
                                  CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name");

        if($options['action'] == 'xlsx'){
            if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $repositories->whereIn('aid_repositories.id',$options['items']);  
               }      
             }
            return $repositories->get();
        }

        $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount, $repositories->paginate()->total());
        return $repositories->paginate($records_per_page);


    }

}