<?php

namespace AidRepository\Model;

use Common\Model\CaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Organization\Model\Organization;
use Organization\Model\OrgLocations;

class ProjectPerson extends Model
{
    const STATUS_CANDIDATE = 1;
    const STATUS_SENT  = 2;
    const STATUS_ACCEPTED  = 3;
    const STATUS_REJECT  = 4;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aid_projects_persons';

    protected $fillable = ['project_id', 'person_id', 'organization_id','nominated_organization_id', 'status','reason','nomination_id'];

    public $incrementing = false;

    protected $appends = ['status_name', 'status_options'];

    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'project_id'  => 'required',
                'person_id'  => 'required',
                'organization_id'  => 'required',
                'nominated_organization_id'  => 'required'
            );
        }

        return self::$rules;
    }

    public static function saveForProject($nomination_id,$project_id, $organization_id, $persons)
    {
        DB::beginTransaction();
        try {
//            self::deleteForProject($project_id);
            foreach ($persons as $k=>$v) {
                $data = [
                    'nomination_id' => $nomination_id,
                    'project_id' => $project_id,
                    'person_id' => $v['id'],
                    'nominated_organization_id' => $organization_id,
                    'organization_id' => $v['organization_id'],
                    'status' => self::STATUS_CANDIDATE,
                ];
                $model = new self($data);
                $model->save();
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public static function saveForProject_($project_id, $organization_id, $persons)
    {
        DB::beginTransaction();
        try {
//            self::deleteForProject($project_id);
            foreach ($persons as $k=>$v) {
                $data = [
                    'project_id' => $project_id,
                    'person_id' => $v['id'],
                    'nominated_organization_id' => $organization_id,
                    'organization_id' => $v['organization_id'],
                    'status' => self::STATUS_ACCEPTED,
                ];
                $model = new self($data);
                $model->save();
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }


    public static function deleteForProject($project_id)
    {
        return DB::table('aid_projects_persons')->where('project_id', '=', $project_id)->delete();
    }

    public static function updateStatus($project_id, $persons, $status,$reason)
    {

        $success=0;
        foreach($persons as $k=>$v){
            $updated=['status' => $status,'reason'=>null];
            if($status ==4){
                $updated['reason']=$reason;
            }
            self::query()->where([
                'project_id' => $project_id,
                'person_id' => $v['id']
            ])->update($updated);
            $success++;
        }
        if($success != 0){
            return true;
        }
        return false;

    }

    public static function removePerson($project_id, $person_id)
    {
        return self::query()->where([
            'project_id' => $project_id,
            'person_id' => $person_id,
        ])->delete();
    }

    public static function allWithOptions($options = array())
    {

        $defaults = array(
            'project_id' => null,
            'person_id' => null,
            'organization_id' => null,
            'status' => null,
            'project_owner' => false,
            'master' => false,
            'with_person_name' => false,
            'with_organization_name' => false,
            'with_candidate_organization_name' => false,
            'with_category_name' => false,
        );

        $options = array_merge($defaults, $options);
        $query = self::query();
        $query->select('aid_projects_persons.*');
        if ($options['with_person_name']) {
            $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id')
                ->selectRaw("char_persons.id_card_number',char_persons.old_id_card_number',
                                         char_persons.first_name,
                                         char_persons.second_name,
                                         char_persons.third_name,
                                         char_persons.last_name,
                                         CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday"
                );
        }

        if ($options['with_candidate_organization_name']) {
            $query->join('char_organizations', 'char_organizations.id', '=', 'aid_projects_persons.organization_id')
                ->addSelect('char_organizations.name as organization_name');
        }

        if ($options['with_organization_name']) {
            $query->join('char_organizations as candidate_org', 'candidate_org.id', '=', 'aid_projects_persons.nominated_organization_id')
                ->addSelect('candidate_org.name as candidate_organization_name');
        }

        if ($options['with_category_name']) {
            if (!$options['with_person_name']) {
                $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id');
            }
            $query->join('char_cases', 'char_cases.person_id', '=', 'char_persons.id')
                ->join('char_categories', 'char_categories.id', '=', 'char_cases.category_id')
                ->addSelect('char_categories.name as category_name');
        }

        if ($options['project_id']) {
            $query->where('project_id', $options['project_id']);
        }

        if ($options['person_id']) {
            $query->where('person_id', $options['person_id']);
        }

        if ($options['master'] == true) {
            $org=$options['organization_id'];
            $project_id=$options['project_id'];
            $query->whereRaw("
                                  ( aid_projects_persons.project_id = '$project_id' and aid_projects_persons.nominated_organization_id != '$org'
                                    and aid_projects_persons.status != 1)
            or
            ( aid_projects_persons.project_id = '$project_id' and aid_projects_persons.nominated_organization_id = '$org')

                            ");

        }else{

            if ($options['organization_id']) {
                $query->where('organization_id', $options['organization_id']);
            }
        }



        if ($options['status']) {
            if (is_array($options['status'])) {
                $query->whereIn('status', $options['status']);
            }
        }

        return $query->limit(10)->get();
    }

    public static function status($value = null)
    {
        $options = array(
            self::STATUS_CANDIDATE => trans('aid-repository::application.candidate'),
            self::STATUS_ACCEPTED => trans('aid-repository::application.accepted'),
            self::STATUS_SENT => trans('aid-repository::application.sent'),
            self::STATUS_REJECT => trans('aid-repository::application.reject'),
        );


        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getStatus()
    {
        return self::status($this->status);
    }

    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }

    public function getStatusOptionsAttribute()
    {
        return self::status();
    }


    public static function getRules($project_id,$repository_id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        return [

            'case_status' => [
                'label' => trans('aid-repository::application.status'),
                'type' => 'options',
                'options' => [
                    ['id' => 0, 'name' => trans('aid::application.active')],
                    ['id' => 1, 'name' => trans('aid::application.inactive')],
                ],
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'char_cases.status',
                    'fk' => 'person_id',
                ],
            ],
            'category' => [
                'label' => trans('aid-repository::application.case_category'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\Categories::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'category_id',
                    'fk' => 'person_id',
                ],
            ],
            'visitor_evaluation ' => [
                'label' => trans('aid::application.visitor_evaluation'),
                'type' => 'options',
                'options' => [
                    ['id' => 0, 'name' => trans('aid::application.very_bad')],
                    ['id' => 1, 'name' => trans('aid::application.bad')],
                    ['id' => 2, 'name' => trans('aid::application.good')],
                    ['id' => 3, 'name' => trans('aid::application.very_good')],
                    ['id' => 4, 'name' => trans('aid::application.excellent')],
                ],
                'select' => [
                    'table' => 'char_cases',
                    'column' => 'visitor_evaluation',
                ],
            ],

            'card_type ' => [
                'label' => trans('aid::application.card_type'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.id_card')],
                    ['id' => 2, 'name' => trans('aid::application.id_number')],
                    ['id' => 3, 'name' => trans('aid::application.passport')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'card_type',
                ],
            ],

            'gender' => [
                'label' => trans('aid-repository::application.gender'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.male')],
                    ['id' => 2, 'name' => trans('aid-repository::application.female')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'gender',
                ],
            ],

            'marital_status' => [
                'label' => trans('aid-repository::application.marital_status'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\MaritalStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->marital_status_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'marital_status_id',
                ],
            ],

            'deserted ' => [
                'label' => trans('aid::application.deserted'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'deserted',
                ],
            ],

            'refugee ' => [
                'label' => trans('aid::application.refugee'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.citizen')],
                    ['id' => 2, 'name' => trans('aid::application.refugee')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'refugee',
                ],
            ],

            'birthday' => [
                'label' => trans('aid-repository::application.birthday'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'birthday',
                ],
            ],

            'dependants' => [
                'label' => trans('aid-repository::application.dependants'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'family_cnt',
//                    'column' => DB::raw('char_get_family_count ( \'left\',char_persons.,null,char_persons.id )'),
////                    'column' => DB::raw('char_get_dependants_count(char_persons.id, \'father\')'),
                ],
            ],
            'dependants_birthday' => [
                'label' => trans('aid-repository::application.dependants_birthday') .' ( 25-01-2022 ) ',
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'birthday',
                ],
            ],
            'monthly_income' => [
                'label' => trans('aid-repository::application.monthly_income'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'monthly_income',
                ],
            ],

            'actual_monthly_income' => [
                'label' => trans('common::application.actual_monthly_income'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'actual_monthly_income',
                ],
            ],

            'receivables ' => [
                'label' => trans('common::application.receivables'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'receivables',
                ],
            ],

            'receivables_sk_amount' => [
                'label' => trans('common::application.receivables_sk_amount'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'receivables_sk_amount',
                ],
            ],

            'has_commercial_records ' => [
                'label' => trans('common::application.has_commercial_records'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'has_commercial_records',
                ],
            ],

            'active_commercial_records' => [
                'label' => trans('common::application.active_commercial_records'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_persons',
                    'column' => 'active_commercial_records',
                ],
            ],
            'work' => [
                'label' => trans('aid-repository::application.work'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'working',
                    'fk' => 'person_id',
                ],
            ],

            'workable' => [
                'label' => trans('common::application.is_can_work'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'can_work',
                    'fk' => 'person_id',
                ],
            ],

            'work_status' => [
                'label' => trans('aid-repository::application.work_status'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Work\StatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->work_status_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'work_status_id',
                    'fk' => 'person_id',
                ],
            ],

            'work_wage' => [
                'label' => trans('aid-repository::application.work_wage'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\Work\Wage::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_work',
                    'column' => 'work_wage_id',
                    'fk' => 'person_id',
                ],
            ],

            'house_property' => [
                'label' => trans('aid-repository::application.house_property'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyTypeI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_type_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'property_type_id',
                    'fk' => 'person_id',
                ],
            ],

            'roof_material' => [
                'label' => trans('aid-repository::application.roof_material'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\RoofMaterialI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'roof_material_id',
                    'fk' => 'person_id',
                ],
            ],

            'residence_condition' => [
                'label' => trans('aid-repository::application.residence_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\HouseStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'residence_condition',
                    'fk' => 'person_id',
                ],
            ],

            'indoor_condition' => [
                'label' => trans('aid-repository::application.indoor_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\FurnitureStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'indoor_condition',
                    'fk' => 'person_id',
                ],
            ],

            'house_condition' => [
                'label' => trans('aid-repository::application.house_condition'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\BuildingStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'house_condition',
                    'fk' => 'person_id',
                ],
            ],

            'habitable' => [
                'label' => trans('aid-repository::application.habitable'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\HabitableStatusI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->roof_material_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'habitable',
                    'fk' => 'person_id',
                ],
            ],
            'has_health_insurance ' => [
                'label' => trans('aid::application.has_health_insurance'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_residence',
                    'column' => 'has_health_insurance',
                ],
            ],
            'has_device ' => [
                'label' => trans('aid::application.has_device'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid-repository::application.yes')],
                    ['id' => 2, 'name' => trans('aid-repository::application.no')],
                ],
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'has_device',
                ],
            ],

            'health_condition' => [
                'label' => trans('aid-repository::application.health'),
                'type' => 'options',
                'options' => [
                    ['id' => 1, 'name' => trans('aid::application.good_status')],
                    ['id' => 2, 'name' => trans('aid::application.chronic_disease')],
                    ['id' => 2, 'name' => trans('aid::application.special_needs')],
                ],
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'condition',
                    'fk' => 'person_id',
                ],
            ]
            ,
            'health' => [
                'label' => trans('aid::application.disease_id'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\DiseaseI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->disease_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_health',
                    'column' => 'disease_id',
                    'fk' => 'person_id',
                ],
            ],


            'need' => [
                'label' => trans('aid::application.need'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Essential::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases_essentials',
                    'column' => 'needs',
                    'fk' => 'case_id',
                ],
            ],

            'not_need' => [
                'label' => trans('aid::application.not_need'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\Essential::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_cases_essentials',
                    'column' => 'needs',
                    'fk' => 'case_id',
                ],
            ],
            'own' => [
                'label' => trans('aid::application.own'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_properties',
                    'column' => 'property_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_own' => [
                'label' => trans('aid::application.not_own'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\PropertyI18n::where('language_id', $language_id)->get() as $entry) {
                        $entries[] = [
                            'id' => $entry->property_id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_properties',
                    'column' => 'property_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit' => [
                'label' => trans('aid-repository::application.benefit'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\AidSource::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_aids',
                    'column' => 'aid_source_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_benefit' => [
                'label' => trans('aid-repository::application.not_benefit'),
                'type' => 'options',
                'options' => function() use($language_id) {
                    $entries = [];
                    foreach (\Setting\Model\AidSource::all() as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_aids',
                    'column' => 'aid_source_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit_vouchers' => [
                'label' => trans('aid-repository::application.benefit_vouchers'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($project_id) {
                    $entries = [];
                    $list = Nomination::getVouchersList($project_id);
                    foreach ($list as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->title,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_vouchers_persons',
                    'column' => 'voucher_id',
                    'fk' => 'voucher_id',
                ],
            ],

//            'benefit_voucher_date' => [
//                'label' => trans('aid-repository::application.benefit_voucher_date') .' ( 25-01-2022 ) ',
//                'type' => 'range',
//                'select' => [
//                    'table' => 'char_vouchers_persons',
//                    'column' => 'receipt_date',
//                    'fk' => 'receipt_date',
//                ],
//            ],

            'benefit_voucher_value' => [
                'label' => trans('aid-repository::application.benefit_voucher_value'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_vouchers',
                    'column' => 'value',
                    'fk' => 'value',
                ],
            ],

            'not_benefit_vouchers' => [
                'label' => trans('aid-repository::application.not_benefit_vouchers'),
                'type' => 'options',
                'options' => function() use($project_id) {
                    $entries = [];
                    $list = self::getVouchersList($project_id);
                    foreach ($list as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->title,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_vouchers_persons',
                    'column' => 'voucher_id',
                    'fk' => 'voucher_id',
                ],
            ],

//            'not_benefit_voucher_date' => [
//                'label' => trans('aid-repository::application.not_benefit_voucher_date') .' ( 25-01-2022 )',
//                'type' => 'range',
//                'select' => [
//                    'table' => 'char_vouchers_persons',
//                    'column' => 'receipt_date',
//                    'fk' => 'receipt_date',
//                ],
//            ],

            'not_benefit_voucher_value' => [
                'label' => trans('aid-repository::application.not_benefit_voucher_value'),
                'type' => 'range',
                'select' => [
                    'table' => 'char_vouchers',
                    'column' => 'value',
                    'fk' => 'value',
                ],
            ],

            'benefit_projects' => [
                'label' => trans('aid-repository::application.benefit_projects'),
                'type' => 'options',
                'not_in' => 'trye',
                'options' => function() use($project_id,$repository_id) {
                    $entries = [];
                    $projects = Project::where(function ($q) use ($project_id,$repository_id) {
                        $q->where('id', '!=', $project_id);
                        $q->where('repository_id', '=', $repository_id);
                        $q->whereNull('.deleted_at');
                    })->get();
                    foreach ($projects as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'aid_projects_persons',
                    'column' => 'project_id',
                    'fk' => 'person_id',
                ],
            ],

            'benefit_projects_date_from' => [
                'label' => trans('aid-repository::application.benefit_projects_date_from') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_from',
                ],
            ],

            'benefit_projects_date_to' => [
                'label' => trans('aid-repository::application.benefit_projects_date_to') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_to',
                ],
            ],

            'not_benefit_projects' => [
                'label' => trans('aid-repository::application.not_benefit_projects'),
                'type' => 'options',
                'options' => function() use($project_id,$repository_id) {
                    $entries = [];
                    $projects = Project::where(function ($q) use ($project_id,$repository_id) {
                        $q->where('id', '!=', $project_id);
                        $q->where('repository_id', '=', $repository_id);
                        $q->whereNull('.deleted_at');
                    })->get();
                    foreach ($projects as $entry) {
                        $entries[] = [
                            'id' => $entry->id,
                            'name' => $entry->name,
                        ];
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'aid_projects_persons',
                    'column' => 'project_id',
                    'fk' => 'person_id',
                ],
            ],

            'not_benefit_projects_date_from' => [
                'label' => trans('aid-repository::application.not_benefit_projects_date_from') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_from',
                ],
            ],

            'not_benefit_projects_date_to' => [
                'label' => trans('aid-repository::application.not_benefit_projects_date_to') .' (25-01-2022)',
                'type' => 'range',
                'select' => [
                    'table' => 'aid_projects',
                    'column' => 'date',
                    'fk' => 'date_to',
                ],
            ],
            /*'education' => [
                'label' => trans('aid-repository::application.education'),
                'type' => 'options',
                'options' => function() {
                    $entries = [];
                    foreach (\Setting\Model\WorkWages::all() as $entry) {
                        $entries[$entry->id] = $entry->name;
                    }
                    return $entries;
                },
                'select' => [
                    'table' => 'char_persons_education',
                    'column' => 'work_wage_id',
                    'fk' => 'person_id',
                ],
            ],*/
        ];
    }

    public static function checkPersons($id){
        $person_id=null;

        $person = \DB::table('char_cases')
            ->join('char_categories','char_categories.id',  '=', 'char_cases.category_id')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_block_id_card_number', function($q)  {
                $q->on('char_block_id_card_number.id_card_number','=','char_persons.id_card_number');
                $q->where('char_block_id_card_number.type','=',2);
            })
            ->leftjoin('char_block_categories', function($q)  {
                $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                $q->on('char_block_categories.category_id', '=', 'char_cases.category_id');
            })
            ->where('char_cases.id',$id)
            ->selectRaw("char_persons.id,
                         char_categories.type,
                         char_cases.organization_id,
                         char_cases.user_id,
                         char_block_categories.block_id,
                         char_persons.id_card_number,
                         char_persons.old_id_card_number,
                         char_persons.adsdistrict_id,
                         char_persons.adsregion_id,
                         char_persons.adsneighborhood_id,
                         char_persons.adssquare_id,
                         char_persons.adsmosques_id,
                         char_organizations.name as organization_name,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                         ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name
                                  ")
            ->first();


        if(!is_null($person)){

            if($person->block_id != null){
                return array('status'=>false,'reason'=>'blocked','organization_name'=>$person->organization_name,'name'=>$person->full_name,'id_card_number'=>$person->id_card_number);
            }else{


                if($person->type ==2){
                    $passed = true;
                    $mainConnector= OrgLocations::getConnected($person->organization_id);
                    CaseModel::DisablePersonInNoConnected($person,$person->organization_id,$person->user_id);

                    if(!is_null($person->adsdistrict_id)){
                        if(!in_array($person->adsdistrict_id,$mainConnector)){
                            $passed = false;
                        }else{
                            if(!is_null($person->adsregion_id)){
                                if(!in_array($person->adsregion_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($person->adsneighborhood_id)){
                                        if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                            $passed = false;
                                        }else{

                                            if(!is_null($person->adssquare_id)){
                                                if(!in_array($person->adssquare_id,$mainConnector)){
                                                    $passed = false;
                                                }else{
                                                    if(!is_null($person->adsmosques_id)){
                                                        if(!in_array($person->adsmosques_id,$mainConnector)){
                                                            $passed = false;
                                                        }
                                                    }else{
                                                        $passed = false;
                                                    }
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }

                    if($passed == false){
                        return array('status'=>false,'reason'=>'out_of_regions','organization_name'=>$person->organization_name,'name'=>$person->full_name,'id_card_number'=>$person->id_card_number);
                    }
                }

                return array('status'=>true);

            }
        }

        return array('status'=>false,'reason'=>'not_case','name'=>' ','id_card_number'=>' ','organization_name'=>' ');

    }

    public static function checkPersonsUsingCard($id_card_number){

        $person = \DB::table('char_cases')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_cases.organization_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_block_id_card_number', function($q)  {
                $q->on('char_block_id_card_number.id_card_number','=','char_persons.id_card_number');
                $q->where('char_block_id_card_number.type','=',2);
            })
            ->leftjoin('char_block_categories', function($q)  {
                $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                $q->on('char_block_categories.category_id', '=', 'char_cases.category_id');
            })
            ->where('char_persons.id_card_number',$id_card_number)
            ->selectRaw("char_persons.id,
                         char_cases.organization_id,
                         char_cases.id as case_id,
                         char_cases.deleted_at,
                         char_cases.user_id,
                         char_block_categories.block_id,
                         char_persons.id_card_number,char_persons.old_id_card_number,
                         char_persons.death_date,
                         char_persons.adsdistrict_id,
                         char_persons.adsregion_id,
                         char_persons.adsneighborhood_id,
                         char_persons.adssquare_id,
                         char_persons.adsmosques_id,
                         char_organizations.name as organization_name,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                         ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name
                                  ")
            ->first();

        if(!is_null($person)){
            if($person->block_id != null){
                return array('status'=>false,'reason'=>'blocked','organization_name'=>$person->organization_name,'name'=>$person->full_name,'id_card_number'=>$person->id_card_number);
            }

            if($person->deleted_at != null){
                return array('status'=>false,'reason'=>'deleted','organization_name'=>$person->organization_name,'name'=>$person->full_name,'id_card_number'=>$person->id_card_number);
            }

            if($person->death_date != null){
                return array('status'=>false,'reason'=>'death','organization_name'=>$person->organization_name,'name'=>$person->full_name,'id_card_number'=>$person->id_card_number);
            }

            return array('status'=>true , 'organization_id'=> $person->organization_id);
        }

        return array('status'=>false,'reason'=>'not_case','name'=>' ','id_card_number'=>' ','organization_name'=>' ');

    }

    public static function filterCandidates($options,$level,$with_organizations,$organization_id)
    {

        $persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $aid_projects_persons = ['status','nomination_id'];
        $condition = [];
        $query=null;
        $all_organization=null;
        $group = false ;

        if(isset($options['persons'])){
            if(is_array($options['persons'])){
                if(sizeof($options['persons']) > 0 ){
                    $group = true ;
                }
            }
        }

        foreach ($options as $key => $value) {
            if ( $value != "" && !is_null($value) ) {
                if(in_array($key, $aid_projects_persons)) {
                    $data = ['aid_projects_persons.' . $key => $value];
                    array_push($condition, $data);
                }

                if($group == false){
                    if(in_array($key, $persons)) {
                        $data = ['char_persons.' . $key => $value];
                        array_push($condition, $data);
                    }
                }
            }
        }


        $language_id =  \App\Http\Helpers::getLocale();
        $query = self::query();
        $query->leftjoin('aid_projects_nominations', 'aid_projects_nominations.id', '=', 'aid_projects_persons.nomination_id');
        $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id');
        $query->leftjoin('char_marital_status_i18n', function($join) use ($language_id) {
            $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                ->where('char_marital_status_i18n.language_id', '=', $language_id);
        });
        $query->join('char_organizations', 'char_organizations.id', '=', 'aid_projects_persons.organization_id');
        $query->join('char_organizations as candidate_org', 'candidate_org.id', '=', 'aid_projects_persons.nominated_organization_id');

        $query->leftjoin('char_persons_contact as primary_mob_number', function($join) {
            $join->on('char_persons.id', '=','primary_mob_number.person_id')
                ->where('primary_mob_number.contact_type','primary_mobile');
        });
        $query->leftjoin('char_persons_contact as wataniya_mob_number', function($join) {
            $join->on('char_persons.id', '=','wataniya_mob_number.person_id')
                ->where('wataniya_mob_number.contact_type','wataniya');
        });
        $query->leftjoin('char_persons_contact as phone_number', function($join) {
            $join->on('char_persons.id', '=','phone_number.person_id')
                ->where('phone_number.contact_type','phone');
        });
        $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
            $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                ->where('country_name.language_id',$language_id);
        });
        $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
            $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                ->where('district_name.language_id',$language_id);
        });

        $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
            $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                ->where('region_name.language_id',$language_id);
        });

        $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
            $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                ->where('location_name.language_id',$language_id);
        });

        $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
            $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                ->where('square_name.language_id',$language_id);
        });

        $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
            $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                ->where('mosques_name.language_id',$language_id);
        });

        $query->where('aid_projects_persons.project_id', $options['project_id']);

//
//        $own_case = false ;
//        if(isset($options['out'])){
//            if($level == 1 && $with_organizations){
//                $own_case = true;
//            }
//            $query->where('aid_projects_persons.organization_id', $organization_id);
//        }
        $organizations = [];
        if (isset($options['organization_id']) != 0) {
            if($options['organization_id'] !=null && $options['organization_id'] !="" && $options['organization_id'] !=[] && sizeof($options['organization_id']) != 0) {
                if($options['organization_id'][0]==""){
                    unset($options['organization_id'][0]);
                }
                $organizations =$options['organization_id'];
            }
        }

        if(sizeof($organizations) > 0){
            $query->wherein('aid_projects_persons.organization_id',$organizations);
        }else{

            $user = \Auth::user();
            $UserType=$user->type;

            if($UserType == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('aid_projects_persons.organization_id',$organization_id);
                    $anq->orwherein('aid_projects_persons.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });
            }else{

                if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('aid_projects_persons.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }else{
                    $query->where('aid_projects_persons.organization_id', $organization_id);
                }
            }

        }
        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }


        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $query ->selectRaw("aid_projects_persons.* ,
                                      CASE WHEN aid_projects_persons.nomination_id is null THEN ' ' Else aid_projects_nominations.label  END  AS nomination_name,
                                      char_persons.id_card_number,char_persons.old_id_card_number,
                                       char_persons.family_cnt,
                                      CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                 ifnull(char_persons.second_name, ' '),' ',
                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                     CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                     CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                     CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                     CASE
                                               WHEN char_persons.gender is null THEN '-'
                                               WHEN char_persons.gender = 1 THEN '$male'
                                               WHEN char_persons.gender = 2 THEN '$female'
                                         END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN '-' Else char_marital_status_i18n.name  END  AS marital_status_name,
                                     CASE WHEN char_persons.adscountry_id  is null THEN '-' Else country_name.name END   AS country_name,
                                     CASE WHEN char_persons.adsdistrict_id  is null THEN '-' Else district_name.name END   AS district_name,
                                     CASE WHEN char_persons.adsregion_id is null THEN '-' Else region_name.name END   AS region_name,
                                     CASE WHEN char_persons.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearlocation_name,
                                     CASE WHEN char_persons.adssquare_id is null THEN '-' Else square_name.name END   AS square_name,
                                     CASE WHEN char_persons.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosques_name,
                                     CASE WHEN primary_mob_number.contact_value is null THEN '-' Else primary_mob_number.contact_value  END  AS mobile ,
                                     CASE WHEN wataniya_mob_number.contact_value is null THEN '-' Else wataniya_mob_number.contact_value  END  AS wataniya ,
                                     CASE WHEN phone_number.contact_value is null THEN '-' Else phone_number.contact_value  END  AS phone,
                                     CONCAT(ifnull(country_name.name,' '),' - ',
                                                            ifnull(district_name.name,' '),' - ',
                                                            ifnull(region_name.name,' '),' - ',
                                                            ifnull(location_name.name,' '),' - ',
                                                            ifnull(square_name.name,' '),' - ',
                                                            ifnull(mosques_name.name,' '),' - ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address,
                                      char_organizations.name as organization_name,
                                      CASE WHEN aid_projects_persons.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                      CASE WHEN aid_projects_persons.nominated_organization_id = '$organization_id' THEN 1 Else 0  END  AS nominated_by,
                                      candidate_org.name as candidate_organization_name
                                      ");
        $query->orderBy('char_organizations.name');
        $query->orderBy('char_persons.first_name');
        $query->orderBy('char_persons.second_name');
        $query->orderBy('char_persons.third_name');
        $query->orderBy('char_persons.last_name');

        if($options['action'] == 'excel'){

            if(isset($options['persons'])){
                if(is_array($options['persons'])){
                    if(sizeof($options['persons']) > 0 ){
                        $query->whereIn('char_persons.id',$options['persons']);
                    }
                }
            }
            return $query->get();
        }

        return $query->paginate(config('constants.records_per_page'));
    }

    public static function filterCandidatesStatistic($options,$level,$organization_id,$status)
    {

        $persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $candidate = ['nomination_id'];
        $condition = [];
        $query=null;
        $all_organization=null;

        foreach ($options as $key => $value) {
            if ( $value != "" && !is_null($value) ) {
                if(in_array($key, $persons)) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }else  if(in_array($key, $candidate)) {
                    $data = ['aid_projects_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
        }
        $query = self::query();
        $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id');
        $query->join('char_organizations', 'char_organizations.id', '=', 'aid_projects_persons.organization_id');
        $query->join('char_organizations as candidate_org', 'candidate_org.id', '=', 'aid_projects_persons.nomination_id');
        $query->where('project_id', $options['project_id']);

        $organizations = [];
        if (isset($options['organization_id']) != 0) {
            if($options['organization_id'] !=null && $options['organization_id'] !="" && $options['organization_id'] !=[] && sizeof($options['organization_id']) != 0) {
                if($options['organization_id'][0]==""){
                    unset($options['organization_id'][0]);
                }
                $organizations =$options['organization_id'];
            }
        }

        if(sizeof($organizations) > 0){
            $query->wherein('aid_projects_persons.organization_id',$organizations);
        }else{
            $user = \Auth::user();
            $UserType=$user->type;

            if($UserType == 2) {
                $query->where(function ($anq) use ($organization_id,$user) {
                    $anq->where('aid_projects_persons.organization_id',$organization_id);
                    $anq->orwherein('aid_projects_persons.organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                });

            }else{

                if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('aid_projects_persons.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }else{
                    $query->where('aid_projects_persons.organization_id', $organization_id);
                }
            }

        }
        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        if(!is_null($status)){
            $query->where('aid_projects_persons.status', $status)->count();
        }

        return $query->count();
    }

    public static function checkCandidates($project_id,$cards,$whereIn)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        if($whereIn){
            $query = self::query();
            $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id');
            $query->join('char_organizations', 'char_organizations.id', '=', 'aid_projects_persons.organization_id');
            $query->join('char_organizations as candidate_org', 'candidate_org.id', '=', 'aid_projects_persons.nominated_organization_id');
            $query->leftjoin('char_persons_contact as primary_mob_number', function($join) {
                $join->on('char_persons.id', '=','primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type','primary_mobile');
            });
            $query->leftjoin('char_persons_contact as wataniya_mob_number', function($join) {
                $join->on('char_persons.id', '=','wataniya_mob_number.person_id')
                    ->where('wataniya_mob_number.contact_type','wataniya');
            });
            $query->join('char_marital_status_i18n', function($join) use ($language_id) {
                $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                    ->where('char_marital_status_i18n.language_id', '=', $language_id);
            });

            $query->leftjoin('char_persons_contact as phone_number', function($join) {
                $join->on('char_persons.id', '=','phone_number.person_id')
                    ->where('phone_number.contact_type','phone');
            });
            $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            });
            $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                    ->where('region_name.language_id',$language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',$language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                    ->where('square_name.language_id',$language_id);
            });

            $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id',$language_id);
            });


            $query->where(function ($q) use ($cards,$project_id,$whereIn) {
                $q->where('project_id', $project_id);
                $q->whereIn('char_persons.id_card_number',$cards);
            });

            $male = trans('common::application.male');
            $female = trans('common::application.female');

            $query ->selectRaw("aid_projects_persons.*,
                                      CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                 ifnull(char_persons.second_name, ' '),' ',
                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                      CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                     CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                         CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                     CASE
                                               WHEN char_persons.gender is null THEN '-'
                                               WHEN char_persons.gender = 1 THEN '$male'
                                               WHEN char_persons.gender = 2 THEN '$female'
                                         END
                                     AS gender,
                                     CASE WHEN char_persons.marital_status_id is null THEN '-' Else char_marital_status_i18n.name  END  AS marital_status_name,
                                     CASE WHEN char_persons.adscountry_id  is null THEN '-' Else country_name.name END   AS country_name,
                                     CASE WHEN char_persons.adsdistrict_id  is null THEN '-' Else district_name.name END   AS district_name,
                                     CASE WHEN char_persons.adsregion_id is null THEN '-' Else region_name.name END   AS region_name,
                                     CASE WHEN char_persons.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearlocation_name,
                                     CASE WHEN char_persons.adssquare_id is null THEN '-' Else square_name.name END   AS square_name,
                                     CASE WHEN char_persons.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosques_name,
                                     CASE WHEN primary_mob_number.contact_value is null THEN '-' Else primary_mob_number.contact_value  END  AS mobile ,
                                     CASE WHEN wataniya_mob_number.contact_value is null THEN '-' Else wataniya_mob_number.contact_value  END  AS wataniya ,
                                     CASE WHEN phone_number.contact_value is null THEN '-' Else phone_number.contact_value  END  AS phone,
                                     CONCAT(ifnull(country_name.name,' '),' - ',
                                                            ifnull(district_name.name,' '),' - ',
                                                            ifnull(region_name.name,' '),' - ',
                                                            ifnull(location_name.name,' '),' - ',
                                                            ifnull(square_name.name,' '),' - ',
                                                            ifnull(mosques_name.name,' '),' - ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address,
                                                                           char_organizations.name as organization_name,
                                      candidate_org.name as candidate_organization_name
                                      ");

        }else{
            $query = DB::table('char_persons');
            $query ->selectRaw("char_persons.id_card_number,char_persons.old_id_card_number,
                                      CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                 ifnull(char_persons.second_name, ' '),' ',
                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name                                       ");

            $query->where(function ($q) use ($cards,$project_id) {
                $q->whereNotIn('char_persons.id', function($sub) use($project_id) {
                    $sub->select('person_id')
                        ->from('aid_projects_persons')
                        ->where('project_id', '=', $project_id);
                });
                $q->whereIn('char_persons.id_card_number',$cards);
            });

        }


        return $query->get();
    }

    public static function findCandidates($project_id,$person_id,$organization_id)
    {

        $query = self::query();
        $query->join('char_persons', 'char_persons.id', '=', 'aid_projects_persons.person_id');
        $query->where(function ($q) use ($person_id,$project_id,$organization_id) {
            $q->where('organization_id', '=', $organization_id);
            $q->where('project_id', $project_id);
            $q->where('char_persons.id',$person_id);
        });
        $query ->selectRaw("aid_projects_persons.*,char_persons.id_card_number,char_persons.old_id_card_number,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                             ifnull(char_persons.second_name, ' '),' ',
                             ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name                                       ");

        return $query->first();
    }

    public static function personsProjectMobile($project_id,$projects)
    {

        if(!isset($projects) && empty($projects))
            $projects[] = $project_id;

        $query = self::query()
            ->join('char_persons_contact', 'aid_projects_persons.person_id', '=', 'char_persons_contact.person_id')
            ->addSelect('char_persons_contact.contact_value')
            ->wherein('aid_projects_persons.project_id', $projects)
            ->where(['char_persons_contact.contact_type'=> 'primary_mobile'])
            ->get();

        $return = array();
        if(sizeof($query) > 0){
            foreach ($query as $key=>$value){
                if(!is_null($value->contact_value) && $value->contact_value != '' && $value->contact_value != ' '){
                    $return[]=$value->contact_value;
                }
            }
        }
        return $return;
    }

    public static function filterRelatedOfCandidates($options)
    {

        $persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $aid_projects_persons = ['status','nomination_id','project_id'];

        $condition = [];
        $projects_conditions = [];
        foreach ($options as $key => $value) {
            if ( $value != "" && !is_null($value) ) {
                if(in_array($key, $aid_projects_persons)) {
                    $projects_conditions[$key] = $value;
                }elseif(in_array($key, $persons)) {
                    $condition[$key] = $value;
                }
            }
        }

        $user = \Auth::user();
        $Org=$user->organization;
        $level = $Org->level;
        $organization_id = $user->organization_id;
        $language_id =  \App\Http\Helpers::getLocale();

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $all_organization=null;
        $organizations = [];
        if (isset($options['organization_id']) != 0) {
            if($options['organization_id'] !=null && $options['organization_id'] !="" && $options['organization_id'] !=[] && sizeof($options['organization_id']) != 0) {
                if($options['organization_id'][0]==""){
                    unset($options['organization_id'][0]);
                }
                $organizations =$options['organization_id'];
            }
        }

        $kinships = [];
        if($options['action'] == 'wives'){
            $kinships = [21,29];
        }else{
            $kinships = [2,22];
        }

        $query = \DB:: table('aid_projects_persons')
            ->join('char_persons as main', 'main.id', '=', 'aid_projects_persons.person_id')
            ->leftjoin('aid_projects_nominations', 'aid_projects_nominations.id', '=', 'aid_projects_persons.nomination_id')
            ->leftjoin('char_marital_status_i18n  as main_ms', function($join) use ($language_id) {
                $join->on('main.marital_status_id', '=', 'main_ms.marital_status_id')
                    ->where('main_ms.language_id', '=', $language_id);
            })
            ->join('char_organizations', 'char_organizations.id', '=', 'aid_projects_persons.organization_id')
            ->join('char_organizations as candidate_org', 'candidate_org.id', '=', 'aid_projects_persons.nominated_organization_id')
            ->join('char_persons_kinship', 'char_persons_kinship.l_person_id', '=', 'main.id')
            ->join('char_persons as branch', 'branch.id', '=', 'char_persons_kinship.r_person_id')
            ->leftjoin('char_marital_status_i18n', function($join) use ($language_id) {
                $join->on('branch.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                    ->where('char_marital_status_i18n.language_id', '=', $language_id);
            })
            ->leftjoin('char_persons_contact as primary_mob_number', function($join) {
                $join->on('main.id', '=','primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as wataniya_mob_number', function($join) {
                $join->on('main.id', '=','wataniya_mob_number.person_id')
                    ->where('wataniya_mob_number.contact_type','wataniya');
            })
            ->leftjoin('char_persons_contact as phone_number', function($join) {
                $join->on('main.id', '=','phone_number.person_id')
                    ->where('phone_number.contact_type','phone');
            })
            ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('main.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
                $join->on('main.adsdistrict_id', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                $join->on('main.adsregion_id', '=','region_name.location_id' )
                    ->where('region_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('main.adsneighborhood_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                $join->on('main.adssquare_id', '=','square_name.location_id' )
                    ->where('square_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('main.adsmosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id',$language_id);
            })
            ->where(function ($query_) use ($condition,$projects_conditions,$organizations,$organization_id,$user,$level,$kinships,$options) {
                $query_->whereIn('char_persons_kinship.kinship_id',$kinships);

                $group = false ;


                if ($group == false && count($condition) != 0) {
                    $query_->where(function ($q) use ($condition) {
                        $names = ['main.first_name', 'main.second_name', 'main.third_name', 'main.last_name'];
                        foreach ($condition as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar(main.$key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where('main'.$key, '=', $value);
                            }
                        }
                    });
                }

                $query_->where(function ($sq) use ($projects_conditions,$organizations,$organization_id,$user,$level) {

                    if(sizeof($projects_conditions) > 0 ){
                        foreach ($projects_conditions as $key => $value) {
                            $sq->where('aid_projects_persons.' .$key, '=', $value);
                        }
                    }
                    if(sizeof($organizations) > 0){
                        $sq->wherein('aid_projects_persons.organization_id',$organizations);
                    }
                    else{
                        if($user->type == 2) {
                            $sq->where(function ($anq) use ($organization_id,$user) {
                                $anq->where('aid_projects_persons.organization_id',$organization_id);
                                $anq->orwherein('organization_id', function($quer) use($user) {
                                    $quer->select('organization_id')
                                        ->from('char_user_organizations')
                                        ->where('user_id', '=', $user->id);
                                });
                            });
                        }else{
                            if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
                                $sq->where(function ($anq) use ($organization_id) {
                                    $anq->wherein('aid_projects_persons.organization_id', function($decq) use($organization_id) {
                                        $decq->select('descendant_id')
                                            ->from('char_organizations_closure')
                                            ->where('ancestor_id', '=', $organization_id);
                                    });
                                });
                            }else{
                                $sq->where('aid_projects_persons.organization_id', $organization_id);
                            }
                        }

                    }
                });

            });

        $candidate = trans('aid-repository::application.candidate');
        $accepted = trans('aid-repository::application.accepted');
        $sent = trans('aid-repository::application.sent');
        $reject = trans('aid-repository::application.reject');

        $query->selectRaw("aid_projects_persons.* ,
                           CASE  WHEN aid_projects_persons.status = 1 THEN '$candidate' 
                                 WHEN aid_projects_persons.status = 2 THEN '$sent' 
                                 WHEN aid_projects_persons.status = 3 THEN '$accepted' 
                                 WHEN aid_projects_persons.status = 4 THEN '$reject' 
                           END AS status_name,
                           CONCAT(ifnull(main.first_name, ' '), ' ' , ifnull(main.second_name, ' '),' ',
                                  ifnull(main.third_name,' '),' ', ifnull(main.last_name,' '))as full_name,
                           CASE WHEN main.id_card_number is null THEN '-' Else main.id_card_number  END  AS id_card_number,
                           CASE WHEN main.old_id_card_number is null THEN '-' Else main.old_id_card_number  END  AS old_id_card_number,
                           char_organizations.name as organization_name,
                           candidate_org.name as candidate_organization_name,
                           CASE WHEN aid_projects_persons.nomination_id is null THEN ' ' Else aid_projects_nominations.label  END  AS nomination_name,
                           CASE WHEN main.marital_status_id is null THEN '-' Else main_ms.name  END  AS marital_status_name,
                           CASE WHEN main.family_cnt is null THEN '-' Else main.family_cnt  END  AS family_cnt,
                           CASE WHEN main.spouses is null THEN '-' Else main.spouses  END  AS spouses,
                           CASE WHEN main.male_live is null THEN '-' Else main.male_live  END  AS male_live,
                           CASE WHEN main.female_live is null THEN '-' Else main.female_live  END  AS female_live,
                           CASE WHEN main.birthday is null THEN '-' Else main.birthday  END  AS birthday,
                           CASE WHEN main.death_date is null THEN '-' Else main.death_date  END  AS death_date,
                           CASE WHEN main.adscountry_id  is null THEN '-' Else country_name.name END   AS country_name,
                           CASE WHEN main.adsdistrict_id  is null THEN '-' Else district_name.name END   AS district_name,
                           CASE WHEN main.adsregion_id is null THEN '-' Else region_name.name END   AS region_name,
                           CASE WHEN main.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearlocation_name,
                           CASE WHEN main.adssquare_id is null THEN '-' Else square_name.name END   AS square_name,
                           CASE WHEN main.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosques_name,
                           CASE WHEN main.street_address is null THEN '-' Else main.street_address  END  AS street_address,
                           CASE WHEN primary_mob_number.contact_value is null THEN '-' Else primary_mob_number.contact_value  END  AS mobile ,
                           CASE WHEN wataniya_mob_number.contact_value is null THEN '-' Else wataniya_mob_number.contact_value  END  AS wataniya ,
                           CASE WHEN phone_number.contact_value is null THEN '-' Else phone_number.contact_value  END  AS phone,
                           CONCAT(ifnull(country_name.name,' '),' - ',ifnull(district_name.name,' '),' - ',
                                  ifnull(region_name.name,' '),' - ', ifnull(location_name.name,' '),' - ',
                                  ifnull(square_name.name,' '),' - ', ifnull(mosques_name.name,' '),' - ',
                                  ifnull(main.street_address,' '))
                           AS address,
                           CONCAT(ifnull(branch.first_name, ' '), ' ' , ifnull(branch.second_name, ' '),' ',
                                  ifnull(branch.third_name,' '),' ', ifnull(branch.last_name,' '))as related_name,
                           CASE WHEN branch.id_card_number is null THEN '-' Else branch.id_card_number  END  AS related_card_number,
                           CASE WHEN branch.old_id_card_number is null THEN '-' Else branch.old_id_card_number  END  AS related_old_id_card_number,
                           CASE WHEN branch.birthday is null THEN '-' Else DATE_FORMAT(branch.birthday,'%Y/%m/%d')  END  AS related_birthday,
                           CASE WHEN branch.gender is null THEN '-'
                                WHEN branch.gender = 1 THEN '$male'
                                WHEN branch.gender = 2 THEN '$female'
                           END 
                           AS related_gender,
                           branch.gender as gender,
                           CASE WHEN branch.marital_status_id is null THEN '-' Else char_marital_status_i18n.name  END  AS related_marital_status,
                           CASE WHEN branch.birthday is null THEN '-' Else branch.birthday  END  AS related_birthday,
                           CASE WHEN branch.death_date is null THEN '-' Else branch.death_date  END  AS related_death_date ,
                           CASE WHEN branch.prev_family_name is null THEN '-' Else branch.prev_family_name  END  AS related_prev_family_name


                           ");

        if(isset($options['persons'])){
            if(is_array($options['persons'])){
                if(sizeof($options['persons']) > 0 ){
                    $query->whereIn('main.id',$options['persons']);
                }
            }
        }
        return $query->get();

        if($options['action'] == 'excel'){

        }

        return $query->paginate(config('constants.records_per_page'));
    }


    public static function StatisticCentralCount($project_id,$user,$status,$level)
    {

        return ProjectPerson::Where(function ($sq) use ($user,$project_id , $status,$level) {
            $sq->where('project_id', $project_id);
            if(!is_null($status)){
                $sq->where('aid_projects_persons.status', $status);
            }

            $sq->where(function($quer_) use($user,$level) {
                if($user->type == 2){
                    $quer_->whereIn('organization_id', function($quer) use($user) {
                        $quer->select('organization_id')
                            ->from('char_user_organizations')
                            ->where('user_id', '=', $user->id);
                    });
                }
                else
                {
                    $quer_->whereIn('organization_id', function($quer) use($user) {
                        $quer->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $user->organization_id);
                    });
                }

                if($level != 1){
                    $quer_->orWhere('aid_projects_persons.organization_id', $user->organization_id);
                }
            });


        })
            ->count();


    }


}