<?php

namespace AidRepository\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Organization\Model\Organization;
use App\Http\Helpers;

class Project extends Model
{
    use SoftDeletes;

    const STATUS_DRAFT      = 1;
    const STATUS_CANDIDATES = 2;
    const STATUS_EXECUTION= 3;
    const STATUS_CLOSED     = 4;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aid_projects';

    protected $appends = ['status_name', 'status_options'];
    protected $dates = ['deleted_at'];
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'repository_id'  => 'required',
                'organization_id'  => 'required',
                'sponsor_id'  => 'required',
                'name'  => 'required',
                'case_category_id' => 'required',
                'allow_day'  => 'required',
                'amount'  => 'required',
                'quantity'  => 'required',
            );
        }

        return self::$rules;
    }

    public function Repository()
    {
        return $this->belongsTo('AidRepository\Model\Repository','repository_id','id');
    }
    public function rules()
    {
        return $this->hasMany('AidRepository\Model\NominationRules','project_id','id');
    }

    public static function findOrFailWithOptions($id, $options = array())
    {
        if (isset($options['with_repository_name']) && $options['with_repository_name']) {
            $entries = DB::table('aid_projects')
                ->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id')
                ->where('aid_projects.id', $id)
                ->select('aid_projects.*', 'aid_repositories.level as repository_level', 'aid_repositories.name as repository_name');

            $model = new self();
            $model->forceFill((array) $entries->first());
            return $model;
        }

        return self::findOrFail($id);
    }

    public static function findOrFailWithDetails($id)
    {

        $entries = DB::table('aid_projects')
                   ->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id')
                   ->join('char_organizations as custom_org', 'aid_projects.custom_organization', '=', 'custom_org.id')
                   ->join('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                   ->join('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                   ->join('char_currencies', 'char_currencies.id', '=', 'aid_projects.currency_id')
                   ->join('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                   ->where('aid_projects.id', $id)
                   ->select('aid_projects.*', 'aid_repositories.name as repository_name' , 'aid_repositories.date as repository_date' ,
                             'custom_org.name as custom_organization_name' ,
                             'char_organizations.name as organization_name' ,
                             'sponsor.name as sponsor_name' ,
                             'char_currencies.name as currency_name' ,
                             'char_vouchers_categories.name as category_name');

        $model = new self();
        $model->forceFill((array) $entries->first());
        return $model;
    }

    public static function allWithOptions($options = array(),$organization_id)
    {
        $default = array(
            'repository_id' => null,
            'category_id' => null,
            'sponsor_id' => null,
            'repository_org_id' => null,
            'organization_id' => null,
            'with_organization_name' => false,
            'with_sponsor_name' => false,
            'with_category_name' => false,
            'with_repository_name' => false,
            'with_organizations' => false,
            'with_count' => false,
            'paginate' => 0,
        );
        $options = array_merge($default, $options);
        $language_id =  \App\Http\Helpers::getLocale();

        $repository_org_id =  $options['repository_org_id'];
        $projects = DB::table('aid_projects')
                      ->selectRaw("aid_projects.* ,
                                   get_project_total( 1, aid_projects.id , $repository_org_id ) as total_ratio,
                                   get_project_total( 2, aid_projects.id , $repository_org_id ) as total_amount,
                                   get_project_total( 3, aid_projects.id , $repository_org_id ) as total_quantity,
                                   organization_quantity,
                                   aid_projects.custom_quantity")
                      ->whereNull('aid_projects.deleted_at');

        if ($options['with_count']) {
            $projects->selectRaw("get_project_persons_count(aid_projects.id , 3 ) as accepted,
                                   get_project_persons_count(aid_projects.id , 1 ) as nominated");
        }

        if ($options['with_repository_name']) {
            $projects->join('aid_repositories', 'aid_projects.repository_id', '=', 'aid_repositories.id')
                ->addSelect('aid_repositories.name as repository_name');
        }
        if ($options['with_organization_name']) {
            $projects->join('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                     ->leftjoin('char_organizations as custom_org', 'aid_projects.custom_organization', '=', 'custom_org.id')
                     ->addSelect('custom_org.name as custom_organization_name')
                     ->addSelect('char_organizations.name as organization_name');
        }
        if ($options['with_sponsor_name']) {
            $projects->join('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                ->addSelect('sponsor.name as sponsor_name');
        }
        if ($options['with_category_name']) {
            $projects->leftJoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                ->addSelect('char_vouchers_categories.name as category_name');
        }
        
        if (!empty($options['repository_id'])) {
            $projects->where('aid_projects.repository_id', '=', $options['repository_id']);
        }
        if (!empty($options['category_id'])) {
            $projects->where('aid_projects.category_id', '=', $options['category_id']);
        }
        if (!empty($options['organization_id'])) {
            $projects->where('aid_projects.organization_id', '=', $options['organization_id']);
        }
        if (!empty($options['sponsor_id'])) {
            $projects->where('aid_projects.sponsor_id', '=', $options['sponsor_id']);
        }

        if ($options['with_organizations']) {
            $projects->leftJoin('aid_projects_organizations', 'aid_projects.id', '=', 'aid_projects_organizations.project_id')
                ->addSelect('aid_projects_organizations.organization_id as project_organization_id', 'aid_projects_organizations.amount as organization_amount', 'aid_projects_organizations.quantity as organization_quantity',
                    'aid_projects_organizations.ratio as project_organization_ratio');
        }

        $projects->leftJoin('char_categories as ca', 'ca.id', '=', 'aid_projects.case_category_id')
                 ->selectRaw("CASE
                                   WHEN aid_projects.case_category_id is null THEN ' '
                                   WHEN $language_id = 1 THEN  ca.name
                                   WHEN $language_id != 2 THEN  ca.en_name
                             END AS case_category_name " );
        
        $projects->leftjoin('char_currencies', 'char_currencies.id', '=', 'aid_projects.currency_id')
                  ->selectRaw('char_currencies.name as currency_name');

        if ($options['paginate'] > 0) {
            return $projects->paginate($options['paginate']);
        }

        if ($options['with_organizations']) {
            $entries = [];
            foreach ($projects->get() as $entry) {
                if (!isset($entries[$entry->id])) {
                    $entries[$entry->id] = array(
                        'id' => $entry->id,
                        'name' => $entry->name,
                        'exchange_rate' => $entry->exchange_rate,
                        'repository_id' => $entry->repository_id,
                        'total_ratio' => $entry->total_ratio,
                        'total_amount' => $entry->total_amount,
                        'total_quantity' => $entry->total_quantity,
                        'category_id' => $entry->category_id,
                        'sponsor_id' => $entry->sponsor_id,
                        'organization_id' => $entry->organization_id,
                        'organization_quantity' => $entry->organization_quantity,
                        'custom_quantity' => $entry->custom_quantity,
                        'amount' => $entry->amount,
                        'quantity' => $entry->quantity,
                        'notes' => $entry->notes,
                        'created_at' => $entry->created_at,
                        'updated_at' => $entry->updated_at,
                        'status' => $entry->status,
                        'organizations' => array(),
                        'repository_name' => isset($entry->repository_name)? $entry->repository_name : '',
                        'category_name' => isset($entry->category_name)? $entry->category_name : '',
                        'organization_name' => isset($entry->organization_name)? $entry->organization_name : '',
                        'sponsor_name' => isset($entry->sponsor_name)? $entry->sponsor_name : '',
                    );
                }
                if (isset($entry->project_organization_id) && $entry->project_organization_id) {
                    $entries[$entry->id]['organizations'][$entry->project_organization_id] = array(
                        'id' => $entry->project_organization_id,
                        'amount' => $entry->organization_amount,
                        'quantity' => $entry->organization_quantity,
                        'ratio' => $entry->project_organization_ratio,
                    );
                }
            }

            $clear = [];
            foreach ($entries as $entry) {
                $clear[] = $entry;
            }

            return $clear;
        }

        return $projects->get();
    }

    public static function status($value = null)
    {
        $options = array(
            self::STATUS_DRAFT      => trans('aid-repository::application.Draft'),
            self::STATUS_CANDIDATES => trans('aid-repository::application.Certified for nomination'),
            self::STATUS_EXECUTION     => trans('aid-repository::application.Certified for execution'),
            self::STATUS_CLOSED     => trans('aid-repository::application.closed'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getStatus()
    {
        return self::status($this->status);
    }

    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }

    public function getStatusOptionsAttribute()
    {
        return self::status();
    }

    public static function filter($options,$organization_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();

        $count = 0;
        $query = DB::table('aid_repositories')
                  ->join('char_organizations', 'aid_repositories.organization_id', '=', 'char_organizations.id');

        if(isset($options['repository_id'])){
            $query->whereIn('aid_repositories.id', $options['repository_id']);
        }

        $query->selectRaw('aid_repositories.id,
                           char_organizations.level as org_level,
                           aid_repositories.level,
                           aid_repositories.organization_id, 
                           aid_repositories.name as repository_name , 
                           aid_repositories.date as repository_date');
        $repositories = $query->get();

        if(sizeof($repositories) > 0){
            foreach ($repositories as $key=>$value){
                $value->count = 0;
                $value->projects = DB::table('aid_projects')
                    ->leftjoin('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                    ->leftjoin('char_organizations as custom_org', 'aid_projects.custom_organization', '=', 'custom_org.id')
                    ->leftjoin('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                    ->leftjoin('char_currencies', 'char_currencies.id', '=', 'aid_projects.currency_id')
                    ->leftjoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                    ->where('aid_projects.repository_id', $value->id)
                    ->selectRaw("aid_projects.*,
                                 CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                 CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                 CASE  WHEN $language_id = 1 THEN custom_org.name Else custom_org.en_name END  AS custom_organization_name,
                                 CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name,
                                 char_vouchers_categories.name as category_name
                                ")->get();

                if($value->level == 1 || $value->level == '1' ){
                    $value->count =  sizeof($value->projects);

                }else{
                    if(sizeof($value->projects) > 0 ){
                        foreach ($value->projects as $k=>$v){
                            $v->count = 0;
                            $v->_count = 0;
                            $v->has_descendants = false;
                            $v->organizations_=\DB::table('aid_projects_organizations')
                                ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                                ->where('aid_projects_organizations.project_id', '=', $v->id)
                                ->where('char_organizations.parent_id', '=', $value->organization_id)
                                ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                            char_organizations.name ,
                                            char_get_organizations_child_nominated_count(char_organizations.id,aid_projects_organizations.project_id ) as child_nominated_count,
                                            char_get_organization_nominated_count(char_organizations.id, aid_projects_organizations.project_id  ) as nominated_count,
                                            round((char_organizations.container_share/100),2) as container_share")->get();

                            if(sizeof( $v->organizations_) > 0){
                                foreach ( $v->organizations_ as $k_=>$v_){
                                    $v_->descendants =\DB::table('aid_projects_organizations')
                                        ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                                        ->where('aid_projects_organizations.project_id', '=',  $v->id)
                                        ->where('char_organizations.parent_id', '=', $v_->organization_id)
                                        ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                             char_organizations.name ,
                                             char_get_organization_nominated_count(char_organizations.id,aid_projects_organizations.project_id ) as nominated_count,
                                             round((char_organizations.container_share/100),2) as container_share")->get();

                                    $v_->_count = sizeof($v_->descendants ) ;

                                    if(sizeof($v_->descendants ) > 0 ){
                                        $count += (sizeof($v_->descendants ) * 3 );
                                        $v->count += (sizeof($v_->descendants ) * 3 );
                                        $v->_count += sizeof($v_->descendants ) ;
                                    }else{
                                        $v->_count += 1 ;
                                        $v->count++;
                                        $count ++;
                                    }

                                }

                                $v->has_descendants = true;
                            }

                            $value->count =  $value->count + $v->count;

                        }
                    }
                }
            }
        }

        return $repositories;
    }


    public static function listOrganizationProjects($options = array(),$user,$filters)
    {


        $org=$options['organization_id'];
        $defaults = array(
            'with_project_name' => false,
            'with_repository_name' => false,
            'with_category_name' => false,
            'with_sponsor_name' => false,
            'with_organization_name' => false,
            'project_id' => null,
            'master' => false,
            'organization_id' => null,
            'parent_id' => null,
            'itemsCount' => 50,
            'container_share' => 0,
            'project_status' => null,
        );

        $options = array_merge($defaults, $options);

        $UserId=$user->id;
        $UserOrg=$user->organization;
        $level = $UserOrg->level;
        $UserType=$user->type;

        $DRAFT      = trans('aid-repository::application.Draft');
        $CANDIDATES = trans('aid-repository::application.Certified for nomination');
        $EXECUTION  = trans('aid-repository::application.Certified for execution');
        $CLOSED     = trans('aid-repository::application.closed');

        $WITHOUT_LEVEL = trans('aid-repository::application.without_level');
        $LEVEL_ORGANIZATION = trans('aid-repository::application.Organization');
        $LEVEL_DISTRICT = trans('aid-repository::application.district');
        $LEVEL_CITY = trans('aid-repository::application.city');
        $LEVEL_REGION = trans('aid-repository::application.region');
        $LEVEL_NEARLOCATION = trans('aid-repository::application.nearlocation');
        $LEVEL_SEQUARE = trans('aid-repository::application.square');
        $LEVEL_MOSQUES = trans('aid-repository::application.mosques');

        $query = \DB::table('aid_projects')
                        ->join('aid_repositories', function($join) {
                             $join->on('aid_projects.repository_id', '=','aid_repositories.id' )
                                  ->whereNull('aid_repositories.deleted_at');
                        })
                        ->leftJoin('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                        ->leftJoin('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                        ->leftJoin('char_currencies', 'char_currencies.id', '=', 'aid_projects.currency_id')
                        ->leftJoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                        ->selectRaw("aid_projects.id as project_id,
                                     aid_projects.organization_id,
                                     aid_projects.exchange_rate,
                                     aid_projects.allow_day,
                                     aid_projects.category_id,
                                     aid_projects.case_category_id,
                                     aid_projects.currency_id,
                                     CASE
                                            WHEN aid_projects.currency_id is null THEN '-'
                                            Else char_currencies.name
                                       END  AS currency,
                                     aid_projects.sponsor_id,
                                     sponsor.name as sponsor_name ,
                                     char_organizations.name as organization_name,
                                     char_vouchers_categories.name as category_name ,
                                     aid_repositories.name as repository_name ,
                                     aid_repositories.status as repository_status ,
                                     aid_repositories.level as repository_level ,
                                     CASE  
                                           WHEN aid_repositories.level = 1 THEN '$WITHOUT_LEVEL' 
                                           WHEN aid_repositories.level = 2 THEN '$LEVEL_ORGANIZATION' 
                                           WHEN aid_repositories.level = 3 THEN '$LEVEL_DISTRICT' 
                                           WHEN aid_repositories.level = 4 THEN '$LEVEL_CITY' 
                                           WHEN aid_repositories.level = 5 THEN '$LEVEL_REGION' 
                                           WHEN aid_repositories.level = 6 THEN '$LEVEL_NEARLOCATION' 
                                           WHEN aid_repositories.level = 7 THEN '$LEVEL_SEQUARE' 
                                           WHEN aid_repositories.level = 8 THEN '$LEVEL_MOSQUES' 
                                           END 
                                     AS repository_level_name,
                                     aid_projects.name AS project_name,
                                     CASE WHEN aid_projects.organization_id = '$org' THEN 1 Else 0  END  AS is_mine,
                                     aid_projects.amount AS project_amount,
                                     CASE WHEN aid_projects.organization_id = '$org' THEN 
                                               (CAST(aid_projects.organization_quantity AS DECIMAL) )
                                          Else 0  
                                     END  AS extra_quantity,
                                     CASE WHEN aid_projects.organization_id = '$org' THEN 
                                              round(COALESCE(((aid_projects.organization_quantity * 100) / aid_projects.quantity),0),2) 
                                          Else 0
                                     END  AS extra_ratio,
                                     aid_projects.allow_day,
                                     aid_projects.date ,
                                     aid_projects.status,
                                     CASE  
                                           WHEN aid_projects.status = 1 THEN '$DRAFT' 
                                           WHEN aid_projects.status = 2 THEN '$CANDIDATES' 
                                           WHEN aid_projects.status = 3 THEN '$EXECUTION' 
                                           WHEN aid_projects.status = 4 THEN '$CLOSED' 
                                           END 
                                     AS status_name");


        if($UserType == 2) {
            // naeb
            $query->selectRaw("CASE WHEN aid_repositories.level = 1 THEN 
                                         round(COALESCE(( ( char_get_project_person_organization_count ( aid_projects.id,'$UserId',1 ) / aid_projects.quantity )),2),2) 
                                          Else round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',1,1) / 100),0),2)  END  
                               AS ratio,
                               CASE WHEN aid_repositories.level = 1 THEN char_get_project_person_organization_count ( aid_projects.id,'$UserId',1 )
                                   Else round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',1,null)),0),2)  END  
                               AS quantity ");

        }
        else{
            // user LEVEL_BRANCH_CENTER
            if ($level == Organization::LEVEL_BRANCH_CENTER) {
                $query->selectRaw("CASE WHEN aid_repositories.level = 1 THEN 
                                        round(COALESCE(( ( char_get_project_person_organization_count ( aid_projects.id,'$org',2 ) / aid_projects.quantity )* 10),2),2)
                                        Else round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',2,1) / 100),0),2)
                                   END AS ratio,
                                   CASE WHEN aid_repositories.level = 1 THEN  char_get_project_person_organization_count ( aid_projects.id,'$org',2 ) 
                                       Else  round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',2,null)),2),2)
                                   END AS quantity ");

            }
            else{
                // user 3adi
                $query->selectRaw("CASE WHEN aid_repositories.level = 1 THEN 
                                               round(COALESCE(( ( char_get_project_person_organization_count ( aid_projects.id,'$org',0 ) / aid_projects.quantity )),2),2) 
                                          Else round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',0,1) / 100),0),2)  END  
                                   AS ratio,
                                   CASE WHEN aid_repositories.level = 1 THEN char_get_project_person_organization_count ( aid_projects.id,'$org',0 )
                                      Else round(COALESCE((char_get_project_organization_data(aid_projects.id,'$org',0,null)),0),2)  END  
                                  AS quantity ");


            }
        }

        if (isset($filters['repository_name'])) {
            if (!(is_null($filters['repository_name']) || $filters['repository_name'] == '' || $filters['repository_name'] == ' ')) {
            }
        }

        $query->where(function($squ) use ($org , $options,$level,$user,$UserType,$UserId,$filters){
            $squ->whereNull('aid_projects.deleted_at');
           // $squ->whereNull('aid_repositories.deleted_at');

            if (!$options['master'] && $options['project_status']) {
                $squ->whereIn('aid_projects.status', [Project::STATUS_CANDIDATES, Project::STATUS_CLOSED, Project::STATUS_EXECUTION]);
            }

        });

        $query->where(function($squ) use ($org , $options,$level,$user,$UserType,$UserId,$filters){

            if($UserType == 2) {
                // naeb
                $squ->where(function ($Sqr_) use ($org,$user,$UserId) {
                    $Sqr_->whereRaw("((aid_repositories.level != 1 and aid_repositories.organization_id != '$org') or 
                                ((aid_repositories.level = 1 and char_get_project_person_organization_count ( aid_projects.id,'$UserId',1 ) > 0 ) or
                                (aid_repositories.level != 1 and char_get_project_organization_data(aid_projects.id,'$org',1,null) > 0 )))");

                });
                $squ->where(function ($Sqr_) use ($org,$user) {
                               $Sqr_->where(function ($Sqp) use ($org,$user) {
                                   $Sqp->whereIn('aid_projects.id', function ($Sq) use ($org,$user) {
                                       $Sq->select('project_id')
                                           ->from('aid_projects_persons')
                                           ->where(function ($anq) use ($org,$user) {
                                               $anq->where('aid_projects_persons.organization_id', $org);
                                               $anq->orwherein('aid_projects_persons.organization_id', function ($quer) use ($user) {
                                                   $quer->select('organization_id')
                                                       ->from('char_user_organizations')
                                                       ->where('user_id', '=', $user->id);
                                               });
                                           });
                                   });
                                   $Sqp->orwhereIn('aid_projects.id', function ($Sq) use ($org) {
                                       $Sq->select('project_id')
                                           ->from('aid_projects_organizations')
                                           ->whereRaw(" (aid_projects_organizations.organization_id = $org and aid_projects_organizations.ratio > 0 ) ");
                                   });
                               });
                    $Sqr_->orwhereRaw("(aid_projects.organization_id = $org and (aid_projects.organization_quantity > 0  or aid_projects.custom_quantity > 0 ) )      ");

                });
            }
            else{
                // user LEVEL_BRANCH_CENTER
                            if ($level == Organization::LEVEL_BRANCHES) {
                                // user 3adi
                                $squ->where(function ($Sqr_) use ($org,$user,$UserId) {
                                    $Sqr_->whereRaw(" aid_repositories.deleted_at is null and  ((aid_repositories.level != 1 and aid_repositories.organization_id != '$org') or 
                                                 ((aid_repositories.level = 1 and char_get_project_person_organization_count ( aid_projects.id,'$org',0 )  > 0 ) or
                                                 (aid_repositories.level != 1 and char_get_project_organization_data(aid_projects.id,'$org',0,null) > 0 )))");
                                });
                                $squ->where(function ($Sqr_) use ($org) {
                                    $Sqr_->whereIn('aid_projects.id', function ($Sq) use ($org) {
                                        $Sq->select('project_id')
                                            ->from('aid_projects_persons')
                                            ->whereRaw(" (aid_projects_persons.organization_id = $org) ");
                                    });

                                    $Sqr_->orwhereIn('aid_projects.id', function ($Sq) use ($org) {
                                        $Sq->select('project_id')
                                            ->from('aid_projects_organizations')
                                            ->whereRaw(" (aid_projects_organizations.organization_id = $org and aid_projects_organizations.ratio > 0 ) ");
                                    });

                                    $Sqr_->orwhereRaw("(aid_projects.organization_id = $org and (aid_projects.organization_quantity > 0  or aid_projects.custom_quantity > 0 ) )      ");
                                });
                            }
                            else if ($level == Organization::LEVEL_BRANCH_CENTER) {
                                $squ->where(function ($Sqr_) use ($org,$user,$UserId) {
                                    $Sqr_->whereRaw("  aid_repositories.deleted_at is null and ((aid_repositories.level != 1 and aid_repositories.organization_id != '$org') or  
                                                 ((aid_repositories.level = 1 and char_get_project_person_organization_count ( aid_projects.id,'$org',2 )  > 0 ) or
                                                 (aid_repositories.level != 1 and char_get_project_organization_data(aid_projects.id,'$org',2,null) > 0 )))");
                                });
                                $squ->where(function ($Sqr) use ($org,$user) {
                                    $Sqr->where(function ($Sqpd) use ($org,$user) {
                                        $Sqpd->whereIn('aid_projects.id', function ($Sq) use ($org) {
                                            $Sq->select('project_id')
                                                ->from('aid_projects_persons')
                                                ->wherein('aid_projects_persons.organization_id', function ($qy) use ($org) {
                                                    $qy->select('descendant_id')
                                                        ->from('char_organizations_closure')
                                                        ->where('ancestor_id', '=', $org);
                                                });
                                        });
                                        $Sqpd->orwhereIn('aid_projects.id', function ($Sq) use ($org) {
                                            $Sq->select('project_id')
                                                ->from('aid_projects_organizations')
                                                ->whereRaw(" (aid_projects_organizations.organization_id = $org and aid_projects_organizations.ratio > 0 ) ");
                                        });
                                    });
                                    $Sqr->orwhereRaw("(aid_projects.organization_id = $org and (aid_projects.organization_quantity > 0  or aid_projects.custom_quantity > 0 )  )     ");
                                });
                            }
                            else{
                                $squ->where(function ($Sqr) use ($org,$user) {
                                    $Sqr->where(function ($Sqru) use ($org,$user) {
                                        $Sqru->whereIn('aid_projects.id', function ($Sq) use ($org) {
                                            $Sq->select('project_id')
                                                ->from('aid_projects_persons')
                                                ->whereRaw(" (aid_projects_persons.organization_id = $org) ");
                                        });
                                        $Sqru->orwhereIn('aid_projects.id', function ($Sq) use ($org) {
                                            $Sq->select('project_id')
                                                ->from('aid_projects_organizations')
                                                ->whereRaw(" (aid_projects_organizations.organization_id = $org and aid_projects_organizations.ratio > 0 ) ");
                                        });
                                    });

                                    $Sqr->orwhereRaw("(aid_projects.organization_id = $org and (aid_projects.organization_quantity > 0  or aid_projects.custom_quantity > 0 )  )     ");
                                });
                            }
            }

        });

        $query->where(function($squ) use ($org , $options,$level,$user,$UserType,$UserId,$filters){

            if (isset($filters['repository_name'])) {
                if (!(is_null($filters['repository_name']) || $filters['repository_name'] == '' || $filters['repository_name'] == ' ')) {
                    $squ->whereRaw("normalize_text_ar(aid_repositories.name) like  normalize_text_ar(?)", "%".$filters['repository_name']."%");
                }
            }

            if (isset($filters['category_id'])) {
                if (!(is_null($filters['category_id']) || $filters['category_id'] == '' || $filters['category_id'] == ' ')) {
                    $squ->whereRaw("aid_projects.category_id = ?",$filters['category_id']);
                }
            }
            if (isset($filters['sponsor_id'])) {
                if (!(is_null($filters['sponsor_id']) || $filters['sponsor_id'] == '' || $filters['sponsor_id'] == ' ')) {
                    $squ->whereRaw("aid_projects.sponsor_id = ?",$filters['category_id']);
                }
            }
            if (isset($filters['project_status'])) {
                if (!(is_null($filters['project_status']) || $filters['project_status'] == '' || $filters['project_status'] == ' ')) {
                    $squ->whereRaw("aid_projects.status = ?",$filters['project_status']);
                }
            }

            if (isset($filters['project_name'])) {
                if (!(is_null($filters['project_name']) || $filters['project_name'] == '' || $filters['project_name'] == ' ')) {
                    $squ->whereRaw("normalize_text_ar(aid_projects.name) like  normalize_text_ar(?)", "%".$filters['project_name']."%");
                }
            }

            $date_to=null;
            $date_from=null;

            if(isset($filters['project_date_to']) && $filters['project_date_to'] !=null){
                $date_to=date('Y-m-d',strtotime($options['project_date_to']));
            }
            if(isset($filters['project_date_from']) && $filters['project_date_from'] !=null){
                $date_from=date('Y-m-d',strtotime($filters['project_date_from']));
            }
            if($date_from != null && $date_to != null) {
                $squ->whereDate('aid_projects.date', '>=', $date_from);
                $squ->whereDate('aid_projects.date', '<=', $date_to);
            }elseif($date_from != null && $date_to == null) {
                $squ->whereDate('aid_projects.date', '>=', $date_from);
            }elseif($date_from == null && $date_to != null) {
                $squ->whereDate('aid_projects.date', '<=', $date_to);
            }

        });

       // $query->groupBy('aid_projects.id');
       // return $query->toSql();
       // return $query->get();

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        $paginate = $query->paginate($records_per_page);
        return $paginate;
    }

    public static function filterForCentral($options)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Auth::user();

        $count = 0;
        $query = DB::table('aid_repositories')
                  ->join('char_organizations', 'aid_repositories.organization_id', '=', 'char_organizations.id');

        if(isset($options['repository_id'])){
            $query->whereIn('aid_repositories.id', $options['repository_id']);
        }

        $query->selectRaw('aid_repositories.id,
                           char_organizations.level as org_level,
                           aid_repositories.level,
                           aid_repositories.organization_id, 
                           aid_repositories.name as repository_name , 
                           aid_repositories.date as repository_date');
        $repositories = $query->get();

        if(sizeof($repositories) > 0){
            foreach ($repositories as $key=>$value){
                $value->count = 0;
                $value->projects = DB::table('aid_projects')
                    ->leftjoin('char_organizations', 'aid_projects.organization_id', '=', 'char_organizations.id')
                    ->leftjoin('char_organizations as custom_org', 'aid_projects.custom_organization', '=', 'custom_org.id')
                    ->leftjoin('char_organizations as sponsor', 'aid_projects.sponsor_id', '=', 'sponsor.id')
                    ->leftjoin('char_currencies', 'char_currencies.id', '=', 'aid_projects.currency_id')
                    ->leftjoin('char_vouchers_categories', 'aid_projects.category_id', '=', 'char_vouchers_categories.id')
                    ->where('aid_projects.repository_id', $value->id)
                    ->selectRaw("aid_projects.*,
                                 CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                 CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                 CASE  WHEN $language_id = 1 THEN custom_org.name Else custom_org.en_name END  AS custom_organization_name,
                                 CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name,
                                 char_vouchers_categories.name as category_name
                                ")->get();

                if($value->level == 1 || $value->level == '1' ){
                    $value->count =  sizeof($value->projects);

                }else{
                    if(sizeof($value->projects) > 0 ){
                        foreach ($value->projects as $k=>$v){
                            $v->count = 0;
                            $v->_count = 0;
                            $v->has_descendants = false;
                            $v->organizations_=\DB::table('aid_projects_organizations')
                                ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                                 ->where(function ($q) use ($value,$user,$v) {
//                                ->where('aid_projects_organizations.project_id', '=', $v->id)
//                                ->where('char_organizations.parent_id', '=', $value->organization_id)
                                        $q->where('aid_projects_organizations.project_id', '=', $v->id);
                    //                    $q->where('char_organizations.parent_id', '=', $organization_id);            
                                        $q->where('char_organizations.id', '!=', $user->organization_id);
                    //                    $q->where('container_share', '<>', 0);
                                         if($user->type == 2){
                                              $q->whereIn('char_organizations.id', function($quer) use($user) {
                                                     $quer->select('organization_id')
                                                          ->from('char_user_organizations')
                                                          ->where('user_id', '=', $user->id);
                                              });
                                        }
                                        else{
                                              $q->wherein('char_organizations.id', function($query) use($user) {
                                                      $query->select('descendant_id')
                                                            ->from('char_organizations_closure')
                                                            ->where('ancestor_id', '=', $user->organization_id);
                                              });
                                        }
                                })
                                ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                            char_organizations.name ,
                                            char_get_organizations_child_nominated_count(char_organizations.id,aid_projects_organizations.project_id ) as child_nominated_count,
                                            char_get_organization_nominated_count(char_organizations.id, aid_projects_organizations.project_id  ) as nominated_count,
                                            round((char_organizations.container_share/100),2) as container_share")->get();

                            if(sizeof( $v->organizations_) > 0){
                                foreach ( $v->organizations_ as $k_=>$v_){
                                    $v_->descendants =\DB::table('aid_projects_organizations')
                                        ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                                        ->where('aid_projects_organizations.project_id', '=',  $v->id)
                                        ->where('char_organizations.parent_id', '=', $v_->organization_id)
                                        ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                             char_organizations.name ,
                                             char_get_organization_nominated_count(char_organizations.id,aid_projects_organizations.project_id ) as nominated_count,
                                             round((char_organizations.container_share/100),2) as container_share")->get();

                                    $v_->_count = sizeof($v_->descendants ) ;

                                    if(sizeof($v_->descendants ) > 0 ){
                                        $count += (sizeof($v_->descendants ) * 3 );
                                        $v->count += (sizeof($v_->descendants ) * 3 );
                                        $v->_count += sizeof($v_->descendants ) ;
                                    }else{
                                        $v->_count += 1 ;
                                        $v->count++;
                                        $count ++;
                                    }

                                }

                                $v->has_descendants = true;
                            }

                            $value->count =  $value->count + $v->count;

                        }
                    }
                }
            }
        }

        return $repositories;
    }

}