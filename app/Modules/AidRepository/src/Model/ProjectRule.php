<?php

namespace AidRepository\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProjectRule extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'aid_projects_rules';
    
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    protected static $rules;
    
    

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'nomination_id'  => 'required',
                'project_id'  => 'required',
                'rule_name'  => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public static function saveForProject($nomination_id , $project_id, $rules)
    {
        DB::beginTransaction();
        try {
            DB::table('aid_projects_rules')->where('nomination_id', $nomination_id)->delete();
            foreach ($rules as $name => $value) {
                $model = new self();
                $model->nomination_id = $nomination_id;
                $model->project_id = $project_id;
                $model->rule_name = $name;
                $model->rule_value = serialize($value);
                $model->save();
            }

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }
}