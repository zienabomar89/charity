<?php
namespace AidRepository\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Setting;
use Common\Model\AidsCases;
use Common\Model\CloneGovernmentPersons;
use Common\Model\GovServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use AidRepository\Model\Repository;
use AidRepository\Model\Project;
use AidRepository\Model\ProjectPerson;
use AidRepository\Model\ProjectOrganization;
use Organization\Model\Organization;
use Common\Model\Person;
use Setting\Model\aidsLocation;
use Aid\Model\Vouchers;
use App\Http\Helpers;

class ProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get aid_projects on repository using repository_id
    public function index()
    {
        $this->authorize('manage', Project::class);
        $user = Auth::user();

        $request = request();
        $projects = Project::allWithOptions(array(
            'repository_id' => $request->input('repository_id'),
            'with_organization_name' => true,
            'with_sponsor_name' => true,
            'with_category_name' => false,
            'with_organizations' => true,
            'with_count' => false
        ),$user->organization_id);
        return response()->json($projects);
    }

    // save new aid_repository_projects
    public function store(Request $request)
    {
        $this->authorize('create', Project::class);

        $error = \App\Http\Helpers::isValid($request->all(),Project::getValidatorRules());
        if($error)
            return response()->json($error);

        $repository_id = $request->input('repository_id');
        $repository = Repository::findOrFail($repository_id);
        if ($repository->status == Repository::STATUS_CLOSED) {
            return response()->json(['error' => trans('aid-repository::application.cannot be added project to a closed container')], 422);
        }

        $project = new Project();
        $project->repository_id = $repository_id;
        $project->status = Project::STATUS_DRAFT;

        $dates = ['date_from','date_to'];
        $insert = ['name','category_id','case_category_id','sponsor_id','organization_id',
            'organization_quantity','custom_organization','custom_quantity',
            'amount','quantity','exchange_rate','currency_id',
            'notes','allow_day','date_from','date_to'];

        foreach ($insert as $key) {
            if(isset($request->$key)){
                if(in_array($key,$dates)){
                    $project->$key =date('Y-m-d',strtotime($request->$key));
                }
                else{
                    $project->$key = $request->$key;
                }
            }
        }

        $nulls = ['custom_organization','custom_quantity','date_from','date_to'];
        foreach ($nulls as $key) {
            if(!isset($request->$key)){
                $project->$key = null;
            } else{
                if(!($request->$key =='' || $request->$key ==' ')){
                    $project->$key = null;
                }
            }
        }

        $user = \Auth::user();
        $district_id = $user->organization->district_id  ;

        $district_code = Helpers::getDistrictCode($user->organization_id,$district_id);
        $mSerial= $district_code.date('Y').date('m').'-';

        $project_cnt = Project::query()
            ->where(function ($q) use ($mSerial) {
                $q->whereRaw("serial like  ?", $mSerial."%");
            })
            ->count();

        $project_cnt++;
        $project->serial = $mSerial.$project_cnt;

        if($project->saveOrFail()){

            $repository = Repository::findOrFail($repository_id);
            if($repository->level != 1 && $repository->level != '1' ){
                $proj_quantity = $project->quantity - $project->organization_quantity;
                $proj_amount = $project->amount * $proj_quantity;

                $organization_id = $repository->organization_id;
                $aid_projects_organizations = [];
                $organization_quantity = $project->organization_quantity;
                $project_custom_quantity = 0;
                $project_custom_ratio = 0;

                if($project->custom_quantity > 0 ){
                    if($project->custom_quantity){
                        if($project->custom_quantity != 0 && $project->custom_quantity != '0'){
                            $project_custom_quantity = $project->custom_quantity;
                            $project_custom_ratio = ( ( $project_custom_quantity * 100 ) / $proj_quantity ) ;
                        }
                    }
                }
                $organizations = Organization::with(['descendants_'])
                    ->where(function ($q) use ($organization_id) {
                        $q->where('type', 1);
                        $q->where('parent_id', '=', $organization_id);
                        $q->where('id', '<>', $organization_id);
                    })
                    ->selectRaw("char_organizations.* ,char_organizations.container_share as container_share")
                    ->orderBy('container_share')->get();

                $custom_org = Organization::where('id', $project->custom_organization)->first();
                $has_subtract_ratio = false ;
                if($custom_org){
                    $original_ration = $custom_org->container_share;
                    if($project_custom_ratio > $original_ration){
                        $changes_ = ($project_custom_ratio - $original_ration) / (100 - $original_ration);
                        $has_subtract_ratio = true;
                    }
                }

                if($has_subtract_ratio){
                    foreach ($organizations as $k=>$v){
                        $temp= ['project_id'=> $project->id,'organization_id'=> $v->id];
                        if($v->id == $project->custom_organization){
                            $temp['ratio'] = round($project_custom_ratio,1);
                            $temp['quantity'] = $project_custom_quantity;
                            $temp['amount'] = round(( $proj_amount * $project_custom_quantity  * $project->exchange_rate),1);
                        }
                        else{
                            $temp['ratio'] = round(($v->container_share - ($v->container_share * $changes_)),1);
                            $temp['quantity'] = round($proj_quantity *( ($v->container_share - ($v->container_share * $changes_)) / 100),0);
                            $temp['amount'] = round(($proj_amount *(($v->container_share - ($v->container_share * $changes_)) / 100) * $project->exchange_rate),0);
                        }
                        $aid_projects_organizations[]= $temp;

                        $parent_id =  $v->id;
                        $organizations_ = Organization::where(function ($q) use ($parent_id) {
                            $q->where('type', 1);
                            $q->where('parent_id', '=', $parent_id);
                        })->selectRaw("round(char_organizations.container_share,1) as container_share , char_organizations.id")
                            ->get();

                        if(sizeof($organizations_) > 0) {
                            foreach ($organizations_ as $k_ => $v_) {
                                $temp_ = ['project_id' => $project->id, 'organization_id' => $v_->id, 'ratio' => $v_->container_share];
                                $parent_quantity = $temp['quantity'];
                                $parent_amount = $temp['amount'];

                                if ($temp['organization_id'] == $project->organization_id) {
                                    $parent_quantity += $organization_quantity;
                                    $parent_amount = ($parent_quantity * $project->amount * $project->exchange_rate);
                                }
                                $temp_['quantity'] = floor($parent_quantity);
                                $temp_['amount'] = floor($parent_amount);
                                $aid_projects_organizations[] = $temp_;
                            }
                        }
                    }
                }
                else{
                    foreach ($organizations as $k=>$v){
                        $temp= ['project_id'=> $project->id,'organization_id'=> $v->id];
                        $temp['ratio'] = round(($v->container_share),1);
                        $temp['quantity'] = floor($proj_quantity *($v->container_share /100));
                        $temp['amount'] = (($proj_amount *($v->container_share/100)) * $project->exchange_rate);
                        $aid_projects_organizations[]= $temp;
                        $parent_id =  $v->id;
                        $organizations_ = Organization::where(function ($q) use ($parent_id) {
                            $q->where('type', 1);
                            $q->where('parent_id', '=', $parent_id);
                        })->selectRaw("char_organizations.container_share , char_organizations.id")
                            ->get();

                        if(sizeof($organizations_) > 0){
                            foreach ($organizations_ as $k_=>$v_){
                                $temp_= ['project_id'=> $project->id,'organization_id'=> $v_->id , 'ratio' => $v_->container_share];
                                $parent_quantity = $temp['quantity'] ;
                                $parent_amount =$temp['amount'] ;
                                if($temp['organization_id'] == $project->organization_id){
                                    $parent_quantity = $temp['quantity'] + $organization_quantity;
                                    $parent_amount = $temp['amount'] + ( $organization_quantity *  $project->amount);
                                }
                                $temp_['quantity'] = floor($parent_quantity);
                                $temp_['amount'] = floor($parent_amount * $project->exchange_rate);
                                $aid_projects_organizations[]= $temp_;
                            }
                        }
                    }
                }
                ProjectOrganization::insert($aid_projects_organizations);

            }

            \Log\Model\Log::saveNewLog('PROJECT_CREATED',trans('aid-repository::application.Added a new project')  . ' "'.$project->name. '" ');
            return response()->json(['status'=>true]);
        }

        return response()->json(['status'=>false]);
    }

    // get aid_repository_projects by id
    public function show($id)
    {
        $request = request();
        $project = Project::findOrFailWithOptions($id, array(
            'with_repository_name' => $request->query('repository', false),
        ));
//        $this->authorize('view', $project);
        return response()->json($project);
    }

    // update details aid_repository_projects
    public function update(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $this->authorize('update', $project);

        $repository = Repository::findOrFail($project->repository_id);
        if ($repository->status == Repository::STATUS_CLOSED) {
            return response()->json(['error' => trans('aid-repository::application.Cannot edit project data in a closed repository')], 422);
        }

        $project_amount_old = $project->amount;
        $project_quantity_old = $project->quantity;
        $project_organization_id_old = $project->organization_id;
        $project_organization_quantity_old = $project->organization_quantity;
        $project_custom_organization_old = $project->custom_organization;
        $project_custom_quantity_old = $project->custom_quantity;

        $dates = ['date_from','date_to'];
        $insert = ['name','category_id','case_category_id','sponsor_id','organization_id',
            'organization_quantity','custom_organization','custom_quantity',
            'amount','quantity','exchange_rate','currency_id',
            'notes','allow_day','date_from','date_to'];

        foreach ($insert as $key) {
            if(isset($request->$key)){
                if(in_array($key,$dates)){
                    $project->$key =date('Y-m-d',strtotime($request->$key));
                }
                else{
                    $project->$key = $request->$key;
                }
            }
        }

        $nulls = ['custom_organization','custom_quantity','date_from','date_to'];
        foreach ($nulls as $key) {
            if(!isset($request->$key)){
                $project->$key = null;
            } else{
                if(!($request->$key =='' || $request->$key ==' ')){
                    $project->$key = null;
                }
            }
        }

        if($project->save()){

            $repository = Repository::findOrFail($project->repository_id);
            if($repository->level != 1 && $repository->level != '1' ) {

                if($project_amount_old != $project->amount ||
                    $project_quantity_old != $project->quantity ||
                    $project_organization_id_old != $project->organization_id ||
                    $project_organization_quantity_old != $project->organization_quantity ||
                    $project_custom_organization_old != $project->custom_organization ||
                    $project_custom_quantity_old != $project->custom_quantity){

                    ProjectOrganization::where(['project_id'=> $project->id])->delete();

                    $proj_quantity = $project->quantity - $project->organization_quantity;
                    $proj_amount = $project->amount * $proj_quantity;

                    $organization_id = $repository->organization_id;
                    $aid_projects_organizations = [];
                    $project_custom_ratio = 0;
                    $project_custom_quantity = 0;

                    if($project->custom_ratio){
                        if($project->custom_ratio != 0 && $project->custom_ratio != '0'){
                            $project_custom_ratio = $project->custom_ratio;
                            $project_custom_quantity = $project->custom_quantity;
                        }
                    }
                    $organizations = Organization::with(['descendants_'])
                        ->where(function ($q) use ($organization_id) {
                            $q->where('type', 1);
                            $q->where('parent_id', '=', $organization_id);
                            $q->where('id', '<>', $organization_id);
                        })
                        ->selectRaw("char_organizations.* ,char_organizations.container_share as container_share")
                        ->orderBy('container_share')->get();

                    $custom_org = Organization::where('id', $project->custom_organization)->first();
                    $has_subtract_ratio = false ;
                    if($custom_org){
                        $original_ration = $custom_org->container_share;
                        if($project_custom_ratio > $original_ration){
                            $changes_ = ($project_custom_ratio - $original_ration) / (100 - $original_ration);
                            $has_subtract_ratio = true;
                        }
                    }

                    if($has_subtract_ratio){
                        foreach ($organizations as $k=>$v){
                            $temp= ['project_id'=> $project->id,'organization_id'=> $v->id];
                            if($v->id == $project->custom_organization){
                                $temp['ratio'] = round($project_custom_ratio,1);
                                $temp['quantity'] = $project_custom_quantity;
                                $temp['amount'] = round( $proj_amount * $project_custom_quantity * $project->exchange_rate,1);
                            }
                            else{
                                $temp['ratio'] = round(($v->container_share - ($v->container_share * $changes_)),1);
                                $temp['quantity'] = round($proj_quantity *( ($v->container_share - ($v->container_share * $changes_)) / 100),0);
                                $temp['amount'] = round($proj_amount * $temp['quantity'] * $project->exchange_rate,0);
                            }
                            $aid_projects_organizations[]= $temp;

                            $parent_id =  $v->id;
                            $organizations_ = Organization::where(function ($q) use ($parent_id) {
                                $q->where('type', 1);
                                $q->where('parent_id', '=', $parent_id);
                            })->selectRaw("round(char_organizations.container_share,1) as container_share , char_organizations.id")
                                ->get();

                            if(sizeof($organizations_) > 0) {
                                foreach ($organizations_ as $k_ => $v_) {
                                    $temp_ = ['project_id' => $project->id, 'organization_id' => $v_->id, 'ratio' => $v_->container_share];
                                    $parent_quantity = $temp['quantity'];
                                    $parent_amount = $temp['amount'];

                                    if ($temp['organization_id'] == $project->organization_id) {
                                        $parent_quantity = $temp['quantity'] + $project->organization_quantityy;
                                        $parent_amount = ( $parent_quantity * $project->amount  * $project->exchange_rate);
                                    }

                                    $temp_['quantity'] = floor($parent_quantity);
                                    $temp_['amount'] = floor($parent_amount);
                                    $aid_projects_organizations[] = $temp_;
                                }
                            }
                        }
                    }
                    else{
                        foreach ($organizations as $k=>$v){
                            $temp= ['project_id'=> $project->id,'organization_id'=> $v->id];
                            $temp['ratio'] = round(($v->container_share),1);
                            $temp['quantity'] = floor($proj_quantity *($v->container_share /100));
                            $temp['amount'] = ($proj_amount * $temp['quantity'] * $project->exchange_rate);
                            $aid_projects_organizations[]= $temp;
                            $parent_id =  $v->id;
                            $organizations_ = Organization::where(function ($q) use ($parent_id) {
                                $q->where('type', 1);
                                $q->where('parent_id', '=', $parent_id);
                            })->selectRaw("char_organizations.container_share , char_organizations.id")
                                ->get();

                            if(sizeof($organizations_) > 0){
                                foreach ($organizations_ as $k_=>$v_){
                                    $temp_= ['project_id'=> $project->id,'organization_id'=> $v_->id , 'ratio' => $v_->container_share];
                                    $parent_quantity = $temp['quantity'] ;
                                    $parent_amount =$temp['amount'] ;
                                    if($temp['organization_id'] == $project->organization_id){
                                        $parent_quantity = $temp['quantity'] + $project->organization_quantityy;
                                        $parent_amount = ( $parent_quantity *  $project->amount  * $project->exchange_rate);
                                    }
                                    $temp_['quantity'] = floor($parent_quantity);
                                    $temp_['amount'] = floor($parent_amount);
                                    $aid_projects_organizations[]= $temp_;
                                }
                            }
                        }
                    }

                    ProjectOrganization::insert($aid_projects_organizations);

                }
                else
                {
                    $organizations = ProjectOrganization::where(['project_id'=> $project->id])->get();
                    foreach ($organizations as $k=>$v){
                        ProjectOrganization::where(['project_id'=> $project->id,'organization_id'=> $v->organization_id])
                            ->update(['amount'=> $v->quantity * $project->amount * $project->exchange_rate]);
                    }
                }

            }
            \Log\Model\Log::saveNewLog('PROJECT_UPDATED',trans('aid-repository::application.edited the project data'). ' "'.$project->name. '" ');
            return response()->json(['status'=>true,'project'=> ProjectOrganization::where(['project_id'=> $project->id])->get()]);
        }

        return response()->json(['status'=>false]);
    }

    // update project status aid_repository_projects
    public function updateProjectStatus(Request $request, $id)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $project = Project::findOrFail($id);
        $this->authorize('update', $project);

        $repository = Repository::findOrFail($project->repository_id);
        if ($repository->status == Repository::STATUS_CLOSED) {
            return response()->json(['status'=>false , 'msg' => trans('aid-repository::application.Cannot edit project data in a closed repository')]);
        }

        $status = $request->input('status');
        if ($status == Project::STATUS_CLOSED || $status == Project::STATUS_EXECUTION) {
            if($repository->level != 1 && $repository->level != '1' ){

                $language_id =  \App\Http\Helpers::getLocale();
                $organizations = \DB::table('char_organizations')
                    ->whereIn('id',function ($w) use ($id){
                        $w->select('organization_id')
                            ->from('aid_projects_organizations')
                            ->where(function ($q) use ($id){
                                $q->where('project_id' , $id);
                                $q->whereNotIn('organization_id', function ($query) use ($id) {
                                    $query->select('char_vouchers.organization_id')
                                        ->from('char_vouchers')
                                        ->where(function ($q_0) use ($id) {
                                            $q_0->whereNull('deleted_at');
                                            $q_0->where('project_id' , $id);
                                        });
                                });
                            });
                    })
                    ->selectRaw("CASE WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name")
                    ->groupBy('char_organizations.id')
                    ->get();

                $persons = \DB::table('char_persons')
                              ->whereIn('id',function ($w) use ($id){
                                    $w->select('person_id')
                                        ->from('aid_projects_persons')
                                        ->where(function ($q) use ($id){
                                            $q->where('project_id' , $id);
                                            $q->whereNotIn('person_id', function ($query) use ($id) {
                                                $query->select('char_vouchers_persons.person_id')
                                                    ->from('char_vouchers_persons')
                                                    ->where(function ($q) use ($id) {
                                                        $q->where('status', 1);
                                                        $q->whereIn('voucher_id', function ($query) use ($id) {
                                                            $query->select('char_vouchers.id')
                                                                ->from('char_vouchers')
                                                                ->where(function ($q_0) use ($id) {
                                                                    $q_0->whereNull('deleted_at');
                                                                    $q_0->where('project_id' , $id);
                                                                });

                                                        });
                                                    });
                                            });
                                        });
                                })
                              ->selectRaw("char_persons.id_card_number")
                              ->groupBy('char_persons.id_card_number')
                              ->get();

                if(sizeof($persons) > 0 || sizeof($organizations) > 0){
                    $token = md5(uniqid());

                    \Excel::create('export_' . $token, function($excel) use($persons,$organizations ) {
                        $excel->setTitle(trans('aid-repository::application.candidates list'));
                        $excel->setDescription(trans('aid-repository::application.candidates list'));
                        if(sizeof($organizations) > 0){
                            $excel->sheet('جمعيات لم تنشئ قسائم', function($sheet) use($organizations) {

                                $sheet->setRightToLeft(true);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setHeight(1,40);
                                $sheet->getDefaultStyle()->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                ]);

                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                $sheet->setCellValue('A1',trans('common::application.organization'));

                                $z= 2;
                                foreach($organizations as $k=>$v){
                                    $sheet->setCellValue('A'.$z,$v->organization_name);
                                    $z++;
                                }

                            });
                        }

                        if(sizeof($persons) > 0){
                            $excel->sheet('اشخاص لم يستلمو قسائم المشروع', function($sheet) use($persons) {

                                $sheet->setRightToLeft(true);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setHeight(1,40);
                                $sheet->getDefaultStyle()->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                ]);

                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                $z= 2;
                                foreach($persons as $k=>$v){
                                    $sheet->setCellValue('A'.$z,$v->id_card_number);
                                    $z++;
                                }

                            });
                        }

                    })->store('xlsx', storage_path('tmp/'));

                    return response()->json(['status'=>false , 'download_token'=>$token, 'msg'=>trans('aid-repository::application.unable update status')]);
                }
            }
        }

        $project->status = $request->input('status');
        $project->date = date('Y-m-d');
        if($project->save()){
            \Log\Model\Log::saveNewLog('PROJECT_UPDATED',trans('aid-repository::application.Edited the project status') . ' "'.$project->name. '" ');
            return response()->json(['status'=>true]);
        }

        return response()->json(['status' => false,'msg'=>trans('organization::application.action failed')]);
    }

    // get mobile number of candidate or organization  of project
    public function getMobilesNumbers(Request $request,$id){
        $projects =  $request->get('projects');
    
        $ProjectOrganization = new ProjectOrganization();
        $ProjectPerson = new ProjectPerson();
        
            if($request->get('org_pers') == 1 || $request->get('org_pers') == '1' ){
                $mobileNumbers = $ProjectOrganization->organizationProjectMobile($id,$projects);
            }else{
                $mobileNumbers = $ProjectPerson->personsProjectMobile($id,$projects);
            }

        if($mobileNumbers){
            $response['mobile_number'] = $mobileNumbers;
            $response['success'] = true;
        }else{
            $response['msg'] = trans('aid-repository::application.No numbers for organizations');
            $response['success'] = false;
        }

        return response()->json($response);
    }

    // destroy project from aid_repository by project_id
    public function destroy(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $this->authorize('delete', $project);
        $repository = Repository::findOrFail($project->repository_id);
        if ($repository->status == Repository::STATUS_CLOSED) {
            return response()->json(['error' => ''], 422);
        }
        $name=$project->name;
        $project->delete();
        \Log\Model\Log::saveNewLog('PROJECT_DELETED',trans('aid-repository::application.cannot be deleted') . ' "'.$name. '" ');
        return response()->json($project);
    }

    // get the candidate on project ro logged user organization
    public function candidates(Request $request, $id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $project = Project::findOrFail($id);
        $user = Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $level = $Org->level;

        $master=true;
        $with_organizations=true;

        if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
            $master=true;
        }

        if ($user->organization_id == $project->organization_id) {
            $with_organizations=true;
        }

        $request->request->add(['project_id'=>$id]);
        $request->request->add(['action'=>$id]);
//        $request->request->project_id = $id;
        $items = ProjectPerson::filterCandidates($request->all(),$level,$with_organizations ,$Org->id);
        return response()->json(['project_owner'=>$with_organizations, 'master'=>$master,'organization_id'=>$user->organization_id, 'data'=>$items]);

    }

    // update status of candidate on project
    public function updateCandidateStatus(Request $request, $id)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $project = Project::findOrFail($id);
        $response = [];
//        $this->authorize('updateCandidatesStatus', $project);
        $status = $request->input('status');
        $user = Auth::user();
        $UserType=$user->type;
        $organization_id = $user->organization_id;
        $UserOrg=$user->organization;
        $level = $UserOrg->level;

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' => trans('aid-repository::application.The status of persons in a closed or approved project cannot be modified') ]);
        }

        $persons = $request->input('persons');
        $reason = $request->input('reason');
        $repository = Repository::findOrFail($project->repository_id);

        $check = false;
        $check_level =(int) $repository->level ;
        if($status ==  ProjectPerson::STATUS_ACCEPTED ) {
            if($repository->level != 1){
                $check = true;
            }
        }

        $quantity = 0;
        $amount = 0;
        $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $organization_id])->first() ;
        if($ProjectOrganization){
            $quantity = $ProjectOrganization->quantity;
            $amount = $ProjectOrganization->amount;
        }

        if($project->custom_quantity){
            if(!is_null($project->custom_quantity) && $project->custom_quantity != 0 && $project->custom_quantity != '0'){
                $project_custom_quantity = $project->custom_quantity;
                if($project_custom_quantity > 0){
                    $custom_org = Organization::where('id', $project->custom_organization)->first();
                    if($custom_org){
                        $original_ration = $custom_org->container_share;
                        $project_custom_ratio = (( 100 * $project->custom_quantity) / $project->quantity );
                        if($project_custom_ratio > $original_ration){
                            $quantity = $project->custom_quantity;
                            $amount = round( $quantity * $project->amount * $project->exchange_rate,1);
                        }
                    }
                }
            }
        }

        if ($organization_id == $project->organization_id) {
            $quantity += $project->organization_quantity;
            $amount += ($project->amount * $quantity);
        }

        if($status ==  ProjectPerson::STATUS_ACCEPTED ) {
            $accepted_person  = ProjectPerson::where(['project_id' => $id, 'status' =>ProjectPerson::STATUS_ACCEPTED])->count();

            $quantity_ = (int) $quantity;
            if($accepted_person == $quantity_){
                return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' =>  trans('aid-repository::application.No new candidates can be accredited because you have the same number of accredited candidates') ]);
            }
        }

        $success = 0;
        $not_update = 0;
        $the_same = 0;
        $limited = 0;
        $passed_to_update = [];
        $return = [];
        foreach($persons as $k=>$v) {
            $row = ProjectPerson::where(['project_id' => $id, 'person_id' => $v['id']])->first();
            if($row->status ==  $status){
                $the_same++;
                $response['return'][] = ['person_id' =>(int) $v['id'],'reason'=>'the_same'];
            }
            else{
                $passed_to_update[]=(int)$v['id'];
            }
        }

        if(sizeof($passed_to_update) > 0){

            if($check){
                $range = [3,4,5,6,7,8];
                if(in_array($check_level,$range)){
                    $Branches = aidsLocation::getBranchesCountProject($quantity,$amount,$id,$organization_id,$check_level);
                    $done =[];
                    foreach ($Branches as $k => $v) {
                        if ($v->max > 0) {

                            $loc_persons = \DB::table('char_persons')
                                ->where(function($subQuery) use ($v,$passed_to_update,$check_level) {
                                    $subQuery->whereIn('char_persons.id', $passed_to_update);
                                    if($check_level == 3){
                                        $subQuery->where('char_persons.adsdistrict_id', $v->id);
                                    }elseif ($check_level == 4){
                                        $subQuery->where('char_persons.adsregion_id', $v->id);
                                    }elseif ($check_level == 5){
                                        $subQuery->where('char_persons.adsneighborhood_id', $v->id);
                                    }elseif ($check_level == 6){
                                        $subQuery->where('char_persons.adssquare_id', $v->id);
                                    }else{
                                        $subQuery->where('char_persons.mosques_id', $v->id);
                                    }
                                })->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.id_card_number")
                                ->groupBy('char_persons.id')
                                ->limit($v->max)
                                ->get();

                            foreach ($loc_persons as $key=>$value){
                                $updated=['status' => $status,'reason'=>null];
                                ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $value->id])->update($updated);
                                $done[]=$value->id;
                                $success++;
                            }
                        }
                    }

                    $person_diff = array_diff($passed_to_update, $done);
                    if (sizeof($person_diff) > 0) {
                        $person_ = \DB::table('char_persons')
                            ->wherein('char_persons.id', $person_diff)
                            ->selectRaw("char_persons.id,CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                                     ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                                                     char_persons.id_card_number")
                            ->groupBy('char_persons.id')
                            ->get();

                        foreach ($person_ as $key => $value) {
                            $result[] = ['name' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
//                           $done[] = $value->id;
                            $limited++;
                        }
                    }

                }
                elseif($check_level == 2){
                    $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $organization_id])->first() ;

                    if ($request->upType){
                        $accepted = ProjectPerson::where(function ($q) use ($id,$UserType,$user,$organization_id,$level) {
                                                        $q->where('project_id', $id);
                                                        $q->where('status', ProjectPerson::STATUS_ACCEPTED);

                                                        if($UserType == 2) {
                                                            $q->where(function ($anq) use ($organization_id,$user) {
                                                                $anq->where('aid_projects_persons.organization_id',$organization_id);
                                                                $anq->orwherein('aid_projects_persons.organization_id', function($quer) use($user) {
                                                                    $quer->select('organization_id')
                                                                        ->from('char_user_organizations')
                                                                        ->where('user_id', '=', $user->id);
                                                                });
                                                            });
                                                        }else{

                                                            if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
                                                                $q->where(function ($anq) use ($organization_id) {
                                                                    $anq->wherein('aid_projects_persons.organization_id', function($decq) use($organization_id) {
                                                                        $decq->select('descendant_id')
                                                                            ->from('char_organizations_closure')
                                                                            ->where('ancestor_id', '=', $organization_id);
                                                                    });
                                                                });
                                                            }else{
                                                                $q->where('aid_projects_persons.organization_id', $organization_id);
                                                            }
                                                        }
                                                  })
                                                  ->count() ;
                    }else{
                        $accepted = ProjectPerson::where(['project_id' =>$id ,
                            'status' =>ProjectPerson::STATUS_ACCEPTED  ,
                            'organization_id' => $organization_id])->count() ;
                    }


                    if($ProjectOrganization){
                        $limited = $ProjectOrganization->quantity;
                    }

                    if ($organization_id == $project->organization_id) {
                        $limited += $project->organization_quantity;
                    }

                    if($accepted == $limited){
                        return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' =>  trans('aid-repository::application.No new candidates can be accredited because you have the same number of accredited candidates')  ]);
                    }

                    foreach($passed_to_update as $per) {
                        $updated=['status' => $status,'reason'=>null];
                        if($limited > $accepted) {
                            ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $per])->update($updated);
                            $success++;
                            $accepted++;
                        }else{
                            $response['return'][] = ['person_id' =>$per,'reason'=>'out_of_limit'];
                            $not_update++;
                        }
                    }

                }
            }else{
                foreach($passed_to_update as $per) {
                    $updated=['status' => $status,'reason'=>null];
                    if($status == ProjectPerson::STATUS_REJECT){
                        $updated['reason'] = $reason;
                    }
                    ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $per])->update($updated);
                    $success++;
                }
            }
        }

        $status_name= ProjectPerson::status($status);
        if(sizeof($persons) == $success){
            \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_UPDATED',trans('aid-repository::application.Modified the status of project recipients to'). ' "'.$status_name. '" ');
            return response()->json(['status' => 'success' ,'msg' =>trans('aid-repository::application.The status of all people has been successfully modified')]);
        }

        if(sizeof($persons) == $the_same){
            return response()->json(['status' => 'error' ,'msg' => trans('aid-repository::application.Not all people have their status modified because they have the same status') ]);
        }

        if(sizeof($persons) == ($not_update + $the_same)){
            return response()->json(['status' => 'error' ,'msg' => trans('aid-repository::application.The status of all people has not been modified because you have reached the maximum number of approved people in this project or have the same status')]);
        }

        if (sizeof($persons) == $limited && $success == 0) {
            return response()->json(['status' => 'error' ,'msg' => trans('aid::application.The selected cases are limited')]);
        }

        \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_UPDATED', trans('aid-repository::application.Modified the status of project recipients to') . ' "'.$status_name. '" ');

        if($success > 0){
            $response['status']  = 'success';
        }
        else{
            $response['status']  = 'error';
        }

        $response['msg']  = trans('aid-repository::application.The status of some people has been successfully modified, as the total number of people') .' :  '. sizeof($persons) . ' ,  '.
            trans('aid-repository::application.The number of names whose status has been modified').' :  ' .$success . ' ,  ' .
            trans('aid-repository::application.Number of names with the same status') .' :  ' .$the_same . ' ,  ' .
            trans('aid::application.limited') . ' : ' . $limited . ' , ' .
            trans('aid-repository::application.Number of people whose status has not been modified') .' :  ' .$not_update ;

        return response()->json($response);

    }

    public function updateCandidateStatus_(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $id = $request->project_id;
        $project = Project::findOrFail($id);

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' =>  trans('aid-repository::application.he status of persons in a closed or approved project cannot be modified') ]);
        }

        $repository = Repository::findOrFail($project->repository_id);
        $user = Auth::user();
        $organization_id = $user->organization_id;
        $status = $request->input('status');

        $quantity = 0;
        $amount = 0;

        if($status ==  ProjectPerson::STATUS_ACCEPTED ){
            $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $organization_id])->first() ;
            $accepted = ProjectPerson::where(['project_id'=>$id,'status'=>ProjectPerson::STATUS_ACCEPTED ,'organization_id'=>$organization_id])->count() ;

            if($ProjectOrganization){
                $quantity = $ProjectOrganization->quantity;
                $amount = $ProjectOrganization->amount;
            }

            if ($organization_id == $project->organization_id) {
                $quantity += $project->organization_quantity;
                $amount = $ProjectOrganization->amount;
            }

            if($accepted == $quantity){
                return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' =>  trans('aid-repository::application.No new candidates can be accredited because you have the same number of accredited candidates') ]);
            }
        }

        $importFile=$request->file ;

        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=\Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;

                        $constraint =  [ "rkm_alhoy" , "alsbb"];
                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $total_ = 0;

                            if ($total > 0) {

                                $valid_persons = [];
                                $invalid_cards = 0;
                                $un_register_to_your_organization = 0;
                                $un_register = 0;
                                $un_defined_reason = 0;

                                $duplicated =0;
                                $processed=[];
                                $invalid=[];

                                $success = 0;
                                $the_same = 0;
                                $limited = 0;
                                $not_update = 0;
                                $response = array();
                                $download_token = null;

                                foreach ($records as $key =>$value) {
                                    if(!is_null($value['rkm_alhoy'])){
                                        if(in_array($value['rkm_alhoy'],$processed)){
                                            $duplicated++;
                                        }
                                        else{
                                            if(strlen($value['rkm_alhoy'])  <= 9) {
                                                $card =(int)$value['rkm_alhoy'];
                                                $hasRaw = Person::HasRaw($card);
                                                if(is_null($hasRaw)) {
                                                    $invalid[]=['card' => $value['rkm_alhoy'] , 'reason' =>'un_register'];
                                                    $un_register++;
                                                }else{
                                                    $candidates_=ProjectPerson::findCandidates($id,$hasRaw ,$organization_id);
                                                    if($candidates_){
                                                        if($candidates_->status == $status){
                                                            $the_same++;
                                                            $invalid[]=['card' => $value['rkm_alhoy'] , 'reason' =>'the_same'];
                                                        }else{
                                                            if ($status == 3) {
                                                                $valid_persons[]=$hasRaw;
                                                            }
                                                            else{
                                                                $update_ = ['status'=>$status , 'reason'=>null];
                                                                $updated = true;

                                                                if($status = 4){
                                                                    $alsbb =$value['alsbb'];
                                                                    if(!is_null($alsbb) && $alsbb !=''&& $alsbb !=' ' && ! empty($alsbb)){
                                                                        $update_['reason'] = $alsbb;
                                                                    }else{
                                                                        $updated = false;
                                                                    }
                                                                }

                                                                if($updated){
                                                                    ProjectPerson::where(['person_id' => $hasRaw,
                                                                        'organization_id' => $organization_id,
                                                                        'project_id' => $id])
                                                                        ->update($update_);

                                                                    $processed[]=$value['rkm_alhoy'];
                                                                    $success++;
                                                                }else{
                                                                    $invalid[]=['card' => $value['rkm_alhoy'] , 'reason' =>'un_defined_reason'];
                                                                    $un_defined_reason++;
                                                                }

                                                            }
                                                        }
                                                    }else{
                                                        $invalid[]=['card' => $value['rkm_alhoy'] , 'reason' =>'un_register_to_your_organization'];
                                                        $un_register_to_your_organization++;
                                                    }
                                                }
                                            }else{
                                                $invalid[]=['card' => $value['rkm_alhoy'] , 'reason' =>'invalid_card'];
                                                $invalid_cards++;
                                            }
                                        }
                                        $total_++;
                                    }

                                }

                                if($status == 3 && sizeof($valid_persons) > 0){
                                    $check = false;
                                    $check_level =(int) $repository->level ;
                                    if($status ==  ProjectPerson::STATUS_ACCEPTED ) {
                                        if($repository->level != 1){
                                            $check = true;
                                        }
                                    }

                                    if($check){
                                        $range = [3,4,5,6,7,8];
                                        if(in_array($check_level,$range)){

                                            $Branches = aidsLocation::getBranchesCountProject($quantity,$amount,$id,$organization_id,$check_level);
                                            $done =[];
                                            foreach ($Branches as $k => $v) {
                                                if ($v->max > 0) {

                                                    $loc_persons = \DB::table('char_persons')
                                                        ->where(function($subQuery) use ($v,$valid_persons,$check_level) {
                                                            $subQuery->whereIn('char_persons.id', $valid_persons);
                                                            if($check_level == 3){
                                                                $subQuery->where('char_persons.adsdistrict_id', $v->id);
                                                            }elseif ($check_level == 4){
                                                                $subQuery->where('char_persons.adsregion_id', $v->id);
                                                            }elseif ($check_level == 5){
                                                                $subQuery->where('char_persons.adsneighborhood_id', $v->id);
                                                            }elseif ($check_level == 6){
                                                                $subQuery->where('char_persons.adssquare_id', $v->id);
                                                            }else{
                                                                $subQuery->where('char_persons.mosques_id', $v->id);
                                                            }
                                                        })->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.id_card_number")
                                                        ->groupBy('char_persons.id')
                                                        ->limit($v->max)
                                                        ->get();

                                                    foreach ($loc_persons as $key=>$value){
                                                        $updated=['status' => $status,'reason'=>null];
                                                        ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $value->id])->update($updated);
                                                        $done[]=$value->id;
                                                        $success++;
                                                    }
                                                }
                                            }

                                            $person_diff = array_diff($valid_persons, $done);
                                            if (sizeof($person_diff) > 0) {
                                                $person_ = \DB::table('char_persons')
                                                    ->wherein('char_persons.id', $person_diff)
                                                    ->selectRaw("char_persons.id,CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                                     ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                                                     char_persons.id_card_number")
                                                    ->groupBy('char_persons.id')
                                                    ->get();

                                                foreach ($person_ as $key => $value) {
                                                    $result[] = ['name' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
//                           $done[] = $value->id;
                                                    $limited++;
                                                }
                                            }

                                        }
                                        elseif($check_level == 2){
                                            $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $organization_id])->first() ;
                                            $accepted = ProjectPerson::where(['project_id' =>$id ,
                                                'status' =>ProjectPerson::STATUS_ACCEPTED  ,
                                                'organization_id' => $organization_id])->count() ;

                                            if($ProjectOrganization){
                                                $limited = $ProjectOrganization->quantity;
                                            }
                                            if ($organization_id == $project->organization_id) {
                                                $limited += $project->organization_quantity;
                                            }

                                            if($accepted == $limited){
                                                return response()->json(['status' => 'error' , 'error_type' =>' ' , 'msg' =>  trans('aid-repository::application.No new candidates can be accredited because you have the same number of accredited candidates')  ]);
                                            }

                                            foreach($valid_persons as $per) {
                                                $updated=['status' => $status,'reason'=>null];
                                                if($limited > $accepted) {
                                                    ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $per])->update($updated);
                                                    $success++;
                                                    $accepted++;
                                                }else{
                                                    $response['return'][] = ['person_id' => $per,'reason'=>'out_of_limit'];
                                                    $not_update++;
                                                }
                                            }

                                        }
                                    }
                                    else{
                                        foreach($valid_persons as $per) {
                                            ProjectPerson::query()->where(['project_id' => $id, 'person_id' => $per])->update(['status' => $status,'reason'=>null]);
                                            $success++;
                                        }


                                    }
                                }

                                if(sizeof($invalid) > 0 ){
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($invalid ) {
                                        $excel->setTitle(trans('aid-repository::application.candidates list'));
                                        $excel->setDescription(trans('aid-repository::application.candidates list'));
                                        if(sizeof($invalid) > 0){
                                            $excel->sheet(trans('aid-repository::application.invalid_cards'), function($sheet) use($invalid) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('B1',trans('aid-repository::application.reason_'));

                                                $z= 2;
                                                foreach($invalid as $k=>$v){
                                                    $sheet->setCellValue('A'.$z,$v['card']);
                                                    $sheet->setCellValue('B'.$z,$translate= trans('aid-repository::application.'.$v['reason']));

                                                    $z++;
                                                }

                                            });
                                        }


                                    })->store('xlsx', storage_path('tmp/'));
                                    $response['download_token']  = $token;
                                }

                                if($total_ == $the_same){
                                    return response()->json(['status' => 'failed' ,'msg' => trans('aid-repository::application.Not all people have their status modified because they have the same status') ]);
                                }

                                if($total_ == $un_defined_reason){
                                    return response()->json(['status' => 'failed' ,'msg' => trans('aid-repository::application.un_defined_reason_for_all_cases') ]);
                                }

                                if ($total_ == $limited && $success == 0) {
                                    return response()->json(['status' => 'failed' ,'msg' => trans('aid::application.The selected cases are limited')]);
                                }

                                if ($total_ == $limited && $success == 0) {
                                    return response()->json(['status' => 'failed' ,'msg' => trans('aid::application.The selected cases are limited')]);
                                }

                                if($total_ == $un_register_to_your_organization || $total_ == $un_register){
                                    return response()->json(['status' => 'failed' ,
                                                             'error_type' => ' ' ,
                                                             'msg' =>trans('aid-repository::application.All names entered are incorrect or not registered in the system')]);
                                }

                                $response['status']  = 'failed';
                                if($success > 0 ){
                                    $response['status']  = 'success';
                                }

                                $response['msg'] = '';

                                if(sizeof($invalid) > 0){
                                    $response['msg']  .= trans('aid-repository::application.Downloading the results file,');
                                }

                                $response['msg']  .= trans('aid-repository::application.The status of some people has been successfully modified, as the total number of people') .' :  '. $total_ . ' ,  '.
                                    trans('aid-repository::application.The number of names whose status has been modified').' :  ' .$success . ' ,  ' .
                                    trans('aid-repository::application.Number of names with the same status') .' :  ' .$the_same . ' ,  ' .
                                    trans('aid-repository::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .($un_register + $un_register_to_your_organization) . ' ,  ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid-repository::application.Number of people whose status has not been modified') .' :  ' .$not_update ;

                                if(sizeof($invalid) > 0){
                                    $response['msg']  .= ' ,  ' .trans('aid-repository::application.Downloading file whose status has not been modified');
                                }

                                return response()->json($response);

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // delete the candidate from project
    public function deleteCandidate(Request $request, $id, $person_id)
    {
        $project = Project::findOrFail($id);
        $this->authorize('deleteCandidates', $project);

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['error' => trans('aid-repository::application.You cannot delete people from a closed or approved project for execution')], 422);
        }

        $user = Auth::user();
        $person = ProjectPerson::allWithOptions(array('project_id' => $id, 'person_id' => $person_id,'with_person_name'=>true));
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        if($master || ($person[0]->nominated_organization_id == $user->organization_id)){
            $result = ProjectPerson::removePerson($id, $person_id);
            $name=$person[0]->first_name.' '.$person[0]->second_name.' '.$person[0]->third_name.' '.$person[0]->last_name;
            \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_DELETED','Deleted project filter' . ' "'.$name. '" ');
            return response()->json(array('result' => $result));
        }

            return response()->json(['error' => trans('aid-repository::application.The candidate must be the person to be deleted from the list of candidates')], 422);

    }

    // empty project from old candidate
    public function rest_candidates(Request $request, $id)
    {
        $project = Project::findOrFail($id);
        $this->authorize('deleteCandidates', $project);

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['error' => trans('aid-repository::application.You cannot delete people from a closed or approved project for execution')], 422);
        }

        if( ProjectPerson::where(['project_id'=> $id])->count() > 0 ){
            if( ProjectPerson::where(['project_id'=> $id])->delete()) {
                \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_DELETED',trans('aid-repository::application.empties the project from the candidates') . ' "'.$project->name. '" ');
                return response()->json(['status' =>'success' ,'msg'=> trans('aid-repository::application.The candidates were successfully emptied from the project')]);
            }
            return response()->json(['status' =>'error' ,'msg' => trans('aid-repository::application.occurred during the operation')]);
        }

        return response()->json(['status' =>'error' ,'msg' => trans('aid-repository::application.There are no candidates saved within the project to empty')]);

    }

    // return organizationShare of logged user organization on the project
    public function organizationShare(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['error' => trans('aid-repository::application.cant update on the ratios of associations for a closed or approved project for implementation')], 422);
        }

        $organizations = $request->input('organizations');
        $project->organization_ratio = $request->input('organization_ratio');
        $project->notes = $request->input('notes');
        $project->adjust = $request->input('adjust');
        if (ProjectOrganization::saveForProject($project, $organizations)) {



            \Log\Model\Log::saveNewLog('PROJECT_ORGANIZATION_UPDATED',trans('aid-repository::application.modified the proportions of the associations for a project')  . ' "'.$project->name. '" ');

            return response()->json([
                'success' => 1,
            ]);
        }
        
        return response()->json([
            'error' => 1,
        ], 422);
    }

    // get organization of logged user organization on this project
    public function listForOrganization(Request $request)
    {

        $user = Auth::user();
        $Org=$user->organization;

        $response = ['master' => false ,'user_id' =>$user->id,'user_iorganization_idd' => $user->organization_id] ;
        $options =
                  [ 'with_project_name' => true, 'with_repository_name' => true,
                    'with_category_name' => true, 'with_sponsor_name' => true,
                    'with_organization_name' => true,
                    'master' => $response['master'], 'parent_id' => null,
                    'container_share' => 0, 'organization_id' => $user->organization_id,
                    'project_status' => [Project::STATUS_CANDIDATES, Project::STATUS_CLOSED, Project::STATUS_EXECUTION]
                  ];

        $level = $Org->level;
//        || $level == Organization::LEVEL_BRANCH_CENTER
        if ($level == Organization::LEVEL_MASTER_CENTER ) {
            $response['master']=true;
            $options['master']=true;
        }else{
            $options['parent_id'] = $Org->parent_id;
            $options['container_share'] = $Org->container_share;
        }

        $query =  Project::listOrganizationProjects($options,$user,$request->all());

        $query->map(function ($value) use ($user){
            $value->ben_count = ProjectPerson::where(['status'=> 3, 'project_id'=> $value->project_id,
                                                      'organization_id'=>$user->organization_id])->count();

            $value->add_voucher = false ;
            if ($value->repository_status != Repository::STATUS_CLOSED) {
                if($value->status != Project::STATUS_CLOSED){
                    if($value->ben_count == $value->quantity){
                        $founded = Vouchers::where(['project_id' =>$value->project_id ,
                            'organization_id'=> $user->organization_id])->count();
                        if($founded == 0 ){
                            $value->add_voucher = true ;
                        }
                    }
                }
            }

            return $value;
        });

        $response['entries'] = $query;

        return response()->json($response);
    }

    // return candidate of logged user organization on this project
    public function organizationCandidates(Request $request,$id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $project = Project::findOrFail($id);
//        $this->authorize('candidates', $project);
        $user = Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $level = $Org->level;

        $master=true;
        $with_organizations=true;

         if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
            $master=true;
        }

        if ($user->organization_id == $project->organization_id) {
            $with_organizations=true;
        }

        $items = ProjectPerson::filterCandidates($request->all(),$level,$with_organizations,$Org->id);
        return response()->json(['project_owner'=>$with_organizations, 'master'=>$master,'organization_id'=>$user->organization_id, 'data'=>$items]);

    }

    // paginate candidate of logged user organization on project
    public function filter(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $id = $request->project_id;
        $project = Project::with('Repository')->findOrFail($id);
        $owned_org = $project->Repository->organization_id;
        $level = $project->Repository->level;
//        $this->authorize('candidates', $project);
        $user = Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $level = $Org->level;

        $master=true;
        $with_organizations=false;

        if ($level == Organization::LEVEL_MASTER_CENTER || $level == Organization::LEVEL_BRANCH_CENTER) {
            $master=true;
        }
        $organizations_quantity = 0;

        if ($user->organization_id == $project->organization_id) {
            $with_organizations=true;
        }

        $items = ProjectPerson::filterCandidates($request->all(),$level,$with_organizations,$Org->id);

        if($request->action =='excel'){
            if(sizeof($items) > 0){
                $token = md5(uniqid());
                \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items)  {
                    $excel->setTitle(trans('aid-repository::application.candidates'));
                    $excel->setDescription(trans('aid-repository::application.candidates'));
                    $excel->sheet(trans('aid-repository::application.candidates'),function($sheet)use($items)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->setOrientation('landscape');

                        $sheet->setWidth([
                                  'A'=>5   ,'B'=> 35 ,'C'=> 20 ,'D'=> 35,
                                  'E'=> 35 ,'F'=> 20 ,'G'=> 20 ,'H'=> 40 ,'I'=> 15 ,'J'=> 15,
                                  'K'=> 15 ,'L'=> 15 ,'M'=> 25 ,'N'=> 25 ,'O'=> 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,'T'=> 25,'U'=> 25
                            ]);

                        $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U'];

                        foreach($map2 as $key ) {
                            $sheet->cells($key .'1', function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                        }

                        $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                        $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                        $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                        $sheet->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                        $sheet->setCellValue('E1',trans('aid-repository::application.nomination'));
                        $sheet->setCellValue('F1',trans('aid-repository::application.Organization_Name'));
                        $sheet->setCellValue('G1',trans('aid-repository::application.status'));
                        $sheet->setCellValue('H1',trans('aid-repository::application.reason'));
                        $sheet->setCellValue('I1',trans('common::application.gender'));
                        $sheet->setCellValue('J1',trans('common::application.marital_status'));
                        $sheet->setCellValue('K1',trans('common::application.birthday'));
                        $sheet->setCellValue('L1',trans('common::application.country'));
                        $sheet->setCellValue('M1',trans('common::application.district'));
                        $sheet->setCellValue('N1',trans('common::application.region_'));
                        $sheet->setCellValue('O1',trans('common::application.nearlocation_'));
                        $sheet->setCellValue('P1',trans('common::application.square'));
                        $sheet->setCellValue('Q1',trans('common::application.mosques'));
                        $sheet->setCellValue('R1',trans('common::application.street_address_'));
                        $sheet->setCellValue('S1',trans('common::application.mobile'));
                        $sheet->setCellValue('T1',trans('common::application.phone'));
                        $sheet->setCellValue('U1',trans('aid-repository::application.family_count'));

                        $z= 2;
                        foreach($items as $k=>$v ) {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                            $sheet->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                            $sheet->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                            $sheet->setCellValue('F'.$z,(is_null($v->nomination_name) || $v->nomination_name == ' ' ) ? '-' : $v->nomination_name);
                            $sheet->setCellValue('G'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                            $sheet->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);
                            $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $v->gender);
                            $sheet->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                            $sheet->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                            $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                            $sheet->setCellValue('M'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                            $sheet->setCellValue('N'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                            $sheet->setCellValue('O'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                            $sheet->setCellValue('P'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                            $sheet->setCellValue('Q'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                            $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                            $sheet->setCellValue('S'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                            $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                            $sheet->setCellValue('U'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);
                            $z++;
                        }
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true , 'download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }

        $src = $request->src;
        
        if(!$request->central){
        
//        if($src == 'main'){
            //        $organizations_quantity = ;
            if ($user->organization_id == $owned_org) {
                $statistic = ['quantity' => $project->quantity  ,
                               'required' => $project->quantity  ,
                               'all' => ProjectPerson::where(['project_id' =>$id])->count()  ,
                               'candidate' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 1)  ,
                               'sent' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 2)  ,
                               'accept' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 3)  ,
                               'rejected' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 4)];
            }
            else{

                $organization_quantity = 0;
                $extra_quantity = 0;

                $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $user->organization_id])->first() ;
                if($ProjectOrganization){
                    $organization_quantity = $ProjectOrganization->quantity;
                }
                if ($user->organization_id == $project->organization_id) {
                    $extra_quantity = $project->organization_quantity;
                }

                $accept = ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 3) ;
                $statistic = [
                    'quantity_' => $ProjectOrganization  ,
                    'quantity' => $organization_quantity  ,
                    'extra_quantity' =>$extra_quantity  ,
                    'required' =>$organization_quantity + $extra_quantity  ,
                    'all' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , null)  ,
                    'candidate' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 1)  ,
                    'sent' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 2)  ,
                    'accept' => $accept  ,
                    'rejected' => ProjectPerson::filterCandidatesStatistic($request->all(),$level,$Org->id , 4)];

                $repo_level = $project->Repository->level ;
                if( ( $repo_level == 1 || $repo_level == '1')){
                    $statistic['quantity']= $accept + $extra_quantity;
                    $statistic['required']= $accept + $extra_quantity;
                }
            }    
        }
        else{
              $quantity = 0;
              $required = 0;
              $all = 0;
              $extra_quantity = 0;              
              
              if($level == 1){
                  $quantity =  ProjectPerson::StatisticCentralCount($id , $user , null ,$level) ; 
                  $required =  ProjectPerson::StatisticCentralCount($id , $user , null ,$level) ; 
                  $all =  ProjectPerson::StatisticCentralCount($id , $user , null ,$level) ; 
              }
              else{


                    $ProjectOrganization = ProjectOrganization::where(['project_id' =>$id , 'organization_id' => $user->organization_id])->first() ;
                    if($ProjectOrganization){
                        $quantity = $ProjectOrganization->quantity;
                    }
                    if ($user->organization_id == $project->organization_id) {
                        $extra_quantity = $project->organization_quantity;
                    }
                
//                  $quantity =  ProjectPerson::StatisticCentralCount($id , $user , null ,$level) ; 
//                  $required =  $quantity ; 
                  $all =   $quantity ; 
                  
              }
              
              $statistic = ['quantity' => $quantity  ,
                            'all' =>  $all ,
                            'extra_quantity' =>$extra_quantity  ,
                            'required' =>$quantity + $extra_quantity  ,
                            'candidate' =>  ProjectPerson::StatisticCentralCount($id , $user , 1 ,$level)  ,
                            'sent' =>  ProjectPerson::StatisticCentralCount($id , $user , 2 ,$level)  ,
                            'accept' =>  ProjectPerson::StatisticCentralCount($id , $user , 3 ,$level)  ,
                            'rejected' =>  ProjectPerson::StatisticCentralCount($id , $user , 4 ,$level)  ];
            
        }

        return response()->json(['statistic'=>$statistic, 'level'=>$level,
                                 'project_owner'=>$with_organizations, 'project'=>$project,
                                 'organization_id'=>$user->organization_id , 'master'=>$master,
                                 'data'=>$items]);

    }
    
// paginate related of candidate to logged user organization on project
    public function related(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $id = $request->project_id;
        $project = Project::with('Repository')->findOrFail($id);
        $owned_org = $project->Repository->organization_id;
        $level = $project->Repository->level;
//        $this->authorize('candidates', $project);
        $user = Auth::user();

        $items = ProjectPerson::filterRelatedOfCandidates($request->all());
        
        $excel_title = '';
//        trans('aid-repository::application.candidates')
        if($request->action == 'wives'){
           $excel_title = trans('aid-repository::application.candidates') .' - ' .trans('common::application.Wives');
           $excel_title0 = 'Wives';
        }else{
           $excel_title = trans('aid-repository::application.candidates') .' - ' .trans('common::application.childs');
            $excel_title0 = 'child';
        }

        $dirName = base_path('storage/xlsx');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }


//        if($request->action =='excel'){
            if(sizeof($items) > 0){
                $dirName = base_path('storage/xlsx');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }else{
                      array_map('unlink', glob("$dirName/*.*"));
                }

                \Maatwebsite\Excel\Facades\Excel::create('candidates', function($excel) use($items,$excel_title,$request)  {
                                    $excel->setTitle($excel_title);
                                    $excel->setDescription($excel_title);
                                    $excel->sheet(trans('aid-repository::application.candidates'),function($sheet)use($items,$request)   {
                                        $sheet->setRightToLeft(true);
                                        $sheet->setAllBorders('thin');
                                        $sheet->setfitToWidth(true);
                                        $sheet->setHeight(1,40);
                                        $sheet->getDefaultStyle()->applyFromArray([
                                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                        ]);
                                        $sheet->setOrientation('landscape');

                                        $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                                            'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                                            'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                                            'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                                            'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                                        ]);

                                        $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                                            'V','W','X','Y','Z','AA','AB','AC'];

                                        foreach($map2 as $key ) {
                                            $sheet->cells($key .'1', function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });
                                        }

                                        $sheet->setCellValue('A1',trans('common::application.#'));
                                        $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                                        $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                        $sheet->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                                        $sheet->setCellValue('E1',trans('aid-repository::application.nomination'));
                                        $sheet->setCellValue('F1',trans('aid-repository::application.Organization_Name'));
                                        $sheet->setCellValue('G1',trans('aid-repository::application.status'));
                                        $sheet->setCellValue('H1',trans('aid-repository::application.reason'));
                                        $sheet->setCellValue('I1',trans('common::application.gender'));
                                        $sheet->setCellValue('J1',trans('common::application.marital_status'));
                                        $sheet->setCellValue('K1',trans('common::application.birthday'));
                                        //     $sheet->setCellValue('L1',trans('common::application.country'));
                                        $sheet->setCellValue('L1',trans('common::application.district'));
                                        $sheet->setCellValue('M1',trans('common::application.region_'));
                                        $sheet->setCellValue('N1',trans('common::application.nearlocation_'));
                                        $sheet->setCellValue('O1',trans('common::application.square'));
                                        $sheet->setCellValue('P1',trans('common::application.mosques'));
                                        //    $sheet->setCellValue('Q1',trans('common::application.street_address_'));
                                        $sheet->setCellValue('Q1',trans('common::application.mobile'));
                                        //  $sheet->setCellValue('T1',trans('common::application.phone'));
                                        $sheet->setCellValue('R1',trans('aid-repository::application.family_count'));


                                        $male = trans('common::application.male');
                                        $female = trans('common::application.female');
                                        $z= 2;
                                        foreach($items as $k=>$v ) {
                                            $sheet->setCellValue('A'.$z,$k+1);
                                            $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                                            $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                            $sheet->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                                            $sheet->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                                            $sheet->setCellValue('F'.$z,(is_null($v->nomination_name) || $v->nomination_name == ' ' ) ? '-' : $v->nomination_name);
                                            $sheet->setCellValue('G'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                            $sheet->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);

                                            if($v->gender = 2){
                                                $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $female);
                                            }else if ($v->gender = 1){
                                                $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $male);
                                            }

                                            $sheet->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                                            $sheet->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                                            // $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                                            $sheet->setCellValue('L'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                                            $sheet->setCellValue('M'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                                            $sheet->setCellValue('N'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                                            $sheet->setCellValue('O'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                                            $sheet->setCellValue('P'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                                            //   $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                                            $sheet->setCellValue('Q'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                            //  $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                            $sheet->setCellValue('R'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);

                                            $z++;
                                        }
                                    });
                                })
                                    ->store('xlsx', storage_path('xlsx/'));

                \Maatwebsite\Excel\Facades\Excel::create($excel_title0,  function($excel) use($items,$excel_title,$request)  {
                    $excel->setTitle($excel_title);
                    $excel->setDescription($excel_title);
                    $excel->sheet($excel_title,function($sheet)use($items,$request)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->setOrientation('landscape');

                        $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                            'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                            'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                            'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                            'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                        ]);

                        $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                            'V','W','X','Y','Z','AA','AB','AC'];

                        foreach($map2 as $key ) {
                            $sheet->cells($key .'1', function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                        }

                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('aid-repository::application.id_card_number'));
                        $sheet->setCellValue('C1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('D1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('E1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('F1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('G1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('H1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                        $sheet->setCellValue('I1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                        // $sheet->setCellValue('AC1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                        $male = trans('common::application.male');
                        $female = trans('common::application.female');
                        $z= 2;
                        foreach($items as $k=>$v ) {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                            $sheet->setCellValue('C'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);

                            $kinship_name = ' ';
                            if($request->action == 'wives'){
                                if($v->related_gender = 2){
                                    $kinship_name = trans('common::application.wife_');
                                }else if ($v->related_gender = 1){
                                    $kinship_name = trans('common::application.husband');
                                }
                            }else{
                                if($v->related_gender = 2){
                                    $kinship_name = trans('common::application.daughter_');
                                }else if ($v->related_gender = 1){
                                    $kinship_name = trans('common::application.son_');
                                }
                            }

                            $sheet->setCellValue('D'.$z,$kinship_name);
                            $sheet->setCellValue('E'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);

                            if($v->related_gender = 2){
                                $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $female);
                            }else if ($v->related_gender = 1){
                                $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $male);
                            }

                            $sheet->setCellValue('G'.$z,(is_null($v->related_marital_status) || $v->related_marital_status == ' ' ) ? '-' : $v->related_marital_status);
                            $sheet->setCellValue('H'.$z,(is_null($v->related_birthday) || $v->related_birthday == ' ' ) ? '-' : $v->related_birthday);
                            $sheet->setCellValue('I'.$z,(is_null($v->related_death_date) || $v->related_death_date == ' ' ) ? '-' : $v->related_death_date);
                            //  $sheet->setCellValue('AC'.$z,(is_null($v->related_prev_family_name) || $v->related_prev_family_name == ' ' ) ? '-' : $v->related_prev_family_name);

                            $z++;
                        }
                    });
                })
                    ->store('xlsx', storage_path('xlsx/'));

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;
                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    foreach ( glob( $dirName . '/*' ) as $fileName )
                    {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }

                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                }

                return response()->json(['status' => true , 'download_token' => $token]);
            }

            return response()->json(['status' => false]);

    }

    public function related0(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $id = $request->project_id;
        $project = Project::with('Repository')->findOrFail($id);
        $owned_org = $project->Repository->organization_id;
        $level = $project->Repository->level;
//        $this->authorize('candidates', $project);
        $user = Auth::user();

        $items = ProjectPerson::filterRelatedOfCandidates($request->all());

        $excel_title = '';
//        trans('aid-repository::application.candidates')
        if($request->action == 'wives'){
            $excel_title = trans('aid-repository::application.candidates') .' - ' .trans('common::application.Wives');
        }else{
            $excel_title = trans('aid-repository::application.candidates') .' - ' .trans('common::application.childs');
        }

//        if($request->action =='excel'){
        if(sizeof($items) > 0){
            $dirName = base_path('storage/xlsx');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items,$excel_title,$request)  {
                $excel->setTitle($excel_title);
                $excel->setDescription($excel_title);
                $excel->sheet(trans('aid-repository::application.candidates'),function($sheet)use($items,$request)   {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->setOrientation('landscape');

                    $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                        'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                        'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                        'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                        'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                    ]);

                    $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                        'V','W','X','Y','Z','AA','AB','AC'];

                    foreach($map2 as $key ) {
                        $sheet->cells($key .'1', function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                    }

                    $sheet->setCellValue('A1',trans('common::application.#'));
                    $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                    $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                    $sheet->setCellValue('D1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('E1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('F1',trans('aid-repository::application.candidate_organization_name'));
                    $sheet->setCellValue('G1',trans('aid-repository::application.nomination'));
                    $sheet->setCellValue('H1',trans('aid-repository::application.Organization_Name'));
                    $sheet->setCellValue('I1',trans('aid-repository::application.status'));
                    $sheet->setCellValue('J1',trans('aid-repository::application.reason'));
                    $sheet->setCellValue('K1',trans('common::application.gender'));
                    $sheet->setCellValue('L1',trans('common::application.marital_status'));
                    $sheet->setCellValue('M1',trans('common::application.birthday'));
                    //     $sheet->setCellValue('L1',trans('common::application.country'));
                    $sheet->setCellValue('N1',trans('common::application.district'));
                    $sheet->setCellValue('O1',trans('common::application.region_'));
                    $sheet->setCellValue('P1',trans('common::application.nearlocation_'));
                    $sheet->setCellValue('Q1',trans('common::application.square'));
                    $sheet->setCellValue('R1',trans('common::application.mosques'));
                    //    $sheet->setCellValue('Q1',trans('common::application.street_address_'));
                    $sheet->setCellValue('S1',trans('common::application.mobile'));
                    //  $sheet->setCellValue('T1',trans('common::application.phone'));
                    $sheet->setCellValue('T1',trans('aid-repository::application.family_count'));


                    $male = trans('common::application.male');
                    $female = trans('common::application.female');
                    $z= 2;
                    foreach($items as $k=>$v ) {
                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                        $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                        $sheet->setCellValue('D'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);
                        $sheet->setCellValue('E'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);
                        $sheet->setCellValue('F'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                        $sheet->setCellValue('G'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                        $sheet->setCellValue('H'.$z,(is_null($v->nomination_name) || $v->nomination_name == ' ' ) ? '-' : $v->nomination_name);
                        $sheet->setCellValue('I'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                        $sheet->setCellValue('J'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);

                        if($v->gender = 2){
                            $sheet->setCellValue('K'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $female);
                        }else if ($v->gender = 1){
                            $sheet->setCellValue('K'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $male);
                        }

                        $sheet->setCellValue('L'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                        $sheet->setCellValue('M'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                        // $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                        $sheet->setCellValue('N'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                        $sheet->setCellValue('O'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                        $sheet->setCellValue('P'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                        $sheet->setCellValue('Q'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                        $sheet->setCellValue('R'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                        //   $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                        $sheet->setCellValue('S'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                        //  $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                        $sheet->setCellValue('T'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);

                        $z++;
                    }
                });
                /*  $excel->sheet($excel_title,function($sheet)use($items,$request)   {
                     $sheet->setRightToLeft(true);
                     $sheet->setAllBorders('thin');
                     $sheet->setfitToWidth(true);
                     $sheet->setHeight(1,40);
                     $sheet->getDefaultStyle()->applyFromArray([
                         'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                         'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                     ]);
                     $sheet->setOrientation('landscape');

                     $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                         'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                         'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                         'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                         'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                     ]);

                     $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                         'V','W','X','Y','Z','AA','AB','AC'];

                     foreach($map2 as $key ) {
                         $sheet->cells($key .'1', function($cells) {
                             $cells->setAlignment('center');
                             $cells->setValignment('center');
                             $cells->setFontWeight('bold');
                         });
                     }

                     $sheet->setCellValue('A1',trans('common::application.#'));
                     $sheet->setCellValue('B1',trans('aid-repository::application.id_card_number'));
                     $sheet->setCellValue('C1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('D1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('E1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('F1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('G1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('H1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                     $sheet->setCellValue('I1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                     // $sheet->setCellValue('AC1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                     $male = trans('common::application.male');
                     $female = trans('common::application.female');
                     $z= 2;
                     foreach($items as $k=>$v ) {
                         $sheet->setCellValue('A'.$z,$k+1);
                         $sheet->setCellValue('B'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                         $sheet->setCellValue('C'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);

                         $kinship_name = ' ';
                         if($request->action == 'wives'){
                             if($v->related_gender = 2){
                                 $kinship_name = trans('common::application.wife_');
                             }else if ($v->related_gender = 1){
                                 $kinship_name = trans('common::application.husband');
                             }
                         }else{
                             if($v->related_gender = 2){
                                 $kinship_name = trans('common::application.daughter_');
                             }else if ($v->related_gender = 1){
                                 $kinship_name = trans('common::application.son_');
                             }
                         }

                         $sheet->setCellValue('D'.$z,$kinship_name);
                         $sheet->setCellValue('E'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);

                         if($v->related_gender = 2){
                             $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $female);
                         }else if ($v->related_gender = 1){
                             $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $male);
                         }

                         $sheet->setCellValue('G'.$z,(is_null($v->related_marital_status) || $v->related_marital_status == ' ' ) ? '-' : $v->related_marital_status);
                         $sheet->setCellValue('H'.$z,(is_null($v->related_birthday) || $v->related_birthday == ' ' ) ? '-' : $v->related_birthday);
                         $sheet->setCellValue('I'.$z,(is_null($v->related_death_date) || $v->related_death_date == ' ' ) ? '-' : $v->related_death_date);
                         //  $sheet->setCellValue('AC'.$z,(is_null($v->related_prev_family_name) || $v->related_prev_family_name == ' ' ) ? '-' : $v->related_prev_family_name);

                         $z++;
                     }
                 });



                $excel->sheet($excel_title,function($sheet)use($items,$request)   {
                          $sheet->setRightToLeft(true);
                          $sheet->setAllBorders('thin');
                          $sheet->setfitToWidth(true);
                          $sheet->setHeight(1,40);
                          $sheet->getDefaultStyle()->applyFromArray([
                              'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                              'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                          ]);
                          $sheet->setOrientation('landscape');

                          $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                                             'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                                             'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                                             'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                                             'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                                        ]);

                          $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                                   'V','W','X','Y','Z','AA','AB','AC'];

                          foreach($map2 as $key ) {
                              $sheet->cells($key .'1', function($cells) {
                                  $cells->setAlignment('center');
                                  $cells->setValignment('center');
                                  $cells->setFontWeight('bold');
                              });
                          }

                          $sheet->setCellValue('A1',trans('common::application.#'));
                          $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                          $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                          $sheet->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                          $sheet->setCellValue('E1',trans('aid-repository::application.nomination'));
                          $sheet->setCellValue('F1',trans('aid-repository::application.Organization_Name'));
                          $sheet->setCellValue('G1',trans('aid-repository::application.status'));
                          $sheet->setCellValue('H1',trans('aid-repository::application.reason'));
                          $sheet->setCellValue('I1',trans('common::application.gender'));
                          $sheet->setCellValue('J1',trans('common::application.marital_status'));
                          $sheet->setCellValue('K1',trans('common::application.birthday'));
                     //     $sheet->setCellValue('L1',trans('common::application.country'));
                          $sheet->setCellValue('L1',trans('common::application.district'));
                          $sheet->setCellValue('M1',trans('common::application.region_'));
                          $sheet->setCellValue('N1',trans('common::application.nearlocation_'));
                          $sheet->setCellValue('O1',trans('common::application.square'));
                          $sheet->setCellValue('P1',trans('common::application.mosques'));
                      //    $sheet->setCellValue('Q1',trans('common::application.street_address_'));
                          $sheet->setCellValue('Q1',trans('common::application.mobile'));
                        //  $sheet->setCellValue('T1',trans('common::application.phone'));
                          $sheet->setCellValue('R1',trans('aid-repository::application.family_count'));

                          $sheet->setCellValue('S1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('T1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('U1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('V1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('W1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('X1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                          $sheet->setCellValue('Y1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                          // $sheet->setCellValue('AC1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                          $male = trans('common::application.male');
                          $female = trans('common::application.female');
                          $z= 2;
                          foreach($items as $k=>$v ) {
                              $sheet->setCellValue('A'.$z,$k+1);
                              $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                              $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                              $sheet->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                              $sheet->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                              $sheet->setCellValue('F'.$z,(is_null($v->nomination_name) || $v->nomination_name == ' ' ) ? '-' : $v->nomination_name);
                              $sheet->setCellValue('G'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                              $sheet->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);

                              if($v->gender = 2){
                                  $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $female);
                              }else if ($v->gender = 1){
                                  $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $male);
                              }

                              $sheet->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                              $sheet->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                             // $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                              $sheet->setCellValue('L'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                              $sheet->setCellValue('M'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                              $sheet->setCellValue('N'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                              $sheet->setCellValue('O'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                              $sheet->setCellValue('P'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                           //   $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                              $sheet->setCellValue('Q'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                            //  $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                              $sheet->setCellValue('R'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);

                              $sheet->setCellValue('S'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);

                              $kinship_name = ' ';
                              if($request->action == 'wives'){
                                  if($v->related_gender = 2){
                                      $kinship_name = trans('common::application.wife_');
                                  }else if ($v->related_gender = 1){
                                      $kinship_name = trans('common::application.husband');
                                  }
                               }else{
                                   if($v->related_gender = 2){
                                      $kinship_name = trans('common::application.daughter_');
                                  }else if ($v->related_gender = 1){
                                      $kinship_name = trans('common::application.son_');
                                  }
                               }

                              $sheet->setCellValue('T'.$z,$kinship_name);
                              $sheet->setCellValue('U'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);

                              if($v->related_gender = 2){
                                  $sheet->setCellValue('V'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $female);
                              }else if ($v->related_gender = 1){
                                  $sheet->setCellValue('V'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $male);
                              }

                              $sheet->setCellValue('W'.$z,(is_null($v->related_marital_status) || $v->related_marital_status == ' ' ) ? '-' : $v->related_marital_status);
                              $sheet->setCellValue('X'.$z,(is_null($v->related_birthday) || $v->related_birthday == ' ' ) ? '-' : $v->related_birthday);
                              $sheet->setCellValue('Y'.$z,(is_null($v->related_death_date) || $v->related_death_date == ' ' ) ? '-' : $v->related_death_date);
                            //  $sheet->setCellValue('AC'.$z,(is_null($v->related_prev_family_name) || $v->related_prev_family_name == ' ' ) ? '-' : $v->related_prev_family_name);

                           $z++;
                          }
                      });


                      */
            })
                ->store('xlsx', storage_path('tmp/'));

            /*           $token = md5(uniqid());
                       \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items,$excel_title,$request)  {
                           $excel->setTitle($excel_title);
                           $excel->setDescription($excel_title);
                                              $excel->sheet($excel_title,function($sheet)use($items,$request)   {
                                                  $sheet->setRightToLeft(true);
                                                  $sheet->setAllBorders('thin');
                                                  $sheet->setfitToWidth(true);
                                                  $sheet->setHeight(1,40);
                                                  $sheet->getDefaultStyle()->applyFromArray([
                                                      'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                      'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                  ]);
                                                  $sheet->setOrientation('landscape');

                                                  $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                                                                     'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                                                                     'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                                                                     'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                                                                     'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                                                                ]);

                                                  $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                                                           'V','W','X','Y','Z','AA','AB','AC'];

                                                  foreach($map2 as $key ) {
                                                      $sheet->cells($key .'1', function($cells) {
                                                          $cells->setAlignment('center');
                                                          $cells->setValignment('center');
                                                          $cells->setFontWeight('bold');
                                                      });
                                                  }

                                                  $sheet->setCellValue('A1',trans('common::application.#'));
                                                  $sheet->setCellValue('B1',trans('aid-repository::application.id_card_number'));
                                                  $sheet->setCellValue('C1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('D1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('E1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('F1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('G1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('H1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('I1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                                                  // $sheet->setCellValue('AC1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                  $male = trans('common::application.male');
                                                  $female = trans('common::application.female');
                                                  $z= 2;
                                                  foreach($items as $k=>$v ) {
                                                      $sheet->setCellValue('A'.$z,$k+1);
                                                      $sheet->setCellValue('B'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                                      $sheet->setCellValue('C'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);

                                                      $kinship_name = ' ';
                                                      if($request->action == 'wives'){
                                                          if($v->related_gender = 2){
                                                              $kinship_name = trans('common::application.wife_');
                                                          }else if ($v->related_gender = 1){
                                                              $kinship_name = trans('common::application.husband');
                                                          }
                                                       }else{
                                                           if($v->related_gender = 2){
                                                              $kinship_name = trans('common::application.daughter_');
                                                          }else if ($v->related_gender = 1){
                                                              $kinship_name = trans('common::application.son_');
                                                          }
                                                       }

                                                      $sheet->setCellValue('D'.$z,$kinship_name);
                                                      $sheet->setCellValue('E'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);

                                                      if($v->related_gender = 2){
                                                          $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $female);
                                                      }else if ($v->related_gender = 1){
                                                          $sheet->setCellValue('F'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $male);
                                                      }

                                                      $sheet->setCellValue('G'.$z,(is_null($v->related_marital_status) || $v->related_marital_status == ' ' ) ? '-' : $v->related_marital_status);
                                                      $sheet->setCellValue('H'.$z,(is_null($v->related_birthday) || $v->related_birthday == ' ' ) ? '-' : $v->related_birthday);
                                                      $sheet->setCellValue('I'.$z,(is_null($v->related_death_date) || $v->related_death_date == ' ' ) ? '-' : $v->related_death_date);
                                                    //  $sheet->setCellValue('AC'.$z,(is_null($v->related_prev_family_name) || $v->related_prev_family_name == ' ' ) ? '-' : $v->related_prev_family_name);

                                                   $z++;
                                                  }
                                              });

                                        $excel->sheet($excel_title,function($sheet)use($items,$request)   {
                                                  $sheet->setRightToLeft(true);
                                                  $sheet->setAllBorders('thin');
                                                  $sheet->setfitToWidth(true);
                                                  $sheet->setHeight(1,40);
                                                  $sheet->getDefaultStyle()->applyFromArray([
                                                      'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                      'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                  ]);
                                                  $sheet->setOrientation('landscape');

                                                  $sheet->setWidth([ 'A'=> 5  ,'B'=> 35 ,'C'=>  20 ,'D'=>  35,
                                                                     'E'=> 35 ,'F'=> 20 ,'G'=>  20 ,'H'=>  40 ,'I' => 15 ,'J'=> 15,
                                                                     'K'=> 15 ,'L'=> 15 ,'M'=>  25 ,'N'=>  25 ,'O' => 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 45,'S'=> 25,
                                                                     'T'=> 25 ,'U'=> 25 ,'V'=>  35 ,'W'=>  20 ,'X' => 35,
                                                                     'Y'=> 35 ,'Z'=> 20 ,'AA'=> 20 ,'AB'=> 20 ,'AC'=> 25
                                                                ]);

                                                  $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U',
                                                           'V','W','X','Y','Z','AA','AB','AC'];

                                                  foreach($map2 as $key ) {
                                                      $sheet->cells($key .'1', function($cells) {
                                                          $cells->setAlignment('center');
                                                          $cells->setValignment('center');
                                                          $cells->setFontWeight('bold');
                                                      });
                                                  }

                                                  $sheet->setCellValue('A1',trans('common::application.#'));
                                                  $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                                                  $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                  $sheet->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                                                  $sheet->setCellValue('E1',trans('aid-repository::application.nomination'));
                                                  $sheet->setCellValue('F1',trans('aid-repository::application.Organization_Name'));
                                                  $sheet->setCellValue('G1',trans('aid-repository::application.status'));
                                                  $sheet->setCellValue('H1',trans('aid-repository::application.reason'));
                                                  $sheet->setCellValue('I1',trans('common::application.gender'));
                                                  $sheet->setCellValue('J1',trans('common::application.marital_status'));
                                                  $sheet->setCellValue('K1',trans('common::application.birthday'));
                                             //     $sheet->setCellValue('L1',trans('common::application.country'));
                                                  $sheet->setCellValue('L1',trans('common::application.district'));
                                                  $sheet->setCellValue('M1',trans('common::application.region_'));
                                                  $sheet->setCellValue('N1',trans('common::application.nearlocation_'));
                                                  $sheet->setCellValue('O1',trans('common::application.square'));
                                                  $sheet->setCellValue('P1',trans('common::application.mosques'));
                                              //    $sheet->setCellValue('Q1',trans('common::application.street_address_'));
                                                  $sheet->setCellValue('Q1',trans('common::application.mobile'));
                                                //  $sheet->setCellValue('T1',trans('common::application.phone'));
                                                  $sheet->setCellValue('R1',trans('aid-repository::application.family_count'));

                                                  $sheet->setCellValue('S1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('T1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('U1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('V1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('W1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('X1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                  $sheet->setCellValue('Y1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                                                  // $sheet->setCellValue('AC1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                  $male = trans('common::application.male');
                                                  $female = trans('common::application.female');
                                                  $z= 2;
                                                  foreach($items as $k=>$v ) {
                                                      $sheet->setCellValue('A'.$z,$k+1);
                                                      $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                                                      $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                                      $sheet->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                                                      $sheet->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                                                      $sheet->setCellValue('F'.$z,(is_null($v->nomination_name) || $v->nomination_name == ' ' ) ? '-' : $v->nomination_name);
                                                      $sheet->setCellValue('G'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                                      $sheet->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);

                                                      if($v->gender = 2){
                                                          $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $female);
                                                      }else if ($v->gender = 1){
                                                          $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $male);
                                                      }

                                                      $sheet->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                                                      $sheet->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                                                     // $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                                                      $sheet->setCellValue('L'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                                                      $sheet->setCellValue('M'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                                                      $sheet->setCellValue('N'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                                                      $sheet->setCellValue('O'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                                                      $sheet->setCellValue('P'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                                                   //   $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                                                      $sheet->setCellValue('Q'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                                    //  $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                                      $sheet->setCellValue('R'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);

                                                      $sheet->setCellValue('S'.$z,(is_null($v->related_card_number) || $v->related_card_number == ' ' ) ? '-' : $v->related_card_number);

                                                      $kinship_name = ' ';
                                                      if($request->action == 'wives'){
                                                          if($v->related_gender = 2){
                                                              $kinship_name = trans('common::application.wife_');
                                                          }else if ($v->related_gender = 1){
                                                              $kinship_name = trans('common::application.husband');
                                                          }
                                                       }else{
                                                           if($v->related_gender = 2){
                                                              $kinship_name = trans('common::application.daughter_');
                                                          }else if ($v->related_gender = 1){
                                                              $kinship_name = trans('common::application.son_');
                                                          }
                                                       }

                                                      $sheet->setCellValue('T'.$z,$kinship_name);
                                                      $sheet->setCellValue('U'.$z,(is_null($v->related_name) || $v->related_name == ' ' ) ? '-' : $v->related_name);

                                                      if($v->related_gender = 2){
                                                          $sheet->setCellValue('V'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $female);
                                                      }else if ($v->related_gender = 1){
                                                          $sheet->setCellValue('V'.$z,(is_null($v->related_gender) || $v->related_gender == ' ' ) ? '-' : $male);
                                                      }

                                                      $sheet->setCellValue('W'.$z,(is_null($v->related_marital_status) || $v->related_marital_status == ' ' ) ? '-' : $v->related_marital_status);
                                                      $sheet->setCellValue('X'.$z,(is_null($v->related_birthday) || $v->related_birthday == ' ' ) ? '-' : $v->related_birthday);
                                                      $sheet->setCellValue('Y'.$z,(is_null($v->related_death_date) || $v->related_death_date == ' ' ) ? '-' : $v->related_death_date);
                                                    //  $sheet->setCellValue('AC'.$z,(is_null($v->related_prev_family_name) || $v->related_prev_family_name == ' ' ) ? '-' : $v->related_prev_family_name);

                                                   $z++;
                                                  }
                                              });

                       })
                         ->store('xlsx', storage_path('tmp/'));
       */
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['status' => true , 'download_token' => $token]);
        }

        return response()->json(['status' => false]);

    }

    // check the benefit of person on project (excel sheet + project_id)
    public function check(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;
        $duplicated =0;
        $success =0;
        $invalid_cards=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=\Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $total_ = 0;

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(!is_null($value['rkm_alhoy'])){
                                        if(in_array($value['rkm_alhoy'],$processed)){
                                            $duplicated++;
                                        }
                                        else{
                                            if(!is_null($value['rkm_alhoy'])){
                                                if(strlen($value['rkm_alhoy'])  <= 9) {
                                                    $card =(int)$value['rkm_alhoy'];
                                                    if(is_null(Person::HasRaw($card))) {
                                                        $invalid_cards[]=$value['rkm_alhoy'];
                                                    }else{
                                                        $processed[]=$value['rkm_alhoy'];
                                                        $success++;
                                                    }
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                        $total_++;
                                    }

                                }

                                if($total_ == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('aid-repository::application.All names entered are incorrect or not registered in the system')  ]);
                                }
                                else{
                                    $project = Project::where('id', $request->project_id)->first();
                                    $candidates=[];
                                    $not_candidates=[];

                                    $candidates_=ProjectPerson::checkCandidates($project->id,$processed ,true);
                                    if(sizeof($candidates_) > 0){
                                        foreach ($candidates_ as $key_ => $value_){
                                            $candidates[]=(Object)[
                                                'project_name' => $project->name,
                                                'amount' => $project->amount,
                                                'exchange_rate' => $project->exchange_rate,
                                                'full_name' => $value_->full_name,
                                                'id_card_number' => $value_->id_card_number,
                                                'gender' => $value_->gender,
                                                'marital_status_name' => $value_->marital_status_name,
                                                'birthday' => $value_->birthday,
                                                'country_name' => $value_->country_name,
                                                'district_name' => $value_->district_name,
                                                'region_name' => $value_->region_name,
                                                'nearlocation_name' => $value_->nearlocation_name,
                                                'square_name' => $value_->square_name,
                                                'mosques_name' => $value_->mosques_name,
                                                'street_address' => $value_->street_address,
                                                'mobile' => $value_->mobile,
                                                'phone' => $value_->phone,
                                                'family_cnt' => $value_->family_cnt,
                                                'organization_name' => $value_->organization_name,
                                                'candidate_organization_name' => $value_->candidate_organization_name,
                                                'status' => $value_->status,
                                                'status_name' => $value_->status_name,
                                                'reason' => $value_->reason
                                            ];

                                        }
                                    }
                                    $not_candidates_=ProjectPerson::checkCandidates($project->id,$processed, false);
                                    if(sizeof($not_candidates_) > 0){
                                        foreach ($not_candidates_ as $key_ => $value_){
                                            $not_candidates[]=(Object)[
                                                'project_name' => $project->name,
                                                'full_name' => $value_->full_name,
                                                'id_card_number' => $value_->id_card_number,
                                                'status_name' => trans('aid-repository::application.Not filtered')
                                            ];
                                        }
                                    }


                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($candidates , $not_candidates , $invalid_cards) {
                                        $excel->setTitle(trans('aid-repository::application.candidates list'));
                                        $excel->setDescription(trans('aid-repository::application.candidates list'));

                                        if(sizeof($candidates) > 0){
                                            $excel->sheet(trans('aid-repository::application.candidates list'), function($sheet) use($candidates) {
                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);
                                                $sheet->setOrientation('landscape');

                                                $sheet->setWidth([
                                                    'A'=>5   ,'B'=> 35 ,'C'=> 20 ,'D'=> 35,
                                                    'E'=> 35 ,'F'=> 20 ,'G'=> 15 ,'H'=> 40 ,'I'=> 15 ,'J'=> 15,
                                                    'K'=> 15 ,'L'=> 25 ,'M'=> 25 ,'N'=> 25 ,'O'=> 25 ,'P'=> 25 ,'Q'=> 25,'R'=> 45,'S'=> 25,'T'=> 25,'U'=> 25
                                                ]);

                                                $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'];

                                                foreach($map2 as $key ) {
                                                    $sheet->cells($key .'1', function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });
                                                }

                                                $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                                                $sheet->setCellValue('B1',trans('aid-repository::application.name'));
                                                $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                $sheet->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                                                $sheet->setCellValue('E1',trans('aid-repository::application.Organization_Name'));
                                                $sheet->setCellValue('F1',trans('aid-repository::application.status'));
                                                $sheet->setCellValue('G1',trans('aid-repository::application.amount_sh'));
                                                $sheet->setCellValue('H1',trans('aid-repository::application.reason'));
                                                $sheet->setCellValue('I1',trans('common::application.gender'));
                                                $sheet->setCellValue('J1',trans('common::application.marital_status'));
                                                $sheet->setCellValue('K1',trans('common::application.birthday'));
                                                $sheet->setCellValue('L1',trans('common::application.country'));
                                                $sheet->setCellValue('M1',trans('common::application.district'));
                                                $sheet->setCellValue('N1',trans('common::application.region_'));
                                                $sheet->setCellValue('O1',trans('common::application.nearlocation_'));
                                                $sheet->setCellValue('P1',trans('common::application.square'));
                                                $sheet->setCellValue('Q1',trans('common::application.mosques'));
                                                $sheet->setCellValue('R1',trans('common::application.street_address_'));
                                                $sheet->setCellValue('S1',trans('common::application.mobile'));
                                                $sheet->setCellValue('T1',trans('common::application.phone'));
                                                $sheet->setCellValue('U1',trans('aid-repository::application.family_count'));

                                                $z= 2;
                                                foreach($candidates as $k=>$v ) {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                                                    $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                                    $sheet->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                                                    $sheet->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                                                    $sheet->setCellValue('F'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                                    if($v->status == 4){
                                                        $sheet->setCellValue('G'.$z,0);
                                                    }else{
                                                        $sheet->setCellValue('G'.$z,($v->amount * $v->exchange_rate));
                                                    }
                                                    $sheet->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);
                                                    $sheet->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $v->gender);
                                                    $sheet->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                                                    $sheet->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                                                    $sheet->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                                                    $sheet->setCellValue('M'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                                                    $sheet->setCellValue('N'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                                                    $sheet->setCellValue('O'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                                                    $sheet->setCellValue('P'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                                                    $sheet->setCellValue('Q'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                                                    $sheet->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                                                    $sheet->setCellValue('S'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                                    $sheet->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                                    $sheet->setCellValue('U'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);
                                                    $z++;
                                                }
                                            });
                                        }
                                        if(sizeof($not_candidates) > 0){
                                            $excel->sheet(trans('aid-repository::application.not candidates to all'), function($sheet) use($not_candidates) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                                                $sheet->setCellValue('B1',trans('aid-repository::application.full_name'));
                                                $sheet->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                $sheet->setCellValue('D1',trans('aid-repository::application.project_name'));
                                                $sheet->setCellValue('E1',trans('aid-repository::application.status'));


                                                $z= 2;
                                                foreach($not_candidates as $k=>$v ) {
                                                    $sheet->setCellValue('A' . $z, $k+1);
                                                    $sheet->setCellValue('B' . $z, (is_null($v->full_name) || $v->full_name == ' ') ? '-' : $v->full_name);
                                                    $sheet->setCellValue('C' . $z, (is_null($v->id_card_number) || $v->id_card_number == ' ') ? '-' : $v->id_card_number);
                                                    $sheet->setCellValue('D' . $z, (is_null($v->project_name) || $v->project_name == ' ') ? '-' : $v->project_name);
                                                    $sheet->setCellValue('E' . $z, (is_null($v->status_name) || $v->status_name == ' ') ? '-' : $v->status_name);
                                                    $z++;
                                                }


                                            });
                                        }

                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('aid-repository::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }


                                    })->store('xlsx', storage_path('tmp/'));

                                    if( $total_ == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('aid-repository::application.Downloading the results file,'). trans('aid-repository::application.All the numbers were checked and their data was fetched') ,'download_token' => $token]);
                                    }
                                    else{

                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('aid-repository::application.Downloading the results file,') .trans('aid-repository::application.Some numbers were successfully checked, as the total number of digits') .' :  '. $total_ . ' ,  '.
                                                trans('aid-repository::application.Number of names checked') .' :  ' .$success . ' ,  ' .
                                               trans('aid-repository::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('aid-repository::application.Number of duplicate names').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }


                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // upload candidates to project (excel sheet + project_id)
    public function upload_candidates(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $project_id = $request->input('project_id');
        $project = Project::with('Repository')->findOrFail($project_id);

        if ($project->status != Project::STATUS_CANDIDATES ) {
            $response['status']='failed';
            $response['msg']= trans('aid-repository::application.Nomination for a project approved for implementation, closed or draft cannot be submitted');
            return response()->json($response);
        }


        $candidates_count = ProjectPerson::where('project_id',$project_id)->count();

        if($candidates_count == $project->quantity){
            return response()->json(['status' =>'failed' , 'error_type' =>' ' ,
                                     'msg' =>  trans('aid-repository::application.No new candidates can be accredited because you have the same number of accredited candidates') ]);
        }

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();
            $user = \Auth::user();
            $categoryObj = \Common\Model\Categories\AidsCategories::where('id',5)->first();
            $category_id = 5;
            if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                $first=\Excel::selectSheets('data')->load($path)->first();

                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ) {

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile) {
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                $total_ = 0;
                                $duplicated =0;
                                $nominated =0;
                                $blocked =0;
                                $not_person =0;
                                $can_not_connect = 0;
                                $invalid_card =0;
                                $not_case =0;
                                $restricted =0;
                                $person_not_register =0;
                                $limited =0;
                                $deceased =0;
                                $deleted_cases =0;
                                $success =0;
                                $processed=[];
                                $return=[];
                                $insert=[];

                                foreach ($records as $key =>$value) {
                                    $card =(int)$value['rkm_alhoy'];
                                    if(!is_null($card)){
                                        if($candidates_count < $project->quantity){
                                            if(in_array($card,$processed)){
                                                $return[]=['name' =>'','id_card_number' =>$card,'reason'=> trans('common::application.duplicated')];
                                                $duplicated++;
                                                $restricted++;
                                            }
                                            else{
                                                if(strlen($card)  <= 9) {
                                                    if (!GovServices::checkCard($card)) {
                                                        $return[]=['id_card_number' => $card , 'reason' =>trans('common::application.invalid_card') , 'name' => ' '];
                                                        $restricted++;
                                                        $invalid_card++;
                                                    }
                                                    else{
                                                        $person_id = null;
                                                        $person_row = Person::where(function ($q) use ($card){
                                                            $q->where('id_card_number',$card);
                                                        })->first();

                                                        if(is_null($person_row)) {

                                                            if (Setting::getIgnoreGovServices()){
                                                                $return[]=['id_card_number' => $card , 'reason' =>trans('common::application.not_person') , 'name' => ' '];
                                                                $restricted++;
                                                                $not_person++;
                                                            }else{
                                                                $row = GovServices::byCard($card);
                                                                if($row['status'] == true) {
                                                                    $record = \Common\Model\Person::PersonMap($row['row']);
                                                                    $adscountryId=null;
                                                                    $adsdistrictId=null;
                                                                    $adsregionId=null;
                                                                    $adsneighborhoodId=null;
                                                                    $adssquareId=null;
                                                                    $adsmosquesId=null;

                                                                    CloneGovernmentPersons::saveNew((Object) $record);
                                                                    $person =\Common\Model\Person::savePerson($record);
                                                                    if(!is_null($person['death_date'])){
                                                                        $return[]=['name' =>'','id_card_number' =>$card,'reason'=> trans('common::application.deceased') ];
                                                                        $restricted++;
                                                                        $deceased++;
                                                                    }else{
                                                                        $case=  \Common\Model\AidsCases::create(['person_id' => $person['id'],
                                                                            'status' => 1, 'category_id' => 5,
                                                                            'created_at' => date('Y-m-d H:i:s'),
                                                                            'user_id' => $user->id,
                                                                            'rank' => 0,
                                                                            'organization_id' => $user->organization_id]);

                                                                        \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                                        \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' . $person['full_name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                                        $passed = ProjectPerson::checkPersonsUsingCard($card);
                                                                        if($passed['status'] == true){
                                                                            $insert[] = ['id' => $person['id'], 'organization_id' => $user->organization_id];
                                                                            $candidates_count++;
                                                                            $success++;
                                                                        }
                                                                        else{
                                                                            if ($passed['reason'] == 'deleted') {
                                                                                $passed['reason'] =  trans('common::application.deleted cases');
                                                                                $deleted_cases++;
                                                                            }

                                                                            if ($passed['reason'] == 'blocked') {
                                                                                $passed['reason'] =  trans('sponsorship::application.blocked');
                                                                                $blocked++;
                                                                            }

                                                                            $return[]=['name' =>'','id_card_number' =>$card,'reason'=> $passed['reason'] ];
                                                                            $restricted++;
                                                                        }
                                                                    }

                                                                }
                                                                else{
                                                                    $return[]=['id_card_number' => $card , 'reason' => trans('common::application.can_not_connect_to_gov_services'), 'name' => ' '];
                                                                    $restricted++;
                                                                    $can_not_connect++;
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            if(!is_null($person_row->death_date)){
                                                                $return[]=['name' =>'','id_card_number' =>$card,'reason'=> trans('common::application.deceased') ];
                                                                $restricted++;
                                                                $deceased++;
                                                            }else{
                                                                $person_id = $person_row->id;
                                                                $founded = ProjectPerson::where(['project_id' => $project_id,'person_id' => $person_id])->first();

                                                                if(is_null($founded)){
                                                                    $passed = ProjectPerson::checkPersonsUsingCard($card);
                                                                    if($passed['status'] == true){
                                                                        if($candidates_count < $project->quantity){
                                                                            $insert[] = ['id' => $person_id, 'organization_id' => $passed['organization_id']];
                                                                            $candidates_count++;
                                                                            $success++;
                                                                        }else{
                                                                            $return[]=['name' =>'','id_card_number' =>$card,'reason'=>trans('common::application.limited') ];
                                                                            $limited++;
                                                                            $restricted++;
                                                                        }
                                                                    }
                                                                    else{
                                                                        if ($passed['reason'] == 'not_case') {
                                                                            $case=  \Common\Model\AidsCases::create(['person_id' => $person_id,
                                                                                'status' => 1, 'category_id' => 5,
                                                                                'created_at' => date('Y-m-d H:i:s'),
                                                                                'user_id' => $user->id,
                                                                                'organization_id' => $user->organization_id]);

                                                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                                            \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);

                                                                            $full_name = $person_row->first_name.' '.$person_row->second_name.' '.$person_row->third_name.' '.$person_row->last_name;
                                                                            \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' . $full_name . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                                            $insert[] = ['id' => $person_id, 'organization_id' => $user->organization_id];
                                                                            $success++;
                                                                        }
                                                                        else{
                                                                            if ($passed['reason'] == 'deleted') {
                                                                                $passed['reason'] =  trans('common::application.deleted cases');
                                                                                $deleted_cases++;
                                                                            }

                                                                            if ($passed['reason'] == 'blocked') {
                                                                                $passed['reason'] =  trans('sponsorship::application.blocked');
                                                                                $blocked++;
                                                                            }

                                                                            $return[]=['name' =>'','id_card_number' =>$card,'reason'=> $passed['reason'] ];
                                                                            $restricted++;
                                                                        }
                                                                    }
                                                                }
                                                                else{
                                                                    $return[]=['name' =>'','id_card_number' =>$card,'reason'=> trans('common::application.nominated')];
                                                                    $restricted++;
                                                                    $nominated++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    $return[]=['id_card_number' => $card , 'reason' =>trans('common::application.invalid_card'), 'name' => ' '];
                                                    $restricted++;
                                                    $invalid_card++;
                                                }

                                                $processed[]=$card;
                                            }
                                        }else{
                                            $return[]=['name' =>'','id_card_number' =>$card,'reason'=> trans('common::application.limited')];
                                            $limited++;
                                            $restricted++;
                                        }
                                        $total_++;
                                    }
                                }

                                $response = array();

                                $user = Auth::user();

                                if(sizeof($insert) > 0) {
                                    ProjectPerson::saveForProject_($project_id, $user->organization_id, $insert);
                                    \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_CREATED',
                                        trans('aid-repository::application.Added new candidates to the project') . ' "'.$project->name. '" ');
                                }

                                if ($success == $total_) {
                                    $response['status']='success';
                                    $response['msg']= trans('aid-repository::application.The cases is successfully nomination');
                                    return response()->json($response);
                                }
                                
                                else if ($can_not_connect == sizeof($records)) {
                                    $response['status']='failed';
                                    $response["msg"] =  trans('aid-repository::application.The cases is not successfully nomination') .
                                                        ' (  ' .trans('common::application.can not connected to api service') . '  )';
                                    return response()->json($response);
                                }       
                                else if ($nominated == sizeof($records)) {
                                    $response['status']='failed';
                                    $response["msg"] = trans('aid-repository::application.all card are nominated');
                                    return response()->json($response);
                                }
                                else if ($blocked == $total_) {
                                    $response['status']='failed';
                                    $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                                    return response()->json($response);
                                }
                                else{

                                    $response['insert']=$insert;
                                    $response['status']='failed';
                                    if($success > 0){
                                        $response['status']='success';
                                    }
                                    $response['msg']= trans('aid-repository::application.The cases is not successfully nomination')
                                        . ' ( '. trans('aid-repository::application.candidates count') .': ' .$total_
                                        .' ، '. trans('aid-repository::application.new nominate') .' : '. $success.' , '.
                                        trans('aid-repository::application.not_case') . ' : ' . $not_case . ' , ' .
                                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                        trans('aid::application.person_not_register_on_system') . ' : ' . $person_not_register . ' , ' .
                                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                        trans('aid-repository::application.exceed project quantity') . ' : ' . $limited . ' , ' .
                                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                        trans('common::application.can_not_connect_to_gov_services'). ' : ' . $can_not_connect . ' , ' .
                                        trans('common::application.count of deleted cases') . ' : ' . $deleted_cases . ' , ' .
                                        trans('common::application.count of death person') . ' : ' . $deceased . ' , ' .
                                        trans('aid-repository::application.previously inserted') . ' : '.$nominated .' )';
                                }

                                if(sizeof($return) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($return) {
                                        $excel->setTitle(trans('aid-repository::application.restricted_'));
                                        $excel->setDescription(trans('aid-repository::application.restricted_'));
                                        $excel->sheet(trans('aid-repository::application.restricted_'), function($sheet) use($return) {

                                            $sheet->setRightToLeft(true);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setHeight(1,40);
                                            $sheet->getDefaultStyle()->applyFromArray([
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                            ]);

                                            $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);
                                            $sheet->setCellValue('A1',trans('common::application.#'));
                                            $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                            $sheet->setCellValue('C1',trans('common::application.reason'));

                                            $z= 2;
                                            foreach($return as $k=>$v )
                                            {

                                                $sheet->setCellValue('A'.$z,$k+1);
                                                $sheet->setCellValue('B'.$z,(is_null($v['id_card_number']) ||$v['id_card_number'] == ' ' ) ? '-' :$v['id_card_number']);
                                                $sheet->setCellValue('C'.$z,(is_null($v['reason']) ||$v['reason'] == ' ' ) ? '-' :$v['reason']);

                                                $z++;
                                            }

                                        });
                                    })->store('xlsx', storage_path('tmp/'));
                                    $response['download_token']=$token;
                                }

                                return response()->json($response);
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }

        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

}
