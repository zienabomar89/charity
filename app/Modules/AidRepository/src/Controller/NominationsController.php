<?php
namespace AidRepository\Controller;

use AidRepository\Model\NominationRules;
use AidRepository\Model\Project;
use AidRepository\Model\ProjectOrganization;
use AidRepository\Model\ProjectPerson;
use AidRepository\Model\ProjectRule;
use AidRepository\Model\Nomination;
use AidRepository\Model\Repository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Organization\Model\Organization;
use Setting\Model\aidsLocation;

class NominationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

//    get ProjectRule using project_id
    public function index()
    {
        $rules = [];
        $project_id = request()->input('project_id');
        $strict = request()->input('strict');
        $projectRules = [];
        foreach (ProjectRule::all()->where('project_id', $project_id) as $entry) {
            if ($entry->rule_value) {
                $projectRules[$entry->rule_name] = unserialize($entry->rule_value);
            }
        }

        $pass = false;
        if(!$strict){
            $pass = true;
        }else{
            if(sizeof($projectRules) > 0){
                $pass = true;
            }
        }
        if($pass){
            $project = Project::findOrFail($project_id);
            foreach (Nomination::getRules($project_id,$project->repository_id) as $key => $rule) {
                if (isset($projectRules[$key])) {
                    if ($rule['type'] == 'range') {
                        $extra = $projectRules[$key];
                    } else {
                        $rule['placeholder']='';
                        $extra = [
                            'value' => $projectRules[$key],
                        ];
                    }
                } else {
                    $rule['placeholder']='';
                    $extra = [];
                }
                $options = isset($rule['options'])? is_callable($rule['options'])? call_user_func($rule['options']) : $rule['options'] : [];
                if ($strict) {
                    if (isset($extra['value']) && is_array($extra['value'])) {
                        foreach ($options as $k => $v) {
                            if (!in_array($v['id'], $extra['value'])) {
                                unset($options[$k]);
                            }
                        }
                    }
                }
                $rules[] = array_merge([
                    'name' => $key,
                    'placeholder' => $rule['placeholder'],
                    'label' => $rule['label'],
                    'type' => $rule['type'],
                    'options' => $options,
                ], $extra);
            }
            return response()->json(['status' => true , 'rules' => $rules]);
        }

    }

    // result of candidate to project according save project rule;
    public function theCandidates($id)
    {

        $project = Project::findOrFail($id);
        $user = Auth::user();
        $Org=\Organization\Model\Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        $rules = [];
        $strict = 1;
        $projectRules = [];

        foreach (ProjectRule::all()->where('project_id', $id) as $entry) {
            if ($entry->rule_value) {
                $projectRules[$entry->rule_name] = unserialize($entry->rule_value);
            }
        }

        foreach (Nomination::getRules($id,$project->repository_id) as $key => $rule) {
            if (isset($projectRules[$key])) {
                if ($rule['type'] == 'range') {
                    $extra = $projectRules[$key];
                } else {
                    $extra = [
                        'value' => $projectRules[$key],
                    ];
                }
            } else {
                $extra = [];
            }
            $options = isset($rule['options'])? is_callable($rule['options'])? call_user_func($rule['options']) : $rule['options'] : [];
            if ($strict) {
                if (isset($extra['value']) && is_array($extra['value'])) {
                    foreach ($options as $k => $v) {
                        if (!in_array($v['id'], $extra['value'])) {
                            unset($options[$k]);
                        }
                    }
                }
            }
            $rules[] = array_merge([
                'name' => $key,
                'label' => $rule['label'],
                'type' => $rule['type'],
                'options' => $options,
            ], $extra);
        }

        $rules['organization_id'] = $user->organization_id;
        return response()->json(['entries'=>Nomination::getNomination($rules,$id,$project->repository_id,$master),'master'=>$master]);
    }

    // statistic of organizations on project using project_id;
    public function statistic(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $project_id = $request->input('project_id');
        $project = Project::findOrFail($project_id);
        $exchange_rate = $project->exchange_rate;
        $user = Auth::user();
        $response['status']=false;
        $organization_id=$user->organization_id;
        $UserOrg=$user->organization;
        $level = $UserOrg->level;

        if($level != Organization::LEVEL_BRANCHES) {

            $organizations_=\DB::table('aid_projects_organizations')
                ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                ->where('aid_projects_organizations.project_id', '=', $project_id)
                ->where('char_organizations.parent_id', '=', $organization_id)
                ->selectRaw("aid_projects_organizations.organization_id ,
                         aid_projects_organizations.ratio , 
                         aid_projects_organizations.quantity ,  
                         aid_projects_organizations.amount , 
                        char_organizations.name ,
                        char_get_organizations_child_nominated_count(char_organizations.id, $project_id ) as child_nominated_count,
                        char_get_organization_nominated_count(char_organizations.id, $project_id ) as nominated_count,
                        round((char_organizations.container_share/100),2) as container_share")->get();

            $count = sizeof($organizations_);
            if($count > 0){
                foreach ($organizations_ as $k=>$v){
                    $v->descendants =\DB::table('char_organizations')
                        ->where('char_organizations.parent_id', '=', $v->organization_id)
                        ->selectRaw("char_organizations.id as organization_id , 
                        char_organizations.name ,
                         char_get_organization_nominated_count(char_organizations.id, $project_id ) as nominated_count,
                        round((char_organizations.container_share/100),2) as container_share")->get();


                    $v->descendants =\DB::table('aid_projects_organizations')
                        ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                        ->where('aid_projects_organizations.project_id', '=', $project_id)
                        ->where('char_organizations.parent_id', '=', $v->organization_id)
                        ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                             char_organizations.name ,
                                             char_get_organization_nominated_count(char_organizations.id, $project_id ) as nominated_count,
                                             round((char_organizations.container_share/100),2) as container_share")->get();


                    $count += sizeof($v->descendants );
                }
                $has_descendants = false;
                if($count > sizeof($organizations_)){
                    $has_descendants = true;
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($organizations_,$count,$level,$exchange_rate,$has_descendants) {
                    $excel->setTitle(trans('aid-repository::application.project_statistic'));
                    $excel->setDescription(trans('aid-repository::application.project_statistic'));
                    $excel->sheet(trans('aid-repository::application.project_statistic'), function($sheet) use($organizations_,$count,$level,$exchange_rate,$has_descendants) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);

                        if($has_descendants){
                            $sheet->setWidth(array('A'=> 10, 'B'=>40, 'H'=>40, 'C' => 15, 'D' => 15, 'E' => 15 ,'F' => 15 ,'G' => 15 ,'I' => 15));
                        }else{
                            $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 15, 'E' => 15 ,'F' => 15 ,'G' => 15 ));
                        }
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.organization_name'));
                        $sheet->setCellValue('C1',trans('common::application.nominated_no'));
                        $sheet->setCellValue('D1',trans('common::application.container_share'));
                        $sheet->setCellValue('E1',trans('common::application.ratio'));
                        $sheet->setCellValue('F1',trans('common::application.quantity'));
                        $sheet->setCellValue('G1',trans('common::application.amount_'));

                        $map2 = ['A','B','C','D','E','F','G'];
                        foreach($map2 as $key ) {
                            $sheet->mergeCells($key .'1:' . $key . 2);
                            $sheet->cells($key .'1:' . $key . 2, function($cells) {

                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                        }


                        if($has_descendants){
                            $sheet->setWidth(array('A'=> 10, 'B'=>40, 'H'=>40, 'C' => 15, 'D' => 15, 'E' => 15 ,'F' => 15 ,'G' => 15 ,'I' => 15));

                            $sheet->setCellValue('H1',trans('common::application.sub_organization_data'));
                            $sheet->mergeCells("H1:K1");
                            $sheet->cells("H1:K1", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet->setCellValue('H2',trans('common::application.organization_name'));
                            $sheet->setCellValue('I2',trans('common::application.nominated_no'));
                            $sheet->setCellValue('J2',trans('common::application.organization_data'));
                            $sheet->mergeCells("J2:K2");
                            $sheet->cells("J2:K2", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells("H2:J2", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $z=3;
                            foreach($organizations_ as $k=>$v )
                            {

                                $descendants_count = sizeof($v->descendants);
                                if($descendants_count > 0 ){
                                    $sheet ->setCellValue('A'.$z,$k+1);
                                    $sheet ->setCellValue('B'.$z,$v->name);
                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                        $sheet ->setCellValue('C'.$z,$v->child_nominated_count);
                                    }else{
                                        $sheet ->setCellValue('C'.$z,$v->nominated_count);
                                    }

                                    $sheet ->setCellValue('D'.$z,$v->container_share);
                                    $sheet ->setCellValue('E'.$z,$v->ratio);
                                    $sheet ->setCellValue('F'.$z,$v->quantity);
                                    $sheet ->setCellValue('G'.$z,$v->amount);

                                    $sheet->mergeCells("A".($z).":A".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("A".($z).":A".($z + ($descendants_count * 3 )), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("B".($z).":B".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("B".($z).":B".($z + ($descendants_count * 3) - 1), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("C".($z).":C".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("C".($z).":C".($z + (sizeof($v->descendants) *3 )), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("D".($z).":D".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("D".($z).":D".($z + (sizeof($v->descendants) *3 )), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("E".($z).":E".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("E".($z).":E".($z + ($descendants_count * 3) - 1), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("F".($z).":F".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("F".($z).":F".($z + ($descendants_count * 3) - 1), function($cells) {

                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->mergeCells("G".($z).":G".($z + ($descendants_count * 3) - 1));
                                    $sheet->cells("G".($z).":G".($z + ($descendants_count * 3) - 1), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    foreach($v->descendants as $k1=>$v1)
                                    {

                                        $sheet ->setCellValue('H'.($z + ($k1 * 3 )),$v1->name);
                                        $sheet->mergeCells('H'.($z + ($k1 * 3 )).":H".(($z + ($k1 * 3 ))+ 2));
                                        $sheet->cells('H'.($z + ($k1 * 3 )).":H".(($z + ($k1 * 3 ))+ 2), function($cells) {

                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                        });
                                        $sheet ->setCellValue('I'.($z + ($k1 * 3 )),$v1->nominated_count);
                                        $sheet->mergeCells('I'.($z + ($k1 * 3 )).":I".(($z + ($k1 * 3 ))+ 2));
                                        $sheet->cells('I'.($z + ($k1 * 3 )).":I".(($z + ($k1 * 3 ))+ 2), function($cells) {

                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                        });
                                        //                            $sheet ->setCellValue('G'.$z + $k1,$k+1);
                                        $sheet ->setCellValue('J'.($z + ($k1 * 3 )),trans('common::application.ratio'));
                                        $sheet ->setCellValue('J'.($z + ($k1 * 3 ) + 1),trans('common::application.quantity'));
                                        $sheet ->setCellValue('J'.($z + ($k1 * 3 ) + 2),trans('common::application.amount_'));
                                        $sheet ->setCellValue('K'.($z + ($k1 * 3 )),$v1->ratio);
                                        $sheet ->setCellValue('K'.($z + ($k1 * 3 ) + 1),($v1->quantity));
                                        $sheet ->setCellValue('K'.($z + ($k1 * 3 ) + 2),($v1->amount));
                                    }


                                }else{
                                    $sheet ->setCellValue('A'.$z,$k+1);
                                    $sheet ->setCellValue('B'.$z,$v->name);
                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                        $sheet ->setCellValue('C'.$z,$v->child_nominated_count);
                                    }else{
                                        $sheet ->setCellValue('C'.$z,$v->nominated_count);
                                    }

                                    $sheet ->setCellValue('D'.$z,$v->container_share);
                                    $sheet ->setCellValue('E'.$z,$v->ratio);
                                    $sheet ->setCellValue('F'.$z,$v->quantity);
                                    $sheet ->setCellValue('G'.$z,$v->amount);
                                    $sheet->setCellValue('H'.$z, ' - ');
                                    $sheet->mergeCells("H".$z .":K" .$z);
                                    $sheet->cells("H".$z .":K" .$z, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });
                                }

                                $z = $z + ($descendants_count * 3);

                            }

                        }
                        else{
                            $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 15, 'E' => 15 ,'F' => 15 ,'G' => 15 ));
                            $z=3;
                            foreach($organizations_ as $k=>$v )
                            {

                                $sheet ->setCellValue('A'.$z,$k+1);
                                $sheet ->setCellValue('B'.$z,$v->name);
                                $sheet ->setCellValue('C'.$z,$v->nominated_count);
                                $sheet ->setCellValue('D'.$z,$v->container_share);
                                $sheet ->setCellValue('E'.$z,$v->ratio);
                                $sheet ->setCellValue('F'.$z,$v->quantity);
                                $sheet ->setCellValue('G'.$z,$v->amount * $exchange_rate);

                                $z ++;
                            }
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' =>true ,'download_token' =>$token]);
            }

            return response()->json(['status' =>false ,'msg' =>trans('common::application.no organization')]);
        }
        else{
            $organizations_=\DB::table('aid_projects_organizations')
                            ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                            ->where('aid_projects_organizations.project_id', '=', $project_id)
                            ->where('char_organizations.id', '=', $organization_id)
                            ->selectRaw("aid_projects_organizations.organization_id ,
                                     aid_projects_organizations.ratio , 
                                     aid_projects_organizations.quantity ,  
                                     aid_projects_organizations.amount , 
                                     char_get_organizations_child_nominated_count(char_organizations.id, $project_id ) as child_nominated_count,
                                     char_get_organization_nominated_count(char_organizations.id, $project_id ) as nominated_count,
                                     round((char_organizations.container_share/100),2) as container_share")
                            ->first();


            $locations = aidsLocation::getTotalCountProjectTree($organizations_->quantity,$organizations_->amount,$project_id,$organization_id,$level);
            $count = sizeof($locations);
            if($count > 0){
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($locations) {
                    $excel->setTitle(trans('aid-repository::application.project_statistic'));
                    $excel->setDescription(trans('aid-repository::application.project_statistic'));
                    $excel->sheet(trans('aid-repository::application.project_statistic'), function($sheet) use($locations) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);

                        $sheet->setWidth(array('A'=> 10, 'B'=>25, 'C' => 15, 'D' => 10,
                                               'E'=> 10, 'F'=>10, 'G'=>25, 'H' => 15,
                                               'I' => 10 ,'J' => 10 ,'K' => 10 ,'L' => 25,'M' => 15 ,'N' => 10));

                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.nearlocation'));
                        $sheet->setCellValue('C1',trans('common::application.nominated_no'));
                        $sheet->setCellValue('D1',trans('common::application.ratio'));
                        $sheet->setCellValue('E1',trans('common::application.quantity'));
                        $sheet->setCellValue('F1',trans('common::application.amount_'));

                        $sheet->setCellValue('G1',trans('common::application.square'));
                        $sheet->setCellValue('H1',trans('common::application.nominated_no'));
                        $sheet->setCellValue('I1',trans('common::application.ratio'));
                        $sheet->setCellValue('J1',trans('common::application.quantity'));
                        $sheet->setCellValue('K1',trans('common::application.amount_'));

                        $sheet->setCellValue('L1',trans('common::application.mosques'));
                        $sheet->setCellValue('M1',trans('common::application.nominated_no'));
                        $sheet->setCellValue('N1',trans('common::application.mosques_data'));
                        $sheet->mergeCells("N1:O1");
                        $sheet->cells("N1:O1", function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->cells("A1:N1", function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $z = 2;
                        foreach($locations as $k=>$v ) {

                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->mergeCells("A".( $z ).":A". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("A".( $z ).":A". ( $z + ( $v->mosque_cnt *3 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet ->setCellValue('B'.$z,$v->location_name);
                            $sheet->mergeCells("B".( $z ).":B". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("B".( $z ).":B". ( $z + ( $v->mosque_cnt *3 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet ->setCellValue('C'.$z,$v->nominated);
                            $sheet->mergeCells("C".( $z ).":C". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("C".( $z ).":C". ( $z + ( $v->mosque_cnt *3 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet ->setCellValue('D'.$z,$v->ratio);
                            $sheet->mergeCells("D".( $z ).":D". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("D".( $z ).":D". ( $z + ( $v->mosque_cnt *2 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet ->setCellValue('E'.$z,$v->total);
                            $sheet->mergeCells("E".( $z ).":E". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("E".( $z ).":E". ( $z + ( $v->mosque_cnt *2 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet ->setCellValue('F'.$z,$v->amount);
                            $sheet->mergeCells("F".( $z ).":F". ( $z + ( $v->mosque_cnt *3 ) - 1));
                            $sheet->cells("F".( $z ).":F". ( $z + ( $v->mosque_cnt *3 )), function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $i = $z;
                            foreach($v->squares as $k1 =>$v1 ) {

                                $sheet ->setCellValue('G'.$i,$v1->location_name);
                                $sheet->mergeCells('G'.$i.":G". ( $i + ( $v1->mosque_cnt *3 ) - 1 ));
                                $sheet->cells('G'.$i.":G". ( $i + ( $v1->mosque_cnt *3 ) - 1 ), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $sheet ->setCellValue('H'.$i,$v1->nominated);
                                $sheet->mergeCells("H".$i .":H". ( $i + ( $v1->mosque_cnt *3 ) - 1 ));
                                $sheet->cells("H".$i.":H". ( $i + ( $v1->mosque_cnt *3 ) - 1 ), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $sheet ->setCellValue('I'.$i,$v1->ratio);
                                $sheet->mergeCells("I".$i.":I". ( $i + ( $v1->mosque_cnt *3 ) - 1 ));
                                $sheet->cells("I".$i.":I". ( $i + ( $v1->mosque_cnt *3 ) - 1 ), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $sheet ->setCellValue('J'.$i,$v1->total);
                                $sheet->mergeCells("J".$i.":J". ( $i + ( $v1->mosque_cnt *3 ) - 1 ));
                                $sheet->cells("J".$i.":J". ( $i + ( $v1->mosque_cnt *3 ) - 1 ), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });

                                $sheet ->setCellValue('K'.$i,$v1->amount);
                                $sheet->mergeCells("K".$i.":K". ( $i + ( $v1->mosque_cnt *3 ) - 1 ));
                                $sheet->cells("K".$i.":K". ( $i + ( $v1->mosque_cnt *3 ) - 1 ), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });

                                $y = $i;
                                foreach($v1->mosques as $k2 =>$v2 ) {

                                    $sheet ->setCellValue('L'.$y,$v2->location_name);
                                    $sheet->mergeCells("L".$y.":L". ( $y + 3 - 1));
                                    $sheet->cells("L".$y.":L". ( $y + 3 - 1 ), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });
                                    $sheet ->setCellValue('M'.$y,$v2->nominated);
                                    $sheet->mergeCells("M".$y.":M". ( $y + 3 - 1 ));
                                    $sheet->cells("M".$y.":M". ( $y + 3 - 1 ), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });
                                    $sheet ->setCellValue('N'.($y),trans('common::application.ratio'));
                                    $sheet ->setCellValue('N'.($y + 1),trans('common::application.quantity'));
                                    $sheet ->setCellValue('N'.($y + 2),trans('common::application.amount_'));
                                    $sheet ->setCellValue('O'.($y),$v1->ratio);
                                    $sheet ->setCellValue('O'.($y + 1),($v1->total));
                                    $sheet ->setCellValue('O'.($y + 2),($v1->amount));
                                    $sheet->cells('N'.($y).":N". ($y + 2), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $y = $y + 3;
                                }

                                $i = $i + ($v1->mosque_cnt *3) ;
                            }

                            $z = $z + ( $v->mosque_cnt *3 );
                        }

                    });

                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' =>true ,'download_token' =>$token]);
            }

            return response()->json(['status' =>false ,'msg' =>trans('common::application.no locations') , $organizations_ , $locations]);
        }

    }

    // ************************************************* //
    // result of candidate to project according save project rule;
    public function filter(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = Auth::user();
        $request = request();
        $project_id = $request->project_id;
        $project = Project::with('Repository')->findOrFail($project_id);
        $organization_id = $project->Repository->organization_id;
        $proj_quantity = $project->quantity - $project->organization_quantity;

        $orderby = [];
        $organizations=\DB::table('aid_projects_organizations')
            ->join('char_organizations', function($join) use ($organization_id)  {
                $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                $join->where(function ($q) use ($organization_id) {
                    $q->where('type', 1);
                    $q->where('parent_id', '=', $organization_id);
                });
            })
            ->where(function ($sQu) use ($project,$organization_id) {
                $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                $sQu->where('aid_projects_organizations.ratio', '!=', 0);
            })
            ->selectRaw("aid_projects_organizations.*, char_organizations.name")
            ->orderBy('ratio','desc')
                    ->get();
            

            $project_custom_ratio = 0;
            $found_custom_org = false;
            $found_project_org = false;
            $has_subtract_ratio = false ;
            $quantity_map=[];

            if($project->custom_organization && $project->custom_quantity > 0 ){
                $custom_org = Organization::where('id', $project->custom_organization)->first();
                $project->organization_container_share= $custom_org->container_share;

                $project_custom_quantity = $project->custom_quantity;
                $project_custom_ratio = ( ( $project_custom_quantity * 100 ) / $proj_quantity ) ;

                if($custom_org){
                    $original_ration = $custom_org->container_share;
                    if($project_custom_ratio > $original_ration){
                        $has_subtract_ratio = true ;
                    }
                }
            }

        foreach ($organizations as $k=>$v){

            if($has_subtract_ratio){
                if($v->organization_id == $project->custom_organization){
                   $v->quantity = $project->custom_quantity;
                   $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
                   $found_custom_org = true;
                }
            }
            
            if($v->organization_id == $project->organization_id){
               $v->quantity += $project->organization_quantity;
               $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
               $found_project_org = true;
            }
                   
            $quantity_map[]=['id'=>$v->organization_id , 'quantity'=> (float)$v->quantity];

            $sub = $v->organization_id;
            $v->childs= \DB::table('aid_projects_organizations')
                ->join('char_organizations', function($join) use ($sub)  {
                    $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                    $join->where(function ($q) use ($sub) {
                        $q->where('type', 1);
                        $q->where('parent_id', '=', $sub);

                    });
                })
                ->where(function ($sQu) use ($project) {
                    $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                    $sQu->where('aid_projects_organizations.ratio', '!=', 0);
                })
                ->selectRaw("aid_projects_organizations.*, char_organizations.name")
                ->orderBy('ratio','desc')->get();

            foreach ( $v->childs as $k2=>$v2){

                if($has_subtract_ratio){
                    if($v2->organization_id == $project->custom_organization){
                       $v2->quantity = $project->custom_quantity;
                       $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                       $found_custom_org = true;
                    }
                }
            
                if($v2->organization_id == $project->organization_id){
                    $v->quantity += $project->organization_quantity;
                    $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                    $found_project_org = true;
                 }
                               
                $quantity_map[]=['id'=>$v2->organization_id , 'quantity'=> (float)$v2->quantity];

            }
        }
        
        if(!$found_project_org){
           $quantity = $project->organization_quantity;
           $quantity_map[]=['id'=>$project->organization_id , 'quantity'=> (float)$quantity];
        }
        
        if(!$found_custom_org){
           $quantity = $project->custom_quantity;
           $quantity_map[]=['id'=>$project->custom_organization , 'quantity'=> (float)$quantity];
        }
 
        usort($quantity_map, function($a, $b) {
                return $a['quantity'] < $b['quantity'];
        });
            
        foreach ( $quantity_map as $k2=>$v2){
            $orderby[]=(int) $v2['id'];
        }

        $data = Nomination::filter($request->project_id,$project->repository_id,$orderby,$request->rules,$user->organization_id,true,$request->itemsCount);
        return response()->json(['data' => $data]);
    }

    // store the candidate to the project according save project rule;
    public function store(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $project_id = $request->project_id;
        $nomination_id = $request->nomination_id;

        $rules = $request->input('rules');
        $status = $request->input('status');

        $project = Project::with('Repository')->findOrFail($project_id);
        $entry = NominationRules::findOrFail($nomination_id);
        $level = $project->repository->level;
        $result = ProjectRule::saveForProject($nomination_id,$project_id, $rules);

        if($result){
            \Log\Model\Log::saveNewLog('PROJECT_RULE_CREATED',trans('aid-repository::application.He added new conditions to a project') . ' "'.$project->name. '" , ' .
                trans('aid-repository::application.nomination') .' : ' . $entry->label);

            if($level != 1 && $level != '1' ){
                $proj_quantity = $project->quantity - $project->organization_quantity;
                ProjectPerson::where(['project_id'=> $project_id,'nomination_id'=>$nomination_id])->delete();

                if($status == 2 || $status == '2'){
                    $user = Auth::user();
                    $UserOrg=$user->organization;
                    $level = $UserOrg->level;
                    $organization_id = $project->Repository->organization_id;             
                    $orderby = [];
                    $organizations=\DB::table('aid_projects_organizations')
                        ->join('char_organizations', function($join) use ($organization_id)  {
                            $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                            $join->where(function ($q) use ($organization_id) {
                                $q->where('type', 1);
                                $q->where('parent_id', '=', $organization_id);
                            });
                        })
                        ->where(function ($sQu) use ($project,$organization_id) {
                            $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                            $sQu->where('aid_projects_organizations.ratio', '!=', 0);
                        })
                        ->selectRaw("aid_projects_organizations.*, char_organizations.name")
                        ->orderBy('ratio','desc')
                                ->get();

                        $project_custom_ratio = 0;
                        $found_custom_org = false;
                        $found_project_org = false;
                        $has_subtract_ratio = false ;
                        $quantity_map=[];

                        if($project->custom_organization && $project->custom_quantity > 0 ){
                            $custom_org = Organization::where('id', $project->custom_organization)->first();
                            $project_custom_quantity = $project->custom_quantity;
                            $project_custom_ratio = ( ( $project_custom_quantity * 100 ) / $proj_quantity ) ;

                            if($custom_org){
                                $original_ration = $custom_org->container_share;
                                if($project_custom_ratio > $original_ration){
                                    $has_subtract_ratio = true ;
                                }
                            }

                        }


                        foreach ($organizations as $k=>$v){
                            if($has_subtract_ratio){
                                if($v->organization_id == $project->custom_organization){
                                   $v->quantity = $project->custom_quantity;
                                   $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
                                   $found_custom_org = true;
                                }
                            }

                            if($v->organization_id == $project->organization_id){
                               $v->quantity += $project->organization_quantity;
                               $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
                               $found_project_org = true;
                            }

                            $quantity_map[]=['id'=>$v->organization_id , 'quantity'=> (float)$v->quantity];

                            $sub = $v->organization_id;
                            $v->childs= \DB::table('aid_projects_organizations')
                                ->join('char_organizations', function($join) use ($sub)  {
                                    $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                                    $join->where(function ($q) use ($sub) {
                                        $q->where('type', 1);
                                        $q->where('parent_id', '=', $sub);

                                    });
                                })
                                ->where(function ($sQu) use ($project) {
                                    $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                                    $sQu->where('aid_projects_organizations.ratio', '!=', 0);
                                })
                                ->selectRaw("aid_projects_organizations.*, char_organizations.name")
                                ->orderBy('ratio','desc')->get();

                            foreach ( $v->childs as $k2=>$v2){

                                if($has_subtract_ratio){
                                    if($v2->organization_id == $project->custom_organization){
                                       $v2->quantity = $project->custom_quantity;
                                       $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                                       $found_custom_org = true;
                                    }
                                }

                                if($v2->organization_id == $project->organization_id){
                                    $v->quantity += $project->organization_quantity;
                                    $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                                    $found_project_org = true;
                                 }

                                $quantity_map[]=['id'=>$v2->organization_id , 'quantity'=> (float)$v2->quantity];

                            }
                        }

                        if(!$found_project_org){
                           $quantity = $project->organization_quantity;
                           $quantity_map[]=['id'=>$project->organization_id , 'quantity'=> (float)$quantity];
                        }

                    if(!$found_custom_org){
                       $quantity = $project->custom_quantity;
                       $quantity_map[]=['id'=>$project->custom_organization , 'quantity'=> (float)$quantity];
                    }


                    usort($quantity_map, function($a, $b) {
                            return $a['quantity'] < $b['quantity'];
                    });

                    foreach ( $quantity_map as $k2=>$v2){
                        $orderby[]=(int) $v2['id'];
                    }
                    $success=0;
                    $nominated=0;
                    $out_of_regions=0;
                    $not_case=0;
                    $blocked=0;
                    $restricted=0;
                    $restricted_=[];
                    $clears_candidates=[];

                    $candidates = Nomination::filter($project_id,$project->repository_id,$orderby,$rules,$user->organization_id,false,0);

                    $count = count($candidates);
                    $response = ['status' => false];

                    if (sizeof($candidates) > 0) {
                        foreach ($candidates as $key=>$value) {
                            $value = (array)$value;
                            $value['project_id'] =$project_id;
                            $find = ProjectPerson::where(['person_id' => $value['person_id'],
                                'project_id' => $project_id ])->first();
                            if(is_null($find)){
                                $passed = ProjectPerson::checkPersons($value['case_id']);
                                if($passed['status'] == true){
                                    $clears_candidates[] = ['id' => $value['person_id'], 'organization_id' => $value['organization_id']];
                                    $success++;
                                }else{

                                    if ($passed['reason'] == 'out_of_regions') {
                                        $out_of_regions++;
                                    }
                                    if ($passed['reason'] == 'blocked') {
                                        $blocked++;
                                    }

                                    if ($passed['reason'] == 'not_case') {
                                        $not_case++;
                                    }

                                    $restricted_[] = $passed;
                                    $restricted++;
                                }
                            }else{
                                $person=\Common\Model\Person::fetch(array('id' => $value['person_id'], 'action'  =>'show','full_name'=> true,'person'=> true));
                                $restricted_[]=['name' => $person->full_name,'id_card_number' =>$person->id_card_number,'reason'=>'nominated'];
                                $nominated++;
                            }
                        }

                        if(sizeof($clears_candidates) > 0 ){
                              foreach ($clears_candidates as $key=>$value) {
                                  $data = [
                                        'nomination_id' => $nomination_id,
                                        'project_id' => $project_id,
                                        'person_id' =>$value['id'],
                                        'nominated_organization_id' => $user->organization_id,
                                        'organization_id' => $value['organization_id'],
                                        'status' => ProjectPerson::STATUS_CANDIDATE,
                                    ];
                        
                                    //ProjectPerson::create($data);
                                    $model = new ProjectPerson($data); 
                                    $model->save();
                              }
//                            ProjectPerson::saveForProject($nomination_id,$project_id, $user->organization_id, $clears_candidates);
                            \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_CREATED',trans('aid-repository::application.Added new candidates to the project'). ' "'.$project->name. '" ');
                        }

                        $response['has_drop']=false;
                        foreach ($organizations as $k=>$org){
                            $org->child_nominated_count = 0 ;
                            $org->nominated_count = 0 ;
                            $org->has_drop = false ;
                            $org_id = $org->organization_id;

                            $total= \DB::select("select char_get_organizations_child_nominated_count ( ?, ? ) AS amount", [$org_id,$project->id]);
                            if($total){
                                $org->child_nominated_count =$total[0]->amount;
                            }

                            $total= \DB::select("select char_get_organization_nominated_count ( ?, ? ) AS amount",[$org_id,$project->id]);
                            if($total){
                                $org->nominated_count =$total[0]->amount;
                            }

                            if($org->nominated_count < $org->quantity){
                                $response['has_drop']=true;
                                $org->has_drop = true ;
                            }
                            foreach ( $org->childs as $k2=>$child){
                                $child->has_drop = false ;
                                $child->nominated_count = 0 ;

                                $total= \DB::select("select char_get_organization_nominated_count ( ?, ? ) AS amount",[$child->organization_id,$project->id]);
                                if($total){
                                    $child->nominated_count =$total[0]->amount;
                                };

                                if($child->nominated_count < $child->quantity){
                                    $response['has_drop']=true;
                                    $child->has_drop = true ;
                                }
                            }
                        }
                        $response['organizations']=$organizations;
                        $response['projects'] = $project ;
                        $response['lears_candidates'] = $clears_candidates ;

                        if ($success == $count) {
                            $response['status']=true;
                            $response['msg']= trans('aid-repository::application.The cases is successfully nomination');
                        }else if ($out_of_regions == $count) {
                            $response['status']=false;
                            $response["msg"] = trans('aid::application.The selected cases are not on connector locations of registered organization');
                        }else if ($blocked == $count) {
                            $response['status']=false;
                            $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                        }
                        else{
                            $response['status']=false;
                            $response['msg']= trans('aid-repository::application.The cases is not successfully nomination')
                                . ' ( '. trans('aid-repository::application.candidates count') .': ' .$count
                                .' ، '. trans('aid-repository::application.new nominate') .' : '. $success.' , '.
                                trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                trans('aid::application.out_of_org_regions') . ' : ' . $out_of_regions . ' , ' .
                                trans('aid-repository::application.previously inserted') . ' : '.$nominated .' )';


                        }
                        if(sizeof($restricted_) > 0  || $response['has_drop'] == true) {
                            $project->organizations = $organizations;
                            $projects []=$project;
                            $token = md5(uniqid());
                            \Excel::create('export_' . $token, function($excel) use($restricted_,$organizations,$response,$level,$projects) {
                                $excel->setTitle(trans('aid-repository::application.restricted_'));
                                $excel->setDescription(trans('aid-repository::application.restricted_'));
                                if(sizeof($restricted_) > 0) {
                                    $excel->sheet(trans('aid-repository::application.restricted_'), function($sheet) use($restricted_) {

                                        $sheet->setRightToLeft(true);
                                        $sheet->setAllBorders('thin');
                                        $sheet->setfitToWidth(true);
                                        $sheet->setHeight(1,40);
                                        $sheet->getDefaultStyle()->applyFromArray([
                                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                        ]);

                                        $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);
                                        $sheet->setCellValue('A1',trans('common::application.#'));
                                        $sheet->setCellValue('B1',trans('common::application.full_name'));
                                        $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                                        $sheet->setCellValue('D1',trans('common::application.reason'));

                                        $z= 2;
                                        foreach($restricted_ as $k=>$v )
                                        {
                                            $sheet ->setCellValue('A'.$z,$k+1);
                                            $sheet ->setCellValue('B'.$z,(is_null($v['name']) ||$v['name'] == ' ' ) ? '-' :$v['name']);
                                            $sheet ->setCellValue('C'.$z,(is_null($v['id_card_number']) ||$v['id_card_number'] == ' ' ) ? '-' :$v['id_card_number']);

                                            $translate ='';

                                            if(!(is_null($v['reason']) ||$v['reason'] == ' ' )){
                                                if ($v['reason'] == 'not_person') {
                                                    $translate = trans('aid::application.not_person');
                                                }

                                                if ($v['reason'] == 'blocked') {
                                                    $translate = trans('aid::application.blocked');
                                                }
                                                if ($v['reason'] == 'nominated') {
                                                    $translate = trans('aid::application.nominated');
                                                }

                                                if ($v['reason'] == 'out_of_regions') {
                                                    $translate = trans('aid::application.out_of_org_regions');
                                                }

                                                if ($v['reason'] == 'not_case') {
                                                    $translate = trans('sponsorship::application.not_case');
                                                }
                                                if ($v['reason'] == 'limited') {
                                                    $translate = trans('aid::application.max_limited_');
                                                }
                                            }

                                            $sheet ->setCellValue('D'.$z,(is_null($v['reason']) ||$v['reason'] == ' ' ) ? '-' :$translate);

                                            $z++;
                                        }
                                    });
                                }
                                if($response['has_drop'] == true){
                                    $excel->sheet(trans('aid-repository::application.organizations'), function($sheet) use($level,$organizations) {

                                        $sheet->setRightToLeft(true);
                                        $sheet->setAllBorders('solid');
                                        $sheet->setfitToWidth(true);
                                        $sheet->setHeight(1,40);
                                        $sheet->getDefaultStyle()->applyFromArray([
                                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                        ]);

                                        if($level != Organization::LEVEL_MASTER_CENTER) {
                                            $sheet->setWidth(array('A'=> 40, 'B'=>20,'C' => 20));
                                            $sheet->setCellValue('A1',trans('common::application.organization_name'));
                                            $sheet->setCellValue('B1',trans('common::application.nominated_no'));
                                            $sheet->setCellValue('C1',trans('common::application.required_no'));

                                            $z2_= 2;
                                            foreach($organizations as $k2_=>$v2_ ) {
                                                $sheet->setCellValue('A'.$z2_, $v2_->name);
                                                $sheet->setCellValue('B'.$z2_,$v2_->nominated_count);
                                                $sheet->setCellValue('C'.$z2_, $v2_->quantity);
                                                $z2_++;
                                            }
                                        }else{
                                            $sheet->setWidth(array('A'=> 40, 'B'=>20,'C' => 20, 'D' => 40, 'E' => 20 ,'F' => 20 ));

                                            $sheet->setCellValue('A1',trans('common::application.organization_name'));
                                            $sheet->setCellValue('B1',trans('common::application.nominated_no'));
                                            $sheet->setCellValue('C1',trans('common::application.required_no'));
                                            $map2 = ['A','B','C'];
                                            foreach($map2 as $key ) {
                                                $sheet->mergeCells($key .'1:' . $key . 2);
                                                $sheet->cells($key .'1:' . $key . 2, function($cells) {
                                                    $cells->setAlignment('center');
                                                    $cells->setValignment('center');
                                                    $cells->setFontWeight('bold');
                                                });
                                            }

                                            $sheet->setCellValue('D1',trans('common::application.sub_organization_data'));
                                            $sheet->mergeCells("D1:F1");
                                            $sheet->cells("D1:F2", function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });
                                            $sheet->setCellValue('D2',trans('common::application.organization_name'));
                                            $sheet->setCellValue('E2',trans('common::application.nominated_no'));
                                            $sheet->setCellValue('F2',trans('common::application.required_no'));

                                            $z2_=3;
                                            foreach($organizations as $k2_=>$v2_ ) {
                                                $descendants_count = sizeof($v2_->childs);
                                                if(sizeof($v2_->childs) > 0 ){
                                                    $descendants_count = $descendants_count;
                                                    $sheet->setCellValue('A'.$z2_,$v2_->name);
                                                    $sheet->mergeCells("A".($z2_).":A".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("A".($z2_).":A".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('B'.$z2_,$v2_->child_nominated_count);
                                                    $sheet->mergeCells("B".($z2_).":B".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("B".($z2_).":B".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('C'.$z2_,$v2_->quantity);
                                                    $sheet->mergeCells("C".($z2_).":C".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("C".($z2_).":C".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    foreach($v2_->childs as $k1=>$v1) {
                                                        $sheet->setCellValue('D'.($z2_ + $k1), $v1->name);
                                                        $sheet->setCellValue('E'.($z2_ + $k1), $v1->nominated_count);
                                                        $sheet->setCellValue('F'.($z2_ + $k1), $v1->quantity);
                                                    }


                                                    $z2_ = ($z2_ + $descendants_count) ;

                                                }
                                                else{
                                                    $sheet->setCellValue('A'.$z2_,$v2_->name);
                                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                                        $sheet->setCellValue('B'.$z2_,$v2_->child_nominated_count);
                                                    }else{
                                                        $sheet->setCellValue('B'.$z2_,$v2_->nominated_count);
                                                    }
                                                    $sheet->setCellValue('C'.$z2_,$v2_->quantity);
                                                    $sheet->setCellValue('D'.$z2_,'-');
                                                    $sheet->mergeCells("D".($z2_).":F".$z2_);
                                                    $z2_++;
                                                }
                                            }
                                        }

                                    });
                                }
                            })->store('xlsx', storage_path('tmp/'));
                            $response['download_token']=$token;
                        }

                        if($response['has_drop'] == true){
                            $response['msg'] .= trans('aid-repository::application.However there are associations whose number of candidates according to the entered conditions did not reach the number required to be nominated by the organization');
                        }

                        return response()->json($response);
                    }

                    return response()->json(['status'=>true , 'msg' => trans('aid-repository::application.Conditions successfully saved but no candidates')]);
                }
            }

            return response()->json(['status'=>true , 'msg' =>trans('aid-repository::application.Terms successfully saved')]);
        }

        return response()->json(['status'=>false, 'msg' =>trans('aid-repository::application.There was a bug during the save')]);

    }

    // save project nominations rule group
    public function saveNomination(Request $request){

        $user = \Auth::user();
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $label =strip_tags($request->label);
        $project_id = $request->project_id ;
        $founded =NominationRules::where(['label'=>$label , 'project_id'=>$project_id])->first();

        if(!isset($request->curId)) {

            if(!is_null($founded)){
                return response()->json(['status'=>false,'msg' =>  trans('common::application.rule label must be unique in project')]);
            }

            $entry  = new NominationRules();
            $entry->created_at =date('Y-m-d H:i:s');
            $entry->user_id =$user->id;
            $entry->project_id =$project_id;

        }else {

            if(!is_null($founded)){
                if($label->id != $request->curId){
                    return response()->json(['status'=>false,'msg' =>  trans('common::application.rule label must be unique in project')]);
                }
            }
            $entry = NominationRules::findOrFail($request->curId);
        }

        $entry->label =$request->label;

        if($entry->save()){
            return response()->json(['status'=>true,'entry'=> NominationRules::with(['user'])->findOrFail($entry->id)]);
        }
        return response()->json(['status'=>false,'msg' =>  trans('common::application.save not successful')]);
    }

    // project nominations list
    public function group($id){
        $project = Project::with(['Repository','rules','rules.user'])->findOrFail($id);

        $user = \Auth::user();
        $project->is_mine = 0;
        if($project->Repository->organization_id == $user->organization_id){
            $project->is_mine = 1;
        }
        return response()->json($project);
    }

    // project nominations list
    public function groups($id){
        $groups = NominationRules::where('project_id',$id)->get();
        return response()->json($groups);
    }

    // return nomination rule
    public function rule(Request $request)
    {
        $rules = [];
        $nomination_id = $request->nomination_id;

        $nomination = NominationRules::with(['project','project.Repository'])->findOrFail($nomination_id);

        $user = \Auth::user();
        $nomination->project->is_mine = 0;
        if($nomination->project->repository->organization_id == $user->organization_id){
            $nomination->project->is_mine = 1;
        }

        $project_id = $nomination->project_id;
        $strict = $request->strict;

        $projectRules = [];
        $entries = ProjectRule::where(['project_id'=>$project_id ,'nomination_id'=>$nomination_id])->get();
        foreach ($entries as $entry) {
            if ($entry->rule_value) {
                $projectRules[$entry->rule_name] = unserialize($entry->rule_value);
            }
        }

        $pass = true;
        if($request->strict){
            if(sizeof($projectRules) == 0){
                $pass = false;
            }
        }

        if($pass){
            foreach (Nomination::getRules($project_id,$nomination->project->repository_id) as $key => $rule) {
                if (isset($projectRules[$key])) {
                    if ($rule['type'] == 'range') {
                        $extra = $projectRules[$key];
                    } else {
                        $rule['placeholder']='';
                        $extra = [
                            'value' => $projectRules[$key],
                        ];
                    }
                } else {
                    $rule['placeholder']='';
                    $extra = [];
                }
                $options = isset($rule['options'])? is_callable($rule['options'])? call_user_func($rule['options']) : $rule['options'] : [];
                if ($strict) {
                    if (isset($extra['value']) && is_array($extra['value'])) {
                        foreach ($options as $k => $v) {
                            if (!in_array($v['id'], $extra['value'])) {
                                unset($options[$k]);
                            }
                        }
                    }
                }
                $rules[] = array_merge([
                    'name' => $key,
                    'placeholder' => $rule['label'],
                    'label' => $rule['label'],
                    'type' => $rule['type'],
                    'options' => $options,
                ], $extra);
            }
            return response()->json(['status' => true , 'rules' => $rules,'nomination'=>$nomination]);
        }

        return response()->json(['status' => false ]);

    }

    // empty nomination from old candidate
    public function rest_candidates(Request $request, $id)
    {
        
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $entry = NominationRules::findOrFail($id);
        $project = Project::findOrFail($entry->project_id);

        $this->authorize('deleteCandidates', $project);

        if ($project->status == Project::STATUS_CLOSED ||
            $project->status == Project::STATUS_EXECUTION ) {
            return response()->json(['error' => trans('aid-repository::application.You cannot delete people from a closed or approved project for execution')], 422);
        }
        if( ProjectPerson::where('nomination_id',$id)->count() > 0 ){
            if( ProjectPerson::where('nomination_id',$id)->delete()) {
                \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_DELETED',trans('aid-repository::application.empties the project from the candidates') . ' "'.$project->name. '" , ' .
                    trans('aid-repository::application.nomination') .' : ' . $entry->label);
                return response()->json(['status' =>'success' ,'msg'=> trans('aid-repository::application.The candidates were successfully emptied from the project')]);
            }
            return response()->json(['status' =>'error' ,'msg' => trans('aid-repository::application.occurred during the operation')]);
        }

        return response()->json(['status' =>'error' ,'msg' => trans('aid-repository::application.There are no candidates saved within the project to empty')]);

    }

    // delete project nominations rule group
    public function deleteNomination(Request $request,$id){

        $entry = NominationRules::findOrFail($id);
        ProjectRule::where('nomination_id',$id)->delete();
        ProjectPerson::where('nomination_id',$id)->delete();

        if($entry->delete()){
            return response()->json(['status'=>true]);
        }
        return response()->json(['status'=>false]);
    }

    // candidate to project using check group or excel file
    public function candidates(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $nomination_id = $request->input('nomination_id');
        $project_id = $request->input('project_id');
        $project = Project::with('Repository')->findOrFail($project_id);

        if ($project->status != Project::STATUS_CANDIDATES ) {
            return response()->json(['error' => trans('aid-repository::application.Nomination for a project approved for implementation, closed or draft cannot be submitted')], 422);
        }

        $user = Auth::user();
        $success=0;
        $nominated=0;
        $restricted=0;
        $restricted_=[];
        $out_of_regions=0;
        $not_case=0;
        $blocked=0;
        $clears_candidates=[];

        if($request->type == 1) {
            $rules = $request->rules;
            ProjectPerson::where(['project_id'=> $project_id])->delete();
            $UserOrg=$user->organization;
            $level = $UserOrg->level;
            $organization_id = $project->Repository->organization_id;
 
            
                $project = Project::with('Repository')->findOrFail($project_id);
        $organization_id = $project->Repository->organization_id;

        $orderby = [];
        $organizations=\DB::table('aid_projects_organizations')
            ->join('char_organizations', function($join) use ($organization_id)  {
                $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                $join->where(function ($q) use ($organization_id) {
                    $q->where('type', 1);
                    $q->where('parent_id', '=', $organization_id);
                });
            })
            ->where(function ($sQu) use ($project,$organization_id) {
                $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                $sQu->where('aid_projects_organizations.ratio', '!=', 0);
            })
            ->selectRaw("aid_projects_organizations.*, char_organizations.name")
            ->orderBy('ratio','desc')
                    ->get();
            

            $project_custom_ratio = 0;
            $found_custom_org = false;
            $found_project_org = false;
            $has_subtract_ratio = false ;
            $quantity_map=[];

            if($project->custom_ratio){
                if(!is_null($project->custom_ratio) && $project->custom_ratio != 0 && $project->custom_ratio != '0'){
                   $project_custom_ratio = $project->custom_ratio;
                   if($project_custom_ratio > 0){
                      $custom_org = Organization::where('id', $project->custom_organization)->first();
                      if($custom_org){
                         $original_ration = $custom_org->container_share;
                         if($project_custom_ratio > $original_ration){
                            $has_subtract_ratio = true ;
                        }
                     }
                  }
               }
            }

        foreach ($organizations as $k=>$v){

            if($has_subtract_ratio){
                if($v->organization_id == $project->custom_organization){
                   $v->quantity = $project->custom_quantity;
                   $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
                   $found_custom_org = true;
                }
            }
            
            if($v->organization_id == $project->organization_id){
               $v->quantity += $project->organization_quantity;
               $v->amount = round(  $v->quantity * $project->amount * $project->exchange_rate,1);
               $found_project_org = true;
            }
                   
            $quantity_map[]=['id'=>$v->organization_id , 'quantity'=> (float)$v->quantity];

            $sub = $v->organization_id;
            $v->childs= \DB::table('aid_projects_organizations')
                ->join('char_organizations', function($join) use ($sub)  {
                    $join->on('aid_projects_organizations.organization_id', '=', 'char_organizations.id');
                    $join->where(function ($q) use ($sub) {
                        $q->where('type', 1);
                        $q->where('parent_id', '=', $sub);

                    });
                })
                ->where(function ($sQu) use ($project) {
                    $sQu->where('aid_projects_organizations.project_id', '=', $project->id);
                    $sQu->where('aid_projects_organizations.ratio', '!=', 0);
                })
                ->selectRaw("aid_projects_organizations.*, char_organizations.name")
                ->orderBy('ratio','desc')->get();

            foreach ( $v->childs as $k2=>$v2){

                if($has_subtract_ratio){
                    if($v2->organization_id == $project->custom_organization){
                       $v2->quantity = $project->custom_quantity;
                       $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                       $found_custom_org = true;
                    }
                }
            
                if($v2->organization_id == $project->organization_id){
                    $v->quantity += $project->organization_quantity;
                    $v2->amount = round(  $v2->quantity * $project->amount * $project->exchange_rate,1);
                    $found_project_org = true;
                 }
                               
                $quantity_map[]=['id'=>$v2->organization_id , 'quantity'=> (float)$v2->quantity];

            }
        }
        
        if(!$found_project_org){
           $quantity = $project->organization_quantity;
           $quantity_map[]=['id'=>$project->organization_id , 'quantity'=> (float)$quantity];
        }
        
        if(!$found_custom_org){
           $quantity = $project->quantity * $project->custom_quantity;
           $quantity_map[]=['id'=>$project->custom_organization , 'quantity'=> (float)$quantity];
        }
        
 
        usort($quantity_map, function($a, $b) {
                return $a['quantity'] < $b['quantity'];
        });
            
        foreach ( $quantity_map as $k2=>$v2){
            $orderby[]=(int) $v2['id'];
        }

    
            
            
            $success=0;
            $nominated=0;
            $restricted=0;
            $blocked=0;
            $restricted_=[];
            $clears_candidates=[];

            $candidates = Nomination::filter($project_id,$project->repository_id,$orderby,$rules,$user->organization_id,false,0);

            $count = count($candidates);
            $response = ['status' => false];
            if (sizeof($candidates) > 0) {
                foreach ($candidates as $key=>$value) {
                    $value = (array)$value;
                    $value['project_id'] =$project_id;
                    $find = ProjectPerson::where(['person_id' => $value['person_id'], 'project_id' => $project_id])->first();
                    if(is_null($find)){
                        $passed = ProjectPerson::checkPersons($value['case_id']);
                        if($passed['status'] == true){
                            $clears_candidates[] = ['id' => $value['person_id'], 'organization_id' => $value['organization_id']];
                            $success++;
                        }else{
                            if ($passed['reason'] == 'out_of_regions') {
                                $out_of_regions++;
                            }
                            if ($passed['reason'] == 'blocked') {
                                $blocked++;
                            }

                            if ($passed['reason'] == 'not_case') {
                                $not_case++;
                            }

                            $restricted_[] = $passed;
                            $restricted++;
                        }
                    }else{
                        $person=\Common\Model\Person::fetch(array('id' => $value['person_id'], 'action'  =>'show','full_name'=> true,'person'=> true));
                        $restricted_[]=['name' => $person->full_name,'id_card_number' =>$person->id_card_number,'reason'=>'nominated'];
                        $nominated++;
                    }
                }

                if(sizeof($clears_candidates) > 0 ){
//                    ProjectPerson::saveForProject($nomination_id,$project_id, $user->organization_id, $clears_candidates);
                    
                              foreach ($clears_candidates as $key=>$value) {
                                  $data = [
                                        'nomination_id' => $nomination_id,
                                        'project_id' => $project_id,
                                        'person_id' =>$value['id'],
                                        'nominated_organization_id' => $user->organization_id,
                                        'organization_id' => $value['organization_id'],
                                        'status' => ProjectPerson::STATUS_CANDIDATE,
                                    ];
                        
                                    //ProjectPerson::create($data);
                                    $model = new ProjectPerson($data); 
                                    $model->save();
                              }
                    
                    \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_CREATED', trans('aid-repository::application.Added new candidates to the project') . ' "'.$project->name. '" ');
                }

                $response['has_drop']=false;
                foreach ($organizations as $k=>$org){
                    $org->child_nominated_count = 0 ;
                    $org->nominated_count = 0 ;
                    $org->has_drop = false ;
                    $org_id = $org->organization_id;

                    $total= \DB::select("select char_get_organizations_child_nominated_count ( ?, ? ) AS amount", [$org_id,$project->id]);
                    if($total){
                        $org->child_nominated_count =$total[0]->amount;
                    }

                    $total= \DB::select("select char_get_organization_nominated_count ( ?, ? ) AS amount",[$org_id,$project->id]);
                    if($total){
                        $org->nominated_count =$total[0]->amount;
                    }

                    if($org->nominated_count < $org->quantity){
                        $response['has_drop']=true;
                        $org->has_drop = true ;
                    }
                    foreach ( $org->childs as $k2=>$child){
                        $child->has_drop = false ;
                        $child->nominated_count = 0 ;

                        $total= \DB::select("select char_get_organization_nominated_count ( ?, ? ) AS amount",[$child->organization_id,$project->id]);
                        if($total){
                            $child->nominated_count =$total[0]->amount;
                        };

                        if($child->nominated_count < $child->quantity){
                            $response['has_drop']=true;
                            $child->has_drop = true ;
                        }
                    }
                }
                $response['organizations']=$organizations;
                $response['projects'] = $project ;

                if ($success == $count) {
                    $response['status']=true;
                    $response['msg']= trans('aid-repository::application.The cases is successfully nomination');
                }
                else if ($out_of_regions == $count) {
                    $response['status']=false;
                    $response["msg"] = trans('aid::application.The selected cases are not on connector locations of registered organization');
                }else if ($blocked == $count) {
                    $response['status']=false;
                    $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                }
                else{
                    $response['status']=false;
                    $response['msg']= trans('aid-repository::application.The cases is not successfully nomination')
                        . ' ( '. trans('aid-repository::application.candidates count') .': ' .$count
                        .' ، '. trans('aid-repository::application.new nominate') .' : '. $success.' , '.
                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                        trans('aid::application.out_of_org_regions') . ' : ' . $out_of_regions . ' , ' .
                        trans('aid-repository::application.previously inserted') . ' : '.$nominated .' )';


                }

                if(sizeof($restricted_) > 0 || $response['has_drop'] == true) {
                    $project->organizations = $organizations;
                    $projects []=$project;
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($restricted_,$organizations,$response,$level,$projects) {
                        $excel->setTitle(trans('aid-repository::application.restricted_'));
                        $excel->setDescription(trans('aid-repository::application.restricted_'));
                        if(sizeof($restricted_) > 0) {
                            $excel->sheet(trans('aid-repository::application.restricted_'), function($sheet) use($restricted_) {

                                $sheet->setRightToLeft(true);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setHeight(1,40);
                                $sheet->getDefaultStyle()->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                ]);

                                $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);
                                $sheet->setCellValue('A1',trans('common::application.#'));
                                $sheet->setCellValue('B1',trans('common::application.full_name'));
                                $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                                $sheet->setCellValue('D1',trans('common::application.reason'));

                                $z= 2;
                                foreach($restricted_ as $k=>$v )
                                {
                                    $sheet ->setCellValue('A'.$z,$k+1);
                                    $sheet ->setCellValue('B'.$z,(is_null($v['name']) ||$v['name'] == ' ' ) ? '-' :$v['name']);
                                    $sheet ->setCellValue('C'.$z,(is_null($v['id_card_number']) ||$v['id_card_number'] == ' ' ) ? '-' :$v['id_card_number']);

                                    $translate ='';

                                    if(!(is_null($v['reason']) ||$v['reason'] == ' ' )){
                                        if ($v['reason'] == 'not_person') {
                                            $translate = trans('aid::application.not_person');
                                        }

                                        if ($v['reason'] == 'blocked') {
                                            $translate = trans('aid::application.blocked');
                                        }
                                        if ($v['reason'] == 'nominated') {
                                            $translate = trans('aid::application.nominated');
                                        }

                                        if ($v['reason'] == 'out_of_regions') {
                                            $translate = trans('aid::application.out_of_org_regions');
                                        }

                                        if ($v['reason'] == 'not_case') {
                                            $translate = trans('sponsorship::application.not_case');
                                        }
                                        if ($v['reason'] == 'limited') {
                                            $translate = trans('aid::application.max_limited_');
                                        }
                                    }

                                    $sheet ->setCellValue('D'.$z,(is_null($v['reason']) ||$v['reason'] == ' ' ) ? '-' :$translate);

                                    $z++;
                                }
                            });
                        }
                        if($response['has_drop'] == true){
                            $excel->sheet(trans('aid-repository::application.organizations'), function($sheet) use($level,$organizations) {

                                $sheet->setRightToLeft(true);
                                $sheet->setAllBorders('solid');
                                $sheet->setfitToWidth(true);
                                $sheet->setHeight(1,40);
                                $sheet->getDefaultStyle()->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                ]);

                                if($level != Organization::LEVEL_MASTER_CENTER) {
                                    $sheet->setWidth(array('A'=> 40, 'B'=>20,'C' => 20));
                                    $sheet->setCellValue('A1',trans('common::application.organization_name'));
                                    $sheet->setCellValue('B1',trans('common::application.nominated_no'));
                                    $sheet->setCellValue('C1',trans('common::application.required_no'));

                                    $z2_= 2;
                                    foreach($organizations as $k2_=>$v2_ ) {
                                        $sheet->setCellValue('A'.($z2_ + $k2_), $v2_->name);
                                        $sheet->setCellValue('B'.($z2_ + $k2_), $v2_->nominated_count);
                                        $sheet->setCellValue('C'.($z2_ + $k2_), $v2_->quantity);
                                        $z2_++;
                                    }
                                }else{
                                    $sheet->setWidth(array('A'=> 40, 'B'=>20,'C' => 20, 'D' => 40, 'E' => 20 ,'F' => 20 ));

                                    $sheet->setCellValue('A1',trans('common::application.organization_name'));
                                    $sheet->setCellValue('B1',trans('common::application.nominated_no'));
                                    $sheet->setCellValue('C1',trans('common::application.required_no'));
                                    $map2 = ['A','B','C'];
                                    foreach($map2 as $key ) {
                                        $sheet->mergeCells($key .'1:' . $key . 2);
                                        $sheet->cells($key .'1:' . $key . 2, function($cells) {
                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                            $cells->setFontWeight('bold');
                                        });
                                    }

                                    $sheet->setCellValue('D1',trans('common::application.sub_organization_data'));
                                    $sheet->mergeCells("D1:F1");
                                    $sheet->cells("D1:F2", function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });
                                    $sheet->setCellValue('D2',trans('common::application.organization_name'));
                                    $sheet->setCellValue('E2',trans('common::application.nominated_no'));
                                    $sheet->setCellValue('F2',trans('common::application.required_no'));

                                    $z2_=3;
                                    foreach($organizations as $k2_=>$v2_ ) {
                                        $descendants_count = sizeof($v2_->childs);
                                        if(sizeof($v2_->childs) > 0 ){
                                            $descendants_count = $descendants_count;
                                            $sheet->setCellValue('A'.$z2_,$v2_->name);
                                            $sheet->mergeCells("A".($z2_).":A".((($z2_ +$descendants_count) - 1)));
                                            $sheet->cells("A".($z2_).":A".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                            });

                                            $sheet ->setCellValue('B'.$z2_,$v2_->child_nominated_count);
                                            $sheet->mergeCells("B".($z2_).":B".((($z2_ +$descendants_count) - 1)));
                                            $sheet->cells("B".($z2_).":B".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                            });

                                            $sheet ->setCellValue('C'.$z2_,$v2_->quantity);
                                            $sheet->mergeCells("C".($z2_).":C".((($z2_ +$descendants_count) - 1)));
                                            $sheet->cells("C".($z2_).":C".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                            });

                                            foreach($v2_->childs as $k1=>$v1) {
                                                $sheet->setCellValue('D'.($z2_ + $k1), $v1->name);
                                                $sheet->setCellValue('E'.($z2_ + $k1), $v1->nominated_count);
                                                $sheet->setCellValue('F'.($z2_ + $k1), $v1->quantity);
                                            }


                                            $z2_ = ($z2_ + $descendants_count) ;

                                        }
                                        else{
                                            $sheet->setCellValue('A'.$z2_,$v2_->name);
                                            $sheet->setCellValue('B'.$z2_,$v2_->child_nominated_count);
                                            $sheet->setCellValue('C'.$z2_,$v2_->quantity);
                                            $sheet->setCellValue('D'.$z2_,'-');
                                            $sheet->mergeCells("D".($z2_).":F".$z2_);
                                            $z2_++;
                                        }
                                    }
                                }

                            });
                        }
                    })->store('xlsx', storage_path('tmp/'));
                    $response['download_token']=$token;
                }

                if($response['has_drop'] == true){
                    $response['msg'] .= trans('aid-repository::application.However, there are associations whose number of candidates according to the entered conditions did not reach the number required to be nominated by the organization') ;
                }

                return response()->json($response);
            }
            return response()->json(['status'=>true , 'msg' => trans('aid-repository::application.There are no candidates')]);

        }
        else{
            $candidates = $request->input('candidates');
            $count = count($candidates);
            foreach ($candidates as $key=>$value) {
                $find = ProjectPerson::where(['project_id' => $project_id,'person_id' => $value['person_id']])->first();
                if(is_null($find)){
                    $passed = ProjectPerson::checkPersons($value['case_id']);

                    if($passed['status'] == true){
                        $clears_candidates[] = ['id' => $value['person_id'], 'organization_id' => $value['organization_id']];
                        $success++;
                    }else{

                        if ($passed['reason'] == 'out_of_regions') {
                            $out_of_regions++;
                        }
                        if ($passed['reason'] == 'blocked') {
                            $blocked++;
                        }

                        if ($passed['reason'] == 'not_case') {
                            $not_case++;
                        }

                        $restricted_[] = $passed;
                        $restricted++;
                    }
                }else{
                    $person=\Common\Model\Person::fetch(array('id' => $value['person_id'], 'action'  =>'show','full_name'=> true,'person'=> true));
                    $restricted_[]=['name' => $person->full_name,'id_card_number' =>$person->id_card_number,'reason'=>'nominated'];
                    $nominated++;
                }
            }

            if(sizeof($clears_candidates) > 0 ){
//                ProjectPerson::saveForProject($nomination_id,$project_id, $user->organization_id, $clears_candidates);
                              foreach ($clears_candidates as $key=>$value) {
                                  $data = [
                                        'nomination_id' => $nomination_id,
                                        'project_id' => $project_id,
                                        'person_id' =>$value['id'],
                                        'nominated_organization_id' => $user->organization_id,
                                        'organization_id' => $value['organization_id'],
                                        'status' => ProjectPerson::STATUS_CANDIDATE,
                                    ];
                        
                                    //ProjectPerson::create($data);
                                    $model = new ProjectPerson($data); 
                                    $model->save();
                              }

                \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_CREATED',trans('aid-repository::application.added new candidates to the project') . ' "'.$project->name. '" ');
            }

            if ($success == $count) {
                $response['status']=true;
                $response['msg']= trans('aid-repository::application.The cases is successfully nomination');
            }
            else if ($out_of_regions == $count) {
                $response['status']=false;
                $response["msg"] = trans('aid::application.The selected cases are not on connector locations of registered organization');
            }else if ($blocked == $count) {
                $response['status']=false;
                $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
            }
            else{
                $response['status']=false;
                $response['msg']= trans('aid-repository::application.The cases is not successfully nomination')
                    . ' ( '. trans('aid-repository::application.candidates count') .': ' .$count
                    .' ، '. trans('aid-repository::application.new nominate') .' : '. $success.' , '.
                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                    trans('aid::application.out_of_org_regions') . ' : ' . $out_of_regions . ' , ' .
                    trans('aid-repository::application.previously inserted') . ' : '.$nominated .' )';
            }

            if(sizeof($restricted_) > 0) {
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($restricted_) {
                    $excel->setTitle(trans('aid-repository::application.restricted_'));
                    $excel->setDescription(trans('aid-repository::application.restricted_'));
                    $excel->sheet(trans('aid-repository::application.restricted_'), function($sheet) use($restricted_) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.full_name'));
                        $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('D1',trans('common::application.reason'));

                        $z= 2;
                        foreach($restricted_ as $k=>$v )
                        {

                            $sheet ->setCellValue('A'.$z,$k+1);
                            $sheet ->setCellValue('B'.$z,(is_null($v['name']) ||$v['name'] == ' ' ) ? '-' :$v['name']);
                            $sheet ->setCellValue('C'.$z,(is_null($v['id_card_number']) ||$v['id_card_number'] == ' ' ) ? '-' :$v['id_card_number']);

                            $translate ='';

                            if(!(is_null($v['reason']) ||$v['reason'] == ' ' )){
                                if ($v['reason'] == 'not_person') {
                                    $translate = trans('aid::application.not_person');
                                }

                                if ($v['reason'] == 'blocked') {
                                    $translate = trans('aid::application.blocked');
                                }
                                if ($v['reason'] == 'nominated') {
                                    $translate = trans('aid::application.nominated');
                                }

                                if ($v['reason'] == 'out_of_regions') {
                                    $translate = trans('aid::application.out_of_org_regions');
                                }

                                if ($v['reason'] == 'not_case') {
                                    $translate = trans('sponsorship::application.not_case');
                                }
                                if ($v['reason'] == 'limited') {
                                    $translate = trans('aid::application.max_limited_');
                                }
                            }

                            $sheet ->setCellValue('D'.$z,(is_null($v['reason']) ||$v['reason'] == ' ' ) ? '-' :$translate);

                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));
                $response['download_token']=$token;
            }

            return response()->json($response);
        }
    }

    public function candidates_(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        
        $project_id = $request->input('project_id');
        $nomination_id = $request->input('nomination_id');
        $project = Project::findOrFail($project_id);

        if ($project->status != Project::STATUS_CANDIDATES ) {
            return response()->json(['error' => trans('aid-repository::application.Nomination for a project approved for implementation, closed or draft cannot be submitted')], 422);
        }

        $user = Auth::user();

        $Org=\Organization\Model\Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        $Options=['project_id' => $project_id];

        if($request->get('sub') && !$master){
            $Options['organization_id']=$user->organization_id;
            $organizations = ProjectOrganization::getOrganizations($project_id,$user->organization_id,$project->organization_id,true);
        }else{
            $organizations = ProjectOrganization::getOrganizations($project_id,$user->organization_id,$project->organization_id,false);
        }

        $projectCandidates = ProjectPerson::allWithOptions($Options);

        $old=[];
        foreach ($projectCandidates as $entry) {
            if (!in_array($entry->person_id, $old)) {
                $old[]=$entry->person_id;
            }
        }

        $old_candidates=sizeof($projectCandidates);
        $max_candidates = $project->quantity;
        $extra_quantity=($project->organization_quantity);

        $candidates = $request->input('candidates');
        $count = count($candidates);
        $success=0;
        $clears_candidates=[];
        $person=[];
        $nominated=[];
        $duplicated=[];
        $restricted=[];
        $project_restricted=[];

        foreach ($organizations as $k=>$v) {

            $org_success=0;
            $org_person=[];
            $org_nominated=[];
            $org_duplicated=[];
            $org_restricted=[];
            $org_project_restricted=[];
            $candidates_cnt=0;

            if($v->id == $project->organization_id){
                $v->max_count=round($v->max_count+$extra_quantity);
            }

            foreach ($candidates as $key=>$value) {
                if ($value['organization_id'] == $v->id) {
                    if (!in_array($value['id'], $org_person)) {
                        $org_person[] = $value['id'];
                    }
                    if (($success + $old_candidates) < $max_candidates) {
                        if (!in_array($value['id'], $old)) {
                            if (($candidates_cnt + $v->count) < $v->max_count) {
                                if (!in_array($value['id'], $person)) {
                                    $person[] = $value['id'];
                                    $clears_candidates[] = ['id' => $value['id'], 'organization_id' => $value['organization_id']];
                                    $candidates_cnt++;
                                    $success++;
                                    $org_success++;
                                } else {
//                                   if (!in_array($value['id'], $org_duplicated)) {
                                    $org_duplicated[] = $value['id'];
//                                   }

//                                    if (!in_array($value['id'], $duplicated)) {
                                    $duplicated[] = $value['id'];
//                                    }
                                }
                            }
                            else{
//                                if (!in_array($value['id'], $org_restricted)) {
                                $org_restricted[]= $value['id'];
//                                }

//                                if (!in_array($value['id'], $restricted)) {
                                $restricted[]= $value['id'];
//                                }
                            }
                        }
                        else {
//                            if (!in_array($value['id'], $org_nominated)) {
                            $org_nominated[] = $value['id'];
//                            }
//                            if (!in_array($value['id'], $nominated)) {
                            $nominated[] = $value['id'];
//                            }
                        }
                    }else{

//                        if (!in_array($value['id'], $org_person) && !in_array($value['id'], $org_restricted) && !in_array($value['id'], $old) &&
//                            !in_array($value['id'], $org_nominated) && !in_array($value['id'], $org_duplicated) ) {
//                            && !in_array($value['id'], $project_restricted)
                        $org_project_restricted[] = $value['id'];
//                        }

//                            if (!in_array($value['id'], $person) && !in_array($value['id'], $restricted) && !in_array($value['id'], $old) &&
//                                !in_array($value['id'], $nominated) && !in_array($value['id'], $duplicated) && !in_array($value['id'], $project_restricted)) {
                        $project_restricted[] = $value['id'];
//                            }
                    }
                }
            }

            $v->nominated=sizeof($org_nominated);
            $v->duplicated=sizeof($org_duplicated);
            $v->candidates=sizeof($org_person);
            $v->success=$org_success;
            $v->restricted=sizeof($org_restricted);
            $v->project_restricted=sizeof($org_project_restricted);
        }

        if ($success > $max_candidates) {
            return response()->json(['error' => trans('aid-repository::application.The candidates is greater than the specified for this project')], 422);
        }

        if(sizeof($clears_candidates) > 0){
                                          foreach ($clears_candidates as $key=>$value) {
                                  $data = [
                                        'nomination_id' => $nomination_id,
                                        'project_id' => $project_id,
                                        'person_id' =>$value['id'],
                                        'nominated_organization_id' => $user->organization_id,
                                        'organization_id' => $value['organization_id'],
                                        'status' => ProjectPerson::STATUS_CANDIDATE,
                                    ];
                        
                                    //ProjectPerson::create($data);
                                    $model = new ProjectPerson($data); 
                                    $model->save();
                              }
            
//        ProjectPerson::saveForProject($nomination_id,$project_id, $user->organization_id, $clears_candidates);
        \Log\Model\Log::saveNewLog('PROJECT_CANDIDATE_CREATED',trans('aid-repository::application.Added new candidates to the project') . ' "'.$project->name. '" ');

        }
        $response['organization']=$organizations;

        if ($success == $count) {
            $response['status']=true;
        }else{
            $response['status']=false;
        }

        $response['msg']= trans('aid-repository::application.The cases is not successfully nomination')
            . ' ( '. trans('aid-repository::application.candidates count') .': ' .$count
            .' ، '. trans('aid-repository::application.new nominate') .' : '. $success.' , '.
            trans('aid-repository::application.policy restricted') .' : '. sizeof($restricted) .' , '.
            trans('aid-repository::application.project restricted') .' : '. sizeof($project_restricted) .' , '.
            trans('aid-repository::application.duplicated') .' : '. sizeof($duplicated) .' , '.
            trans('aid-repository::application.previously inserted') . ' : '.sizeof($nominated) .' )';


        if(!($request->get('sub') && !$master)) {
            $response['candidates_cnt']=$count;
            $response['prev_candidates_cnt']=$old_candidates;
            $response['success_cnt']=$success;
            $response['nominated']=sizeof($nominated);
            $response['duplicated']=sizeof($duplicated);
            $response['restricted']=sizeof($restricted);
            $response['max_candidates']=$max_candidates;
            $response['project_restricted']=sizeof($project_restricted);
        }

        return response()->json($response);



    }

}