<?php

namespace AidRepository\Controller;

use AidRepository\Model\Project;
use AidRepository\Model\ProjectPerson;
use AidRepository\Model\ProjectOrganization;


use App\Http\Controllers\Controller;
use Common\Model\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use AidRepository\Model\Repository;
use Organization\Model\Organization;

class CentralRepositoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate central aid_repositories to logged user according condition
    public function filter(Request $request)
    {
        $this->authorize('manageCentral', Repository::class);

        $language_id =  \App\Http\Helpers::getLocale();

        $items = Repository::central($request->all());
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit',-1);

        if($request->action =='xlsx'){
            if(sizeof($items) > 0){
                $token = md5(uniqid());
                \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items)  {
                    $excel->setTitle(trans('aid-repository::application.aid_repositories'));
                    $excel->setDescription(trans('aid-repository::application.aid_repositories'));
                    $excel->sheet(trans('aid-repository::application.aid_repositories'),function($sheet)use($items)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('solid');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);

                        $sheet->setWidth(['A'=>5 ,'B'=> 35,'C'=> 20,'D'=> 35,'E'=> 35,'F'=> 35, 'G'=> 35]);

                        $range="A1:G1";
                        $sheet->getStyle($range)->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                        ]);

                        $sheet ->setCellValue('A1',trans('aid-repository::application.#'));
                        $sheet ->setCellValue('B1',trans('aid-repository::application.repository_name'));
                        $sheet ->setCellValue('C1',trans('aid-repository::application.repository_date'));
                        $sheet ->setCellValue('D1',trans('aid-repository::application.user_name'));
                        $sheet ->setCellValue('E1',trans('aid-repository::application.Organization_Name'));
                        $sheet ->setCellValue('F1',trans('aid-repository::application.created_at'));
                        $sheet ->setCellValue('G1',trans('aid-repository::application.notes'));

                        $z= 2;
                        foreach($items as $k=>$v )
                        {
                            $sheet ->setCellValue('A'.$z,$k+1);
                            $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                            $sheet ->setCellValue('C'.$z,(is_null($v->date) || $v->date == ' ' ) ? '-' : $v->date);
                            $sheet ->setCellValue('D'.$z,(is_null($v->user_name) || $v->user_name == ' ' ) ? '-' : $v->user_name);
                            $sheet ->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                            $sheet ->setCellValue('F'.$z,(is_null($v->created_at) || $v->created_at == ' ' ) ? '-' : $v->created_at);
                            $sheet ->setCellValue('G'.$z,(is_null($v->notes) || $v->notes == ' ' ) ? '-' : $v->notes);
                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true , 'download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        elseif($request->action =='statistic'){
            if(sizeof($items) > 0){
                $user = Auth::user();
                $organization_id=$user->organization_id;
                $aid_repositories = Project::filter(['repository_id' => $items],$organization_id);
                if(sizeof($aid_repositories) > 0){
                    $UserOrg=$user->organization;
                    $level = $UserOrg->level;
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($aid_repositories,$level) {
                        $excel->setTitle(trans('aid-repository::application.statistic'));
                        $excel->setDescription(trans('aid-repository::application.statistic'));
                        $excel->sheet(trans('aid-repository::application.statistic'), function($sheet) use($aid_repositories,$level) {

                            $sheet->setBorder('A1', 'solid');
                            $sheet->setRightToLeft(true);
//                    $sheet->setAllBorders('solid');
                            $sheet->setfitToWidth(true);
                            $sheet->setHeight(1,40);
                            $sheet->getDefaultStyle()->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                            ]);

                            $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                            $sheet->setCellValue('B1',trans('aid-repository::application.repository_name'));
                            $sheet->setCellValue('C1',trans('aid-repository::application.repository_date'));
                            $sheet->setCellValue('D1',trans('aid-repository::application.project_name'));
                            $sheet->setCellValue('E1',trans('aid-repository::application.category_name'));
                            $sheet->setCellValue('F1',trans('aid-repository::application.organization_name'));
                            $sheet->setCellValue('G1',trans('aid-repository::application.organization_quantity'));
                            $sheet->setCellValue('H1',trans('aid-repository::application.custom_organization'));
                            $sheet->setCellValue('I1',trans('aid-repository::application.custom_quantity'));
                            $sheet->setCellValue('J1',trans('aid-repository::application.sponsor_name'));
                            $sheet->setCellValue('K1',trans('aid-repository::application.quantity'));
                            $sheet->setCellValue('L1',trans('aid-repository::application.currency'));
                            $sheet->setCellValue('M1',trans('aid-repository::application.exchange_rate'));
                            $sheet->setCellValue('N1',trans('aid-repository::application.amount_'));
                            $sheet->setCellValue('O1',trans('aid-repository::application.amount_sh'));

                            $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'];
                            foreach($map2 as $key ) {
                                $sheet->mergeCells($key .'1:' . $key . 2);
                                $sheet->cells($key .'1:' . $key . 2, function($cells) {
                                    $cells->setBorder('solid','solid','solid','solid');
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                            }

                            $sheet->setCellValue('P1',trans('aid-repository::application.main_organization_detail'));
                            $sheet->mergeCells('P1:U1');
                            $sheet->cells('P1:U1', function($cells) {
                                $cells->setBorder('solid','solid','solid','solid');
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->setCellValue('P2',trans('common::application.organization_name'));
                            $sheet->setCellValue('Q2',trans('common::application.nominated_no'));
                            $sheet->setCellValue('R2',trans('common::application.container_share'));
                            $sheet->setCellValue('S2',trans('common::application.ratio'));
                            $sheet->setCellValue('T2',trans('common::application.quantity'));
                            $sheet->setCellValue('U2',trans('common::application.amount_'));
                            $sheet->cells("P2:U2", function($cells) {
                                $cells->setBorder('solid','solid','solid','solid');
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });


                            if($level == Organization::LEVEL_MASTER_CENTER) {
                                $sheet->setCellValue('V1',trans('common::application.sub_organization_data'));
                                $sheet->mergeCells("V1:Y1");
                                $sheet->cells("V1:Y1", function($cells) {
                                    $cells->setBorder('solid','solid','solid','solid');
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });

                                $sheet->setCellValue('V2',trans('common::application.organization_name'));
                                $sheet->setCellValue('W2',trans('common::application.nominated_no'));
                                $sheet->setCellValue('X2',trans('common::application.organization_data'));
                                $sheet->mergeCells("X2:Y2");
                                $sheet->cells("U2:Y2", function($cells) {
                                    $cells->setBorder('solid','solid','solid','solid');
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $sheet->cells("W2:Y2", function($cells) {
                                    $cells->setBorder('solid','solid','solid','solid');
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 40, 'E' => 20 ,'F' => 40 ,'G' => 15,'H' => 40,
                                    'I' => 15 ,'J' => 40 ,'K' => 15,'L' => 15 ,'M' => 20,'N' => 15 ,'O' => 15  ,'P' => 40  ,'Q' => 15  ,'R' => 15 ,'S' => 15
                                ,'T' => 15  ,'U' => 15  ,'V' => 40 ,'W' => 15,'X' => 15,'Y' => 15));
                            }else{
                                $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 40, 'E' => 20 ,'F' => 40 ,'G' => 40,'H' => 15,
                                    'I' => 15 ,'J' => 15 ,'K' => 15,'L' => 15 ,'M' => 20,'N' => 15 ,'O' => 40  ,'P' => 15  ,'Q' => 15  ,'R' => 15 ,'S' => 15
                                ,'T' => 15 ,'U' => 15 ));

                            }



                            $z = 3;
                            foreach ($aid_repositories as $k => $v) {
                                if( !($v->level == 1 || $v->level == '1') ) {
                                    if(sizeof($v->projects) > 0){
                                        if(!(1 < $v->count)){
                                            $sheet->setCellValue('A' . $z, $k + 1);
                                            $sheet->setCellValue('B' . $z, $v->repository_name);
                                            $sheet->setCellValue('C' . $z, $v->repository_date);
                                            $sheet->cells("A".($z).":C".($z), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $z2=$z;
                                            foreach($v->projects as $k2=>$v2 ) {
                                                if($v2->has_descendants){
                                                    $sheet->setCellValue('D' . $z2, $v2->name);
                                                    $sheet->mergeCells("D".($z2).":D".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("D".($z).":D".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('E' . $z2, $v2->category_name);
                                                    $sheet->mergeCells("E".($z2).":E".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("E".($z).":E".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });


                                                    $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                                    $sheet->mergeCells("F".($z2).":F".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("F".($z).":F".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                                    $sheet->mergeCells("G".($z2).":G".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("G".($z).":G".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                                    $sheet->mergeCells("H".($z2).":H".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("H".($z).":H".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('I' . $z2, ($v2->custom_quantity));
                                                    $sheet->mergeCells("I".($z2).":I".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("I".($z).":I".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                                    $sheet->mergeCells("J".($z2).":J".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("J".($z).":J".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('K' . $z2, $v2->quantity);
                                                    $sheet->mergeCells("K".($z2).":K".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("K".($z).":K".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                                    $sheet->mergeCells("L".($z2).":L".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("L".($z).":L".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                                    $sheet->mergeCells("M".($z2).":M".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("M".($z).":M".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('N' . $z2, $v2->amount );
                                                    $sheet->mergeCells("N".($z2).":N".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("N".($z).":N".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                                    $sheet->mergeCells("O".($z2).":O".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("O".($z).":O".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $z2_=3;
                                                    foreach($v2->organizations_ as $k2_=>$v2_ ) {
                                                        $descendants_count = sizeof($v2_->descendants);
                                                        if(sizeof($v2_->descendants) > 0 ){
                                                            $descendants_count = $descendants_count * 3;
                                                            $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                            $sheet->mergeCells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            if($level == Organization::LEVEL_MASTER_CENTER) {
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                            }else{
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                            }
                                                            $sheet->mergeCells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                            $sheet->mergeCells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                            $sheet->mergeCells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                            $sheet->mergeCells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                            $sheet->mergeCells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            foreach($v2_->descendants as $k1=>$v1) {
                                                                $sheet->setCellValue('V'.($z2_ + ($k1 * 3 )), $v1->name);
                                                                $sheet->mergeCells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2));
                                                                $sheet->cells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                });

                                                                $sheet->setCellValue('W'.($z2_ + ($k1 * 3 )), $v1->nominated_count);
                                                                $sheet->mergeCells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2));
                                                                $sheet->cells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                });
                                                                $sheet ->setCellValue('X'.($z2_ + ($k1 * 3 )),trans('common::application.ratio'));
                                                                $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 1),trans('common::application.quantity'));
                                                                $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 2),trans('common::application.amount_'));

                                                                $sheet ->setCellValue('Y'.($z2_ + ($k1 * 3 )),$v1->ratio);
                                                                $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 1),$v1->quantity);
                                                                $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 2),$v1->amount);

                                                            }


                                                            $z2_ = ($z2_ + $descendants_count) ;

                                                        }
                                                        else{
                                                            $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                            if($level == Organization::LEVEL_MASTER_CENTER) {
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                            }else{
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                            }
                                                            $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                            $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                            $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                            $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                            $sheet->setCellValue('V'.$z2_, ' - ');
                                                            $sheet->mergeCells("V".$z2_ .":Y" .$z2_);
                                                            $sheet->cells("V".$z2_ .":Y" .$z2_, function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $z2_++;
                                                        }
                                                    }

                                                    $z2 = $z2 + $v2->count ;
                                                }

                                            }

                                            $z++;

                                        }
                                        else{

                                            $sheet->setCellValue('A' . $z, $k + 1);
                                            $sheet->mergeCells("A".($z).":A".(($z + ($v->count)) - 1));
                                            $sheet->cells("A".($z).":A".($z + ($v->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('B' . $z, $v->repository_name);
                                            $sheet->mergeCells("B".($z).":B".(($z + ($v->count)) - 1));
                                            $sheet->cells("B".($z).":B".($z + ($v->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('C' . $z, $v->repository_date);
                                            $sheet->mergeCells("C".($z).":C".(($z + ($v->count)) - 1));
                                            $sheet->cells("C".($z).":C".($z + ($v->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $z2=$z;
                                            foreach($v->projects as $k2=>$v2 ) {
                                                if($v2->has_descendants){
                                                    $sheet->setCellValue('D' . $z2, $v2->name);
                                                    $sheet->mergeCells("D".($z2).":D".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("D".($z).":D".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('E' . $z2, $v2->category_name);
                                                    $sheet->mergeCells("E".($z2).":E".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("E".($z).":E".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });


                                                    $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                                    $sheet->mergeCells("F".($z2).":F".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("F".($z).":F".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                                    $sheet->mergeCells("G".($z2).":G".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("G".($z).":G".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                                    $sheet->mergeCells("H".($z2).":H".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("H".($z).":H".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('I' . $z2, ($v2->custom_quantity / 100));
                                                    $sheet->mergeCells("I".($z2).":I".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("I".($z).":I".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                                    $sheet->mergeCells("J".($z2).":J".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("J".($z).":J".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('K' . $z2, $v2->quantity);
                                                    $sheet->mergeCells("K".($z2).":K".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("K".($z).":K".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                                    $sheet->mergeCells("L".($z2).":L".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("L".($z).":L".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                                    $sheet->mergeCells("M".($z2).":M".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("M".($z).":M".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('N' . $z2, $v2->amount );
                                                    $sheet->mergeCells("N".($z2).":N".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("N".($z).":N".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                                    $sheet->mergeCells("O".($z2).":O".(($z2 + ($v2->count)) - 1));
                                                    $sheet->cells("O".($z).":O".($z2 + ($v2->count) - 1), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $z2_=$z2;
                                                    foreach($v2->organizations_ as $k2_=>$v2_ ) {
                                                        $descendants_count = sizeof($v2_->descendants);
                                                        if(sizeof($v2_->descendants) > 0 ){
                                                            $descendants_count = $descendants_count * 3;
                                                            $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                            $sheet->mergeCells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            if($level == Organization::LEVEL_MASTER_CENTER) {
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                            }else{
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                            }
                                                            $sheet->mergeCells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                            $sheet->mergeCells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                            $sheet->mergeCells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                            $sheet->mergeCells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                            $sheet->mergeCells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)));
                                                            $sheet->cells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                            });

                                                            foreach($v2_->descendants as $k1=>$v1) {
                                                                $sheet->setCellValue('V'.($z2_ + ($k1 * 3 )), $v1->name);
                                                                $sheet->mergeCells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2));
                                                                $sheet->cells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                });

                                                                $sheet->setCellValue('W'.($z2_ + ($k1 * 3 )), $v1->nominated_count);
                                                                $sheet->mergeCells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2));
                                                                $sheet->cells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                });
                                                                $sheet ->setCellValue('X'.($z2_ + ($k1 * 3 )),trans('common::application.ratio'));
                                                                $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 1),trans('common::application.quantity'));
                                                                $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 2),trans('common::application.amount_'));

                                                                $sheet ->setCellValue('Y'.($z2_ + ($k1 * 3 )),$v1->ratio);
                                                                $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 1),$v1->quantity);
                                                                $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 2),$v1->amount);

                                                            }


                                                            $z2_ = ($z2_ + $descendants_count) ;

                                                        }
                                                        else{
                                                            $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                            if($level == Organization::LEVEL_MASTER_CENTER) {
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                            }else{
                                                                $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                            }
                                                            $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                            $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                            $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                            $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                            $sheet->setCellValue('V'.$z2_, ' - ');
                                                            $sheet->mergeCells("V".$z2_ .":Y" .$z2_);
                                                            $sheet->cells("V".$z2_ .":Y" .$z2_, function($cells) {
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $z2_++;
                                                        }
                                                    }

                                                    $z2 = $z2 + $v2->count ;
                                                }

                                            }

                                            $z += $v->count ;
                                        }
                                    }
                                    else{
                                        $sheet->setCellValue('A' . $z, $k + 1);
                                        $sheet->setCellValue('B' . $z, $v->repository_name);
                                        $sheet->setCellValue('C' . $z, $v->repository_date);
                                        $sheet->setCellValue('D'.$z, ' - ');
                                        $sheet->mergeCells("D".$z .":Y" .$z);
                                        $sheet->cells("D".$z .":Y" .$z, function($cells) {
                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                            $cells->setFontWeight('bold');
                                        });
                                        $z++;
                                    }
                                }else{
                                    if(sizeof($v->projects) > 0){
                                        $sheet->setCellValue('A' . $z, $k + 1);
                                        $sheet->mergeCells("A".$z .":A" .( $z  + sizeof($v->projects) - 1 ));
                                        $sheet->setCellValue('B' . $z, $v->repository_name);
                                        $sheet->mergeCells("B".$z .":B" .( $z  + sizeof($v->projects) - 1 ));
                                        $sheet->setCellValue('C' . $z, $v->repository_date);
                                        $sheet->mergeCells("C".$z .":C" .( $z  + sizeof($v->projects) - 1 ));
                                        $sheet->cells("A".($z).":C".($z), function($cells) {
                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                            $cells->setFontWeight('bold');
                                        });

                                        $z2=$z;
                                        foreach($v->projects as $k2=>$v2 ) {
                                            $sheet->setCellValue('D' . $z2, $v2->name);
                                            $sheet->setCellValue('E' . $z2, $v2->category_name);
                                            $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                            $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                            $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                            $sheet->setCellValue('I' . $z2, ($v2->custom_quantity));
                                            $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                            $sheet->setCellValue('K' . $z2, $v2->quantity);
                                            $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                            $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                            $sheet->setCellValue('N' . $z2, $v2->amount );
                                            $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                            $sheet->setCellValue('P'.$z2, ' - ');
                                            $sheet->mergeCells("P".$z2 .":Y" .$z2);
                                            $sheet->cells("D".$z2 .":Y" .$z2, function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $z2++ ;
                                        }

                                        $z += sizeof($v->projects) ;

                                    }
                                    else{
                                        $sheet->setCellValue('A' . $z, $k + 1);
                                        $sheet->setCellValue('B' . $z, $v->repository_name);
                                        $sheet->setCellValue('C' . $z, $v->repository_date);
                                        $sheet->setCellValue('D'.$z, ' - ');
                                        $sheet->mergeCells("D".$z .":Y" .$z);
                                        $sheet->cells("D".$z .":Y" .$z, function($cells) {
                                            $cells->setAlignment('center');
                                            $cells->setValignment('center');
                                            $cells->setFontWeight('bold');
                                        });
                                        $z++;
                                    }
                                }
                            }
                        });
                    })->store('xlsx', storage_path('tmp/'));
                    return response()->json(['status' =>true ,'download_token' =>$token]);
                }
                return response()->json(['status' =>false ,'msg' =>trans('common::application.no organization')]);
            }
            return response()->json(['status' => false]);
        }


        return response()->json($items);
    }

    // get projectsOptions of aid_repository by id
    public function projectsOptions($id)
    {

        $this->authorize('manageCentral', Repository::class);

        $user = Auth::user();
        $repository = Repository::findOrFail($id);
        $organization_id = $user->organization_id;

        $organizations = Organization::where(function ($q) use ($user) {
                                           $q->where('type', 1);
                                            $q->where('id', '!=', $user->organization_id);

                                           if($user->type == 2){
                                                $q->whereIn('id', function($quer) use($user) {
                                                            $quer->select('organization_id')
                                                                 ->from('char_user_organizations')
                                                                 ->where('user_id', '=', $user->id);
                                                });
                                           }
                                           else{
                                                $q->wherein('id', function($query) use($user) {
                                                     $query->select('descendant_id')
                                                         ->from('char_organizations_closure')
                                                         ->where('ancestor_id', '=', $user->organization_id);
                                                 });
                                           }
                                          })->selectRaw("char_organizations.*")
                                          ->orderBy('container_share','desc')->get();                  

        return response()->json(['exe_org' => $organizations]);

    }

    // get projects of aid_repository by id
    public function projects($id)
    {
        $this->authorize('manageCentral', Repository::class);
        $user = Auth::user();
        $organization_id = $user->organization_id;
        $repository = Repository::findOrFail($id);

        $repository->is_mine = 0;
        if($repository->organization_id == $organization_id){
            $repository->is_mine = 1;
        }

        if( !($repository->level == 1 || $repository->level == '1') ){
            $repository->projects = Project::allWithOptions(array(
                'repository_id' => $id,
                'repository_org_id' => $repository->organization_id,
                'with_organization_name' => true,
                'with_sponsor_name' => true,
                'with_category_name' => true,
                'with_organizations' => false,
                'with_count' => false
            ),$organization_id);
            $count = sizeof($repository->projects) ;

            $org_in_set = [];
            if(sizeof($repository->projects) > 0) {
                foreach ($repository->projects as $k1 => $v2) {
                    if($v2->organization_quantity != 0){
                        $org_in_set[] = $v2->organization_id;
                    }

                    if($v2->custom_quantity != 0){
                        $org_in_set[] = $v2->custom_organization;
                    }
                    $ProjectOrganization= ProjectOrganization::where(['project_id'=>$v2->id ,
                                                                      'organization_id'=>$user->organization_id])
                            ->first();
                    $v2->quantity = 0;
                    if(!is_null($ProjectOrganization)){
                        $v2->quantity = $ProjectOrganization->quantity;
                    }
                   
                }

                $repository->org_in_set = $org_in_set;
            }
            $organizations = Organization::where(function ($q) use ($repository,$user) {
//                   $q->where('parent_id', '=', $repository->organization_id);
                    $q->where('id', '!=', $user->organization_id);
                    $q->where('container_share', '<>', 0);
                     if($user->type == 2){
                                                $q->whereIn('id', function($quer) use($user) {
                                                            $quer->select('organization_id')
                                                                 ->from('char_user_organizations')
                                                                 ->where('user_id', '=', $user->id);
                                                });
                                           }
                                           else{
                                                $q->wherein('id', function($query) use($user) {
                                                     $query->select('descendant_id')
                                                         ->from('char_organizations_closure')
                                                         ->where('ancestor_id', '=', $user->organization_id);
                                                 });
                                           }
                    $q->whereIn('id',function ($q0) use ($repository) {
                        $q0->select('organization_id')
                            ->from('aid_projects_organizations')
                            ->where(function ($q0_) use ($repository) {
                                $q0_->where('aid_projects_organizations.ratio', '!=', 0);
                                $q0_->whereIn('project_id',function ($q0) use ($repository) {
                                    $q0->select('aid_projects.id')
                                        ->from('aid_projects')
                                        ->where('repository_id', '=', $repository->id);
                                });
                            });
                    });
//                $q->orWhereIn('id', $org_in_set);

            })
                ->selectRaw("char_organizations.* ,
                                                               get_organization_total(1 ,'$id' ,char_organizations.id , '$count') as total_ratio ,
                                                               get_organization_total(2 ,'$id' ,char_organizations.id , '$count') as total_amount ,
                                                               get_organization_total(3 ,'$id' ,char_organizations.id , '$count') as total_quantity ,
                                                              char_organizations.container_share")
                ->orderBy('container_share','desc')->get();

            $repository->organizations =$organizations;
            if(sizeof($repository->projects) > 0){
                $map_ =[];
                foreach ($repository->projects as $k1=>$v2){
                    $v2->total_quantity = 0;
                    $v2->total_amount = 0;
                    $v2->total_ratio = 0;
                    $v2->changes_= 0;
                    $v2->has_subtract_ratio = false ;

                    $project_id = $v2->id;
                    $proj_quantity = $v2->quantity - ($v2->organization_quantity);
                    $proj_amount = $v2->amount * $proj_quantity;


                    if($v2->custom_organization  && $v2->custom_quantity > 0 ){
                        $custom_org = Organization::where('id', $v2->custom_organization)->first();
                        $v2->organization_container_share= $custom_org->container_share;

                        $project_custom_ratio = 0;

                        if($v2->custom_quantity > 0 ){
                            if($v2->custom_quantity){
                                if($v2->custom_quantity != 0 && $v2->custom_quantity != '0'){
                                    $project_custom_quantity = $v2->custom_quantity;
                                    $project_custom_ratio = ( ( $project_custom_quantity * 100 ) / $proj_quantity ) ;
                                }
                            }
                        }

                        if($custom_org){
                            $original_ration = $custom_org->container_share;
                            if($project_custom_ratio > $original_ration){
                                $v2->changes_ = ($project_custom_ratio - $original_ration) / (100 - $original_ration);
                                $v2->has_subtract_ratio = true;
                            }
                        }
                    }

                    $organizations_=\DB::table('aid_projects_organizations')
                        ->where('aid_projects_organizations.project_id', '=', $project_id)
                        ->selectRaw('aid_projects_organizations.*')->get();

                    $map =[];
                    foreach ($organizations_ as $k0=>$v0){
                        $map[$v0->organization_id]=['ratio' =>$v0->ratio ,'amount' =>$v0->amount ,'quantity' =>$v0->quantity];
                    }

                    if($v2->organization_quantity != 0){
                        if(!isset($map[$v2->organization_id])){
                            $quantity_n = $v2->organization_quantity ;
                            $amount_n = $quantity_n * $v2->amount ;
                            $map[$v2->organization_id]=['ratio' =>$v2->organization_ratio ,'amount' =>$amount_n ,'quantity' =>$quantity_n];
                        }
                    }

                    $map_[]=$map;
                    $repository->map = $map_;
                    $v2->man = $organizations;
                    $v2->organizations = [];
                    foreach ($organizations as $k=>$v){
                        if(isset($map[$v->id])){
                            $temp =['organization_id' => $v->id , 'name' => $v->name ,
                                    'container_share' => $v->container_share ,
                                    'ratio' => $map[$v->id]['ratio'] , 'amount' => $map[$v->id]['amount'] ,
                                    'quantity' => $map[$v->id]['quantity'] , 'project_id' =>  $v2->id ,
                                    'has_custom_ratio' => false ];

                            if($v2->has_subtract_ratio && $v->id == $v2->organization_id){
                                $temp['has_custom_ratio'] = true;
                            }

                            $v2->organizations[]=$temp;

                            $repository->total_ratio += $temp['ratio'] ;
                            $repository->total_quantity += $temp['quantity'] ;
                            $repository->total_amount += $temp['amount'] ;
                            
                            $v2->total_quantity += $temp['quantity'] ;
                            $v2->total_amount += $temp['amount'] ;
                            $v2->total_ratio += $temp['ratio'] ;
                    
                        }
                    }

                }

                $repository->total_ratio =  round(($repository->total_ratio / sizeof($repository->projects)),2) ;

            }
            else{
                foreach ($repository->organizations as $k=>$v){
                    $v->total_quantity = 0;
                    $v->total_amount = 0;
                    $v->total_ratio = 0;
                }
            }
        }
        else{
            $repository->projects = Project::allWithOptions(array(
                'repository_id' => $id,
                'repository_org_id' => $repository->organization_id,
                'with_organization_name' => true,
                'with_sponsor_name' => true,
                'with_category_name' => true,
                'with_organizations' => false,
                'with_count' => true
            ),$organization_id);

            if(sizeof($repository->projects) > 0) {
                foreach ($repository->projects as $k1 => $v2) {
                    
                    $quantity_= ProjectPerson::Where(function ($sq) use ($user,$v2) {
                              $sq->where('project_id', $v2->id);

                                                if($user->type == 2)
                                                {
                                                    $sq->whereIn('organization_id', function($quer) use($user) {
                                                              $quer->select('organization_id')
                                                                      ->from('char_user_organizations')
                                                                      ->where('user_id', '=', $user->id);
                                                        });                                                                                                      
                                                }
                                                else
                                                {
                                                    $sq->whereIn('organization_id', function($quer) use($user) {
                                                           $quer->select('descendant_id')
                                                                 ->from('char_organizations_closure')
                                                                 ->where('ancestor_id', '=', $user->organization_id);
                                                       });
                                                }
                                          })
                            ->count();
                    $v2->quantity = $quantity_;                    
                    $repository->total_quantity += $v2->quantity ;
                    $repository->total_amount_sk += ( $v2->quantity * $v2->amount * $v2 ->exchange_rate) ;
                    $repository->total_amount += $v2->amount * $v2 ->exchange_rate ;
                }
            }
        }

        return response()->json($repository);
    }

     // return organizationShare of logged user organization on the project
    public function organizationShare(Request $request, $id)
    {
        $project = Project::findOrFail($id);

        if ($project->status == \AidRepository\Model\Project::STATUS_CLOSED ||
            $project->status == \AidRepository\Model\Project::STATUS_EXECUTION ) {
            return response()->json(['error' => trans('aid-repository::application.cant update on the ratios of associations for a closed or approved project for implementation')], 422);
        }

        $organizations = $request->input('organizations');
        $project->organization_ratio = $request->input('organization_ratio');
        $project->notes = $request->input('notes');
        $project->adjust = $request->input('adjust');
        if (\AidRepository\Model\ProjectOrganization::saveCentralForProject($project, $organizations)) {

            \Log\Model\Log::saveNewLog('PROJECT_ORGANIZATION_UPDATED',trans('aid-repository::application.modified the proportions of the associations for a project')  . ' "'.$project->name. '" ');

            return response()->json([
                'success' => 1,
            ]);
        }
        
        return response()->json([
            'error' => 1,
        ], 422);
    }
    
    // statistic of organizations on project using project_id;
    public function statistic(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit',-1);

        $project_id = $request->project_id;
        $project = Project::findOrFailWithDetails($project_id);

        $name = $project->name;
        $exchange_rate = $project->exchange_rate;
        $user = Auth::user();
        $response['status']=false;
        $organization_id=$user->organization_id;
        $UserOrg=$user->organization;
        $level = $UserOrg->level;

        $project->organizations_=\DB::table('aid_projects_organizations')
            ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                ->where(function ($q) use ($user,$project_id) {
                    $q->where('aid_projects_organizations.project_id', '=', $project_id);
//                    $q->where('char_organizations.parent_id', '=', $organization_id);            
                    $q->where('char_organizations.id', '!=', $user->organization_id);
//                    $q->where('container_share', '<>', 0);
                     if($user->type == 2){
                          $q->whereIn('char_organizations.id', function($quer) use($user) {
                                 $quer->select('organization_id')
                                      ->from('char_user_organizations')
                                      ->where('user_id', '=', $user->id);
                          });
                    }
                    else{
                          $q->wherein('char_organizations.id', function($query) use($user) {
                                  $query->select('descendant_id')
                                        ->from('char_organizations_closure')
                                        ->where('ancestor_id', '=', $user->organization_id);
                          });
                    }
            })
                
                
            ->selectRaw("aid_projects_organizations.organization_id ,
                         aid_projects_organizations.ratio , 
                         aid_projects_organizations.quantity ,  
                         aid_projects_organizations.amount , 
                        char_organizations.name ,
                        char_get_organizations_child_nominated_count(char_organizations.id, '$project_id' ) as child_nominated_count,
                        char_get_organization_nominated_count(char_organizations.id, '$project_id' ) as nominated_count,
                        round((char_organizations.container_share/100),2) as container_share")->get();

        $count_ = sizeof( $project->organizations_);
        $count = 0;
        foreach ( $project->organizations_ as $k=>$v){
            $v->descendants =\DB::table('aid_projects_organizations')
                ->join('char_organizations', 'aid_projects_organizations.organization_id', '=', 'char_organizations.id')
                ->where('aid_projects_organizations.project_id', '=', $project_id)
                ->where('char_organizations.parent_id', '=', $v->organization_id)
                ->selectRaw("aid_projects_organizations.organization_id ,
                                             aid_projects_organizations.ratio , 
                                             aid_projects_organizations.quantity ,  
                                             aid_projects_organizations.amount , 
                                             char_organizations.name ,
                                             char_get_organization_nominated_count(char_organizations.id, $project_id ) as nominated_count,
                                             round((char_organizations.container_share/100),2) as container_share")->get();

            if(sizeof($v->descendants ) > 0 ){
                $count += (sizeof($v->descendants ) * 3 );

            }else{
                $count ++;

            }

        }
        $has_descendants = false;
        $project->has_descendants = false;
        $project->count = $count;
        if(($count_ + $count) > sizeof( $project->organizations_)){
            $has_descendants = true;
            $project->has_descendants = true;
        }

        if($count > 0){
            $projects[] = $project;
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($projects,$name,$count,$has_descendants,$level) {
                $excel->setTitle($name);
                $excel->setDescription($name);
                $excel->sheet($name, function($sheet) use($projects,$name,$count,$has_descendants,$level) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('solid');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                    $sheet->setCellValue('B1',trans('aid-repository::application.repository_name'));
                    $sheet->setCellValue('C1',trans('aid-repository::application.repository_date'));
                    $sheet->setCellValue('D1',trans('aid-repository::application.project_name'));
                    $sheet->setCellValue('E1',trans('aid-repository::application.organization_name'));
                    $sheet->setCellValue('F1',trans('aid-repository::application.organization_quantity'));
                    $sheet->setCellValue('G1',trans('aid-repository::application.custom_organization'));
                    $sheet->setCellValue('H1',trans('aid-repository::application.custom_quantity'));
                    $sheet->setCellValue('I1',trans('aid-repository::application.sponsor_name'));
                    $sheet->setCellValue('J1',trans('aid-repository::application.quantity'));
                    $sheet->setCellValue('K1',trans('aid-repository::application.currency'));
                    $sheet->setCellValue('L1',trans('aid-repository::application.exchange_rate'));
                    $sheet->setCellValue('M1',trans('aid-repository::application.amount_'));
                    $sheet->setCellValue('N1',trans('aid-repository::application.amount_sh'));

                    $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N'];

                    foreach($map2 as $key ) {
                        $sheet->mergeCells($key .'1:' . $key . 3);
                        $sheet->cells($key .'1:' . $key . 3, function($cells) {

                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                    }

                    $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 40, 'E' => 40 ,'F' => 15 ,'G' => 40,'H' => 15,
                        'I' => 40 ,'J' => 15 ,'K' => 15,'L' => 15 ,'M' => 15,'N' => 15 ,'O' => 40  ,'P' => 15  ,'Q' => 15  ,'R' => 15 ,'S' => 15
                       ,'T' => 15  ,'U' => 40  ,'V' => 15 ,'W' => 15,'x' => 15));


                    if($has_descendants){
                        $sheet->setCellValue('O1',trans('aid-repository::application.main_organization_detail'));
                        $sheet->mergeCells('O1:T2');
                        $sheet->cells('O1:T2', function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->setCellValue('O3',trans('common::application.organization_name'));
                        $sheet->setCellValue('P3',trans('common::application.nominated_no'));
                        $sheet->setCellValue('Q3',trans('common::application.container_share'));
                        $sheet->setCellValue('R3',trans('common::application.ratio'));
                        $sheet->setCellValue('S3',trans('common::application.quantity'));
                        $sheet->setCellValue('T3',trans('common::application.amount_'));
                        $sheet->cells("O3:T3", function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        if($count > 0 ){
                            $sheet->setCellValue('U1',trans('common::application.sub_organization_data'));
                            $sheet->mergeCells("U1:X2");
                            $sheet->cells("U1:X2", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });

                            $sheet->setCellValue('U3',trans('common::application.organization_name'));
                            $sheet->setCellValue('V3',trans('common::application.nominated_no'));
                            $sheet->setCellValue('W3',trans('common::application.organization_data'));
                            $sheet->mergeCells("W3:X3");
                            $sheet->cells("W3:X3", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                            $sheet->cells("U3:X3", function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                        }
                        $map2 = ['A', 'B','C' ,'D'  , 'E','F' ,'G','H', 'I' ,'J' ,'K','L' ,'M' ,'N' ];
                        foreach($map2 as $key ) {
                            $sheet->mergeCells($key .'1:' . $key . 3);
                            $sheet->cells($key .'1:' . $key . 3, function($cells) {

                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                $cells->setFontWeight('bold');
                            });
                        }
                    }
                    $z = 4;
                    foreach ($projects as $k => $v) {
                        $sheet->setCellValue('A' . $z, $k + 1);
                        $sheet->mergeCells("A".($z).":A".(($z + ($count)) - 1));
                        $sheet->cells("A".($z).":A".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('B' . $z, $v->repository_name);
                        $sheet->mergeCells("B".($z).":B".(($z + ($count)) - 1));
                        $sheet->cells("B".($z).":B".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('C' . $z, $v->repository_date);
                        $sheet->mergeCells("C".($z).":C".(($z + ($count)) - 1));
                        $sheet->cells("C".($z).":C".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('D' . $z, $v->name);
                        $sheet->mergeCells("D".($z).":D".(($z + ($count)) - 1));
                        $sheet->cells("D".($z).":D".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->setCellValue('E' . $z, $v->organization_name);
                        $sheet->mergeCells("E".($z).":E".(($z + ($count)) - 1));
                        $sheet->cells("E".($z).":E".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('F' . $z,( $v->organization_quantity));
                        $sheet->mergeCells("F".($z).":F".(($z + ($count)) - 1));
                        $sheet->cells("F".($z).":F".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('G' . $z, $v->custom_organization_name);
                        $sheet->mergeCells("G".($z).":G".(($z + ($count)) - 1));
                        $sheet->cells("G".($z).":G".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('H' . $z,  $v->custom_quantity);
                        $sheet->mergeCells("H".($z).":H".(($z + ($count)) - 1));
                        $sheet->cells("H".($z).":H".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('I' . $z, $v->sponsor_name);
                        $sheet->mergeCells("I".($z).":I".(($z + ($count)) - 1));
                        $sheet->cells("I".($z).":I".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('J' . $z, $v->quantity);
                        $sheet->mergeCells("J".($z).":J".(($z + ($count)) - 1));
                        $sheet->cells("J".($z).":J".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('K' . $z, $v->currency_name);
                        $sheet->mergeCells("K".($z).":K".(($z + ($count)) - 1));
                        $sheet->cells("K".($z).":K".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('L' . $z, $v->exchange_rate);
                        $sheet->mergeCells("L".($z).":L".(($z + ($count)) - 1));
                        $sheet->cells("L".($z).":L".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->setCellValue('M' . $z, $v->amount );
                        $sheet->mergeCells("M".($z).":M".(($z + ($count)) - 1));
                        $sheet->cells("M".($z).":M".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('N' . $z, $v->exchange_rate * $v->amount);
                        $sheet->mergeCells("N".($z).":N".(($z + ($count)) - 1));
                        $sheet->cells("N".($z).":N".($z + ($count) - 1), function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });


                        $z2=4;
                        foreach($v->organizations_ as $k2=>$v2 ) {
                            $descendants_count = sizeof($v2->descendants);
                            if(sizeof($v2->descendants) > 0 ){
                                $descendants_count = $descendants_count * 3;
                                $sheet->setCellValue('O'.$z2,$v2->name);
                                $sheet->mergeCells("O".($z2).":O".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("O".($z2).":O".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                if($level == Organization::LEVEL_MASTER_CENTER) {
                                    $sheet ->setCellValue('P'.$z2,$v2->child_nominated_count);
                                }else{
                                    $sheet ->setCellValue('P'.$z2,$v2->nominated_count);
                                }
                                $sheet->mergeCells("P".($z2).":P".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("P".($z2).":P".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                $sheet ->setCellValue('Q'.$z2,$v2->container_share);
                                $sheet->mergeCells("Q".($z2).":Q".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("Q".($z2).":Q".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                $sheet ->setCellValue('R'.$z2,$v2->ratio);
                                $sheet->mergeCells("R".($z2).":R".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("R".($z2).":R".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                $sheet ->setCellValue('S'.$z2,$v2->quantity);
                                $sheet->mergeCells("S".($z2).":S".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("S".($z2).":S".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                $sheet ->setCellValue('T'.$z2,$v2->amount);
                                $sheet->mergeCells("T".($z2).":T".((($z2 +$descendants_count) - 1)));
                                $sheet->cells("T".($z2).":T".((($z2 +$descendants_count) - 1)), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                });

                                foreach($v2->descendants as $k1=>$v1)
                                {

                                    $sheet->setCellValue('U'.($z2 + ($k1 * 3 )), $v1->name);
                                    $sheet->mergeCells('U'.($z2 + ($k1 * 3 )).":U".(($z2 + ($k1 * 3 )) + 2));
                                    $sheet->cells('U'.($z2 + ($k1 * 3 )).":U".(($z2 + ($k1 * 3 )) + 2), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });

                                    $sheet->setCellValue('V'.($z2 + ($k1 * 3 )), $v1->nominated_count);
                                    $sheet->mergeCells('V'.($z2 + ($k1 * 3 )).":V".(($z2 + ($k1 * 3 )) + 2));
                                    $sheet->cells('V'.($z2 + ($k1 * 3 )).":V".(($z2 + ($k1 * 3 )) + 2), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                    });
                                    $sheet ->setCellValue('W'.($z2 + ($k1 * 3 )),trans('common::application.ratio'));
                                    $sheet ->setCellValue('W'.(($z2 + ($k1 * 3 )) + 1),trans('common::application.quantity'));
                                    $sheet ->setCellValue('W'.(($z2 + ($k1 * 3 )) + 2),trans('common::application.amount_'));

                                    $sheet ->setCellValue('X'.($z2 + ($k1 * 3 )),$v1->ratio);
                                    $sheet ->setCellValue('X'.(($z2 + ($k1 * 3 )) + 1),$v1->quantity);
                                    $sheet ->setCellValue('X'.(($z2 + ($k1 * 3 )) + 2),$v1->amount);
                                }

                                $z2 = ($z2 + (sizeof($v2->descendants) * 3)) ;

                            }
                            else{
                                $sheet->setCellValue('O'.$z2,$v2->name);
                                if($level == Organization::LEVEL_MASTER_CENTER) {
                                    $sheet ->setCellValue('P'.$z2,$v2->child_nominated_count);
                                }else{
                                    $sheet ->setCellValue('P'.$z2,$v2->nominated_count);
                                }
                                $sheet ->setCellValue('Q'.$z2,$v2->container_share);
                                $sheet ->setCellValue('R'.$z2,$v2->ratio);
                                $sheet ->setCellValue('S'.$z2,$v2->quantity);
                                $sheet ->setCellValue('T'.$z2,$v2->amount);
                                $sheet->setCellValue('U'.$z2, ' - ');
                                $sheet->mergeCells("U".$z2 .":X" .$z2);
                                $sheet->cells("U".$z2 .":X" .$z2, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });

                                $z2++;
                            }
                        }

                        $z++;
                    }
                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['status' =>true ,'download_token' =>$token]);
        }

        return response()->json(['status' =>false ,'msg' =>trans('common::application.no organization')]);

    }

    // statistic of aid_repository (organizations on project ) using repository_id;
    public function MainStatistic(Request $request)
    {
        $user = Auth::user();
        $aid_repositories = Project::filterForCentral(['repository_id' => [$request->repository_id]]);

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit',-1);

        if(sizeof($aid_repositories) > 0){
            $UserOrg=$user->organization;
            $level = $UserOrg->level;
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($aid_repositories,$level) {
                $excel->setTitle(trans('aid-repository::application.statistic'));
                $excel->setDescription(trans('aid-repository::application.statistic'));
                $excel->sheet(trans('aid-repository::application.statistic'), function($sheet) use($aid_repositories,$level) {

                    $sheet->setBorder('A1', 'solid');
                    $sheet->setRightToLeft(true);
//                    $sheet->setAllBorders('solid');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->setCellValue('A1',trans('aid-repository::application.#'));
                    $sheet->setCellValue('B1',trans('aid-repository::application.repository_name'));
                    $sheet->setCellValue('C1',trans('aid-repository::application.repository_date'));
                    $sheet->setCellValue('D1',trans('aid-repository::application.project_name'));
                    $sheet->setCellValue('E1',trans('aid-repository::application.category_name'));
                    $sheet->setCellValue('F1',trans('aid-repository::application.organization_name'));
                    $sheet->setCellValue('G1',trans('aid-repository::application.organization_quantity'));
                    $sheet->setCellValue('H1',trans('aid-repository::application.custom_organization'));
                    $sheet->setCellValue('I1',trans('aid-repository::application.custom_quantity'));
                    $sheet->setCellValue('J1',trans('aid-repository::application.sponsor_name'));
                    $sheet->setCellValue('K1',trans('aid-repository::application.quantity'));
                    $sheet->setCellValue('L1',trans('aid-repository::application.currency'));
                    $sheet->setCellValue('M1',trans('aid-repository::application.exchange_rate'));
                    $sheet->setCellValue('N1',trans('aid-repository::application.amount_'));
                    $sheet->setCellValue('O1',trans('aid-repository::application.amount_sh'));

                    $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'];
                    foreach($map2 as $key ) {
                        $sheet->mergeCells($key .'1:' . $key . 2);
                        $sheet->cells($key .'1:' . $key . 2, function($cells) {
                            $cells->setBorder('solid','solid','solid','solid');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                    }

                    $sheet->setCellValue('P1',trans('aid-repository::application.main_organization_detail'));
                    $sheet->mergeCells('P1:U1');
                    $sheet->cells('P1:U1', function($cells) {
                        $cells->setBorder('solid','solid','solid','solid');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });
                    $sheet->setCellValue('P2',trans('common::application.organization_name'));
                    $sheet->setCellValue('Q2',trans('common::application.nominated_no'));
                    $sheet->setCellValue('R2',trans('common::application.container_share'));
                    $sheet->setCellValue('S2',trans('common::application.ratio'));
                    $sheet->setCellValue('T2',trans('common::application.quantity'));
                    $sheet->setCellValue('U2',trans('common::application.amount_'));
                    $sheet->cells("P2:U2", function($cells) {
                        $cells->setBorder('solid','solid','solid','solid');
                        $cells->setAlignment('center');
                        $cells->setValignment('center');
                        $cells->setFontWeight('bold');
                    });


//                    if($level == Organization::LEVEL_MASTER_CENTER) {
                        $sheet->setCellValue('V1',trans('common::application.sub_organization_data'));
                        $sheet->mergeCells("V1:Y1");
                        $sheet->cells("V1:Y1", function($cells) {
                            $cells->setBorder('solid','solid','solid','solid');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });

                        $sheet->setCellValue('V2',trans('common::application.organization_name'));
                        $sheet->setCellValue('W2',trans('common::application.nominated_no'));
                        $sheet->setCellValue('X2',trans('common::application.organization_data'));
                        $sheet->mergeCells("X2:Y2");
                        $sheet->cells("U2:Y2", function($cells) {
                            $cells->setBorder('solid','solid','solid','solid');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->cells("W2:Y2", function($cells) {
                            $cells->setBorder('solid','solid','solid','solid');
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            $cells->setFontWeight('bold');
                        });
                        $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 40, 'E' => 20 ,'F' => 40 ,'G' => 15,'H' => 40,
                            'I' => 15 ,'J' => 40 ,'K' => 15,'L' => 15 ,'M' => 20,'N' => 15 ,'O' => 15  ,'P' => 40  ,'Q' => 15  ,'R' => 15 ,'S' => 15
                        ,'T' => 15  ,'U' => 15  ,'V' => 40 ,'W' => 15,'X' => 15,'Y' => 15));
//                    }else{
//                        $sheet->setWidth(array('A'=> 10, 'B'=>40,'C' => 15, 'D' => 40, 'E' => 20 ,'F' => 40 ,'G' => 40,'H' => 15,
//                            'I' => 15 ,'J' => 15 ,'K' => 15,'L' => 15 ,'M' => 20,'N' => 15 ,'O' => 40  ,'P' => 15  ,'Q' => 15  ,'R' => 15 ,'S' => 15
//                        ,'T' => 15 ,'U' => 15 ));
//
//                    }



                    $z = 3;
                    foreach ($aid_repositories as $k => $v) {
                        if( !($v->level == 1 || $v->level == '1') ) {
                            if(sizeof($v->projects) > 0){
                                if(!(1 < $v->count)){
                                    $sheet->setCellValue('A' . $z, $k + 1);
                                    $sheet->setCellValue('B' . $z, $v->repository_name);
                                    $sheet->setCellValue('C' . $z, $v->repository_date);
                                    $sheet->cells("A".($z).":C".($z), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $z2=$z;
                                    foreach($v->projects as $k2=>$v2 ) {
                                        if($v2->has_descendants){
                                            $sheet->setCellValue('D' . $z2, $v2->name);
                                            $sheet->mergeCells("D".($z2).":D".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("D".($z).":D".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('E' . $z2, $v2->category_name);
                                            $sheet->mergeCells("E".($z2).":E".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("E".($z).":E".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });


                                            $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                            $sheet->mergeCells("F".($z2).":F".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("F".($z).":F".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                            $sheet->mergeCells("G".($z2).":G".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("G".($z).":G".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                            $sheet->mergeCells("H".($z2).":H".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("H".($z).":H".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('I' . $z2, ($v2->custom_quantity / 100));
                                            $sheet->mergeCells("I".($z2).":I".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("I".($z).":I".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                            $sheet->mergeCells("J".($z2).":J".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("J".($z).":J".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('K' . $z2, $v2->quantity);
                                            $sheet->mergeCells("K".($z2).":K".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("K".($z).":K".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                            $sheet->mergeCells("L".($z2).":L".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("L".($z).":L".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                            $sheet->mergeCells("M".($z2).":M".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("M".($z).":M".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('N' . $z2, $v2->amount );
                                            $sheet->mergeCells("N".($z2).":N".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("N".($z).":N".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                            $sheet->mergeCells("O".($z2).":O".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("O".($z).":O".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $z2_=3;
                                            foreach($v2->organizations_ as $k2_=>$v2_ ) {
                                                $descendants_count = sizeof($v2_->descendants);
                                                if(sizeof($v2_->descendants) > 0 ){
                                                    $descendants_count = $descendants_count * 3;
                                                    $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                    $sheet->mergeCells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                    }else{
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                    }
                                                    $sheet->mergeCells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                    $sheet->mergeCells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                    $sheet->mergeCells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                    $sheet->mergeCells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                    $sheet->mergeCells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    foreach($v2_->descendants as $k1=>$v1) {
                                                        $sheet->setCellValue('V'.($z2_ + ($k1 * 3 )), $v1->name);
                                                        $sheet->mergeCells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2));
                                                        $sheet->cells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                        });

                                                        $sheet->setCellValue('W'.($z2_ + ($k1 * 3 )), $v1->nominated_count);
                                                        $sheet->mergeCells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2));
                                                        $sheet->cells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                        });
                                                        $sheet ->setCellValue('X'.($z2_ + ($k1 * 3 )),trans('common::application.ratio'));
                                                        $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 1),trans('common::application.quantity'));
                                                        $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 2),trans('common::application.amount_'));

                                                        $sheet ->setCellValue('Y'.($z2_ + ($k1 * 3 )),$v1->ratio);
                                                        $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 1),$v1->quantity);
                                                        $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 2),$v1->amount);

                                                    }


                                                    $z2_ = ($z2_ + $descendants_count) ;

                                                }
                                                else{
                                                    $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                    }else{
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                    }
                                                    $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                    $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                    $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                    $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                    $sheet->setCellValue('V'.$z2_, ' - ');
                                                    $sheet->mergeCells("V".$z2_ .":Y" .$z2_);
                                                    $sheet->cells("V".$z2_ .":Y" .$z2_, function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $z2_++;
                                                }
                                            }

                                            $z2 = $z2 + $v2->count ;
                                        }

                                    }

                                    $z++;

                                }
                                else{

                                    $sheet->setCellValue('A' . $z, $k + 1);
                                    $sheet->mergeCells("A".($z).":A".(($z + ($v->count)) - 1));
                                    $sheet->cells("A".($z).":A".($z + ($v->count) - 1), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $sheet->setCellValue('B' . $z, $v->repository_name);
                                    $sheet->mergeCells("B".($z).":B".(($z + ($v->count)) - 1));
                                    $sheet->cells("B".($z).":B".($z + ($v->count) - 1), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $sheet->setCellValue('C' . $z, $v->repository_date);
                                    $sheet->mergeCells("C".($z).":C".(($z + ($v->count)) - 1));
                                    $sheet->cells("C".($z).":C".($z + ($v->count) - 1), function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $z2=$z;
                                    foreach($v->projects as $k2=>$v2 ) {
                                        if($v2->has_descendants){
                                            $sheet->setCellValue('D' . $z2, $v2->name);
                                            $sheet->mergeCells("D".($z2).":D".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("D".($z).":D".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('E' . $z2, $v2->category_name);
                                            $sheet->mergeCells("E".($z2).":E".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("E".($z).":E".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });


                                            $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                            $sheet->mergeCells("F".($z2).":F".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("F".($z).":F".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                            $sheet->mergeCells("G".($z2).":G".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("G".($z).":G".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                            $sheet->mergeCells("H".($z2).":H".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("H".($z).":H".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('I' . $z2, ($v2->custom_quantity));
                                            $sheet->mergeCells("I".($z2).":I".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("I".($z).":I".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                            $sheet->mergeCells("J".($z2).":J".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("J".($z).":J".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('K' . $z2, $v2->quantity);
                                            $sheet->mergeCells("K".($z2).":K".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("K".($z).":K".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                            $sheet->mergeCells("L".($z2).":L".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("L".($z).":L".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                            $sheet->mergeCells("M".($z2).":M".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("M".($z).":M".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('N' . $z2, $v2->amount );
                                            $sheet->mergeCells("N".($z2).":N".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("N".($z).":N".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                            $sheet->mergeCells("O".($z2).":O".(($z2 + ($v2->count)) - 1));
                                            $sheet->cells("O".($z).":O".($z2 + ($v2->count) - 1), function($cells) {
                                                $cells->setAlignment('center');
                                                $cells->setValignment('center');
                                                $cells->setFontWeight('bold');
                                            });

                                            $z2_=$z2;
                                            foreach($v2->organizations_ as $k2_=>$v2_ ) {
                                                $descendants_count = sizeof($v2_->descendants);
                                                if(sizeof($v2_->descendants) > 0 ){
                                                    $descendants_count = $descendants_count * 3;
                                                    $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                    $sheet->mergeCells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("P".($z2_).":P".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                    }else{
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                    }
                                                    $sheet->mergeCells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("Q".($z2_).":Q".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                    $sheet->mergeCells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("R".($z2_).":R".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                    $sheet->mergeCells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("S".($z2_).":S".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                    $sheet->mergeCells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("T".($z2_).":T".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                    $sheet->mergeCells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)));
                                                    $sheet->cells("U".($z2_).":U".((($z2_ +$descendants_count) - 1)), function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                    });

                                                    foreach($v2_->descendants as $k1=>$v1) {
                                                        $sheet->setCellValue('V'.($z2_ + ($k1 * 3 )), $v1->name);
                                                        $sheet->mergeCells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2));
                                                        $sheet->cells('V'.($z2_ + ($k1 * 3 )).":V".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                        });

                                                        $sheet->setCellValue('W'.($z2_ + ($k1 * 3 )), $v1->nominated_count);
                                                        $sheet->mergeCells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2));
                                                        $sheet->cells('W'.($z2_ + ($k1 * 3 )).":W".(($z2_ + ($k1 * 3 )) + 2), function($cells) {
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                        });
                                                        $sheet ->setCellValue('X'.($z2_ + ($k1 * 3 )),trans('common::application.ratio'));
                                                        $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 1),trans('common::application.quantity'));
                                                        $sheet ->setCellValue('X'.(($z2_ + ($k1 * 3 )) + 2),trans('common::application.amount_'));

                                                        $sheet ->setCellValue('Y'.($z2_ + ($k1 * 3 )),$v1->ratio);
                                                        $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 1),$v1->quantity);
                                                        $sheet ->setCellValue('Y'.(($z2_ + ($k1 * 3 )) + 2),$v1->amount);

                                                    }


                                                    $z2_ = ($z2_ + $descendants_count) ;

                                                }
                                                else{
                                                    $sheet->setCellValue('P'.$z2_,$v2_->name);
                                                    if($level == Organization::LEVEL_MASTER_CENTER) {
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->child_nominated_count);
                                                    }else{
                                                        $sheet ->setCellValue('Q'.$z2_,$v2_->nominated_count);
                                                    }
                                                    $sheet ->setCellValue('R'.$z2_,$v2_->container_share);
                                                    $sheet ->setCellValue('S'.$z2_,$v2_->ratio);
                                                    $sheet ->setCellValue('T'.$z2_,$v2_->quantity);
                                                    $sheet ->setCellValue('U'.$z2_,$v2_->amount);
                                                    $sheet->setCellValue('V'.$z2_, ' - ');
                                                    $sheet->mergeCells("V".$z2_ .":Y" .$z2_);
                                                    $sheet->cells("V".$z2_ .":Y" .$z2_, function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });

                                                    $z2_++;
                                                }
                                            }

                                            $z2 = $z2 + $v2->count ;
                                        }

                                    }

                                    $z += $v->count ;
                                }
                            }
                            else{
                                $sheet->setCellValue('A' . $z, $k + 1);
                                $sheet->setCellValue('B' . $z, $v->repository_name);
                                $sheet->setCellValue('C' . $z, $v->repository_date);
                                $sheet->setCellValue('D'.$z, ' - ');
                                $sheet->mergeCells("D".$z .":Y" .$z);
                                $sheet->cells("D".$z .":Y" .$z, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $z++;
                            }
                        }else{
                            if(sizeof($v->projects) > 0){
                                $sheet->setCellValue('A' . $z, $k + 1);
                                $sheet->mergeCells("A".$z .":A" .( $z  + sizeof($v->projects) - 1 ));
                                $sheet->setCellValue('B' . $z, $v->repository_name);
                                $sheet->mergeCells("B".$z .":B" .( $z  + sizeof($v->projects) - 1 ));
                                $sheet->setCellValue('C' . $z, $v->repository_date);
                                $sheet->mergeCells("C".$z .":C" .( $z  + sizeof($v->projects) - 1 ));
                                $sheet->cells("A".($z).":C".($z), function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });

                                $z2=$z;
                                foreach($v->projects as $k2=>$v2 ) {
                                    $sheet->setCellValue('D' . $z2, $v2->name);
                                    $sheet->setCellValue('E' . $z2, $v2->category_name);
                                    $sheet->setCellValue('F' . $z2, $v2->organization_name);
                                    $sheet->setCellValue('G' . $z2, ($v2->organization_quantity));
                                    $sheet->setCellValue('H' . $z2, $v2->custom_organization_name);
                                    $sheet->setCellValue('I' . $z2, ($v2->custom_quantity));
                                    $sheet->setCellValue('J' . $z2, $v2->sponsor_name);
                                    $sheet->setCellValue('K' . $z2, $v2->quantity);
                                    $sheet->setCellValue('L' . $z2, $v2->currency_name);
                                    $sheet->setCellValue('M' . $z2, $v2->exchange_rate);
                                    $sheet->setCellValue('N' . $z2, $v2->amount );
                                    $sheet->setCellValue('O' . $z2, $v2->exchange_rate * $v2->amount);
                                    $sheet->setCellValue('P'.$z2, ' - ');
                                    $sheet->mergeCells("P".$z2 .":Y" .$z2);
                                    $sheet->cells("D".$z2 .":Y" .$z2, function($cells) {
                                        $cells->setAlignment('center');
                                        $cells->setValignment('center');
                                        $cells->setFontWeight('bold');
                                    });

                                    $z2++ ;
                                }

                                $z += sizeof($v->projects) ;

                            }
                            else{
                                $sheet->setCellValue('A' . $z, $k + 1);
                                $sheet->setCellValue('B' . $z, $v->repository_name);
                                $sheet->setCellValue('C' . $z, $v->repository_date);
                                $sheet->setCellValue('D'.$z, ' - ');
                                $sheet->mergeCells("D".$z .":Y" .$z);
                                $sheet->cells("D".$z .":Y" .$z, function($cells) {
                                    $cells->setAlignment('center');
                                    $cells->setValignment('center');
                                    $cells->setFontWeight('bold');
                                });
                                $z++;
                            }
                        }
                    }
                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['status' =>true ,'download_token' =>$token]);
        }

        return response()->json(['status' =>false ,'msg' =>trans('common::application.no organization')]);

    }

    // check person candidates of aid_repository project using repository_id;
    public function check(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $undefined_cards=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=\Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $total_ = 0;

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(!is_null($value['rkm_alhoy'])){
                                        if(in_array($value['rkm_alhoy'],$processed)){
                                            $duplicated++;
                                        }
                                        else{
                                            if(!is_null($value['rkm_alhoy'])){
                                                if(strlen($value['rkm_alhoy'])  <= 9) {
                                                    $card =(int)$value['rkm_alhoy'];
                                                    if(is_null(Person::HasRaw($card))) {
                                                        $invalid_cards[]=$value['rkm_alhoy'];
                                                    }else{
                                                        $processed[]=$value['rkm_alhoy'];
                                                        $success++;
                                                    }
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                        $total_++;
                                    }

                                }

                                if($total_ == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('aid-repository::application.All names entered are incorrect or not registered in the system')  ]);
                                }
                                else{
                                    $projects = Project::where('repository_id', $request->repository_id)->get();
                                    $candidates=[];
                                    $not_candidates=[];

                                    foreach ($projects as $key => $value){
                                        $candidates_=ProjectPerson::checkCandidates($value->id,$processed ,true);
                                        if(sizeof($candidates_) > 0){
                                            foreach ($candidates_ as $key_ => $value_){
                                                $candidates[]=(Object)[
                                                    'project_name' => $value->name,
                                                    'full_name' => $value_->full_name,
                                                    'id_card_number' => $value_->id_card_number,
                                                    'gender' => $value_->gender,
                                                    'marital_status_name' => $value_->marital_status_name,
                                                    'birthday' => $value_->birthday,
                                                    'country_name' => $value_->country_name,
                                                    'district_name' => $value_->district_name,
                                                    'region_name' => $value_->region_name,
                                                    'nearlocation_name' => $value_->nearlocation_name,
                                                    'square_name' => $value_->square_name,
                                                    'mosques_name' => $value_->mosques_name,
                                                    'street_address' => $value_->street_address,
                                                    'mobile' => $value_->mobile,
                                                    'phone' => $value_->phone,
                                                    'family_cnt' => $value_->family_cnt,
                                                    'organization_name' => $value_->organization_name,
                                                    'candidate_organization_name' => $value_->candidate_organization_name,
                                                    'status_name' => $value_->status_name,
                                                    'status' => $value_->status,
                                                    'reason' => $value_->reason,
                                                    'amount' => $value->amount,
                                                    'exchange_rate' => $value->exchange_rate,
                                                ];
                                            }
                                        }
                                        $not_candidates_=ProjectPerson::checkCandidates($value->id,$processed, false);
                                        if(sizeof($not_candidates_) > 0){
                                            foreach ($not_candidates_ as $key_ => $value_){
                                                $not_candidates[]=(Object)[
                                                    'project_name' => $value->name,
                                                    'full_name' => $value_->full_name,
                                                    'id_card_number' => $value_->id_card_number,
                                                    'status_name' => trans('aid-repository::application.Not filtered')
                                                ];
                                            }
                                        }
                                    }


                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($candidates , $not_candidates , $invalid_cards) {
                                        $excel->setTitle(trans('aid-repository::application.candidates list'));
                                        $excel->setDescription(trans('aid-repository::application.candidates list'));

                                        if(sizeof($candidates) > 0){
                                            $excel->sheet(trans('aid-repository::application.candidates list'), function($sheet) use($candidates) {
                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);
                                                $sheet->setOrientation('landscape');

                                                $sheet->setWidth([
                                                    'A'=>5   ,'B'=> 35 ,'C'=> 20 ,'D'=> 35,
                                                    'E'=> 35 ,'F'=> 20 ,'G'=> 40 ,'H'=> 15 ,'I'=> 15 ,'J'=> 15,
                                                    'K'=> 15 ,'L'=> 25 ,'M'=> 25 ,'N'=> 25 ,'O'=> 25 ,'P'=> 25 ,'Q'=> 45,'R'=> 25,'S'=> 25,'T'=> 25
                                                ]);

                                                $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'];

                                                foreach($map2 as $key ) {
                                                    $sheet->cells($key .'1', function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });
                                                }

                                                $sheet ->setCellValue('A1',trans('aid-repository::application.#'));
                                                $sheet ->setCellValue('B1',trans('aid-repository::application.name'));
                                                $sheet ->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                $sheet ->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                                                $sheet ->setCellValue('E1',trans('aid-repository::application.Organization_Name'));
                                                $sheet ->setCellValue('F1',trans('aid-repository::application.status'));
                                                $sheet ->setCellValue('G1',trans('aid-repository::application.amount_sh'));
                                                $sheet ->setCellValue('H1',trans('aid-repository::application.reason'));
                                                $sheet ->setCellValue('I1',trans('common::application.gender'));
                                                $sheet ->setCellValue('J1',trans('common::application.marital_status'));
                                                $sheet ->setCellValue('K1',trans('common::application.birthday'));
                                                $sheet ->setCellValue('L1',trans('common::application.country'));
                                                $sheet ->setCellValue('M1',trans('common::application.district'));
                                                $sheet ->setCellValue('N1',trans('common::application.region_'));
                                                $sheet ->setCellValue('O1',trans('common::application.nearlocation_'));
                                                $sheet ->setCellValue('P1',trans('common::application.square'));
                                                $sheet ->setCellValue('Q1',trans('common::application.mosques'));
                                                $sheet ->setCellValue('R1',trans('common::application.street_address_'));
                                                $sheet ->setCellValue('S1',trans('common::application.mobile'));
                                                $sheet ->setCellValue('T1',trans('common::application.phone'));
                                                $sheet ->setCellValue('U1',trans('aid-repository::application.family_count'));

                                                $z= 2;
                                                foreach($candidates as $k=>$v ) {
                                                    $sheet ->setCellValue('A'.$z,$k+1);
                                                    $sheet ->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                                                    $sheet ->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                                    $sheet ->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                                                    $sheet ->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                                                    $sheet ->setCellValue('F'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                                    if($v->status == 4){
                                                        $sheet ->setCellValue('G'.$z,0);
                                                    }else{
                                                        $sheet ->setCellValue('G'.$z,($v->amount * $v->exchange_rate));
                                                    }
                                                    $sheet ->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);
                                                    $sheet ->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $v->gender);
                                                    $sheet ->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                                                    $sheet ->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                                                    $sheet ->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                                                    $sheet ->setCellValue('M'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                                                    $sheet ->setCellValue('N'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                                                    $sheet ->setCellValue('O'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                                                    $sheet ->setCellValue('P'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                                                    $sheet ->setCellValue('Q'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                                                    $sheet ->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                                                    $sheet ->setCellValue('S'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                                    $sheet ->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                                    $sheet ->setCellValue('U'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);
                                                    $z++;
                                                }
                                            });
                                        }
                                        if(sizeof($not_candidates) > 0){
                                            $excel->sheet(trans('aid-repository::application.not candidates to all'), function($sheet) use($not_candidates) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet ->setCellValue('A1',trans('aid-repository::application.#'));
                                                $sheet ->setCellValue('B1',trans('aid-repository::application.full_name'));
                                                $sheet ->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                $sheet ->setCellValue('D1',trans('aid-repository::application.project_name'));
                                                $sheet ->setCellValue('E1',trans('aid-repository::application.status'));


                                                $z= 2;
                                                foreach($not_candidates as $k=>$v ) {
                                                    $sheet->setCellValue('A' . $z, $k+1);
                                                    $sheet->setCellValue('B' . $z, (is_null($v->full_name) || $v->full_name == ' ') ? '-' : $v->full_name);
                                                    $sheet->setCellValue('C' . $z, (is_null($v->id_card_number) || $v->id_card_number == ' ') ? '-' : $v->id_card_number);
                                                    $sheet->setCellValue('D' . $z, (is_null($v->project_name) || $v->project_name == ' ') ? '-' : $v->project_name);
                                                    $sheet->setCellValue('E' . $z, (is_null($v->status_name) || $v->status_name == ' ') ? '-' : $v->status_name);
                                                    $z++;
                                                }


                                            });
                                        }

                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('aid-repository::application.candidates list'), function($sheet) use($candidates) {
                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);
                                                $sheet->setOrientation('landscape');

                                                $sheet->setWidth([
                                                    'A'=>5   ,'B'=> 35 ,'C'=> 20 ,'D'=> 35,
                                                    'E'=> 35 ,'F'=> 20 ,'G'=> 15 ,'H'=> 40 ,'I'=> 15 ,'J'=> 15,
                                                    'K'=> 15 ,'L'=> 25 ,'M'=> 25 ,'N'=> 25 ,'O'=> 25 ,'P'=> 25 ,'Q'=> 25,'R'=> 45,'S'=> 25,'T'=> 25,'U'=> 25
                                                ]);

                                                $map2 = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'];

                                                foreach($map2 as $key ) {
                                                    $sheet->cells($key .'1', function($cells) {
                                                        $cells->setAlignment('center');
                                                        $cells->setValignment('center');
                                                        $cells->setFontWeight('bold');
                                                    });
                                                }

                                                $sheet ->setCellValue('A1',trans('aid-repository::application.#'));
                                                $sheet ->setCellValue('B1',trans('aid-repository::application.name'));
                                                $sheet ->setCellValue('C1',trans('aid-repository::application.id_card_number'));
                                                $sheet ->setCellValue('D1',trans('aid-repository::application.candidate_organization_name'));
                                                $sheet ->setCellValue('E1',trans('aid-repository::application.Organization_Name'));
                                                $sheet ->setCellValue('F1',trans('aid-repository::application.status'));
                                                $sheet ->setCellValue('G1',trans('aid-repository::application.amount_sh'));
                                                $sheet ->setCellValue('H1',trans('aid-repository::application.reason'));
                                                $sheet ->setCellValue('I1',trans('common::application.gender'));
                                                $sheet ->setCellValue('J1',trans('common::application.marital_status'));
                                                $sheet ->setCellValue('K1',trans('common::application.birthday'));
                                                $sheet ->setCellValue('L1',trans('common::application.country'));
                                                $sheet ->setCellValue('M1',trans('common::application.district'));
                                                $sheet ->setCellValue('N1',trans('common::application.region_'));
                                                $sheet ->setCellValue('O1',trans('common::application.nearlocation_'));
                                                $sheet ->setCellValue('P1',trans('common::application.square'));
                                                $sheet ->setCellValue('Q1',trans('common::application.mosques'));
                                                $sheet ->setCellValue('R1',trans('common::application.street_address_'));
                                                $sheet ->setCellValue('S1',trans('common::application.mobile'));
                                                $sheet ->setCellValue('T1',trans('common::application.phone'));
                                                $sheet ->setCellValue('U1',trans('aid-repository::application.family_count'));

                                                $z= 2;
                                                foreach($candidates as $k=>$v ) {
                                                    $sheet ->setCellValue('A'.$z,$k+1);
                                                    $sheet ->setCellValue('B'.$z,(is_null($v->full_name) || $v->full_name == ' ' ) ? '-' : $v->full_name);
                                                    $sheet ->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                                                    $sheet ->setCellValue('D'.$z,(is_null($v->candidate_organization_name) || $v->candidate_organization_name == ' ' ) ? '-' : $v->candidate_organization_name);
                                                    $sheet ->setCellValue('E'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);
                                                    $sheet ->setCellValue('F'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                                    if($v->status == 4){
                                                        $sheet ->setCellValue('G'.$z,0);
                                                    }else{
                                                        $sheet ->setCellValue('G'.$z,($v->amount * $v->exchange_rate));
                                                    }
                                                    $sheet ->setCellValue('H'.$z,(is_null($v->reason) || $v->reason == ' ' ) ? '-' : $v->reason);
                                                    $sheet ->setCellValue('I'.$z,(is_null($v->gender) || $v->gender == ' ' ) ? '-' : $v->gender);
                                                    $sheet ->setCellValue('J'.$z,(is_null($v->marital_status_name) || $v->marital_status_name == ' ' ) ? '-' : $v->marital_status_name);
                                                    $sheet ->setCellValue('K'.$z,(is_null($v->birthday) || $v->birthday == ' ' ) ? '-' : $v->birthday);
                                                    $sheet ->setCellValue('L'.$z,(is_null($v->country_name) || $v->country_name == ' ' ) ? '-' : $v->country_name);
                                                    $sheet ->setCellValue('M'.$z,(is_null($v->district_name) || $v->district_name == ' ' ) ? '-' : $v->district_name);
                                                    $sheet ->setCellValue('N'.$z,(is_null($v->region_name) || $v->region_name == ' ' ) ? '-' : $v->region_name);
                                                    $sheet ->setCellValue('O'.$z,(is_null($v->nearlocation_name) || $v->nearlocation_name == ' ' ) ? '-' : $v->nearlocation_name);
                                                    $sheet ->setCellValue('P'.$z,(is_null($v->square_name) || $v->square_name == ' ' ) ? '-' : $v->square_name);
                                                    $sheet ->setCellValue('Q'.$z,(is_null($v->mosques_name) || $v->mosques_name == ' ' ) ? '-' : $v->mosques_name);
                                                    $sheet ->setCellValue('R'.$z,(is_null($v->street_address) || $v->street_address == ' ' ) ? '-' : $v->street_address);
                                                    $sheet ->setCellValue('S'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                                    $sheet ->setCellValue('T'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                                    $sheet ->setCellValue('U'.$z,(is_null($v->family_cnt) || $v->family_cnt == ' ' ) ? '-' : $v->family_cnt);
                                                    $z++;
                                                }
                                            });
                                        }


                                    })->store('xlsx', storage_path('tmp/'));

                                    if( $total_ == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('aid-repository::application.Downloading the results file,'). trans('aid-repository::application.All the numbers were checked and their data was fetched') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('aid-repository::application.Downloading the results file,'). trans('aid-repository::application.Some numbers were successfully checked, as the total number of digits') .' :  '. $total_ . ' ,  '.
                                                trans('aid-repository::application.Number of names checked') .' :  ' .$success . ' ,  ' .
                                                trans('aid-repository::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('aid-repository::application.Number of duplicate names').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }



                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }


}
