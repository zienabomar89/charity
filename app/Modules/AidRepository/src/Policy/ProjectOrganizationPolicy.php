<?php

namespace AidRepository\Policy;

use Auth\Model\User;
use AidRepository\Model\ProjectOrganization;

class ProjectOrganizationPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aidRepository.organizationProjects.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function candidates(User $user)
    {
        if ($user->hasPermission('aidRepository.organizationProjects.candidates')) {
            return true;
        }
        
        return false;
    }
    
    public function nominate(User $user)
    {
        if ($user->hasPermission('aidRepository.organizationProjects.nominate')) {
            return true;
        }
        
        return false;
    }
    
}