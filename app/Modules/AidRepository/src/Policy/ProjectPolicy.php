<?php

namespace AidRepository\Policy;

use Auth\Model\User;
use AidRepository\Model\Project;

class ProjectPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aidRepository.projects.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('aidRepository.projects.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Project $project = null)
    {
        if ($user->hasPermission(['aidRepository.projects.view', 'aidRepository.projects.update'])) {
            return true;
        }
        
        return false;
    }
    
    public function candidates(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.candidates')) {
            return true;
        }
        
        return false;
    }
    
    public function nomination(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.nomination')) {
            return true;
        }
        
        return false;
    }
    
    public function updateCandidatesStatus(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.updateCandidatesStatus')) {
            return true;
        }
        
        return false;
    }
    
    public function deleteCandidates(User $user, Project $project = null)
    {
        if ($user->hasPermission('aidRepository.projects.deleteCandidates')) {
            return true;
        }
        
        return false;
    }
}

