<?php

namespace AidRepository\Policy;

use Auth\Model\User;
use AidRepository\Model\Repository;

class RepositoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aidRepository.repositories.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function manageCentral(User $user)
    {
        if ($user->hasPermission('aidRepository.repositories.manageCentral')) {
            return true;
        }
        
        return false;
    }
    
    
    public function create(User $user)
    {
        if ($user->hasPermission('aidRepository.repositories.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Repository $repository = null)
    {
        if ($user->hasPermission('aidRepository.repositories.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Repository $repository = null)
    {
        if ($user->hasPermission('aidRepository.repositories.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Repository $repository = null)
    {
        if ($user->hasPermission(['aidRepository.repositories.view', 'aidRepository.repositories.update'])) {
            return true;
        }
        
        return false;
    }
}

