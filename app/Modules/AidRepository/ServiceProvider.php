<?php

namespace App\Modules\AidRepository;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \AidRepository\Model\Repository::class => \AidRepository\Policy\RepositoryPolicy::class,
        \AidRepository\Model\Project::class => \AidRepository\Policy\ProjectPolicy::class,
        \AidRepository\Model\ProjectOrganization::class => \AidRepository\Policy\ProjectOrganizationPolicy::class,
    ];
    
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'aid-repository');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'aid-repository');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'auth'
        );*/
    }

}
