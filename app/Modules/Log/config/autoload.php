<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Log\\', __DIR__ . '/../src');
$loader->register();