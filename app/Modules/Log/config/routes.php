<?php

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0','namespace' => 'Log\Controller'], function() {

    Route::put('Language/setLocale/{id}', 'LanguageController@setLocale')->name('api.v1.0.notifications.setLocale');
    Route::get('Language/getLocale', 'LanguageController@getLocale')->name('api.v1.0.notifications.getLocale');

    Route::group(['prefix' => 'notifications'], function() {
        Route::post('checkCards', 'NotificationsController@checkCards')->name('api.v1.0.notifications.checkCards');
        Route::put('read/{id}', 'NotificationsController@read')->name('api.v1.0.notifications.read');
        Route::put('logout', 'NotificationsController@logout')->name('api.v1.0.notifications.logout');
        Route::post('header', 'NotificationsController@header')->name('api.v1.0.notifications.header');
        Route::post('filter', 'NotificationsController@filter')->name('api.v1.0.notifications.filter');
        Route::get('revokeUser', 'NotificationsController@revokeUser')->name('api.v1.0.notifications.revokeUser');
        Route::get('unreadMessages', 'NotificationsController@unreadMessages')->name('api.v1.0.notifications.unreadMessages');
    });
    Route::resource('notifications', 'NotificationsController');

    Route::group(['prefix' => 'logs'], function() {
        Route::get('getOptions', 'LogsController@getOptions')->name('api.v1.0.Logs.getOptions');
        Route::get('getCardsByUser', 'LogsController@getCardsByUser')->name('api.v1.0.Logs.getCardsByUser');
        Route::post('filter', 'LogsController@filter')->name('api.v1.0.logs.filter');
    });
    Route::resource('logs', 'LogsController');

    Route::group(['prefix' => 'Mailling'], function() {
        Route::post('destroyMesgs', 'MaillingController@destroyMesgs')->name('api.v1.0.Mailling.destroyMesgs');
        Route::delete('destroyFile/{id}', 'MaillingController@destroyFile')->name('api.v1.0.Mailling.destroyFile');
        Route::get('download/{id}', 'MaillingController@download')->name('api.v1.0.Mailling.download');
        Route::post('store', 'MaillingController@store')->name('api.v1.0.Mailling.store');
        Route::post('filter', 'MaillingController@filter')->name('api.v1.0.Mailling.filter');
        Route::post('getUsersList', 'MaillingController@getUsersList')->name('api.v1.0.Mailling.getUsersList');
    });
    Route::resource('Mailling', 'MaillingController');

    Route::group(['prefix' => 'Suggestion'], function() {
        Route::get('download/{id}', 'SuggestionController@download')->name('api.v1.0.logs.download');
        Route::delete('destroyFile/{id}', 'SuggestionController@destroyFile')->name('api.v1.0.Suggestion.destroyFile');
        Route::post('reply', 'SuggestionController@reply')->name('api.v1.0.Suggestion.reply');
        Route::post('store', 'SuggestionController@store')->name('api.v1.0.Suggestion.store');
        Route::post('filter', 'SuggestionController@filter')->name('api.v1.0.Suggestion.filter');
    });
    Route::resource('Suggestion', 'SuggestionController');
});


