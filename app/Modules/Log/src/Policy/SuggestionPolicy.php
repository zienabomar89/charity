<?php

namespace Log\Policy;

use Auth\Model\User;
use Log\Model\Suggestion;

class SuggestionPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('auth.suggestion.manage')) {
            return true;
        }

        return false;
    }

    public function manageSuggestion(User $user)
    {
        if ($user->hasPermission('auth.suggestion.manageSuggestion')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('auth.suggestion.create')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Suggestion $suggestion = null)
    {
        if ($user->hasPermission(['auth.suggestion.view'])) {
            return true;
        }

        return false;
    }

}
