<?php

namespace Log\Policy;

use Auth\Model\User;
use Log\Model\Log;

class LogPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('log.logs.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Log $log = null)
    {
        if ($user->hasPermission('log.logs.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Log $log = null)
    {
        if ($user->hasPermission('log.logs.view')) {
            return true;
        }
        
        return false;
    }
}

