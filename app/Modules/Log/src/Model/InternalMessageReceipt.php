<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;
use Log\Model\Mailling;
use Log\Model\InternalMessageAttachments;
use Illuminate\Support\Facades\Auth;


class InternalMessageReceipt extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_internal_message_receipt';
    protected $fillable = ['date', 'receipt_user_id','origin_msg_id', 'type','msg_id','is_read','created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];

    public static function fetch($options = array())
    {
        $condition = [];
        $filters = [ 'subject', 'details'];

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = ['char_internal_messages.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $user = Auth::user();
        $query = self::query()
            ->select('char_internal_message_receipt.*')
            ->orderBy('char_internal_message_receipt.created_at', 'DESC')
            ->where(function ($q) use ($user,$options,$condition){
                $q->where('receipt_user_id',$user->id);

                if(isset($options['users'])){
                    if(is_array($options['users'] ) && $options['users'] && $options['users'] !=null && $options['users'] !=""){
                        if(sizeof($options['users'] ) > 0){

                            $q->where(function ($q1_) use ($options){
                                $q1_->whereIn('char_internal_message_receipt.origin_msg_id',function ($q_5) use ($options){
                                    $q_5->select('id')
                                        ->from('char_internal_messages')
                                        ->whereIn('user_id', $options['users']);

                                });
                                $q1_->orwhereIn('char_internal_message_receipt.msg_id',function ($q0_) use ($options){
                                    $q0_->select('id')
                                        ->from('char_internal_messages')
                                        ->whereIn('user_id', $options['users']);

                                });
                            });


                        }
                    }
                }

                if(isset($options['subject'])){
                    if($options['subject'] && $options['subject'] !=null && $options['subject'] !=""){
                        $q->where(function ($q1_) use ($options){
                            $q1_->whereIn('char_internal_message_receipt.origin_msg_id',function ($q_5) use ($options){
                                $q_5->select('id')
                                    ->from('char_internal_messages')
                                    ->whereRaw("normalize_text_ar(char_internal_messages.details) like  normalize_text_ar(?)", "%".$options['subject']."%");

                            });
                            $q1_->orwhereIn('char_internal_message_receipt.msg_id',function ($q0_) use ($options){
                                $q0_->select('id')
                                    ->from('char_internal_messages')
                                    ->whereRaw("normalize_text_ar(char_internal_messages.details) like  normalize_text_ar(?)", "%".$options['subject']."%");

                            });
                        });

                    }
                }

                if(isset($options['details'])){
                    if($options['details'] && $options['details'] !=null && $options['details'] !=""){
                        $q->where(function ($q1_) use ($options){
                            $q1_->whereIn('char_internal_message_receipt.origin_msg_id',function ($q_5) use ($options){
                                $q_5->select('id')
                                    ->from('char_internal_messages')
                                    ->whereRaw("normalize_text_ar(char_internal_messages.details) like  normalize_text_ar(?)", "%".$options['details']."%");

                            });
                            $q1_->orwhereIn('char_internal_message_receipt.msg_id',function ($q0_) use ($options){
                                $q0_->select('id')
                                    ->from('char_internal_messages')
                                    ->whereRaw("normalize_text_ar(char_internal_messages.details) like  normalize_text_ar(?)", "%".$options['details']."%");

                            });
                        });

                    }
                }

                $to=null;
                $from=null;

                if(isset($options['to'])){
                    if(isset($options['to']) && $options['to'] !=null){
                        $to=date('Y-m-d',strtotime($options['to']));
                    }
                }

                if(isset($options['from'])){
                    if(isset($options['from']) && $options['from'] !=null){
                        $from=date('Y-m-d',strtotime($options['from']));
                    }
                }

                if($from != null && $to != null) {
                    $q->whereDate('char_internal_message_receipt.created_at', '>=', $from);
                    $q->whereDate('char_internal_message_receipt.created_at', '<=', $to);
                }elseif($from != null && $to == null) {
                    $q->whereDate('char_internal_message_receipt.created_at', '>=', $from);
                }elseif($from == null && $to != null) {
                    $q->whereDate('char_internal_message_receipt.created_at', '<=', $to);
                }

            })
            ->groupBy('receipt_user_id','origin_msg_id')
            ->paginate(10);

            $query->map(function ($q) {
                $msg =Mailling::query()
                    ->select('char_internal_messages.*','char_users.username AS sender')
                    ->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')
                    ->where('char_internal_messages.id',$q->msg_id)->first();
                $q->msg = $msg;

                return $q;
            });

            return $query;
    }
    public static function getById($id)
    {
        $user = Auth::user();
        $query = Mailling::query()
            ->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as sender ,char_internal_messages.*")
            // ->select('char_internal_messages.*','char_users.username AS sender')
            // ->join('char_internal_message_receipt', 'char_internal_messages.id', '=', 'char_internal_message_receipt.msg_id')
            ->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')
            ->where('char_internal_messages.id',$id)
            ->get();
        $query->map(function ($q) use ($user){
            $replays =Mailling::query()->select('char_internal_messages.*','char_users.username AS sender')
               ->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')
               ->join('char_internal_message_receipt', 'char_internal_messages.id', '=', 'char_internal_message_receipt.msg_id')
                ->where(['char_internal_messages.msg_replay_id'=>$q->id
                    //,'char_internal_message_receipt.receipt_user_id'=>$user['id']
            ])
                //->orWhere(['char_internal_messages.id'=>$q->id,'char_internal_messages.user_id'=>$user['id']])
                ->where('char_internal_messages.id','!=',$q->id)
                ->groupBy('char_internal_message_receipt.msg_id')->get();
            $replays->map(function ($q) {
            
                $attachs =InternalMessageAttachments::query()->select('char_internal_message_attachments.*','doc_files.*')->join('doc_files', 'doc_files.id', '=', 'char_internal_message_attachments.file_id')->where('char_internal_message_attachments.msg_id',$q->id)->get();
                    $q->attachs = $attachs; 
                
                // $receipts =InternalMessageReceipt::query()->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as text ,char_users.id")->join('char_users', 'char_internal_message_receipt.receipt_user_id', '=', 'char_users.id')->groupBy('char_users.id')->where('char_internal_message_receipt.msg_id',$q->id)->get();
                //     $q->receipts = $receipts;
                                       
            });

            $q->replays = $replays;

            $receipts =InternalMessageReceipt::query()
                ->selectRaw("CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as name ,char_users.id")->join('char_users', 'char_internal_message_receipt.receipt_user_id', '=', 'char_users.id')->groupBy('char_users.id')->where('char_internal_message_receipt.msg_id',$q->id)
                ->where('char_internal_message_receipt.receipt_user_id','!=',$user['id'])
                ->get();

             if($q->user_id != $user['id'])   
                $receipts[] = ['text'=>$q->sender,'id'=>$q->user_id];    
            
            $q->receipts = (object)$receipts;

            $attachs =InternalMessageAttachments::query()->select('char_internal_message_attachments.*','doc_files.*')->join('doc_files', 'doc_files.id', '=', 'char_internal_message_attachments.file_id')->where('char_internal_message_attachments.msg_id',$q->id)->get();
            $q->attachs = $attachs;
        });
        return $query;
    }


    public static function unReadCount()
    {

        $user = Auth::user();
        return self::query()
                   ->where(function ($q) use ($user){
                        $q->where('is_read',0);
                        $q->where('receipt_user_id', $user->id);
                   })
                   ->orderBy('created_at','asc')
                   ->count();

    }

}