<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    protected $table = 'char_notifications';
    protected $fillable = ['user_id', 'user_id_to', 'created_at', 'message','url', 'status'];
    public $timestamps = false;
    protected $dates = ['created_at'];

    public static function saveNotification($input){

        $user = \Auth::user();
        if(!is_null($input['user_id_to'])) {
            if(sizeof($input['user_id_to']) > 0) {
                foreach ($input['user_id_to'] as $item){
                    if(!is_null($item) && $item != ''){
                        if($item != $user->id){
                            $notice =new Notifications();
                            $notice->status =0;
                            $notice->created_at =date('Y-m-d H:i:s');
                            $notice->user_id =$user->id;
                            $notice->user_id_to =$item;
                            $notice->message =$input['message'];
                            $notice->url =$input['url'];
                            $notice->save();
                        }
                    }
                }
            }
        }
        return ;
    }

    public static function unRead(){
        $user = \Auth::user();
        return self::query()
                ->leftjoin('char_users','char_users.id','=','char_notifications.user_id')
                ->selectRaw("char_notifications.*.DATE_FORMAT(char_notifications.created_at,'%Y-%m-%d') as date,
                             DATE_FORMAT(char_notifications.created_at,'%h-%i-%s') as time,
                             DATE_FORMAT(char_notifications.created_at,'%p') as pm,
                             ifnull(char_users.full_name, '-') as sender_name")
                ->where(function ($q) use ($user) {
                    $q->where('status','1');
                    $q->where('user_id_to',$user->id);
                })
                ->orderBy('char_notifications.created_at', 'desc')
                ->get();
    }

    public static function unReadCount(){
        $user = \Auth::user();
        return self::where(['status'=>0])->where('user_id_to',$user->id)->count();
    }


    public  static function filter($options)
    {
        $user = \Auth::user();

        return self::query()
            ->join('char_users','char_notifications.user_id_to', '=', 'char_users.id')
            ->join('char_users as usr','char_notifications.user_id', '=', 'usr.id')
            ->where('char_notifications.user_id_to', '=', $user->id)
            ->orderBy('char_notifications.created_at','desc')
            ->selectRaw("
                         char_notifications.*,
                         DATE_FORMAT(char_users.created_at,'%Y-%m-%d') as date,
                         DATE_FORMAT(char_users.created_at,'%h-%i-%s') as time,
                         DATE_FORMAT(char_users.created_at,'%p') as pm,
                         CONCAT(ifnull(usr.firstname, ' '), ' ' ,ifnull(usr.lastname, ' '))as usr_name,
                         CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name")
            ->paginate(config('constants.records_per_page'));
    }

    public  static function lastNotifications()
    {
        $user = \Auth::user();

        return self::query()
            ->join('char_users','char_notifications.user_id_to', '=', 'char_users.id')
            ->join('char_users as usr','char_notifications.user_id', '=', 'usr.id')
            ->where('char_notifications.user_id_to', '=', $user->id)
            ->orderBy('char_notifications.created_at','desc')
            ->selectRaw("
                         char_notifications.*,
                         DATE_FORMAT(char_users.created_at,'%Y-%m-%d') as date,
                         DATE_FORMAT(char_users.created_at,'%h-%i-%s') as time,
                         DATE_FORMAT(char_users.created_at,'%p') as pm,
                         CONCAT(ifnull(usr.firstname, ' '), ' ' ,ifnull(usr.lastname, ' '))as usr_name,
                         CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name")
            ->limit(20)
            ->get();
    }

    public  static function header()
    {
        $user = \Auth::user();

        return self::query()
            ->join('char_users','char_notifications.user_id_to', '=', 'char_users.id')
            ->join('char_users as usr','char_notifications.user_id', '=', 'usr.id')
            ->where('char_notifications.user_id_to', '=', $user->id)
            ->orderBy('char_notifications.created_at','desc')
            ->selectRaw("
                         char_notifications.*,
                         DATE_FORMAT(char_users.created_at,'%Y-%m-%d') as date,
                         DATE_FORMAT(char_users.created_at,'%h-%i-%s') as time,
                         DATE_FORMAT(char_users.created_at,'%p') as pm,
                         CONCAT(ifnull(usr.firstname, ' '), ' ' ,ifnull(usr.lastname, ' '))as usr_name,
                         CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name")
            ->limit(20)
            ->get();
    }


}