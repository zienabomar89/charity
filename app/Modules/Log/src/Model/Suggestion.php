<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const STATUS_DRAFT      = 1;
    const STATUS_CANDIDATES = 2;
    const STATUS_EXECUTION= 3;
    const STATUS_CLOSED     = 4;

    
    protected $table = 'char_suggestions';
    protected $fillable = ['date', 'user_id', 'subject', 'details','status_id','created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'subject'  => 'required',
                'details'  => 'required',
            );
        }

        return self::$rules;
    }
    

    public static function filter($page,$options)
    {

        $condition = [];
        $filters = ['subject'];

        $user = \Auth::user();
        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = ['char_suggestions.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $query = self::query()
                    ->join('char_users', 'char_suggestions.user_id', '=', 'char_users.id')
                    ->selectRaw("char_suggestions.*,
                                           DATE_FORMAT(char_suggestions.created_at,'%Y-%m-%d') as date,
                                           DATE_FORMAT(char_suggestions.created_at,'%h-%i-%s') as time,
                                           DATE_FORMAT(char_suggestions.created_at,'%p') as pm,
                                           CONCAT(char_users.firstname,' ',char_users.lastname)as user_name");


        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $str = ['char_suggestions.subject'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        if(isset($options['user_id'])){
            if(isset($options['user_id']) && $options['user_id'] !=null && $options['user_id'] !="" && $options['user_id'] !=[] && sizeof($options['user_id']) != 0) {
                if($options['user_id'][0]==""){
                    unset($options['user_id'][0]);
                }
                if (sizeof($options['user_id']) > 0){
                    $query->whereIn('char_suggestions.user_id',$options['user_id']);
                }
            }

        }

        $to=null;
        $from=null;

        if(isset($options['to'])){
            if(isset($options['to']) && $options['to'] !=null){
                $to=date('Y-m-d',strtotime($options['to']));
            }
        }

        if(isset($options['from'])){
            if(isset($options['from']) && $options['from'] !=null){
                $from=date('Y-m-d',strtotime($options['from']));
            }
        }

        if($from != null && $to != null) {
            $query->whereDate('char_suggestions.created_at', '>=', $from);
            $query->whereDate('char_suggestions.created_at', '<=', $to);
        }elseif($from != null && $to == null) {
            $query->whereDate('char_suggestions.created_at', '>=', $from);
        }elseif($from == null && $to != null) {
            $query->whereDate('char_suggestions.created_at', '<=', $to);
        }
        $query->orderBy('char_suggestions.created_at', 'DESC');

        return $query->paginate(config('constants.records_per_page'));
    }
}