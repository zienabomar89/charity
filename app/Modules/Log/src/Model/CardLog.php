<?php

namespace Log\Model;

class CardLog  extends \Illuminate\Database\Eloquent\Model
{
	protected $table = 'char_users_card_log';
	protected $primaryKey='id';
	protected $fillable = [ 'user_id','id_card_number'];
	public $timestamps = false;

	public static function saveCardLog($user_id,$id_card_number){
		$query = self::query()->select('char_users_card_log.*')
		->where('char_users_card_log.user_id',$user_id);
		$count = $query->count();
		$cardLog = $query->where('char_users_card_log.id_card_number',$id_card_number)
		->first(); 

		if ($cardLog){
			$cardLog->created_at=date('Y-m-d H:i:s');
			$cardLog->update();
			return  $cardLog;

		}else {
			if ($count < 10){
			$user = \Auth::user();
			$cardLog1 = new \Log\Model\CardLog();
			$cardLog1->user_id= $user->id;
			$cardLog1->created_at=date('Y-m-d H:i:s');
			$cardLog1->id_card_number= $id_card_number;
			$cardLog1->save();
			return  $cardLog;
		}else {
			$cardLog2 = self::query()->select('char_users_card_log.*')
		    ->where('char_users_card_log.user_id',$user_id)
		    ->orderBy('char_users_card_log.created_at', 'ASC')
		    ->first(); 
			$cardLog2->created_at=date('Y-m-d H:i:s');
			$cardLog2->id_card_number= $id_card_number;
			$cardLog2->update();
			return  $cardLog2;
		}
		}
	}

	public static function filter()
	{
		$user = \Auth::user();
		$user_id = $user->id ;
		$query = self::query()->select('char_users_card_log.*')
		->where('char_users_card_log.user_id',$user_id)
		->orderBy('char_users_card_log.created_at', 'DESC'); 

		return $query->get();
	}

}
