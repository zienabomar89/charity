<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;
use Log\Model\InternalMessageReceipt;
use Illuminate\Support\Facades\Auth;


class Mailling extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_internal_messages';
    protected $fillable = ['date', 'msg_id', 'user_id', 'subject', 'details','msg_importance_id','msg_status_id','msg_replay_id','created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];
    
    public static function fetch($options = array())
    {

        $condition = [];
        $filters = [ 'subject', 'details'];

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = ['char_internal_messages.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $user = Auth::user();

        $query = self::query()
                  ->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')
                  ->selectRaw('char_internal_messages.* , char_users.firstname AS sender')
                  ->where(function ($q) use ($user,$options,$condition){
                      $q->where('user_id',$user->id);

                              if(isset($options['users'])){
                                    if(is_array($options['users'] ) && $options['users'] && $options['users'] !=null && $options['users'] !=""){
                                        if(sizeof($options['users'] ) > 0){
                                            $q->whereIn('char_internal_messages.id',function ($q_) use ($options){
                                                $q_->select('origin_msg_id')
                                                    ->from('char_internal_message_receipt')
                                                    ->whereIn('receipt_user_id', $options['users']);

                                            });
                                        }
                                    }
                                }

                      if (count($condition) != 0) {
                          $q->where(function ($Sq) use ($condition) {
                              $str = ['char_internal_messages.subject','char_internal_messages.details'];
                                  for ($i = 0; $i < count($condition); $i++) {
                                      foreach ($condition[$i] as $key => $value) {
                                          if(in_array($key, $str)) {
                                              $Sq->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                                          } else {
                                              $Sq->where($key, '=', $value);
                                          }
                                      }
                                  }
                              });
                      }
                      $to=null;
                      $from=null;

                      if(isset($options['to'])){
                          if(isset($options['to']) && $options['to'] !=null){
                              $to=date('Y-m-d',strtotime($options['to']));
                          }
                      }

                      if(isset($options['from'])){
                          if(isset($options['from']) && $options['from'] !=null){
                              $from=date('Y-m-d',strtotime($options['from']));
                          }
                      }

                      if($from != null && $to != null) {
                          $q->whereDate('char_internal_messages.created_at', '>=', $from);
                          $q->whereDate('char_internal_messages.created_at', '<=', $to);
                      }elseif($from != null && $to == null) {
                          $q->whereDate('char_internal_messages.created_at', '>=', $from);
                      }elseif($from == null && $to != null) {
                          $q->whereDate('char_internal_messages.created_at', '<=', $to);
                      }

                  })
                  ->orderBy('char_internal_messages.created_at', 'DESC')->groupBy('msg_replay_id')->paginate(10);

                $query->map(function ($q){
                    $ids =InternalMessageReceipt::query()->select('char_users.firstname')->join('char_users', 'char_internal_message_receipt.receipt_user_id', '=', 'char_users.id')->where('msg_id',$q->id)->groupBy('char_users.id')->get();
                    $q->receipts = $ids;
                    $q->replays_count =self::query()
                    ->where('msg_replay_id',$q->id)
                    ->orWhere('msg_replay_id',$q->msg_replay_id)
                    ->get()->count();

                    return $q;

                });

                return $query;
    }

    public static function getById($id)
    {
        $query = self::query()->select('char_internal_messages.*')
        ->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')
                  ->addSelect('char_users.firstname AS sender')->where('char_internal_messages.id',$id)->first();
        return $query;
    }



}