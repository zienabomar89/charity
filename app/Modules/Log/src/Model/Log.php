<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers;

class Log extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'app_logs';
    protected $fillable = ['action', 'user_id', 'ip','user_agent','url', 'created_at', 'message'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];
    
    public static function fetch($options = array())
    {
        $default = array(
            'action' => null,
            'from' => null,
            'to' => null,
            'user_id' => null,
            'ip' => null,
            'with_user_name' => false,
            'organization_id' => null,
            'with_organization_name' => false,
            'paginate' => 0,
        );
        $options = array_merge($default, $options);

        $query = self::query()->select('app_logs.*')->orderBy('app_logs.created_at', 'DESC');
        if ($options['with_user_name']) {
            $query->join('char_users', 'app_logs.user_id', '=', 'char_users.id')
                  ->addSelect('char_users.firstname', 'char_users.lastname', 'char_users.username');
        }
        if ($options['with_organization_name'] || $options['organization_id']) {
            if (!$options['with_user_name']) {
                $query->join('char_users', 'app_logs.user_id', '=', 'char_users.id');
            }
            $query->join('char_organizations', 'char_users.organization_id', '=', 'char_organizations.id');
            if ($options['with_organization_name']) {
                $query->addSelect('char_organizations.name AS organization_name');
            }
            if ($options['organization_id']) {
                $query->where('char_users.organization_id', '=', $options['organization_id']);
            }
        }
        if ($options['action']) {
            $query->where('app_logs.action', $options['action']);
        }
        if ($options['user_id']) {
            $query->where('app_logs.user_id', $options['user_id']);
        }
        if ($options['ip']) {
            $query->where('app_logs.ip', $options['ip']);
        }
        if ($options['from']) {
            $query->whereDate('app_logs.created_at', '>=', date('Y-m-d',strtotime($options['from'])));
        }
        if ($options['to']) {
            $query->whereDate('app_logs.created_at', '>=', date('Y-m-d',strtotime($options['to'])));
        }

        if ($options['paginate'] > 0) {
            return $query->paginate($options['paginate']);
        }
        
        return $query->get();
    }

    public static function saveNewLog($action,$message,$card= null ,$name= null)
    {
        $log = new \Log\Model\Log();
        $log->action=  $action;
        $user = \Auth::user();
        if($user){
            $log->user_id= $user->id;
        }
        $log->ip = request()->ip();
        $log->user_agent = request()->userAgent();
        $log->url = request()->url();

        $log->created_at=date('Y-m-d H:i:s');
        $log->message= $message;
        $log->save();
        return  $log;
    }

    public  static function filter($options)
    {
        $condition = [];
        $filters = ['action','ip', 'created_at', 'user_agent', 'message'];
        $user = \Auth::user();
        $UserType=$user->type;

        $organization_id=  $user->organization_id;

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ( $value != "" ) {
                    $data = ['app_logs.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $logs = self::query()
            ->join('char_users', function($join) use ($user){
                $join->on( 'app_logs.user_id', '=', 'char_users.id');
                if(!$user->super_admin){
                    $join->where(function ($q) use ($user) {
                        $q->where('char_users.parent_id', $user->id);
                        $q->orwhere('char_users.id', $user->id);
                    });
                }
            })
            ->join('char_organizations','char_organizations.id',  '=', 'char_users.organization_id');

        if($UserType == 2) {
            $logs->where(function ($anq) use ($organization_id,$user) {
                $anq->where('char_users.organization_id',$organization_id);
                $anq->orwherein('char_users.organization_id', function($decq) use($user) {
                    $decq->select('organization_id')
                        ->from('char_user_organizations')
                        ->where('user_id', '=', $user->id);
                });
            });

        }else{

            $logs->where(function ($anq) use ($organization_id) {
                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                $anq->wherein('char_users.organization_id', function($decq) use($organization_id) {
                    $decq->select('descendant_id')
                        ->from('char_organizations_closure')
                        ->where('ancestor_id', '=', $organization_id);
                });
            });
        }

        $to=null;
        $from=null;

        if(isset($options['to'])){
            if(isset($options['to']) && $options['to'] !=null){
                $to=date('Y-m-d',strtotime($options['to']));
            }
        }

        if(isset($options['from'])){
            if(isset($options['from']) && $options['from'] !=null){
                $from=date('Y-m-d',strtotime($options['from']));
            }
        }

        if($from != null && $to != null) {
            $logs->whereDate('app_logs.created_at', '>=', $from);
            $logs->whereDate('app_logs.created_at', '<=', $to);
        }elseif($from != null && $to == null) {
            $logs->whereDate('app_logs.created_at', '>=', $from);
        }elseif($from == null && $to != null) {
            $logs->whereDate('app_logs.created_at', '<=', $to);
        }

        if(isset($options['users'])){
            if(is_array($options['users'] ) && $options['users'] && $options['users'] !=null && $options['users'] !=""){
                if(sizeof($options['users'] ) > 0){
//                    $list=[];
//                    foreach ($options['user_id'] as $key=>$value){
//                        $list[]=$value['id'];
//                    }

                    $logs->whereIn('app_logs.user_id', $options['users']);
                }
            }
        }

        if(isset($options['actions'])){
            if(is_array($options['actions'] ) && $options['actions'] && $options['actions'] !=null && $options['actions'] !=""){
                if(sizeof($options['actions'] ) > 0){
//                    $list=[];
//                    foreach ($options['user_id'] as $key=>$value){
//                        $list[]=$value['id'];
//                    }

                    $logs->whereIn('app_logs.action', $options['actions']);
                }
            }
        }

        if (count($condition) != 0) {
            $logs =  $logs
                ->where(function ($q) use ($condition) {
                    $str = ['app_logs.action','app_logs.user_agent','app_logs.message'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }


        $logs->orderBy('app_logs.created_at','desc')
             ->selectRaw("app_logs.*,
                        CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name,
                        char_organizations.name as organization_name")
             ->groupBy(['app_logs.id']);

        if($options['filter_status'] == 'xlsx'){
            return $logs->get();
        }

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount'] : 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$logs->paginate()->total()); 

        return $logs->paginate($records_per_page);
    }


}