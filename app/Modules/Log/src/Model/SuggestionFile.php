<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;

class SuggestionFile extends Model
{

    protected $table = 'char_suggestion_attachments';
    protected $fillable = ['date', 'suggestion_id', 'file_id','created_at'];

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'suggestion_id'  => 'required|int',
            );
        }
        
        return self::$rules;
    }
    public static function fetch($suggestion_id)
    {

        return self::query()
               ->select('char_suggestion_attachments.*')
                   ->where('char_suggestion_attachments.suggestion_id', '=', $suggestion_id)
                   ->join('doc_files', 'char_suggestion_attachments.file_id', '=', 'doc_files.id')
                   ->addSelect(['doc_files.size', 'doc_files.mimetype', 'doc_files.name', 'doc_files.filepath',])
                   ->get();

    }


}