<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;
use Log\Model\Mailling;


class InternalMessageAttachments extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_internal_message_attachments';
    protected $fillable = ['date', 'file_id','msg_id','name','created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];
    
    // public static function fetch($option = array())
    // {      
    //     $query = self::query()->select('char_internal_message_receipt.*')->orderBy('char_internal_message_receipt.created_at', 'DESC')->where('receipt_user_id',$option['user_id'])->get();
    //     $query->map(function ($q) {
    //         $msg =Mailling::query()->select('char_internal_messages.*','char_users.username AS sender')->join('char_users', 'char_internal_messages.user_id', '=', 'char_users.id')->where('char_internal_messages.id',$q->msg_id)->first();
    //         $q->msg = $msg;
    //     });
    //     return $query;
    // }


}