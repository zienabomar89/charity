<?php

namespace Log\Model;

use Illuminate\Database\Eloquent\Model;
use Document\Model\File;

class SuggestionReplay extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const STATUS_DRAFT      = 1;
    const STATUS_CANDIDATES = 2;
    const STATUS_EXECUTION= 3;
    const STATUS_CLOSED     = 4;

    
    protected $table = 'char_suggestion_replays';
    protected $fillable = ['date', 'id', 'replay_user_id', 'suggestion_id', 'details','created_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at'];
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'suggestion_id'  => 'required',
            );
        }

        return self::$rules;
    }

    public static function fetch($suggestion_id)
    {

       $replays = self::query()
                    ->join('char_users','char_users.id',  '=', 'char_suggestion_replays.replay_user_id')
                    ->where('char_suggestion_replays.suggestion_id',$suggestion_id)
                    ->selectRaw("char_users.document_id , char_users.username ,char_suggestion_replays.* ,
                                          DATE_FORMAT(char_suggestion_replays.created_at,'%h-%i-%s') as time,
                                           DATE_FORMAT(char_suggestion_replays.created_at,'%p') as pm
                                         ")
                    ->get();
        

//        $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
//                           'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];
//
//        
//        foreach($replays as $key => $value) {
//                $value->filepath = 'emptyUser.png';
//                
//                if(!is_null($value->document_id)){
//                    $file = File::findOrFail($value->document_id);
//                    if(is_null($file)){
//                       $image = base_path('storage/app/' . $file->filepath);
//                       if (is_file($image)) {
//                           if (in_array($file->mimetype, $imageMimeTypes)) {
//                               $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
//                              if (in_array($fileType, $imageMimeTypes)) {
//                                  $value->filepath = $file->filepath;
//                              }
//                           }
//                        }
//                   }
//                }
//        }                          
           
        return $replays;
    }
}