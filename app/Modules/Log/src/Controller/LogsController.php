<?php

namespace Log\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log\Model\Log;
use Log\Model\CardLog;
use Auth\Model\User;
use Excel;

class LogsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // fetch logs according many options
    public function index(){
        $this->authorize('manage', Log::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $user = Auth::user();
        $request = request();
        $options = array(
            'with_user_name' => true,
            'with_organization_name' => $user->super_admin,
            'organization_id' => $user->super_admin? $request->query('organization_id') : $user->organization_id,
            'from' => $request->query('from'),
            'to' => $request->query('to'),
            'user_id' => $request->query('user'),
            'ip' => $request->query('ip'),
            'action' => $request->query('action'),
            'paginate' => 20,
        );
        
        return response()->json(Log::fetch($options));
        
    }

    // filter logs according many options
    public function filter(Request $request)
    {
        $this->authorize('manage', Log::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

      $items = Log::filter($request->all());

      if($request->filter_status =="xlsx"){

          $user = Auth::user();
          $super_admin= $user->super_admin;

        $token = md5(uniqid());
        \Excel::create('export_' . $token, function($excel) use($items,$super_admin) {
            $excel->setTitle(trans('log::application.Logs'));
            $excel->setDescription(trans('log::application.Logs'));
            $excel->sheet('logs', function($sheet) use($items,$super_admin) {

                $sheet->setRightToLeft(true);
                $sheet->setAllBorders('thin');
                $sheet->setfitToWidth(true);
                $sheet->setHeight(1,40);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                ]);

                $range="A1:G1";
                $sheet->getStyle($range)->applyFromArray(['font' => ['bold' => true]]);
                $sheet ->setCellValue('A1','م');
                $sheet ->setCellValue('B1',trans('log::application.Action'));
                $sheet ->setCellValue('C1',trans('log::application.Message'));
                $sheet ->setCellValue('D1',trans('log::application.User'));
                $sheet ->setCellValue('E1',trans('log::application.IP_Address'));
                $sheet ->setCellValue('F1',trans('log::application.Time'));
                $sheet ->setCellValue('G1',trans('log::application.Organization'));

                  $z= 2;
                foreach($items as $k=>$v )
                {
                    $sheet ->setCellValue('A'.$z,$k+1);
                    $sheet ->setCellValue('B'.$z,(is_null($v->action) || $v->action == ' ' ) ? '-' : $v->action);
                    $sheet ->setCellValue('C'.$z,(is_null($v->message) || $v->message == ' ' ) ? '-' : $v->message);
                    $sheet ->setCellValue('D'.$z,(is_null($v->user_name) || $v->user_name == ' ' ) ? '-' : $v->user_name);
                    $sheet ->setCellValue('E'.$z,(is_null($v->ip) || $v->ip == ' ' ) ? '-' : $v->ip);
                    $sheet ->setCellValue('F'.$z,(is_null($v->created_at) || $v->created_at == ' ' ) ? '-' : $v->created_at);
                     $sheet ->setCellValue('G'.$z,(is_null($v->organization_name) || $v->organization_name == ' ' ) ? '-' : $v->organization_name);


                    $z++;
                }

            });
        })->store('xlsx', storage_path('tmp/'));
          return response()->json(['download_token' => $token]);
      }
        return response()->json($items);

    }

    // get options to logs page
    public function getOptions(Request $request){
        return response()->json(['users' => User::userList()]);
    }

    // get Log by id
    public function show($id){
        $log = Log::findOrFail($id);
        $this->authorize('view', $log);
        return response()->json($log);
    }

    // delete exist Log model by id
    public function destroy(Request $request, $id){
        $log = Log::findOrFail($id);
        $this->authorize('delete', $log);
        return response()->json($log);
    }

    // fetch last card searched by user
    public function getCardsByUser(){
        $items = CardLog::filter();
        return response()->json($items);
    }

}
