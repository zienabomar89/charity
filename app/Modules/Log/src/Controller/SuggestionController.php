<?php

namespace Log\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log\Model\Suggestion;
use Log\Model\SuggestionReplay;
use Log\Model\SuggestionReplayController;
use Log\Model\SuggestionFile;
use Auth\Model\User;
use Document\Model\File;
use Excel;

class SuggestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter suggestion using many filter options
    public function filter(Request $request){
         $user = Auth::user();
         if($request->is_mine){
             $this->authorize('manage', Suggestion::class);
             $request['user_id'] = [$user->id] ;
         }else{
             $this->authorize('manageSuggestion', Suggestion::class);
         }
        return response()->json(Suggestion::filter($request['page'],$request->all()));
    }

    // create new suggestion
    public function store(Request $request){
        $this->authorize('create', Suggestion::class);
        $error = \App\Http\Helpers::isValid($request->all(),Suggestion::getValidatorRules());
        if($error)
            return response()->json($error);

        $user = \Auth::user();
        $suggest = Suggestion::create( [ 'subject'=> $request->subject, 'details'=> htmlspecialchars_decode($request->details),
                                           'date' => date('Y-m-d'), 'user_id'=> $user->id, 'status_id'=>1
                                        ]);
        if($suggest){

                $files =  $request->attach_files ? $request->attach_files :[];

                if(sizeof($files) > 0){
                    foreach ($files as $key => $value) {
                        SuggestionFile::create(['file_id'=> $value, 'suggestion_id' => $suggest->id, 'date' => date('Y-m-d')]);
                    }
                }

                $users =User::userHasPermission('auth.suggestion.manageSuggestion',array());
                $user_name =  $user->firstname .' ' .$user->lastname;
                if(sizeof($users)>0){
                    \Log\Model\Notifications::saveNotification([
                        'user_id_to' =>$users,
                        'url' =>"#/suggestions/manage/".$suggest->id,
                        'message' =>
                            trans('log::application.do'). '  ' .  $user_name . ' ، ' .
                            trans('log::application.Added a new proposal entitled') . ' : ' . $suggest->subject
                    ]);
                }

           return response()->json(['status'=>true , 'msg'=>  trans('common::application.action success')]);
        }
        return response()->json(['status'=>false , 'msg'=> trans('log::application.Add operation was not successful')]);
    }

    // create reply to suggestion
    public function reply(Request $request){

        $error = \App\Http\Helpers::isValid($request->all(),SuggestionReplay::getValidatorRules());
        if($error)
            return response()->json($error);

        $user = \Auth::user();
        $entry = new SuggestionReplay();
        $entry->replay_user_id= $user->id;
        $entry->created_at=date('Y-m-d H:i:s');
        $entry->date=date('Y-m-d H:i:s');
        $entry->details= $request->comment;
        $entry->suggestion_id= $request->suggestion_id;
        $entry->date =  date("Y-m-d");
      
        if($entry->save()){
            $suggest = Suggestion::findOrFail($entry->suggestion_id);
            $users =[$suggest->user_id];
            $user_name =  $user->firstname .' ' .$user->lastname;
            if(sizeof($users)>0){
                \Log\Model\Notifications::saveNotification([
                    'user_id_to' =>$users,
                    'url' =>"#/suggestions/show/".$suggest->id,
                    'message' => $suggest->subject . trans('log::application.do') .  $user_name . trans('log::application.Post a comment on a proposal you submit')
                ]);
            }

            return response()->json(['status'=>'success' , 'msg' =>trans('log::application.Saved successfully')]);
        }
        return response()->json(['status'=>'error' , 'msg' => trans('log::application.There was an error during the save')]);
    }

    // get suggestion by id
    public function show($id)
    {
        $entry = Suggestion::findOrFail($id);
        $entry->time = date('H:i:s' ,strtotime($entry->created_at) );
        $entry->pm = date('A' ,strtotime($entry->created_at) );
                
        if($entry){
            $entry->replays = SuggestionReplay::fetch($id);
            $entry->files = SuggestionFile::fetch($id);
        }
        return response()->json($entry);
    }

    // delete exist suggestion model by id
    public function destroy(Request $request, $id)
    {
        $entry = explode(',', $request->suggestion_ids);
        foreach ($entry as $id) {
            SuggestionReplay::where(['suggestion_id'=>$id])->delete();
            SuggestionFile::where(['suggestion_id'=>$id])->delete();
            $entry = Suggestion::findOrFail($id);
            $entry = \Log\Model\Suggestion::findOrFail($entry->id);

            if ($entry->status == \Log\Model\Suggestion::STATUS_CLOSED) {
                return response()->json(['status'=>'error' , 'msg' => trans('log::application.Cannot delete suggestion')]);
            }
            if (!$entry->delete()){
              return response()->json(['status'=>'error' , 'msg' =>trans('log::application.There was an error during the save')]);
          }
      }
      return response()->json(['status'=>'success' , 'msg' => trans('log::application.Saved successfully')]);
    }

    // download exist file by id
    public function download(Request $request, $id)
    {
        $file = File::findOrFail($id);
        if ($file) {
            return response()->download(base_path('storage/app/' . $file->filepath));
        }

        return response()->json(array('error' => 'Invalid file'));
    }

    // delete exist suggestion file by id
    public function destroyFile(Request $request, $id)
    {
        $file = File::findOrFail($id);

        \Illuminate\Support\Facades\Storage::delete($file->filepath);
        foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
            \Illuminate\Support\Facades\Storage::delete($revision->filepath);
        }
        if($file->delete()){
            return response()->json(['status'=>true]);

        }
        return response()->json(['status'=>false]);
    }

}
