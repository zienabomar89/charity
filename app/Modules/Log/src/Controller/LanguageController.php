<?php

namespace Log\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Setting\Model\Setting;
use Auth\Model\User;
use Illuminate\Support\Facades\Lang;

class LanguageController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // reset user current language
    public function setLocale($id)
    {

        $user = User::findOrFail(\Illuminate\Support\Facades\Auth::user()->id);
        $user->locale = $id;
        $user->save();

        App::setLocale($user->locale);
        Lang::setLocale($user->locale);
        $language_id = 1;
        if($user->locale == 'en'){
            $language_id = 2;
        }
        \Application\Model\I18nableEntity::setGlobalLanguageId($language_id);


        return response()->json(['status'=>true ,'msg'=>$user]);
    }

    // get user current language
    public function getLocale()
    {
        $user = User::findOrFail(\Illuminate\Support\Facades\Auth::user()->id);
        return response()->json(['status'=>true ,'locale'=>$user->locale]);
    }

}
