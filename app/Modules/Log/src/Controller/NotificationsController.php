<?php

namespace Log\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log\Model\InternalMessageReceipt;
use Log\Model\Mailling;
use Log\Model\Notifications;
use Auth\Model\User;
use Organization\Model\Organization;
use Common\Model\Person;

class NotificationsController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter notifications according many options
    // and get un read notifications
    public function filter(Request $request){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        return response()->json(['notifications' => Notifications::filter($request->all()) ,
                                 'unread_notifications_count' => Notifications::unReadCount()]);
    }

    // filter notifications according many options to header
    // and get un read notifications
    public function header(Request $request){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = Auth::user();
        $user->organization = $user->organizationName();

        $permissions = array();

        if ($user->super_admin) {
            $entries = \Auth\Model\Permission::all(array('code'));
            if ($entries) {

                $options['crossed'] = true;
                $permissions = [];
                $except = [];
                $allowed = $user->permissions();

                $allowed_map = [];
                $last_update = [];
                if ($allowed) {
                    foreach ($allowed as $entry) {
                        if(!isset($allowed_map[$entry->permission_id])){
                            $allowed_map[$entry->permission_id] = [];
                        }
                        $allowed_map[$entry->permission_id][]=$entry;
                    }

                    foreach ($allowed_map as $permission_id => $entry) {
                        if(sizeof($entry) > 1){
                            $max = $entry[0];
                            foreach ($entry as $key => $value) {
                                if (new \DateTime($max->created_at) < new \DateTime($value->created_at)) {
                                    $max = $value;
                                }
                            }
                            $last_update[]= $max;
                        }else{
                            $last_update[]= $entry[0];
                        }

                    }
                    foreach ($last_update as $entry) {
                        if($entry->allow == 1 || $entry->allow == '1'){
                            if(!in_array($entry->permission_id,$permissions)){
                                $permissions[] = $entry->code;
                            }
                        }else{
                            $except[]=$entry;
                        }
                    }
                }
//            foreach ($entries as $entry) {
//                $permissions[] = $entry->code;
//            }
            }
        } else {
            $permissions = Permission::allowedUserPermissionCode($user->id);
        }
        $user->permissions = $permissions;

        return response()->json(['user' => $user ,
            'notifications' => Notifications::lastNotifications() ,
            'unread_notifications_count' => Notifications::unReadCount()]);
    }

    // get Notifications By Id
    public function show($id){
        $entry = Notifications::findOrFail($id);
        return response()->json($entry);
    }

    // set notifications as read
    public function read($id){
        $entry = Notifications::findOrFail($id);
        $entry->status = 1;
        $entry->save();

        if($entry->save()){
            return response()->json(['status'=>true]);
        }
        return response()->json(['status'=>false]);
    }

    public function revokeUser(){

//        $user = \Auth::user();
//        $refreshTokenRepository = app(\Laravel\Passport\Bridge\RefreshTokenRepository::class);
//
//        $lastToken = \DB::table('oauth_access_tokens')
//                        ->where(function($q) use ($user){
//                            $q->where('user_id',$user->id);
//                            $q->where('revoked',0);
//                        })
//                        ->orderby('oauth_access_tokens.created_at','asc')
//                        ->first();
//
//        $tokens =\DB::table('oauth_access_tokens')
//                    ->where(function($q) use ($user){
//                        $q->where('user_id',$user->id);
//                        $q->where('revoked',0);
//                    })->get() ;
//
//        $tokens_list =[];
//        if (sizeof($tokens) > 1){
//            $user_name =  $user->firstname .' ' .$user->lastname;
//            foreach($tokens as $token) {
//                $tokens_list[] = $token->id;
////                $token->revoke();
////                $refreshTokenRepository->revokeRefreshToken($token->id);
//            }
//
//            \DB::table('oauth_access_tokens')
//                ->where(function ($q) use ($user) {
//                    $q->where('user_id', $user->id);
//                    $q->where('revoked', 0);
//                })->update(['revoked' => 1]);
//
//            if (sizeof($tokens_list) > 0){
//                \DB::table('oauth_refresh_tokens')->whereIn('access_token_id',$tokens_list)->update(['revoked'=>1]);
//                \DB::table('oauth_refresh_tokens')->whereIn('id',$tokens_list)->update(['revoked'=>1]);
//            }
//            \Log\Model\Log::saveNewLog('AUTOMATIC_LOGOUT',trans('common::application.Automatic logout') . ' : ' . $user_name);
//        }

        return response()->json(['status'=>true ]);

    }

    public function unreadMessages(){
        return response()->json(['status'=>true , 'count' => InternalMessageReceipt::unReadCount() ]);

    }

// log out user and destroy revoke token
    public function logout(\Illuminate\Http\Request $request)
    {

        $user = $request->user();
        $user_name =  $user->firstname .' ' .$user->lastname;
        $id = $request->user()->id;

        $refreshTokenRepository = app(\Laravel\Passport\Bridge\RefreshTokenRepository::class);
        $tokens =\DB::table('oauth_access_tokens')
            ->where(function($q) use ($id){
                $q->where('user_id',$id);
                $q->where('revoked',0);
            })->orderBy('updated_at','asc')->get() ;

        $tokens_list =[];
        foreach($tokens as $token) {
            $token_id = "'".$token->id."'";
            $tokens_list[] = $token_id;
//                $token->revoke();
//                $refreshTokenRepository->revokeRefreshToken($token->id);
            \DB::table('oauth_access_tokens')->where('id',$token->id)->update(['revoked'=>1]);
            \DB::table('oauth_refresh_tokens')
                ->where(function($q) use ($token){
                    $q->where('access_token_id',$token->id);
                    $q->orwhere('id',$token->id);
                })->update(['revoked'=>1]);

        }

        \Log\Model\Log::saveNewLog('LOGOUT',trans('common::application.logout') . ' : ' . $user_name);

        return response()->json(['status'=>true]);
        return redirect()->guest('account/login');
    }

    // filter cards according values id has update
    public function checkCards(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $cards = explode("\n",$request->cards);

        $items = Person::oldCardCheck($cards);

        if(sizeof($items) > 0){
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($items) {
                $excel->setTitle(trans('log::application.cards'));
                $excel->setDescription(trans('log::application.cards'));
                $excel->sheet(trans('log::application.cards'), function($sheet) use($items) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $range="A1:C1";
                    $sheet->getStyle($range)->applyFromArray(['font' => ['bold' => true]]);
                    $sheet ->setCellValue('A1','م');
                    $sheet ->setCellValue('B1',trans('log::application.old_id_card_number'));
                    $sheet ->setCellValue('C1',trans('log::application.id_card_number'));

                    $z= 2;
                    foreach($items as $k=>$v )
                    {
                        $sheet ->setCellValue('A'.$z,$k+1);
                        $sheet ->setCellValue('B'.$z,(is_null($v->old_id_card_number) || $v->old_id_card_number == ' ' ) ? '-' : $v->old_id_card_number);
                        $sheet ->setCellValue('C'.$z,(is_null($v->id_card_number) || $v->id_card_number == ' ' ) ? '-' : $v->id_card_number);
                        $z++;
                    }

                });
            })->store('xlsx', storage_path('tmp/'));

            return response()->json(['status' => 'success',
                'download_token' => $token ,
                'msg' =>
                    trans('log::application.there is cards update')  . ' ، '  .
                    trans('log::application.cards_count')  . ' : '  . sizeof($cards) . ' ، '  .
                    trans('log::application.cards_count')  . ' : '  . sizeof($items) . ' ، '  .
                    trans('log::application.downloading')

            ]);

        }

        return response()->json(["status" => 'failed', "msg"=>trans('setting::application.there is no cards update')]);

    }

}
