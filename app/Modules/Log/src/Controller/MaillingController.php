<?php

namespace Log\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Log\Model\Mailling;
use Log\Model\InternalMessageReceipt;
use Log\Model\InternalMessageAttachments;
use League\Flysystem\Exception;
use Auth\Model\User;
use Document\Model\File;

class MaillingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
    }

    // filter mail using many options (according type)
    public function filter(Request $request)
    {
        if($request->get('msg_type') == 1)
            $items = Mailling::fetch($request->all());
        else if($request->get('msg_type') == 2)
            $items = InternalMessageReceipt::fetch($request->all());

        return response()->json($items);
    }

    // get users list to mail page options
    public function getUsersList(Request $request)
    {
//        return response()->json(['users' => User::userList()]);
        return response()->json(['users'=>User::userListOfTags(array())]);
    }

// get users list to mail page options
    public function getReplayUsersList($id)
    {
//        return response()->json(['users' => User::userList()]);
        $receipt = InternalMessageReceipt::where('origin_msg_id',$id)->get();
        $except = [];
        if(sizeof($receipt) > 0){
            foreach ($receipt as $key => $value) {
                $except[]=  $value->receipt_user_id;
            }
        }

        return response()->json(['users'=>User::userListOfTags($except)]);
    }

    // create new mail
    public function store(Request $request)
    { 
        $data = $request;
        $user = Auth::user();
        $UserType=$user->type;
        $UserOrg = $user->organization_id;

        $receipt =[];

        $input = [
            'subject'=> $data['subject'],
            'details'=> htmlspecialchars_decode($data['details']),
            'date' => date('Y-m-d'),
            'user_id'=> $user->id,
            'msg_type_id'=>1,
            'msg_importance_id'=>1,
            'msg_status_id'=>1,
            'created_at' => date('Y-m-d H:i:s')
        ];
        if($data['msg_replay_id']){
            $input['msg_replay_id'] = $data['msg_replay_id'];
            $inserted=Mailling::create($input);
        }else{
            $inserted=Mailling::create($input);
            Mailling::where('id',$inserted->id)->update(['msg_replay_id'=>$inserted->id]);
        }

        $inst = false;
        $files =  $request->attach_files ? $request->attach_files :[];


        $users =[];
        if($inserted){
            if ($request->online){
                $selected  = $request->selected ;
                if ($selected == 'true'){
                    $users =  $data['users_to']?$data['users_to']:[];
                }else{
                    if ($selected == 'true'){
                        $users =  $data['users_to']?$data['users_to']:[];
                    }else{

                        $users_ids =[];
                        $logged_users = \DB::table('oauth_access_tokens')
                            ->where(function($q) use ($user){
                                $q->where('revoked',0);
                            })
                            ->get();

                        foreach ($logged_users as $k=>$v){
                            if(!in_array($v->user_id, $users_ids)) {
                                $users_ids[]=(int)$v->user_id;
                            }
                        }

                        $users_list = \DB::table('char_users')
                            ->where(function ($q) use ($user,$UserType,$UserOrg,$users_ids){
                                if(!empty($users_ids)){
                                    $q->wherein('id', $users_ids);
                                }
                                if($UserType == 2) {
                                    $q->where(function ($q) use ($user,$UserOrg) {
                                        $q->where('organization_id',$UserOrg);
                                        $q->orwhereIn('organization_id', function ($query) use ($user) {
                                            $query->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });
                                }
                                else{
                                    $q->where(function ($q) use ($user , $UserOrg) {
                                        $q->where('organization_id',$UserOrg);
                                        $q->orwhereIn('organization_id', function($decq) use($UserOrg) {
                                            $decq->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $UserOrg);
                                        });
                                    });
                                }
                            })->get();

                        foreach ($users_list as $k=>$v){
                            if(!in_array($v->id, $users)) {
                                $users[]=(int)$v->id;
                            }
                        }
                    }
                }
            }
            else{
                $users =  $data['users_to']?$data['users_to']:[];
            }
        }

        $inst =[];
        $inst_recipt_count = 0;
        $inst_attach = 0;
        if($inserted){

            if ($request->online){

            }else{
                foreach ($files as $key => $value) {
                    $attach = [
                        'file_id'=> $value,
                        'msg_id' => $inserted->id,
                        'date' => date('Y-m-d'),
                    ];
                    InternalMessageAttachments::create($attach);
                    $inst_attach++;
                }
            }

            if ($inst_attach != sizeof($files)){
                return response()->json(['success' =>  false  ,
                    'msg'=> trans('common::application.An error occurred during during process')]);
            }

            $origin_msg_id = 0;
            if(sizeof($users)>0){
                foreach ($users as $key => $value) {

                    if (is_object($value)){
                        $value = (array) $value;
                    }

                    if($data['msg_replay_id']){
                        $origin_msg_id = $data['msg_replay_id'];
                    }else{
                        $origin_msg_id = $inserted->id;
                    }

                    if($data['msg_replay_id']){
                        \Log\Model\Notifications::saveNotification([
                            'user_id_to' =>[$value['id']],
                            'url' =>'#/maillingSystem/'.$data['msg_replay_id'],
                            'message' => trans('log::application.He replied to a message titled ') . ' ' . $inserted->subject
                        ]);
                        // reply
                    }
                    else{
                        // new
                        \Log\Model\Notifications::saveNotification([
                            'user_id_to' =>[$value['id']],
                            'url' =>'#/maillingSystem/'.$inserted->id,
                            'message' => trans('log::application.Has sent a message entitled ') . ' ' . $inserted->subject
                        ]);
                    }


                    $receipt = [
                        'receipt_user_id'=> $value['id'],
                        'msg_id' => $inserted->id,
                        'origin_msg_id'=>(int)$origin_msg_id,
                        'date' => date('Y-m-d')
                    ];

                    $inst[]= $receipt;
                    $inst_recipt = InternalMessageReceipt::create($receipt);
                    if($inst_recipt)
                        InternalMessageReceipt::where(['origin_msg_id'=>$origin_msg_id,'receipt_user_id'=>$value['id']])->update(['is_read' => 0]);

                    $inst_recipt_count++;
                }
            }

        }

        if ($inst_recipt_count == 0){
            return response()->json(['success' =>  false ,
                'msg'=> trans('common::application.An error occurred during during process')]);

        }
        return response()->json(['success' =>  true,'msg'=> trans('common::application.action success')]);
    }

    // delete exist msgs model by id
    public function destroyMesgs(Request $request)
    {
        $msgs = $request->ids;
        $msg_type = $request->msg_type;
        $user = Auth::user();
        foreach ($msgs as $key => $value) {
            if($msg_type == 'inbox'){
                $deleteReceipt =InternalMessageReceipt::where(['msg_id'=>$value,'receipt_user_id'=>$user->id])->delete();
                if($deleteReceipt){
                    $response['success'] = true;
                    $response['msg'] = trans('common::application.action success');
                }else{
                    $response['success'] = false;
                    $response['msg'] = trans('common::application.An error occurred during during process') ;
                }
            }else{
                $reciepts = InternalMessageReceipt::where('msg_id',$value)->count();
                $attachments  = InternalMessageAttachments::where('msg_id',$value)->count();
                if($attachments != 0 || $reciepts != 0){
                    InternalMessageReceipt::where('msg_id',$value)->delete();
                    InternalMessageAttachments::where('msg_id',$value)->delete();
                }
                $msg = Mailling::findOrFail($value);
                if($msg)
                {
                    if(Mailling::destroy($value)){
                        $response['success'] = true;
                        $response['msg'] = trans('common::application.action success');
                    }else{
                        $response['success'] = false;
                        $response['msg'] = trans('common::application.An error occurred during during process') ;
                    }
                }
            }
        }
       
        return response()->json($response);
    }

    // get mail by id
    public function show($id)
    {
        $user = Auth::user();
        $msg = InternalMessageReceipt::getById($id);

        if($msg)
            InternalMessageReceipt::where(['origin_msg_id'=>$id,'receipt_user_id'=>$user->id])->update(['is_read' => 1]);
        return response()->json($msg);
    }

    // download exist file by id
    public function download(Request $request, $id)
    {
        $file = File::findOrFail($id);

        if ($file) {
            return response()->download(base_path('storage/app/' . $file->filepath));
        }

        return response()->json(array(
            'error' => 'Invalid file',
        ));
    }

    // delete exist mail file by id
    public function destroyFile(Request $request, $id)
    {
        $file = File::findOrFail($id);

         \Illuminate\Support\Facades\Storage::delete($file->filepath);
        foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
            \Illuminate\Support\Facades\Storage::delete($revision->filepath);
        }
        if($file->delete()){
            return response()->json(['status'=>true]);

        }
        return response()->json(['status'=>false]);
    }

}


