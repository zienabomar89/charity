<?php

namespace App\Modules\Log;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Log\Model\Log::class => \Log\Policy\LogPolicy::class,
        \Log\Model\Suggestion::class => \Log\Policy\SuggestionPolicy::class,
    ];
    
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'log');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'log');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'auth'
        );*/
    }
}
