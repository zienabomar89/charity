
<form name="form" class="form-horizontal" novalidate="" category="form" ng-submit="submit()" ng-controller="<?= $controller ?>">
    <fieldset>
        <legend><?= trans('document::application.information') ?></legend>

        <div class="form-group" ng-class="errorClass('name')">
            <label for="username" class="col-md-2 control-label"><?= trans('document::application.name') ?></label>
            <div class="col-md-4">
                <input type="text" class="form-control has-error" id="name" name="name" placeholder="<?= trans('document::application.name') ?>" value="{{name}}" 
                       ng-model="category.name" ng-required="true">
                <span class="help-block" ng-show="form.name.$invalid && form.name.$dirty">{{errorMessage('name')}}</span>
            </div>
        </div>
    </fieldset>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn blue"><?= trans('document::application.save') ?></button>
        </div>
    </div>
</form>