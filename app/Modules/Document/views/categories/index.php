<div ng-controller="CategoreisController">
    <div class="portlet light portlet-fit bordered">
        <div class="portlet-title">
            <div class="caption">
                <span class="glyphicon glyphicon-tags"></span>
                <?= trans('document::application.categories') ?>
            </div>
            <div class="actions">
                <a uib-tooltip="<?= trans('document::application.new') ?>" class="btn btn-outline green" href="#/doc/categories/new"><span class="glyphicon glyphicon-plus"></span>
                    <span class="hidden-sm hidden-md  hidden-xs"><?= trans('document::application.new') ?></span></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="alert alert-success" ng-show="success" category="alert">
                {{message}}
            </div>
            <div class="alert alert-danger" ng-show="error" category="alert">
                {{message}}
            </div>
            <div class="table-scrollable">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th><?= trans('document::application.category') ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody ng-repeat="category in categories">
                        <tr>
                            <td>{{category.name}}</td>
                            <td>
                                <a class="btn btn-default btn-xs" href="{{'#/doc/categories/edit/' + category.id}}"><?= trans('document::application.edit') ?></a>
                                <button class="btn btn-danger btn-xs" ng-click="deleteCategory(category, $index)"><?= trans('document::application.delete') ?></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>