<div class="row" ng-controller="FileEditController">
    <div class="col-md-6">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption"><?= trans('document::application.edit') ?></div>
            </div>
            <div class="portlet-body">
                <form name="form" class="form-horizontal" novalidate="" category="form" ng-submit="submit()">
                    <div class="form-group" ng-class="errorClass('category_id')">
                        <label for="category_id" class="col-md-3 control-label"><?= trans('document::application.category') ?></label>
                        <div class="col-md-9">
                            <select class="form-control" id="category_id" name="category_id" ng-model="file.category_id" ng-required="true">
                                <option ng-repeat="category in categories" value="{{category.id}}" ng-value="file.category_id">{{category.name}}</option>
                            </select>
                            <span class="help-block" ng-show="form.category_id.$invalid && form.category_id.$dirty">{{errorMessage('category_id')}}</span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="errorClass('name')">
                        <label for="name" class="col-md-3 control-label"><?= trans('document::application.name') ?></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="name" name="name" placeholder="<?= trans('document::application.name') ?>" value="{{name}}" 
                                   ng-model="file.name" ng-required="true">
                            <span class="help-block" ng-show="form.name.$invalid && form.name.$dirty">{{errorMessage('name')}}</span>
                        </div>
                    </div>
                    <div class="form-group" ng-class="errorClass('description')">
                        <label for="description" class="col-md-3 control-label"><?= trans('document::application.description') ?></label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="description" name="description" placeholder="<?= trans('document::application.description') ?>"
                                      ng-model="file.description">{{description}}</textarea>
                            <span class="help-block" ng-show="form.description.$invalid && form.description.$dirty">{{errorMessage('description')}}</span>
                        </div>
                    </div>
                    <script type="text/ng-template" id="customTemplate.html">
                        <a><span ng-bind-html="match.model.name | uibTypeaheadHighlight:query"></span></a>
                    </script>
                    <div class="form-group">
                        <label for="tags" class="col-md-3 control-label"><?= trans('document::application.tags') ?>
                        <span ng-show="loadingLocations" class="glyphicon glyphicon-refresh"></span></label>
                        <div class="col-md-9">
                            <div class="margin-bottom-15">
                                <span ng-repeat="tag in tags">
                                    <span class="label label-info label-sm">{{tag.name}}
                                        <span class="glyphicon glyphicon-remove" ng-click="removeTag(tag, $index)"></span></span> 
                                </span>
                            </div>
                            <div class="margin-bottom-15">
                                <input type="text" ng-model="selectedTag" uib-typeahead="tag for tag in getTags($viewValue)" typeahead-loading="loadingLocations" typeahead-no-results="noResults" typeahead-on-select="tagSelected($item, $model, $label, $event)" typeahead-template-url="customTemplate.html" typeahead-show-hint="true" class="form-control">
                            </div>
                            <div ng-show="noResults">
                                <a ng-click="addTag(selectedTag)"><span class="glyphicon glyphicon-plus"></span> <b>{{selectedTag}}</b></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn blue"><?= trans('document::application.save') ?></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption"><?= trans('document::application.information') ?></div>
                <div class="actions">
                    <a doc-file-download data-url="/doc/files/download/{{file.id}}" class="btn btn-outline btn-lg green">
                        <span class="fa fa-download"></span>
                        <?= trans('document::application.download') ?></a>
                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#upload">
                        <span class="fa fa-upload"></span>
                        <?= trans('document::application.upload_new_version') ?></button>
                </div>
            </div>
            <div class="portlet-body">
                <dl class="dl-horizontal">
                    <dt><?= trans('document::application.path') ?></dt>
                    <dd>{{file.filepath}}</dd>
                    <dt><?= trans('document::application.type') ?></dt>
                    <dd>{{file.mimetype}}</dd>
                    <dt><?= trans('document::application.size') ?></dt>
                    <dd>{{file.size| Filesize}}</dd>
                    <dt><?= trans('document::application.created_at') ?></dt>
                    <dd>{{file.created_at}}</dd>
                    <dt><?= trans('document::application.updated_at') ?></dt>
                    <dd>{{file.updated_at}}</dd>
                </dl>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light portlet-fit bordered">
            <div class="portlet-title">
                <div class="caption"><?= trans('document::application.revisions') ?></div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th><?= trans('document::application.size') ?></th>
                                <th><?= trans('document::application.type') ?></th>
                                <th><?= trans('document::application.created_at') ?></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody ng-repeat="revision in revisions">
                            <tr>
                                <td>{{revision.size | Filesize}}</td>
                                <td>{{revision.mimetype}}</td>
                                <td>{{revision.created_at}}</td>
                                <td>
                                    <a class="btn btn-default btn-xs" doc-file-download data-url="/doc/revisions/download/{{revision.id}}"><?= trans('document::application.download') ?></a>
                                    <button class="btn btn-danger btn-xs" ng-click="deleteRevision(revision, $index)"><?= trans('document::application.delete') ?></button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="upload" aria-labelledby="uploadLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="uploadLabel"><?= trans('document::application.upload_new_version') ?>
                    <span class="caption-subject bold uppercase"><?= trans('document::application.upload_queue') ?></span>
                    <span class="caption-helper"><?= trans('document::application.queue_length') ?>: {{ uploader.queue.length}}</span></h4>
            </div>
            <div class="modal-body">
                <div class="margin-bottom-20">
                    <input type="file" nv-file-select="" uploader="uploader" multiple>
                </div>
                <div ng-show="uploader.isHTML5">
                    <div nv-file-drop="" uploader="uploader">
                        <div nv-file-over="" uploader="uploader" over-class="file-drop-zone-over" class="file-drop-zone margin-bottom-20"> <?= trans('document::application.drop_files_here') ?> </div>
                    </div>
                </div>
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th width="50%"><?= trans('document::application.name') ?></th>
                                <th ng-show="uploader.isHTML5"><?= trans('document::application.size') ?></th>
                                <th ng-show="uploader.isHTML5"><?= trans('document::application.progress') ?></th>
                                <th><?= trans('document::application.status') ?></th>
                                <th><?= trans('document::application.actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in uploader.queue">
                                <td>
                                    <strong>{{ item.file.name}}</strong>
                                </td>
                                <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size / 1024 / 1024|number:2 }} MB</td>
                                <td ng-show="uploader.isHTML5">
                                    <div class="progress progress-sm" style="margin-bottom: 0;">
                                        <div class="progress-bar progress-bar-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span ng-show="item.isSuccess" class="text-success">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </span>
                                    <span ng-show="item.isCancel" class="text-info">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                    </span>
                                    <span ng-show="item.isError" class="text-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </span>
                                </td>
                                <td nowrap>
                                    <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                        <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.upload') ?> </button>
                                    <button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
                                        <span class="glyphicon glyphicon-ban-circle"></span> <?= trans('document::application.cancel') ?> </button>
                                    <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                        <span class="glyphicon glyphicon-trash"></span> <?= trans('document::application.remove') ?> </button>
                                    <button type="button" class="btn btn-success btn-xs" ng-click="download(item.id)" ng-disabled="!item.isSuccess">
                                        <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.download') ?> </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <p><?= trans('document::application.queue_progress') ?>:</p>
                    <div class="progress progress-sm" style="">
                        <div class="progress-bar progress-bar-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                    <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.upload_all') ?> </button>
                <button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
                    <span class="glyphicon glyphicon-ban-circle"></span> <?= trans('document::application.cancel_all') ?> </button>
                <button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                    <span class="glyphicon glyphicon-trash"></span> <?= trans('document::application.remove_all') ?> </button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= trans('document::application.close') ?></button>
            </div>
        </div>
    </div>
</div>