<div class="row" ng-controller="FileCreateController" nv-file-drop="" uploader="uploader" filters="queueLimit, customFilter">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase">Select files</span>
                    <span class="caption-helper hide"></span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-2"><?= trans('document::application.category') ?></label>
                        <div class="col-sm-10">
                            <select class="form-control" id="category_id" name="category_id" 
                                    ng-model="category_id" ng-required="true">
                                <option ng-repeat="category in categories" value="{{category.id}}">{{category.name}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2"><?= trans('document::application.file') ?></label>
                        <div class="col-sm-10">
                            <div class="margin-bottom-20">
                                <input type="file" nv-file-select="" uploader="uploader" multiple>
                            </div>
                            <div ng-show="uploader.isHTML5">
                                <div nv-file-drop="" uploader="uploader">
                                    <div nv-file-over="" uploader="uploader" over-class="file-drop-zone-over" class="file-drop-zone margin-bottom-20"> <?= trans('document::application.drop_files_here') ?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption font-green-sharp">
                    <i class="icon-settings font-green-sharp"></i>
                    <span class="caption-subject bold uppercase"><?= trans('document::application.upload_queue') ?></span>
                    <span class="caption-helper"><?= trans('document::application.queue_length') ?>: {{ uploader.queue.length}}</span>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="#" data-original-uib-tooltip=""> </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th width="50%"><?= trans('document::application.name') ?></th>
                                <th ng-show="uploader.isHTML5"><?= trans('document::application.size') ?></th>
                                <th ng-show="uploader.isHTML5"><?= trans('document::application.progress') ?></th>
                                <th><?= trans('document::application.status') ?></th>
                                <th><?= trans('document::application.actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="item in uploader.queue">
                                <td>
                                    <strong>{{ item.file.name}}</strong>
                                </td>
                                <td ng-show="uploader.isHTML5" nowrap>{{ item.file.size / 1024 / 1024|number:2 }} MB</td>
                                <td ng-show="uploader.isHTML5">
                                    <div class="progress progress-sm" style="margin-bottom: 0;">
                                        <div class="progress-bar progress-bar-info" role="progressbar" ng-style="{ 'width': item.progress + '%' }"></div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <span ng-show="item.isSuccess" class="text-success">
                                        <i class="glyphicon glyphicon-ok"></i>
                                    </span>
                                    <span ng-show="item.isCancel" class="text-info">
                                        <i class="glyphicon glyphicon-ban-circle"></i>
                                    </span>
                                    <span ng-show="item.isError" class="text-danger">
                                        <i class="glyphicon glyphicon-remove"></i>
                                    </span>
                                </td>
                                <td nowrap>
                                    <button type="button" class="btn btn-success btn-xs" ng-click="item.upload()" ng-disabled="item.isReady || item.isUploading || item.isSuccess">
                                        <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.upload') ?> </button>
                                    <button type="button" class="btn btn-warning btn-xs" ng-click="item.cancel()" ng-disabled="!item.isUploading">
                                        <span class="glyphicon glyphicon-ban-circle"></span> <?= trans('document::application.cancel') ?> </button>
                                    <button type="button" class="btn btn-danger btn-xs" ng-click="item.remove()">
                                        <span class="glyphicon glyphicon-trash"></span> <?= trans('document::application.remove') ?> </button>
                                    <button type="button" class="btn btn-success btn-xs" doc-file-download data-url="/doc/files/download/{{item.id}}" ng-disabled="!item.isSuccess">
                                        <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.download') ?> </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div>
                    <p><?= trans('document::application.queue_progress') ?>:</p>
                    <div class="progress progress-sm" style="">
                        <div class="progress-bar progress-bar-info" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                    </div>
                </div>
                <button type="button" class="btn btn-success btn-s" ng-click="uploader.uploadAll()" ng-disabled="!uploader.getNotUploadedItems().length">
                    <span class="glyphicon glyphicon-upload"></span> <?= trans('document::application.upload_all') ?> </button>
                <button type="button" class="btn btn-warning btn-s" ng-click="uploader.cancelAll()" ng-disabled="!uploader.isUploading">
                    <span class="glyphicon glyphicon-ban-circle"></span> <?= trans('document::application.cancel_all') ?> </button>
                <button type="button" class="btn btn-danger btn-s" ng-click="uploader.clearQueue()" ng-disabled="!uploader.queue.length">
                    <span class="glyphicon glyphicon-trash"></span> <?= trans('document::application.remove_all') ?> </button>
            </div>
        </div>
    </div>
</div>