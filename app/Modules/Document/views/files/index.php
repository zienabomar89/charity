<div ng-controller="FilesController" class="portlet light portlet-fit bordered">
    <div class="portlet-title">
        <div class="caption">
            <span class="glyphicon glyphicon-file"></span>
                <?= trans('document::application.files') ?>
        </div>
        <div class="actions">
            <a uib-tooltip="<?= trans('document::application.new') ?>"class="btn btn-default btn-outline green" href="#/doc/files/new"><span class="glyphicon glyphicon-plus"></span>
            <span class="hidden-sm hidden-md  hidden-xs"><?= trans('document::application.new') ?></span></a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="alert alert-success" ng-show="success" role="alert">
            {{message}}
        </div>
        <div class="alert alert-danger" ng-show="error" role="alert">
            {{message}}
        </div>
        
        <div class="well">
            <form method="get" action="" class="form-inline" ng-submit="search()">
                <fieldset>
                    <legend><?= trans('document::application.search') ?></legend>
                    <input type="text" ng-model="srchName" placeholder="<?= trans('document::application.name') ?>" class="form-control">
                    <input type="text" ng-model="srchTag" placeholder="<?= trans('document::application.tag') ?>" class="form-control">
                    <select class="form-control" ng-model="srchCategoryId">
                        <option><?= trans('document::application.category') ?></option>
                        <option ng-repeat="category in categories" value="{{category.id}}">{{category.name}}</option>
                    </select>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></button>
                </fieldset>
            </form>
        </div>

        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th><?= trans('document::application.file') ?></th>
                    <th><?= trans('document::application.category') ?></th>
                    <th><?= trans('document::application.created_at') ?></th>
                    <th><?= trans('document::application.size') ?></th>
                    <th></th>
                </tr>
            </thead>
            <tbody ng-repeat="file in files">
                <tr>
                    <td>{{file.name}}<br>
                        <small class="text-muted">{{file.mimetype_name}}</small></td>
                    <td>{{file.category_name}}</td>
                    <td>{{file.created_at}}</td>
                    <td>{{file.size| Filesize}}</td>
                    <td>
                        <a class="btn btn-success btn-xs" doc-file-download data-url="/doc/files/download/{{file.id}}"><span class="glyphicon glyphicon-download"></span> <?= trans('document::application.download') ?>
                            <a class="btn btn-default btn-xs" href="{{'#/doc/files/edit/' + file.id}}"><?= trans('document::application.edit') ?></a>
                            <button class="btn btn-danger btn-xs" ng-click="deleteFile(file, $index)"><?= trans('document::application.delete') ?></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <ul class="pagination pagination-sm" uib-pagination ng-change="pageChanged()" boundary-links="true" total-items="totalItems" items-per-page="itemsPerPage" ng-model="currentPage" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></ul>
    </div>    
</div>