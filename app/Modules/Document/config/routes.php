<?php

Route::group(['prefix' => '/doc/files/' , 'namespace' => 'Document\Controller'], function() {
    Route::get('download/{id}', 'UnAuthController@download');
    Route::get('downloadWithOutToken/{id}', 'UnAuthController@downloadWithOutToken');
    //Route::get('download/{id}', 'FilesController@download');
    Route::get('preview/{id}', 'FilesController@preview');
    Route::get('thumbnail/{id}', 'FilesController@thumbnail');
    Route::get('/doc/revisions/download/{id}', 'RevisionsController@download');
    Route::get('casesImage/{id}/{type}', 'FilesController@casesImage');
    Route::get('reportsImage/{id}', 'FilesController@reportsImage');
    Route::get('getImage/{id}', 'FilesController@getImage');
    Route::get('userImage', 'FilesController@userImage');
});

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/doc' ,'namespace' => 'Document\Controller'], function() {
    Route::resource('files', 'FilesController');
    Route::resource('tags', 'TagsController');
    Route::resource('categories', 'CategoriesController');
    Route::resource('attributes', 'AttributesController');
    Route::resource('revisions', 'RevisionsController');
    Route::resource('file-tags', 'FileTagsController', [
        'only' => ['index'],
    ]);
});

?>
