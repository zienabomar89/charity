<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Document\\', __DIR__ . '/../src');
$loader->register();