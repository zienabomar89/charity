<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;

class FileStatus extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_files_status';
}