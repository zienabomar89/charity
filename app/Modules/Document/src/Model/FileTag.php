<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FileTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_files_tags';
    
    protected $fillable = ['file_id', 'tag_id'];
    
    public $incrementing = false;
    public $timestamps   = false;
    
    public static function deleteFileTags($file_id)
    {
        return DB::table('doc_files_tags')->where('file_id', '=', $file_id)->delete();
    }
    
    public static function allForFile($file_id)
    {
         return DB::table('doc_files_tags')
                 ->join('doc_tags', 'doc_files_tags.tag_id', '=', 'doc_tags.id')
                 ->where('doc_files_tags.file_id', '=', (int) $file_id)
                 ->select('doc_tags.*')
                 ->get();
    }
}