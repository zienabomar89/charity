<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_categories';
    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name'  => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public static function allWithOptions($options = array())
    {
        $defaults = array(
            'exclude' => null,
            'files_count' => false,
        );
        
        $options = array_merge($defaults, $options);
        
        $categories = DB::table('doc_categories');
        $categories->select('doc_categories.*')
                   ->orderBy('doc_categories.name');
        if ($options['exclude']) {
            $categories->where('doc_categories.id', '<>', $options['exclude']);
        }
        if ($options['files_count']) {
            $categories->select(DB::raw('doc_categories.*, (select COUNT(doc_files.id) FROM doc_files WHERE doc_files.category_id = doc_categories.id) as files'));
        }
        
        return $categories->get();
    }
}