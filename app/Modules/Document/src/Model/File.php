<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class File extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_files';
    protected $fillable = ['name', 'filepath', 'size','created_at'];

    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'category_id'  => 'required|int',
            );
        }
        
        return self::$rules;
    }
    
    public static function allWithOptions($options = array())
    {
        $default = array(
            'name' => null,
            'tag' => null,
            'category_id' => null,
            'with_mimetype_name' => false,
            'with_category_name' => false,
            'paginate' => 0,
        );
        $options = array_merge($default, $options);
        
        $files = DB::table('doc_files')
                ->select('doc_files.*');
        
        if ($options['with_mimetype_name']) {
            $files->join('doc_mimetypes', 'doc_files.mimetype', '=', 'doc_mimetypes.id')
                  ->addSelect('doc_mimetypes.name as mimetype_name');
        }
        if ($options['with_category_name']) {
            $files->leftJoin('doc_categories', 'doc_files.category_id', '=', 'doc_categories.id')
                  ->addSelect('doc_categories.name as category_name');
        }
        
        if (!empty($options['name'])) {
            $name = '%' . $options['name'] . '%';
            $files->whereRaw("normalize_text_ar(doc_files.name) like  normalize_text_ar(?)", "%".$name."%");
        }
        if (!empty($options['category_id'])) {
            $files->where('doc_files.category_id', '=', $options['category_id']);
        }
        if (!empty($options['tag'])) {
            $tag = '%' . $options['tag'] . '%';
            $files->join('doc_files_tags', 'doc_files.id', '=', 'doc_files_tags.file_id')
                  ->join('doc_tags', 'doc_files_tags.tag_id', '=', 'doc_tags.id')  
                  ->whereRaw("normalize_text_ar(doc_tags.name) like  normalize_text_ar(?)", "%".$tag."%");
        }
        
        if ($options['paginate'] > 0) {
            return $files->paginate($options['paginate']);
        }
        
        return $files->get();
    }
    
    public function saveWithTags($tags)
    {
        DB::beginTransaction();
        try {
            $this->save();
            $file_id = $this->id;
            
            FileTag::deleteFileTags($file_id);
            foreach ($tags as $tag_id) {
                $data = [
                    'file_id' => $file_id,
                    'tag_id' => $tag_id,
                ];
                FileTag::create($data);
            }
            
            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            echo $e->getTraceAsString();
            return false;
        }
    }
    
    public static function findOrFailWithOptions($id, $options = array())
    {
        $default = array(
            'with_mimetype_name' => false,
            'with_category_name' => false,
        );
        $options = array_merge($default, $options);
        
        $files = DB::table('doc_files')
                ->select('doc_files.*')
                ->where('doc_files.id', '=', $id);
        
        if ($options['with_mimetype_name']) {
            $files->join('doc_mimetypes', 'doc_files.mimetype', '=', 'doc_mimetypes.id')
                  ->addSelect('doc_mimetypes.name as mimetype_name');
        }
        if ($options['with_category_name']) {
            $files->leftJoin('doc_categories', 'doc_files.category_id', '=', 'doc_categories.id')
                  ->addSelect('doc_categories.name as category_name');
        }
        
        //return $files->first();
        //return File::hydrate((array) $files->first())->all();
        return static::toModel($files->first());
    }

    public function sanitizeName($name)
    {
        $name = str_replace(array('.', '-', '_'), ' ', $name);
        $name = preg_replace('/\s+/', ' ', $name);
        return $name;
    }
    
    protected static function toModel($attributes)
    {
        if ($attributes instanceof \stdClass) {
            $attributes = (array) $attributes;
        }
        
        $model = new static();
        $model->fillable(array_keys($attributes));
        $model->fill($attributes);
        return $model;
    }
    
    public static function createRevision($attributes)
    {
        return \Illuminate\Support\Facades\DB::transaction(function() use($attributes) {
            $query = 'INSERT INTO `doc_files_revisions` (`file_id`, `notes`, `filepath`, `mimetype`, `size`, `created_at`)
                      SELECT `id`, :notes, `filepath`, `mimetype`, `size`, `created_at` FROM `doc_files` WHERE `id` = :id';
            \Illuminate\Support\Facades\DB::insert($query, [
                'notes' => $attributes['notes'],
                'id' => $attributes['id'],
            ]);
            
            $query = 'UPDATE `doc_files` SET
                      `filepath` = :filepath,
                      `mimetype` = :mimetype,
                      `size` = :size,
                      `updated_at` = NOW(),
                      `updated_by` = :user_id
                      WHERE `id` = :id';
            \Illuminate\Support\Facades\DB::update($query, [
                'filepath' => $attributes['filepath'],
                'mimetype' => $attributes['mimetype'],
                'size' => $attributes['size'],
                'user_id' => \Auth::user()->id,
                'id' => $attributes['id'],
            ]);
        });
        
    }
}