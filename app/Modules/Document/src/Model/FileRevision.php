<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FileRevision extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_files_revisions';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * Get the name of the "updated at" column.
     *
     * @return string|null
     */
    public function getUpdatedAtColumn()
    {
        return null;
    }
    
    public static function allforFile($file_id, $options = array())
    {
        $default = array(
            'with_mimetype_name' => false,
        );
        $options = array_merge($default, $options);
        
        $revisions = DB::table('doc_files_revisions')
                ->select('doc_files_revisions.*')
                ->where('doc_files_revisions.file_id', '=', $file_id)
                ->orderBy('doc_files_revisions.created_at', 'DESC');
        
        if ($options['with_mimetype_name']) {
            $revisions->join('doc_mimetypes', 'doc_files_revisions.mimetype', '=', 'doc_mimetypes.id')
                      ->addSelect('doc_mimetypes.name as mimetype_name');
        }
        
        return $revisions->get();
    }
}