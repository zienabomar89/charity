<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AttributeValue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_attributes_values';
    
    public static function deleteAttributeValues($attribute_id)
    {
        return DB::table('doc_attributes_values')->where('attribute_id', '=', $attribute_id)->delete();
    }
    
    public static function saveAttributeValues($attribute_id, $values)
    {
        $attrValues = self::all()->where('attribute_id', '=', $attribute_id);
        foreach ($attrValues as $v) {
            if (!in_array($v->id, $values)) {
                $delete[] = $v->id;
            } else {
                $atrrValueIds[] = $v->id;
            }
        }
        foreach ($values as $id) {
            if (!in_array($id, $atrrValueIds)) {
                $insert[] = $id;
            }
        }
    }
}