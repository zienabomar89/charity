<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;

class FileAttribute extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_files_attributes';
}