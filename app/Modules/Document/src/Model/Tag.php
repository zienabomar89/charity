<?php

namespace Document\Model;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'doc_tags';
    
    public $timestamps   = false;

    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name'  => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public static function allWithOptions($options = array())
    {
        if (isset($options['name']) && !empty($options['name'])) {
            return \Illuminate\Support\Facades\DB::table('doc_tags')->where('name', 'LIKE', "%{$options['name']}%")->get();
        }
        return self::all();
    }
}