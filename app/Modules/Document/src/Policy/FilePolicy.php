<?php

namespace Document\Policy;

use Auth\Model\User;
use Document\Model\File;

class FilePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('doc.files.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('doc.files.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, File $file = null)
    {
        if ($user->hasPermission('doc.files.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, File $file = null)
    {
        if ($user->hasPermission('doc.files.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, File $file = null)
    {
        if ($user->hasPermission(['doc.files.view', 'doc.files.update'])) {
            return true;
        }
        
        return false;
    }
    
    public function download(User $user, File $file = null)
    {
        if ($user->hasPermission(['doc.files.download', 'doc.files.update'])) {
            return true;
        }
        
        return false;
    }
}

