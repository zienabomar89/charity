<?php

namespace Document\Policy;

use Auth\Model\User;
use Document\Model\Tag;

class TagPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('doc.tags.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('doc.tags.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Tag $tag = null)
    {
        if ($user->hasPermission('doc.tags.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Tag $tag = null)
    {
        if ($user->hasPermission('doc.tags.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Tag $tag = null)
    {
        if ($user->hasPermission(['doc.tags.view', 'doc.tags.update'])) {
            return true;
        }
        
        return false;
    }
}

