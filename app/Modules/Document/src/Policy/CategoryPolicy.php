<?php

namespace Document\Policy;

use Auth\Model\User;
use Document\Model\Category;

class CategoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('doc.categories.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('doc.categories.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Category $category = null)
    {
        if ($user->hasPermission('doc.categories.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Category $category = null)
    {
        if ($user->hasPermission('doc.categories.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Category $category = null)
    {
        if ($user->hasPermission(['doc.categories.view', 'doc.categories.update'])) {
            return true;
        }
        
        return false;
    }
}

