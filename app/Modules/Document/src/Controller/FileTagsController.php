<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Document\Model\FileTag;

class FileTagsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get FileTag model searching by file_id
    public function index()
    {
        //$this->authorize('manage', Tag::class);
        $tags = FileTag::allForFile(request()->input('file_id'));
        return response()->json($tags);
    }
}
