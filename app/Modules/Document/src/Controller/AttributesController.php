<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Document\Model\Attribute;

class AttributesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get all Attributes
    public function index()
    {
        $this->authorize('manage', Attribute::class);
        
        $attributes = Attribute::all();
        return response()->json($attributes);
    }

    // create new Attribute model
    public function store(Request $request)
    {
        $this->authorize('manage', Attribute::class);
        $this->validate($request, Attribute::getValidatorRules());
        
        $attribute = new Attribute();
        $attribute->name = $request->input('name');
        $attribute->category_id = $request->input('category_id');
        $attribute->type = $request->input('type');
        $attribute->required = $request->input('required');
        $values = $request->input('values');
        
        $attribute->saveWithValues($values);
        return response()->json($attribute);
    }

    // get existed object model by id
    public function show($id)
    {
        $attribute = Attribute::findWithValues($id);
        return response()->json($attribute);
    }

    // update data of Attribute object model
    public function update(Request $request, $id)
    {
        $attribute = Attribute::findOrFail($id);
        $this->authorize('update', $attribute);
        
        $attribute->name = $request->input('name');
        $attribute->category_id = $request->input('category_id');
        $attribute->type = $request->input('type');
        $attribute->required = $request->input('required');
        $values = $request->input('values');
        
        $attribute->saveWithValues($values);
        return response()->json($attribute);
    }

    // delete exist Attribute model by id
    public function destroy(Request $request, $id)
    {
        $attribute = Attribute::findOrFail($id);
        $this->authorize('delete', $attribute);
        
        $attribute->delete();
        return response()->json($attribute);
    }
}
