<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Document\Model\Category;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get Category model searching by name
    public function index()
    {
        $this->authorize('manage', Category::class);
        
        $request = request();
        $categories = Category::allWithOptions(array(
            'exclude' => $request->input('exclude'),
            'files_count' => $request->input('files', false),
        ));
        return response()->json($categories);
    }

    // create new Category model
    public function store(Request $request)
    {
        $this->authorize('create', Category::class);
        $this->validate($request, Category::getValidatorRules());
        
        $category = new Category();
        $category->name = $request->input('name');
        $category->parent_id = $request->input('parent_id');
        $category->created_by = Auth::user()->id;
        $category->save();
        return response()->json($category);
    }

    // get existed object model by id
    public function show($id)
    {
        $category = Category::findOrFail($id);
        $this->authorize('view', $category);
        return response()->json($category);
    }

    // update data of Category object model
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $this->authorize('update', $category);
        
        $category->name = $request->input('name');
        $category->parent_id = $request->input('parent_id');
        $category->updated_by = Auth::user()->id;
        $category->save();
        return response()->json($category);
    }

    // delete exist Category model by id
    public function destroy(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $this->authorize('delete', $category);
        
        $category->delete();
        return response()->json($category);
    }
}
