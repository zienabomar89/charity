<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Common\Model\PersonDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Document\Model\File;
use Sponsorship\Model\ReportsCasesDocuments;

class FilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get File model searching by filter options
    public function index()
    {
        $this->authorize('manage', File::class);

        $request = request();
        $options = array(
            'with_mimetype_name' => true,
            'with_category_name' => true,
            'name' => $request->input('name'),
            'tag' => $request->input('tag'),
            'category_id' => $request->input('category_id'),
            'paginate' => 20,
        );
        $files = File::allWithOptions($options);
        return response()->json($files);
    }

    // create new File model
    public function store(Request $request)
    {
        //$this->authorize('create', File::class);
        //$this->validate($request, File::getValidatorRules());
        $request->action;

        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            if ($request->action == "check"){
                $less = 100*1024 ;
                $bigger = 300*1024 ;
                $size = $uploadFile->getSize() ;
                if ($less < $size && $size < $bigger){
                    $path = $uploadFile->store('documents');
                    if ($path) {
                        $file = new File();
                        $file->category_id = $request->input('category_id');
                        $file->file_status_id = 1;
                        $file->name = $uploadFile->getClientOriginalName();
                        $file->description = $request->input('description');
                        $file->filepath = $path;
                        $file->size = $size;
                        $file->mimetype = $uploadFile->getMimeType();
                        $file->created_by = Auth::user()->id;

                        $file->save();
                        return response()->json($file);
                    } 
                }else {
                    return response()->json(['status' =>'error','msg'=>trans('document::application.the uploaded file is out of allowed range for file size')]);
                }
            }

                $path = $uploadFile->store('documents');
                if ($path) {
                    $file = new File();
                    $file->category_id = $request->input('category_id');
                    $file->file_status_id = 1;
                    $file->name = $uploadFile->getClientOriginalName();
                    $file->description = $request->input('description');
                    $file->filepath = $path;
                    $file->size = $uploadFile->getSize();
                    $file->mimetype = $uploadFile->getMimeType();
                    $file->created_by = Auth::user()->id;

                    $file->save();
                    return response()->json($file);
                }
            }

            return response()->json($uploadFile->getErrorMessage());
        }

    // get existed object model by id
    public function show($id)
    {
        $options = array(
            'with_mimetype_name' => true,
            'with_category_name' => true,
        );
        $file = File::findOrFailWithOptions($id, $options);
        $this->authorize('view', $file);

        return response()->json($file);
    }

    // update data of File object model
    public function update(Request $request, $id)
    {
        $file = File::findOrFail($id);
        $this->authorize('update', $file);
        
        $file->category_id = $request->input('category_id');
        $file->name = $request->input('name');
        $file->description = $request->input('description');
        $file->updated_by = Auth::user()->id;
        
        $tags = $request->input('tags', array());
        $file->saveWithTags($tags);
        return response()->json($file);
    }

    // delete exist File model by id
    public function destroy(Request $request, $id)
    {
        $file = File::findOrFail($id);
        $this->authorize('delete', $file);
        
        \Illuminate\Support\Facades\Storage::delete($file->filepath);
        foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
            \Illuminate\Support\Facades\Storage::delete($revision->filepath);
        }
        $file->delete();
        return response()->json($file);
    }

    // get existed File to preview by id
    public function preview(Request $request, $id)
    {
        $file = File::findOrFail($id);
        //$this->authorize('view', $file);
        return response()->file(base_path('storage/app/' . $file->filepath));
    }

    // get existed File to thumbnail by id
    public function thumbnail(Request $request, $id)
    {
        $file = File::findOrFail($id);
        //$this->authorize('view', $file);

        $image = base_path('storage/app/' . $file->filepath);
        if (!is_file($image)) {
            return response()->file(base_path('storage/app/file.png'));
        }
        $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
                            'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];

        if (!in_array($file->mimetype, $imageMimeTypes)) {
            $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
            if (!in_array($fileType, $imageMimeTypes)) {
                $image = base_path('storage/app/file.png');
            }
        }
        
        return response()->file($image);
    }

    // get case image File to preview by id
    public function casesImage($id,$type)
    {
//        $file = File::findOrFail($id);
        $file = PersonDocument::attachmentImagePath($id,$type);  //$this->authorize('view', $file);

        if(is_null($file)){
            return response()->file(base_path('storage/app/file.png'));
        }

        $image = base_path('storage/app/' . $file->filepath);
        if (!is_file($image)) {
            return response()->file(base_path('storage/app/file.png'));
        }
        $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
                            'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];

        if (!in_array($file->mimetype, $imageMimeTypes)) {
            $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
            if (!in_array($fileType, $imageMimeTypes)) {
                $image = base_path('storage/app/file.png');
            }
        }

        return response()->file($image);
    }

    // get report image File to preview by id
    public function reportsImage($id)
    {
        $ReportsCasesDocuments = ReportsCasesDocuments::findOrFail($id);
        $file = File::findOrFail($ReportsCasesDocuments->document_id);

        if(is_null($file)){
            return response()->file(base_path('storage/app/file.png'));
        }

        $image = base_path('storage/app/' . $file->filepath);
        if (!is_file($image)) {
            return response()->file(base_path('storage/app/file.png'));
        }
        $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
                            'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];

        if (!in_array($file->mimetype, $imageMimeTypes)) {
            $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
            if (!in_array($fileType, $imageMimeTypes)) {
                $image = base_path('storage/app/file.png');
            }
        }

        return response()->file($image);
    }

    // get logged user image File to preview
    public function userImage()
    {
        $user = \Auth::user();

        if(is_null($user->document_id)){
            return response()->file(base_path('storage/app/emptyUser.png'));
        }

        $file = File::findOrFail($user->document_id);

        if(is_null($file)){
            return response()->file(base_path('storage/app/emptyUser.png'));
        }

        $image = base_path('storage/app/' . $file->filepath);
        if (!is_file($image)) {
            return response()->file(base_path('storage/app/emptyUser.png'));
        }
        $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
                            'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];

        if (!in_array($file->mimetype, $imageMimeTypes)) {
            $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
            if (!in_array($fileType, $imageMimeTypes)) {
                $image = base_path('storage/app/emptyUser.png');
            }
        }

        return response()->file($image);
    }

}
