<?php

namespace Document\Controller;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Document\Model\File;

class UnAuthController extends Controller
{
    // download exist FileRevision by id
    public function download(Request $request, $id)
    {
//        $file = File::findOrFail($id);
//        $filename = basename($file->filepath);
//        $filename = substr(basename($filename), 0, strrpos($filename, '.'));
//        $token = $request->query('token');
//
//        if (!$token) {
//            $this->middleware('auth:api');
////            $this->authorize('download', $file);
//
//            return response()->json(array(
//                'download_token' => \Illuminate\Support\Facades\Hash::make($filename)
//            ));
//        }
//
//        if (\Illuminate\Support\Facades\Hash::check($filename, $token)) {
//            return response()->download(base_path('storage/app/' . $file->filepath));
//        }
//
//        return response()->json(array(
//            'error' => 'Invalid file token',
//        ));

        $file = File::findOrFail($id);
        $filename = basename($file->filepath);
        $filename = substr(basename($filename), 0, strrpos($filename, '.'));
        $token = $request->query('token');

        if($file->filepath){
            $filepath = base_path('storage/app/' . $file->filepath);
            if (is_file($filepath)) {
                return response()->download(base_path('storage/app/' . $file->filepath));
            }
        }

        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process') ]);

    }

    public function downloadWithOutToken(Request $request, $id)
    {
        $file = File::findOrFail($id);
        $filename = basename($file->filepath);
        $filename = substr(basename($filename), 0, strrpos($filename, '.'));
        $token = $request->query('token');

        if($file->filepath){
            $filepath = base_path('storage/app/' . $file->filepath);
            if (is_file($filepath)) {
                return response()->download(base_path('storage/app/' . $file->filepath));
            }
        }

        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process') ]);

    }

}