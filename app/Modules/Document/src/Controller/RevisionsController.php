<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Document\Model\FileRevision;
use Document\Model\File;

class RevisionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get FileRevision model searching by file_id
    public function index()
    {
        //$this->authorize('manage', FileRevision::class);
        $file_id = request()->input('file_id');
        $revisions = FileRevision::allforFile($file_id, array(
            'with_mimetype_name' => true,
        ));
        return response()->json($revisions);
    }

    // create new FileRevision model
    public function store(Request $request)
    {
        //$this->authorize('manage', User::class);
        //$this->validate($request, User::getValidatorRules());
        $file_id = $request->input('file_id');
        $file = File::findOrFail($file_id);
        
        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            $path = $uploadFile->store('documents');
            if ($path) {
                $p = $file->filepath;
                $s = $file->size;
                $mt = $file->mimetype;
                $c = $file->updated_at? $file->updated_at : $file->created_at;
                
                $revision = new FileRevision();
                $revision->file_id = $file_id;
                $revision->notes = $request->input('notes');
                $revision->filepath = $p;
                $revision->size = $s;
                $revision->mimetype = $mt;
                $revision->created_at = $c;
                $revision->save();
                
                $file->filepath = $path;
                $file->size = $uploadFile->getSize();
                $file->mimetype = $uploadFile->getMimeType();
                //$file->updated_by = Auth::user()->id;
                $file->save();
                return response()->json($revision);

            }
        }
        
        return response()->json($uploadFile->getErrorMessage());
    }

    // get existed object model by id
    public function show($id)
    {
        $revision = FileRevision::findOrFail($id);
        return response()->json($revision);
    }

    // update data of FileRevision object model
    public function update(Request $request, $id)
    {
        $revision = FileRevision::findOrFail($id);
        $this->authorize('update', $revision);
        return response()->json($revision);
    }

    // delete exist FileRevision model by id
    public function destroy(Request $request, $id)
    {
        $revision = FileRevision::findOrFail($id);
        $this->authorize('delete', $revision);
        
        $revision->delete();
        return response()->json($revision);
    }

    // download exist FileRevision by id
    public function download(Request $request, $id)
    {
        $revision = FileRevision::findOrFail($id);
        return response()->download(base_path('storage/app/' . $revision->filepath));
    }
}
