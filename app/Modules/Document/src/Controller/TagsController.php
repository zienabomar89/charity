<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Document\Model\Tag;

class TagsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get Tag model searching by name
    public function index()
    {
        $this->authorize('manage', Tag::class);
        
        $tags = Tag::allWithOptions(array(
            'name' => request()->input('name')
        ));
        return response()->json($tags);
    }

    // create new Tag model
    public function store(Request $request)
    {
        $this->authorize('manage', Tag::class);
        $this->validate($request, Tag::getValidatorRules());
        
        $tag = new Tag();
        $tag->name = $request->input('name');
        $tag->save();
        return response()->json($tag);
    }

    // get existed object model by id
    public function show($id)
    {
        $tag = Tag::findOrFail($id);
        $this->authorize('view', $tag);
        return response()->json($tag);
    }

    // update data of Tag object model
    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $this->authorize('update', $tag);
        
        $tag->name = $request->input('name');
        $tag->save();
        return response()->json($tag);
    }

    // delete exist BackupVersions model by id
    public function destroy(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);
        $this->authorize('delete', $tag);
        
        $tag->delete();
        return response()->json($tag);
    }

}
