<?php

namespace Document\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Document\Model\AttributeValue;

class AttributeValuesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get AttributeValue model searching by name
    public function index()
    {
        //$this->authorize('manage', AttributeValue::class);
        
        $values = AttributeValue::allWithOptions(array(
            'name' => request()->input('name')
        ));
        return response()->json($values);
    }

    // create new AttributeValue model
    public function store(Request $request)
    {
        //$this->authorize('manage', AttributeValue::class);
        $this->validate($request, AttributeValue::getValidatorRules());
        
        $value = new AttributeValue();
        $value->name = $request->input('value');
        $value->attribute_id = $request->input('attribute_id');
        $value->save();
        return response()->json($value);
    }

    // get existed object model by id
    public function show($id)
    {
        $value = AttributeValue::findOrFail($id);
        return response()->json($value);
    }

    // update data of AttributeValue object model
    public function update(Request $request, $id)
    {
        $value = AttributeValue::findOrFail($id);
        $this->authorize('update', $value);
        
        $value->name = $request->input('value');
        $value->attribute_id = $request->input('attribute_id');
        $value->save();
        return response()->json($value);
    }

    // delete exist AttributeValue model by id
    public function destroy(Request $request, $id)
    {
        $value = AttributeValue::findOrFail($id);
        $this->authorize('delete', $value);
        
        $value->delete();
        return response()->json($value);
    }
}
