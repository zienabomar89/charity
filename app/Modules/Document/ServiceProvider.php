<?php

namespace App\Modules\Document;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Document\Model\Category::class => \Document\Policy\CategoryPolicy::class,
        \Document\Model\File::class => \Document\Policy\FilePolicy::class,
        \Document\Model\Tag::class => \Document\Policy\TagPolicy::class,
    ];
    
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'document');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'document');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'auth'
        );*/
    }

}
