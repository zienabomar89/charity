<?php

return array(
    'doc.catregories.manage' => 'إدارة تصنيفات الملفات',
    'doc.catregories.view'   => 'عرض بيانات تصنيف الملفات',
    'doc.catregories.create' => 'إنشاء تصنيف ملف جديد',
    'doc.catregories.update' => 'تحرير تصنيف ملف',
    'doc.catregories.delete' => 'حذف تصنيف ملف',
    
    'doc.files.manage' => 'إدارة الملفات',
    'doc.files.view'   => 'عرض معلومات ملف',
    'doc.files.create' => 'إنشاء ملف جديد',
    'doc.files.update' => 'تحرير معلومات ملف',
    'doc.files.delete' => 'حذف ملف',
    
    'doc.tags.manage' => 'إدارة وسوم الملفات',
    'doc.tags.view'   => 'عرض معلومات الوسم',
    'doc.tags.create' => 'إنشاء وسم جديد',
    'doc.tags.update' => 'تحرير معلومات وسم',
    'doc.tags.delete' => 'حذف وسم',
);
