<?php

  return array(
        "actions" => "Actions",
        "add" => "Add",
        "attribute" => "Attribute",
        "attributes" => "Attributes",
        "back" => "Back",
        "cancel" => "Cancel",
        "cancel_all" => "Cancel All",
        "categories" => "Categories",
        "category" => "Category",
        "created_at" => "Created At",
        "delete" => "Delete",
        "description" => "Description",
        "download" => "Download",
        "drop_files_here" => "Drop Files Here",
        "edit" => "Edit",
        "file" => "File",
        "file_path" => "File Path",
        "files" => "Files",
        "information" => "Information",
        "invalid file" => "Invalid File",
        "name" => "Name",
        "new" => "New",
        "new_version" => "New Version",
        "options" => "Options",
        "parent" => "Parent",
        "path" => "Path",
        "progress" => "Progress",
        "queue_length" => "Queue Length",
        "queue_progress" => "Queue Progress",
        "remove" => "Remove",
        "remove_all" => "Remove All",
        "required" => "Required",
        "revision" => "Revision",
        "revisions" => "Revisions",
        "save" => "Save",
        "search" => "Search",
        "size" => "Size",
        "status" => "Status",
        "tag" => "Tag",
        "tags" => "Tags",
        "the uploaded file is out of allowed range for file size" => "The Uploaded File Is Out Of Allowed Range For File Size",
        "title" => "Title",
        "type" => "Type",
        "update" => "Update",
        "updated_at" => "Updated At",
        "upload" => "Upload",
        "upload_all" => "Upload All",
        "upload_new_version" => "Upload New Version",
        "upload_queue" => "Upload Queue",
        "values" => "Values",
);

?>
