<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {

        return parent::render($request, $exception);


        $ip_address = request()->server('SERVER_ADDR'); /*  return ip address */
//        $url = \URL::to('/'); /*  return domain  */
//
//        $ip_address = '168.2.2.2';
//
//        if($ip_address != '127.0.0.1' || $url != 'takamulsys.com'){
//           return redirect()->guest('');
//        }

        if ($exception instanceof \Illuminate\Auth\Access\AuthorizationException
                && $request->expectsJson()) {
            return response()->json(['error' => $exception->getMessage()], 403);
        }

        if ($exception instanceof \Illuminate\Database\QueryException
                && $request->expectsJson()) {
            $response = [
                'status' => 'error',
                'error_code' => $exception->getCode(),
                'msg' => trans('setting::application.An error occurred during the execution of the operation in the database') . $exception->getMessage(),
            ];
            return response()->json($response);
        }


      return parent::render($request, $exception);

    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'invalid_token'], 401, ['www-authenticate' => 'Bearer realm="login"']);
        }

        return redirect()->guest('account/login');
    }
}
