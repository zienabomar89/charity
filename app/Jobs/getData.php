<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Aid\Model\VoucherPersons;
use Common\Model\Person;
use Common\Model\GovServices;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class getData  extends Notification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $cards;
    protected $type;
    protected $permission;

    public function __construct($cards,$type,$permission){
//        $this->queue = 'persons-data';
        $this->cards = $cards;
        $this->type = $type;
        $this->permission = $permission;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }


    public function handle(){

        DB::beginTransaction();
        try {

            return (new MailMessage)
                ->subject('حساب مستخدم جديد')
                ->line('Mail: '.'zienab-19@hotmail.com')
                //                ->greeting(sprintf('السيد، %s %s', $this->user->firstname, $this->user->lastname))
                ->line('تم إنشاء حساب خاص بك على النظام المحوسب لإدارة الجمعيات. فيما يلي بيانات الدخول الخاصة بك:')
//                ->line(sprintf('اسم المستخدم: %s', $this->user->username))
//                ->line(sprintf('كلمة المرور: %s', $this->password))
//                ->action('تسجيل الدخول', $url)
                ->line('شكرا لك!');

            $type = $this->type;
            $duplicated =0;
            $success =0;

            $final_records=[];
            $invalid_cards=[];
            $processed=[];
            $health = [];

            foreach ($this->cards as $key =>$value) {
                $card= (int) $value['rkm_alhoy'];
                if(in_array($card,$processed)){
                    $duplicated++;
                }
                else{
                    $card= (int) $card;
                    if(strlen(($card))  <= 9) {
                        if(GovServices::checkCard($card)){
                            $row = GovServices::byCard($card);
                            if($row['status'] == true) {
//                                $temp= $row['row'];
//                                $temp->MOBILE=null;
//                                $temp->TEL=null;
//                                $contact_data = GovServices::ssoInfo($card);
//                                if($contact_data['status'] != false){
//                                    $temp->MOBILE=GovServices::ObjectValue($contact_data['row'],'USERMOBILE');
//                                    $temp->TEL=GovServices::ObjectValue($contact_data['row'],'USERTELEPHONE');
//                                }
//
//                                $temp->MR_CODE = $temp->LOC_NAME_AR = $temp->MR_PATIENT_CD = $temp->DOCTOR_NAME = $temp->MR_CREATED_ON= $temp->DREF_NAME_AR=
//                                $temp->MR_DIAGNOSIS_AR= $temp->MR_DIAGNOSIS_EN= $temp->MR_COMPLAINT= $temp->MR_EXAMINATION = NULL;
//
//                                $health_data_ = GovServices::medicalReports($card);
//                                if($health_data_['status'] != false){
//                                    foreach ($health_data_['row'] as $k_=>$v_){
//                                        $v_->card = $card;
//                                        $v_->name =  ((is_null($temp->FNAME_ARB) || $temp->FNAME_ARB == ' ') ? ' ' : $temp->FNAME_ARB) . ' ' .
//                                            ((is_null($temp->SNAME_ARB) || $temp->SNAME_ARB == ' ') ? ' ' : $temp->SNAME_ARB) . ' ' .
//                                            ((is_null($temp->TNAME_ARB) || $temp->TNAME_ARB == ' ') ? ' ' : $temp->TNAME_ARB) . ' ' .
//                                            ((is_null($temp->LNAME_ARB) || $temp->LNAME_ARB == ' ') ? ' ' : $temp->LNAME_ARB);
//                                        $health[]= $v_;
//                                    }
//                                }
//
//                                $commercial_data = GovServices::commercialRecords($card);
//                                $temp->COMP_NAME = $temp->REC_CODE = $temp->PERSON_TYPE_DESC =
//                                $temp->IS_VALID_DESC = $temp->STATUS_ID_DESC =  $temp->COMP_TYPE_DESC =
//                                $temp->REC_TYPE_DESC = $temp->REGISTER_NO = $temp->COMP_TYPE_DESC = $temp->START_DATE =
//                                $temp->WORK_CLASS_DESC = $temp->BRAND_NAME= $temp->CITY_DESC = null;
//
//                                if($commercial_data['status'] != false){
//                                    $commercial=$commercial_data['row'];
//                                    if(isset($temp->commercial_data->START_DATE)){
//                                        $commercial->START_DATE = date('d/m/Y',strtotime($temp->commercial_data->START_DATE));
//                                    }
//                                    $temp->COMP_NAME=$commercial->COMP_NAME;
//                                    $temp->REC_CODE=$commercial->REC_CODE;
//                                    $temp->PERSON_TYPE_DESC=$commercial->PERSON_TYPE_DESC;
//                                    $temp->IS_VALID_DESC=$commercial->IS_VALID_DESC;
//                                    $temp->STATUS_ID_DESC=$commercial->STATUS_ID_DESC;
//                                    $temp->COMP_TYPE_DESC=$commercial->COMP_TYPE_DESC;
//                                    $temp->REC_TYPE_DESC=$commercial->REC_TYPE_DESC;
//                                    $temp->REGISTER_NO=$commercial->REGISTER_NO;
//                                    $temp->COMP_TYPE_DESC=$commercial->COMP_TYPE_DESC;
//                                    $temp->WORK_CLASS_DESC=$commercial->WORK_CLASS_DESC;
//                                    $temp->BRAND_NAME=$commercial->BRAND_NAME;
//                                    $temp->CITY_DESC=$commercial->CITY_DESC;
//
//                                }
//
//                                $temp->social_affairs_status= trans('common::application.un_beneficiary');
//                                $temp->AID_CLASS = $temp->AID_TYPE = $temp->AID_AMOUNT = $temp->AID_SOURCE = $temp->AID_PERIODIC =  $temp->ST_BENEFIT_DATE =  $temp->END_BENEFIT_DATE = null;
//
//                                $socialAffairs=GovServices::socialAffairsStatus($card);
//                                if($socialAffairs['status'] != false){
//                                    $socialAffairs_=$socialAffairs['row'];
//                                    $temp->social_affairs_status= trans('common::application.un_beneficiary');
//                                    $temp->AID_CLASS    = $socialAffairs_->AID_CLASS ;
//                                    $temp->AID_TYPE     = $socialAffairs_->AID_TYPE ;
//                                    $temp->AID_AMOUNT   = $socialAffairs_->AID_AMOUNT ;
//                                    $temp->AID_SOURCE   = $socialAffairs_->AID_SOURCE ;
//                                    $temp->AID_PERIODIC =  $socialAffairs_->AID_PERIODIC ;
//                                    $temp->ST_BENEFIT_DATE  = $socialAffairs_->ST_BENEFIT_DATE ;
//                                    $temp->END_BENEFIT_DATE = $socialAffairs_->END_BENEFIT_DATE;
//                                }
//
//                                $main=(array) $temp;
//                                $main['related']= null;
//
//                                if($type == 2){
//                                    $family = GovServices::getAllPersonRelations($card);
//                                    if($family['status'] != false){
//                                        $family = $family['row'];
//                                        foreach ($family as $kf_=> $vf_) {
//                                            if( $vf_->RELATIVE_CD == 287 ||  $vf_->RELATIVE_CD == 286 ||
//                                                $vf_->RELATIVE_CD == 284 ||  $vf_->RELATIVE_CD == 285) {
//                                                $main['related']= $vf_;
//                                                $final_records[] = $main;
//                                            }
//                                        }
//                                    }else{
//                                        $final_records[]=$main;
//                                    }
//                                }
//                                else if($type == 3){
//                                    $wives_no= 0;
//                                    $wives = GovServices::getPersonWivesCard($card);
//                                    if($wives['status'] == true){
//                                        $wives_no=sizeof($wives['row']);
//                                        foreach ($wives['row'] as $wife){
//                                            $wifeRow = GovServices::byCard($wife);
//                                            if($wifeRow['status'] == true){
//                                                $wifeRow['row']->kinship_name=trans('common::application.wife_');
//                                                $main['related']= $wifeRow['row'];
//                                                $final_records[] = $main;
//                                            }
//                                        }
//                                    }
//
//                                    if($wives_no == 0){
//                                        $final_records[] = $main;
//                                    }
//                                }
//                                else if($type == 4){
//                                    $childs_no= 0;
//                                    $childs = GovServices::getPersonChild($card);
//                                    if($childs['status'] == true) {
//                                        $childs_no=sizeof($childs['row']);
//                                        foreach ($childs['row'] as $child){
//                                            $childRow = GovServices::byCard($child);
//                                            if($childRow['status'] == true){
//                                                $childRow['row']->kinship_name=trans('common::application.son_').'/'.trans('common::application.daughter_');
//                                                $main['related']= $childRow['row'];
//                                                $final_records[] = $main;
//                                            }
//                                        }
//                                    }
//
//
//                                    if($childs_no == 0){
//                                        $final_records[] = $main;
//                                    }
//
//                                }
//                                else{
//                                    $main['wives_count']= 0;
//                                    $main['child_count']= 0;
//                                    $main['male_live']= 0;
//                                    $main['female_live']= 0;
//                                    $main['family_cnt']= 1;
//
//                                    $related = GovServices::getPersonRelatedCard($card);
//                                    if(sizeof($related['row']) > 0 ) {
//                                        $kinships =$related['kinship'];
//                                        $SEX_CD =$related['SEX_CD'];
//                                        foreach ($related['row'] as $crd){
//                                            $rel =$kinships[$crd];
//                                            if($rel =='wife'){
//                                                $main['wives_count'] ++;
//                                            }
//                                            else if($rel =='son' &&($SEX_CD[$crd] == 2 || $SEX_CD[$crd] == "2" || $SEX_CD[$crd] == '2')){
//                                                $main['female_live'] ++;
//                                            }
//                                            else if($rel =='son' && ($SEX_CD[$crd] == 1 || $SEX_CD[$crd] == "1" || $SEX_CD[$crd] == '1')){
//                                                $main['male_live'] ++;
//                                            }
//                                        }
//
//                                        $main['family_cnt'] = ( $main['wives_count'] + $main['male_live'] +  $main['female_live']) + 1;
//                                    }
//
//                                    $final_records[] = $main;
//                                }
                                $processed[]=$card;
                                $success++;
                            }else{
                                $invalid_cards[]=$card;
                            }
                        }else{
                            $invalid_cards[]=$card;
                        }

                    }else{
                        $invalid_cards[]=$card;
                    }
                }
            }

            $cases_list = [];

            if ($this->permission && sizeof($processed) > 0 && $type == 2) {
                $cases_list = \Common\Model\Person::getPersonCardCases($processed);
            }

            $token = md5(uniqid());
            Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$cases_list,$health,$type) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));

                if(sizeof($final_records) > 0){
                    $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records,$type) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        if($type == 1 ||$type == 2 ){
                            if($type == 1){
                                $sheet->getStyle("A1:O1")->applyFromArray(['font' => ['bold' => true]]);
                            }else{
                                $sheet->getStyle("A1:V1")->applyFromArray(['font' => ['bold' => true]]);

                            }

                            $sheet->setCellValue('A1',trans('common::application.#'));
                            $sheet->setCellValue('B1',trans('common::application.full_name'));
                            $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                            $sheet->setCellValue('D1',trans('common::application.gender'));
                            $sheet->setCellValue('E1',trans('common::application.marital_status'));
                            $sheet->setCellValue('F1',trans('common::application.birthday'));
                            $sheet->setCellValue('G1',trans('common::application.death_date'));
                            $sheet->setCellValue('H1',trans('common::application.birth_place_main'));
                            $sheet->setCellValue('I1',trans('common::application.birth_place_sub'));
                            $sheet->setCellValue('J1',trans('common::application.mother_name'));
                            $sheet->setCellValue('K1',trans('common::application.address'));
                            $sheet->setCellValue('L1',trans('common::application.mobile'));
                            $sheet->setCellValue('M1',trans('common::application.phone'));
                            $sheet->setCellValue('N1',trans('common::application.SOCIAL_AFFAIRS_STATUS'));
                            $sheet->setCellValue('O1',trans('common::application.AID_CLASS'));
                            $sheet->setCellValue('P1',trans('common::application.AID_TYPE'));
                            $sheet->setCellValue('Q1',trans('common::application.AID_AMOUNT'));
                            $sheet->setCellValue('R1',trans('common::application.AID_SOURCE'));
                            $sheet->setCellValue('S1',trans('common::application.AID_PERIODIC'));
                            $sheet->setCellValue('T1',trans('common::application.ST_BENEFIT_DATE'));
                            $sheet->setCellValue('U1',trans('common::application.END_BENEFIT_DATE'));

                            $sheet->setCellValue('V1',trans('common::application.MINISTRY_NAME'));
                            $sheet->setCellValue('W1',trans('common::application.JOB_DESC'));
                            $sheet->setCellValue('X1',trans('common::application.JOB_START_DT'));
                            $sheet->setCellValue('Y1',trans('common::application.END_DATE'));
                            $sheet->setCellValue('Z1',trans('common::application.EMP_STATE_DESC'));
                            $sheet->setCellValue('AA1',trans('common::application.EMP_WORK_STATUS'));

                            $sheet->setCellValue('AB1',trans('common::application.MR_CODE'));
                            $sheet->setCellValue('AC1',trans('common::application.LOC_NAME_AR'));
                            $sheet->setCellValue('AD1',trans('common::application.MR_PATIENT_CD'));
                            $sheet->setCellValue('AE1',trans('common::application.DOCTOR_NAME'));
                            $sheet->setCellValue('AF1',trans('common::application.MR_CREATED_ON'));
                            $sheet->setCellValue('AG1',trans('common::application.DREF_NAME_AR'));
                            $sheet->setCellValue('AH1',trans('common::application.MR_DIAGNOSIS_AR'));
                            $sheet->setCellValue('AI1',trans('common::application.MR_DIAGNOSIS_EN'));
                            $sheet->setCellValue('AJ1',trans('common::application.MR_COMPLAINT'));
                            $sheet->setCellValue('AK1',trans('common::application.MR_EXAMINATION'));

                            $sheet->setCellValue('AL1',trans('common::application.COMP_NAME'));
                            $sheet->setCellValue('AM1',trans('common::application.REC_CODE'));
                            $sheet->setCellValue('AN1',trans('common::application.PERSON_TYPE_DESC'));
                            $sheet->setCellValue('AO1',trans('common::application.IS_VALID_DESC'));
                            $sheet->setCellValue('AP1',trans('common::application.STATUS_ID_DESC'));
                            $sheet->setCellValue('AQ1',trans('common::application.COMP_TYPE_DESC'));
                            $sheet->setCellValue('AR1',trans('common::application.REC_TYPE_DESC'));
                            $sheet->setCellValue('AS1',trans('common::application.REGISTER_NO'));
                            $sheet->setCellValue('AT1',trans('common::application.COMP_TYPE_DESC'));
                            $sheet->setCellValue('AU1',trans('common::application.START_DATE'));
                            $sheet->setCellValue('AV1',trans('common::application.WORK_CLASS_DESC'));
                            $sheet->setCellValue('AW1',trans('common::application.BRAND_NAME'));
                            $sheet->setCellValue('AX1',trans('common::application.CITY_DESC'));

                            if($type != 1){
                                $sheet->setCellValue('AY1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('AZ1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BA1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BB1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BC1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BD1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BE1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BF1',trans('common::application.birth_place_main').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BG1',trans('common::application.birth_place_sub').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BH1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                $sheet->setCellValue('BI1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                            }else{
                                $sheet->setCellValue('AY1',trans('common::application.family_count'));
                                $sheet->setCellValue('AZ1',trans('common::application.wives_count'));
                                $sheet->setCellValue('BA1',trans('common::application.male_count'));
                                $sheet->setCellValue('BB1',trans('common::application.female_count'));
                                $sheet->getStyle("A1:BB1")->applyFromArray(['font' => ['bold' => true]]);
                            }

                            $z= 2;
                            foreach($final_records as $k=>$main ) {

                                $main = (Object) $main;
                                $v = (Object) $main->related;

                                $sheet->setCellValue('A' . $z, $k+1);
                                $sheet->setCellValue('B' . $z,
                                    ((is_null($main->FNAME_ARB) || $main->FNAME_ARB == ' ') ? ' ' : $main->FNAME_ARB) . ' ' .
                                    ((is_null($main->SNAME_ARB) || $main->SNAME_ARB == ' ') ? ' ' : $main->SNAME_ARB) . ' ' .
                                    ((is_null($main->TNAME_ARB) || $main->TNAME_ARB == ' ') ? ' ' : $main->TNAME_ARB) . ' ' .
                                    ((is_null($main->LNAME_ARB) || $main->LNAME_ARB == ' ') ? ' ' : $main->LNAME_ARB)
                                );
                                $sheet->setCellValue('C' . $z, (is_null($main->IDNO) || $main->IDNO == ' ') ? '-' : $main->IDNO);
                                $sheet->setCellValue('D' . $z, (is_null($main->SEX) || $main->SEX == ' ') ? '-' : $main->SEX);
                                $sheet->setCellValue('E' . $z, (is_null($main->SOCIAL_STATUS) || $main->SOCIAL_STATUS == ' ') ? '-' : $main->SOCIAL_STATUS);
                                $sheet->setCellValue('F' . $z, (is_null($main->BIRTH_DT) || $main->BIRTH_DT == ' ') ? '-' : $main->BIRTH_DT);
                                $sheet->setCellValue('G' . $z, (is_null($main->DETH_DT) || $main->DETH_DT == ' ') ? '-' : $main->DETH_DT);
                                $sheet->setCellValue('H' . $z, (is_null($main->BIRTH_PMAIN) || $main->BIRTH_PMAIN == ' ') ? '-' : $main->BIRTH_PMAIN);
                                $sheet->setCellValue('I' . $z, (is_null($main->BIRTH_PSUB) || $main->BIRTH_PSUB == ' ') ? '-' : $main->BIRTH_PSUB);
                                $sheet->setCellValue('J' . $z, (is_null($main->MOTHER_ARB) || $main->MOTHER_ARB == ' ') ? '-' : $main->MOTHER_ARB);
                                $sheet->setCellValue('K' . $z,
                                    ((is_null($main->CI_REGION) || $main->CI_REGION == ' ') ? ' ' : $main->CI_REGION) . ' ' .
                                    ((is_null($main->CI_CITY) || $main->CI_CITY == ' ') ? ' ' : $main->CI_CITY) . ' ' .
                                    ((is_null($main->STREET_ARB) || $main->STREET_ARB == ' ') ? ' ' : $main->STREET_ARB)
                                );

                                $sheet->setCellValue('L' . $z, (is_null($main->MOBILE) || $main->MOBILE == ' ') ? '-' : $main->MOBILE);
                                $sheet->setCellValue('M' . $z, (is_null($main->TEL) || $main->TEL == ' ') ? '-' : $main->TEL);

                                $sheet->setCellValue('N'.$z,(is_null($main->social_affairs_status) || $main->social_affairs_status == ' ' ) ? '-' : $main->SOCIAL_AFFAIRS_STATUS);
                                $sheet->setCellValue('O'.$z,(is_null($main->AID_CLASS) || $main->AID_CLASS == ' ' ) ? '-' : $main->AID_CLASS);
                                $sheet->setCellValue('P'.$z,(is_null($main->AID_TYPE) || $main->AID_TYPE == ' ' ) ? '-' : $main->AID_TYPE);
                                $sheet->setCellValue('Q'.$z,(is_null($main->AID_AMOUNT) || $main->AID_AMOUNT == ' ' ) ? '-' : $main->AID_AMOUNT);
                                $sheet->setCellValue('R'.$z,(is_null($main->AID_SOURCE) || $main->AID_SOURCE == ' ' ) ? '-' : $main->AID_SOURCE);
                                $sheet->setCellValue('S'.$z,(is_null($main->AID_PERIODIC) || $main->AID_PERIODIC == ' ' ) ? '-' : $main->AID_PERIODIC);
                                $sheet->setCellValue('T'.$z,(is_null($main->ST_BENEFIT_DATE) || $main->ST_BENEFIT_DATE == ' ' ) ? '-' : $main->ST_BENEFIT_DATE);
                                $sheet->setCellValue('U'.$z,(is_null($main->END_BENEFIT_DATE) || $main->END_BENEFIT_DATE == ' ' ) ? '-' : $main->END_BENEFIT_DATE);

                                $sheet->setCellValue('V'.$z,(is_null($main->MINISTRY_NAME) || $main->MINISTRY_NAME == ' ' ) ? '-' : $main->MINISTRY_NAME);
                                $sheet->setCellValue('W'.$z,(is_null($main->JOB_DESC) || $main->JOB_DESC == ' ' ) ? '-' : $main->JOB_DESC);
                                $sheet->setCellValue('X'.$z,(is_null($main->JOB_START_DT) || $main->JOB_START_DT == ' ' ) ? '-' : $main->JOB_START_DT);
                                $sheet->setCellValue('Y'.$z,(is_null($main->END_DATE) || $main->END_DATE == ' ' ) ? '-' : $main->END_DATE);
                                $sheet->setCellValue('Z'.$z,(is_null($main->EMP_STATE_DESC) || $main->EMP_STATE_DESC == ' ' ) ? '-' : $main->EMP_STATE_DESC);
                                $sheet->setCellValue('AA'.$z,(is_null($main->EMP_WORK_STATUS) || $main->EMP_WORK_STATUS == ' ' ) ? '-' : $main->EMP_WORK_STATUS);


                                $sheet->setCellValue('AB'.$z,(is_null($main->MR_CODE) || $main->MR_CODE == ' ' ) ? '-' : $main->MR_CODE);
                                $sheet->setCellValue('AC'.$z,(is_null($main->LOC_NAME_AR) || $main->LOC_NAME_AR == ' ' ) ? '-' : $main->LOC_NAME_AR);
                                $sheet->setCellValue('AD'.$z,(is_null($main->MR_PATIENT_CD) || $main->MR_PATIENT_CD == ' ' ) ? '-' : $main->MR_PATIENT_CD);
                                $sheet->setCellValue('AE'.$z,(is_null($main->DOCTOR_NAME) || $main->DOCTOR_NAME == ' ' ) ? '-' : $main->DOCTOR_NAME);

                                $sheet->setCellValue('AF'.$z,(is_null($main->MR_CREATED_ON) || $main->MR_CREATED_ON == ' ' ) ? '-' : $main->MR_CREATED_ON);
                                $sheet->setCellValue('AG'.$z,(is_null($main->DREF_NAME_AR) || $main->DREF_NAME_AR == ' ' ) ? '-' : $main->DREF_NAME_AR);
                                $sheet->setCellValue('AH'.$z,(is_null($main->MR_DIAGNOSIS_AR) || $main->MR_DIAGNOSIS_AR == ' ' ) ? '-' : $main->MR_DIAGNOSIS_AR);
                                $sheet->setCellValue('AI'.$z,(is_null($main->MR_DIAGNOSIS_EN) || $main->MR_DIAGNOSIS_EN == ' ' ) ? '-' : $main->MR_DIAGNOSIS_EN);
                                $sheet->setCellValue('AJ'.$z,(is_null($main->MR_COMPLAINT) || $main->MR_COMPLAINT == ' ' ) ? '-' : $main->MR_COMPLAINT);
                                $sheet->setCellValue('AK'.$z,(is_null($main->MR_EXAMINATION) || $main->MR_EXAMINATION == ' ' ) ? '-' : $main->MR_EXAMINATION);

                                $sheet->setCellValue('AL'.$z,(is_null($main->COMP_NAME) || $main->COMP_NAME == ' ' ) ? '-' : $main->COMP_NAME);
                                $sheet->setCellValue('AM'.$z,(is_null($main->REC_CODE) || $main->REC_CODE == ' ' ) ? '-' : $main->REC_CODE);
                                $sheet->setCellValue('AN'.$z,(is_null($main->PERSON_TYPE_DESC) || $main->PERSON_TYPE_DESC == ' ' ) ? '-' : $main->PERSON_TYPE_DESC);
                                $sheet->setCellValue('AO'.$z,(is_null($main->IS_VALID_DESC) || $main->IS_VALID_DESC == ' ' ) ? '-' : $main->IS_VALID_DESC);
                                $sheet->setCellValue('AP'.$z,(is_null($main->STATUS_ID_DESC) || $main->STATUS_ID_DESC == ' ' ) ? '-' : $main->STATUS_ID_DESC);
                                $sheet->setCellValue('AQ'.$z,(is_null($main->COMP_TYPE_DESC) || $main->COMP_TYPE_DESC == ' ' ) ? '-' : $main->COMP_TYPE_DESC);
                                $sheet->setCellValue('AR'.$z,(is_null($main->REC_TYPE_DESC) || $main->REC_TYPE_DESC == ' ' ) ? '-' : $main->REC_TYPE_DESC);
                                $sheet->setCellValue('AS'.$z,(is_null($main->REGISTER_NO) || $main->REGISTER_NO == ' ' ) ? '-' : $main->REGISTER_NO);
                                $sheet->setCellValue('AT'.$z,(is_null($main->COMP_TYPE_DESC) || $main->COMP_TYPE_DESC == ' ' ) ? '-' : $main->COMP_TYPE_DESC);
                                $sheet->setCellValue('AU'.$z,(is_null($main->START_DATE) || $main->START_DATE == ' ' ) ? '-' : $main->START_DATE);
                                $sheet->setCellValue('AV'.$z,(is_null($main->WORK_CLASS_DESC) || $main->WORK_CLASS_DESC == ' ' ) ? '-' : $main->WORK_CLASS_DESC);
                                $sheet->setCellValue('AW'.$z,(is_null($main->BRAND_NAME) || $main->BRAND_NAME == ' ' ) ? '-' : $main->BRAND_NAME);
                                $sheet->setCellValue('AX'.$z,(is_null($main->CITY_DESC) || $main->CITY_DESC == ' ' ) ? '-' : $main->CITY_DESC);

                                if($type != 1){
                                    if($main->related){
                                        $sheet->setCellValue('AY' . $z, (is_null($v->IDNO_RELATIVE) || $v->IDNO_RELATIVE == ' ') ? '-' : $v->IDNO_RELATIVE);
                                        $sheet->setCellValue('AZ' . $z, (is_null($v->RELATIVE_DESC) || $v->RELATIVE_DESC == ' ') ? '-' : $v->RELATIVE_DESC);
                                        $sheet->setCellValue('BA' . $z,
                                            ((is_null($v->FNAME_ARB) || $v->FNAME_ARB == ' ') ? ' ' : $v->FNAME_ARB) . ' ' .
                                            ((is_null($v->SNAME_ARB) || $v->SNAME_ARB == ' ') ? ' ' : $v->SNAME_ARB) . ' ' .
                                            ((is_null($v->TNAME_ARB) || $v->TNAME_ARB == ' ') ? ' ' : $v->TNAME_ARB) . ' ' .
                                            ((is_null($v->LNAME_ARB) || $v->LNAME_ARB == ' ') ? ' ' : $v->LNAME_ARB)
                                        );
                                        $sheet->setCellValue('BB' . $z, (is_null($v->SEX) || $v->SEX == ' ') ? '-' : $v->SEX);
                                        $sheet->setCellValue('BC' . $z, (is_null($v->SOCIAL_STATUS) || $v->SOCIAL_STATUS == ' ') ? '-' : $v->SOCIAL_STATUS);
                                        $sheet->setCellValue('BD' . $z, (is_null($v->BIRTH_DT) || $v->BIRTH_DT == ' ') ? '-' : $v->BIRTH_DT);
                                        $sheet->setCellValue('BE' . $z, (is_null($v->DETH_DT) || $v->DETH_DT == ' ') ? '-' : $v->DETH_DT);
                                        $sheet->setCellValue('BF' . $z, (is_null($v->BIRTH_PMAIN) || $v->BIRTH_PMAIN == ' ') ? '-' : $v->BIRTH_PMAIN);
                                        $sheet->setCellValue('BG' . $z, (is_null($v->BIRTH_PSUB) || $v->BIRTH_PSUB == ' ') ? '-' : $v->BIRTH_PSUB);
                                        $sheet->setCellValue('BH' . $z, (is_null($v->PREV_LNAME_ARB) || $v->PREV_LNAME_ARB == ' ') ? '-' : $v->PREV_LNAME_ARB);
                                        $sheet->setCellValue('BI' . $z, (is_null($v->MOTHER_ARB) || $v->MOTHER_ARB == ' ') ? '-' : $v->MOTHER_ARB);

                                    } else {

                                        $sheet->setCellValue('AY' . $z, '-');
                                        $sheet->setCellValue('AZ' . $z, '-');
                                        $sheet->setCellValue('BA' . $z, '-');
                                        $sheet->setCellValue('BB' . $z, '-');
                                        $sheet->setCellValue('BC' . $z, '-');
                                        $sheet->setCellValue('BD' . $z, '-');
                                        $sheet->setCellValue('BE' . $z, '-');
                                        $sheet->setCellValue('BF' . $z, '-');
                                        $sheet->setCellValue('BG' . $z, '-');
                                        $sheet->setCellValue('BH' . $z, '-');
                                        $sheet->setCellValue('BI' . $z, '-');

                                    }
                                }else{
                                    $sheet->setCellValue('AY' . $z, (is_null($main->family_cnt) || $main->family_cnt == ' ') ? '-' : $main->family_cnt);
                                    $sheet->setCellValue('AZ' . $z, (is_null($main->wives_count) || $main->wives_count == ' ') ? '-' : $main->wives_count);
                                    $sheet->setCellValue('BA' . $z, (is_null($main->male_live) || $main->male_live == ' ') ? '-' : $main->male_live);
                                    $sheet->setCellValue('BB' . $z, (is_null($main->female_live) || $main->female_live == ' ') ? '-' : $main->female_live);
                                }
                                $sheet->getRowDimension($z)->setRowHeight(-1);
                                $z++;
                            }
                        }else{

                            $sheet->getStyle("A1:S1")->applyFromArray(['font' => ['bold' => true]]);
                            $sheet->setCellValue('A1',trans('common::application.#'));
                            $sheet->setCellValue('B1',trans('common::application.full_name'));
                            $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                            $sheet->setCellValue('D1',trans('common::application.marital_status'));
                            $sheet->setCellValue('E1',trans('common::application.death_date'));
                            $sheet->setCellValue('F1',trans('common::application.address'));
                            $sheet->setCellValue('G1',trans('common::application.mobile'));
                            $sheet->setCellValue('H1',trans('common::application.phone'));
                            $sheet->setCellValue('I1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('J1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('K1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('L1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('M1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('N1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('O1',trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('P1',trans('common::application.birth_place_main').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('Q1',trans('common::application.birth_place_sub').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('R1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                            $sheet->setCellValue('S1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');

                            $z= 2;
                            foreach($final_records as $k=>$main ) {

                                $main = (Object) $main;
                                $v = (Object) $main->related;

                                $sheet->setCellValue('A' . $z, $k+1);
                                $sheet->setCellValue('B' . $z,
                                    ((is_null($main->FNAME_ARB) || $main->FNAME_ARB == ' ') ? ' ' : $main->FNAME_ARB) . ' ' .
                                    ((is_null($main->SNAME_ARB) || $main->SNAME_ARB == ' ') ? ' ' : $main->SNAME_ARB) . ' ' .
                                    ((is_null($main->TNAME_ARB) || $main->TNAME_ARB == ' ') ? ' ' : $main->TNAME_ARB) . ' ' .
                                    ((is_null($main->LNAME_ARB) || $main->LNAME_ARB == ' ') ? ' ' : $main->LNAME_ARB)
                                );
                                $sheet->setCellValue('C' . $z, (is_null($main->IDNO) || $main->IDNO == ' ') ? '-' : $main->IDNO);
                                $sheet->setCellValue('D' . $z, (is_null($main->SOCIAL_STATUS) || $main->SOCIAL_STATUS == ' ') ? '-' : $main->SOCIAL_STATUS);
//                                                        $sheet->setCellValue('E' . $z, (is_null($main->BIRTH_DT) || $main->BIRTH_DT == ' ') ? '-' : $main->BIRTH_DT);
                                $sheet->setCellValue('E' . $z, (is_null($main->DETH_DT) || $main->DETH_DT == ' ') ? '-' : $main->DETH_DT);
                                $sheet->setCellValue('F' . $z,
                                    ((is_null($main->CI_REGION) || $main->CI_REGION == ' ') ? ' ' : $main->CI_REGION) . ' ' .
                                    ((is_null($main->CI_CITY) || $main->CI_CITY == ' ') ? ' ' : $main->CI_CITY) . ' ' .
                                    ((is_null($main->STREET_ARB) || $main->STREET_ARB == ' ') ? ' ' : $main->STREET_ARB)
                                );
                                $sheet->setCellValue('G' . $z, (is_null($main->MOBILE) || $main->MOBILE == ' ') ? '-' : $main->MOBILE);
                                $sheet->setCellValue('H' . $z, (is_null($main->TEL) || $main->TEL == ' ') ? '-' : $main->TEL);
                                if($main->related){
                                    $sheet->setCellValue('I' . $z, (is_null($v->IDNO) || $v->IDNO == ' ') ? '-' : $v->IDNO);
                                    $sheet->setCellValue('J' . $z, (is_null($v->kinship_name) || $v->kinship_name == ' ') ? '-' : $v->kinship_name);
                                    $sheet->setCellValue('K' . $z,
                                        ((is_null($v->FNAME_ARB) || $v->FNAME_ARB == ' ') ? ' ' : $v->FNAME_ARB) . ' ' .
                                        ((is_null($v->SNAME_ARB) || $v->SNAME_ARB == ' ') ? ' ' : $v->SNAME_ARB) . ' ' .
                                        ((is_null($v->TNAME_ARB) || $v->TNAME_ARB == ' ') ? ' ' : $v->TNAME_ARB) . ' ' .
                                        ((is_null($v->LNAME_ARB) || $v->LNAME_ARB == ' ') ? ' ' : $v->LNAME_ARB)
                                    );
                                    $sheet->setCellValue('L' . $z, (is_null($v->SEX) || $v->SEX == ' ') ? '-' : $v->SEX);
                                    $sheet->setCellValue('M' . $z, (is_null($v->SOCIAL_STATUS) || $v->SOCIAL_STATUS == ' ') ? '-' : $v->SOCIAL_STATUS);
                                    $sheet->setCellValue('N' . $z, (is_null($v->BIRTH_DT) || $v->BIRTH_DT == ' ') ? '-' : $v->BIRTH_DT);
                                    $sheet->setCellValue('O' . $z, (is_null($v->DETH_DT) || $v->DETH_DT == ' ') ? '-' : $v->DETH_DT);
                                    $sheet->setCellValue('P' . $z, (is_null($v->BIRTH_PMAIN) || $v->BIRTH_PMAIN == ' ') ? '-' : $v->BIRTH_PMAIN);
                                    $sheet->setCellValue('Q' . $z, (is_null($v->BIRTH_PSUB) || $v->BIRTH_PSUB == ' ') ? '-' : $v->BIRTH_PSUB);
                                    $sheet->setCellValue('R' . $z, (is_null($v->PREV_LNAME_ARB) || $v->PREV_LNAME_ARB == ' ') ? '-' : $v->PREV_LNAME_ARB);
                                    $sheet->setCellValue('S' . $z, (is_null($v->MOTHER_ARB) || $v->MOTHER_ARB == ' ') ? '-' : $v->MOTHER_ARB);
                                } else {
                                    $sheet->setCellValue('I' . $z, '-');
                                    $sheet->setCellValue('J' . $z, '-');
                                    $sheet->setCellValue('K' . $z, '-');
                                    $sheet->setCellValue('L' . $z, '-');
                                    $sheet->setCellValue('M' . $z, '-');
                                    $sheet->setCellValue('N' . $z, '-');
                                    $sheet->setCellValue('O' . $z, '-');
                                    $sheet->setCellValue('P' . $z, '-');
                                    $sheet->setCellValue('Q' . $z, '-');
                                    $sheet->setCellValue('R' . $z, '-');
                                    $sheet->setCellValue('S' . $z, '-');
                                }
                                $sheet->getRowDimension($z)->setRowHeight(-1);
                                $z++;
                            }
                        }



                    });
                }
                if(sizeof($health) > 0){
                    $excel->sheet(trans('common::application.health info'), function($sheet) use($health) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.full_name'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.id_card_number'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.MR_CODE'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.LOC_NAME_AR'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.MR_PATIENT_CD'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.DOCTOR_NAME'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.MR_CREATED_ON'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.DREF_NAME_AR'));
                        $sheet->setCellValue('J' . $start_from ,trans('common::application.MR_DIAGNOSIS_AR'));
                        $sheet->setCellValue('K' . $start_from ,trans('common::application.MR_DIAGNOSIS_EN'));
                        $sheet->setCellValue('L' . $start_from ,trans('common::application.MR_COMPLAINT'));
                        $sheet->setCellValue('M' . $start_from ,trans('common::application.MR_EXAMINATION'));
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('L')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('M')->getAlignment()->setWrapText(true);
                        $z= 2;
                        foreach($health as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->name) ||$v->name == ' ' ) ? '-' :$v->name);
                            $sheet->setCellValue('C'.$z,(is_null($v->card) ||$v->card == ' ' ) ? '-' :$v->card);
                            $sheet->setCellValue('D'.$z,(is_null($v->MR_CODE) ||$v->MR_CODE == ' ' ) ? '-' :$v->MR_CODE);
                            $sheet->setCellValue('E'.$z,(is_null($v->LOC_NAME_AR) ||$v->LOC_NAME_AR == ' ' ) ? '-' :$v->LOC_NAME_AR);
                            $sheet->setCellValue('F'.$z,(is_null($v->MR_PATIENT_CD) ||$v->MR_PATIENT_CD == ' ' ) ? '-' :$v->MR_PATIENT_CD);
                            $sheet->setCellValue('H'.$z,(is_null($v->DOCTOR_NAME) ||$v->DOCTOR_NAME == ' ' ) ? '-' :$v->DOCTOR_NAME);
                            $sheet->setCellValue('H'.$z,(is_null($v->MR_CREATED_ON) ||$v->MR_CREATED_ON == ' ' ) ? '-' :$v->MR_CREATED_ON);
                            $sheet->setCellValue('I'.$z,(is_null($v->DREF_NAME_AR) ||$v->DREF_NAME_AR == ' ' ) ? '-' :$v->DREF_NAME_AR);
                            $sheet->setCellValue('J'.$z,(is_null($v->MR_DIAGNOSIS_AR) ||$v->MR_DIAGNOSIS_AR == ' ' ) ? '-' :$v->MR_DIAGNOSIS_AR);
                            $sheet->setCellValue('K'.$z,(is_null($v->MR_DIAGNOSIS_EN) ||$v->MR_DIAGNOSIS_EN == ' ' ) ? '-' :$v->MR_DIAGNOSIS_EN);
                            $sheet->setCellValue('L'.$z,(is_null($v->MR_COMPLAINT) || $v->MR_COMPLAINT == ' ' ) ? '-' : $v->MR_COMPLAINT);
                            $sheet->setCellValue('M'.$z,(is_null($v->MR_EXAMINATION) || $v->MR_EXAMINATION == ' ' ) ? '-' : $v->MR_EXAMINATION);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);

                    });
                }
                if (sizeof($cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($cases_list) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                    });
                }
                if(sizeof($invalid_cards) > 0){
                    $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                        $z= 2;
                        foreach($invalid_cards as $v){
                            $sheet->setCellValue('A'.$z,$v);
                            $z++;
                        }

                    });
                }

            })->store('xlsx', storage_path('tmp/'));

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

    }

}