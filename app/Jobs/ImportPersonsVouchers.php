<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Aid\Model\VoucherPersons;
use Common\Model\Person;
use Common\Model\GovServices;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class ImportPersonsVouchers  implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $cards;
    protected $voucher_id;
    protected $status;
    
    public function __construct($voucher_id,$status,$cards){
//        $this->queue = 'persons-vouchers';
        $this->cards = $cards;
        $this->voucher_id = $voucher_id;
        $this->status = $status;
    }

    public function handle(){

        DB::beginTransaction();
        try {

            $process =[];
            $restricted =[];
            $pass =[];

            foreach ($this->cards  as $key => $value) {
                $card =$value['rkm_alhoy'];
                if(in_array($card,$process)){
                    $restricted [] = array('id_card_number'=>$card,'reason'=>'duplicated','name' => Null);
                }
                else{
                    if(strlen(($card))  <= 9 || GovServices::checkCard($card)) {
                        $person = Person::where(['id_card_number' =>  $card ])->first();
                        if ($person) {
                            $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
                            $founded = VoucherPersons::where(['voucher_id' => $this->voucher_id,'person_id' => $person->id])->first();
                            if ($founded) {
                                $restricted [] = array('id_card_number'=>$card,'reason'=>'nominated','name' => $full_name);
                            } else {
                                $pass [] = array('id_card_number'=>$card,'name' => $full_name);
                                VoucherPersons::create(['voucher_id'=>  $this->voucher_id  ,'status'=> $this->status , 'person_id'=>  $person->id  ]);
                            }
                        }
                        else{
                            $restricted [] = array('id_card_number'=>$card,'reason'=>'not_person','name' => Null);
                        }
                    }
                    else{
                        $restricted [] = array('id_card_number'=>$card,'reason'=>'invalid_card','name' => Null);
                    }
                }
            }

            $token = md5(uniqid());
            Excel::create('export_' . $token, function($excel) use($restricted,$pass) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.restricted_cards'));

                if (sizeof($pass) > 0){
                    $excel->sheet('pass', function($sheet) use($pass)  {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:B1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('B1',trans('common::application.name'));
                        $z= 2;
                        foreach($pass as $k=>$v){
                            $sheet->setCellValue('A'.$z,$v['id_card_number']);
                            $sheet->setCellValue('B'.$z,$v['name']);
                            $z++;
                        }

                    });
                }

                if (sizeof($restricted) > 0){
                    $excel->sheet('restricted', function($sheet) use($restricted)  {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('B1',trans('common::application.name'));
                        $sheet->setCellValue('C1',trans('common::application.reason'));

                        $z= 2;
                        foreach($restricted as $k=>$v){
                            $sheet->setCellValue('A'.$z,$v['id_card_number']);
                            $sheet->setCellValue('B'.$z,$v['name']);
                            $sheet->setCellValue('C'.$z,$v['reason']);
                            $z++;
                        }

                    });
                }


            })->store('xlsx', storage_path('tmp/'));

            DB::commit();
            return true;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

    }

}