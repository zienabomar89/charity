<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'كلمات المرور يجب أن تتكون على الأقل من 6 أحرف ومتطابقة مع التأكيد.',
    'reset' => 'تم إعادة تعيين كلمة المرور الخاصة بك!',
    'sent' => 'تم إرسال رابط إعادة تعيين كلمة المرور إلى عنوان البريد الإلكتروني الخاص بك!',
    'token' => 'رمز إعادة تعيين كلمة المرور غير صحيح.',
    'user' => "لا يوجد مستخدم مسجل بعنوان البريد الإلكتروني المدخل.",

];
