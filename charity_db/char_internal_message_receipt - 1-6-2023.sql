
-- ----------------------------
DROP TABLE IF EXISTS `char_internal_message_receipt`;
CREATE TABLE `char_internal_message_receipt`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول',
  `date` date NOT NULL COMMENT 'التاريخ',
  `receipt_user_id` int(10) UNSIGNED NOT NULL COMMENT 'المستخدم الذي قام بالاستلام',
  `type` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'النوع',
  `msg_id` int(11) NOT NULL COMMENT 'كود الرسالة',
  `origin_msg_id` int(11) NOT NULL COMMENT 'رقم الرسالة الاصلية',
  `is_read` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'تمت القراءة نعم او لا',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ اخر تعديل على السجل',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `char_internal_message_receipt$receipt_user_id_indx`(`receipt_user_id`) USING BTREE,
  INDEX `char_internal_message_receipt$msg_id_indx`(`msg_id`) USING BTREE,
  CONSTRAINT `char_internal_message_receipt$msg_id_fk` FOREIGN KEY (`msg_id`) REFERENCES `char_internal_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `char_internal_message_receipt$receipt_user_id_fk` FOREIGN KEY (`receipt_user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = 'جدول يحتوي على مستلمي المراسلات ' ROW_FORMAT = COMPACT;

