INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(533, 3, 'aid.voucher.deleteGroup', 'حذف القسائم من خلال الكود الموحد');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 533, 1, CURRENT_TIMESTAMP, 1),
(2, 533, 1, CURRENT_TIMESTAMP, 1);

UPDATE `char_permissions` SET `module_id` = '3' WHERE code LIKE 'aid.%';
-- ------------------------------------------------------------------------------------------

ALTER TABLE `char_administrative_report_organization` ADD `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'اسم المستخدم';
ALTER TABLE `char_administrative_report_organization` ADD KEY `fk_administrative_report_organization_userId_idx` (`user_id`);

ALTER TABLE `char_administrative_report_organization`
  ADD CONSTRAINT `fk_administrative_report_organization_userId` FOREIGN KEY (`user_id`)
  REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


UPDATE `char_administrative_report_organization` , `char_users`
SET `char_administrative_report_organization`.`user_id` = `char_users`.`id`
where `char_users`.`type` = 3 and char_administrative_report_organization.organization_id = `char_users`.`organization_id` and (`char_administrative_report_organization`.`organization_id` in ( SELECT id FROM `char_organizations` WHERE level = 3)) ;


UPDATE `char_administrative_report_organization` , `char_users`
SET `char_administrative_report_organization`.`user_id` = `char_users`.`id`
where `char_users`.`type` = 1 and char_administrative_report_organization.organization_id = `char_users`.`organization_id` and (`char_administrative_report_organization`.`organization_id` in ( SELECT id FROM `char_organizations` WHERE level = 2)) ;


UPDATE `char_administrative_report_organization` , `char_users`
SET `char_administrative_report_organization`.`user_id` = `char_users`.`id`
where `char_users`.`type` = 3 and char_administrative_report_organization.organization_id = `char_users`.`organization_id` and (`char_administrative_report_organization`.`organization_id` in ( SELECT id FROM `char_organizations` WHERE level = 1)) ;

ALTER TABLE `char_social_search_cases_essentials` CHANGE `condition` `condition` TINYINT(4) NULL DEFAULT NULL COMMENT 'الحالة: 5 ممتاز، 4 جيد جدا، 3 جيد، 2 سيء، 0 لا يوجد';

ALTER TABLE `char_social_search` ADD `close` tinyint(1) DEFAULT NULL COMMENT 'هل الدورة مغلقة ام مفتوحة : 1 مفتوحة 0 مغلقة';
ALTER TABLE `char_social_search` ADD `reward_id` int(11) DEFAULT NULL COMMENT 'رقم الدورة في نظام باحث';
ALTER TABLE `char_social_search_cases` ADD `operation_case_id` int(11) DEFAULT NULL COMMENT 'رقم الحالة في نظام باحث';

ALTER TABLE `char_social_search_cases_details`
 CHANGE `needs` `needs` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'الاحتياجات',
 CHANGE `promised` `promised` TINYINT(1) NULL DEFAULT NULL COMMENT 'هل وعدت بالبناء : : 1- لا ، 2 نعم',
 CHANGE `reconstructions_organization_name` `reconstructions_organization_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL
 DEFAULT NULL COMMENT 'اسم الجمعية او المؤسسة التي وعدت العائلة';
-- ------------------------------------------------------------------------------------------------------------
ALTER TABLE `char_social_search_cases_family` ADD `birth_place` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'مكان الميلاد' AFTER birthday;
ALTER TABLE `char_social_search_cases_family` ADD `monthly_income` FLOAT NULL DEFAULT NULL COMMENT 'الدخل الشهري الكلي' AFTER birthday;
ALTER TABLE `char_social_search_cases_family` ADD `study` tinyint(1) NULL DEFAULT NULL COMMENT 'حالة الدراسة' AFTER `details`;
ALTER TABLE `char_social_search_cases_family` ADD `study_type` tinyint(4) NULL DEFAULT NULL COMMENT 'نوع الدراسة: 1 أكاديمي، 2 مهني' AFTER `study`;
ALTER TABLE `char_social_search_cases_family` ADD `authority` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'معرف المؤسسة التعليمية' AFTER `study_type`;
ALTER TABLE `char_social_search_cases_family` ADD `stage` int(10) UNSIGNED NULL DEFAULT NULL COMMENT 'المرحلة' AFTER `authority`;
ALTER TABLE `char_social_search_cases_family` ADD `specialization` varchar(255) DEFAULT '' COMMENT 'التخصص' AFTER `stage`;
ALTER TABLE `char_social_search_cases_family` ADD `degree` INT(10) NULL DEFAULT NULL AFTER `grade`;
ALTER TABLE `char_social_search_cases_family` ADD `year` varchar(45) DEFAULT '' COMMENT 'السنة الدراسية' AFTER `degree`;
ALTER TABLE `char_social_search_cases_family` ADD `level` tinyint(4) NULL DEFAULT NULL
COMMENT 'مستوى التحصيل: 1ضعيف، 2 مقبول، 3 جيد، 4 جيد جدا، 5 ممتاز' AFTER `year`;
ALTER TABLE `char_social_search_cases_family` ADD `points` float NULL DEFAULT NULL COMMENT 'المعدل' AFTER `level`;

ALTER TABLE `char_social_search_cases_family` ADD `working` tinyint(1) UNSIGNED DEFAULT NULL COMMENT 'هل يعمل:1-نعم ، 2-لا ' AFTER `school`;
ALTER TABLE `char_social_search_cases_family` ADD `work_job_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المسمى الوظيفي'AFTER `working`;

-- ------------------------------------------------------------------------------------------------------------
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(534, 1, 'auth.users.online', 'المستخدمين المتصلين');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 534, 1, CURRENT_TIMESTAMP, 1),
(2, 534, 1, CURRENT_TIMESTAMP, 1);

-- -------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------
CREATE TABLE `char_aids_locations_tree` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'كود العنوان',
  `location_type` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'نوع العنوان',
  `name` varchar(255) NOT NULL COMMENT 'التسمية',
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الأب',
  `parent_name` varchar(255) UNSIGNED DEFAULT NULL COMMENT 'اسم الاب'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول عناوين المساعدات بشكل شجري' ROW_FORMAT=COMPACT;

CREATE TABLE `rewards_cases_locations_np` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدولة',
  `country_takamul` varchar(255) DEFAULT NULL COMMENT 'الدولة',
  `district_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	المحافظة',
  `district_takamul` varchar(255) DEFAULT NULL COMMENT '	المحافظة',
  `region_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المنطقة',
  `region_takamul` varchar(255) DEFAULT NULL COMMENT 'المنطقة',
  `neighborhood_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	الحي',
  `neighborhood_takamul` varchar(255) DEFAULT NULL COMMENT '	الحي',
  `square_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المربع',
  `square_takamul` varchar(255) DEFAULT NULL COMMENT 'المربع',
  `mosque_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المسجد',
  `mosque_takamul` varchar(255) DEFAULT NULL COMMENT 'المسجد',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- -------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------

CREATE TABLE `rewards_cases` (
  `operation_case_id` bigint(20) UNSIGNED NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `id_card_number` int(11) DEFAULT NULL COMMENT 'رقم الهوية',
  `card_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع البطاقة',
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `fullname_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم الأول',
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الأب',
  `third_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجد',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة',
  `prev_family_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة السابق',
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط الطول',
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط العرض',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 0- أنثى',
  `marital_status_id` int(11) DEFAULT NULL COMMENT 'الحالة الاجتماعية ',
  `deserted` int(11) DEFAULT NULL COMMENT 'مهجورة في حال انثى متزوجة :1-نعم ، 0-لا',
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'مكان الميلاد - جدول ثوابت',
  `death_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `death_cause_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'سبب الوفاة - جدول ثوابت',
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الجنسية ''المفتاح الأساسي للجدول'',',
  `family_cnt` int(11) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `spouses` int(11) DEFAULT NULL COMMENT 'عدد الزوجات',
  `male_live` int(11) DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة',
  `female_live` int(11) DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة',
  `refugee` int(11) DEFAULT NULL COMMENT 'حالة المواطنة:1- مواطن ، 2-لاجيء',
  `unrwa_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم كرت المؤن في ال كان لاجيء',
  `is_qualified` int(11) DEFAULT NULL COMMENT '	هل مؤهل:1-مؤهل ، 0-غير مؤهل',
  `qualified_card_number` int(11) DEFAULT NULL COMMENT '	رقم هوية المعييل في حال كان رب الاسرة غير مؤهل',
  `receivables` int(11) DEFAULT NULL COMMENT '	هل لديه ذمم مالية:1-نعم ، 0-لا',
  `receivables_sk_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'قيمة الذمم المالية بالشيكل',
  `monthly_income` int(11) DEFAULT NULL COMMENT 'الدخل الشهري الكلي',
  `actual_monthly_income` int(11) DEFAULT NULL COMMENT '	الدخل الشهري الفعلي',
  `has_commercial_records` int(11) DEFAULT NULL COMMENT ' هل لديه سجل تجاري :1 نعم , 0 لا',
  `active_commercial_records` int(11) DEFAULT NULL COMMENT 'عدد السجلات التجاريةالفعالة',
  `gov_commercial_records_details` text COLLATE utf8mb4_unicode_ci COMMENT 'بيان السجلات التجارية',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم التلفون الارضي',
  `primary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الجوال الاساسي',
  `secondary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم الجوال الاحتياطي',
  `watanya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الوطنية',
  `charity_id` int(11) DEFAULT NULL COMMENT ' الجمعية',
  `country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدولة - جدول ثوابت',
  `district_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	المحافظة - جدول ثوابت',
  `region_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المنطقة - جدول ثوابت',
  `neighborhood_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	الحي - جدول ثوابت',
  `square_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المربع - جدول ثوابت',
  `mosque_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المسجد - جدول ثوابت',
  `street_address_details` text COLLATE utf8mb4_unicode_ci COMMENT '	تفاصيل العنوان - الشارع',
  `street_address` text COLLATE utf8mb4_unicode_ci COMMENT '	 العنوان - الشارع',
  `needs` text COLLATE utf8mb4_unicode_ci COMMENT 'الاحتياجات',
  `promised` int(11) DEFAULT NULL COMMENT 'هل وعدت بالبناء : : 1- لا ، 2 نعم',
  `reconstructions_organization_name` text COLLATE utf8mb4_unicode_ci COMMENT 'اسم الجمعية او المؤسسة التي وعدت العائلة',
  `visitor` text COLLATE utf8mb4_unicode_ci COMMENT '	اسم الباحث الاجتماعي',
  `visitor_card` int(11) DEFAULT NULL COMMENT '	رقم هوية الباحث الاجتماعي',
  `visited_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	تاريخ زيارة الباحث',
  `visitor_evaluation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	تقييم الباحث لحالة المنزل : 4- ممتاز ، 3- جيد جدا ، 2 جيد ، 1 سيء ، 0 سيء جدا',
  `visitor_opinion` text COLLATE utf8mb4_unicode_ci COMMENT '	رأي الباحث الاجتماعي',
  `notes` text COLLATE utf8mb4_unicode_ci COMMENT 'توصيات الباحث الاجتماعي',
  `bank1AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `working` int(11) DEFAULT NULL COMMENT 'هل يعمل : 1 نعم ,0 لا',
  `work_job_id` text COLLATE utf8mb4_unicode_ci COMMENT ' المسمى الوظيفي ',
  `work_status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة العمل',
  `work_wage_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' الدخل حسب الفئة',
  `work_location` text COLLATE utf8mb4_unicode_ci COMMENT 'مكان العمل',
  `can_work` int(11) DEFAULT NULL COMMENT '  قدرة رب الأسرة على العمل: 1 نعم , 0 لا ',
  `work_reason_id` text COLLATE utf8mb4_unicode_ci COMMENT ' سبب عدم المقدرة',
  `abilityOfWorkReasonAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'سبب عدم القعدرة ع العمل',
  `has_other_work_resources` int(11) DEFAULT NULL COMMENT 'هل لديه مصادر دخل أخرى : 1 نعم 0 لا ',
  `property_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملكية المنزل : 0 ايجار , 1 ملك',
  `roof_material_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' سقف وجدران المنزل',
  `house_condition` text COLLATE utf8mb4_unicode_ci COMMENT 'تقييم وضع المنزل ',
  `indoor_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'وضع الأثاث :0 سيىء , 1 جيد , 2 جيد جدا ',
  `residence_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' حالة المنزل  :0 سيىء , 1 جيد , 2 جيد جدا ',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المساحة بالمتر',
  `rooms` int(11) DEFAULT NULL COMMENT 'عدد الغرف ',
  `rent_value` int(11) DEFAULT NULL COMMENT 'قيمة الايجار بالشيكل',
  `need_repair` int(11) DEFAULT NULL COMMENT '  هل بحاجة لترميم المنزل للموائمة : 0 لا , 1 نعم',
  `habitable` text COLLATE utf8mb4_unicode_ci COMMENT 'وصف وضع المنزل',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الصحية',
  `details` text COLLATE utf8mb4_unicode_ci COMMENT 'تفاصيل الحالة الصحية',
  `disease_id` text COLLATE utf8mb4_unicode_ci COMMENT 'المرض أو الاعاقة',
  `has_health_insurance` int(11) DEFAULT NULL COMMENT 'هل لديك تأمين صحي 0 لا 1 نعم',
  `health_insurance_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم التأمين الصحي',
  `health_insurance_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع التأمين الصحي',
  `has_device` int(11) DEFAULT NULL COMMENT '  هل يستخدم جهاز 0 لا 1 نعم',
  `used_device_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجهاز المستخدم',
  `medical_report_summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملخص التقارير الطبية',
  `hasCHF` tinyint(1) NOT NULL DEFAULT '0',
  `hasOXFAM` tinyint(1) NOT NULL DEFAULT '0',
  `hasWFP` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilyMartyrs` int(11) NOT NULL DEFAULT '0',
  `hasPrisoners` tinyint(1) NOT NULL DEFAULT '0',
  `hasWorldBank` tinyint(1) NOT NULL DEFAULT '0',
  `hasWounded` tinyint(1) NOT NULL DEFAULT '0',
  `hasPermanentUnemployment` tinyint(1) NOT NULL DEFAULT '0',
  `hasSocialAffairs` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilySponsorship` tinyint(1) NOT NULL DEFAULT '0',
  `hasGuaranteesOfOrphans` tinyint(1) NOT NULL DEFAULT '0',
  `hasNeighborhoodCommittees` tinyint(1) NOT NULL DEFAULT '0',
  `hasZakat` tinyint(1) NOT NULL DEFAULT '0',
  `hasAgencyAid` tinyint(1) NOT NULL DEFAULT '0',
  `hasQatarHelp` tinyint(1) NOT NULL DEFAULT '0',
  `chf` int(11) DEFAULT NULL COMMENT 'CHF(نقدية)',
  `oxform` int(11) DEFAULT NULL COMMENT 'Oxform(نقدية)',
  `wfp` int(11) DEFAULT NULL COMMENT 'WFP(نقدية)',
  `family_martyrs` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(نقدية)',
  `prisoners` int(11) DEFAULT NULL COMMENT 'الاسرى(نقدية)',
  `world_bank` int(11) DEFAULT NULL COMMENT 'البنك الدولي(نقدية)',
  `wounded` int(11) DEFAULT NULL COMMENT 'الجرحى(نقدية)',
  `permanent_unemployment` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(نقدية)',
  `social_affairs` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(نقدية)',
  `benefactor` int(11) DEFAULT NULL COMMENT 'فاعل خير(نقدية)',
  `family_sponsorship` int(11) DEFAULT NULL COMMENT 'كفالات أسر(نقدية)',
  `guarantees_of_orphans` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(نقدية)',
  `neighborhood_committees` int(11) DEFAULT NULL COMMENT 'لجان الحي(نقدية)',
  `Zakat` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(نقدية)',
  `agency_aid` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(نقدية)',
  `qatarHelp` int(11) NOT NULL DEFAULT '0',
  `CHFCount` int(11) DEFAULT NULL,
  `OXFAMCount` int(11) DEFAULT NULL,
  `WFPCount` int(11) DEFAULT NULL,
  `familyMartyrsCount` int(11) DEFAULT NULL,
  `prisonersCount` int(11) DEFAULT NULL,
  `worldBankCount` int(11) DEFAULT NULL,
  `woundedCount` int(11) DEFAULT NULL,
  `permanentUnemploymentCount` int(11) DEFAULT NULL,
  `socialAffairsCount` int(11) DEFAULT NULL,
  `familySponsorshipCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesCount` int(11) DEFAULT NULL,
  `zakatCount` int(11) DEFAULT NULL,
  `agencyAidCount` int(11) DEFAULT NULL,
  `qatarHelpCount` int(11) NOT NULL DEFAULT '0',
  `chf_sample` int(11) DEFAULT NULL COMMENT 'CHF (عينية) ',
  `oxform_sample` int(11) DEFAULT NULL COMMENT 'Oxform (عينية) ',
  `wfp_sample` int(11) DEFAULT NULL COMMENT 'WFP(عينية)',
  `family_martyrs_sample` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(عينية)',
  `prisoners_sample` int(11) DEFAULT NULL COMMENT 'الاسرى(عينية)',
  `world_bank_sample` int(11) DEFAULT NULL COMMENT 'البنك الدولي(عينية)',
  `wounded_sample` int(11) DEFAULT NULL COMMENT 'الجرحى(عينية)',
  `permanent_unemployment_sample` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(عينية)',
  `social_affairs_sample` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(عينية)',
  `benefactor_sample` int(11) DEFAULT NULL COMMENT 'فاعل خير(عينية)',
  `family_sponsorship_sample` int(11) DEFAULT NULL COMMENT 'كفالات أسر(عينية)',
  `guarantees_of_orphans_sample` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(عينية)',
  `neighborhood_committees_sample` int(11) DEFAULT NULL COMMENT 'لجان الحي(عينية)',
  `Zakat_sample` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(عينية)',
  `agency_aid_sample` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(عينية)',
  `qatarHelpSample` int(11) NOT NULL DEFAULT '0',
  `chfSampleCount` int(11) DEFAULT NULL,
  `oxformSampleCount` int(11) DEFAULT NULL,
  `wfpSampleCount` int(11) DEFAULT NULL,
  `familyMartyrsSampleCount` int(11) DEFAULT NULL,
  `prisonersSampleCount` int(11) DEFAULT NULL,
  `worldBankSampleCount` int(11) DEFAULT NULL,
  `woundedSampleCount` int(11) DEFAULT NULL,
  `permanentUnemploymentSampleCount` int(11) DEFAULT NULL,
  `socialAffairsSampleCount` bigint(20) DEFAULT NULL,
  `familySponsorshipSampleCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansSampleCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesSampleCount` int(11) DEFAULT NULL,
  `zakatSampleCount` bigint(20) DEFAULT NULL,
  `agencyAidSampleCount` bigint(20) DEFAULT NULL,
  `qatarHelpSampleCount` int(11) NOT NULL DEFAULT '0',
  `hasCar` tinyint(1) NOT NULL DEFAULT '0',
  `cars` int(11) DEFAULT NULL COMMENT 'سيارات',
  `hasCattle` tinyint(1) NOT NULL DEFAULT '0',
  `cattle` int(11) DEFAULT NULL COMMENT 'مواشي',
  `hasLands` tinyint(1) NOT NULL DEFAULT '0',
  `lands` int(11) DEFAULT NULL COMMENT 'أراضي',
  `hasProperty` tinyint(1) NOT NULL DEFAULT '0',
  `property` int(11) DEFAULT NULL COMMENT 'عقارات',
  `doors` int(11) DEFAULT NULL COMMENT 'ابواب',
  `safe_lighting` int(11) DEFAULT NULL COMMENT 'انارة امنة',
  `gas_tube` int(11) DEFAULT NULL COMMENT 'انبوبة غاز',
  `cobble` int(11) DEFAULT NULL COMMENT 'بلاط',
  `tv` int(11) DEFAULT NULL COMMENT 'تلفزيون',
  `fridge` int(11) DEFAULT NULL COMMENT 'ثلاجة',
  `solarium` int(11) DEFAULT NULL COMMENT 'حمام شمسي',
  `watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه',
  `sweet_watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه حلوة',
  `closet` int(11) DEFAULT NULL COMMENT 'خزانة ملابس',
  `heater` int(11) DEFAULT NULL COMMENT 'دفاية',
  `windows` int(11) DEFAULT NULL COMMENT 'شبابيك',
  `electricity_network` int(11) DEFAULT NULL COMMENT 'شبكة  كهرباء',
  `water_network` int(11) DEFAULT NULL COMMENT 'شبكة مياه',
  `electric_cooker` int(11) DEFAULT NULL COMMENT 'طنجرة كهرباء',
  `stuff_kitchen` int(11) DEFAULT NULL COMMENT 'عفش المطبخ',
  `gas` int(11) DEFAULT NULL COMMENT 'غاز',
  `washer` int(11) DEFAULT NULL COMMENT 'غسالة',
  `mattresses` int(11) DEFAULT NULL COMMENT 'فرشات',
  `plastic_chairs` int(11) DEFAULT NULL COMMENT 'كراسي بلاستيك/كنب',
  `fan` int(11) DEFAULT NULL COMMENT 'مروحة',
  `top_ceiling` int(11) DEFAULT NULL COMMENT 'نايلون مقوى للسقف',
  `solarPower` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'طاقة شمسية',
  `qualifiedRelationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'صلة القرابة بالمعيل',
  `wallsOfHouseAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة الاسقف اخري',
  `hasAnotherHelp` int(11) NOT NULL DEFAULT '0',
  `other1HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other2HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other1HelpValue` int(11) NOT NULL DEFAULT '0',
  `other2HelpValue` int(11) NOT NULL DEFAULT '0',
  `other1HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other2HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other1HelpCount` int(11) NOT NULL DEFAULT '0',
  `other2HelpCount` int(11) NOT NULL DEFAULT '0',
  `home_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم المنزل',
  `not_qualified_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_present` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: الحالة فعالة\r\n0: طلب تغيير  الحالة لغير فعالة على باحث وتكامل',
  `reason` int(11) DEFAULT NULL COMMENT 'سبب طلب إلغاء فعالية الحالة من باحث وتكامل',
  `search_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_update_data` timestamp NULL DEFAULT NULL,
  `case_status` enum('NEW','INCOMPLETE','COMPLETED','SENT','ACCEPTED','CONFIRMED','RETURNED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW' COMMENT 'حالة الاستمارة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `rewards_cases`
  ADD PRIMARY KEY (`operation_case_id`),
  ADD KEY `id_card_number` (`id_card_number`),
  ADD KEY `operation_case_id` (`operation_case_id`);

ALTER TABLE `rewards_cases`
  MODIFY `operation_case_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;



CREATE TABLE `rewards_cases_copy` (
  `operation_case_id` bigint(20) UNSIGNED NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `id_card_number` int(11) DEFAULT NULL COMMENT 'رقم الهوية',
  `card_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع البطاقة',
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `fullname_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم الأول',
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الأب',
  `third_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجد',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة',
  `prev_family_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة السابق',
  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط الطول',
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط العرض',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 0- أنثى',
  `marital_status_id` int(11) DEFAULT NULL COMMENT 'الحالة الاجتماعية ',
  `deserted` int(11) DEFAULT NULL COMMENT 'مهجورة في حال انثى متزوجة :1-نعم ، 0-لا',
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'مكان الميلاد - جدول ثوابت',
  `death_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `death_cause_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'سبب الوفاة - جدول ثوابت',
  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الجنسية ''المفتاح الأساسي للجدول'',',
  `family_cnt` int(11) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `spouses` int(11) DEFAULT NULL COMMENT 'عدد الزوجات',
  `male_live` int(11) DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة',
  `female_live` int(11) DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة',
  `refugee` int(11) DEFAULT NULL COMMENT 'حالة المواطنة:1- مواطن ، 2-لاجيء',
  `unrwa_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم كرت المؤن في ال كان لاجيء',
  `is_qualified` int(11) DEFAULT NULL COMMENT '	هل مؤهل:1-مؤهل ، 0-غير مؤهل',
  `qualified_card_number` int(11) DEFAULT NULL COMMENT '	رقم هوية المعييل في حال كان رب الاسرة غير مؤهل',
  `receivables` int(11) DEFAULT NULL COMMENT '	هل لديه ذمم مالية:1-نعم ، 0-لا',
  `receivables_sk_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'قيمة الذمم المالية بالشيكل',
  `monthly_income` int(11) DEFAULT NULL COMMENT 'الدخل الشهري الكلي',
  `actual_monthly_income` int(11) DEFAULT NULL COMMENT '	الدخل الشهري الفعلي',
  `has_commercial_records` int(11) DEFAULT NULL COMMENT ' هل لديه سجل تجاري :1 نعم , 0 لا',
  `active_commercial_records` int(11) DEFAULT NULL COMMENT 'عدد السجلات التجاريةالفعالة',
  `gov_commercial_records_details` text COLLATE utf8mb4_unicode_ci COMMENT 'بيان السجلات التجارية',
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم التلفون الارضي',
  `primary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الجوال الاساسي',
  `secondary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم الجوال الاحتياطي',
  `watanya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الوطنية',
  `charity_id` int(11) DEFAULT NULL COMMENT ' الجمعية',
  `country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدولة - جدول ثوابت',
  `district_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	المحافظة - جدول ثوابت',
  `region_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المنطقة - جدول ثوابت',
  `neighborhood_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	الحي - جدول ثوابت',
  `square_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المربع - جدول ثوابت',
  `mosque_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المسجد - جدول ثوابت',
  `street_address_details` text COLLATE utf8mb4_unicode_ci COMMENT '	تفاصيل العنوان - الشارع',
  `street_address` text COLLATE utf8mb4_unicode_ci COMMENT '	 العنوان - الشارع',
  `needs` text COLLATE utf8mb4_unicode_ci COMMENT 'الاحتياجات',
  `promised` int(11) DEFAULT NULL COMMENT 'هل وعدت بالبناء : : 1- لا ، 2 نعم',
  `reconstructions_organization_name` text COLLATE utf8mb4_unicode_ci COMMENT 'اسم الجمعية او المؤسسة التي وعدت العائلة',
  `visitor` text COLLATE utf8mb4_unicode_ci COMMENT '	اسم الباحث الاجتماعي',
  `visitor_card` int(11) DEFAULT NULL COMMENT '	رقم هوية الباحث الاجتماعي',
  `visited_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	تاريخ زيارة الباحث',
  `visitor_evaluation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	تقييم الباحث لحالة المنزل : 4- ممتاز ، 3- جيد جدا ، 2 جيد ، 1 سيء ، 0 سيء جدا',
  `visitor_opinion` text COLLATE utf8mb4_unicode_ci COMMENT '	رأي الباحث الاجتماعي',
  `notes` text COLLATE utf8mb4_unicode_ci COMMENT 'توصيات الباحث الاجتماعي',
  `bank1AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `working` int(11) DEFAULT NULL COMMENT 'هل يعمل : 1 نعم ,0 لا',
  `work_job_id` text COLLATE utf8mb4_unicode_ci COMMENT ' المسمى الوظيفي ',
  `work_status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة العمل',
  `work_wage_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' الدخل حسب الفئة',
  `work_location` text COLLATE utf8mb4_unicode_ci COMMENT 'مكان العمل',
  `can_work` int(11) DEFAULT NULL COMMENT '  قدرة رب الأسرة على العمل: 1 نعم , 0 لا ',
  `work_reason_id` text COLLATE utf8mb4_unicode_ci COMMENT ' سبب عدم المقدرة',
  `abilityOfWorkReasonAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'سبب عدم القعدرة ع العمل',
  `has_other_work_resources` int(11) DEFAULT NULL COMMENT 'هل لديه مصادر دخل أخرى : 1 نعم 0 لا ',
  `property_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملكية المنزل : 0 ايجار , 1 ملك',
  `roof_material_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' سقف وجدران المنزل',
  `house_condition` text COLLATE utf8mb4_unicode_ci COMMENT 'تقييم وضع المنزل ',
  `indoor_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'وضع الأثاث :0 سيىء , 1 جيد , 2 جيد جدا ',
  `residence_condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' حالة المنزل  :0 سيىء , 1 جيد , 2 جيد جدا ',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المساحة بالمتر',
  `rooms` int(11) DEFAULT NULL COMMENT 'عدد الغرف ',
  `rent_value` int(11) DEFAULT NULL COMMENT 'قيمة الايجار بالشيكل',
  `need_repair` int(11) DEFAULT NULL COMMENT '  هل بحاجة لترميم المنزل للموائمة : 0 لا , 1 نعم',
  `habitable` text COLLATE utf8mb4_unicode_ci COMMENT 'وصف وضع المنزل',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الصحية',
  `details` text COLLATE utf8mb4_unicode_ci COMMENT 'تفاصيل الحالة الصحية',
  `disease_id` text COLLATE utf8mb4_unicode_ci COMMENT 'المرض أو الاعاقة',
  `has_health_insurance` int(11) DEFAULT NULL COMMENT 'هل لديك تأمين صحي 0 لا 1 نعم',
  `health_insurance_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم التأمين الصحي',
  `health_insurance_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع التأمين الصحي',
  `has_device` int(11) DEFAULT NULL COMMENT '  هل يستخدم جهاز 0 لا 1 نعم',
  `used_device_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجهاز المستخدم',
  `medical_report_summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملخص التقارير الطبية',
  `hasCHF` tinyint(1) NOT NULL DEFAULT '0',
  `hasOXFAM` tinyint(1) NOT NULL DEFAULT '0',
  `hasWFP` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilyMartyrs` int(11) NOT NULL DEFAULT '0',
  `hasPrisoners` tinyint(1) NOT NULL DEFAULT '0',
  `hasWorldBank` tinyint(1) NOT NULL DEFAULT '0',
  `hasWounded` tinyint(1) NOT NULL DEFAULT '0',
  `hasPermanentUnemployment` tinyint(1) NOT NULL DEFAULT '0',
  `hasSocialAffairs` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilySponsorship` tinyint(1) NOT NULL DEFAULT '0',
  `hasGuaranteesOfOrphans` tinyint(1) NOT NULL DEFAULT '0',
  `hasNeighborhoodCommittees` tinyint(1) NOT NULL DEFAULT '0',
  `hasZakat` tinyint(1) NOT NULL DEFAULT '0',
  `hasAgencyAid` tinyint(1) NOT NULL DEFAULT '0',
  `hasQatarHelp` tinyint(1) NOT NULL DEFAULT '0',
  `chf` int(11) DEFAULT NULL COMMENT 'CHF(نقدية)',
  `oxform` int(11) DEFAULT NULL COMMENT 'Oxform(نقدية)',
  `wfp` int(11) DEFAULT NULL COMMENT 'WFP(نقدية)',
  `family_martyrs` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(نقدية)',
  `prisoners` int(11) DEFAULT NULL COMMENT 'الاسرى(نقدية)',
  `world_bank` int(11) DEFAULT NULL COMMENT 'البنك الدولي(نقدية)',
  `wounded` int(11) DEFAULT NULL COMMENT 'الجرحى(نقدية)',
  `permanent_unemployment` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(نقدية)',
  `social_affairs` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(نقدية)',
  `benefactor` int(11) DEFAULT NULL COMMENT 'فاعل خير(نقدية)',
  `family_sponsorship` int(11) DEFAULT NULL COMMENT 'كفالات أسر(نقدية)',
  `guarantees_of_orphans` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(نقدية)',
  `neighborhood_committees` int(11) DEFAULT NULL COMMENT 'لجان الحي(نقدية)',
  `Zakat` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(نقدية)',
  `agency_aid` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(نقدية)',
  `qatarHelp` int(11) NOT NULL DEFAULT '0',
  `CHFCount` int(11) DEFAULT NULL,
  `OXFAMCount` int(11) DEFAULT NULL,
  `WFPCount` int(11) DEFAULT NULL,
  `familyMartyrsCount` int(11) DEFAULT NULL,
  `prisonersCount` int(11) DEFAULT NULL,
  `worldBankCount` int(11) DEFAULT NULL,
  `woundedCount` int(11) DEFAULT NULL,
  `permanentUnemploymentCount` int(11) DEFAULT NULL,
  `socialAffairsCount` int(11) DEFAULT NULL,
  `familySponsorshipCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesCount` int(11) DEFAULT NULL,
  `zakatCount` int(11) DEFAULT NULL,
  `agencyAidCount` int(11) DEFAULT NULL,
  `qatarHelpCount` int(11) NOT NULL DEFAULT '0',
  `chf_sample` int(11) DEFAULT NULL COMMENT 'CHF (عينية) ',
  `oxform_sample` int(11) DEFAULT NULL COMMENT 'Oxform (عينية) ',
  `wfp_sample` int(11) DEFAULT NULL COMMENT 'WFP(عينية)',
  `family_martyrs_sample` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(عينية)',
  `prisoners_sample` int(11) DEFAULT NULL COMMENT 'الاسرى(عينية)',
  `world_bank_sample` int(11) DEFAULT NULL COMMENT 'البنك الدولي(عينية)',
  `wounded_sample` int(11) DEFAULT NULL COMMENT 'الجرحى(عينية)',
  `permanent_unemployment_sample` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(عينية)',
  `social_affairs_sample` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(عينية)',
  `benefactor_sample` int(11) DEFAULT NULL COMMENT 'فاعل خير(عينية)',
  `family_sponsorship_sample` int(11) DEFAULT NULL COMMENT 'كفالات أسر(عينية)',
  `guarantees_of_orphans_sample` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(عينية)',
  `neighborhood_committees_sample` int(11) DEFAULT NULL COMMENT 'لجان الحي(عينية)',
  `Zakat_sample` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(عينية)',
  `agency_aid_sample` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(عينية)',
  `qatarHelpSample` int(11) NOT NULL DEFAULT '0',
  `chfSampleCount` int(11) DEFAULT NULL,
  `oxformSampleCount` int(11) DEFAULT NULL,
  `wfpSampleCount` int(11) DEFAULT NULL,
  `familyMartyrsSampleCount` int(11) DEFAULT NULL,
  `prisonersSampleCount` int(11) DEFAULT NULL,
  `worldBankSampleCount` int(11) DEFAULT NULL,
  `woundedSampleCount` int(11) DEFAULT NULL,
  `permanentUnemploymentSampleCount` int(11) DEFAULT NULL,
  `socialAffairsSampleCount` bigint(20) DEFAULT NULL,
  `familySponsorshipSampleCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansSampleCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesSampleCount` int(11) DEFAULT NULL,
  `zakatSampleCount` bigint(20) DEFAULT NULL,
  `agencyAidSampleCount` bigint(20) DEFAULT NULL,
  `qatarHelpSampleCount` int(11) NOT NULL DEFAULT '0',
  `hasCar` tinyint(1) NOT NULL DEFAULT '0',
  `cars` int(11) DEFAULT NULL COMMENT 'سيارات',
  `hasCattle` tinyint(1) NOT NULL DEFAULT '0',
  `cattle` int(11) DEFAULT NULL COMMENT 'مواشي',
  `hasLands` tinyint(1) NOT NULL DEFAULT '0',
  `lands` int(11) DEFAULT NULL COMMENT 'أراضي',
  `hasProperty` tinyint(1) NOT NULL DEFAULT '0',
  `property` int(11) DEFAULT NULL COMMENT 'عقارات',
  `doors` int(11) DEFAULT NULL COMMENT 'ابواب',
  `safe_lighting` int(11) DEFAULT NULL COMMENT 'انارة امنة',
  `gas_tube` int(11) DEFAULT NULL COMMENT 'انبوبة غاز',
  `cobble` int(11) DEFAULT NULL COMMENT 'بلاط',
  `tv` int(11) DEFAULT NULL COMMENT 'تلفزيون',
  `fridge` int(11) DEFAULT NULL COMMENT 'ثلاجة',
  `solarium` int(11) DEFAULT NULL COMMENT 'حمام شمسي',
  `watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه',
  `sweet_watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه حلوة',
  `closet` int(11) DEFAULT NULL COMMENT 'خزانة ملابس',
  `heater` int(11) DEFAULT NULL COMMENT 'دفاية',
  `windows` int(11) DEFAULT NULL COMMENT 'شبابيك',
  `electricity_network` int(11) DEFAULT NULL COMMENT 'شبكة  كهرباء',
  `water_network` int(11) DEFAULT NULL COMMENT 'شبكة مياه',
  `electric_cooker` int(11) DEFAULT NULL COMMENT 'طنجرة كهرباء',
  `stuff_kitchen` int(11) DEFAULT NULL COMMENT 'عفش المطبخ',
  `gas` int(11) DEFAULT NULL COMMENT 'غاز',
  `washer` int(11) DEFAULT NULL COMMENT 'غسالة',
  `mattresses` int(11) DEFAULT NULL COMMENT 'فرشات',
  `plastic_chairs` int(11) DEFAULT NULL COMMENT 'كراسي بلاستيك/كنب',
  `fan` int(11) DEFAULT NULL COMMENT 'مروحة',
  `top_ceiling` int(11) DEFAULT NULL COMMENT 'نايلون مقوى للسقف',
  `solarPower` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'طاقة شمسية',
  `qualifiedRelationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'صلة القرابة بالمعيل',
  `wallsOfHouseAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة الاسقف اخري',
  `hasAnotherHelp` int(11) NOT NULL DEFAULT '0',
  `other1HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other2HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other1HelpValue` int(11) NOT NULL DEFAULT '0',
  `other2HelpValue` int(11) NOT NULL DEFAULT '0',
  `other1HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other2HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other1HelpCount` int(11) NOT NULL DEFAULT '0',
  `other2HelpCount` int(11) NOT NULL DEFAULT '0',
  `home_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم المنزل',
  `not_qualified_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_present` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: الحالة فعالة\r\n0: طلب تغيير  الحالة لغير فعالة على باحث وتكامل',
  `reason` int(11) DEFAULT NULL COMMENT 'سبب طلب إلغاء فعالية الحالة من باحث وتكامل',
  `search_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_update_data` timestamp NULL DEFAULT NULL,
  `case_status` enum('NEW','INCOMPLETE','COMPLETED','SENT','ACCEPTED','CONFIRMED','RETURNED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW' COMMENT 'حالة الاستمارة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `rewards_cases_copy`
  ADD PRIMARY KEY (`operation_case_id`),
  ADD KEY `id_card_number` (`id_card_number`),
  ADD KEY `operation_case_id` (`operation_case_id`);

ALTER TABLE `rewards_cases_copy`
  MODIFY `operation_case_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

-- -------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------


CREATE TABLE `rewards_cases_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_card_number` int(11) DEFAULT NULL COMMENT 'رقم الهوية',
  `family_id` int(11) DEFAULT NULL,
  `operation_case_id` int(11) DEFAULT NULL,
  `new_operation_case_id` int(11) DEFAULT NULL,
  `father_id_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم ',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم الأول',
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الأب',
  `third_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجد',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 0- أنثى',
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'مكان الميلاد - جدول ثوابت',
  `marital_status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الاجتماعية ',
  `kinship_id` text COLLATE utf8mb4_unicode_ci COMMENT 'صلة القرابة ( رب الأسرة ) ',
  `primary_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_income` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' الدخل الشهري بالشيكل',
  `study` tinyint(1) DEFAULT NULL COMMENT 'يدرس',
  `study_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع الدراسة',
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المؤسسة التعليمية',
  `degree` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدرجة العلمية',
  `stage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المرحلة الدراسية',
  `grade` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الصف',
  `working` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'هل يعمل ',
  `work_job_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' المسمى الوظيفي ',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الصحية',
  `details` text COLLATE utf8mb4_unicode_ci COMMENT 'تفاصيل الحالة الصحية',
  `disease_id` text COLLATE utf8mb4_unicode_ci COMMENT 'المرض أو الاعاقة',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prayer` tinyint(1) DEFAULT NULL,
  `prayer_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quran_center` int(11) DEFAULT NULL,
  `quran_parts` int(11) DEFAULT NULL,
  `quran_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `rewards_cases_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `father_id_card` (`father_id_card`),
  ADD KEY `id_card_number` (`id_card_number`);

ALTER TABLE `rewards_cases_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

-- -------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------

ALTER TABLE `char_social_search_cases_family` ADD `birth_place` varchar(45) DEFAULT NULL COMMENT 'مكان الميلاد' AFTER `birthday`;
ALTER TABLE `char_social_search_cases_family` ADD `study` TINYINT(1) NULL DEFAULT NULL AFTER `details` ;
ALTER TABLE `char_social_search_cases_family` ADD `type` TINYINT(4) NULL DEFAULT NULL AFTER `study`;
ALTER TABLE `char_social_search_cases_family` ADD `authority` INT(10) NULL DEFAULT NULL AFTER `type`;
ALTER TABLE `char_social_search_cases_family` ADD `stage` INT(10) NULL DEFAULT NULL AFTER `authority`;
ALTER TABLE `char_social_search_cases_family` ADD `degree` INT(10) NULL DEFAULT NULL AFTER `stage`;

ALTER TABLE `char_social_search_cases_family` ADD `prayer` tinyint(3) UNSIGNED DEFAULT NULL COMMENT  'الصلاة: 1 يواظب، 2 لا يصلي، 3 متقطع'AFTER `school`;
ALTER TABLE `char_social_search_cases_family` ADD `quran_center` tinyint(3) UNSIGNED DEFAULT NULL COMMENT  'هل ملتحق بمركز تحفيظ' AFTER `prayer`;
ALTER TABLE `char_social_search_cases_family` ADD `quran_parts` varchar(255) DEFAULT NULL COMMENT  'حفظ القرآن: عدد الأجزاء' AFTER `quran_center`;
ALTER TABLE `char_social_search_cases_family` ADD `quran_chapters` varchar(255) DEFAULT NULL COMMENT 'حفظ القرآن: عدد السور'  AFTER `quran_parts`;
ALTER TABLE `char_social_search_cases_family` ADD `prayer_reason` text COMMENT 'سبب ترك الصلاة' AFTER `quran_chapters`;
ALTER TABLE `char_social_search_cases_family` ADD `quran_reason` text COMMENT 'سبب ترك حفظ القرآن' AFTER `prayer_reason`;

ALTER TABLE `char_social_search_cases_family` ADD `working` TINYINT(1) NULL DEFAULT NULL AFTER `quran_reason`;
ALTER TABLE `char_social_search_cases_family` ADD `work_job_id` INT(10) NULL DEFAULT NULL AFTER `working`;

-- ----------------------  25/3/2023 -----------------------

ALTER TABLE `char_vouchers` ADD `organization_project_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `project_id`;
ALTER TABLE `char_vouchers` ADD KEY `fk_vouchers_organization_project_id_idx` (`organization_project_id`);
ALTER TABLE `char_vouchers` ADD CONSTRAINT `fk_vouchers_organization_project_id_fk` FOREIGN KEY (`organization_project_id`) REFERENCES `char_organization_projects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `char_organization_projects` ADD `voucher_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `organization_id`;
ALTER TABLE `char_organization_projects` ADD KEY `fk_organization_projects_voucher_id_idx` (`voucher_id`);
ALTER TABLE `char_organization_projects` ADD CONSTRAINT `fk_organization_projects_voucher_id_fk` FOREIGN KEY (`voucher_id`) REFERENCES `char_vouchers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `char_users_card_log` DROP FOREIGN KEY `fk_user_card_log_user_id`;

-- -------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_users_online_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_users_online_view`  AS
SELECT oauth_access_tokens.user_id ,CONCAT(char_users.firstname,' ',char_users.lastname)as user_name ,
oauth_access_tokens.created_at as login_dt,
char_users.`username`,char_users.`id_card_number`, char_users.`locale`,
char_users.`type`,char_users.`connect_with_sponsor`,char_users.`password`,
char_users.`email`,char_users.`nick_name`,
char_users.`position`, char_users.`document_id`,
char_users.`firstname`,char_users.`lastname`,
char_users.`job_title`, char_users.`mobile`,
char_users.`wataniya`,char_users.`organization_id`,
char_users.`parent_id`,char_users.`super_admin`,char_users.`status`, char_users.`os_deleted`,
char_users.`remember_token`,
char_users.`created_at`, char_users.`updated_at`, char_users.`deleted_at`,
char_organizations.name as org_ar_name , char_organizations.en_name as org_en_name , char_organizations.type as org_type ,
char_organizations.level as org_level ,
char_organizations.parent_id as org_parent_id,
char_organizations.category_id as org_category_id
FROM `oauth_access_tokens`
join char_users  on char_users.id = oauth_access_tokens.user_id
join char_organizations  on char_organizations.id = char_users.organization_id
WHERE revoked = 0
ORDER BY `oauth_access_tokens`.`created_at`  DESC

-- -----------------------------------------------------
--  -----------------------------------------------------

ALTER TABLE `app_logs` ADD `user_agent` varchar(255) DEFAULT NULL COMMENT 'المتصفح' AFTER `ip`;
ALTER TABLE `app_logs` ADD `url` text DEFAULT NULL COMMENT 'الرابط الرئيسي للحركة' AFTER `ip`;

ALTER TABLE `char_settings` CHANGE `value` `value` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'القيمة';
ALTER TABLE `char_settings` CHANGE `value` `value` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT 'القيمة';

ALTER TABLE `char_settings` CHANGE `value` `value` LONGTEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'القيمة';

INSERT INTO `char_settings` (`id`, `value`, `organization_id`) VALUES
 ('gov_service_client_id', 'MAIN_MOASSASAT', '0'),
 ('gov_service_client_secret', '_78235884d334d0b798a35f7b65ff906698f0a60069', '0') ,
 ('gov_service_client_authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZiYTNhOGZiYmEwYjlmYWFkMzYwNGFhMjEzOGM0MzUzMTBkZGVmM2Q2MDIzMmI2MjFhMmMwOWE3YzYxNGU3MjRjNGExMTJjMjVjMWZiZDI2In0.eyJhdWQiOiJNQUlOX01PQVNTQVNBVCIsImp0aSI6IjZiYTNhOGZiYmEwYjlmYWFkMzYwNGFhMjEzOGM0MzUzMTBkZGVmM2Q2MDIzMmI2MjFhMmMwOWE3YzYxNGU3MjRjNGExMTJjMjVjMWZiZDI2IiwiaWF0IjoxNjg5NzcxMDYxLCJuYmYiOjE2ODk3NzEwNjEsImV4cCI6MTY5MjQ0OTQ2MSwic3ViIjoiIiwic2NvcGVzIjpbXX0.NG0W5bax7GAljnHe8uWDqryhvCUrm2WEaHEELxfvs_grihcf8-nMbEw2QSwkBak7ZZodgutZNR5TCWGEnIun5uLT5qGJT6BCtN93fqpf_BmS4543FBcyi80KsTo4HV_ghAwxbco1lWmzglxv_10_DBfea9RXa-76V-uMVvGcn5U', '0');


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`)
 VALUES (537, '10','reports.case.socialAffairsReceipt', 'الاستعلام في استفادات الشئون الاجتماعية');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 537, 1, CURRENT_TIMESTAMP, 1),
(2, 537, 1, CURRENT_TIMESTAMP, 1);
-- ------------------------------------------------------------------------------------------
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`)
 VALUES
 (538, '10','reports.case.MinistryOfJustice', 'بيانات وزارة العدل'),
 (539, '10','reports.case.MinistryOfHealth', 'بيانات وزارة الصحة'),
 (540, '10','reports.case.MinistryOfTransportation', 'بيانات وزارة النقل والمواصللات'),
 (541, '10','reports.case.MinistryOfCommunications', 'بيانات وزارة تكنولوجيا المعلومات والاتصالات'),
 (542, '10','reports.case.MinistryOfFinance', 'بيانات وزارة المالية'),
 (543, '10','reports.case.MinistryOfEconomy', 'بيانات وزارة الاقتصاد'),
 (544, '10','reports.case.LandAuthority', 'بيانات سلطة الأراضي'),
 (545, '10','reports.case.MinistryOfLabor', 'بيانات وزارة العمل'),
 (546, '10','reports.case.MinistryOfSocialDevelopment', 'بيانات وزارة التنمية الاجتماعية');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 538, 1, CURRENT_TIMESTAMP, 1),
(2, 538, 1, CURRENT_TIMESTAMP, 1),

(1, 539, 1, CURRENT_TIMESTAMP, 1),
(2, 539, 1, CURRENT_TIMESTAMP, 1),

(1, 540, 1, CURRENT_TIMESTAMP, 1),
(2, 540, 1, CURRENT_TIMESTAMP, 1),

(1, 541, 1, CURRENT_TIMESTAMP, 1),
(2, 541, 1, CURRENT_TIMESTAMP, 1),

(1, 542, 1, CURRENT_TIMESTAMP, 1),
(2, 542, 1, CURRENT_TIMESTAMP, 1),

(1, 543, 1, CURRENT_TIMESTAMP, 1),
(2, 543, 1, CURRENT_TIMESTAMP, 1),

(1, 544, 1, CURRENT_TIMESTAMP, 1),
(2, 544, 1, CURRENT_TIMESTAMP, 1),

(1, 545, 1, CURRENT_TIMESTAMP, 1),
(2, 545, 1, CURRENT_TIMESTAMP, 1),

(1, 546, 1, CURRENT_TIMESTAMP, 1),
(2, 546, 1, CURRENT_TIMESTAMP, 1);


INSERT INTO `char_settings` (`id`, `value`, `organization_id`) VALUES
 ('gov_service_expires_in', '2023-10-01 19:08:32', '0');

