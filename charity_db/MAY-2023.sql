
-- --------------------------------------------  start at 3- 5-2023 -----------------------------------------------------

ALTER TABLE `char_users` ADD `no_sessions_at_time` INT(10) UNSIGNED NULL DEFAULT 1  COMMENT 'عدد الجلسات المتاحة في آن واحد' AFTER `status`;
ALTER TABLE `char_persons` ADD `old_id_card_number` varchar(9) NULL COMMENT 'رقم بطاقة التعريف القديم' AFTER `id_card_number`;

ALTER TABLE `char_persons` ADD `old_id_card_number`  varchar(9) NULL COMMENT 'رقم بطاقة التعريف القديم' ;
-- ALTER TABLE `char_persons` CHANGE `old_id_card_number` `old_id_card_number`  varchar(9) NULL DEFAULT NULL COMMENT 'رقم بطاقة التعريف القديم' ;

UPDATE `char_persons`
SET  `old_id_card_number` = `id_card_number`
WHERE id_card_number LIKE '700%' ;
-- -------------------------------------------------------------------------------------------------------------
-- add deleted_at to vouchers table and edit all view and functions and procedures the be get vouchers to except deleted

ALTER TABLE `char_vouchers` ADD `deleted_at` datetime NULL DEFAULT NULL COMMENT 'تاريخ ووقت الحذف' AFTER `updated_at`;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(535, 3, 'aid.voucher.trashed', 'إدارة قسائم المساعدات المحذوفة'),
(536, 3, 'aid.voucher.restore', 'استعادة قسائم المساعدات المحذوفة');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 535, 1, CURRENT_TIMESTAMP, 1),
(1, 536, 1, CURRENT_TIMESTAMP, 1);
-- -------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_vouchers_persons_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view`  AS SELECT `char_vouchers_persons`.`id` AS `id`,`char_vouchers_persons`.`document_id` AS `document_id`, `char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`, `v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`,`v`.`organization_project_id` AS `organization_project_id`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'من مشروع' else 'يدوي' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'نقدية' when (`v`.`type` = 2) then 'عينية' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'عادية' when (`v`.`urgent` = 1) then 'عاجلة' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'مباشر' when (`v`.`transfer` = 1) then 'تصنيف جمعية' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'ذكر ' when (`c`.`gender` = 2) then 'أنثى' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'ذكر ' when (`ind`.`gender` = 2) then 'أنثى' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 0) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 2) then ' لم يتم الاستلام' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 1)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 1)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 1)))) ;

DROP VIEW IF EXISTS `char_vouchers_persons_view_en`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view_en`  AS SELECT `char_vouchers_persons`.`id` AS `id`, `char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`,  `v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`,`v`.`organization_project_id` AS `organization_project_id`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'from_project' else 'manually' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'financial' when (`v`.`type` = 2) then 'non_financial' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'not_urgent' when (`v`.`urgent` = 1) then 'urgent' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'direct' when (`v`.`transfer` = 1) then 'transfer_company' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'male' when (`c`.`gender` = 2) then 'female' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'male' when (`ind`.`gender` = 2) then 'female' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'receipt' when (`char_vouchers_persons`.`status` = 0) then 'receipt' when (`char_vouchers_persons`.`status` = 2) then 'not_receipt' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 2)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 2)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 2)))) ;

-- -------------------------------------------------------------------------------------------------------------
-- --------------------------------- add deleted at

DROP FUNCTION IF EXISTS `char_get_sponsor_voucher_person_amount`;
DELIMITER $$
CREATE  FUNCTION `char_get_sponsor_voucher_person_amount`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب قيمة القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;
-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `char_get_sponsor_voucher_person_count`;
DELIMITER $$
CREATE  FUNCTION `char_get_sponsor_voucher_person_count`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `char_get_voucher_persons_count`;
DELIMITER $$
CREATE  FUNCTION `char_get_voucher_persons_count`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة'
BEGIN
	DECLARE c INTEGER;
 if p_date_from is null and p_date_to is null then
		set c = (
              SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`  and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif p_date_from is not null and p_date_to is null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`  and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif p_date_from is null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif p_date_from is not null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and `char_vouchers`.`deleted_at` is null
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `get_voucher_count_`;
DELIMITER $$
CREATE  FUNCTION `get_voucher_count_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(45)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
if type = 'sponsors' then
if date_from is null and date_to is null then
	set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
				FROM  `char_vouchers` AS `v`
				join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
				WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
else
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
end if;
else
if type = 'sponsors' then
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))and (`v`.`category_id` = category_id)));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id)  and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
else
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`category_id` = category_id) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`deleted_at` is null and `v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
end if;
end if;
RETURN c;
END$$
DELIMITER ;

-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `get_voucher_count`;
DELIMITER $$
CREATE  FUNCTION `get_voucher_count`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
 if type = 'sponsors' then
     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id)));
     elseif date_from is not null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from )));
     elseif date_from is null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`voucher_date`  <= date_to )));
     elseif date_from is not null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
else
    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id)));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
     end if;
else
    if type = 'sponsors' then
     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id) and (`v`.`category_id` = category_id)));
     elseif date_from is not null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
     elseif date_from is null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
     elseif date_from is not null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`deleted_at` is null and `v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
else
    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id) and (`v`.`category_id` = category_id) ));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE (( `v`.`deleted_at` is null and  `v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
     end if;
end if;
RETURN c;
END$$
DELIMITER ;

-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `get_organization_vouchers_value_`;
DELIMITER $$
CREATE  FUNCTION `get_organization_vouchers_value_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
		if type = 'sponsors' then
				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where (( `char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id ) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                       (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null,orgs) != 0)));
				elseif date_from is not null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where (( `char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date` >= date_from )
				      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null,orgs)  != 0)));
				elseif date_from is null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where (( `char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null,orgs)  != 0)));
				elseif date_from is not null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where (( `char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null,orgs) != 0)));
				end if;
		else
				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));
				elseif date_from is not null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
				and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));
				elseif date_from is null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));
				elseif date_from is not null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));
				end if;
		end if;
else
if type = 'sponsors' then
		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
              (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id,orgs) != 0)));
		elseif date_from is not null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
              (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id,orgs)  != 0)));
		elseif date_from is null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id,orgs)  != 0)));
		elseif date_from is not null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id,orgs) != 0)));
		end if;
else
		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));
		elseif date_from is not null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id)
		and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));
		elseif date_from is null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));
		elseif date_from is not null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));
		end if;
end if;
end if;
RETURN c;
END$$
DELIMITER ;

-- -------------------------------------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS `get_organization_vouchers_value`;
DELIMITER $$
CREATE  FUNCTION `get_organization_vouchers_value`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null) != 0)));
               end if;
else
   if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id ) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id )  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
               end if;
   end if;
else
    if type = 'sponsors' then
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` = id ) and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` = id ) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` = id ) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`sponsor_id` = id )  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id) != 0)));
               end if;
else
  if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id )  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id )  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id )  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`deleted_at` is null and `char_vouchers`.`organization_id` = id )  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
               end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------- add old_id_card_number

DROP VIEW IF EXISTS `char_aid_cases_incomplete_data`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_aid_cases_incomplete_data`  AS  select distinct `c`.`id` AS `id`,`c`.`person_id` AS `person_id`,`c`.`organization_id` AS `organization_id`,`org`.`name` AS `organization_name`,`c`.`category_id` AS `category_id`,`cat`.`name` AS `category_name`,`c`.`notes` AS `notes`,`c`.`visited_at` AS `visited_at`,`c`.`visitor` AS `visitor`,`c`.`status` AS `status`,`c`.`rank` AS `rank`,`c`.`user_id` AS `user_id`,`c`.`created_at` AS `case_created_at`,`c`.`updated_at` AS `case_updated_at`,`c`.`deleted_at` AS `case_deleted_at`,`p`.`first_name` AS `first_name`,`p`.`second_name` AS `second_name`,`p`.`third_name` AS `third_name`,`p`.`last_name` AS `last_name`,
`p`.`id_card_number` AS `id_card_number`,`p`.`old_id_card_number` AS `old_id_card_number`,`p`.`gender` AS `gender`,`p`.`marital_status_id` AS `marital_status_id`,`p`.`birthday` AS `birthday`,`p`.`birth_place` AS `birth_place`,`p`.`nationality` AS `nationality`,`p`.`death_date` AS `death_date`,`p`.`death_cause_id` AS `death_cause_id`,`p`.`father_id` AS `father_id`,`p`.`mother_id` AS `mother_id`,`p`.`location_id` AS `location_id`,`p`.`street_address` AS `street_address`,`p`.`refugee` AS `refugee`,
`p`.`unrwa_card_number` AS `unrwa_card_number`,`p`.`monthly_income` AS `monthly_income`,`p`.`spouses` AS `spouses`,`p`.`created_at` AS `person_created_at`,`p`.`updated_at` AS `person_updated_at`,`p`.`deleted_at` AS `person_deleted_at`,concat(`p`.`first_name`,' ',`p`.`second_name`,' ',`p`.`third_name`,' ',`p`.`last_name`) AS `name` from (((((((((`char_cases` `c` join `char_persons` `p` on((`c`.`person_id` = `p`.`id`))) join `char_organizations` `org` on((`org`.`id` = `c`.`organization_id`))) join `char_categories` `cat` on(((`c`.`category_id` = `cat`.`id`) and (`cat`.`type` = 2)))) left join `char_persons_i18n` `pi18n` on(((`p`.`id` = `pi18n`.`person_id`) and (`pi18n`.`language_id` = 2)))) left join `char_persons_contact` `pc` on((`p`.`id` = `pc`.`person_id`))) left join `char_persons_work` `pw` on((`p`.`id` = `pw`.`person_id`))) left join `char_residence` `r` on((`p`.`id` = `r`.`person_id`))) left join `char_reconstructions` `rc` on((`c`.`id` = `rc`.`case_id`))) left join `char_case_needs` `cn` on((`c`.`id` = `cn`.`case_id`))) where ((isnull(`p`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`p`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`p`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`p`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4))
or (isnull(`p`.`id_card_number`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'id_card_number') = 4)) or (isnull(`p`.`gender`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'gender') = 4)) or (isnull(`p`.`marital_status_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'marital_status_id') = 4)) or (isnull(`p`.`birthday`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birthday') = 4)) or (isnull(`p`.`birth_place`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birth_place') = 4)) or (isnull(`p`.`nationality`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'nationality') = 4)) or (isnull(`p`.`street_address`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'street_address') = 4)) or (isnull(`p`.`refugee`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'refugee') = 4)) or (isnull(`p`.`unrwa_card_number`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'unrwa_card_number') = 4)) or (isnull(`p`.`monthly_income`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'monthly_income') = 4)) or (isnull(`pc`.`contact_value`) and (`pc`.`contact_type` = 'phone') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'phone') = 4)) or (isnull(`pc`.`contact_value`) and (`pc`.`contact_type` = 'primary_mobile') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(1,'primary_mobile') = 4)) or (isnull(`pc`.`contact_value`) and (`pc`.`contact_type` = 'secondery_mobile') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'secondery_mobile') = 4)) or (isnull(`pw`.`working`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'working') = 4)) or (isnull(`pw`.`can_work`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'can_work') = 4)) or (isnull(`pw`.`work_status_id`) and (`pw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_status_id') = 4)) or (isnull(`pw`.`work_job_id`) and (`pw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_job_id') = 4)) or (isnull(`pw`.`work_wage_id`) and (`pw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_wage_id') = 4)) or (isnull(`pw`.`work_location`) and (`pw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_location') = 4)) or (isnull(`pw`.`work_reason_id`) and (`pw`.`can_work` = 2) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_reason_id') = 4)) or (isnull(`r`.`property_type_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'property_type_id') = 4)) or (isnull(`r`.`roof_material_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'roof_material_id') = 4)) or (isnull(`r`.`residence_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'residence_condition') = 4)) or (isnull(`r`.`indoor_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'indoor_condition') = 4)) or (isnull(`r`.`habitable`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'habitable') = 4)) or (isnull(`r`.`house_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'house_condition') = 4)) or (isnull(`r`.`rooms`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'rooms') = 4)) or (isnull(`r`.`area`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'area') = 4)) or (isnull(`c`.`notes`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'notes') = 4)) or (isnull(`c`.`visitor`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'visitor') = 4)) or (isnull(`c`.`visited_at`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'visited_at') = 4)) or (isnull(`rc`.`promised`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'reconstructions_promised') = 4)) or (isnull(`rc`.`organization_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'reconstructions_organization_name') = 4)) or (isnull(`cn`.`needs`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'case_needs') = 4)) or ((0 = (select count(0) from `char_persons_properties` where (`char_persons_properties`.`person_id` = `p`.`id`))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'essential_condition') = 4)) or ((0 = (select count(0) from `char_cases_essentials` where (`char_cases_essentials`.`case_id` = `c`.`id`))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'properties_condition') = 4)) or ((0 = (select count(0) from `char_persons_banks` where (`char_persons_banks`.`person_id` = `p`.`id`))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'banks') = 4)) or ((0 = (select count(0) from `char_persons_aids` where ((`char_persons_aids`.`person_id` = `p`.`id`) and (`char_persons_aids`.`aid_type` = 1)))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'financial_aid_condition') = 4)) or ((0 = (select count(0) from `char_persons_aids` where ((`char_persons_aids`.`person_id` = `p`.`id`) and (`char_persons_aids`.`aid_type` = 2)))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'non_financial_aid_condition') = 4))) ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_relays_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_relays_view`  AS  select `char_categories`.`type` AS `category_type`,concat(ifnull(`char_users`.`firstname`,' '),' ',ifnull(`char_users`.`lastname`,' ')) AS `user_name`,
`char_persons`.`id_card_number` AS `id_card_number`,`char_persons`.`old_id_card_number` AS `old_id_card_number`,
concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',
ifnull(`char_persons`.`third_name`,' '),' ',ifnull(`char_persons`.`last_name`,' ')) AS `full_name`,
ifnull(`mobno`.`contact_value`,'-') AS `primary_mobile`,ifnull(`phono`.`contact_value`,'-') AS `phone`,
`char_persons`.`first_name` AS `first_name`,`char_persons`.`second_name` AS `second_name`,
`char_persons`.`third_name` AS `third_name`,`char_persons`.`last_name` AS `last_name`,
`char_persons`.`prev_family_name` AS `prev_family_name`,`char_persons`.`gender` AS `gender`,
`char_persons`.`marital_status_id` AS `marital_status_id`,`char_persons`.`birthday` AS `birthday`,`char_persons`.`country` AS `country`,
`char_persons`.`governarate` AS `governarate`,`char_persons`.`city` AS `city`,`char_persons`.`location_id` AS `location_id`,
`char_persons`.`mosques_id` AS `mosques_id`,`char_persons`.`street_address` AS `street_address`,`char_persons`.`adscountry_id` AS `adscountry_id`,
`char_persons`.`adsdistrict_id` AS `adsdistrict_id`,`char_persons`.`adsregion_id` AS `adsregion_id`,
`char_persons`.`adsneighborhood_id` AS `adsneighborhood_id`,`char_persons`.`adssquare_id` AS `adssquare_id`,
`char_persons`.`adsmosques_id` AS `adsmosques_id`,`char_relays`.`id` AS `id`,`char_relays`.`person_id` AS `person_id`,
`char_relays`.`organization_id` AS `relay_organization_id`,`char_relays`.`category_id` AS `category_id`,`char_relays`.`user_id` AS `user_id`,
`char_relays`.`created_at` AS `created_at`,`char_relays`.`updated_at` AS `updated_at`,`char_relays`.`deleted_at` AS `deleted_at`,
`char_categories`.`name` AS `category_name`
from ((((((`char_relays`
join `char_categories` on((`char_categories`.`id` = `char_relays`.`category_id`)))
join `char_persons` on((`char_persons`.`id` = `char_relays`.`person_id`)))
left join `char_persons_contact` `mobno` on(((`mobno`.`person_id` = `char_relays`.`person_id`) and (`mobno`.`contact_type` = 'primary_mobile'))))
left join `char_persons_contact` `phono` on(((`phono`.`person_id` = `char_relays`.`person_id`) and (`phono`.`contact_type` = 'phone'))))
join `char_organizations` on((`char_organizations`.`id` = `char_relays`.`organization_id`)))
join `char_users` on((`char_users`.`id` = `char_relays`.`user_id`)))
 order by `char_relays`.`created_at` desc ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_sponsorship_cases_incomplete_data`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_sponsorship_cases_incomplete_data`  AS  select distinct `c`.`id` AS `id`,`c`.`person_id` AS `person_id`,`c`.`organization_id` AS `organization_id`,`org`.`name` AS `organization_name`,`c`.`category_id` AS `category_id`,`cat`.`name` AS `category_name`,`c`.`notes` AS `notes`,`c`.`visited_at` AS `visited_at`,`c`.`visitor` AS `visitor`,`c`.`status` AS `status`,`c`.`rank` AS `rank`,`c`.`user_id` AS `user_id`,`c`.`created_at` AS `case_created_at`,`c`.`updated_at` AS `case_updated_at`,`c`.`deleted_at` AS `case_deleted_at`,`p`.`first_name` AS `first_name`,`p`.`second_name` AS `second_name`,`p`.`third_name` AS `third_name`,`p`.`last_name` AS `last_name`,
`p`.`id_card_number` AS `id_card_number`,`p`.`old_id_card_number` AS `old_id_card_number`,`p`.`gender` AS `gender`,
`p`.`marital_status_id` AS `marital_status_id`,`p`.`birthday` AS `birthday`,`p`.`birth_place` AS `birth_place`,
`p`.`nationality` AS `nationality`,`p`.`death_date` AS `death_date`,`p`.`death_cause_id` AS `death_cause_id`,
`p`.`father_id` AS `father_id`,`p`.`mother_id` AS `mother_id`,`p`.`location_id` AS `location_id`,
`p`.`street_address` AS `street_address`,`p`.`refugee` AS `refugee`,
`p`.`unrwa_card_number` AS `unrwa_card_number`,`p`.`monthly_income` AS `monthly_income`,`p`.`spouses` AS
`spouses`,`p`.`created_at` AS `person_created_at`,`p`.`updated_at` AS `person_updated_at`,`p`.`deleted_at` AS `person_deleted_at`,
concat(`p`.`first_name`,' ',`p`.`second_name`,' ',`p`.`third_name`,' ',`p`.`last_name`) AS `name`,`guardians`.`guardian_id`
AS `guardian_id` from ((((((((((((((((((((((((((`char_cases` `c`
join `char_persons` `p` on((`c`.`person_id` = `p`.`id`)))
 join `char_organizations` `org` on((`org`.`id` = `c`.`organization_id`)))
 join `char_categories` `cat` on(((`c`.`category_id` = `cat`.`id`) and (`cat`.`type` = 1))))
 left join `char_persons` `f` on((`p`.`father_id` = `f`.`id`)))
 left join `char_persons` `m` on((`p`.`mother_id` = `m`.`id`)))
 left join `char_guardians` `guardians` on(((`p`.`id` = `guardians`.`individual_id`) and (`guardians`.`organization_id` = `c`.`organization_id`)
 and (`guardians`.`status` = 1)))) left join `char_persons` `g` on((`guardians`.`guardian_id` = `g`.`id`)))
 left join `char_residence` `r` on((`p`.`id` = `r`.`person_id`)))
 left join `char_residence` `gr` on((`g`.`id` = `r`.`person_id`)))
 left join `char_persons_work` `fpw` on((`f`.`id` = `fpw`.`person_id`)))
 left join `char_persons_work` `mw` on((`m`.`id` = `mw`.`person_id`)))
 left join `char_persons_work` `gpw` on((`g`.`id` = `gpw`.`person_id`)))
 left join `char_persons_education` `pe` on((`p`.`id` = `pe`.`person_id`)))
 left join `char_persons_education` `fe` on((`f`.`id` = `fe`.`person_id`)))
 left join `char_persons_education` `me` on((`m`.`id` = `me`.`person_id`)))
 left join `char_persons_education` `ge` on((`g`.`id` = `ge`.`person_id`)))
 left join `char_persons_health` `ph` on((`p`.`id` = `ph`.`person_id`)))
 left join `char_persons_health` `fh` on((`f`.`id` = `fh`.`person_id`)))
 left join `char_persons_health` `mh` on((`m`.`id` = `mh`.`person_id`)))
 left join `char_persons_i18n` `pi18n` on(((`p`.`id` = `pi18n`.`person_id`) and (`pi18n`.`language_id` = 2))))
 left join `char_persons_i18n` `fi18n` on(((`f`.`id` = `fi18n`.`person_id`) and (`fi18n`.`language_id` = 2))))
 left join `char_persons_i18n` `mi18n` on(((`m`.`id` = `mi18n`.`person_id`) and (`mi18n`.`language_id` = 2))))
 left join `char_persons_i18n` `gi18n` on(((`g`.`id` = `gi18n`.`person_id`) and (`gi18n`.`language_id` = 2))))
 left join `char_persons_contact` `pc` on((`p`.`id` = `pc`.`person_id`)))
 left join `char_persons_contact` `gc` on((`p`.`id` = `gc`.`person_id`)))
 left join `char_persons_islamic_commitment` `pi` on((`p`.`id` = `pi`.`person_id`)))
 where ((isnull(`p`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4))
 or (isnull(`p`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or
 (isnull(`p`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`p`.`last_name`)
 and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`p`.`id_card_number`)
 and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'id_card_number') = 4)) or (isnull(`p`.`gender`)
 and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'gender') = 4)) or (isnull(`p`.`birthday`) and
 (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birthday') = 4)) or (isnull(`p`.`birth_place`) and
  (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birth_place') = 4)) or (isnull(`p`.`nationality`)
   and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'nationality') = 4)) or (isnull(`pc`.`contact_value`)
    and (`pc`.`contact_type` = 'phone') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'phone') = 4))
    or (isnull(`pc`.`contact_value`) and (`pc`.`contact_type` = 'primary_mobile') and
    (`CHAR_GET_FORM_ELEMENT_PRIORITY`(1,'primary_mobile') = 4)) or
    (isnull(`pc`.`contact_value`) and (`pc`.`contact_type` = 'secondery_mobile')
    and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'secondery_mobile') = 4)) or (isnull(`r`.`property_type_id`) and
    (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'property_type_id') = 4)) or (isnull(`r`.`roof_material_id`) and
     (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'roof_material_id') = 4)) or (isnull(`r`.`residence_condition`)
      and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'residence_condition') = 4)) or (isnull(`r`.`indoor_condition`)
      and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'indoor_condition') = 4)) or (isnull(`r`.`habitable`) and
       (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'habitable') = 4)) or (isnull(`r`.`house_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'house_condition') = 4)) or (isnull(`r`.`rooms`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'rooms') = 4)) or (isnull(`pe`.`type`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'type') = 4)) or (isnull(`pe`.`authority`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'authority') = 4)) or (isnull(`pe`.`stage`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'stage') = 4)) or (isnull(`pe`.`grade`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'grade') = 4)) or (isnull(`pe`.`year`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'year') = 4)) or (isnull(`pe`.`school`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'school') = 4)) or (isnull(`pe`.`points`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'points') = 4)) or (isnull(`pe`.`level`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'level') = 4)) or (isnull(`ph`.`condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'health_condition') = 4)) or (isnull(`ph`.`disease_id`) and ((`ph`.`condition` = 2) or (`ph`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'disease_id') = 4)) or (isnull(`ph`.`details`) and ((`ph`.`condition` = 2) or (`ph`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'details') = 4)) or (isnull(`pi`.`prayer`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'prayer') = 4)) or (isnull(`pi`.`quran_parts`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'quran_parts') = 4)) or (isnull(`pi`.`quran_chapters`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'quran_chapters') = 4)) or (isnull(`pi`.`quran_parts`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'quran_parts') = 4)) or (isnull(`pi`.`quran_reason`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'quran_reason') = 4)) or (isnull(`f`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`f`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`f`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`f`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`f`.`id_card_number`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'id_card_number') = 4)) or (isnull(`f`.`birthday`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birthday') = 4)) or (isnull(`f`.`death_date`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'death_date') = 4)) or (isnull(`f`.`death_cause_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'death_cause') = 4)) or (isnull(`f`.`spouses`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'spouses') = 4)) or (isnull(`fi18n`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`fi18n`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`fi18n`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`fi18n`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`fpw`.`working`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'working') = 4)) or (isnull(`fpw`.`work_job_id`) and (`fpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_job_id') = 4)) or (isnull(`fpw`.`work_location`) and (`fpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_location') = 4)) or (isnull(`f`.`monthly_income`) and (`fpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'monthly_income') = 4)) or (isnull(`fh`.`condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'father_condition') = 4)) or (isnull(`fh`.`disease_id`) and ((`fh`.`condition` = 2) or (`fh`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'disease_id') = 4)) or (isnull(`fh`.`details`) and ((`fh`.`condition` = 2) or (`fh`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'details') = 4)) or (isnull(`fe`.`degree`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'degree') = 4)) or (isnull(`fe`.`specialization`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'specialization') = 4)) or (isnull(`m`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`m`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`m`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`m`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`m`.`id_card_number`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'id_card_number') = 4)) or (isnull(`m`.`marital_status_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_marital_status') = 4)) or (isnull(`m`.`birthday`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birthday') = 4)) or (isnull(`m`.`nationality`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_nationality') = 4)) or (isnull(`m`.`prev_family_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_prev_family_name') = 4)) or (isnull(`m`.`death_date`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_death_date') = 4)) or (isnull(`m`.`death_cause_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_death_cause') = 4)) or (isnull(`mi18n`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`mi18n`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`mi18n`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`mi18n`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`mw`.`working`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'working') = 4)) or (isnull(`mw`.`work_job_id`) and (`mw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_job_id') = 4)) or (isnull(`mw`.`work_location`) and (`mw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_location') = 4)) or (isnull(`m`.`monthly_income`) and (`mw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'monthly_income') = 4)) or (isnull(`mh`.`condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_condition') = 4)) or (isnull(`mh`.`disease_id`) and ((`mh`.`condition` = 2) or (`mh`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_disease') = 4)) or (isnull(`mh`.`details`) and ((`mh`.`condition` = 2) or (`mh`.`condition` = 3)) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_details') = 4)) or (isnull(`me`.`stage`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_stage') = 4)) or (isnull(`me`.`specialization`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'specialization') = 4)) or (isnull(`g`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`g`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`g`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`g`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`g`.`id_card_number`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'id_card_number') = 4)) or (isnull(`g`.`marital_status_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_marital_status') = 4)) or (isnull(`g`.`birthday`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'birthday') = 4)) or (isnull(`g`.`nationality`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'mother_nationality') = 4)) or (isnull(`g`.`location_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_address') = 4)) or (isnull(`gi18n`.`first_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`gi18n`.`second_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`gi18n`.`third_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`gi18n`.`last_name`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'name') = 4)) or (isnull(`gpw`.`working`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'working') = 4)) or (isnull(`gpw`.`work_job_id`) and (`gpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_job_id') = 4)) or (isnull(`gpw`.`work_location`) and (`gpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'work_location') = 4)) or (isnull(`g`.`monthly_income`) and (`gpw`.`working` = 1) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'monthly_income') = 4)) or (isnull(`ge`.`stage`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_stage') = 4)) or ((0 = (select count(0) from `char_persons_banks` where (`char_persons_banks`.`person_id` = `g`.`id`))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_banks') = 4)) or ((0 = (select count(0) from `char_persons_banks` where (`char_persons_banks`.`person_id` = `p`.`id`))) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'banks') = 4)) or (isnull(`gr`.`property_type_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_property_type') = 4)) or (isnull(`gr`.`roof_material_id`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_roof_material') = 4)) or (isnull(`gr`.`residence_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_residence_condition') = 4)) or (isnull(`gr`.`indoor_condition`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_indoor_condition') = 4)) or (isnull(`gr`.`rooms`) and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'guardian_rooms') = 4)) or (isnull(`gc`.`contact_value`) and (`gc`.`contact_type` = 'phone') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'phone') = 4)) or (isnull(`gc`.`contact_value`) and (`gc`.`contact_type` = 'primary_mobile') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(1,'primary_mobile') = 4)) or (isnull(`gc`.`contact_value`) and (`gc`.`contact_type` = 'secondery_mobile') and (`CHAR_GET_FORM_ELEMENT_PRIORITY`(`cat`.`id`,'secondery_mobile') = 4))) ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_transfers_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_transfers_view`
AS  select `char_categories`.`type` AS `category_type`,concat(ifnull(`char_users`.`firstname`,' '),' ',
ifnull(`char_users`.`lastname`,' ')) AS `user_name`,`char_persons`.`id_card_number` AS `id_card_number`,
`char_persons`.`old_id_card_number` AS `old_id_card_number`,concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',ifnull(`char_persons`.`third_name`,' '),' ',ifnull(`char_persons`.`last_name`,' ')) AS `full_name`,ifnull(`mobno`.`contact_value`,'-') AS `primary_mobile`,ifnull(`phono`.`contact_value`,'-') AS `phone`,`char_persons`.`first_name` AS `first_name`,`char_persons`.`second_name` AS `second_name`,`char_persons`.`third_name` AS `third_name`,`char_persons`.`last_name` AS `last_name`,`char_persons`.`prev_family_name` AS `prev_family_name`,`char_persons`.`gender` AS `gender`,`char_persons`.`marital_status_id` AS `marital_status_id`,`char_persons`.`birthday` AS `birthday`,`char_persons`.`country` AS `country`,`char_persons`.`governarate` AS `governarate`,`char_persons`.`city` AS `city`,`char_persons`.`location_id` AS `location_id`,`char_persons`.`mosques_id` AS `mosques_id`,`char_persons`.`street_address` AS `street_address`,`char_transfers`.`prev_adscountry_id` AS `prev_adscountry_id`,`char_transfers`.`prev_adsdistrict_id` AS `prev_adsdistrict_id`,`char_transfers`.`prev_adsregion_id` AS `prev_adsregion_id`,`char_transfers`.`prev_adsneighborhood_id` AS `prev_adsneighborhood_id`,`char_transfers`.`prev_adssquare_id` AS `prev_adssquare_id`,`char_transfers`.`prev_adsmosques_id` AS `prev_adsmosques_id`,`char_transfers`.`adscountry_id` AS `adscountry_id`,`char_transfers`.`adsdistrict_id` AS `adsdistrict_id`,`char_transfers`.`adsregion_id` AS `adsregion_id`,`char_transfers`.`adsneighborhood_id` AS `adsneighborhood_id`,`char_transfers`.`adssquare_id` AS `adssquare_id`,`char_transfers`.`adsmosques_id` AS `adsmosques_id`,`char_transfers`.`id` AS `id`,`char_transfers`.`person_id` AS `person_id`,`char_transfers`.`organization_id` AS `transfer_organization_id`,`char_transfers`.`category_id` AS `category_id`,`char_transfers`.`user_id` AS `user_id`,`char_transfers`.`created_at` AS `created_at`,`char_transfers`.`updated_at` AS `updated_at`,`char_transfers`.`deleted_at` AS `deleted_at`,`char_transfers`.`organization_id` AS `organization_id`,`char_organizations`.`name` AS `organization_name`,`char_categories`.`name` AS `category_name` from ((((((`char_transfers` join `char_categories` on((`char_categories`.`id` = `char_transfers`.`category_id`))) join `char_persons` on((`char_persons`.`id` = `char_transfers`.`person_id`))) left join `char_persons_contact` `mobno` on(((`mobno`.`person_id` = `char_transfers`.`person_id`) and (`mobno`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `phono` on(((`phono`.`person_id` = `char_transfers`.`person_id`) and (`phono`.`contact_type` = 'phone')))) join `char_organizations` on((`char_organizations`.`id` = `char_transfers`.`organization_id`))) join `char_users` on((`char_users`.`id` = `char_transfers`.`user_id`))) order by `char_transfers`.`created_at` desc ;

-- ---------------------------------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_vouchers_persons_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW
`char_vouchers_persons_view`  AS  select `char_vouchers_persons`.`id` AS `id`,
`char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`,`char_vouchers_persons`.`person_id` AS `person_id`,`char_vouchers_persons`.`individual_id` AS `individual_id`,`char_vouchers_persons`.`receipt_date` AS `receipt_date`,`char_vouchers_persons`.`receipt_time` AS `receipt_time`,`char_vouchers_persons`.`receipt_location` AS `receipt_location`,`char_vouchers_persons`.`status` AS `rec_status`,`v`.`organization_id` AS `organization_id`,`v`.`category_id` AS `category_id`,`v`.`project_id` AS `project_id`,`org`.`name` AS `organization_name`,`org`.`en_name` AS `en_organization_name`,`sponsor`.`name` AS `sponsor_name`,`sponsor`.`en_name` AS `en_sponsor_name`,`v`.`type` AS `type`,`v`.`value` AS `value`,(`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`,`v`.`title` AS `title`,`v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`,`v`.`beneficiary` AS `beneficiary`,`v`.`currency_id` AS `currency_id`,`v`.`exchange_rate` AS `exchange_rate`,`v`.`template` AS `template`,`v`.`content` AS `content`,`v`.`header` AS `header`,`v`.`footer` AS `footer`,`v`.`notes` AS `notes`,`v`.`created_at` AS `created_at`,`v`.`updated_at` AS `updated_at`,`v`.`deleted_at` AS `deleted_at`,`v`.`sponsor_id` AS `sponsor_id`,`v`.`voucher_date` AS `voucher_date`,`v`.`count` AS `count`,`v`.`urgent` AS `urgent`,`v`.`status` AS `status`,`v`.`transfer` AS `transfer`,`v`.`transfer_company_id` AS `transfer_company_id`,`v`.`allow_day` AS `allow_day`,`v`.`organization_project_id` AS `organization_project_id`,`char_vouchers_categories`.`name` AS `category_type`,(case when isnull(`v`.`project_id`) then 'من مشروع' else 'يدوي' end) AS `voucher_source`,(case when (`v`.`type` = 1) then 'نقدية' when (`v`.`type` = 2) then 'عينية' end) AS `voucher_type`,(case when (`v`.`urgent` = 0) then 'عادية' when (`v`.`urgent` = 1) then 'عاجلة' end) AS `voucher_urgent`,(case when (`v`.`transfer` = 0) then 'مباشر' when (`v`.`transfer` = 1) then 'تصنيف جمعية' end) AS `transfer_status`,(case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`,(case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`,(case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`,concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`,`c`.`first_name` AS `first_name`,`c`.`second_name` AS `second_name`,`c`.`third_name` AS `third_name`,`c`.`last_name` AS `last_name`,(case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`,(case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'ذكر ' when (`c`.`gender` = 2) then 'أنثى' end) AS `gender`,`c`.`gender` AS `p_gender`,
(case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`,
(case when isnull(`c`.`old_id_card_number`) then ' ' else `c`.`old_id_card_number` end) AS `old_id_card_number`,

`char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`,concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`,`ind`.`first_name` AS `ind_first_name`,`ind`.`second_name` AS `ind_second_name`,`ind`.`third_name` AS `ind_third_name`,`ind`.`last_name` AS `ind_last_name`,
(case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`,
(case when isnull(`ind`.`old_id_card_number`) then ' ' else `ind`.`old_id_card_number` end) AS `individual_old_id_card_number`,
(case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'ذكر ' when (`ind`.`gender` = 2) then 'أنثى' end) AS `individual_gender`,`ind`.`gender` AS `ind_gender`,(case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`,(case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`,(case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`,(case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`,(case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`,(case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`,ifnull(`country_name`.`name`,' ') AS `country`,ifnull(`district_name`.`name`,' ') AS `district`,ifnull(`region_name`.`name`,' ') AS `region`,ifnull(`location_name`.`name`,' ') AS `nearlocation`,ifnull(`square_name`.`name`,' ') AS `square`,ifnull(`mosques_name`.`name`,' ') AS `mosque`,ifnull(`c`.`street_address`,' ') AS `street_address`,concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`,time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`,`char_vouchers_persons`.`status` AS `receipt_status`,(case when (`char_vouchers_persons`.`status` = 1) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 0) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 2) then ' لم يتم الاستلام' end) AS `receipt_status_name`,`v`.`case_category_id` AS `case_category_id`,`ca`.`name` AS `case_category_name`,`ca`.`en_name` AS `en_case_category_name` from (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 1)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 1)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 1)))) ;

-- --------------------------------------------------------

DROP VIEW IF EXISTS `char_vouchers_persons_view_en`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view_en`  AS  select `char_vouchers_persons`.`id` AS `id`,`char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`,`char_vouchers_persons`.`person_id` AS `person_id`,`char_vouchers_persons`.`individual_id` AS `individual_id`,`char_vouchers_persons`.`receipt_date` AS `receipt_date`,`char_vouchers_persons`.`receipt_time` AS `receipt_time`,`char_vouchers_persons`.`receipt_location` AS `receipt_location`,`char_vouchers_persons`.`status` AS `rec_status`,`v`.`organization_id` AS `organization_id`,`v`.`category_id` AS `category_id`,`v`.`project_id` AS `project_id`,`org`.`name` AS `organization_name`,`org`.`en_name` AS `en_organization_name`,`sponsor`.`name` AS `sponsor_name`,`sponsor`.`en_name` AS `en_sponsor_name`,`v`.`type` AS `type`,`v`.`value` AS `value`,(`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`,`v`.`title` AS `title`,`v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`,`v`.`beneficiary` AS `beneficiary`,`v`.`currency_id` AS `currency_id`,`v`.`exchange_rate` AS `exchange_rate`,`v`.`template` AS `template`,`v`.`content` AS `content`,`v`.`header` AS `header`,`v`.`footer` AS `footer`,`v`.`notes` AS `notes`,`v`.`created_at` AS `created_at`,`v`.`updated_at` AS `updated_at`,`v`.`deleted_at` AS `deleted_at`,`v`.`sponsor_id` AS `sponsor_id`,`v`.`voucher_date` AS `voucher_date`,`v`.`count` AS `count`,`v`.`urgent` AS `urgent`,`v`.`status` AS `status`,`v`.`transfer` AS `transfer`,`v`.`transfer_company_id` AS `transfer_company_id`,`v`.`allow_day` AS `allow_day`,`v`.`organization_project_id` AS `organization_project_id`,`char_vouchers_categories`.`name` AS `category_type`,(case when isnull(`v`.`project_id`) then 'from_project' else 'manually' end) AS `voucher_source`,(case when (`v`.`type` = 1) then 'financial' when (`v`.`type` = 2) then 'non_financial' end) AS `voucher_type`,(case when (`v`.`urgent` = 0) then 'not_urgent' when (`v`.`urgent` = 1) then 'urgent' end) AS `voucher_urgent`,(case when (`v`.`transfer` = 0) then 'direct' when (`v`.`transfer` = 1) then 'transfer_company' end) AS `transfer_status`,(case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`,(case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`,(case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`,concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`,`c`.`first_name` AS `first_name`,`c`.`second_name` AS `second_name`,`c`.`third_name` AS `third_name`,`c`.`last_name` AS `last_name`,(case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`,(case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'male' when (`c`.`gender` = 2) then 'female' end) AS `gender`,`c`.`gender` AS `p_gender`,
(case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`,
(case when isnull(`c`.`old_id_card_number`) then ' ' else `c`.`old_id_card_number` end) AS `old_id_card_number`,
`char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`,concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`,`ind`.`first_name` AS `ind_first_name`,`ind`.`second_name` AS `ind_second_name`,`ind`.`third_name` AS `ind_third_name`,`ind`.`last_name` AS `ind_last_name`,
(case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`,
(case when isnull(`ind`.`old_id_card_number`) then ' ' else `ind`.`old_id_card_number` end) AS `individual_old_id_card_number`,
(case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'male' when (`ind`.`gender` = 2) then 'female' end) AS `individual_gender`,`ind`.`gender` AS `ind_gender`,(case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`,(case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`,(case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`,(case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`,(case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`,(case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`,ifnull(`country_name`.`name`,' ') AS `country`,ifnull(`district_name`.`name`,' ') AS `district`,ifnull(`region_name`.`name`,' ') AS `region`,ifnull(`location_name`.`name`,' ') AS `nearlocation`,ifnull(`square_name`.`name`,' ') AS `square`,ifnull(`mosques_name`.`name`,' ') AS `mosque`,ifnull(`c`.`street_address`,' ') AS `street_address`,concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`,time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`,`char_vouchers_persons`.`status` AS `receipt_status`,(case when (`char_vouchers_persons`.`status` = 1) then 'receipt' when (`char_vouchers_persons`.`status` = 0) then 'receipt' when (`char_vouchers_persons`.`status` = 2) then 'not_receipt' end) AS `receipt_status_name`,`v`.`case_category_id` AS `case_category_id`,`ca`.`name` AS `case_category_name`,`ca`.`en_name` AS `en_case_category_name` from (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 2)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 2)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 2)))) ;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE `clone_government_persons` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `IDNO` varchar(200) DEFAULT NULL COMMENT 'رقم الهوية',
  `FATHER_IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية الأب',
  `MOTHER_IDNO` int(11) DEFAULT NULL COMMENT 'رقم  هوية الأم',

  `FNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'الاسم الأول',
  `SNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأب',
  `TNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الجد',
  `LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة',
  `PREV_LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة السابق',
  `ENG_NAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالانجليزي',

  `SEX` varchar(200) DEFAULT NULL COMMENT 'الجنس',
  `SOCIAL_STATUS` varchar(200) DEFAULT NULL COMMENT 'الحالة الاجتماعية',

  `MOBILE` varchar(200) DEFAULT NULL COMMENT 'رقم الجوال',
  `TEL` varchar(200) DEFAULT NULL COMMENT 'رقم التلفون',

  `BIRTH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `DETH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الوفاة',

  `BIRTH_PMAIN` varchar(200) DEFAULT NULL COMMENT 'دولة الميلاد',
  `BIRTH_PSUB` varchar(200) DEFAULT NULL COMMENT 'مدينة الميلاد',

  `CI_CITY` varchar(200) DEFAULT NULL COMMENT 'المدينة',
  `CI_REGION` varchar(200) DEFAULT NULL COMMENT 'المنطقة',
  `CI_RELIGION` varchar(200) DEFAULT NULL COMMENT 'الديانة',

  `MOTHER_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأم',

  `CLASS_NAME` varchar(200) DEFAULT NULL COMMENT 'درجة  الموظف',
  `DEGREE_NAME` varchar(200) DEFAULT NULL COMMENT 'درجة  الموظف',
  `EMP_BIRTH_DT` varchar(200) DEFAULT NULL,
  `EMP_DOC_ID` varchar(200) DEFAULT NULL,
  `EMP_FULL_NAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالكامل',
  `EMP_NO` varchar(200) DEFAULT NULL COMMENT 'رقم الموظف',
  `EMP_STATE_DESC` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف',
  `EMP_STATE_NOW` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف حاليا',
  `EMP_STATE_NOW_TXT` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف حاليا',
  `EMP_WORK_STATUS` varchar(200) DEFAULT NULL COMMENT 'حالة عمل الموظف',
  `END_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ نهاية العمل',
  `JOB_DESC` varchar(200) DEFAULT NULL COMMENT 'الوظيفة',
  `JOB_START_DT` varchar(200) DEFAULT NULL,
  `MINISTRY_NAME` varchar(200) DEFAULT NULL COMMENT 'اسم المؤسسة',
  `MINISTRY_UNIT_NAME` varchar(200) DEFAULT NULL COMMENT 'الوحدة او القسم',
  `MIN_DATA_SOURCE` varchar(200) DEFAULT NULL,

  `has_commercial_records` varchar(200) DEFAULT NULL COMMENT 'هل يومجد سجلات تجارية فعالة',
  `active_commercial_records` varchar(200) DEFAULT NULL COMMENT 'عدد السجلات الفعالة',
  `gov_commercial_records_details` longtext COMMENT 'تفاصيل السجلات الفعالة',

  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل على السجل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البيانات الشخصية - محفوظ من البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `clone_government_persons`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `clone_government_persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول';

-- --------------------------------------------------------

CREATE TABLE `clone_government_relations` (
  `IDNO` int(11) DEFAULT NULL,
  `IDNO_RELATIVE` int(11) DEFAULT NULL,
  `RELATIVE` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول العلاقات';

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------
-- create database gov_data_db
CREATE DATABASE `gov_data_db` CHARACTER SET 'utf8' COLLATE 'utf8_general_ci';


CREATE TABLE `gov_persons` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `IDNO` varchar(200) DEFAULT NULL COMMENT 'رقم الهوية',
  `FATHER_IDNO` int(11) DEFAULT NULL,
  `MOTHER_IDNO` int(11) DEFAULT NULL,
  `FNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'الاسم الأول',
  `SNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأب',
  `TNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الجد',
  `LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة',
  `PREV_LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة السابق',
  `ENG_NAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالانجليزي',
  `SEX` varchar(200) DEFAULT NULL COMMENT 'الجنس',
  `SOCIAL_STATUS` varchar(200) DEFAULT NULL COMMENT 'الحالة الاجتماعية',
  `MOBILE` varchar(200) DEFAULT NULL COMMENT 'رقم الجوال',
  `BIRTH_PMAIN` varchar(200) DEFAULT NULL COMMENT 'دولة الميلاد',
  `BIRTH_PSUB` varchar(200) DEFAULT NULL COMMENT 'مدينة الميلاد',
  `BIRTH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `DETH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `TEL` varchar(200) DEFAULT NULL COMMENT 'رقم التلفون',
  `CI_CITY` varchar(200) DEFAULT NULL COMMENT 'المدينة',
  `CI_REGION` varchar(200) DEFAULT NULL COMMENT 'المنطقة',
  `CI_RELIGION` varchar(200) DEFAULT NULL COMMENT 'الديانة',
  `MOTHER_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأم',
  `CLASS_NAME` varchar(200) DEFAULT NULL COMMENT 'درجة  الموظف',
  `DEGREE_NAME` varchar(200) DEFAULT NULL COMMENT 'درجة  الموظف',
  `EMP_BIRTH_DT` varchar(200) DEFAULT NULL,
  `EMP_DOC_ID` varchar(200) DEFAULT NULL,
  `EMP_FULL_NAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالكامل',
  `EMP_NO` varchar(200) DEFAULT NULL COMMENT 'رقم الموظف',
  `EMP_STATE_DESC` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف',
  `EMP_STATE_NOW` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف حاليا',
  `EMP_STATE_NOW_TXT` varchar(200) DEFAULT NULL COMMENT 'حالة الموظف حاليا',
  `EMP_WORK_STATUS` varchar(200) DEFAULT NULL COMMENT 'حالة عمل الموظف',
  `END_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ نهاية العمل',
  `JOB_DESC` varchar(200) DEFAULT NULL COMMENT 'الوظيفة',
  `JOB_START_DT` varchar(200) DEFAULT NULL,
  `MINISTRY_NAME` varchar(200) DEFAULT NULL COMMENT 'اسم المؤسسة',
  `MINISTRY_UNIT_NAME` varchar(200) DEFAULT NULL COMMENT 'الوحدة او القسم',
  `MIN_DATA_SOURCE` varchar(200) DEFAULT NULL,
  `has_commercial_records` varchar(200) DEFAULT NULL COMMENT 'هل يومجد سجلات تجارية فعالة',
  `active_commercial_records` varchar(200) DEFAULT NULL COMMENT 'عدد السجلات الفعالة',
  `gov_commercial_records_details` longtext COMMENT 'تفاصيل السجلات الفعالة',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل على السجل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البيانات الشخصية - محفوظ من البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `gov_persons`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `gov_persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول';

-- --------------------------------------------------------
CREATE TABLE `gov_relations` (
  `IDNO` int(11) DEFAULT NULL,
  `IDNO_RELATIVE` int(11) DEFAULT NULL,
  `RELATIVE` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول العلاقات';

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE `char_vouchers_persons_files` (
  `id` int(11) NOT NULL COMMENT 'المفتاح الاساسي للجدول',
  `voucher_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم القسيمة',
  `file_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الملف',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT 'المستخدم الذي رفع الملف',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ التعديل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول ملفات أرقام المستفيدين';

ALTER TABLE `char_vouchers_persons_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_vouchers_persons_files_file_id_idx` (`file_id`) ,
  ADD KEY `char_vouchers_persons_files_user_id_idx` (`user_id`) ,
  ADD KEY `char_vouchers_persons_files_voucher_id_idx` (`voucher_id`) ;

ALTER TABLE `char_vouchers_persons_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الاساسي للجدول';

ALTER TABLE `char_vouchers_persons_files`
  ADD CONSTRAINT `fk_char_vouchers_persons_files_file_id` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_char_vouchers_persons_files_user_id` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`),
  ADD CONSTRAINT `fk_char_vouchers_persons_files_voucher_id` FOREIGN KEY (`voucher_id`) REFERENCES `char_vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

ALTER TABLE `char_persons` DROP `old_id_card_number`;
ALTER TABLE `char_persons` ADD `old_id_card_number`   varchar(9) NULL DEFAULT NULL COMMENT 'رقم بطاقة التعريف القديم' ;
UPDATE `char_persons` SET  `old_id_card_number` = `id_card_number` WHERE id_card_number LIKE '700%' ;

-- ---------------------------------------------------------------------------------------------------------------------

UPDATE `char_persons` , old_crd
SET `char_persons`.`old_id_card_number` = `old_crd`.`MSS_old_id`
WHERE `char_persons`.`id_card_number` = `old_crd`.`MSS_new_id`;

ALTER TABLE `old_crd`
ADD `contact_primary` TINYINT(2) NOT NULL DEFAULT '0' AFTER `ids`,
ADD `contact_secondary` TINYINT(2) NOT NULL DEFAULT '0' AFTER `contact_primary`,
ADD `contact_phone` TINYINT(2) NOT NULL DEFAULT '0' AFTER `contact_secondary`,
ADD `education` TINYINT(2) NOT NULL DEFAULT '0' AFTER `contact_phone`,
ADD `health` TINYINT(2) NOT NULL DEFAULT '0' AFTER `education`,
ADD `i18n` TINYINT(2) NULL DEFAULT '0' AFTER `health`,
ADD `islamic_commitment` TINYINT(2) NOT NULL DEFAULT '0' AFTER `i18n`,
ADD `work` TINYINT(2) NOT NULL DEFAULT '0' AFTER `islamic_commitment`,
ADD `residence` TINYINT(2) NOT NULL DEFAULT '0' AFTER `work`,
ADD `old_person_id` INT NULL DEFAULT NULL AFTER `MSS_old_id`,
ADD `new_person_id` INT NULL DEFAULT NULL AFTER `MSS_new_id`;

-- ---------------------------------------------------------------------------------------------------------------------
UPDATE `char_persons` , old_crd
SET `old_crd`.`old_person_id` = `char_persons`.`id`
WHERE `char_persons`.`id_card_number` = `old_crd`.`MSS_old_id`;
-- ---------------------------------------------------------------------------------------------------------------------
UPDATE `char_persons` , old_crd
SET `old_crd`.`new_person_id` = `char_persons`.`id`
WHERE `char_persons`.`id_card_number` = `old_crd`.`MSS_new_id`;





