CREATE TABLE `clone_government_persons` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `IDNO` varchar(200) DEFAULT NULL COMMENT 'رقم الهوية',
  `FATHER_IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية الأب',
  `MOTHER_IDNO` int(11) DEFAULT NULL COMMENT 'رقم  هوية الأم',
  `FULLNAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالعربي كامل',
  `FNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'الاسم الأول',
  `SNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأب',
  `TNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الجد',
  `LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة',
  `PREV_LNAME_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم العائلة السابق',
  `MOTHER_ARB` varchar(200) DEFAULT NULL COMMENT 'اسم الأم',
  `ENG_NAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالانجليزي',
  `SEX` varchar(200) DEFAULT NULL COMMENT 'الجنس',
  `SOCIAL_STATUS` varchar(200) DEFAULT NULL COMMENT 'الحالة الاجتماعية',
  `CI_RELIGION` varchar(200) DEFAULT NULL COMMENT 'الديانة',
  `MOBILE` varchar(200) DEFAULT NULL COMMENT 'رقم الجوال',
  `TEL` varchar(200) DEFAULT NULL COMMENT 'رقم التلفون',
  `BIRTH_PMAIN` varchar(200) DEFAULT NULL COMMENT 'دولة الميلاد',
  `BIRTH_PSUB` varchar(200) DEFAULT NULL COMMENT 'مدينة الميلاد',
  `BIRTH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `DETH_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `IS_DEAD_DESC` varchar(200) DEFAULT NULL COMMENT 'هل متوفى',
  `CI_CITY` varchar(200) DEFAULT NULL COMMENT 'المدينة',
  `CI_REGION` varchar(200) DEFAULT NULL COMMENT 'المنطقة',
  `STREET_ARB` varchar(200) DEFAULT NULL COMMENT 'الشارع',
  `CTZN_STATUS` varchar(200) DEFAULT NULL COMMENT 'حالة المواطن ',
  `CTZN_TRANS_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ اخر حركة/  تنقل',
  `VISIT_PURPOSE_DESC` varchar(200) DEFAULT NULL COMMENT 'سبب اخر حركة / تنقل',
  `photo_url` varchar(255) DEFAULT NULL COMMENT 'رابط صورة المواطن ',
  `photo_update_date` varchar(255) DEFAULT NULL COMMENT 'تاريخ اخر تحديث لصورة المواطن ',
  `PASSPORT_NO` varchar(255) DEFAULT NULL COMMENT 'رقم الجواز ',
  `PASSPORT_ISSUED_ON` varchar(255) DEFAULT NULL COMMENT 'تاريخ اصدار الجواز ',
  `PASSPORT_TYPE` varchar(255) DEFAULT NULL COMMENT 'نوع الجواز ',
  `CARD_NO` varchar(255) DEFAULT NULL COMMENT 'رقم التأمين الصحي ',
  `EXP_DATE` varchar(255) DEFAULT NULL COMMENT 'تاريخ انتهاء التأمين الصحي ',
  `INS_STATUS_DESC` varchar(255) DEFAULT NULL COMMENT 'حالة التأمين الصحي ',
  `INS_TYPE_DESC` varchar(255) DEFAULT NULL COMMENT 'نوع التأمين الصحي ',
  `WORK_SITE_DESC` varchar(255) DEFAULT NULL COMMENT 'مكان اصدار التأمين الصحي ',
  `family_cnt` int(11) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `spouses` int(11) DEFAULT NULL COMMENT 'عدد الزوجات',
  `male_live` int(11) DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة',
  `female_live` int(11) DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة',
  `GOV_NAME` varchar(200) DEFAULT NULL COMMENT 'المحافظة حسب تحديث sso',
  `CITY_NAME` varchar(200) DEFAULT NULL COMMENT 'المدينة حسب تحديث sso',
  `PART_NAME` varchar(200) DEFAULT NULL COMMENT 'منطقة و المربع حسب تحديث sso',
  `ADDRESS_DET` varchar(200) DEFAULT NULL COMMENT 'تفاصيل العنوان حسب تحديث sso',
  `social_affairs_status` varchar(200) DEFAULT NULL COMMENT 'حالة الاستفادة من الشئون الاجتماعية' ,
  `AID_CLASS` varchar(200) DEFAULT NULL COMMENT 'تصنيف المساعدة من الشئون الاجتماعية',
  `AID_TYPE` varchar(200) DEFAULT NULL COMMENT 'نوع المساعدة من الشئون الاجتماعية',
  `AID_AMOUNT` varchar(200) DEFAULT NULL COMMENT 'مبلغ المساعدة من الشئون الاجتماعية' ,
  `AID_SOURCE` varchar(200) DEFAULT NULL COMMENT 'مصدر المساعدة من الشئون الاجتماعية' ,
  `AID_PERIODIC` varchar(200) DEFAULT NULL COMMENT 'دورية المساعدة من الشئون الاجتماعية' ,
  `ST_BENEFIT_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ بدء الاستفادة من الشئون الاجتماعية' ,
  `END_BENEFIT_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ انتهاء الاستفادة من الشئون الاجتماعية' ,
  `home_address` varchar(200) DEFAULT NULL COMMENT 'عنوان المنزل - حسب لجان الزكاة ' ,
  `near_mosque` varchar(200) DEFAULT NULL COMMENT 'أقرب مسجد - حسب لجان الزكاة ' ,
  `paterfamilias_mobile` varchar(200) DEFAULT NULL COMMENT 'رقم جوال رب الأسرة - حسب لجان الزكاة ' ,
  `telephone` varchar(200) DEFAULT NULL COMMENT 'رقم هاتف - حسب لجان الزكاة ' ,
  `current_career` varchar(200) DEFAULT NULL COMMENT 'المهنة الحالية - حسب لجان الزكاة ' ,
  `father_death_reason` varchar(200) DEFAULT NULL COMMENT 'سبب وفاة الأب - حسب لجان الزكاة ' ,
  `mother_death_reason` varchar(200) DEFAULT NULL COMMENT 'سبب وفاة الأم - حسب لجان الزكاة ' ,
  `building_type` varchar(200) DEFAULT NULL COMMENT 'نوع بناء المنزل - حسب لجان الزكاة ' ,
  `home_type` varchar(200) DEFAULT NULL COMMENT 'نوع المنزل - حسب لجان الزكاة ' ,
  `furniture_type` varchar(200) DEFAULT NULL COMMENT 'نوع أو حالة الأثاث - حسب لجان الزكاة ' ,
  `home_status` varchar(200) DEFAULT NULL COMMENT 'حالة المنزل - حسب لجان الزكاة ' ,
  `home_description` longtext DEFAULT NULL COMMENT 'وصف عن المنزل - حسب لجان الزكاة ' ,
  `total_family_income` varchar(200) DEFAULT NULL COMMENT 'دخل الأسرة - حسب لجان الزكاة ' ,
  `health_status` varchar(200) DEFAULT NULL COMMENT 'الحالة الصحية - حسب لجان الزكاة ' ,
  `wife_mobile` varchar(200) DEFAULT NULL COMMENT 'جوال الزوجة - حسب لجان الزكاة ' ,
  `affected_by_wars` varchar(200) DEFAULT NULL COMMENT 'متضرر من الحرب - حسب لجان الزكاة ' ,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل على السجل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البيانات الشخصية - محفوظ من البحث' ROW_FORMAT=DYNAMIC;

ALTER TABLE `clone_government_persons` ADD PRIMARY KEY (`id`);
ALTER TABLE `clone_government_persons`  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول';

-- ALTER TABLE `clone_government_persons` ADD `FULLNAME` varchar(200) DEFAULT NULL COMMENT 'الاسم بالعربي كامل'  AFTER `MOTHER_IDNO`;
-- ALTER TABLE `clone_government_persons` ADD `IS_DEAD_DESC` varchar(200) DEFAULT NULL COMMENT 'هل متوفى'  AFTER `BIRTH_DT`;
--
-- ALTER TABLE `clone_government_persons` ADD `STREET_ARB` varchar(200) DEFAULT NULL COMMENT 'الشارع'   AFTER `CI_REGION`;
--
-- ALTER TABLE `clone_government_persons` ADD `CTZN_STATUS` varchar(200) DEFAULT NULL COMMENT 'حالة المواطن '  AFTER `TEL`;
-- ALTER TABLE `clone_government_persons` ADD `CTZN_TRANS_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ اخر حركة/  تنقل'  AFTER `CTZN_STATUS`;
-- ALTER TABLE `clone_government_persons` ADD  `VISIT_PURPOSE_DESC` varchar(200) DEFAULT NULL COMMENT 'سبب اخر حركة / تنقل'  AFTER `CTZN_TRANS_DT`;
--
-- ALTER TABLE `clone_government_persons` ADD `photo_url` varchar(255) DEFAULT NULL COMMENT 'حالة المواطن ' AFTER `CTZN_TRANS_DT`;
-- ALTER TABLE `clone_government_persons` ADD `photo_update_date` varchar(255) DEFAULT NULL COMMENT 'حالة المواطن ' AFTER `photo_url`;
--
-- ALTER TABLE `clone_government_persons` ADD `PASSPORT_NO` varchar(255) DEFAULT NULL COMMENT 'رقم الجواز ' AFTER `photo_update_date`;
-- ALTER TABLE `clone_government_persons` ADD `PASSPORT_ISSUED_ON` varchar(255) DEFAULT NULL COMMENT 'تاريخ اصدار الجواز ' AFTER `PASSPORT_NO`;
-- ALTER TABLE `clone_government_persons` ADD  `PASSPORT_TYPE` varchar(255) DEFAULT NULL COMMENT 'نوع الجواز ' AFTER `PASSPORT_ISSUED_ON`;
--
-- ALTER TABLE `clone_government_persons` ADD   `CARD_NO` varchar(255) DEFAULT NULL COMMENT 'رقم التأمين الصحي ' AFTER `PASSPORT_TYPE`;
-- ALTER TABLE `clone_government_persons` ADD   `EXP_DATE` varchar(255) DEFAULT NULL COMMENT 'تاريخ انتهاء التأمين الصحي ' AFTER `CARD_NO`;
-- ALTER TABLE `clone_government_persons` ADD   `INS_STATUS_DESC` varchar(255) DEFAULT NULL COMMENT 'حالة التأمين الصحي ' AFTER `EXP_DATE`;
-- ALTER TABLE `clone_government_persons` ADD   `INS_TYPE_DESC` varchar(255) DEFAULT NULL COMMENT 'نوع التأمين الصحي ' AFTER `INS_STATUS_DESC`;
-- ALTER TABLE `clone_government_persons` ADD   `WORK_SITE_DESC` varchar(255) DEFAULT NULL COMMENT 'مكان اصدار التأمين الصحي ' AFTER `INS_TYPE_DESC`;

ALTER TABLE `clone_government_persons` ADD `family_cnt` int(11) DEFAULT NULL COMMENT 'عدد أفراد العائلة' AFTER `WORK_SITE_DESC`;
ALTER TABLE `clone_government_persons` ADD `spouses` int(11) DEFAULT NULL COMMENT 'عدد الزوجات على قيد الحياة' AFTER `family_cnt`;
ALTER TABLE `clone_government_persons` ADD `male_live` int(11) DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة' AFTER `spouses`;
ALTER TABLE `clone_government_persons` ADD `female_live` int(11) DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة' AFTER `male_live`;

ALTER TABLE `clone_government_persons` ADD `GOV_NAME` varchar(200) DEFAULT NULL COMMENT 'المحافظة حسب تحديث sso' AFTER `female_live`;
ALTER TABLE `clone_government_persons` ADD `CITY_NAME` varchar(200) DEFAULT NULL COMMENT 'المدينة حسب تحديث sso' AFTER `GOV_NAME`;
ALTER TABLE `clone_government_persons` ADD `PART_NAME` varchar(200) DEFAULT NULL COMMENT 'منطقة و المربع حسب تحديث sso' AFTER `CITY_NAME`;
ALTER TABLE `clone_government_persons` ADD `ADDRESS_DET` varchar(200) DEFAULT NULL COMMENT 'تفاصيل العنوان حسب تحديث sso' AFTER `PART_NAME`;

ALTER TABLE `clone_government_persons` ADD `social_affairs_status` varchar(200) DEFAULT NULL COMMENT 'حالة الاستفادة من الشئون الاجتماعية' AFTER `ADDRESS_DET`;
ALTER TABLE `clone_government_persons` ADD `AID_CLASS` varchar(200) DEFAULT NULL COMMENT 'تصنيف المساعدة من الشئون الاجتماعية' AFTER `ADDRESS_DET`;
ALTER TABLE `clone_government_persons` ADD `AID_TYPE` varchar(200) DEFAULT NULL COMMENT 'نوع المساعدة من الشئون الاجتماعية' AFTER `AID_CLASS`;
ALTER TABLE `clone_government_persons` ADD `AID_AMOUNT` varchar(200) DEFAULT NULL COMMENT 'مبلغ المساعدة من الشئون الاجتماعية' AFTER `AID_TYPE`;
ALTER TABLE `clone_government_persons` ADD `AID_SOURCE` varchar(200) DEFAULT NULL COMMENT 'مصدر المساعدة من الشئون الاجتماعية' AFTER `AID_TYPE`;
ALTER TABLE `clone_government_persons` ADD `AID_PERIODIC` varchar(200) DEFAULT NULL COMMENT 'دورية المساعدة من الشئون الاجتماعية' AFTER `AID_TYPE`;
ALTER TABLE `clone_government_persons` ADD `ST_BENEFIT_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ بدء الاستفادة من الشئون الاجتماعية' AFTER `AID_TYPE`;
ALTER TABLE `clone_government_persons` ADD `END_BENEFIT_DATE` varchar(200) DEFAULT NULL COMMENT 'تاريخ انتهاء الاستفادة من الشئون الاجتماعية' AFTER `AID_TYPE`;

ALTER TABLE `clone_government_persons` ADD `home_address` varchar(200) DEFAULT NULL COMMENT 'عنوان المنزل - حسب لجان الزكاة ' AFTER `END_BENEFIT_DATE`;
ALTER TABLE `clone_government_persons` ADD `near_mosque` varchar(200) DEFAULT NULL COMMENT 'أقرب مسجد - حسب لجان الزكاة ' AFTER `home_address`;
ALTER TABLE `clone_government_persons` ADD `paterfamilias_mobile` varchar(200) DEFAULT NULL COMMENT 'رقم جوال رب الأسرة - حسب لجان الزكاة ' AFTER `near_mosque`;
ALTER TABLE `clone_government_persons` ADD `telephone` varchar(200) DEFAULT NULL COMMENT 'رقم هاتف - حسب لجان الزكاة ' AFTER `paterfamilias_mobile`;
ALTER TABLE `clone_government_persons` ADD `current_career` varchar(200) DEFAULT NULL COMMENT 'المهنة الحالية - حسب لجان الزكاة ' AFTER `telephone`;
ALTER TABLE `clone_government_persons` ADD `father_death_reason` varchar(200) DEFAULT NULL COMMENT 'سبب وفاة الأب - حسب لجان الزكاة ' AFTER `current_career`;
ALTER TABLE `clone_government_persons` ADD `mother_death_reason` varchar(200) DEFAULT NULL COMMENT 'سبب وفاة الأم - حسب لجان الزكاة ' AFTER `father_death_reason`;
ALTER TABLE `clone_government_persons` ADD `building_type` varchar(200) DEFAULT NULL COMMENT 'نوع بناء المنزل - حسب لجان الزكاة ' AFTER `mother_death_reason`;
ALTER TABLE `clone_government_persons` ADD `home_type` varchar(200) DEFAULT NULL COMMENT 'نوع المنزل - حسب لجان الزكاة ' AFTER `building_type`;
ALTER TABLE `clone_government_persons` ADD `furniture_type` varchar(200) DEFAULT NULL COMMENT 'نوع أو حالة الأثاث - حسب لجان الزكاة ' AFTER `home_type`;
ALTER TABLE `clone_government_persons` ADD `home_status` varchar(200) DEFAULT NULL COMMENT 'حالة المنزل - حسب لجان الزكاة ' AFTER `furniture_type`;
ALTER TABLE `clone_government_persons` ADD `home_description` longtext DEFAULT NULL COMMENT 'وصف عن المنزل - حسب لجان الزكاة ' AFTER `home_status`;
ALTER TABLE `clone_government_persons` ADD `total_family_income` varchar(200) DEFAULT NULL COMMENT 'دخل الأسرة - حسب لجان الزكاة ' AFTER `home_description`;
ALTER TABLE `clone_government_persons` ADD `health_status` varchar(200) DEFAULT NULL COMMENT 'الحالة الصحية - حسب لجان الزكاة ' AFTER `total_family_income`;
ALTER TABLE `clone_government_persons` ADD `wife_mobile` varchar(200) DEFAULT NULL COMMENT 'جوال الزوجة - حسب لجان الزكاة ' AFTER `health_status`;
ALTER TABLE `clone_government_persons` ADD `affected_by_wars` varchar(200) DEFAULT NULL COMMENT 'متضرر من الحرب - حسب لجان الزكاة ' AFTER `wife_mobile`;

CREATE TABLE `clone_government_persons_relations` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية الطرف الأول ف العلاقة',
  `IDNO_RELATIVE` int(11) DEFAULT NULL COMMENT 'رقم هوية الطرف الثاني ف العلاقة',
  `RELATIVE` varchar(200) DEFAULT NULL COMMENT 'مسمى العلاقة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول صلة القرابة بين الاشخاص';

CREATE TABLE `clone_government_persons_commercial_records` (
  `IDNO` int(11) DEFAULT NULL,
  `COMP_NAME` varchar(255) DEFAULT NULL COMMENT 'اسم السجل ',
  `REC_CODE` varchar(255) DEFAULT NULL COMMENT 'رقم السجل التجاري',
  `PERSON_TYPE_DESC` varchar(255) DEFAULT NULL COMMENT 'صفة الشخص في السجل التجاري',
  `IS_VALID_DESC` varchar(255) DEFAULT NULL COMMENT 'فعالية رقم السجل التجاري',
  `STATUS_ID_DESC` varchar(255) DEFAULT NULL COMMENT 'حالة الشخص ضمن السجل التجاري',
  `COMP_TYPE_DESC` varchar(255) DEFAULT NULL COMMENT 'نوع الشركة',
  `REC_TYPE_DESC` varchar(255) DEFAULT NULL COMMENT 'نوع  السجل التجاري',
  `REGISTER_NO` varchar(255) DEFAULT NULL COMMENT 'رقم مشتغل مرخص/الشركة',
  `START_DATE` varchar(255) DEFAULT NULL COMMENT 'تاريخ بداية العمل',
  `WORK_CLASS_DESC` varchar(255) DEFAULT NULL COMMENT ' تصنيف العمل',
  `BRAND_NAME` varchar(255) DEFAULT NULL COMMENT 'السمة التجارية ',
  `CITY_DESC` varchar(255) DEFAULT NULL COMMENT 'المدينة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول السجلات التجارية ';

CREATE TABLE `clone_government_persons_works` (
  `IDNO` int(11) DEFAULT NULL,
  `EMP_DOC_ID` varchar(255) DEFAULT NULL COMMENT   'رقم الملف',
  `MINISTRY_NAME` varchar(255) DEFAULT NULL COMMENT   'اسم المؤسسة او الوزارة',
  `JOB_START_DT` varchar(255) DEFAULT NULL COMMENT   'تاريخ بداية العمل',
  `DEGREE_NAME` varchar(255) DEFAULT NULL COMMENT   'الدرجة',
  `EMP_STATE_DESC` varchar(255) DEFAULT NULL COMMENT   'حالة  للموظف',
  `EMP_WORK_STATUS` varchar(255) DEFAULT NULL COMMENT   'حالة العمل للموظف',
  `JOB_DESC` varchar(255) DEFAULT NULL COMMENT   'الوصف الوظيفي',
  `MIN_DATA_SOURCE` varchar(255) DEFAULT NULL COMMENT   'مصدر البيانات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول عقود العمل ';

CREATE TABLE `clone_government_persons_medical_reports` (
  `IDNO` int(11) DEFAULT NULL,
  `MR_CODE` varchar(255) DEFAULT NULL COMMENT   'رقم الملف الطبي',
  `LOC_NAME_AR` varchar(255) DEFAULT NULL COMMENT   'القسم',
  `MR_PATIENT_CD` varchar(255) DEFAULT NULL COMMENT   'رقم التقرير',
  `DOCTOR_NAME` varchar(255) DEFAULT NULL COMMENT   'الطبيب',
  `MR_CREATED_ON` longtext DEFAULT NULL COMMENT   'تاريخ اصدار التقرير',
  `DREF_NAME_AR` longtext DEFAULT NULL COMMENT   'المستشفى',
  `MR_DIAGNOSIS_AR` longtext DEFAULT NULL COMMENT   'التشخيص EN',
  `MR_DIAGNOSIS_EN` longtext DEFAULT NULL COMMENT   'التشخيص',
  `MR_COMPLAINT` longtext DEFAULT NULL COMMENT   'الشكوى',
  `MR_EXAMINATION` longtext DEFAULT NULL COMMENT   'الفحصوصات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول التقارير الطبية ';

CREATE TABLE `clone_government_persons_travel` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية المسافر - مفتاح الربط ',
  `CTZN_TRANS_DT` varchar(200) DEFAULT NULL COMMENT 'تاريخ الحركة',
  `CTZN_TRANS_TYPE` varchar(200) DEFAULT NULL COMMENT 'نوع الحركة',
  `CTZN_TRANS_BORDER` varchar(200) DEFAULT NULL COMMENT 'الممر الذي تمت من خلاله الحركة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول التنقلات ( حركات السفر )';

CREATE TABLE `clone_government_persons_vehicles` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية المسافر - مفتاح الربط ',
  `MODEL_YEAR` varchar(200) DEFAULT NULL COMMENT 'سنة الموديل',
  `OWNER_TYPE_NAME` varchar(200) DEFAULT NULL COMMENT 'نوع الملكية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول المركبات';

CREATE TABLE `clone_government_persons_properties` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية المالك - مفتاح الربط ',
  `DOC_NO` varchar(200) DEFAULT NULL COMMENT 'رقم الملف',
  `REAL_OWNER_AREA` varchar(200) DEFAULT NULL COMMENT 'منطقة الملكية الحقيقية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول املاك الطابو';

CREATE TABLE `clone_government_persons_reg48_license` (
  `WORKER_ID` int(11) DEFAULT NULL COMMENT 'رقم هوية صاحب التصريح - مفتاح الربط ',
  `STATUS_NAME`  varchar(200) DEFAULT NULL COMMENT 'حالة التصريح',
  `DATE_FROM`  varchar(200) DEFAULT NULL COMMENT 'تاريخ بداية التصريح',
  `DATE_TO`  varchar(200) DEFAULT NULL COMMENT 'تاريخ انتهاء التصريح',
  `LICENSE_NAME`  varchar(200) DEFAULT NULL COMMENT 'تصنيف التصريح',
  `LIC_REC_STATUS_NAME`  varchar(200) DEFAULT NULL COMMENT 'حالة اصدار التصريح',
  `LIC_REC_DATE`  varchar(200) DEFAULT NULL COMMENT 'تاريخ اصدار التصريح'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' بيانات تصاريح العمل - مناطق 48 برقم الهوية';

CREATE TABLE `clone_government_persons_social_affairs_receipt` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية المستلم ',
  `SRV_INF_NAME`  varchar(200) DEFAULT NULL COMMENT ' اسم الخدمة المقدمة',
  `ORG_NM_MON`  varchar(200) DEFAULT NULL COMMENT ' نوع المنظمة المقدمة للخدمة',
  `SRV_TYPE_MAIN_NM`  varchar(200) DEFAULT NULL COMMENT 'نوع الخدمة',
  `CURRENCY`  varchar(200) DEFAULT NULL COMMENT 'العملة',
  `RECP_AID_AMOUNT`  varchar(200) DEFAULT NULL COMMENT 'المبلع ',
  `RECP_DELV_DT`  varchar(200) DEFAULT NULL COMMENT 'تاريخ الاستلام'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول استفادات الشئون الاجتماعية';

CREATE TABLE `clone_government_persons_marriage` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية صاحب العقود ',
  `CONTRACT_NO`  varchar(200) DEFAULT NULL COMMENT 'رقم عقد الزواج',
  `CONTRACT_DT`  varchar(200) DEFAULT NULL COMMENT  'تاريخ العقد',
  `HUSBAND_SSN`  varchar(200) DEFAULT NULL COMMENT 'رقم هوية  الزوج',
  `HUSBAND_NAME`  varchar(200) DEFAULT NULL COMMENT 'اسم الزوج',
  `WIFE_SSN`  varchar(200) DEFAULT NULL COMMENT 'رقم هوية الزوجة',
  `WIFE_NAME`  varchar(200) DEFAULT NULL COMMENT 'اسم الزوجة '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول عقود الزواج ';

CREATE TABLE `clone_government_persons_divorce` (
  `IDNO` int(11) DEFAULT NULL COMMENT 'رقم هوية صاحب العقود ',
  `DIV_CERTIFIED_NO`  varchar(200) DEFAULT NULL COMMENT 'رقم عقد الطلاق',
  `CONTRACT_DT`  varchar(200) DEFAULT NULL COMMENT 'تاريخ العقد',
  `DIV_TYPE_NAME`  varchar(200) DEFAULT NULL COMMENT 'نوع / تصنيف الطلاق',
  `HUSBAND_SSN`  varchar(200) DEFAULT NULL COMMENT 'رقم هوية  الزوج',
  `HUSBAND_NAME`  varchar(200) DEFAULT NULL COMMENT 'اسم الزوج',
  `WIFE_SSN`  varchar(200) DEFAULT NULL COMMENT 'رقم هوية الزوجة',
  `WIFE_NAME`  varchar(200) DEFAULT NULL COMMENT 'اسم الزوجة '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT=' جدول عقود الطلاق ';
