INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(521, 8, 'aid.voucher.center', 'اضافة القسائم المركزية');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 521, 1, CURRENT_TIMESTAMP, 1),
(2, 521, 1, CURRENT_TIMESTAMP, 1);

-- ------------------------------------------------------------------------------------------

ALTER TABLE `char_vouchers_persons` ADD `document_id` INT(10) UNSIGNED NULL  AFTER `receipt_date`;

DROP VIEW IF EXISTS `char_vouchers_persons_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view`  AS SELECT `char_vouchers_persons`.`id` AS `id`,`char_vouchers_persons`.`document_id` AS `document_id`, `char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'من مشروع' else 'يدوي' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'نقدية' when (`v`.`type` = 2) then 'عينية' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'عادية' when (`v`.`urgent` = 1) then 'عاجلة' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'مباشر' when (`v`.`transfer` = 1) then 'تصنيف جمعية' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'ذكر ' when (`c`.`gender` = 2) then 'أنثى' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'ذكر ' when (`ind`.`gender` = 2) then 'أنثى' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 0) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 2) then ' لم يتم الاستلام' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 1)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 1)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 1)))) ;

-- --------------------------------------------------------
DROP VIEW IF EXISTS `char_vouchers_persons_view_en`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view_en`  AS SELECT `char_vouchers_persons`.`id` AS `id`, `char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'from_project' else 'manually' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'financial' when (`v`.`type` = 2) then 'non_financial' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'not_urgent' when (`v`.`urgent` = 1) then 'urgent' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'direct' when (`v`.`transfer` = 1) then 'transfer_company' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'male' when (`c`.`gender` = 2) then 'female' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'male' when (`ind`.`gender` = 2) then 'female' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'receipt' when (`char_vouchers_persons`.`status` = 0) then 'receipt' when (`char_vouchers_persons`.`status` = 2) then 'not_receipt' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 2)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 2)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 2)))) ;

-- ---------------------------------------------------------------------------------------
-- ---------------------------2/4/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------

ALTER TABLE `aid_projects` ADD `date_from` DATE NULL DEFAULT NULL AFTER `date`, ADD `date_to` DATE NULL DEFAULT NULL AFTER `date_from`;
ALTER TABLE `aid_projects` ADD `serial` VARCHAR(255) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `char_vouchers` ADD `serial` VARCHAR(255) NULL DEFAULT NULL AFTER `id`;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(522, 8, 'aidRepository.repositories.manageCandidates', 'تدقيق مستفيدي المشاريع');


INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 522, 1, CURRENT_TIMESTAMP, 1),
(2, 522, 1, CURRENT_TIMESTAMP, 1);


-- ---------------------------------------------------------------------------------------
-- ---------------------------15/5/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------


DROP VIEW IF EXISTS `char_vouchers_persons_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view`  AS SELECT `char_vouchers_persons`.`id` AS `id`,`char_vouchers_persons`.`document_id` AS `document_id`, `char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`, `v`.`serial` AS `serial`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'من مشروع' else 'يدوي' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'نقدية' when (`v`.`type` = 2) then 'عينية' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'عادية' when (`v`.`urgent` = 1) then 'عاجلة' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'مباشر' when (`v`.`transfer` = 1) then 'تصنيف جمعية' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'ذكر ' when (`c`.`gender` = 2) then 'أنثى' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'ذكر ' when (`ind`.`gender` = 2) then 'أنثى' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 0) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 2) then ' لم يتم الاستلام' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 1)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 1)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 1)))) ;

-- --------------------------------------------------------
DROP VIEW IF EXISTS `char_vouchers_persons_view_en`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view_en`  AS SELECT `char_vouchers_persons`.`id` AS `id`, `char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`,  `v`.`serial` AS `serial`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'from_project' else 'manually' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'financial' when (`v`.`type` = 2) then 'non_financial' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'not_urgent' when (`v`.`urgent` = 1) then 'urgent' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'direct' when (`v`.`transfer` = 1) then 'transfer_company' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'male' when (`c`.`gender` = 2) then 'female' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'male' when (`ind`.`gender` = 2) then 'female' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'receipt' when (`char_vouchers_persons`.`status` = 0) then 'receipt' when (`char_vouchers_persons`.`status` = 2) then 'not_receipt' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 2)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 2)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 2)))) ;


-- ---------------------------------------------------------------------------------------
-- ---------------------------1/6/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------

ALTER TABLE `char_organizations` ADD `category_id` INT(10) UNSIGNED NULL AFTER `type`;

CREATE TABLE `char_organizations_category` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على تصنيفات الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_organizations_category_i18n` (
  `category_id` int(10) UNSIGNED NOT NULL COMMENT 'تصنيف الجمعيات',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات تصنيفات الجمعيات وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_organizations_category`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_organizations_category_i18n`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `fk_char_organizations_category_i18n_2_idx` (`language_id`);

ALTER TABLE `char_organizations_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_organizations_category_i18n`
  ADD CONSTRAINT `fk_char_organizations_category_i18n_1` FOREIGN KEY (`category_id`) REFERENCES `char_organizations_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_char_organizations_category_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

INSERT INTO `char_organizations_category` (`id`) VALUES (1),(2);
INSERT INTO `char_organizations_category_i18n` (`category_id`, `language_id`, `name`) VALUES
(1, 1, 'ممثلة'),
(2, 1, 'غير ممثلة');

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (523, '7', 'setting.OrganizationsCategory.manage', 'إدارة تصنيفات الجمعيات'),
 (524, '7', 'setting.OrganizationsCategory.create', 'إنشاء تصنيف جمعية جديد'),
 (525, '7', 'setting.OrganizationsCategory.update', 'تحرير بيانات تصنيف جمعية'),
 (526, '7', 'setting.OrganizationsCategory.delete', 'حذف تصنيف جمعية'),
 (527, '7', 'setting.OrganizationsCategory.view', 'عرض بيانات تصنيف جمعية');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 523, 1, CURRENT_TIMESTAMP, 1),
(1, 524, 1, CURRENT_TIMESTAMP, 1),
(1, 525, 1, CURRENT_TIMESTAMP, 1),
(1, 526, 1, CURRENT_TIMESTAMP, 1),
(2, 527, 1, CURRENT_TIMESTAMP, 1);

-- ---------------------------------------------------------------------------------------
-- ---------------------------4/6/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(528 , '10', 'reports.transfers.manage', 'إدارة طلبات النقل'),
(529 , '10', 'reports.transfers.manageConfirm', 'إدارة طلبات النقل المعتمدة'),
(530 , '10', 'reports.transfers.confirm', 'اعتماد طلبات النقل');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 528, 1, CURRENT_TIMESTAMP, 1),
(2, 528, 1, CURRENT_TIMESTAMP, 1),
(1, 529, 1, CURRENT_TIMESTAMP, 1),
(2, 529, 1, CURRENT_TIMESTAMP, 1),
(1, 530, 1, CURRENT_TIMESTAMP, 1),
(2, 530, 1, CURRENT_TIMESTAMP, 1);

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
SELECT id as role_id , 528 as permission_id , 1 as allow , CURRENT_TIMESTAMP as  created_at , user_id as created_by
FROM `char_roles` WHERE name LIKE '%مدير جمعي%' or name LIKE '%نائب مدير%' ;

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
SELECT id as role_id , 529 as permission_id , 1 as allow , CURRENT_TIMESTAMP as  created_at , user_id as created_by
FROM `char_roles` WHERE name LIKE '%مدير جمعي%' or name LIKE '%نائب مدير%' ;

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
SELECT id as role_id , 530 as permission_id , 1 as allow , CURRENT_TIMESTAMP as  created_at , user_id as created_by
FROM `char_roles` WHERE name LIKE '%مدير جمعي%' or name LIKE '%نائب مدير%' ;
-- ---------------------------------------------------------------------------------------------------------------------
CREATE TABLE `char_transfers` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لطلبات النقل الناتجة عن التغيير في شجرة عناوين المساعدات';

ALTER TABLE `char_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transfers_personId_idx` (`person_id`),
  ADD KEY `fk_transfers_organizationId_idx` (`organization_id`),
  ADD KEY `fk_transfers_userId_idx` (`user_id`),
  ADD KEY `fk_transfers_categoryId_idx` (`category_id`);

ALTER TABLE `char_transfers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `char_transfers`
  ADD CONSTRAINT `fk_transfers_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_transfers_personId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_transfers_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE TABLE `char_transfers_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `transfer_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط الاشخاص المنقولين مع الجمعيات المعتمدة للنقل';

ALTER TABLE `char_transfers_log` ADD `status` TINYINT NULL DEFAULT NULL AFTER `user_id`;
ALTER TABLE `char_transfers_log`  ADD `reason` text NULL DEFAULT NULL  AFTER `status`;
ALTER TABLE `char_transfers_log` ADD `confirm` TINYINT NULL DEFAULT NULL AFTER `reason`;
ALTER TABLE `char_transfers_log` ADD `confirm_date` Date NULL DEFAULT NULL AFTER `confirm`;
ALTER TABLE `char_transfers_log` ADD `confirm_user_id` int(10) UNSIGNED NULL DEFAULT NULL AFTER `confirm_date`;
ALTER TABLE `char_transfers_log`  ADD `confirm_reason` text NULL DEFAULT NULL  AFTER `confirm_user_id`;

ALTER TABLE `char_transfers_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_transfers_log_transferId_idx` (`transfer_id`),
  ADD KEY `fk_transfers_log_organizationId_idx` (`organization_id`),
  ADD KEY `fk_transfers_log_userId_idx` (`user_id`);

ALTER TABLE `char_transfers_log` MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `char_transfers_log`
  ADD CONSTRAINT `fk_transfers_log_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_transfers_log_transferId` FOREIGN KEY (`transfer_id`) REFERENCES `char_transfers` (`id`),
  ADD CONSTRAINT `fk_transfers_log_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_transfers_view`  AS
SELECT `char_categories`.`type` AS `category_type`, concat(ifnull(`char_users`.`firstname`,' '),' ',ifnull(`char_users`.`lastname`,' ')) AS `user_name`,
`char_persons`.`id_card_number` AS `id_card_number`,
concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',ifnull(`char_persons`.`third_name`,' '),' ',
ifnull(`char_persons`.`last_name`,' ')) AS `full_name`, ifnull(`mobno`.`contact_value`,'-') AS `primary_mobile`, ifnull(`phono`.`contact_value`,'-') AS `phone`,
`char_persons`.`first_name` AS `first_name`, `char_persons`.`second_name` AS `second_name`, `char_persons`.`third_name` AS `third_name`,
`char_persons`.`last_name` AS `last_name`, `char_persons`.`prev_family_name` AS `prev_family_name`, `char_persons`.`gender` AS `gender`,
`char_persons`.`marital_status_id` AS `marital_status_id`, `char_persons`.`birthday` AS `birthday`,
`char_persons`.`country` AS `country`, `char_persons`.`governarate` AS `governarate`, `char_persons`.`city` AS `city`,
`char_persons`.`location_id` AS `location_id`, `char_persons`.`mosques_id` AS `mosques_id`,
`char_persons`.`street_address` AS `street_address`, `char_persons`.`adscountry_id` AS `adscountry_id`,
`char_persons`.`adsdistrict_id` AS `adsdistrict_id`, `char_persons`.`adsregion_id` AS `adsregion_id`,
`char_persons`.`adsneighborhood_id` AS `adsneighborhood_id`, `char_persons`.`adssquare_id` AS `adssquare_id`,
`char_persons`.`adsmosques_id` AS `adsmosques_id`, `char_transfers`.`id` AS `id`, `char_transfers`.`person_id` AS `person_id`,
`char_transfers`.`organization_id` AS `transfer_organization_id`, `char_transfers`.`category_id` AS `category_id`,
`char_transfers`.`user_id` AS `user_id`, `char_transfers`.`created_at` AS `created_at`, `char_transfers`.`updated_at` AS `updated_at`,
 `char_transfers`.`deleted_at` AS `deleted_at`, `char_categories`.`name` AS `category_name`
  FROM ((((((
  `char_transfers`
   join `char_categories` on((`char_categories`.`id` = `char_transfers`.`category_id`)))
    join `char_persons` on((`char_persons`.`id` = `char_transfers`.`person_id`)))
    left join `char_persons_contact` `mobno` on(((`mobno`.`person_id` = `char_transfers`.`person_id`) and (`mobno`.`contact_type` = 'primary_mobile'))))
    left join `char_persons_contact` `phono` on(((`phono`.`person_id` = `char_transfers`.`person_id`) and (`phono`.`contact_type` = 'phone'))))
    join `char_organizations` on((`char_organizations`.`id` = `char_transfers`.`organization_id`)))
    join `char_users` on((`char_users`.`id` = `char_transfers`.`user_id`))) ORDER BY `char_transfers`.`created_at` DESC ;



-- ---------------------------------------------------------------------------------------
-- ---------------------------10/6/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------

ALTER TABLE `aid_projects` ADD `organization_quantity` FLOAT NULL DEFAULT '0' AFTER `organization_ratio`;
ALTER TABLE `aid_projects` ADD `custom_quantity` FLOAT NULL DEFAULT '0' AFTER `custom_organization`;

UPDATE aid_projects SET organization_quantity = (organization_ratio * quantity)/100  WHERE organization_ratio > 0 ;
UPDATE aid_projects SET custom_quantity = (custom_ratio * quantity)/100  WHERE custom_ratio > 0

-- ---------------------------------------------------------------------------
-- ---------------------------- 5/7/2022 -----------------------------------------------

ALTER TABLE `char_transfers`
ADD `adscountry_id` INT(10) NULL DEFAULT NULL  AFTER `person_id`,
ADD `adsdistrict_id` INT(10) NULL DEFAULT NULL  AFTER `adscountry_id`,
ADD `adsregion_id` INT(10) NULL DEFAULT NULL  AFTER `adsdistrict_id`,
ADD `adsneighborhood_id` INT(10) NULL DEFAULT NULL  AFTER `adsregion_id`,
ADD `adssquare_id` INT(10) NULL DEFAULT NULL  AFTER `adsneighborhood_id`,
ADD `adsmosques_id` INT(10) NULL DEFAULT NULL  AFTER `adssquare_id`;

DROP VIEW IF EXISTS `char_transfers_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_transfers_view`  AS
SELECT `char_categories`.`type` AS `category_type`, concat(ifnull(`char_users`.`firstname`,' '),' ',ifnull(`char_users`.`lastname`,' ')) AS `user_name`,
`char_persons`.`id_card_number` AS `id_card_number`,
concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',ifnull(`char_persons`.`third_name`,' '),' ',
ifnull(`char_persons`.`last_name`,' ')) AS `full_name`, ifnull(`mobno`.`contact_value`,'-') AS `primary_mobile`, ifnull(`phono`.`contact_value`,'-') AS `phone`,
`char_persons`.`first_name` AS `first_name`, `char_persons`.`second_name` AS `second_name`, `char_persons`.`third_name` AS `third_name`,
`char_persons`.`last_name` AS `last_name`, `char_persons`.`prev_family_name` AS `prev_family_name`, `char_persons`.`gender` AS `gender`,
`char_persons`.`marital_status_id` AS `marital_status_id`, `char_persons`.`birthday` AS `birthday`,
`char_persons`.`country` AS `country`, `char_persons`.`governarate` AS `governarate`, `char_persons`.`city` AS `city`,
`char_persons`.`location_id` AS `location_id`, `char_persons`.`mosques_id` AS `mosques_id`,
`char_persons`.`street_address` AS `street_address`, `char_transfers`.`adscountry_id` AS `adscountry_id`,
`char_transfers`.`adsdistrict_id` AS `adsdistrict_id`, `char_transfers`.`adsregion_id` AS `adsregion_id`,
`char_transfers`.`adsneighborhood_id` AS `adsneighborhood_id`, `char_transfers`.`adssquare_id` AS `adssquare_id`,
`char_transfers`.`adsmosques_id` AS `adsmosques_id`, `char_transfers`.`id` AS `id`, `char_transfers`.`person_id` AS `person_id`,
`char_transfers`.`organization_id` AS `transfer_organization_id`, `char_transfers`.`category_id` AS `category_id`,
`char_transfers`.`user_id` AS `user_id`, `char_transfers`.`created_at` AS `created_at`, `char_transfers`.`updated_at` AS `updated_at`,
 `char_transfers`.`deleted_at` AS `deleted_at`,`char_transfers`.`organization_id`, `char_organizations`.`name` AS `organization_name`, `char_categories`.`name` AS `category_name`
  FROM ((((((
  `char_transfers`
   join `char_categories` on((`char_categories`.`id` = `char_transfers`.`category_id`)))
    join `char_persons` on((`char_persons`.`id` = `char_transfers`.`person_id`)))
    left join `char_persons_contact` `mobno` on(((`mobno`.`person_id` = `char_transfers`.`person_id`) and (`mobno`.`contact_type` = 'primary_mobile'))))
    left join `char_persons_contact` `phono` on(((`phono`.`person_id` = `char_transfers`.`person_id`) and (`phono`.`contact_type` = 'phone'))))
    join `char_organizations` on((`char_organizations`.`id` = `char_transfers`.`organization_id`)))
    join `char_users` on((`char_users`.`id` = `char_transfers`.`user_id`))) ORDER BY `char_transfers`.`created_at` DESC ;


-- ---------------------------------------------------------------------------------------
-- ---------------------------16/7/2022 ---------------------------------------------------
-- ---------------------------------------------------------------------------------------
DROP VIEW IF EXISTS `char_active_aid_cases_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_active_aid_cases_view`  AS
select
char_cases.organization_id ,
char_categories.name AS 'نوع المساعدة',
org.name as 'اسم الجمعية',
char_organizations_category_i18n.name AS 'تصنيف الجمعية',
CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS 'رقم الهوية',
CASE WHEN char_persons.card_type is null THEN ' '  WHEN char_persons.card_type = 1 THEN 'رقم الهوية '
     WHEN char_persons.card_type = 2 THEN 'بطاقة التعريف '     WHEN char_persons.card_type = 3 THEN 'جواز السفر '
END AS 'نوع البطاقة' ,
ifnull(char_persons.first_name, ' ') AS 'الاسم الاول (عربي)' ,
ifnull(char_persons.second_name, ' ') AS 'الاسم الثاني (عربي)',
ifnull(char_persons.third_name, ' ') AS 'الاسم الثالث (عربي)',
ifnull(char_persons.last_name,' ')  AS 'العائلة (عربي)',
ifnull(char_persons_i18n.first_name, ' ') AS 'الاسم الأول (انجليزي)' ,
ifnull(char_persons_i18n.second_name, ' ') AS'الاسم الثاني (انجليزي)',
ifnull(char_persons_i18n.third_name, ' ') AS'الاسم الثالث (انجليزي)',
ifnull(char_persons_i18n.last_name,' ') AS 'العائلة (انجليزي)',

CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS 'تاريخ الميلاد',
CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS 'مكان الميلاد',
CASE WHEN char_persons.gender is null THEN ' '   WHEN char_persons.gender = 1 THEN 'ذكر '  WHEN char_persons.gender = 2 THEN 'أنثى'    END    AS 'الجنس',
CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS 'الجنسية',
char_persons.spouses AS 'عدد الزوجات',
CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS 'الحالة الاجتماعية' ,
CASE WHEN char_persons.deserted is null THEN ' ' WHEN char_persons.deserted = 1 THEN 'نعم'  WHEN char_persons.deserted = 0 THEN ' لا' END AS 'مهجورة',

CASE  WHEN char_persons.is_qualified is null THEN '-'
      WHEN char_persons.is_qualified = 0 THEN 'غير مؤهل ' WHEN char_persons.is_qualified = 1 THEN ' مؤهل '        END       AS 'أهلية رب الأسرة',
CASE WHEN char_persons.qualified_card_number is null THEN '-' Else char_persons.qualified_card_number  END  AS 'رقم المعييل',

CASE WHEN char_persons.refugee is null THEN ' ' WHEN char_persons.refugee = 0 THEN ' '
     WHEN char_persons.refugee = 1 THEN ' مواطن '  WHEN char_persons.refugee = 2 THEN 'لاجيء'
END AS 'حالة المواطنة',
CASE WHEN char_persons.refugee is null THEN ' ' WHEN char_persons.refugee = 0 THEN ' '
     WHEN char_persons.refugee = 1 THEN ' ' WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
END  AS 'رقم كرت التموين',

char_persons.family_cnt AS family_count,
char_persons.male_live AS male_count,
char_persons.female_live AS female_count,

CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS 'الدولة',
CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS 'المحافظة' ,
CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS 'المنطقة',
CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS 'الحي',
CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS 'المربع',
CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS 'المسجد',
CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS 'تفاصيل العنوان',

CASE WHEN primary_num.contact_value is null THEN ' ' Else primary_num.contact_value  END  AS 'رقم جوال اساسي',
CASE WHEN secondary_mob.contact_value is null THEN ' ' Else secondary_mob.contact_value  END  AS 'رقم جوال احتياطي',
CASE WHEN wataniya_num.contact_value is null THEN ' ' Else wataniya_num.contact_value  END  AS 'رقم الوطنية' ,
CASE WHEN phone_num.contact_value is null THEN ' ' Else phone_num.contact_value  END  AS 'رقم هاتف' ,
'' AS 'اسم البنك','' AS 'الفرع','' AS 'اسم صاحب الحساب','' AS 'رقم الحساب',

CASE WHEN char_persons_work.working is null THEN ' '  WHEN char_persons_work.working = 1 THEN 'نعم' WHEN char_persons_work.working = 2 THEN ' لا' END AS 'هل يعمل',

CASE  WHEN char_persons_work.can_work is null THEN ' '   WHEN char_persons_work.can_work = 1 THEN 'نعم'  WHEN char_persons_work.can_work = 2 THEN ' لا'    END    AS 'قادر على العمل',
CASE  WHEN char_persons_work.can_work is null THEN ' '   WHEN char_persons_work.can_work = 1 THEN ' '   WHEN char_persons_work.can_work = 2 THEN work_reason.name    END    AS 'سبب عدم القدرة',

CASE WHEN char_persons_work.working is null THEN ' '  WHEN char_persons_work.working = 1 THEN work_status.name WHEN char_persons_work.working = 2 THEN ' '    END    AS 'حالة العمل',

CASE WHEN char_persons_work.working is null THEN ' '  WHEN char_persons_work.working = 1 THEN work_job.name WHEN char_persons_work.working = 2 THEN ' '    END    AS 'الوظيفة',
CASE  WHEN char_persons_work.working is null THEN ' '   WHEN char_persons_work.working = 1 THEN char_persons_work.work_location   WHEN char_persons_work.working = 2 THEN ' '    END
AS 'مكان العمل',
CASE  WHEN char_persons_work.working is null THEN ' '   WHEN char_persons_work.working = 1 THEN work_wage.name WHEN char_persons_work.working = 2 THEN ' ' END  AS 'فئة الأجر',


CASE WHEN char_persons_work.has_other_work_resources is null THEN ' '
     WHEN char_persons_work.has_other_work_resources = 1 THEN 'نعم'         WHEN char_persons_work.has_other_work_resources = 0 THEN ' لا'    END AS 'هل لديه مصادر دخل أخرى',

CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS 'الدخل الشهري / شيكل',
CASE WHEN char_persons.actual_monthly_income is null THEN ' ' Else char_persons.actual_monthly_income  END  AS 'الدخل الشهري الفعلي / شيكل',

CASE WHEN char_persons.receivables = 1 THEN 'نعم' Else ' لا' END AS 'هل لديه ذمم مالية',
CASE WHEN char_persons.receivables is null THEN '0'     WHEN char_persons.receivables = 0 THEN '0'
       WHEN char_persons.receivables = 1 THEN char_persons.receivables_sk_amount END AS 'قيمة الذمم بالشيكل' ,

CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS 'ملكية المنزل',
CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS 'سقف المنزل',
CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS 'قابلية المنزل للسكن',

CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS 'وضع المنزل',
CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS 'عدد الغرف',

CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS 'حالة الأثاث',
CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS 'حالة المسكن',
CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS 'المساحة بالمتر',
CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS 'قيمة الايجار',

CASE WHEN char_residence.need_repair is null THEN ' '  WHEN char_residence.need_repair = 1 THEN 'نعم' WHEN char_residence.need_repair = 2 THEN ' لا'       END AS 'هل بحاجة لترميم للموائمة' ,
CASE WHEN char_residence.repair_notes is null   THEN ' ' Else char_residence.repair_notes  END  AS 'وصف وضع المنزل',

     CASE  WHEN char_persons_health.has_health_insurance is null THEN ' '
            WHEN char_persons_health.has_health_insurance = 1 THEN 'نعم'
            WHEN char_persons_health.has_health_insurance = 0 THEN ' لا'
       END  AS 'هل لديه تأمين صحي',
     CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' Else char_persons_health.health_insurance_type END   AS 'نوع التأمين الصحي',
     CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' Else char_persons_health.health_insurance_number END   AS 'رقم التأمين الصحي',
     CASE  WHEN char_persons_health.has_device is null THEN ' '    WHEN char_persons_health.has_device = 1 THEN 'نعم'
           WHEN char_persons_health.has_device = 0 THEN ' لا'   END  AS 'هل يستخدم جهاز',
     CASE WHEN char_persons_health.used_device_name is null THEN ' ' Else char_persons_health.used_device_name END   AS 'اسم الجهاز المستخدم',
 CASE WHEN char_persons_health.condition is null THEN ' '
          WHEN char_persons_health.condition = 1 THEN 'جيدة'
          WHEN char_persons_health.condition = 2 THEN 'يعاني من مرض مزمن'
          WHEN char_persons_health.condition = 3 THEN 'ذوي الاحتياجات الخاصة'
      END  AS 'الحالة الصحية',
      CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS 'المرض أو الاعاقة',
      CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS 'تفاصيل الحالة الصحية',

CASE  WHEN char_reconstructions.promised is null THEN ' ' WHEN char_reconstructions.promised = 1 THEN ' لا' WHEN char_reconstructions.promised = 2 THEN 'نعم'   END   AS 'هل وعدت بالبناء من قبل وكالة الغوث أو جهة أخرى',
CASE  WHEN char_reconstructions.promised = 1 THEN ' ' WHEN char_reconstructions.promised is null THEN ' '  WHEN char_reconstructions.promised = 2 THEN char_reconstructions.organization_name
     END  AS 'اسم المؤسسة التي وعدت بالبناء',
     CASE WHEN char_case_needs.needs is null       THEN ' ' Else char_case_needs.needs  END  AS 'احتياجات العائلة بشدّة' ,
    CASE WHEN char_cases.visitor is null THEN ' ' Else char_cases.visitor END AS 'اسم الباحث الاجتماعي',
     CASE WHEN char_cases.visitor_card is null THEN ' ' Else char_cases.visitor_card END AS 'رقم هوية الباحث',
     CASE WHEN char_cases.notes is null THEN ' ' Else char_cases.notes END AS 'توصيات الباحث الاجتماعي',
     CASE WHEN char_cases.visitor_opinion is null THEN ' ' Else char_cases.visitor_opinion END AS 'رأي الباحث الاجتماعي',
     CASE WHEN char_cases.visitor_evaluation is null THEN ' ' Else char_cases.visitor_evaluation END AS 'تقييم الباحث لحالة المنزل',
 CASE WHEN char_cases.visited_at is null THEN ' ' Else char_cases.visited_at END AS 'تاريخ الزيارة'

from `char_cases`
join `char_organizations` as `org` on `org`.`id` = `char_cases`.`organization_id`
join `char_categories` on `char_categories`.`id` = `char_cases`.`category_id` and `char_categories`.`type` = 2
left join `char_organizations_category_i18n` on `char_organizations_category_i18n`.`category_id` = `org`.`category_id` and `char_organizations_category_i18n`.`language_id` = 1
join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
left join `char_marital_status_i18n` as `char_marital_status` on `char_marital_status`.`marital_status_id` = `char_persons`.`marital_status_id` and `char_marital_status`.`language_id` = 1
left join `char_aids_locations_i18n` as `district_name` on `char_persons`.`adsdistrict_id` = `district_name`.`location_id` and `district_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `region_name` on `char_persons`.`adsregion_id` = `region_name`.`location_id` and `region_name`.`language_id` = 1
left join `char_persons_health` on `char_persons_health`.`person_id` = `char_persons`.`id`
left join `char_diseases_i18n` on `char_diseases_i18n`.`disease_id` = `char_persons_health`.`disease_id` and `char_diseases_i18n`.`language_id` = 1
left join `char_aids_locations_i18n` as `country_name` on `char_persons`.`adscountry_id` = `country_name`.`location_id` and `country_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `location_name` on `char_persons`.`adsneighborhood_id` = `location_name`.`location_id` and `location_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `square_name` on `char_persons`.`adssquare_id` = `square_name`.`location_id` and `square_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `mosques_name` on `char_persons`.`adsmosques_id` = `mosques_name`.`location_id` and `mosques_name`.`language_id` = 1
left join `char_locations_i18n` as `L` on `L`.`location_id` = `char_persons`.`birth_place` and `L`.`language_id` = 1
left join `char_locations_i18n` as `L0` on `L0`.`location_id` = `char_persons`.`nationality` and `L0`.`language_id` = 1
left join `char_persons_i18n` on `char_persons_i18n`.`person_id` = `char_cases`.`person_id`
left join `char_persons_contact` as `wataniya_num` on `wataniya_num`.`person_id` = `char_persons`.`id` and `wataniya_num`.`contact_type` = 'wataniya'
left join `char_persons_contact` as `phone_num` on `phone_num`.`person_id` = `char_persons`.`id` and `phone_num`.`contact_type` = 'phone'
left join `char_persons_contact` as `primary_num` on `primary_num`.`person_id` = `char_persons`.`id` and `primary_num`.`contact_type` = 'primary_mobile'
left join `char_persons_contact` as `secondary_mob` on `secondary_mob`.`person_id` = `char_persons`.`id` and `secondary_mob`.`contact_type` = 'secondary_mobile'
left join `char_reconstructions` on `char_reconstructions`.`case_id` = `char_cases`.`id`
left join `char_case_needs` on `char_case_needs`.`case_id` = `char_cases`.`id`
left join `char_residence` on `char_residence`.`person_id` = `char_persons`.`id`
left join `char_property_types_i18n` on `char_property_types_i18n`.`property_type_id` = `char_residence`.`property_type_id` and `char_property_types_i18n`.`language_id` = 1
left join `char_roof_materials_i18n` on `char_roof_materials_i18n`.`roof_material_id` = `char_residence`.`roof_material_id` and `char_roof_materials_i18n`.`language_id` = 1
left join `char_building_status_i18n` on `char_building_status_i18n`.`building_status_id` = `char_residence`.`house_condition` and `char_building_status_i18n`.`language_id` = 1
left join `char_furniture_status_i18n` on `char_furniture_status_i18n`.`furniture_status_id` = `char_residence`.`indoor_condition` and `char_furniture_status_i18n`.`language_id` = 1
left join `char_habitable_status_i18n` on `char_habitable_status_i18n`.`habitable_status_id` = `char_residence`.`habitable` and `char_habitable_status_i18n`.`language_id` = 1
left join `char_house_status_i18n` on `char_house_status_i18n`.`house_status_id` = `char_residence`.`residence_condition` and `char_house_status_i18n`.`language_id` = 1
left join `char_persons_work` on `char_persons_work`.`person_id` = `char_persons`.`id`
left join `char_work_jobs_i18n` as `work_job` on `work_job`.`work_job_id` = `char_persons_work`.`work_job_id` and `work_job`.`language_id` = 1
left join `char_work_wages` as `work_wage` on `work_wage`.`id` = `char_persons_work`.`work_wage_id`
left join `char_work_status_i18n` as `work_status` on `work_status`.`work_status_id` = `char_persons_work`.`work_status_id` and `work_status`.`language_id` = 1
left join `char_work_reasons_i18n` as `work_reason` on `work_reason`.`work_reason_id` = `char_persons_work`.`work_reason_id` and `work_reason`.`language_id` = 1
where `char_cases`.`deleted_at` is null and `char_cases`.`status` = 0;
-- order by char_persons.id_card_number desc

-- ----------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------

DROP VIEW IF EXISTS `char_active_cases_family_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_active_cases_family_view`  AS
select char_persons.id as person_id ,char_persons_kinship.r_person_id,
CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS 'رقم الهوية ( رب الأسرة)',
ifnull(char_persons.first_name, ' ') AS 'الاسم الاول  (عربي - رب الأسرة)' ,
ifnull(char_persons.second_name, ' ') AS 'الاسم الثاني  (عربي - رب الأسرة)',
ifnull(char_persons.third_name, ' ') AS 'الاسم الثالث  (عربي - رب الأسرة)',
ifnull(char_persons.last_name,' ')  AS 'العائلة      (عربي - رب الأسرة)' ,
char_kinship_i18n.name as 'صلة القرابة     (رب الأسرة)',
CASE WHEN r_person.id_card_number is null THEN ' ' Else r_person.id_card_number  END  AS 'رقم الهوية',
CASE WHEN r_person.card_type is null THEN ' '  WHEN r_person.card_type = 1 THEN 'رقم الهوية '
     WHEN r_person.card_type = 2 THEN 'بطاقة التعريف '     WHEN r_person.card_type = 3 THEN 'جواز السفر '
END AS 'نوع البطاقة' ,
ifnull(r_person.first_name, ' ') AS 'الاسم الاول (عربي)' ,
ifnull(r_person.second_name, ' ') AS 'الاسم الثاني (عربي)',
ifnull(r_person.third_name, ' ') AS 'الاسم الثالث (عربي)',
ifnull(r_person.last_name,' ')  AS 'العائلة (عربي)',
ifnull(char_persons_i18n.first_name, ' ') AS 'الاسم الأول (انجليزي)' ,
ifnull(char_persons_i18n.second_name, ' ') AS'الاسم الثاني (انجليزي)',
ifnull(char_persons_i18n.third_name, ' ') AS'الاسم الثالث (انجليزي)',
ifnull(char_persons_i18n.last_name,' ') AS 'العائلة (انجليزي)',

CASE WHEN r_person.birthday is null THEN ' ' Else DATE_FORMAT(r_person.birthday,'%Y/%m/%d')  END  AS 'تاريخ الميلاد',
CASE WHEN r_person.birth_place is null THEN ' ' Else L.name END AS 'مكان الميلاد',
CASE WHEN r_person.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS 'الحالة الاجتماعية' ,
CASE WHEN r_person.gender is null THEN ' '   WHEN r_person.gender = 1 THEN 'ذكر '  WHEN r_person.gender = 2 THEN 'أنثى'    END    AS 'الجنس',
CASE WHEN r_person.nationality is null THEN ' ' Else L0.name END AS 'الجنسية',

CASE WHEN char_persons_work.working is null THEN ' '  WHEN char_persons_work.working = 1 THEN 'نعم' WHEN char_persons_work.working = 2 THEN ' لا' END AS 'هل يعمل',
CASE WHEN char_persons_work.working is null THEN ' '  WHEN char_persons_work.working = 1 THEN work_job.name WHEN char_persons_work.working = 2 THEN ' '    END    AS 'الوظيفة',
CASE  WHEN char_persons_work.working is null THEN ' '   WHEN char_persons_work.working = 1 THEN char_persons_work.work_location   WHEN char_persons_work.working = 2 THEN ' '    END
AS 'مكان العمل',
CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS 'الدخل الشهري / شيكل',
 CASE WHEN char_persons_health.condition is null THEN ' '
          WHEN char_persons_health.condition = 1 THEN 'جيدة'
          WHEN char_persons_health.condition = 2 THEN 'يعاني من مرض مزمن'
          WHEN char_persons_health.condition = 3 THEN 'ذوي الاحتياجات الخاصة'
      END  AS 'الحالة الصحية',
      CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS 'المرض أو الاعاقة',
      CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS 'تفاصيل الحالة الصحية',
      CASE WHEN char_persons_education.study is null THEN ' ' WHEN char_persons_education.study = 1 THEN 'نعم'
           WHEN char_persons_education.study = 0 THEN 'لا' END  AS 'يدرس أو متعلم',
       CASE WHEN char_persons_education.type is null THEN ' ' WHEN char_persons_education.type = 1 THEN 'تقني'
            WHEN char_persons_education.type = 2 THEN 'اكاديمي'END AS 'نوع الدراسة',
                                                     CASE WHEN char_edu_authorities_i18n.name is null THEN ' '  Else char_edu_authorities_i18n.name END AS 'جهة الدراسة',
                                                     CASE WHEN char_edu_degrees_i18n.name is null THEN ' ' Else char_edu_degrees_i18n.name END as 'المؤهل العلمي' ,
                                                     CASE WHEN char_edu_stages_i18n.name is null THEN ' '  Else char_edu_stages_i18n.name END AS 'المرحلة الدراسية',
                                                     CASE WHEN char_persons_education.specialization is null THEN ' ' Else char_persons_education.specialization END  AS 'التخصص',
CASE WHEN char_persons_education.grade is null THEN ' '
	 WHEN char_persons_education.grade = 1 THEN 'اول ابتدائي'
	 WHEN char_persons_education.grade = 2 THEN 'ثاني ابتدائي'
	 WHEN char_persons_education.grade = 3 THEN 'ثالث ابتدئي'
     WHEN char_persons_education.grade = 4 THEN 'رابع ابتدئي'
 	 WHEN char_persons_education.grade = 5 THEN 'خامس ابتدئي'
 	 WHEN char_persons_education.grade = 6 THEN 'سادس ابتدئي'
  	WHEN char_persons_education.grade = 7 THEN 'السابع'
  	WHEN char_persons_education.grade = 8 THEN 'الثامن'
	 WHEN char_persons_education.grade = 9 THEN 'التاسع'
	WHEN char_persons_education.grade = 10 THEN 'العاشر'
	WHEN char_persons_education.grade = 11 THEN 'الحادي عشر'
	WHEN char_persons_education.grade = 12 THEN 'الثانس عشر'
	WHEN char_persons_education.grade = 13 THEN 'دراسات خاصة'
	WHEN char_persons_education.grade = 14 THEN 'تدريب مهنى'
	WHEN char_persons_education.grade = 15 THEN 'سنة أولى جامعة'
	WHEN char_persons_education.grade = 16 THEN 'سنة ثانية جامعة'
	WHEN char_persons_education.grade = 17 THEN 'سنة ثالثة جامعة'
	WHEN char_persons_education.grade = 18 THEN 'سنة رابعة جامعة'
	WHEN char_persons_education.grade = 19 THEN 'سنة خامسة جامعة'
                                                    END
                                                     AS 'الصف',
                                                      CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS 'السنة الدارسية',
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS 'اسم المدرسة',
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS 'اخر نتيجة',
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN 'ضعيف'
                                                               WHEN char_persons_education.level = 2 THEN 'مقبول'
                                                               WHEN char_persons_education.level = 3 THEN 'جيد'
                                                               WHEN char_persons_education.level = 4 THEN 'جيد جدا'
                                                               WHEN char_persons_education.level = 5 THEN 'ممتاز'
                                                     END
                                                     AS 'المستوى التحصيل',

                                 CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                     WHEN char_persons_islamic_commitment.prayer = 0 THEN 'يواظب'
                                     WHEN char_persons_islamic_commitment.prayer = 1 THEN 'لايصلي'
                                     WHEN char_persons_islamic_commitment.prayer = 2 THEN 'يقطع'
                               END AS 'هل يصلي',


                               CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' '
                                    Else char_persons_islamic_commitment.prayer_reason
                               END AS 'سبب ترك الصلاة',

                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN 'لا'
                                    Else 'نعم'
                               END AS 'هل يحفظ القران',
                               CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                    Else char_persons_islamic_commitment.quran_parts
                               END  AS 'عدد الأجزاء',
                               CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' '
                                    Else char_persons_islamic_commitment.quran_chapters
                               END AS 'عدد السور',
                               CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' '
                                    Else char_persons_islamic_commitment.quran_reason
                               END AS 'سبب ترك التحفيظ',
                               CASE WHEN char_persons_islamic_commitment.quran_center is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN 'لا'
                                    WHEN char_persons_islamic_commitment.quran_center = 1 THEN 'نعم'
                                    Else 'نعم'
                               END AS 'ملتحق بالمسجد'

from char_persons
join char_persons_kinship on char_persons.id = char_persons_kinship.l_person_id
join char_kinship_i18n on char_kinship_i18n.kinship_id = char_persons_kinship.kinship_id and char_kinship_i18n.language_id = 1
join char_persons as r_person on r_person.id = char_persons_kinship.r_person_id
left join char_marital_status_i18n as char_marital_status on char_marital_status.marital_status_id = r_person.marital_status_id and char_marital_status.language_id = 1
left join char_locations_i18n as L on L.location_id = char_persons.birth_place and L.language_id = 1
left join char_locations_i18n as L0 on L0.location_id = char_persons.nationality and L0.language_id = 1
left join char_persons_i18n on char_persons_i18n.person_id = r_person.id
left join char_persons_work on char_persons_work.person_id = r_person.id
left join char_work_jobs_i18n as work_job on work_job.work_job_id = char_persons_work.work_job_id and work_job.language_id = 1
left join char_persons_health on char_persons_health.person_id = r_person.id
left join char_diseases_i18n on char_diseases_i18n.disease_id = char_persons_health.disease_id and char_diseases_i18n.language_id = 1
left join char_persons_education on char_persons_education.person_id = r_person.id
left join char_edu_stages_i18n on char_edu_stages_i18n.edu_stage_id = char_persons_education.stage and char_edu_stages_i18n.language_id = 1
left join char_edu_authorities_i18n on char_edu_authorities_i18n.edu_authority_id = char_persons_education.authority and char_edu_authorities_i18n.language_id = 1
left join char_edu_degrees_i18n on char_edu_degrees_i18n.edu_degree_id = char_persons_education.degree and char_edu_degrees_i18n.language_id = 1
left join char_persons_islamic_commitment on char_persons_islamic_commitment.person_id = r_person.id
where char_persons.id  in (select char_cases.person_id from char_cases where char_cases.deleted_at is null and char_cases.status = 0 );



-- ----------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------
select * from char_active_aid_cases_view
where organization_id in (select descendant_id from char_organizations_closure where ancestor_id = 7)

/*
ancestor_id values
1 : all
2 : khanyounis
3 : rafah
4 : alwosta
5 : south gaza
6 : east gaza
7 : west gaza
8 : north gaza
*/

-- ----------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------
select * from char_active_cases_family_view
where person_id in (select person_id from char_cases where organization_id in (select descendant_id from char_organizations_closure where ancestor_id = 7))

/*
ancestor_id values
1 : all
2 : khanyounis
3 : rafah
4 : alwosta
5 : south gaza
6 : east gaza
7 : west gaza
8 : north gaza
*/

-- -----------------------------------------------------------------------------------------------------
-- ---------------------------- 25/7/2022 --------------------------------------------------------------

ALTER TABLE `char_vouchers` ADD `un_serial` VARCHAR(255) NULL DEFAULT NULL AFTER `id`;
ALTER TABLE `char_vouchers` ADD `collected` TINYINT(1) NULL DEFAULT '0' AFTER `un_serial`;

DROP VIEW IF EXISTS `char_vouchers_persons_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view`  AS SELECT `char_vouchers_persons`.`id` AS `id`,`char_vouchers_persons`.`document_id` AS `document_id`, `char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`, `v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'من مشروع' else 'يدوي' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'نقدية' when (`v`.`type` = 2) then 'عينية' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'عادية' when (`v`.`urgent` = 1) then 'عاجلة' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'مباشر' when (`v`.`transfer` = 1) then 'تصنيف جمعية' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'ذكر ' when (`c`.`gender` = 2) then 'أنثى' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'ذكر ' when (`ind`.`gender` = 2) then 'أنثى' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 0) then 'تم الاستلام' when (`char_vouchers_persons`.`status` = 2) then ' لم يتم الاستلام' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 1)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 1)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 1)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 1)))) ;

-- --------------------------------------------------------
DROP VIEW IF EXISTS `char_vouchers_persons_view_en`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_vouchers_persons_view_en`  AS SELECT `char_vouchers_persons`.`id` AS `id`, `char_vouchers_persons`.`document_id` AS `document_id`,`char_vouchers_persons`.`voucher_id` AS `voucher_id`, `char_vouchers_persons`.`person_id` AS `person_id`, `char_vouchers_persons`.`individual_id` AS `individual_id`, `char_vouchers_persons`.`receipt_date` AS `receipt_date`, `char_vouchers_persons`.`receipt_time` AS `receipt_time`, `char_vouchers_persons`.`receipt_location` AS `receipt_location`, `char_vouchers_persons`.`status` AS `rec_status`, `v`.`organization_id` AS `organization_id`, `v`.`category_id` AS `category_id`, `v`.`project_id` AS `project_id`, `org`.`name` AS `organization_name`, `org`.`en_name` AS `en_organization_name`, `sponsor`.`name` AS `sponsor_name`, `sponsor`.`en_name` AS `en_sponsor_name`, `v`.`type` AS `type`, `v`.`value` AS `value`, (`v`.`exchange_rate` * `v`.`value`) AS `shekel_value`, `v`.`title` AS `title`,  `v`.`serial` AS `serial`,`v`.`un_serial` AS `un_serial`,`v`.`collected` AS `collected`, `v`.`beneficiary` AS `beneficiary`, `v`.`currency_id` AS `currency_id`, `v`.`exchange_rate` AS `exchange_rate`, `v`.`template` AS `template`, `v`.`content` AS `content`, `v`.`header` AS `header`, `v`.`footer` AS `footer`, `v`.`notes` AS `notes`, `v`.`created_at` AS `created_at`, `v`.`updated_at` AS `updated_at`, `v`.`deleted_at` AS `deleted_at`, `v`.`sponsor_id` AS `sponsor_id`, `v`.`voucher_date` AS `voucher_date`, `v`.`count` AS `count`, `v`.`urgent` AS `urgent`, `v`.`status` AS `status`, `v`.`transfer` AS `transfer`, `v`.`transfer_company_id` AS `transfer_company_id`, `v`.`allow_day` AS `allow_day`, `char_vouchers_categories`.`name` AS `category_type`, (case when isnull(`v`.`project_id`) then 'from_project' else 'manually' end) AS `voucher_source`, (case when (`v`.`type` = 1) then 'financial' when (`v`.`type` = 2) then 'non_financial' end) AS `voucher_type`, (case when (`v`.`urgent` = 0) then 'not_urgent' when (`v`.`urgent` = 1) then 'urgent' end) AS `voucher_urgent`, (case when (`v`.`transfer` = 0) then 'direct' when (`v`.`transfer` = 1) then 'transfer_company' end) AS `transfer_status`, (case when isnull(`v`.`transfer_company_id`) then '-' else `char_transfer_company`.`name` end) AS `transfer_company_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`name` end) AS `currency_name`, (case when isnull(`v`.`currency_id`) then '-' else `char_currencies`.`en_name` end) AS `en_currency_name`, concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) AS `name`, `c`.`first_name` AS `first_name`, `c`.`second_name` AS `second_name`, `c`.`third_name` AS `third_name`, `c`.`last_name` AS `last_name`, (case when isnull(`c`.`marital_status_id`) then ' ' else `m`.`name` end) AS `marital_status`, (case when isnull(`c`.`gender`) then ' ' when (`c`.`gender` = 1) then 'male' when (`c`.`gender` = 2) then 'female' end) AS `gender`, `c`.`gender` AS `p_gender`, (case when isnull(`c`.`id_card_number`) then ' ' else `c`.`id_card_number` end) AS `id_card_number`, `char_get_family_count`('left',`c`.`id`,NULL,`c`.`id`) AS `family_count`, concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) AS `individual_name`, `ind`.`first_name` AS `ind_first_name`, `ind`.`second_name` AS `ind_second_name`, `ind`.`third_name` AS `ind_third_name`, `ind`.`last_name` AS `ind_last_name`, (case when isnull(`ind`.`id_card_number`) then ' ' else `ind`.`id_card_number` end) AS `individual_id_card_number`, (case when isnull(`ind`.`gender`) then ' ' when (`ind`.`gender` = 1) then 'male' when (`ind`.`gender` = 2) then 'female' end) AS `individual_gender`, `ind`.`gender` AS `ind_gender`, (case when isnull(`ind`.`marital_status_id`) then ' ' else `min`.`name` end) AS `individual_marital_status`, (case when (`v`.`beneficiary` = 1) then concat(ifnull(`c`.`first_name`,' '),' ',ifnull(`c`.`second_name`,' '),' ',ifnull(`c`.`third_name`,' '),' ',ifnull(`c`.`last_name`,' ')) else concat(ifnull(`ind`.`first_name`,' '),' ',ifnull(`ind`.`second_name`,' '),' ',ifnull(`ind`.`third_name`,' '),' ',ifnull(`ind`.`last_name`,' ')) end) AS `beneficiary_name`, (case when isnull(`primary_mob_number`.`contact_value`) then ' ' else `primary_mob_number`.`contact_value` end) AS `primary_mobile`, (case when isnull(`wataniya_mob_number`.`contact_value`) then ' ' else `wataniya_mob_number`.`contact_value` end) AS `wataniya`, (case when isnull(`secondary_mob_number`.`contact_value`) then ' ' else `secondary_mob_number`.`contact_value` end) AS `secondery_mobile`, (case when isnull(`phone_number`.`contact_value`) then ' ' else `phone_number`.`contact_value` end) AS `phone`, ifnull(`country_name`.`name`,' ') AS `country`, ifnull(`district_name`.`name`,' ') AS `district`, ifnull(`region_name`.`name`,' ') AS `region`, ifnull(`location_name`.`name`,' ') AS `nearlocation`, ifnull(`square_name`.`name`,' ') AS `square`, ifnull(`mosques_name`.`name`,' ') AS `mosque`, ifnull(`c`.`street_address`,' ') AS `street_address`, concat(ifnull(`country_name`.`name`,' '),'  ',ifnull(`district_name`.`name`,' '),'  ',ifnull(`region_name`.`name`,' '),'  ',ifnull(`location_name`.`name`,' '),'  ',ifnull(`square_name`.`name`,' '),'  ',ifnull(`mosques_name`.`name`,' '),'  ',ifnull(`c`.`street_address`,' ')) AS `full_address`, time_format(`char_vouchers_persons`.`receipt_time`,'%r') AS `receipt_time_`, `char_vouchers_persons`.`status` AS `receipt_status`, (case when (`char_vouchers_persons`.`status` = 1) then 'receipt' when (`char_vouchers_persons`.`status` = 0) then 'receipt' when (`char_vouchers_persons`.`status` = 2) then 'not_receipt' end) AS `receipt_status_name`, `v`.`case_category_id` AS `case_category_id`, `ca`.`name` AS `case_category_name`, `ca`.`en_name` AS `en_case_category_name` FROM (((((((((((((((((((((`char_vouchers_persons` join `char_vouchers` `v` on((`v`.`id` = `char_vouchers_persons`.`voucher_id`))) join `char_categories` `ca` on((`ca`.`id` = `v`.`case_category_id`))) join `char_organizations` `org` on((`org`.`id` = `v`.`organization_id`))) join `char_organizations` `sponsor` on((`sponsor`.`id` = `v`.`sponsor_id`))) left join `char_currencies` on((`char_currencies`.`id` = `v`.`currency_id`))) left join `char_transfer_company` on((`char_transfer_company`.`id` = `v`.`transfer_company_id`))) left join `char_vouchers_categories` on((`char_vouchers_categories`.`id` = `v`.`category_id`))) join `char_persons` `c` on((`c`.`id` = `char_vouchers_persons`.`person_id`))) left join `char_marital_status_i18n` `m` on(((`m`.`marital_status_id` = `c`.`marital_status_id`) and (`m`.`language_id` = 2)))) left join `char_persons_contact` `primary_mob_number` on(((`c`.`id` = `primary_mob_number`.`person_id`) and (`primary_mob_number`.`contact_type` = 'primary_mobile')))) left join `char_persons_contact` `wataniya_mob_number` on(((`c`.`id` = `wataniya_mob_number`.`person_id`) and (`wataniya_mob_number`.`contact_type` = 'wataniya')))) left join `char_persons_contact` `secondary_mob_number` on(((`c`.`id` = `secondary_mob_number`.`person_id`) and (`secondary_mob_number`.`contact_type` = 'secondery_mobile')))) left join `char_persons_contact` `phone_number` on(((`c`.`id` = `phone_number`.`person_id`) and (`phone_number`.`contact_type` = 'phone')))) left join `char_aids_locations_i18n` `country_name` on(((`c`.`adscountry_id` = `country_name`.`location_id`) and (`country_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `district_name` on(((`c`.`adsdistrict_id` = `district_name`.`location_id`) and (`district_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `region_name` on(((`c`.`adsregion_id` = `region_name`.`location_id`) and (`region_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `location_name` on(((`c`.`adsneighborhood_id` = `location_name`.`location_id`) and (`location_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `square_name` on(((`c`.`adssquare_id` = `square_name`.`location_id`) and (`square_name`.`language_id` = 2)))) left join `char_aids_locations_i18n` `mosques_name` on(((`c`.`adsmosques_id` = `mosques_name`.`location_id`) and (`mosques_name`.`language_id` = 2)))) left join `char_persons` `ind` on((`ind`.`id` = `char_vouchers_persons`.`individual_id`))) left join `char_marital_status_i18n` `min` on(((`min`.`marital_status_id` = `ind`.`marital_status_id`) and (`min`.`language_id` = 2)))) ;

-- --------------------------------------------------------

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(531 , '10', 'reports.transfers.create', 'اضافة طلب نقل');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 531, 1, CURRENT_TIMESTAMP, 1),
(2, 531, 1, CURRENT_TIMESTAMP, 1);

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(532 , '10', 'aid.voucher.updateSerial', 'تعديل الكود الموحد');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 532, 1, CURRENT_TIMESTAMP, 1);


---------------------------------------------------------------------------------------------------------------
------------------------------------------ 9/8/2022 -----------------------------------------------------------


ALTER TABLE `char_cases` CHANGE `status` `status` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'الحالة';


-- --------------------------------------------------------------------------------------
-- ---------------------------- 10/8/2022 -----------------------------------------------

ALTER TABLE `char_transfers`
ADD `prev_adscountry_id` INT(10) NULL DEFAULT NULL  AFTER `person_id`,
ADD `prev_adsdistrict_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adscountry_id`,
ADD `prev_adsregion_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adsdistrict_id`,
ADD `prev_adsneighborhood_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adsregion_id`,
ADD `prev_adssquare_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adsneighborhood_id`,
ADD `prev_adsmosques_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adssquare_id`;
ADD `prev_adsmosques_id` INT(10) NULL DEFAULT NULL  AFTER `prev_adssquare_id`;

DROP VIEW IF EXISTS `char_transfers_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_transfers_view`  AS
SELECT `char_categories`.`type` AS `category_type`, concat(ifnull(`char_users`.`firstname`,' '),' ',ifnull(`char_users`.`lastname`,' ')) AS `user_name`,
`char_persons`.`id_card_number` AS `id_card_number`,
concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',ifnull(`char_persons`.`third_name`,' '),' ',
ifnull(`char_persons`.`last_name`,' ')) AS `full_name`, ifnull(`mobno`.`contact_value`,'-') AS `primary_mobile`, ifnull(`phono`.`contact_value`,'-') AS `phone`,
`char_persons`.`first_name` AS `first_name`, `char_persons`.`second_name` AS `second_name`, `char_persons`.`third_name` AS `third_name`,
`char_persons`.`last_name` AS `last_name`, `char_persons`.`prev_family_name` AS `prev_family_name`, `char_persons`.`gender` AS `gender`,
`char_persons`.`marital_status_id` AS `marital_status_id`, `char_persons`.`birthday` AS `birthday`,
`char_persons`.`country` AS `country`, `char_persons`.`governarate` AS `governarate`, `char_persons`.`city` AS `city`,
`char_persons`.`location_id` AS `location_id`, `char_persons`.`mosques_id` AS `mosques_id`,
`char_persons`.`street_address` AS `street_address`,
`char_transfers`.`prev_adscountry_id` AS `prev_adscountry_id`,
`char_transfers`.`prev_adsdistrict_id` AS `prev_adsdistrict_id`,
`char_transfers`.`prev_adsregion_id` AS `prev_adsregion_id`,
`char_transfers`.`prev_adsneighborhood_id` AS `prev_adsneighborhood_id`,
`char_transfers`.`prev_adssquare_id` AS `prev_adssquare_id`,
`char_transfers`.`prev_adsmosques_id` AS `prev_adsmosques_id`,
`char_transfers`.`adscountry_id` AS `adscountry_id`,
`char_transfers`.`adsdistrict_id` AS `adsdistrict_id`,
`char_transfers`.`adsregion_id` AS `adsregion_id`,
`char_transfers`.`adsneighborhood_id` AS `adsneighborhood_id`,
`char_transfers`.`adssquare_id` AS `adssquare_id`,
`char_transfers`.`adsmosques_id` AS `adsmosques_id`,
`char_transfers`.`id` AS `id`, `char_transfers`.`person_id` AS `person_id`,
`char_transfers`.`organization_id` AS `transfer_organization_id`, `char_transfers`.`category_id` AS `category_id`,
`char_transfers`.`user_id` AS `user_id`, `char_transfers`.`created_at` AS `created_at`, `char_transfers`.`updated_at` AS `updated_at`,
 `char_transfers`.`deleted_at` AS `deleted_at`,`char_transfers`.`organization_id`, `char_organizations`.`name` AS `organization_name`, `char_categories`.`name` AS `category_name`
  FROM ((((((
  `char_transfers`
   join `char_categories` on((`char_categories`.`id` = `char_transfers`.`category_id`)))
    join `char_persons` on((`char_persons`.`id` = `char_transfers`.`person_id`)))
    left join `char_persons_contact` `mobno` on(((`mobno`.`person_id` = `char_transfers`.`person_id`) and (`mobno`.`contact_type` = 'primary_mobile'))))
    left join `char_persons_contact` `phono` on(((`phono`.`person_id` = `char_transfers`.`person_id`) and (`phono`.`contact_type` = 'phone'))))
    join `char_organizations` on((`char_organizations`.`id` = `char_transfers`.`organization_id`)))
    join `char_users` on((`char_users`.`id` = `char_transfers`.`user_id`))) ORDER BY `char_transfers`.`created_at` DESC ;

update char_persons , char_transfers
set char_transfers.prev_adscountry_id = char_persons.adscountry_id ,
    char_transfers.prev_adsdistrict_id = char_persons.adsdistrict_id ,
    char_transfers.prev_adsregion_id = char_persons.adsregion_id ,
    char_transfers.prev_adsneighborhood_id = char_persons.adsneighborhood_id ,
    char_transfers.prev_adssquare_id = char_persons.adssquare_id ,
    char_transfers.prev_adsmosques_id = char_persons.adsmosques_id
where char_persons.id = char_transfers.person_id;



/*
select  char_persons.id,char_persons.family_cnt,char_persons.male_live, char_persons.female_live,char_persons.spouses, CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name, CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number, CASE WHEN  char_persons.birthday is null THEN '-' Else  DATE_FORMAT(char_persons.birthday,'%Y\/%m\/%d') END AS birthday, CASE WHEN  char_persons.marital_status_id is null THEN '-' Else  char_marital_status.name END AS marital_status, CASE WHEN  char_persons.id_card_number is null THEN '-' Else  char_persons.id_card_number END AS id_card_number, CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,   CASE              WHEN char_persons.gender is null THEN '-'              WHEN char_persons.gender = 1 THEN '\u0630\u0643\u0631 '              WHEN char_persons.gender = 2 THEN '\u0623\u0646\u062b\u0649'        END        AS gender          , CONCAT(ifnull(country_name.name,' '),' _ ',                           ifnull(district_name.name,' '),' _ ',                           ifnull(region_name.name,' '),' _ ',                           ifnull(location_name.name,' '),' _ ',                           ifnull(square_name.name,' '),' _ ',                           ifnull(mosques_name.name,' '),' _ ',                           ifnull(char_persons.street_address,' '))           AS address

from `char_persons`
left join `char_marital_status_i18n` as `char_marital_status` on `char_marital_status`.`marital_status_id` = `char_persons`.`marital_status_id` and `char_marital_status`.`language_id` = 1
left join `char_persons_contact` as `cont2` on `cont2`.`person_id` = `char_persons`.`id` and `cont2`.`contact_type` = 'primary_mobile'
left join `char_aids_locations_i18n` as `country_name` on `char_persons`.`adscountry_id` = `country_name`.`location_id` and `country_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `district_name` on `char_persons`.`adsdistrict_id` = `district_name`.`location_id` and `district_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `region_name` on `char_persons`.`adsregion_id` = `region_name`.`location_id` and `region_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `location_name` on `char_persons`.`adsneighborhood_id` = `location_name`.`location_id` and `location_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `square_name` on `char_persons`.`adssquare_id` = `square_name`.`location_id` and `square_name`.`language_id` = 1
left join `char_aids_locations_i18n` as `mosques_name` on `char_persons`.`adsmosques_id` = `mosques_name`.`location_id` and `mosques_name`.`language_id` = 1

where (normalize_text_ar(char_persons.first_name) like  normalize_text_ar('%زينب%') and normalize_text_ar(char_persons.last_name) like  normalize_text_ar('%حطب%'))
 */


INSERT INTO `char_settings` (`id`, `value`, `organization_id`) VALUES ('gov_service', '0', '0');
