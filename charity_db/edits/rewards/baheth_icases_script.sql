SELECT

operation_cases.reward_id,

operation_cases.case_status,
operation_cases.active_reason_hint,

operation_cases.id,

operation_cases.id_card_number,
operation_cases.card_type,
operation_cases.fullname,
operation_cases.fullname_en,
operation_cases.first_name,
operation_cases.second_name,
operation_cases.third_name,
operation_cases.last_name,

operation_cases.longitude,
operation_cases.latitude,
operation_cases.prev_family_name,

CASE WHEN operation_cases.gender = 1 THEN 1  Else 2  END  AS gender,

operation_cases.marital_status,

CASE WHEN operation_cases.deserted = 1 THEN 1  Else 2  END  AS deserted,

operation_cases.birthday,

(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.nationality ) AS nationality,

(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.birth_place ) AS birth_place,

operation_cases.death_date,

(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.death_cause_id ) AS death_cause,

operation_cases.family_cnt,
operation_cases.spouses,
operation_cases.male_live ,
operation_cases.female_live ,

operation_cases.phone,
operation_cases.primary_mobile,
operation_cases.secondary_mobile,
operation_cases.watanya,

'دولة فلسطين' AS country,
governorates.`name` AS `district`,
regoins.`name` AS `region`,
neighborhoods.`name` AS `neighborhood`,
squares.`name` AS `square`,
mosques.`name` AS mosque ,
(SELECT	mosques.`name`FROM mosques WHERE mosques.id = mosque_id) AS `المسجد`,
operation_cases.street_address_details,
operation_cases.street_address,

operation_cases.status,
operation_cases.reason,
operation_cases.status_type,

CASE WHEN operation_cases.refugee = 1 THEN 1  Else 2  END  AS refugee,
CASE WHEN operation_cases.refugee = 2 THEN operation_cases.unrwa_card_number  Else null  END  AS unrwa_card_number,

CASE WHEN operation_cases.is_qualified = 1 THEN 1  Else 0  END  AS is_qualified,
CASE WHEN operation_cases.is_qualified = 1 THEN operation_cases.qualified_card_number  Else null END  AS qualified_card_number,

CASE WHEN operation_cases.receivables = 1 THEN 1 Else 0 END AS receivables,
CASE WHEN operation_cases.receivables = 1 THEN operation_cases.receivables_sk_amount Else 0 END AS receivables_sk_amount,

operation_cases.monthly_income ,
operation_cases.actual_monthly_income ,

researchers.fullname AS visitor,
researchers.id_number AS visitor_card,
CASE operation_cases.visitor_evaluation
				WHEN 4 THEN	'ممتاز'
		    WHEN 3 THEN	'جيد جدا'
        WHEN 2 THEN		'جيد'
        WHEN 1 THEN	'سيء'
       WHEN 0 THEN	'سيء جدا'
	END AS visitor_evaluation,

operation_cases.researcher_recommendations as notes,
operation_cases.visitor_opinion,
operation_cases.visited_at,

operation_cases.needs,
CASE  WHEN operation_cases.promised = 1 THEN 1 Else 2 END AS promised,
CASE  WHEN operation_cases.promised = 1 THEN operation_cases.reconstructions_organization_name Else 2 END AS reconstructions_organization_name,

operation_cases.health_status,
( SELECT constants.NAME FROM constants WHERE constants.id = operation_cases.disease ) AS disease,
operation_cases.health_status_description,

CASE WHEN operation_cases.health_insurance = 1 THEN 1  Else 0  END  AS has_health_insurance,
CASE WHEN operation_cases.health_insurance_type is null THEN ' ' Else ( SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.health_insurance_type ) END   AS health_insurance_type,
CASE WHEN operation_cases.health_insurance_number is null THEN ' ' Else operation_cases.health_insurance_number END   AS health_insurance_number,

CASE WHEN operation_cases.device_use = 1 THEN 1 ELSE 0 END AS has_device,
CASE WHEN operation_cases.device_use = 1 THEN operation_cases.device_use_name ELSE null END AS used_device_name,
operation_cases.medical_report_summary,


CASE WHEN operation_cases.do_working = 1 THEN 1  Else 2  END  AS working,
( SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_status ) AS work_status,
( SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_title ) AS work_job,
operation_cases.Income_by_category AS work_wage,
operation_cases.work_space AS work_location,

CASE WHEN operation_cases.ability_of_work = 1 THEN 1  Else 2  END  AS can_work,
ability_of_work_reason AS work_reason,


CASE operation_cases.home_ownership
		WHEN 0 THEN 'ايجار'WHEN 1 THEN 'ايجار'	WHEN 2 THEN 'ملك'
		WHEN 3 THEN 'دون مقابل'	WHEN 4 THEN 'أراضي حكومية'	WHEN 5 THEN 'مستضاف'
END AS property_type,

operation_cases.rent_value,

CASE	operation_cases.walls_of_house
      WHEN 942 THEN	'باطون جيد'		WHEN 956 THEN	'كرفان'
      WHEN 943 THEN	'كرميد'	WHEN 944 THEN	'زينكو'
      WHEN 459442 THEN	'باطون جديد'	WHEN 945 THEN	'خيمة'
      WHEN 946 THEN	'أسبست'	WHEN 947 THEN	'بلاستيك'
      WHEN 948 THEN	'أخرى'	WHEN 949 THEN	'صفيح جديد'
      WHEN 459323 THEN	'أسبست'	WHEN 950 THEN	'صفيح قديم'
      WHEN 459525 THEN	'باطون'	WHEN 951 THEN'باطون قديم'
      WHEN 952 THEN'نصف بطون و نصف صفيح'	WHEN 953 THEN	'جزء باطون والاخر زينكو'
      WHEN 954 THEN	'جزء باطون والاخر أسبست'	WHEN 459466 THEN'باطون سيئ'
      WHEN 955 THEN	'جزء زينكو والاخر أسبست'	Else Null
      END AS roof_material,


CASE operation_cases.home_status
		WHEN 62 THEN	'قديم'	WHEN 459397 THEN	'سيء جدا'
		WHEN 459431 THEN	'سيء'	WHEN 56 THEN	'ممتاز'
		WHEN 57 THEN	'جيد جدا'	WHEN 58 THEN		'جيد'
		WHEN 459325 THEN	'سيء'	WHEN 59 THEN		'متوسط'
		WHEN 60 THEN	'سيء'	WHEN 61 THEN	'سيء جدا'
	END AS residence_condition,

CASE operation_cases.placement_of_furniture
		WHEN 1216 THEN	'سئ'	WHEN 1217 THEN	'سئ جدا'
		WHEN 1218 THEN	'قديم'	WHEN 459441 THEN	'لا يوجد'
		WHEN 459443 THEN'سئ'	WHEN 459040 THEN'سئ'
		WHEN 459043 THEN'سئ جدا'	WHEN 34 THEN	'ممتاز'
		WHEN 35 THEN'جيد جدا'	WHEN 459324 THEN'سئ'
		WHEN 459465 THEN	'سئ جدا'	WHEN 1215 THEN	'جيد'
END AS indoor_condition,

CASE	operation_cases.description_of_home_status
		WHEN 315 THEN	'بحاجة لإعمار كلي'
		WHEN 316 THEN	'اعمار جذري'
		WHEN 38 THEN	'لا يحتاج للترميم'
		WHEN 39 THEN	'بحاحة لإعمار جزئي'
	END AS habitable,

CASE	operation_cases.home_assessment
		WHEN 63 THEN	'جديد (1-10سنة)'
		WHEN 64 THEN	'متوسط (11-20سنة)'
		WHEN 65 THEN	'قديم (أكثر من 20سنة)'
		WHEN 459203 THEN	'متوسط (11-20سنة)'
		WHEN 459204 THEN	'جديد (1-10سنة)'
	END AS house_condition,

operation_cases.room_number as rooms,
operation_cases.house_area_in_meters as area,

CASE WHEN operation_cases.renovate_the_house = 1 THEN 1  Else 2  END  AS need_repair,
( SELECT constants.NAME FROM constants WHERE constants.id = description_of_home_status ) AS repair_notes,



CASE WHEN operation_cases.commercial_record = 1 THEN 1 Else 0 END AS has_commercial_records,
CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_active_number Else 0 END AS active_commercial_records,
CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_info Else ' ' END AS gov_commercial_records_details,



operation_cases.chf,
operation_cases.oxform,
operation_cases.wfp,
operation_cases.family_martyrs,
operation_cases.prisoners,
operation_cases.world_bank,
operation_cases.wounded,
operation_cases.permanent_unemployment,
operation_cases.social_affairs,
operation_cases.benefactor,
operation_cases.family_sponsorship,
operation_cases.guarantees_of_orphans,
operation_cases.neighborhood_committees,
operation_cases.Zakat,
operation_cases.agency_aid,


operation_cases.chf_sample,
operation_cases.oxform_sample,
operation_cases.wfp_sample,
operation_cases.family_martyrs_sample,
operation_cases.prisoners_sample,
operation_cases.world_bank_sample,
operation_cases.wounded_sample,
operation_cases.permanent_unemployment_sample,
operation_cases.social_affairs_sample,
operation_cases.benefactor_sample,
operation_cases.family_sponsorship_sample,
operation_cases.guarantees_of_orphans_sample,
operation_cases.neighborhood_committees_sample,
operation_cases.Zakat_sample,
operation_cases.agency_aid_sample,

operation_cases.lands,
operation_cases.cars,
operation_cases.property,
operation_cases.cattle,


operation_cases.doors,
operation_cases.safe_lighting,
operation_cases.gas_tube,
operation_cases.cobble,
operation_cases.tv,
operation_cases.fridge,
operation_cases.solarium,
operation_cases.watertank,
operation_cases.sweet_watertank,
operation_cases.closet,
operation_cases.heater,
operation_cases.windows,
operation_cases.electricity_network,
operation_cases.water_network,
operation_cases.electric_cooker,
operation_cases.stuff_kitchen,
operation_cases.gas,
operation_cases.washer,
operation_cases.mattresses,
operation_cases.plastic_chairs,
operation_cases.fan,
operation_cases.top_ceiling,
operation_cases.solarPower,

operation_cases.qualifiedRelationship,
operation_cases.abilityOfWorkReasonAnother,
operation_cases.wallsOfHouseAnother,

operation_cases.hasCHF,
operation_cases.hasOXFAM,
operation_cases.hasZakat,
operation_cases.hasGuaranteesOfOrphans,
operation_cases.hasFamilySponsorship,
operation_cases.hasAgencyAid,
operation_cases.hasNeighborhoodCommittees,
operation_cases.hasSocialAffairs,
operation_cases.hasPermanentUnemployment,
operation_cases.hasWounded,
operation_cases.hasWorldBank,
operation_cases.hasPrisoners,
operation_cases.hasFamilyMartyrs,
operation_cases.hasWFP,

operation_cases.zakatCount,
operation_cases.guaranteesOfOrphansCount,
operation_cases.familySponsorshipCount,
operation_cases.agencyAidCount,
operation_cases.neighborhoodCommitteesCount,
operation_cases.socialAffairsCount,
operation_cases.permanentUnemploymentCount,
operation_cases.woundedCount,
operation_cases.worldBankCount,
operation_cases.prisonersCount,
operation_cases.familyMartyrsCount,
operation_cases.WFPCount,
operation_cases.OXFAMCount,
operation_cases.CHFCount,
operation_cases.chfSampleCount,
operation_cases.oxformSampleCount,
operation_cases.wfpSampleCount,
operation_cases.familyMartyrsSampleCount,
operation_cases.prisonersSampleCount,
operation_cases.worldBankSampleCount,
operation_cases.woundedSampleCount,
operation_cases.permanentUnemploymentSampleCount,
operation_cases.socialAffairsSampleCount,
operation_cases.neighborhoodCommitteesSampleCount,
operation_cases.agencyAidSampleCount,
operation_cases.familySponsorshipSampleCount,
operation_cases.guaranteesOfOrphansSampleCount,
operation_cases.zakatSampleCount,

operation_cases.other1HelpType,
operation_cases.other2HelpType,
operation_cases.hasAnotherHelp,

operation_cases.other1HelpCount,
operation_cases.other1HelpSource,
operation_cases.other1HelpValue,
operation_cases.other2HelpValue,
operation_cases.other2HelpSource,
operation_cases.other2HelpCount,

operation_cases.bank1AccountNumber,
operation_cases.bank1AccountOwner,
operation_cases.bank1AccountBranch,
operation_cases.bank1Name,

operation_cases.bank2AccountNumber,
operation_cases.bank2AccountOwner,
operation_cases.bank2AccountBranch,
operation_cases.bank2Name,

operation_cases.hasCar,
operation_cases.hasCattle,
operation_cases.hasLands,
operation_cases.hasProperty,
operation_cases.hasQatarHelp,

operation_cases.qatarHelp,
operation_cases.qatarHelpCount,
operation_cases.qatarHelpSample,
operation_cases.qatarHelpSampleCount,

operation_cases.home_number,

operation_cases.created_at,
operation_cases.updated_at,

operation_cases.not_qualified_reason,
operation_cases.person_present,


operation_cases.active,
operation_cases.activeReason,
operation_cases.active_reason_hint,
operation_cases.manual,
operation_cases.case_status,
operation_cases.return_status,  
operation_cases.operation_case_returned_reason

FROM charities
     RIGHT JOIN operation_cases ON charities.id = operation_cases.charity_id
     LEFT JOIN mosques ON operation_cases.mosque_id = mosques.id
     LEFT JOIN squares ON operation_cases.square_id = squares.id
     LEFT JOIN regoins ON operation_cases.region_id = regoins.id
     INNER JOIN neighborhoods ON operation_cases.neighborhood_id = neighborhoods.id
     LEFT JOIN researchers ON operation_cases.researcher_id = researchers.id
     INNER JOIN governorates ON charities.governorate_id = governorates.id
WHERE operation_cases.case_status LIKE "CONFIRMED"  AND operation_cases.reward_id =13

-- operation_cases.case_status LIKE "COMPLETED"  AND