
UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومي/مدني%';
UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومي / مدني%';
UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومة غزة / مدني%';
UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومه غزة%';

UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومي/عسكري%';
UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومي / عسكري%';
UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومة غزة / عسكري%';
UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومة / عسكري%';

-- ------------------------------------------------------------------------------------------------------------
UPDATE  operation_cases set walls_of_house = 948 where  walls_of_house = 'أخرى' ;
UPDATE  operation_cases set walls_of_house = 459323 where  walls_of_house = 'أسبست' ;
UPDATE  operation_cases set walls_of_house = 459442 where  walls_of_house ='باطون جيد' ;
UPDATE  operation_cases set walls_of_house = 951 where  walls_of_house ='باطون قديم' ;
UPDATE  operation_cases set walls_of_house = 954 where  walls_of_house ='جزء باطون والاخر أسبست' ;
UPDATE  operation_cases set walls_of_house = 955 where  walls_of_house ='جزء زينكو والاخر أسبست' ;
UPDATE  operation_cases set walls_of_house = 944 where  walls_of_house ='زينكو' ;
UPDATE  operation_cases set walls_of_house = 949 where  walls_of_house ='صفيح جديد' ;
UPDATE  operation_cases set walls_of_house = 950 where  walls_of_house ='صفيح قديم' ;
UPDATE  operation_cases set walls_of_house = 943 where  walls_of_house ='كرميد' ;
UPDATE  operation_cases set walls_of_house = 952 where  walls_of_house = 'نصف بطون و نصف صفيح' ;

UPDATE  operation_cases set bank1AccountBranch = 'فرع الوسطى' where  bank1AccountBranch LIKE '%الوسطى%';
UPDATE  operation_cases set bank1AccountBranch = 'فرع خانيونس' where  bank1AccountBranch LIKE '%خانيونس%' ;
UPDATE  operation_cases set bank1AccountBranch = 'فرع النصيرات' where  bank1AccountBranch LIKE '%نصيرات%' ;

UPDATE  operation_cases set bank2AccountBranch = 'فرع الوسطى' where  bank2AccountBranch LIKE '%الوسطى%';
UPDATE  operation_cases set bank2AccountBranch = 'فرع خانيونس' where  bank2AccountBranch LIKE '%خانيونس%' ;
UPDATE  operation_cases set bank2AccountBranch = 'فرع النصيرات' where  bank2AccountBranch LIKE '%نصيرات%' ;
--
-- UPDATE  _rewards_cases set bank1AccountBranch = 'فرع الوسطى' where  bank1AccountBranch LIKE '%الوسطى%';
-- UPDATE  _rewards_cases set bank1AccountBranch = 'فرع خانيونس' where  bank1AccountBranch LIKE '%خانيونس%' ;
-- UPDATE  _rewards_cases set bank1AccountBranch = 'فرع النصيرات' where  bank1AccountBranch LIKE '%نصيرات%' ;
--
-- UPDATE  _rewards_cases set bank2AccountBranch = 'فرع الوسطى' where  bank2AccountBranch LIKE '%الوسطى%';
-- UPDATE  _rewards_cases set bank2AccountBranch = 'فرع خانيونس' where  bank2AccountBranch LIKE '%خانيونس%' ;
-- UPDATE  _rewards_cases set bank2AccountBranch = 'فرع النصيرات' where  bank2AccountBranch LIKE '%نصيرات%' ;

UPDATE  operation_cases set work_status  = 'حكومة غزة / مدني' where  work_status  LIKE '%حكومي/مدني%';
UPDATE  operation_cases set work_status  = 'حكومة غزة / عسكري' where  work_status  LIKE '%حكومي/عسكري%';
UPDATE  operation_cases set work_status  = 'سلطة رام الله / مدني' where  work_status  LIKE '%رام الله/مدني%';
UPDATE  operation_cases set work_status  = 'سلطة رام الله / عسكري' where  work_status  LIKE '%سلطة رام الله/عسكري%';

UPDATE  _rewards_cases set work_status  = 'حكومة غزة / مدني' where  work_status  LIKE '%حكومي/مدني%';
UPDATE  _rewards_cases set work_status  = 'حكومة غزة / عسكري' where  work_status  LIKE '%حكومي/عسكري%';
UPDATE  _rewards_cases set work_status  = 'سلطة رام الله / مدني' where  work_status  LIKE '%سلطة رام الله/مدني%';
UPDATE  _rewards_cases set work_status  = 'سلطة رام الله / عسكري' where  work_status  LIKE '%سلطة رام الله/عسكري%';
UPDATE  _rewards_cases set work_status  = 'حكومة غزة / مدني' where  work_status  LIKE '%حكومه غزة%';

UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومي/مدني%';
UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومي/عسكري%';
UPDATE  constants set name  = 'سلطة رام الله / مدني' where  name  LIKE '%رام الله/مدني%';
UPDATE  constants set name  = 'سلطة رام الله / عسكري' where  name  LIKE '%سلطة رام الله/عسكري%';

UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومي/مدني%';
UPDATE  constants set name  = 'حكومة غزة / عسكري' where  name  LIKE '%حكومي/عسكري%';
UPDATE  constants set name  = 'سلطة رام الله / مدني' where  name  LIKE '%سلطة رام الله/مدني%';
UPDATE  constants set name  = 'سلطة رام الله / عسكري' where  name  LIKE '%سلطة رام الله/عسكري%';
UPDATE  constants set name  = 'حكومة غزة / مدني' where  name  LIKE '%حكومه غزة%';
-- UPDATE  _rewards_cases set house_condition  = 'قديم (أكثر من 20سنة' where  house_condition  LIKE '%قديم (أكثر من 20سنة)%';

UPDATE  _rewards_cases set roof_material  = 'باطون قديم' where  roof_material  LIKE '%باطون سيئ';
UPDATE  _rewards_cases set roof_material  = 'باطون جيد' where  roof_material  LIKE '%باطون جديد%';
UPDATE  _rewards_cases set roof_material  = 'باطون جيد' where  roof_material  LIKE '%باطون%';


-- ------------------------------------------------------------------------------------------------------------
-- reason null
-- UPDATE  _rewards_cases set work_wage   = null where  1;


-- ------------------------------------------------------------------------------------------------------------


CREATE TABLE `rewards_cases` (
  `operation_case_id` bigint(20) UNSIGNED NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `id_card_number` int(11) DEFAULT NULL COMMENT 'رقم الهوية',
  `card_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع البطاقة',

  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `fullname_en` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم رباعي',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم الأول',
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الأب',
  `third_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجد',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة',
  `prev_family_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة السابق',

  `longitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط الطول',
  `latitude` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'خط العرض',

  `gender` tinyint(1) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 0- أنثى',

  `marital_status_id` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الاجتماعية ',
  `deserted` int(11) DEFAULT NULL COMMENT 'مهجورة في حال انثى متزوجة :1-نعم ، 0-لا',

  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'مكان الميلاد - جدول ثوابت',

  `death_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `death_cause_id` varchar(255) DEFAULT NULL COMMENT 'سبب الوفاة - جدول ثوابت',

  `nationality` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الجنسية ''المفتاح الأساسي للجدول'',',

  `family_cnt` int(11) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `spouses` int(11) DEFAULT NULL COMMENT 'عدد الزوجات',
  `male_live` int(11) DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة',
  `female_live` int(11) DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة',

  `refugee` int(11) DEFAULT NULL COMMENT 'حالة المواطنة:1- مواطن ، 2-لاجيء',
  `unrwa_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم كرت المؤن في ال كان لاجيء',

  `is_qualified` int(11) DEFAULT NULL COMMENT '	هل مؤهل:1-مؤهل ، 0-غير مؤهل',
  `qualified_card_number` int(11) DEFAULT NULL COMMENT '	رقم هوية المعييل في حال كان رب الاسرة غير مؤهل',

  `receivables` int(11) DEFAULT NULL COMMENT '	هل لديه ذمم مالية:1-نعم ، 0-لا',
  `receivables_sk_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'قيمة الذمم المالية بالشيكل',
  `monthly_income` int(11) DEFAULT NULL COMMENT 'الدخل الشهري الكلي',
  `actual_monthly_income` int(11) DEFAULT NULL COMMENT '	الدخل الشهري الفعلي',

  `has_commercial_records` int(11) DEFAULT NULL COMMENT ' هل لديه سجل تجاري :1 نعم , 0 لا',
  `active_commercial_records` int(11) DEFAULT NULL COMMENT 'عدد السجلات التجاريةالفعالة',
  `gov_commercial_records_details` text COLLATE utf8mb4_unicode_ci COMMENT 'بيان السجلات التجارية',

  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم التلفون الارضي',
  `primary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الجوال الاساسي',
  `secondary_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	رقم الجوال الاحتياطي',
  `watanya` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم الوطنية',

  `charity_id` int(11) DEFAULT NULL COMMENT ' الجمعية',

  `country_id` varchar(255) DEFAULT NULL COMMENT 'الدولة - جدول ثوابت',
  `district_id` varchar(255) DEFAULT NULL COMMENT '	المحافظة - جدول ثوابت',
  `region_id` varchar(255) DEFAULT NULL COMMENT 'المنطقة - جدول ثوابت',
  `neighborhood_id` varchar(255) DEFAULT NULL COMMENT '	الحي - جدول ثوابت',
  `square_id` varchar(255) DEFAULT NULL COMMENT 'المربع - جدول ثوابت',
  `mosque_id` varchar(255) DEFAULT NULL COMMENT 'المسجد - جدول ثوابت',
  `street_address_details` text COLLATE utf8mb4_unicode_ci COMMENT '	تفاصيل العنوان - الشارع',
  `street_address` text COLLATE utf8mb4_unicode_ci COMMENT '	 العنوان - الشارع',

  `needs` text COLLATE utf8mb4_unicode_ci COMMENT 'الاحتياجات',

  `promised` int(11) DEFAULT NULL COMMENT 'هل وعدت بالبناء : : 1- لا ، 2 نعم',
  `reconstructions_organization_name` text COLLATE utf8mb4_unicode_ci COMMENT 'اسم الجمعية او المؤسسة التي وعدت العائلة',

  `visitor` text COLLATE utf8mb4_unicode_ci COMMENT '	اسم الباحث الاجتماعي',
  `visitor_card` int(11) DEFAULT NULL COMMENT '	رقم هوية الباحث الاجتماعي',
  `visited_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	تاريخ زيارة الباحث',
  `visitor_evaluation` varchar(255) DEFAULT NULL COMMENT '	تقييم الباحث لحالة المنزل : 4- ممتاز ، 3- جيد جدا ، 2 جيد ، 1 سيء ، 0 سيء جدا',
  `visitor_opinion` text COLLATE utf8mb4_unicode_ci COMMENT '	رأي الباحث الاجتماعي',
  `notes` text COLLATE utf8mb4_unicode_ci COMMENT 'توصيات الباحث الاجتماعي',

  `bank1AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank1Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

  `bank2AccountNumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountOwner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2AccountBranch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank2Name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

  `working` int(11) DEFAULT NULL COMMENT 'هل يعمل : 1 نعم ,0 لا',
  `work_job_id` text COLLATE utf8mb4_unicode_ci COMMENT ' المسمى الوظيفي ',
  `work_status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة العمل',
  `work_wage_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' الدخل حسب الفئة',
  `work_location` text COLLATE utf8mb4_unicode_ci COMMENT 'مكان العمل',

  `can_work` int(11) DEFAULT NULL COMMENT '  قدرة رب الأسرة على العمل: 1 نعم , 0 لا ',
  `work_reason_id` text COLLATE utf8mb4_unicode_ci COMMENT ' سبب عدم المقدرة',
  `abilityOfWorkReasonAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'سبب عدم القعدرة ع العمل',

  `has_other_work_resources` int(11) DEFAULT NULL COMMENT 'هل لديه مصادر دخل أخرى : 1 نعم 0 لا ',
  `property_type_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملكية المنزل : 0 ايجار , 1 ملك',
  `roof_material_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' سقف وجدران المنزل',
  `house_condition` text COLLATE utf8mb4_unicode_ci COMMENT 'تقييم وضع المنزل ',
  `indoor_condition` varchar(255) DEFAULT NULL COMMENT 'وضع الأثاث :0 سيىء , 1 جيد , 2 جيد جدا ',
  `residence_condition` varchar(255) DEFAULT NULL COMMENT ' حالة المنزل  :0 سيىء , 1 جيد , 2 جيد جدا ',
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المساحة بالمتر',
  `rooms` int(11) DEFAULT NULL COMMENT 'عدد الغرف ',
  `rent_value` int(11) DEFAULT NULL COMMENT 'قيمة الايجار بالشيكل',
  `need_repair` int(11) DEFAULT NULL COMMENT '  هل بحاجة لترميم المنزل للموائمة : 0 لا , 1 نعم',
  `repair_notes` text COLLATE utf8mb4_unicode_ci COMMENT 'وصف وضع المنزل',

  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الصحية',
  `details` text COLLATE utf8mb4_unicode_ci COMMENT 'تفاصيل الحالة الصحية',
  `disease_id` text COLLATE utf8mb4_unicode_ci COMMENT 'المرض أو الاعاقة',

  `has_health_insurance` int(11) DEFAULT NULL COMMENT 'هل لديك تأمين صحي 0 لا 1 نعم',
  `health_insurance_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم التأمين الصحي',
  `health_insurance_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع التأمين الصحي',

  `has_device` int(11) DEFAULT NULL COMMENT '  هل يستخدم جهاز 0 لا 1 نعم',
  `used_device_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجهاز المستخدم',
  `medical_report_summary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ملخص التقارير الطبية',

  `hasCHF` tinyint(1) NOT NULL DEFAULT '0',
  `hasOXFAM` tinyint(1) NOT NULL DEFAULT '0',
  `hasWFP` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilyMartyrs` int(11) NOT NULL DEFAULT '0',
  `hasPrisoners` tinyint(1) NOT NULL DEFAULT '0',
  `hasWorldBank` tinyint(1) NOT NULL DEFAULT '0',
  `hasWounded` tinyint(1) NOT NULL DEFAULT '0',
  `hasPermanentUnemployment` tinyint(1) NOT NULL DEFAULT '0',
  `hasSocialAffairs` tinyint(1) NOT NULL DEFAULT '0',
  `hasFamilySponsorship` tinyint(1) NOT NULL DEFAULT '0',
  `hasGuaranteesOfOrphans` tinyint(1) NOT NULL DEFAULT '0',
  `hasNeighborhoodCommittees` tinyint(1) NOT NULL DEFAULT '0',
  `hasZakat` tinyint(1) NOT NULL DEFAULT '0',
  `hasAgencyAid` tinyint(1) NOT NULL DEFAULT '0',

  `hasQatarHelp` tinyint(1) NOT NULL DEFAULT '0',

  `chf` int(11) DEFAULT NULL COMMENT 'CHF(نقدية)',
  `oxform` int(11) DEFAULT NULL COMMENT 'Oxform(نقدية)',
  `wfp` int(11) DEFAULT NULL COMMENT 'WFP(نقدية)',
  `family_martyrs` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(نقدية)',
  `prisoners` int(11) DEFAULT NULL COMMENT 'الاسرى(نقدية)',
  `world_bank` int(11) DEFAULT NULL COMMENT 'البنك الدولي(نقدية)',
  `wounded` int(11) DEFAULT NULL COMMENT 'الجرحى(نقدية)',
  `permanent_unemployment` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(نقدية)',
  `social_affairs` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(نقدية)',
  `benefactor` int(11) DEFAULT NULL COMMENT 'فاعل خير(نقدية)',
  `family_sponsorship` int(11) DEFAULT NULL COMMENT 'كفالات أسر(نقدية)',
  `guarantees_of_orphans` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(نقدية)',
  `neighborhood_committees` int(11) DEFAULT NULL COMMENT 'لجان الحي(نقدية)',
  `Zakat` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(نقدية)',
  `agency_aid` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(نقدية)',
  `qatarHelp` int(11) NOT NULL DEFAULT '0',

  `CHFCount` int(11) DEFAULT NULL,
  `OXFAMCount` int(11) DEFAULT NULL,
  `WFPCount` int(11) DEFAULT NULL,
  `familyMartyrsCount` int(11) DEFAULT NULL,
  `prisonersCount` int(11) DEFAULT NULL,
  `worldBankCount` int(11) DEFAULT NULL,
  `woundedCount` int(11) DEFAULT NULL,
  `permanentUnemploymentCount` int(11) DEFAULT NULL,
  `socialAffairsCount` int(11) DEFAULT NULL,
  `familySponsorshipCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesCount` int(11) DEFAULT NULL,
  `zakatCount` int(11) DEFAULT NULL,
  `agencyAidCount` int(11) DEFAULT NULL,
  `qatarHelpCount` int(11) NOT NULL DEFAULT '0',

  `chf_sample` int(11) DEFAULT NULL COMMENT 'CHF (عينية) ',
  `oxform_sample` int(11) DEFAULT NULL COMMENT 'Oxform (عينية) ',
  `wfp_sample` int(11) DEFAULT NULL COMMENT 'WFP(عينية)',
  `family_martyrs_sample` int(11) DEFAULT NULL COMMENT 'اسر الشهداء(عينية)',
  `prisoners_sample` int(11) DEFAULT NULL COMMENT 'الاسرى(عينية)',
  `world_bank_sample` int(11) DEFAULT NULL COMMENT 'البنك الدولي(عينية)',
  `wounded_sample` int(11) DEFAULT NULL COMMENT 'الجرحى(عينية)',
  `permanent_unemployment_sample` int(11) DEFAULT NULL COMMENT 'بطالة دائمة(عينية)',
  `social_affairs_sample` int(11) DEFAULT NULL COMMENT 'شؤون اجتماعية(عينية)',
  `benefactor_sample` int(11) DEFAULT NULL COMMENT 'فاعل خير(عينية)',
  `family_sponsorship_sample` int(11) DEFAULT NULL COMMENT 'كفالات أسر(عينية)',
  `guarantees_of_orphans_sample` int(11) DEFAULT NULL COMMENT 'كفالات أيتام(عينية)',
  `neighborhood_committees_sample` int(11) DEFAULT NULL COMMENT 'لجان الحي(عينية)',
  `Zakat_sample` int(11) DEFAULT NULL COMMENT 'لجان الزكاة(عينية)',
  `agency_aid_sample` int(11) DEFAULT NULL COMMENT 'مساعدات وكالة(عينية)',
  `qatarHelpSample` int(11) NOT NULL DEFAULT '0',

  `chfSampleCount` int(11) DEFAULT NULL,
  `oxformSampleCount` int(11) DEFAULT NULL,
  `wfpSampleCount` int(11) DEFAULT NULL,
  `familyMartyrsSampleCount` int(11) DEFAULT NULL,
  `prisonersSampleCount` int(11) DEFAULT NULL,
  `worldBankSampleCount` int(11) DEFAULT NULL,
  `woundedSampleCount` int(11) DEFAULT NULL,
  `permanentUnemploymentSampleCount` int(11) DEFAULT NULL,
  `socialAffairsSampleCount` bigint(20) DEFAULT NULL,
  `familySponsorshipSampleCount` int(11) DEFAULT NULL,
  `guaranteesOfOrphansSampleCount` int(11) DEFAULT NULL,
  `neighborhoodCommitteesSampleCount` int(11) DEFAULT NULL,
  `zakatSampleCount` bigint(20) DEFAULT NULL,
  `agencyAidSampleCount` bigint(20) DEFAULT NULL,
  `qatarHelpSampleCount` int(11) NOT NULL DEFAULT '0',

  `hasCar` tinyint(1) NOT NULL DEFAULT '0',
  `cars` int(11) DEFAULT NULL COMMENT 'سيارات',

  `hasCattle` tinyint(1) NOT NULL DEFAULT '0',
  `cattle` int(11) DEFAULT NULL COMMENT 'مواشي',

  `hasLands` tinyint(1) NOT NULL DEFAULT '0',
  `lands` int(11) DEFAULT NULL COMMENT 'أراضي',

  `hasProperty` tinyint(1) NOT NULL DEFAULT '0',
  `property` int(11) DEFAULT NULL COMMENT 'عقارات',

  `doors` int(11) DEFAULT NULL COMMENT 'ابواب',
  `safe_lighting` int(11) DEFAULT NULL COMMENT 'انارة امنة',
  `gas_tube` int(11) DEFAULT NULL COMMENT 'انبوبة غاز',
  `cobble` int(11) DEFAULT NULL COMMENT 'بلاط',
  `tv` int(11) DEFAULT NULL COMMENT 'تلفزيون',
  `fridge` int(11) DEFAULT NULL COMMENT 'ثلاجة',
  `solarium` int(11) DEFAULT NULL COMMENT 'حمام شمسي',
  `watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه',
  `sweet_watertank` int(11) DEFAULT NULL COMMENT 'خزان مياه حلوة',
  `closet` int(11) DEFAULT NULL COMMENT 'خزانة ملابس',
  `heater` int(11) DEFAULT NULL COMMENT 'دفاية',
  `windows` int(11) DEFAULT NULL COMMENT 'شبابيك',
  `electricity_network` int(11) DEFAULT NULL COMMENT 'شبكة  كهرباء',
  `water_network` int(11) DEFAULT NULL COMMENT 'شبكة مياه',
  `electric_cooker` int(11) DEFAULT NULL COMMENT 'طنجرة كهرباء',
  `stuff_kitchen` int(11) DEFAULT NULL COMMENT 'عفش المطبخ',
  `gas` int(11) DEFAULT NULL COMMENT 'غاز',
  `washer` int(11) DEFAULT NULL COMMENT 'غسالة',
  `mattresses` int(11) DEFAULT NULL COMMENT 'فرشات',
  `plastic_chairs` int(11) DEFAULT NULL COMMENT 'كراسي بلاستيك/كنب',
  `fan` int(11) DEFAULT NULL COMMENT 'مروحة',
  `top_ceiling` int(11) DEFAULT NULL COMMENT 'نايلون مقوى للسقف',
  `solarPower` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'طاقة شمسية',

  `qualifiedRelationship` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'صلة القرابة بالمعيل',
  `wallsOfHouseAnother` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'حالة الاسقف اخري',

  `hasAnotherHelp` int(11) NOT NULL DEFAULT '0',
  `other1HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

  `other2HelpSource` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

  `other1HelpValue` int(11) NOT NULL DEFAULT '0',
  `other2HelpValue` int(11) NOT NULL DEFAULT '0',

  `other1HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `other2HelpType` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,

  `other1HelpCount` int(11) NOT NULL DEFAULT '0',
  `other2HelpCount` int(11) NOT NULL DEFAULT '0',


  `home_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'رقم المنزل',
  `not_qualified_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `person_present` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: الحالة فعالة\r\n0: طلب تغيير  الحالة لغير فعالة على باحث وتكامل',
  `reason` int(11) DEFAULT NULL COMMENT 'سبب طلب إلغاء فعالية الحالة من باحث وتكامل',

  `search_id` int(11) DEFAULT NULL,

  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_update_data` timestamp NULL DEFAULT NULL,
  `case_status` enum('NEW','INCOMPLETE','COMPLETED','SENT','ACCEPTED','CONFIRMED','RETURNED') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NEW' COMMENT 'حالة الاستمارة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

ALTER TABLE `rewards_cases`
  ADD PRIMARY KEY (`operation_case_id`),
  ADD KEY `id_card_number` (`id_card_number`),
  ADD KEY `operation_case_id` (`operation_case_id`);

ALTER TABLE `rewards_cases` MODIFY `operation_case_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

-- ------------------------------------------------------------------------------------------------------------

INSERT INTO `rewards_cases`(`id`, `family_id`, `id_card_number`, `card_type`, `fullname`, `fullname_en`, `first_name`, `second_name`, `third_name`, `last_name`, `prev_family_name`, `longitude`, `latitude`, `gender`, `marital_status_id`, `deserted`, `birthday`, `birth_place`, `death_date`, `death_cause_id`, `nationality`, `family_cnt`, `spouses`, `male_live`, `female_live`, `refugee`, `unrwa_card_number`, `is_qualified`, `qualified_card_number`, `receivables`, `receivables_sk_amount`, `monthly_income`, `actual_monthly_income`, `has_commercial_records`, `active_commercial_records`, `gov_commercial_records_details`, `phone`, `primary_mobile`, `secondary_mobile`, `watanya`, `charity_id`, `country_id`, `district_id`, `region_id`, `neighborhood_id`, `square_id`, `mosque_id`, `street_address_details`, `street_address`, `needs`, `promised`, `reconstructions_organization_name`, `visitor`, `visitor_card`, `visited_at`, `visitor_evaluation`, `visitor_opinion`, `notes`, `bank1AccountNumber`, `bank1AccountOwner`, `bank1AccountBranch`, `bank1Name`, `bank2AccountNumber`, `bank2AccountOwner`, `bank2AccountBranch`, `bank2Name`, `working`, `work_job`, `work_status`, `work_wage`, `work_location`, `can_work`, `work_reason`, `abilityOfWorkReasonAnother`, `has_other_work_resources`, `property_type`, `roof_material`, `house_condition`, `indoor_condition`, `residence_condition`, `area`, `rooms`, `rent_value`, `need_repair`, `repair_notes`, `health_status`, `health_status_description`, `disease`, `health_insurance`, `health_insurance_number`, `health_insurance_type`, `device_use`, `device_use_name`, `medical_report_summary`, `hasCHF`, `hasOXFAM`, `hasWFP`, `hasFamilyMartyrs`, `hasPrisoners`, `hasWorldBank`, `hasWounded`, `hasPermanentUnemployment`, `hasSocialAffairs`, `hasFamilySponsorship`, `hasGuaranteesOfOrphans`, `hasNeighborhoodCommittees`, `hasZakat`, `hasAgencyAid`, `hasQatarHelp`, `chf`, `oxform`, `wfp`, `family_martyrs`, `prisoners`, `world_bank`, `wounded`, `permanent_unemployment`, `social_affairs`, `benefactor`, `family_sponsorship`, `guarantees_of_orphans`, `neighborhood_committees`, `Zakat`, `agency_aid`, `qatarHelp`, `CHFCount`, `OXFAMCount`, `WFPCount`, `familyMartyrsCount`, `prisonersCount`, `worldBankCount`, `woundedCount`, `permanentUnemploymentCount`, `socialAffairsCount`, `familySponsorshipCount`, `guaranteesOfOrphansCount`, `neighborhoodCommitteesCount`, `zakatCount`, `agencyAidCount`, `qatarHelpCount`, `chf_sample`, `oxform_sample`, `wfp_sample`, `family_martyrs_sample`, `prisoners_sample`, `world_bank_sample`, `wounded_sample`, `permanent_unemployment_sample`, `social_affairs_sample`, `benefactor_sample`, `family_sponsorship_sample`, `guarantees_of_orphans_sample`, `neighborhood_committees_sample`, `Zakat_sample`, `agency_aid_sample`, `qatarHelpSample`, `chfSampleCount`, `oxformSampleCount`, `wfpSampleCount`, `familyMartyrsSampleCount`, `prisonersSampleCount`, `worldBankSampleCount`, `woundedSampleCount`, `permanentUnemploymentSampleCount`, `socialAffairsSampleCount`, `familySponsorshipSampleCount`, `guaranteesOfOrphansSampleCount`, `neighborhoodCommitteesSampleCount`, `zakatSampleCount`, `agencyAidSampleCount`, `qatarHelpSampleCount`, `hasCar`, `cars`, `hasCattle`, `cattle`, `hasLands`, `lands`, `hasProperty`, `property`, `doors`, `safe_lighting`, `gas_tube`, `cobble`, `tv`, `fridge`, `solarium`, `watertank`, `sweet_watertank`, `closet`, `heater`, `windows`, `electricity_network`, `water_network`, `electric_cooker`, `stuff_kitchen`, `gas`, `washer`, `mattresses`, `plastic_chairs`, `fan`, `top_ceiling`, `solarPower`, `qualifiedRelationship`, `wallsOfHouseAnother`, `hasAnotherHelp`, `other1HelpSource`, `other2HelpSource`, `other1HelpValue`, `other2HelpValue`, `other1HelpType`, `other2HelpType`, `other1HelpCount`, `other2HelpCount`, `home_number`, `not_qualified_reason`, `person_present`, `active`, `activeReason`, `reward_id`, `created_at`, `updated_at`, `last_update_data`, `case_status`)

SELECT
operation_cases.id,
operation_cases.family_id,
operation_cases.id_card_number,
CASE	operation_cases.card_type WHEN 'رقم الهوية' THEN	1   WHEN 'بطاقة التعريف' THEN	2    WHEN 'جواز السفر' THEN	3
END AS card_type,
operation_cases.fullname,
operation_cases.fullname_en,
operation_cases.first_name,
operation_cases.second_name,
operation_cases.third_name,
operation_cases.last_name,
operation_cases.prev_family_name,
operation_cases.longitude,
operation_cases.latitude,
CASE WHEN operation_cases.gender = 1 THEN 1  Else 2  END  AS gender,
CASE	marital_status
      WHEN 'أرمل' THEN	40  WHEN 'أرمل / ة' THEN	40
      WHEN 'أعزب / آنسة' THEN	 10
      WHEN 'متزوج' THEN	 20  WHEN 'متزوج / ة' THEN	 20
      WHEN 'متعدد الزوجات' THEN	21
      WHEN 'متوفى / ة' THEN	 4
      WHEN 'مطلق' THEN	30  WHEN 'مطلق  / ة' THEN	30  WHEN 'مطلق / ة' THEN	30
END AS marital_status_id,
CASE WHEN operation_cases.deserted = 1 THEN 1  Else 0  END  AS deserted,
birthday,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.birth_place ) AS birth_place,
death_date,
death_cause_id,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.nationality ) AS nationality,

operation_cases.family_cnt,
operation_cases.spouses,
operation_cases.male_live ,
operation_cases.female_live ,

CASE WHEN operation_cases.refugee = 1 THEN 1  Else 2  END  AS refugee,
CASE WHEN operation_cases.refugee = 2 THEN operation_cases.unrwa_card_number  Else null  END  AS unrwa_card_number,

CASE WHEN operation_cases.is_qualified = 1 THEN 1  Else 0  END  AS is_qualified,
CASE WHEN operation_cases.is_qualified = 1 THEN operation_cases.qualified_card_number  Else null END  AS qualified_card_number,

CASE WHEN operation_cases.receivables = 1 THEN 1 Else 0 END AS receivables,
CASE WHEN operation_cases.receivables = 1 THEN operation_cases.receivables_sk_amount Else 0 END AS receivables_sk_amount,
operation_cases.monthly_income ,
operation_cases.actual_monthly_income ,

CASE WHEN operation_cases.commercial_record = 1 THEN 1 Else 0 END AS has_commercial_records,
CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_active_number Else 0 END AS active_commercial_records,
CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_info Else ' ' END AS gov_commercial_records_details,

operation_cases.phone,
operation_cases.primary_mobile,
operation_cases.secondary_mobile,
operation_cases.watanya,

operation_cases.charity_id,

'دولة فلسطين' AS country,
governorates.name AS district,
regoins.name AS region,
neighborhoods.name AS neighborhood,
squares.name AS square,
mosques.name AS mosque ,
operation_cases.street_address_details AS street_address,
operation_cases.street_address AS street_address,

operation_cases.needs,
CASE  WHEN operation_cases.promised = 1 THEN 1 Else 2 END AS promised,
operation_cases.reconstructions_organization_name,

researchers.fullname AS visitor,
researchers.id_number AS visitor_card,
operation_cases.visited_at,
CASE operation_cases.visitor_evaluation
      WHEN 4 THEN	'ممتاز'
      WHEN 3 THEN	'جيد جدا'
      WHEN 2 THEN		'جيد'
      WHEN 1 THEN	'سيء'
      WHEN 0 THEN	'سيء جدا'
END AS visitor_evaluation,

operation_cases.visitor_opinion,
operation_cases.researcher_recommendations as notes,

bank1AccountNumber, bank1AccountOwner, bank1AccountBranch,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.bank1Name ) AS bank1Name,

bank2AccountNumber, bank2AccountOwner, bank2AccountBranch,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.bank2Name ) AS bank2Name,

CASE WHEN operation_cases.do_working = 1 THEN 1  Else 2  END  AS working,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_title ) AS work_job,
(SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_status ) AS work_status,
operation_cases.Income_by_category AS work_wage,
operation_cases.work_space AS work_location,
CASE WHEN operation_cases.ability_of_work = 1 THEN 1  Else 2  END  AS can_work,
operation_cases.ability_of_work_reason AS work_reason,
operation_cases.abilityOfWorkReasonAnother AS abilityOfWorkReasonAnother,
operation_cases.does_have_source_of_income AS has_other_work_resources,

CASE operation_cases.home_ownership
      WHEN 0 THEN 'ايجار'WHEN 1 THEN 'ايجار'	WHEN 2 THEN 'ملك'
      WHEN 3 THEN 'دون مقابل'	WHEN 4 THEN 'أراضي حكومية'	WHEN 5 THEN 'مستضاف'
  END AS property_type,

CASE operation_cases.walls_of_house
     WHEN 942 THEN	'باطون جيد'		WHEN 956 THEN	'كرفان'
     WHEN 943 THEN	'كرميد'	WHEN 944 THEN	'زينكو'
     WHEN 459442 THEN	'باطون جيد'	WHEN 945 THEN	'خيمة'
     WHEN 946 THEN	'أسبست'	WHEN 947 THEN	'بلاستيك'
     WHEN 948 THEN	'أخرى'	WHEN 949 THEN	'صفيح جديد'
     WHEN 459323 THEN	'أسبست'	WHEN 950 THEN	'صفيح قديم'
     WHEN 459525 THEN	'باطون'	WHEN 951 THEN'باطون قديم'
     WHEN 952 THEN'نصف بطون و نصف صفيح'	WHEN 953 THEN	'جزء باطون والاخر زينكو'
     WHEN 954 THEN	'جزء باطون والاخر أسبست'	WHEN 459466 THEN'باطون قديم'
     WHEN 955 THEN	'جزء زينكو والاخر أسبست'	Else Null
END AS roof_material,

CASE	operation_cases.home_assessment
  		WHEN 63 THEN	'جديد (1-10سنة)'
		  WHEN 64 THEN	'متوسط (11-20سنة)'
		  WHEN 65 THEN	'قديم (أكثر من 20سنة'
		  WHEN 459203 THEN	'متوسط (11-20سنة)'
		  WHEN 459204 THEN	'جديد (1-10سنة)'
END AS house_condition,

CASE operation_cases.placement_of_furniture
		WHEN 1216 THEN	'سئ'	WHEN 1217 THEN	'سئ جدا'
		WHEN 1218 THEN	'قديم'	WHEN 459441 THEN	'لا يوجد'
		WHEN 459443 THEN'سئ'	WHEN 459040 THEN'سئ'
		WHEN 459043 THEN'سئ جدا'	WHEN 34 THEN	'ممتاز'
		WHEN 35 THEN'جيد جدا'	WHEN 459324 THEN'سئ'
		WHEN 459465 THEN	'سئ جدا'	WHEN 1215 THEN	'جيد'
END AS indoor_condition,

CASE operation_cases.home_status
		WHEN 62 THEN	'قديم'	WHEN 459397 THEN	'سيء جدا'
		WHEN 459431 THEN	'سيء'	WHEN 56 THEN	'ممتاز'
		WHEN 57 THEN	'جيد جدا'	WHEN 58 THEN		'جيد'
		WHEN 459325 THEN	'سيء'	WHEN 59 THEN		'متوسط'
		WHEN 60 THEN	'سيء'	WHEN 61 THEN	'سيء جدا'
END AS residence_condition,

operation_cases.house_area_in_meters as area,
operation_cases.room_number as rooms,
operation_cases.rent_value,

CASE WHEN operation_cases.renovate_the_house = 1 THEN 1  Else 2  END  AS need_repair,
( SELECT constants.NAME FROM constants WHERE constants.id = description_of_home_status ) AS repair_notes,

operation_cases.health_status,
( SELECT constants.NAME FROM constants WHERE constants.id = operation_cases.disease ) AS disease,
operation_cases.health_status_description,

CASE WHEN operation_cases.health_insurance = 1 THEN 1  Else 0  END  AS has_health_insurance,
CASE WHEN operation_cases.health_insurance_number is null THEN ' ' Else operation_cases.health_insurance_number END   AS health_insurance_number,
CASE WHEN operation_cases.health_insurance_type is null THEN ' ' Else ( SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.health_insurance_type ) END   AS health_insurance_type,

CASE WHEN operation_cases.device_use = 1 THEN 1 ELSE 0 END AS has_device,
CASE WHEN operation_cases.device_use = 1 THEN operation_cases.device_use_name ELSE null END AS used_device_name,
operation_cases.medical_report_summary,

   hasCHF, hasOXFAM, hasWFP, hasFamilyMartyrs, hasPrisoners, hasWorldBank, hasWounded,
   hasPermanentUnemployment, hasSocialAffairs, hasFamilySponsorship, hasGuaranteesOfOrphans,
    hasNeighborhoodCommittees, hasZakat, hasAgencyAid, hasQatarHelp,

   chf, oxform, wfp, family_martyrs, prisoners, world_bank, wounded, permanent_unemployment, social_affairs,
   benefactor, family_sponsorship, guarantees_of_orphans, neighborhood_committees, Zakat,
   agency_aid, qatarHelp,

   CHFCount, OXFAMCount, WFPCount, familyMartyrsCount, prisonersCount, worldBankCount,
   woundedCount, permanentUnemploymentCount, socialAffairsCount, familySponsorshipCount,
   guaranteesOfOrphansCount, neighborhoodCommitteesCount, zakatCount, agencyAidCount,
    qatarHelpCount,

    chf_sample, oxform_sample, wfp_sample, family_martyrs_sample,
     prisoners_sample, world_bank_sample, wounded_sample, permanent_unemployment_sample,
      social_affairs_sample, benefactor_sample, family_sponsorship_sample,
      guarantees_of_orphans_sample, neighborhood_committees_sample, Zakat_sample,
      agency_aid_sample, qatarHelpSample,

chfSampleCount, oxformSampleCount, wfpSampleCount, familyMartyrsSampleCount,
 prisonersSampleCount, worldBankSampleCount, woundedSampleCount, permanentUnemploymentSampleCount,
  socialAffairsSampleCount, familySponsorshipSampleCount, guaranteesOfOrphansSampleCount,
   neighborhoodCommitteesSampleCount, zakatSampleCount, agencyAidSampleCount,
   qatarHelpSampleCount,

hasCar, cars, hasCattle, cattle, hasLands, lands, hasProperty, property,
doors, safe_lighting, gas_tube, cobble, tv, fridge, solarium, watertank,
 sweet_watertank, closet, heater, windows, electricity_network, water_network,
  electric_cooker, stuff_kitchen, gas, washer, mattresses, plastic_chairs, fan, top_ceiling,
   solarPower, qualifiedRelationship, wallsOfHouseAnother,
    hasAnotherHelp, other1HelpSource, other2HelpSource, other1HelpValue,
     other2HelpValue, other1HelpType, other2HelpType, other1HelpCount,
     other2HelpCount, home_number, not_qualified_reason, person_present,
     active, activeReason, reward_id,
     operation_cases.created_at,
operation_cases.updated_at, last_update_data, case_status
FROM charities
     RIGHT JOIN operation_cases ON charities.id = operation_cases.charity_id
     LEFT JOIN mosques ON operation_cases.mosque_id = mosques.id
     LEFT JOIN squares ON operation_cases.square_id = squares.id
     LEFT JOIN regoins ON operation_cases.region_id = regoins.id
     INNER JOIN neighborhoods ON operation_cases.neighborhood_id = neighborhoods.id
     LEFT JOIN researchers ON operation_cases.researcher_id = researchers.id
     INNER JOIN governorates ON charities.governorate_id = governorates.id
WHERE operation_cases.case_status LIKE 'COMPLETED'  AND operation_cases.reward_id =13;

-- ------------------------------------------------------------------------------------------------------------

UPDATE  rewards_cases set fullname = REPLACE(fullname, 'عبد ال', 'عبدال') where fullname like  '%عبد ال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'ابو ال', 'ابوال') where fullname like  '%ابو ال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'أبو ال', 'أبوال') where fullname like  '%أبو ال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'ابو ', 'ابو') where fullname like  '%ابو %';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'أبو ', 'أبو') where fullname like  '%أبو %';

UPDATE  rewards_cases set
first_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(fullname, ' ', 1), ' ', -1) ,
second_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(fullname, ' ', 2), ' ', -1),
third_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(fullname, ' ', 3), ' ', -1),
last_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(fullname, ' ', 4), ' ', -1)
where 1 ;

UPDATE  rewards_cases set fullname = REPLACE(fullname, 'عبدال', 'عبد ال') where fullname like  '%عبدال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'ابوال', 'ابو ال') where fullname like  '%ابوال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'أبوال', 'أبو ال') where fullname like  '%أبوال%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'ابو', 'ابو ') where fullname like  '%ابو%';
UPDATE  rewards_cases set fullname = REPLACE(fullname, 'أبو', 'أبو ') where fullname like  '%أبو%';

UPDATE  rewards_cases set first_name = REPLACE(first_name, 'عبدال', 'عبد ال') where first_name like  '%عبدال%';
UPDATE  rewards_cases set first_name = REPLACE(first_name, 'ابوال', 'ابو ال') where first_name like  '%ابوال%';
UPDATE  rewards_cases set first_name = REPLACE(first_name, 'أبوال', 'أبو ال') where first_name like  '%أبوال%';
UPDATE  rewards_cases set first_name = REPLACE(first_name, 'ابو', 'ابو ') where first_name like  '%ابو%';
UPDATE  rewards_cases set first_name = REPLACE(first_name, 'أبو', 'أبو ') where first_name like  '%أبو%';

UPDATE  rewards_cases set second_name = REPLACE(second_name, 'عبدال', 'عبد ال') where second_name like  '%عبدال%';
UPDATE  rewards_cases set second_name = REPLACE(second_name, 'ابوال', 'ابو ال') where second_name like  '%ابوال%';
UPDATE  rewards_cases set second_name = REPLACE(second_name, 'أبوال', 'أبو ال') where second_name like  '%أبوال%';
UPDATE  rewards_cases set second_name = REPLACE(second_name, 'ابو', 'ابو ') where second_name like  '%ابو%';
UPDATE  rewards_cases set second_name = REPLACE(second_name, 'أبو', 'أبو ') where second_name like  '%أبو%';


UPDATE  rewards_cases set third_name = REPLACE(third_name, 'عبدال', 'عبد ال') where third_name like  '%عبدال%';
UPDATE  rewards_cases set third_name = REPLACE(third_name, 'ابوال', 'ابو ال') where third_name like  '%ابوال%';
UPDATE  rewards_cases set third_name = REPLACE(third_name, 'أبوال', 'أبو ال') where third_name like  '%أبوال%';
UPDATE  rewards_cases set third_name = REPLACE(third_name, 'ابو', 'ابو ') where third_name like  '%ابو%';
UPDATE  rewards_cases set third_name = REPLACE(third_name, 'أبو', 'أبو ') where third_name like  '%أبو%';

UPDATE  rewards_cases set last_name = REPLACE(last_name, 'عبدال', 'عبد ال') where last_name like  '%عبدال%';
UPDATE  rewards_cases set last_name = REPLACE(last_name, 'ابوال', 'ابو ال') where last_name like  '%ابوال%';
UPDATE  rewards_cases set last_name = REPLACE(last_name, 'أبوال', 'أبو ال') where last_name like  '%أبوال%';
UPDATE  rewards_cases set last_name = REPLACE(last_name, 'ابو', 'ابو ') where last_name like  '%ابو%';
UPDATE  rewards_cases set last_name = REPLACE(last_name, 'أبو', 'أبو ') where last_name like  '%أبو%';

UPDATE  rewards_cases set fullname = REPLACE(fullname, '  ', ' ') where fullname like  '%  %';
UPDATE  rewards_cases set first_name = REPLACE(first_name, '  ', ' ') where first_name like  '%  %';
UPDATE  rewards_cases set second_name = REPLACE(second_name,'  ', ' ') where second_name like  '%  %';;
UPDATE  rewards_cases set third_name = REPLACE(third_name, '  ', ' ') where third_name like  '%  %';
UPDATE  rewards_cases set last_name = REPLACE(last_name, '  ', ' ') where last_name like  '%  %';

UPDATE  rewards_cases set birthday = REPLACE(birthday, '/', '-') where 1;

UPDATE  rewards_cases set phone = NULL where phone = 0;
UPDATE  rewards_cases set primary_mobile = NULL where primary_mobile = 0;
UPDATE  rewards_cases set secondary_mobile = NULL where secondary_mobile = 0;
UPDATE  rewards_cases set watanya = NULL where watanya = 0;

UPDATE  rewards_cases set phone = NULL where phone = '0';
UPDATE  rewards_cases set primary_mobile = NULL where primary_mobile = '0';
UPDATE  rewards_cases set secondary_mobile = NULL where secondary_mobile = '0';
UPDATE  rewards_cases set watanya = NULL where watanya = '0';

UPDATE  rewards_cases set health_status = NULL where health_status = '';
UPDATE  rewards_cases set health_status = NULL where health_status = ' ';
UPDATE  rewards_cases set health_status = 'جيدة' where health_status = NULL;

UPDATE  rewards_cases set unrwa_card_number = NULL where unrwa_card_number = 0;
UPDATE  rewards_cases set qualified_card_number = NULL where qualified_card_number = 0;
UPDATE  rewards_cases set active_commercial_records = NULL , gov_commercial_records_details = NULL where has_commercial_records = 0;

-- ------------------------------------------------------------------------------------------------------------
-- birth_place
UPDATE rewards_cases,char_locations_i18n
set    rewards_cases.birth_place = char_locations_i18n.location_id
where rewards_cases.birth_place = char_locations_i18n.name and char_locations_i18n.language_id = 1  ;

-- nationality
UPDATE rewards_cases,char_locations_i18n
set    rewards_cases.nationality = char_locations_i18n.location_id
where  rewards_cases.nationality = char_locations_i18n.name and char_locations_i18n.language_id = 1  ;

-- death_cause
UPDATE rewards_cases,char_death_causes_i18n
set    rewards_cases.death_cause_id  = char_death_causes_i18n.death_cause_id
where  rewards_cases.death_cause_id  = char_death_causes_i18n.name and char_locations_i18n.language_id = 1  ;

-- bank1
UPDATE rewards_cases,char_banks
set    rewards_cases.bank1Name  = char_banks.id
where  rewards_cases.bank1Name  = char_banks.name  ;

-- branch1
UPDATE rewards_cases,char_branches
set    rewards_cases.bank1AccountBranch  = char_branches.id
where  rewards_cases.bank1AccountBranch  = char_branches.name  and rewards_cases.bank1Name = char_branches.bank_id ;

-- bank2
UPDATE rewards_cases,char_banks
set    rewards_cases.bank2Name  = char_banks.id
where  rewards_cases.bank2Name  = char_banks.name  ;

-- branch2
UPDATE rewards_cases,char_branches
set    rewards_cases.bank2AccountBranch  = char_branches.id
where  rewards_cases.bank2AccountBranch  = char_branches.name  and rewards_cases.bank2Name = char_branches.bank_id ;

-- work_job
UPDATE rewards_cases,char_work_jobs_i18n
set    rewards_cases.work_job  = char_work_jobs_i18n.work_job_id
where  rewards_cases.work_job  = char_work_jobs_i18n.name and char_work_jobs_i18n.language_id = 1  ;

-- work_status
UPDATE rewards_cases,char_work_status_i18n
set    rewards_cases.work_status  = char_work_status_i18n.work_status_id
where  rewards_cases.work_status  = char_work_status_i18n.name and char_work_status_i18n.language_id = 1  ;

-- work_wage
UPDATE rewards_cases,char_work_wages
set    rewards_cases.work_wage  = char_work_wages.id
where  rewards_cases.work_wage  = char_work_wages.name  ;

-- char_work_reasons_i18n
UPDATE rewards_cases,char_work_reasons_i18n
set    rewards_cases.work_reason  = char_work_reasons_i18n.work_reason_id
where  rewards_cases.work_reason  = char_work_reasons_i18n.name and char_work_reasons_i18n.language_id = 1  ;

-- property_type
UPDATE rewards_cases,char_property_types_i18n
set    rewards_cases.property_type  = char_property_types_i18n.property_type_id
where  rewards_cases.property_type  = char_property_types_i18n.name and char_property_types_i18n.language_id = 1  ;

-- roof_material
UPDATE rewards_cases,char_roof_materials_i18n
set    rewards_cases.roof_material  = char_roof_materials_i18n.roof_material_id
where  rewards_cases.roof_material  = char_roof_materials_i18n.name and char_roof_materials_i18n.language_id = 1  ;

-- residence_condition
UPDATE rewards_cases,char_house_status_i18n
set    rewards_cases.residence_condition  = char_house_status_i18n.house_status_id
where  rewards_cases.residence_condition  = char_house_status_i18n.name and char_house_status_i18n.language_id = 1  ;

-- indoor_condition
UPDATE rewards_cases,char_furniture_status_i18n
set    rewards_cases.indoor_condition  = char_furniture_status_i18n.furniture_status_id
where  rewards_cases.indoor_condition  = char_furniture_status_i18n.name and char_furniture_status_i18n.language_id = 1  ;

-- house status
UPDATE rewards_cases,char_building_status_i18n
set    rewards_cases.house_condition  = char_building_status_i18n.building_status_id
where  rewards_cases.house_condition  = char_building_status_i18n.name and char_building_status_i18n.language_id = 1  ;

-- habitable_status
UPDATE rewards_cases,char_habitable_status_i18n
set    rewards_cases.repair_notes  = char_habitable_status_i18n.habitable_status_id
where  rewards_cases.repair_notes  = char_habitable_status_i18n.name and char_habitable_status_i18n.language_id = 1  ;

-- disease
UPDATE rewards_cases,char_diseases_i18n
set    rewards_cases.disease  = char_diseases_i18n.disease_id
where  rewards_cases.disease  = char_diseases_i18n.name and char_diseases_i18n.language_id = 1  ;

-- locations (aid tree )

-- country
UPDATE rewards_cases set country_id = 1 where 1;

-- district
UPDATE rewards_cases,char_aids_locations_i18n
set rewards_cases.district_id = char_aids_locations_i18n.location_id
where rewards_cases.district_id = char_aids_locations_i18n.name  and char_aids_locations_i18n.language_id = 1;

-- ------------------------------------------------------------------------------------------------------------
CREATE TABLE `rewards_cases_locations_map` (
   `id` bigint(20) UNSIGNED NOT NULL,
   `country_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدولة',
   `country` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدولة',

  `district_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	المحافظة',
  `district` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	المحافظة',

  `region_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المنطقة',
  `region` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المنطقة',

  `neighborhood_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	الحي',
  `neighborhood` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '	الحي',

  `square_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المربع',
  `square` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المربع',

  `mosque_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المسجد',
  `mosque` int(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المسجد',
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

  ALTER TABLE `rewards_cases_locations_map`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_card_number` (`id_card_number`),
  ADD KEY `id` (`id`);

ALTER TABLE `rewards_cases_locations_map` MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

-- --------------------------------------------------------------------------------------------------------------------

INSERT INTO `rewards_cases_locations_map`(`id`, `country_id`, `country`, `district_id`, `district`, `region_id`, `region`, `neighborhood_id`, `neighborhood`, `square_id`, `square`, `mosque_id`, `mosque`)

SELECT null as `id`, `country_id`, null as  `country`, `district_id`,null as  `district`, `region_id`, null as `region`, `neighborhood_id`, null as  `neighborhood` , `square_id`, null as  `square`, `mosque_id`, null as  `mosque`
FROM `rewards_cases`
GROUP BY  `country_id`, `district_id`, `region_id`, `neighborhood_id`, `square_id`, `mosque_id`;

-- --------------------------------------------------------------------------------------------
UPDATE rewards_cases_locations_map set  country = 1 where 1;

-- --------------------------------------------------------------------------------------------
update  z_aids_locations_i18n , z_aids_locations
set  z_aids_locations_i18n.parant_id = z_aids_locations.parent_id
where z_aids_locations.id = z_aids_locations_i18n.location_id ;
-- --------------------------------------------------------------------------------------------
-- district
UPDATE rewards_cases_locations_map,z_aids_locations_i18n
set    rewards_cases_locations_map.district = z_aids_locations_i18n.location_id
where rewards_cases_locations_map.district_id = z_aids_locations_i18n.name ;
-- --------------------------------------------------------------------------------------------
-- regoins
UPDATE rewards_cases_locations_map,z_aids_locations_i18n
set    rewards_cases_locations_map.region = z_aids_locations_i18n.location_id
where  rewards_cases_locations_map.region_id = z_aids_locations_i18n.name and
       rewards_cases_locations_map.district = z_aids_locations_i18n.parant_id;

-- ---------------------------------------------------------------------------------------
-- neighborhoods
UPDATE rewards_cases_locations_map,z_aids_locations_i18n
set    rewards_cases_locations_map.neighborhood = z_aids_locations_i18n.location_id
where  rewards_cases_locations_map.neighborhood_id = z_aids_locations_i18n.name and
       rewards_cases_locations_map.region = z_aids_locations_i18n.parant_id;

-- ---------------------------------------------------------------------------------------
-- squares
UPDATE rewards_cases_locations_map,z_aids_locations_i18n
set    rewards_cases_locations_map.square = z_aids_locations_i18n.location_id
where  rewards_cases_locations_map.square_id = z_aids_locations_i18n.name and
       rewards_cases_locations_map.neighborhood = z_aids_locations_i18n.parant_id;

-- ---------------------------------------------------------------------------------------
-- mosque
UPDATE rewards_cases_locations_map,z_aids_locations_i18n
set    rewards_cases_locations_map.mosque = z_aids_locations_i18n.location_id
where  rewards_cases_locations_map.mosque_id = z_aids_locations_i18n.name and
       rewards_cases_locations_map.square = z_aids_locations_i18n.parant_id;




