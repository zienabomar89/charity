-- district
UPDATE rewards_cases_locations_map,governorates
set rewards_cases_locations_map.district = governorates.name
where rewards_cases_locations_map.district_id = governorates.id ;

UPDATE rewards_cases_locations_map,char_aids_locations_i18n
set rewards_cases_locations_map.district_takamul = char_aids_locations_i18n.location_id
 where rewards_cases_locations_map.district = char_aids_locations_i18n.name ;

-- ---------------------------------------------------------------------------------------
-- regoins
UPDATE rewards_cases_locations_map,regoins set rewards_cases_locations_map.region = regoins.name where rewards_cases_locations_map.region_id = regoins.id ;
UPDATE rewards_cases_locations_map,char_aids_locations_i18n
set rewards_cases_locations_map.region_takamul = char_aids_locations_i18n.location_id
where rewards_cases_locations_map.region = char_aids_locations_i18n.name and
      rewards_cases_locations_map.district_takamul = char_aids_locations_i18n.parent_id ;

-- ---------------------------------------------------------------------------------------
-- neighborhoods
UPDATE rewards_cases_locations_map,neighborhoods set rewards_cases_locations_map.neighborhood = neighborhoods.name where rewards_cases_locations_map.neighborhood_id = neighborhoods.id ;
UPDATE rewards_cases_locations_map,char_aids_locations_i18n
set rewards_cases_locations_map.neighborhood_takamul = char_aids_locations_i18n.location_id
 where rewards_cases_locations_map.neighborhood = char_aids_locations_i18n.name and
       rewards_cases_locations_map.region_takamul = char_aids_locations_i18n.parent_id;

-- ---------------------------------------------------------------------------------------
-- squares
UPDATE rewards_cases_locations_map,squares set rewards_cases_locations_map.square = squares.name where rewards_cases_locations_map.square_id = squares.id ;
UPDATE rewards_cases_locations_map,char_aids_locations_i18n
set rewards_cases_locations_map.square_takamul = char_aids_locations_i18n.location_id
 where rewards_cases_locations_map.square = char_aids_locations_i18n.name and
       rewards_cases_locations_map.neighborhood_takamul = char_aids_locations_i18n.parent_id;

-- ---------------------------------------------------------------------------------------
-- mosque
UPDATE rewards_cases_locations_map,mosques set rewards_cases_locations_map.mosque = mosques.name where rewards_cases_locations_map.mosque_id = mosques.id ;
UPDATE rewards_cases_locations_map,char_aids_locations_i18n
set rewards_cases_locations_map.mosque_takamul = char_aids_locations_i18n.location_id
 where rewards_cases_locations_map.mosque = char_aids_locations_i18n.name and
       rewards_cases_locations_map.square_takamul = char_aids_locations_i18n.parent_id;


-- ---------------------------------------------------------------------------------------

UPDATE `z_aids_locations_i18n` ,  `z_aids_locations`
SET `z_aids_locations_i18n`.`parant_id` = `z_aids_locations`.`parent_id`
where `z_aids_locations_i18n`.`location_id` =  `z_aids_locations`.`id`

-- ---------------------------------------------------------------------------------------
UPDATE `z_rewards_cases` SET district_id = 1 WHERE district_id = 'محافظة شرق غزة'  ;
UPDATE `z_rewards_cases` SET district_id = 2 WHERE district_id = 'محافظات الوسطى'  ;
UPDATE `z_rewards_cases` SET district_id = 3 WHERE district_id = 'محافظة خانيونس'  ;
UPDATE `z_rewards_cases` SET district_id = 4 WHERE district_id = 'محافظة شمال غزة'  ;
UPDATE `z_rewards_cases` SET district_id = 5 WHERE district_id = 'محافظة غرب غزة'  ;
UPDATE `z_rewards_cases` SET district_id = 6 WHERE district_id = 'محافظة جنوب غزة'  ;
UPDATE `z_rewards_cases` SET district_id = 7 WHERE district_id = 'محافظة رفح'  ;

-- ---------------------------------------------------------------------------------------

UPDATE z_rewards_cases,z_aids_locations_i18n
set z_rewards_cases.region_id = z_aids_locations_i18n.location_id
 where z_rewards_cases.region_id = z_aids_locations_i18n.name and
       z_rewards_cases.district_id = z_aids_locations_i18n.parant_id ;

-- ---------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------
UPDATE `z_rewards_cases` SET card_type = 1 WHERE card_type = 'رقم الهوية'  ;
UPDATE `z_rewards_cases` SET card_type = 2 WHERE card_type = 'بطاقة التعريف'  ;
UPDATE `z_rewards_cases` SET card_type = 3 WHERE card_type = 'جواز السفر'  ;

-- --------------------------------------------------------------------------------

UPDATE `z_rewards_cases` SET  marital_status = 40  WHERE marital_status = 'أرمل' ;
UPDATE `z_rewards_cases` SET  marital_status = 40  WHERE marital_status = 'أرمل / ة' ;
UPDATE `z_rewards_cases` SET  marital_status = 10  WHERE marital_status = 'أعزب / آنسة'  ;
UPDATE `z_rewards_cases` SET  marital_status = 20 WHERE marital_status = 'متزوج' ;
UPDATE `z_rewards_cases` SET  marital_status = 20  WHERE marital_status = 'متزوج / ة' ;
UPDATE `z_rewards_cases` SET  marital_status = 21  WHERE marital_status = 'متعدد الزوجات' ;
UPDATE `z_rewards_cases` SET  marital_status = 4  WHERE marital_status = 'متوفى / ة' ;
UPDATE `z_rewards_cases` SET  marital_status = 30  WHERE marital_status = 'مطلق' ;
UPDATE `z_rewards_cases` SET  marital_status = 30  WHERE marital_status = 'مطلق  / ة'  ;
UPDATE `z_rewards_cases` SET  marital_status = 30  WHERE marital_status = 'مطلق / ة' ;

-- --------------------------------------------------------------------------------
UPDATE z_rewards_cases,mosques set z_rewards_cases.mosque_id = mosques.name where z_rewards_cases.mosque_id = mosques.id ;
UPDATE z_rewards_cases,squares set z_rewards_cases.square_id = squares.name where z_rewards_cases.square_id = squares.id ;
UPDATE z_rewards_cases,regoins set z_rewards_cases.region_id = regoins.name where z_rewards_cases.region_id = regoins.id ;
UPDATE z_rewards_cases,neighborhoods set z_rewards_cases.neighborhood_id = neighborhoods.name where z_rewards_cases.neighborhood_id = neighborhoods.id ;
UPDATE z_rewards_cases,governorates set z_rewards_cases.district = governorates.name where z_rewards_cases.governorate_id = governorates.id ;
UPDATE z_rewards_cases,researchers set z_rewards_cases.visitor = researchers.fullname where z_rewards_cases.researcher_id = researchers.id ;

UPDATE z_rewards_cases,researchers set z_rewards_cases.visitor_card = researchers.id_number where z_rewards_cases.researcher_id = researchers.id ;

researchers.id_number AS visitor_card,
