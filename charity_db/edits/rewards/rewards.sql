

CREATE TABLE `rewards_cases_members` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_card_number` int(11) DEFAULT NULL COMMENT 'رقم الهوية',
  `family_id` int(11) DEFAULT NULL,
  `operation_case_id` int(11) DEFAULT NULL,
  `new_operation_case_id` int(11) DEFAULT NULL,
  `father_id_card` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم ',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الاسم الأول',
  `second_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الأب',
  `third_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم الجد',
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'اسم العائلة',
  `gender` tinyint(1) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 0- أنثى',
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'مكان الميلاد - جدول ثوابت',
  `marital_status_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الاجتماعية ',
  `kinship_id` text COLLATE utf8mb4_unicode_ci COMMENT 'صلة القرابة ( رب الأسرة ) ',
  `primary_mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monthly_income` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' الدخل الشهري بالشيكل',
  `study` tinyint(1) DEFAULT NULL COMMENT 'يدرس',
  `study_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'نوع الدراسة',
  `school` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المؤسسة التعليمية',
  `degree` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الدرجة العلمية',
  `stage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'المرحلة الدراسية',
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الصف',
  `working` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'هل يعمل ',
  `work_job_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT ' المسمى الوظيفي ',
  `condition` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'الحالة الصحية',
  `details` text COLLATE utf8mb4_unicode_ci COMMENT 'تفاصيل الحالة الصحية',
  `disease_id` text COLLATE utf8mb4_unicode_ci COMMENT 'المرض أو الاعاقة',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prayer` tinyint(1) DEFAULT NULL,
  `prayer_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quran_center` int(11) DEFAULT NULL,
  `doPreserveQuran` int(11) DEFAULT NULL,
  `quran_parts` int(11) DEFAULT NULL,
  `quran_reason` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `rewards_cases_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `father_id_card` (`father_id_card`),
  ADD KEY `id_card_number` (`id_card_number`);


ALTER TABLE `rewards_cases_members`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;