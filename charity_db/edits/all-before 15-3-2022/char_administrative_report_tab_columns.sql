
ALTER TABLE `char_administrative_report` ADD `category` TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'نوع التقرير : مركزي او مخصص ' AFTER `title`;

CREATE TABLE `char_administrative_report_tab_columns` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `tab_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم التبويبة',
  `name` varchar(255) NOT NULL COMMENT 'الاسم باللغة الانجليزية',
  `label` varchar(255) DEFAULT '' COMMENT 'مسمى العمود',
  `priority` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'تصنيف العمود :  نص - عدد صحيح  -  عدد عشري - تاريخ - مساحة نصية ',
  `type` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'تصنيف العمود :  نص - عدد صحيح  -  عدد عشري - تاريخ - مساحة نصية '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول ‘مدة التبويبات ل التقارير الادارية المخصصة ' ROW_FORMAT=COMPACT;

ALTER TABLE `char_administrative_report_tab_columns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_tab_columns_tabId_idx` (`tab_id`);

ALTER TABLE `char_administrative_report_tab_columns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=46;

ALTER TABLE `char_administrative_report_tab_columns`
  ADD CONSTRAINT `fk_char_administrative_report_tab_columns_tabId` FOREIGN KEY (`tab_id`) REFERENCES `char_administrative_report_tab` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;



--
-- Table structure for table `char_administrative_report_data`
--

CREATE TABLE `char_administrative_report_data` (
  `organization_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `column_id` int(10) UNSIGNED NULL,
  `list_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'في حال كان الحقل متعدد الخيارات تخزن هنا قيمة الخيار أو تترك فارغة في حالة الحقول غير متعددة الخيارات',
  `value` text,
  `amount` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على البيانات الخاصة بالجمعيات لكل حقل في التقرير';


--
-- Indexes for table `char_administrative_report_data`
--
ALTER TABLE `char_administrative_report_data`
--   ADD PRIMARY KEY (`organization_id`,`option_id`,`column_id`),
  ADD KEY `fk_administrative_report_data_organization_id_idx` (`organization_id`),
  ADD KEY `fk_administrative_report_data_list_id_idx` (`list_id`),
  ADD KEY `fk_administrative_report_data_column_id_idx` (`column_id`),
  ADD KEY `fk_administrative_report_data_option_id_idx` (`option_id`);

--
-- Constraints for table `char_administrative_report_data`
--
ALTER TABLE `char_administrative_report_data`
  ADD CONSTRAINT `fk_administrative_report_data_list_id` FOREIGN KEY (`list_id`) REFERENCES `char_administrative_report_tab_option_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrative_report_data_column_id` FOREIGN KEY (`column_id`) REFERENCES `char_administrative_report_tab_columns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrative_report_data_option_id` FOREIGN KEY (`option_id`) REFERENCES `char_administrative_report_tab_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrative_report_data_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);




CREATE TABLE `char_administrative_report_tab_column_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `column_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `char_administrative_report_tab_column_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_tab_column_id_idx` (`column_id`);

ALTER TABLE `char_administrative_report_tab_column_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `char_administrative_report_tab_column_list`
  ADD CONSTRAINT `fk_char_administrative_report_tab_column_id` FOREIGN KEY (`column_id`) REFERENCES `char_administrative_report_tab_columns` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `char_administrative_report_data` ADD `column_list_id` INT NULL AFTER `column_id`;
