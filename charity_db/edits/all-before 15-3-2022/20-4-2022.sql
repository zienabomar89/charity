DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_case_file`(`id` INT) RETURNS text CHARSET utf32
    COMMENT 'ارجاع مرفقات حالة ما بناء على معرف الحالة'
BEGIN
	declare c text;
    set c =
            (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_cases_files`
              join `doc_files` on `doc_files`.`id` = `char_cases_files`.`file_id`
              where `char_cases_files`.`case_id`=id
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `CHAR_GET_FORM_ELEMENT_PRIORITY`(`p_category_id` INT, `p_element_name` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع خيار الأولوية لحقل ما'
BEGIN
                DECLARE v_priority INT;
    SET v_priority = (SELECT `e`.`priority`
                        FROM `char_categories_form_element_priority` `e`
                        JOIN `char_forms_elements` AS `el` on `el`.`id` =`e`.`element_id` and `el`.`name` = p_element_name
						where`e`.`category_id` = p_category_id);
RETURN v_priority;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_beneficiary_voucher_amount`(`id` INT) RETURNS int(11)
BEGIN
declare c INTEGER;
    	set c = (
  					  select SUM(`voucher_value`) AS `amount`
				  from `char_vouchers_persons`
				  where `char_vouchers_persons`.`voucher_id` =id
			     );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_case_rank`(`c_id` INT) RETURNS float
    COMMENT 'دالة لارجاع محصلة الوزن لحالة ما بناء على رقم الحالة'
BEGIN
	DECLARE v_rank FLOAT;
	SET v_rank := (select (
      ifnull(`char_death_causes`.`weight`, 0)
    + ifnull(`char_marital_status`.`weight`, 0)
    + ifnull(`char_diseases`.`weight`, 0)
    + ifnull(`char_roof_materials`.`weight`, 0)
    + ifnull(`char_property_types`.`weight`, 0)
    + ifnull(`char_work_status`.`weight`, 0)
    + ifnull(`char_work_reasons`.`weight`, 0)
    + ifnull(`char_work_jobs`.`weight`, 0)
    + ifnull(`char_work_wages`.`weight`, 0)
    + sum(ifnull(`char_aid_sources_weights`.`weight`, 0))
    + sum(ifnull(`char_essentials_weights`.`weight`, 0))
    + sum(ifnull(`char_properties_weights`.`weight`, 0))) AS rank
	from `char_cases`
	inner join  `char_persons` on `char_cases`.`person_id` = `char_persons`.`id`
	left join `char_death_causes` on `char_persons`.`death_cause_id` = `char_death_causes`.`id`
	left join `char_marital_status` on `char_persons`.`marital_status_id` = `char_marital_status`.`id`
	left join `char_persons_health` on `char_cases`.`person_id` = `char_persons_health`.`person_id`
	left join `char_diseases` on `char_persons_health`.`disease_id` = `char_diseases`.`weight`
    left join `char_residence` on `char_cases`.`person_id` = `char_residence`.`person_id`
    left join `char_property_types` on `char_residence`.`property_type_id` = `char_property_types`.`id`
    left join `char_roof_materials` on `char_residence`.`roof_material_id` = `char_roof_materials`.`id`
    left join `char_persons_properties` on `char_cases`.`person_id` = `char_persons_properties`.`person_id`
    left join `char_properties_weights` on `char_persons_properties`.`property_id` = `char_properties_weights`.`property_id`
    AND `char_persons_properties`.`quantity` BETWEEN `char_properties_weights`.`value_min` AND `char_properties_weights`.`value_max`
	left join `char_persons_work` on `char_cases`.`person_id` = `char_persons_work`.`person_id`
	left join `char_work_status` on `char_persons_work`.`work_status_id` = `char_work_status`.`id`
	left join `char_work_reasons` on `char_persons_work`.`work_reason_id` = `char_work_reasons`.`id`
	left join `char_work_jobs` on `char_persons_work`.`work_job_id` = `char_work_jobs`.`id`
	left join `char_work_wages` on `char_persons_work`.`work_wage_id` = `char_work_wages`.`id`
    left join `char_persons_aids` on `char_cases`.`person_id` = `char_persons_aids`.`person_id`
    left join `char_aid_sources_weights` on `char_persons_aids`.`aid_source_id` = `char_aid_sources_weights`.`aid_source_id`
    AND `char_persons_aids`.`aid_value` BETWEEN `char_aid_sources_weights`.`value_min` AND `char_aid_sources_weights`.`value_max`
    left join `char_cases_essentials` on `char_cases`.`id` = `char_cases_essentials`.`case_id`
    left join `char_essentials_weights` on `char_cases_essentials`.`essential_id` = `char_essentials_weights`.`essential_id`
    AND `char_cases_essentials`.`condition` = `char_essentials_weights`.`value`
    WHERE `char_cases`.`id` = c_id);
RETURN v_rank;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_case_essentials_need`(`id` INT) RETURNS text CHARSET utf8
    COMMENT 'دالة لارجاع احتياجات العائلة من الأثاث  '
BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_essentials`.`name` SEPARATOR ' - ' )
             FROM `char_cases_essentials`
             join `char_essentials` ON `char_essentials`.`id` = `char_cases_essentials`.`essential_id`
             where `char_cases_essentials`.`case_id`=id and
                  (`char_cases_essentials`.`needs` is not null ) and
                  ( `char_cases_essentials`.`needs` != 0)
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_active_case_count`(`id` INT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الحالات الفعالة للشخص لدى كافة الجمعيات'
BEGIN
     declare c INTEGER;
     set c = ( SELECT COUNT(`char_cases`.`id`)
               FROM `char_cases`
               WHERE `char_cases`.`status` = 0 and `char_cases`.`person_id` = id and  `char_cases`.`category_id` in (SELECT  `char_categories`.`id`  FROM `char_categories` WHERE `type` = 2) ) ;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_children_count`(`person_id` INT(10), `gender` TINYINT(4)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأبناء (ذكور أو اناث او الكل) لشخص ما بناء على معرف الشخص'
BEGIN
declare c INTEGER;
   	if gender is null then
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE `a`.`father_id` = person_id  or `a`.`mother_id` = person_id
					);
	       else
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE (`a`.`father_id` = person_id  or `a`.`mother_id` = person_id) and `a`.`gender`= gender
					);
	       end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_case_aid_sources`(`id` INT) RETURNS text CHARSET latin1
    COMMENT 'دالة لارجاع مصادر المساعدات لشخص ما بناء على معرف الشخص '
BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_aid_sources`.`name` SEPARATOR ' - ' )
FROM `char_persons_aids`
join `char_aid_sources` ON `char_aid_sources`.`id` = `char_persons_aids`.`aid_source_id`
where `char_persons_aids`.`person_id`=id  and `char_persons_aids`.`aid_take` =1
    );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_family_total_category_payments`(`father` INT(10), `p_date_from` DATE, `p_date_to` DATE, `category` INT(10)) RETURNS double
    COMMENT 'دالة لارجاع كافة مبالغ كفالة ما  تلقتها عائلة خلال فترة معينة من خلال كود معرف الأب'
BEGIN
  declare sum double;
  IF category is null THEN
   if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                               AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     end if;
     ELSE
     if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                                AND   ((`char_cases`.`category_id` = category ))
                                AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
        end if;
     END IF;
RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_guardian_persons_count`(`p_guardian_id` INTEGER) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المعاليين  لشخص ما بناء على معرف الشخص'
BEGIN
	DECLARE c INTEGER;
    set c = (SELECT COUNT(`individual_id`) FROM `char_guardians` WHERE `guardian_id` = p_guardian_id);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_count_voucher_persons`(`id` INT(1)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأشخاص المستفيدين من قسيمة معينة بناء على رقم القسيمة'
BEGIN
declare c INTEGER;
		set c = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons`
        from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_family_count`(`p_via` VARCHAR(15), `master_id` INT(10), `gender` INT(4), `edge_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
BEGIN
declare c INTEGER;
      if p_via = 'left' then
	if gender is null then
				set c = (
						  SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
					      WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
		                );
	       else
				set c = (
						 SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
			  			 JOIN  `char_persons` ON ((`char_persons`.`id`=`k`.`r_person_id`) and (`char_persons`.`gender`= gender))
						 WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
	                    );
	       end if;
    elseif p_via = 'right' then
		if gender is null then
				set c = (
  						   SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			      		   WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id
			                 );
	       else
				set c = (
						  SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			    		 JOIN  `char_persons` ON ( (`char_persons`.`id`=`k`.`l_person_id`) and  (`char_persons`.`gender`= gender ))
						 WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id
	                    );
	        end if;
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_payment_document_type_id`(`pId` INT, `type_` TINYINT) RETURNS int(11)
    COMMENT 'دالة لارجاع رقم المرفق المرفوع ضمن صرفية بناء على نوع المرفق'
BEGIN
	declare c int;
    set c =  (select `char_payments_documents`.`document_id` FROM `char_payments_documents`
              where `char_payments_documents`.`payment_id` = pId AND
                    `char_payments_documents`.`attachment_type`=type_
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_family_total_payments`(`father` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
    COMMENT 'دالة لارجاع كافة المبالغ التي تلقتها عائلة خلال فترة معينة من خلال كود معرف الأب'
BEGIN
  declare sum double;
    if p_date_from is null and p_date_to is null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                       )
              );
  elseif p_date_from is not null and p_date_to is null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       )
               );
    elseif p_date_from is null and p_date_to is not null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       )
              );
    elseif p_date_from is not null and p_date_to is not null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       )
              );
    end if;
RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_payment_document_type_count`(`pId` INT, `type_` TINYINT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المرفقات المحملة لصرفية ما من نوع مرفق ما '
BEGIN
	declare c int;
    set c =  (select count(`char_payments_documents`.`document_id`) FROM `char_payments_documents`
              where `char_payments_documents`.`payment_id` = pId AND
                    `char_payments_documents`.`attachment_type` = type_
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_organizations_child_nominated_count`(`o_id` INT ,`p_id` INT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المرشحين للجمعيات الأبناء لجمعية  ما ضمن مشروع معين من خلال رقم المشروع ورقم الجمعية'
BEGIN
declare c INTEGER;
      set c = ( select COUNT(*) FROM `aid_projects_persons`
                WHERE `project_id` = p_id and
                organization_id in (SELECT char_organizations.id FROM char_organizations where parent_id = o_id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_persons_properties`(`id` INT) RETURNS text CHARSET utf8
    COMMENT 'دالة لارجاع ممتلكات شخص ما'
BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_properties_i18n`.`name` SEPARATOR ' - ' )
             FROM `char_persons_properties`
             join `char_properties_i18n` ON `char_properties_i18n`.`property_id` = `char_persons_properties`.`property_id` and    `char_properties_i18n`.`language_id` =1
             where `char_persons_properties`.`person_id`=id
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_organization_nominated_count`(`o_id` INT ,`p_id` INT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المرشحين لجمعية ما ضمن مشروع معين من خلال رقم المشروع ورقم الجمعية'
BEGIN
declare c INTEGER;
      set c = ( select COUNT(*) FROM `aid_projects_persons`   WHERE `project_id` = p_id and  organization_id  = o_id);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_project_organization_data`(`proId` INT, `key_id` INT, `key_type` TINYINT(4), `target` TINYINT(4)) RETURNS float
    COMMENT 'دالة لارجاع  اجمالي القيمة او الكمية او النسبة للجمعية ضمن مشروع معين'
BEGIN
DECLARE c  FLOAT;
    if target is null then
            if key_type  = 0 then
               set c = ( select `quantity` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));
            elseif key_type  = 1 then
                   set c = ( select SUM(`quantity`)
                             from `aid_projects_organizations`
                             where ((`project_id` = proId ) and
                                    (`organization_id` in ( select char_user_organizations.organization_id
                                                            from char_user_organizations
                                                            where char_user_organizations.user_id = key_id) ))
                           );
            elseif key_type  = 2 then
                   set c = ( select `quantity` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));
            end if;
    else
            if key_type = 0 then
               set c = ( select `ratio` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));
            elseif key_type  = 1 then
                 set c = ( select SUM(`ratio`)
                           from `aid_projects_organizations`
                           where ((`project_id` = proId ) and
                                   (`organization_id` in ( select char_user_organizations.organization_id
                                                           from char_user_organizations
                                                           where char_user_organizations.user_id = key_id) ))
                        );
            elseif key_type  = 2 then
                   set c = ( select `ratio` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));
            end if;
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_project_person_organization_count`(`proId` INT, `key_id` INT, `key_type` TINYINT(4)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المرشحين لمشروع معين اما ضمن جمعية او أبناء لجمعية او جمعيات تابعة لجمعية'
BEGIN
DECLARE c  INT;
    if key_type  = 0 then
				 			set c = (
				 			          select count(`person_id`) from `aid_projects_persons`
				 			          where (`organization_id` = key_id  and `project_id` = proId )
				 			        );
		elseif key_type  = 1 then
				 		set c = (
			          select count(`person_id`)
			          from `aid_projects_persons`
			          where (
			                  (`project_id` = proId ) and
			                  (`organization_id` in ( select char_user_organizations.organization_id
			                                          from char_user_organizations
			                                          where char_user_organizations.user_id = key_id) )
			                )
			        );
		elseif key_type  = 2 then
				 	set c = (
			          select count(`person_id`)
			          from `aid_projects_persons`
			          where (
			                  (`project_id` = proId ) and
			                  (`organization_id` in ( select char_organizations_closure.descendant_id
			                                          from char_organizations_closure
			                                          where char_organizations_closure.ancestor_id = key_id) )
			                )
			        );
		end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_siblings_count`(`p_person_id` INT, `p_via` VARCHAR(15), `gender` INT(4)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الاخوة  (ذكور أو اناث او الكل)  عن طريق الام أو الاب لشخص ما بناء على معرف الشخص'
BEGIN
	declare c INTEGER;
      if p_via = 'mother' then
      	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`mother_id` = (
								select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	       else
	       	set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`mother_id` = (
							select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);
	       end if;
    elseif p_via = 'father' then
     	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`father_id` = (
								select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	    else
			set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`father_id` = (
							select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);
	    end if;
    else
     	if gender is null then
		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id
			)) and `a`.`id` !=  p_person_id
		);
	    else
		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id and `c`.`gender`= gender
			)) and `a`.`id` !=  p_person_id
		);
	    end if;
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_person_average_payments`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
    COMMENT 'دالة لارجاع متوسط المبالغ التي تلقتها حالة ما خلال فترة معينة'
BEGIN
  declare average double;
    if p_date_from is null and p_date_to is null then
    set average = (SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                       (`char_cases`.`person_id` = p_person_id) and
                       ( (`p`.`status` = 1) or
                           (`p`.`status` = 2)
                       ))) ;
  elseif p_date_from is not null and p_date_to is null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and  ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                    )
                   ) ;
    elseif p_date_from is null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;
    elseif p_date_from is not null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and
                        ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                        ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;
    end if;
RETURN average;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_person_total_payments`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
    COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها شخص ما خلال فترة معينة'
BEGIN
  declare sum double;
    if p_date_from is null and p_date_to is null then
    set sum = (SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or
                                 (`p`.`status` = 2)
                              ))) ;
  elseif p_date_from is not null and p_date_to is null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         )
                   ) ;
    elseif p_date_from is null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;
    elseif p_date_from is not null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ((`p`.`status` = 1) or (`p`.`status` = 2)) and
                              ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                              ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;
    end if;
RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_siblings_counts`(`p_person_id` INT(10), `l_person_id` INT(10), `e_person_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الاخوة  (ذكور أو اناث او الكل)  عن طريق الام أو الاب لشخص ما بناء على معرف الشخص'
BEGIN
	declare c INTEGER;
       if l_person_id is null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` =  p_person_id)
                  );
       elseif l_person_id is not null and e_person_id is not null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` = l_person_id) and (`k`.`r_person_id` != p_person_id) and (`k`.`r_person_id` != e_person_id)
                  );
       elseif l_person_id is not null and e_person_id is null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` = l_person_id) and (`k`.`r_person_id` != p_person_id)
                  );
       end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsor_voucher_person_amount`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب قيمة القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsorship_cases_documents_count`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد مرفقات التقرير المحملة لحالة معين ولنوع مرفق معينضمن فترة تقرير ما'
BEGIN
	declare c int;
    set c =  (select count(`char_sponsorship_cases_documents`.`id`)
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsorship_count`(`case_id` INT, `status` TINYINT , `organization_id` INT) RETURNS int(11)
    COMMENT 'دالة لجلب عدد الجهات الكافلة المرشح او المكفول لهها حالة ما'
BEGIN
  declare c int;
    set c =  (select count(`char_sponsorship_cases`.`id`)
              FROM `char_sponsorship_cases`
              join `char_sponsorships`   on `char_sponsorships` .`id` = `char_sponsorship_cases` .`sponsorship_id` and  `char_sponsorships` .`organization_id` = organization_id
              where `char_sponsorship_cases`.`case_id`=case_id AND
                    `char_sponsorship_cases`.`status`=status );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsorship_cases_documents_file_path`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS text CHARSET utf8
    COMMENT 'دالة لارجاع مسار الملف المرفق لتقرير جهة كافلة حسب نوع المرفق و تصنيفه '
BEGIN
	declare c text;
    set c =  (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsor_voucher_person_count`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_voucher_persons`(`id` INT, `p_type` TINYINT(3), `status` TINYINT(3)) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة'
BEGIN
	DECLARE c INTEGER;
 if p_type is null and status is null then
			set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id
            );
 elseif p_type is null and status is NOT null then
				set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`status` =  status
            );
 elseif ( p_type = 1  OR p_type = 2 )and status is null then
			set c = (
               SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
               WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`type` =  p_type
            );
 elseif ( p_type = 1  OR p_type = 2 )and status is NOT null then
				set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`type` =  p_type AND  `char_vouchers_persons`.`status` =  status
            );
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_persons_receipt_count`(`vid` INT(11) , `pid` INT(11)) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة'
BEGIN
declare c INTEGER;
		set c = (select COUNT(`char_vouchers_persons`.`id`) AS `persons`
        from `char_vouchers_persons`
        where (`char_vouchers_persons`.`voucher_id` =vid) and (`char_vouchers_persons`.`person_id` =pid));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_voucher_persons_count`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة'
BEGIN
	DECLARE c INTEGER;
 if p_date_from is null and p_date_to is null then
		set c = (
              SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif p_date_from is not null and p_date_to is null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif p_date_from is null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif p_date_from is not null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_org_case_count`(`org_id` INT(10), `ctype` INT(4)) RETURNS int(11)
    COMMENT 'دالة لجلب عدد الحالات المسجلة لدى جمعية معينة'
BEGIN
declare c INTEGER;
 	set c = (
		     SELECT COUNT(`char_cases`.`id`) FROM `char_cases`
		     WHERE `char_cases`.`status` = 1  and
		           `char_cases`.`organization_id` = org_id  and
		           `char_cases`.`category_id` in (SELECT `c`.`id` FROM `char_categories` `c` where `c`.type = ctype)
		    );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsorship_cases_documents_id`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS text CHARSET latin1
    COMMENT 'دالة لارجاع رقم الملف المرفق لتقرير جهة كافلة حسب نوع المرفق و تصنيفه '
BEGIN
	declare c text;
   if document_type_id = 5 or document_type_id = 7 then
          set c =  (select GROUP_CONCAT(DISTINCT `char_sponsorship_cases_documents`.`id` SEPARATOR ',' )
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
    else
 set c =  (select `char_sponsorship_cases_documents`.`id`
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_voucher_total_amount`(`id` INT(1) ,`p_via` VARCHAR(15)) RETURNS int(11)
    COMMENT 'دالة لجلب  اجمالي المبلغ في القسيمة بعملة القسيمة'
BEGIN
declare c INTEGER;
      if p_via = 'o' then
	set c = (select (`char_vouchers`.`value` * `char_vouchers`.`count`) AS `amount`
                         from `char_vouchers`
                         where (`char_vouchers`.`id` =id));
     elseif p_via = 'sh' then
		set c = (select (`char_vouchers`.`value` * `char_vouchers`.`count` * `char_vouchers`.`exchange_rate`) AS `amount`
                         from `char_vouchers`
                         where (`char_vouchers`.`id` =id));
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_total_voucher_amount`(`id` INT(1)) RETURNS int(11)
    COMMENT 'دالة لجلب  اجمالي المبلغ المستحق لشخص ما ضمن قسيمة معينة'
BEGIN
declare c INTEGER;
		set c = (select (`char_vouchers`.`exchange_rate` * `char_vouchers`.`value` * char_count_voucher_persons(`char_vouchers`.`id`)) AS `amount`
                 from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_cases_count`(`organization_id` INT, `location_id` INT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الحالات التابعة لمنطقة معينة المسجلة لدى جمعية ما'
BEGIN
declare c INTEGER;
 set c = (
  				 SELECT COUNT(`char_cases`.`person_id`)
  				 FROM `char_cases`
  				 join `char_persons` ON (`char_cases`.`person_id` = `char_persons`.`id` and `char_persons`.`location_id` = location_id )
			     WHERE `char_cases`.`status` = 0 and `char_cases`.`organization_id` = organization_id
			   );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_voucher_amount`(`id` INT(1)) RETURNS int(11)
    COMMENT 'دالة لاحتساب اجمالي المبلغ الخاص بقسيمة معينة'
BEGIN
declare c INTEGER;
declare d INTEGER;
		set d = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons` from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));
		set c = (select (`char_vouchers`.`value` * d) AS `amount` from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_case_documents`(`id` INT) RETURNS text CHARSET utf8
    COMMENT 'دالة لارجاع ارقام الملفات المرفقة لحالة '
BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_cases_files`
          where `case_id`=id
		);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_cases_essentials_count`(`id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد مستلزمات الأثاث لدى حالة ما'
BEGIN
	declare c INTEGER;
    set c = (
			select COUNT(`a`.`essential_id`)
			FROM `char_cases_essentials` AS `a`
			WHERE `a`.`case_id` = id
		);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `clear_text`(`p_text` TEXT) RETURNS text CHARSET utf8
    COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات'
BEGIN
	SET p_text = normalize_text_ar(p_text);
    SET p_text = REPLACE(p_text, '1', '');
    SET p_text = REPLACE(p_text, '2', '');
    SET p_text = REPLACE(p_text, '3', '');
    SET p_text = REPLACE(p_text, '4', '');
    SET p_text = REPLACE(p_text, '5', '');
    SET p_text = REPLACE(p_text, '6', '');
    SET p_text = REPLACE(p_text, '7', '');
    SET p_text = REPLACE(p_text, '8', '');
    SET p_text = REPLACE(p_text, '9', '');
    SET p_text = REPLACE(p_text, '0', '');
    SET p_text = REPLACE(p_text, ' ', '');
RETURN p_text;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_document_type`(`id` INT, `cat_id` INT) RETURNS text CHARSET latin1
    COMMENT 'دالة لارجاع ارقام الملفات المرفقة لشخص ما'
BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT char_persons_documents.`document_type_id` SEPARATOR ',' )
          FROM `char_persons_documents`
          where `person_id`=id  and  document_type_id in (select  char_categories_document_types.document_type_id
                                                          from char_categories_document_types
                                                          where category_id = cat_id)
		);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_mother_child_count`(`p_via` VARCHAR(15), `mother_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
BEGIN
declare c INTEGER;
      if p_via = 'male' then
		set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
			                 );
     elseif p_via = 'female' then
			set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
			                 );
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_count`(`organization_id` INT) RETURNS int(11)
BEGIN
declare c INTEGER;
 set c = (
  				 SELECT COUNT(`char_cases`.`person_id`)
  				 FROM `char_cases`
			     WHERE `char_cases`.`status` = 0 and `char_cases`.`organization_id` = organization_id
			   );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_mosque_person_count`(`vId` INT(11) , `mosque_id` INT(11)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأشخاص التابعين لمسجد معين ضمن قسيمة ما'
BEGIN
declare c INTEGER;
		set c = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons`
					from `char_vouchers_persons`
					join `char_persons` on `char_persons`.`id` = `char_vouchers_persons`.`person_id`
					where `char_vouchers_persons`.`voucher_id` = vId and `char_persons`.`adsmosques_id` = mosque_id);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_org_payments_cases_count_`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الصرفيات التي استلمها شخص ما خلال فترة معينة ضمن تصنيف معين و من خلال جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                  (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                            (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                    (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
 (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                              (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                       (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
(`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                  (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
    (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                               (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
        (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_in_count`(`p_via` VARCHAR(15), `father_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
BEGIN
declare c INTEGER;
      if p_via = 'male' then
		set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
			                 );
     elseif p_via = 'female' then
			set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
			                 );
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_org_payments_cases_amount_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها شخص ما خلال فترة معينة ضمن تصنيف معين و من خلال جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                                                                               (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                                 (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
 (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                             (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                            (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
      (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                          (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
        (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                             (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                           (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
       (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                    (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
         (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_amount_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_amount`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها  المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_org_report_data_count`(`id` INT, `options` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة ارجات عدد القيم التي أدخلتها جمعيات معينة لخيار ما في التقرير الإداري'
BEGIN
declare c INTEGER;
		set c = ( select count(`d`.`value`)
                  FROM  `char_administrative_report_data` AS `d`
                  WHERE ( (`d`.`organization_id` = id ) and (FIND_IN_SET(`d`.`option_id`,options) != 0 ) )
                );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_category_amount`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها  المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_category_count`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الصرفيات التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                            WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                             (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                             (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and(`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` = category_id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_category_amount`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي مبالغ الصرفيات التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else
         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_count_`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                  (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                            (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_category_count`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                            WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                             (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                             (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and(`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and(`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`category_id` IN (select `cat`.`id` FROM   `char_payment_category` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 )) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_document`(`id` INT) RETURNS text CHARSET latin1
    COMMENT 'دالة لارجاع ارقام الملفات المرفقة لشخص ما'
BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_persons_documents`
          where `person_id`=id
		);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_payments_cases_count`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then
          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else
    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );
          elseif date_from is not null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );
          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_vouchers_value_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
		if type = 'sponsors' then
				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                       (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null,orgs) != 0)));
				elseif date_from is not null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date` >= date_from )
				      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null,orgs)  != 0)));
				elseif date_from is null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null,orgs)  != 0)));
				elseif date_from is not null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
                      (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null,orgs) != 0)));
				end if;
		else
				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));
				elseif date_from is not null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
				and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));
				elseif date_from is null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));
				elseif date_from is not null and date_to is not null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));
				end if;
		end if;
else
if type = 'sponsors' then
		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
              (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id,orgs) != 0)));
		elseif date_from is not null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and
              (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id,orgs)  != 0)));
		elseif date_from is null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id,orgs)  != 0)));
		elseif date_from is not null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id,orgs) != 0)));
		end if;
else
		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));
		elseif date_from is not null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)
		and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));
		elseif date_from is null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));
		elseif date_from is not null and date_to is not null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));
		end if;
end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_payments_cases_count`(`id` INT(10), `payment_id` INT(10), `sponsor_number` VARCHAR(45)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأرقام الذين يملكون رقم كفالة معين ضمن الصرفية'
BEGIN
declare c INTEGER;
		set c = (
			select COUNT(`p`.`payment_id`) FROM `char_payments_cases` AS `p`
			WHERE (`p`.`sponsor_number` = sponsor_number) and
             (`p`.`case_id` = case_id) and (`p`.`payment_id` !=  payment_id)
		);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_payments_recipient_amount`(`id` INT(10), `p_via` VARCHAR(45), `payment_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي المبالغ المستلمة في الصرفية بناء على المستلم ( الشخص نفسه أو المعيل)'
BEGIN
declare c INTEGER;
        if p_via = 'person' then
		set c = (
            select sum(`p`.`amount`)
FROM `char_payments_cases` AS `p`
join `char_cases` ON `char_cases`.`id` =`p`.`case_id`
join `char_persons` ON `char_persons`.`id` =`char_cases`.`person_id`
WHERE (`char_persons`.`id` = id ) and (`p`.`payment_id` =  payment_id) and (`p`.`status` !=  3)
);
    else
		set c = (
                   select sum(`p`.`amount`)
                   FROM `char_payments_cases` AS `p`
                   WHERE (`p`.`guardian_id` = id) and (`p`.`payment_id` =  payment_id)
                );
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_total`(`type` INT(10), `repository_id` INT(10), `org_id` INT(10), `factory` INT(10)) RETURNS decimal(10,2)
    COMMENT 'دالة لارجاع اجمال القيمة او الكمية او النسبة لجمعية ما ضمن مشروع في الوعاء'
BEGIN
declare c decimal(10,2);
    if type = 1 then
        set c = (SELECT round(sum(ratio / factory),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects
				 where aid_projects.repository_id = repository_id  and aid_projects.deleted_at is null ) );
    elseif type = 2 then
        set c = (SELECT round(sum(amount),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects
			     where aid_projects.repository_id = repository_id and aid_projects.deleted_at is null) );
    else
        set c = (SELECT round(sum(quantity),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects
                 where aid_projects.repository_id = repository_id and aid_projects.deleted_at is null) );
	    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_payments_beneficiary_count`(`id` INT(10), `c_via` VARCHAR(45)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد المستفيدين من الصرفية حسب تصنيفهم ( مكفولين ، جدد ، ليس لديهم حركة مالية )'
BEGIN
declare c INTEGER;
      if c_via = 'all' then
		      set c = (	select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( `p`.`payment_id`  =  id )	);
      elseif c_via = 'guaranteed' then
			    set c = (select COUNT(`p`.`sponsor_number`)
FROM `char_payments_cases` AS `p`
WHERE ( (`p`.`payment_id` = id)  and ( (`p`.`status` = 1) or (`p`.`status` = 2)) ));
      else
			    set c = (select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( (`p`.`payment_id` = id) and (`p`.`status` = 3)	));
      end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_organization_vouchers_value`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
    if type = 'sponsors' then
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null) != 0)));
               end if;
else
   if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
               end if;
   end if;
else
    if type = 'sponsors' then
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id) != 0)));
               end if;
else
  if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));
               elseif date_from is not null and date_to is null then
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));
               elseif date_from is null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));
               elseif date_from is not null and date_to is not null then
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
               end if;
    end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_project_total`(`type` INT(11), `id` INT(11), `rep_org_id` INT(11)) RETURNS decimal(10,2)
    COMMENT 'دالة لارجاع اجمال القيمة او الكمية او النسبة لجمعية أم ضمن مشروع في الوعاء'
BEGIN
declare c decimal(10,2);
    if type = 1 then
        set c = (SELECT round(sum(ratio),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));
    elseif type = 2 then
        set c = (SELECT round(sum(amount),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));
    else
        set c = (SELECT round(sum(quantity),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));
	end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_project_location_persons_count`(`pId` INT(11) ,
                                             `statusId` INT(11) , `loc_type` INT(11) ,`loc_id` INT(11) ,`orgId` INT(11)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأشخاص في مشروع ما تابعين لجمعية معينة ويقعون ضمن منطقة جغرافية ما حسب تصنيف المنطقة ( محافظة ، منطقة ، حي ، مربع ، مسجد)'
BEGIN
declare c INTEGER;
		if loc_type  = 1 then
				  set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsdistrict_id` = loc_id
						  );
		elseif loc_type  = 2 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsregion_id` = loc_id
						  );
		elseif loc_type  = 3 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsneighborhood_id` = loc_id
						  );
		 elseif loc_type  = 4 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId  and `char_persons`.`adssquare_id` = loc_id
						  );
		elseif loc_type  = 5 then
				  set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId  and `char_persons`.`adsmosques_id` = loc_id
						  );
		end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_recipient_amount`(`recipient_id` INT, `payment_id` INT, `father_id` INT, `mother_id` INT, `type` TINYINT) RETURNS int(11)
    COMMENT 'دالة لارجاع المبلغ الذي استلمه شخص معين في صرفية ما ( المبلغ الأصلي ، المبلغ الأصلي بعد الخصم ، المبلغ بالشيكل ، المبلغ بالشيكل بعد الخصم'
BEGIN
declare c INTEGER;
      if type = 0 then
        		        set c = ( SELECT ifnull(sum(char_payments_cases.amount), 0)
                          from `char_payments_cases`
                          join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                          join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                          join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                          where (
                                     (
                                         (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                         or
                                         (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                     )
                                     and (
                                          `char_payments_cases`.`payment_id` = payment_id and
                                          `char_persons`.`father_id` = father_id and
                                          `char_persons`.`mother_id` = mother_id
                                         )
                                  )
                        );
         elseif type = 1 THEN
                    set c = ( SELECT ifnull(sum(char_payments_cases.amount_after_discount), 0)
                        from `char_payments_cases`
                        join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                        join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                        join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                        where (
                                   (
                                       (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                       or
                                       (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                   )
                                   and (
                                        `char_payments_cases`.`payment_id` = payment_id and
                                        `char_persons`.`father_id` = father_id and
                                        `char_persons`.`mother_id` = mother_id
                                       )
                                )
                      );
        elseif type = 2 THEN
                     set c = ( SELECT ifnull(sum(char_payments_cases.shekel_amount_before_discount), 0)
                        from `char_payments_cases`
                        join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                        join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                        join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                        where (
                                   (
                                       (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                       or
                                       (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                   )
                                   and (
                                        `char_payments_cases`.`payment_id` = payment_id and
                                        `char_persons`.`father_id` = father_id and
                                        `char_persons`.`mother_id` = mother_id
                                       )
                                )
                      );
               else
                     set c = ( SELECT ifnull(sum(char_payments_cases.shekel_amount), 0)
                                      from `char_payments_cases`
                                      join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                                      join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                                      join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                                      where (
                                                 (
                                                     (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                                     or
                                                     (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                                 )
                                                 and (
                                                      `char_payments_cases`.`payment_id` = payment_id and
                                                      `char_persons`.`father_id` = father_id and
                                                      `char_persons`.`mother_id` = mother_id
                                                     )
                                              )
                                    );
      end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_project_persons_count`(`pId` INT(11) ,`statusId` INT(11)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الأشخاص حسب حالتهم في مشروع ضمن وعاء'
BEGIN
declare c INTEGER;
		set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							 from `aid_projects_persons`
						 	 join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
						 	 where `aid_projects_persons`.`project_id` = pId and  `aid_projects_persons`.`status` = statusId
						  );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_voucher_count`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
 if type = 'sponsors' then
     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)));
     elseif date_from is not null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from )));
     elseif date_from is null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date`  <= date_to )));
     elseif date_from is not null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
else
    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
     end if;
else
    if type = 'sponsors' then
     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`category_id` = category_id)));
     elseif date_from is not null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
     elseif date_from is null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
     elseif date_from is not null and date_to is not null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
else
    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`category_id` = category_id) ));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
     end if;
     end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_voucher_count_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(45)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة'
BEGIN
declare c INTEGER;
if category_id is null then
if type = 'sponsors' then
if date_from is null and date_to is null then
	set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
				FROM  `char_vouchers` AS `v`
				join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
				WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
else
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
end if;
else
if type = 'sponsors' then
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))and (`v`.`category_id` = category_id)));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id)  and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
else
if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`category_id` = category_id) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
end if;
end if;
end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_wive_count`(`husband_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة ارجاع عدد الزوجات'
BEGIN
declare c INTEGER;
    	set c = (
  					 SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      	 WHERE `p`.`gender` = 2 and `p`.`death_date` is null and `p`.`marital_status_id` = 20 and
                            `p`.`id` IN ( SELECT `kin`.`r_person_id`
                                          from `char_persons_kinship` AS `kin`
                                          where  `kin`.`kinship_id` in(21) and  `kin`.`l_person_id` = husband_id)
			     );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_wives_count_hus_id`(`hus_id` INT(10)) RETURNS int(11)
BEGIN
declare c INTEGER;
      set c = ( SELECT COUNT(`id`) FROM `wife_cnt`  WHERE id = hus_id );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_total_recipient_amount`(`id` INT, `pIds` TEXT, `type` INT) RETURNS text CHARSET utf8
BEGIN
declare c INTEGER;
    if type = 0 then
       set c = ( select SUM(`amount`) as amount   FROM `char_payments_recipient` where `payment_id` IN(pIds) and `person_id` = id );
         else
       set c = ( select SUM(`shekel_amount`) as amount   FROM `char_payments_recipient` where `payment_id` IN(pIds) and `person_id` = id );
         end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `normalize_text_ar_old`(`p_text` TEXT CHARSET utf8) RETURNS text CHARSET utf8
    COMMENT 'عمل توحيد للنصوص المحفوظة على النظام - الاصلية'
BEGIN
	DECLARE v_comma VARCHAR(255) DEFAULT '،';
	DECLARE v_semicolon VARCHAR(255) DEFAULT '؛';
	DECLARE v_question VARCHAR(255) DEFAULT '؟';
	DECLARE v_hamza VARCHAR(255) DEFAULT 'ء';
	DECLARE v_alef_madda VARCHAR(255) DEFAULT 'آ';
	DECLARE v_alef_hamza_above VARCHAR(255) DEFAULT 'أ';
	DECLARE v_waw_hamza VARCHAR(255) DEFAULT 'ؤ';
	DECLARE v_alef_hamza_below VARCHAR(255) DEFAULT 'إ';
	DECLARE v_yeh_hamza VARCHAR(255) DEFAULT 'ئ';
	DECLARE v_alef VARCHAR(255) DEFAULT 'ا';
	DECLARE v_beh VARCHAR(255) DEFAULT 'ب';
	DECLARE v_teh_marbuta VARCHAR(255) DEFAULT 'ة';
	DECLARE v_teh VARCHAR(255) DEFAULT 'ت';
	DECLARE v_theh VARCHAR(255) DEFAULT 'ث';
	DECLARE v_jeem VARCHAR(255) DEFAULT 'ج';
	DECLARE v_hah VARCHAR(255) DEFAULT 'ح';
	DECLARE v_khah VARCHAR(255) DEFAULT 'خ';
	DECLARE v_dal VARCHAR(255) DEFAULT 'د';
	DECLARE v_thal VARCHAR(255) DEFAULT 'ذ';
	DECLARE v_reh VARCHAR(255) DEFAULT 'ر';
	DECLARE v_zain VARCHAR(255) DEFAULT 'ز';
	DECLARE v_seen VARCHAR(255) DEFAULT 'س';
	DECLARE v_sheen VARCHAR(255) DEFAULT 'ش';
	DECLARE v_sad VARCHAR(255) DEFAULT 'ص';
	DECLARE v_dad VARCHAR(255) DEFAULT 'ض';
	DECLARE v_tah VARCHAR(255) DEFAULT 'ط';
	DECLARE v_zah VARCHAR(255) DEFAULT 'ظ';
	DECLARE v_ain VARCHAR(255) DEFAULT 'ع';
	DECLARE v_ghain VARCHAR(255) DEFAULT 'غ';
	DECLARE v_tatweel VARCHAR(255) DEFAULT 'ـ';
	DECLARE v_feh VARCHAR(255) DEFAULT 'ف';
	DECLARE v_qaf VARCHAR(255) DEFAULT 'ق';
	DECLARE v_kaf VARCHAR(255) DEFAULT 'ك';
	DECLARE v_lam VARCHAR(255) DEFAULT 'ل';
	DECLARE v_meem VARCHAR(255) DEFAULT 'م';
	DECLARE v_noon VARCHAR(255) DEFAULT 'ن';
	DECLARE v_heh VARCHAR(255) DEFAULT 'ه';
	DECLARE v_waw VARCHAR(255) DEFAULT 'و';
	DECLARE v_alef_maksura VARCHAR(255) DEFAULT 'ى';
	DECLARE v_yeh VARCHAR(255) DEFAULT 'ي';
	DECLARE v_madda_above VARCHAR(255) DEFAULT 'ٓ';
	DECLARE v_hamza_above VARCHAR(255) DEFAULT 'ٔ';
	DECLARE v_hamza_below VARCHAR(255) DEFAULT 'ٕ';
	DECLARE v_zero VARCHAR(255) DEFAULT '٠';
	DECLARE v_one VARCHAR(255) DEFAULT '١';
	DECLARE v_two VARCHAR(255) DEFAULT '٢';
	DECLARE v_three VARCHAR(255) DEFAULT '٣';
	DECLARE v_four VARCHAR(255) DEFAULT '٤';
	DECLARE v_five VARCHAR(255) DEFAULT '٥';
	DECLARE v_six VARCHAR(255) DEFAULT '٦';
	DECLARE v_seven VARCHAR(255) DEFAULT '٧';
	DECLARE v_eight VARCHAR(255) DEFAULT '٨';
	DECLARE v_nine VARCHAR(255) DEFAULT '٩';
	DECLARE v_percent VARCHAR(255) DEFAULT '٪';
	DECLARE v_decimal VARCHAR(255) DEFAULT '٫';
	DECLARE v_thousands VARCHAR(255) DEFAULT '٬';
	DECLARE v_star VARCHAR(255) DEFAULT '٭';
	DECLARE v_mini_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_alef_wasla VARCHAR(255) DEFAULT 'ٱ';
	DECLARE v_full_stop VARCHAR(255) DEFAULT '۔';
	DECLARE v_byte_order_mark VARCHAR(255) DEFAULT '﻿';
	DECLARE v_fathatan VARCHAR(255) DEFAULT 'ً';
	DECLARE v_dammatan VARCHAR(255) DEFAULT 'ٌ';
	DECLARE v_kasratan VARCHAR(255) DEFAULT 'ٍ';
	DECLARE v_fatha VARCHAR(255) DEFAULT 'َ';
	DECLARE v_damma VARCHAR(255) DEFAULT 'ُ';
	DECLARE v_kasra VARCHAR(255) DEFAULT 'ِ';
	DECLARE v_shadda VARCHAR(255) DEFAULT 'ّ';
	DECLARE v_sukun VARCHAR(255) DEFAULT 'ْ';
	DECLARE v_small_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_small_waw VARCHAR(255) DEFAULT 'ۥ';
	DECLARE v_small_yeh VARCHAR(255) DEFAULT 'ۦ';
	DECLARE v_lam_alef VARCHAR(255) DEFAULT 'ﻻ';
	DECLARE v_lam_alef_hamza_above VARCHAR(255) DEFAULT 'ﻷ';
	DECLARE v_lam_alef_hamza_below VARCHAR(255) DEFAULT 'ﻹ';
	DECLARE v_lam_alef_madda_above VARCHAR(255) DEFAULT 'ﻵ';
	DECLARE v_simple_lam_alef VARCHAR(255) DEFAULT 'لَا';
	DECLARE v_simple_lam_alef_hamza_above VARCHAR(255) DEFAULT 'لأ';
	DECLARE v_simple_lam_alef_hamza_below VARCHAR(255) DEFAULT 'لإ';
	DECLARE v_simple_lam_alef_madda_above VARCHAR(255) DEFAULT 'لءَا';
    SET p_text = REPLACE(p_text, v_tatweel, '');
    SET p_text = REPLACE(p_text, v_fathatan, '');
    SET p_text = REPLACE(p_text, v_dammatan, '');
    SET p_text = REPLACE(p_text, v_kasratan, '');
    SET p_text = REPLACE(p_text, v_fatha, '');
    SET p_text = REPLACE(p_text, v_damma, '');
    SET p_text = REPLACE(p_text, v_kasra, '');
    SET p_text = REPLACE(p_text, v_shadda, '');
    SET p_text = REPLACE(p_text, v_sukun, '');
    SET p_text = REPLACE(p_text, v_waw_hamza, v_waw);
    SET p_text = REPLACE(p_text, v_yeh_hamza, v_yeh);
    SET p_text = REPLACE(p_text, v_alef_madda, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_lam_alef, v_simple_lam_alef);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_above, v_simple_lam_alef_hamza_above);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_below, v_simple_lam_alef_hamza_below);
    SET p_text = REPLACE(p_text, v_lam_alef_madda_above, v_simple_lam_alef_madda_above);
    SET p_text = REPLACE(p_text, v_teh_marbuta, v_heh);
    WHILE instr(p_text, '  ') > 0 do
		SET p_text = replace(p_text, '  ', ' ');
	END WHILE;
RETURN trim(p_text);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `getfiles`(`id` INT) RETURNS text CHARSET utf8
    COMMENT 'ارجاع مرفقات شخص ما بناء على معرف الشخص'
BEGIN
	declare c text;
    set c =
            (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_persons_documents`
              join `doc_files` on `doc_files`.`id` = `char_persons_documents`.`document_id`
              where `char_persons_documents`.`person_id`=id
            );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `normalize_text_ar`(`p_text` TEXT CHARSET utf8) RETURNS text CHARSET utf8
    COMMENT 'عمل توحيد للنصوص المحفوظة على النظام - نسخة معدلة'
BEGIN
    SET p_text = REPLACE(p_text, 'ـ', '');
    SET p_text = REPLACE(p_text, 'ً', '');
    SET p_text = REPLACE(p_text, 'ٌ', '');
    SET p_text = REPLACE(p_text, 'ٍ', '');
    SET p_text = REPLACE(p_text, 'َ', '');
    SET p_text = REPLACE(p_text, 'ُ', '');
    SET p_text = REPLACE(p_text, 'ِ', '');
    SET p_text = REPLACE(p_text, 'ّ', '');
    SET p_text = REPLACE(p_text, 'ْ', '');
    SET p_text = REPLACE(p_text,  'ؤ', 'و');
    SET p_text = REPLACE(p_text, 'ئ', 'ى');
    SET p_text = REPLACE(p_text, 'آ', 'ا');
    SET p_text = REPLACE(p_text, 'أ', 'ا');
    SET p_text = REPLACE(p_text, 'إ', 'ا');
    SET p_text = REPLACE(p_text, 'ٔ', 'ا');
    SET p_text = REPLACE(p_text, 'ٕ', 'ا');
    SET p_text = REPLACE(p_text, 'ﻻ', 'لَا');
    SET p_text = REPLACE(p_text, 'ﻷ', 'لأ');
    SET p_text = REPLACE(p_text, 'ﻹ', 'لأ');
    SET p_text = REPLACE(p_text, 'ﻵ', 'لإ');
    SET p_text = REPLACE(p_text,'ة','ه');
    WHILE instr(p_text, '  ') > 0 do
		SET p_text = replace(p_text, '  ', ' ');
	END WHILE;
RETURN trim(p_text);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_total_org_ratio`(`repository_id` INT(10), `org_id` INT(10) , `factory` int(10)) RETURNS decimal(5,2)
    COMMENT 'دالة لارجاع اجمالي النسبة الخاص بجمعية معينة ضمن مشروع ما في الوعاء'
BEGIN
declare c decimal(5,2);
        set c = (
        SELECT round(sum(ratio / factory),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects
        where aid_projects.repository_id = repository_id ) );
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_wive_count_`(`husband_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة ارجاع عدد الزوجات'
BEGIN
declare c INTEGER;
    	set c = (
  					 SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      	 WHERE `p`.`husband_id` = husband_id and (marital_status_id = 20 and death_date is null and gender =2)
			     );
RETURN c;
END$$
DELIMITER ;
