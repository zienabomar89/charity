
CREATE TABLE `char_project_beneficiary_category` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على تصنيفات المستفيدين من مشاريع الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_project_beneficiary_category_i18n` (
  `beneficiary_category_id` int(10) UNSIGNED NOT NULL COMMENT 'تصنيف المستفيدين',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات تصنيفات المستفيدين وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_project_beneficiary_category`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_project_beneficiary_category_i18n`
  ADD PRIMARY KEY (`beneficiary_category_id`,`language_id`),
  ADD KEY `fk_project_beneficiary_category_i18n_2_idx` (`language_id`);

ALTER TABLE `char_project_beneficiary_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_project_beneficiary_category_i18n`
  ADD CONSTRAINT `fk_project_beneficiary_category_i18n_1` FOREIGN KEY (`beneficiary_category_id`) REFERENCES `char_project_beneficiary_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_project_beneficiary_category_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

INSERT INTO `char_project_beneficiary_category` (`id`) VALUES (1),(2);
INSERT INTO `char_project_beneficiary_category_i18n` (`beneficiary_category_id`, `language_id`, `name`) VALUES
(1, 1, 'أسرة'),
(2, 1, 'فرد');

CREATE TABLE `char_project_activities_category` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مجال عمل النشاط في مشاريع الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_project_activities_category_i18n` (
  `activity_category_id` int(10) UNSIGNED NOT NULL COMMENT 'تصنيف النشاط',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات مجال عمل النشاط في مشاريع الجمعيات وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_project_activities_category`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_project_activities_category_i18n`
  ADD PRIMARY KEY (`activity_category_id`,`language_id`),
  ADD KEY `fk_project_activities_category_i18n_2_idx` (`language_id`);

ALTER TABLE `char_project_activities_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_project_activities_category_i18n`
  ADD CONSTRAINT `fk_project_activities_category_i18n_1` FOREIGN KEY (`activity_category_id`) REFERENCES `char_project_activities_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_project_activities_category_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);


INSERT INTO `char_project_activities_category` (`id`) VALUES (1),(2),(3),(4),(5),(6),(7);
INSERT INTO `char_project_activities_category_i18n` (`activity_category_id`, `language_id`, `name`) VALUES
(1, 1, 'صحي'),
(2, 1, 'اجتماعي'),
(3, 1, 'اقتصادي'),
(4, 1, 'مشاريع صغيرة'),
(5, 1, 'زراعي'),
(6, 1, 'تعليمي'),
(7, 1, 'مرأة');


CREATE TABLE `char_project_regions` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على النطاق الجغرافي في مشاريع الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_project_regions_i18n` (
  `region_id` int(10) UNSIGNED NOT NULL COMMENT 'تصنيف المستفيدين',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات النطاق الجغرافي وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_project_regions`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_project_regions_i18n`
  ADD PRIMARY KEY (`region_id`,`language_id`),
  ADD KEY `fk_project_beneficiary_category_i18n_2_idx` (`language_id`);

ALTER TABLE `char_project_regions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_project_regions_i18n`
  ADD CONSTRAINT `fk_project_regions_i18n_1` FOREIGN KEY (`region_id`) REFERENCES `char_project_regions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_project_regions_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

INSERT INTO `char_project_regions` (`id`) VALUES (1),(2),(3),(4),(5),(6);
INSERT INTO `char_project_regions_i18n` (`region_id`, `language_id`, `name`) VALUES
(1, 1, 'الشمال'),
(2, 1, 'غزة'),
(3, 1, 'الوسطى'),
(4, 1, 'خانيونس'),
(5, 1, 'رفح'),
(6, 1, 'كل المحافظات');


CREATE TABLE `char_organization_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'اسم المشروع',
  `description` varchar(255) NOT NULL COMMENT 'وصف المشروع',
  `date` date NOT NULL COMMENT 'تاريخ المشروع',
  `activity_category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'مجال نشاط المشروع',
  `amount` float NOT NULL COMMENT 'إجمالي تكلفة المشروع',
  `currency_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'العملة',
  `date_from` date NOT NULL COMMENT 'تاريخ بداية المشروع',
  `date_to` date NOT NULL COMMENT 'تاريخ انتهاء المشروع',
  `region` int(10) UNSIGNED DEFAULT NULL COMMENT 'النطاق الجغرافي لتنفيذ المشروع',
  `target_group` varchar(255) NOT NULL COMMENT 'الفئة المستهدفة',
  `count` int(10) UNSIGNED DEFAULT NULL COMMENT 'عدد المستفيدين',
  `beneficiary_category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'تصنيف المستفيدين من المشروع',
  `sponsor_id` int(10) UNSIGNED NOT NULL COMMENT 'جهة التمويل',
  `country_id` int(10) UNSIGNED NOT NULL COMMENT 'الدولة التي بها المقر الرئيس لجهة التمويل',
  `shared` tinyint(4) DEFAULT '1' COMMENT 'تم تنفيذ المشروع بالشراكة:1 - نعم، 2- لا ',
  `shared_organizations` text COMMENT 'الجمعيات الشريكة في تنفيذ المشروع',
  `notes` text COMMENT 'ملاحظات',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مشاريع الجمعيات';

ALTER TABLE `char_organization_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_organization_projects_organizationId_idx` (`organization_id`),
  ADD KEY `fk_char_organization_projects_userId_idx` (`user_id`),
  ADD KEY `fk_organization_projects_activity_categoryId_idx` (`activity_category_id`),
  ADD KEY `fk_char_organization_currencyId_idx` (`currency_id`),
  ADD KEY `fk_organization_projects_beneficiary_categoryId_idx` (`beneficiary_category_id`),
  ADD KEY `fk_char_organization_projects_countryId_idx` (`country_id`),
  ADD KEY `fk_char_organization_projects_sponsorId_idx` (`sponsor_id`);

ALTER TABLE `char_organization_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `char_organization_projects`
  ADD CONSTRAINT `fk_char_organization_projects_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_char_organization_projects_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_char_organization_projects_sponsorId` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_char_organization_projects_countryId` FOREIGN KEY (`country_id`) REFERENCES `char_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_char_organization_currencyId` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_organization_projects_beneficiary_categoryId` FOREIGN KEY (`beneficiary_category_id`) REFERENCES `char_project_beneficiary_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_organization_projects_activity_categoryId` FOREIGN KEY (`activity_category_id`) REFERENCES `char_project_activities_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(467, 7, 'setting.ProjectActivities.manage', 'إدارة مجال نشاط المشروع'),
(468, 7, 'setting.ProjectActivities.create', 'إنشاء مجال نشاط جديد'),
(469, 7, 'setting.ProjectActivities.update', 'تحرير بيانات مجال نشاط مشروع'),
(470, 7, 'setting.ProjectActivities.delete', 'حذف مجال نشاط مشروع'),
(471, 7, 'setting.ProjectActivities.view', 'عرض بيانات مجال نشاط مشروع'),
(472, 7, 'setting.ProjectBeneficiaryCategory.manage', 'إدارة تصنيفات المستفيدين من المشاريع'),
(473, 7, 'setting.ProjectBeneficiaryCategory.create', 'إنشاء تصنيف مستفيدين جديد'),
(474, 7, 'setting.ProjectBeneficiaryCategory.update', 'تحرير بيانات تصنيف مستفيدين'),
(475, 7, 'setting.ProjectBeneficiaryCategory.delete', 'حذف تصنيف مستفيدين'),
(476, 7, 'setting.ProjectBeneficiaryCategory.view', 'عرض بيانات تصنيف مستفيدين'),
(497, 8, 'org.organizationsProjects.manage', 'إدارة مشاريع الجمعيات'),
(498, 8, 'org.organizationsProjects.create', 'إضافة مشروع جديد'),
(499, 8, 'org.organizationsProjects.update', 'تحرير بيانات مشروع'),
(500, 8, 'org.organizationsProjects.view', 'مشاهدة بيانات مشروع'),
(501, 8, 'org.organizationsProjects.delete', 'حذف مشروع');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 467, 1, CURRENT_TIMESTAMP, 1),
(1, 468, 1, CURRENT_TIMESTAMP, 1),
(1, 469, 1, CURRENT_TIMESTAMP, 1),
(1, 470, 1, CURRENT_TIMESTAMP, 1),
(1, 471, 1, CURRENT_TIMESTAMP, 1),
(1, 472, 1, CURRENT_TIMESTAMP, 1),
(1, 473, 1, CURRENT_TIMESTAMP, 1),
(1, 474, 1, CURRENT_TIMESTAMP, 1),
(1, 475, 1, CURRENT_TIMESTAMP, 1),
(1, 476, 1, CURRENT_TIMESTAMP, 1),
(1, 497, 1, CURRENT_TIMESTAMP, 1),
(1, 498, 1, CURRENT_TIMESTAMP, 1),
(1, 499, 1, CURRENT_TIMESTAMP, 1),
(1, 500, 1, CURRENT_TIMESTAMP, 1),
(1, 501, 1, CURRENT_TIMESTAMP, 1),
(2, 467, 1, CURRENT_TIMESTAMP, 1),
(2, 468, 1, CURRENT_TIMESTAMP, 1),
(2, 469, 1, CURRENT_TIMESTAMP, 1),
(2, 470, 1, CURRENT_TIMESTAMP, 1),
(2, 471, 1, CURRENT_TIMESTAMP, 1),
(2, 472, 1, CURRENT_TIMESTAMP, 1),
(2, 473, 1, CURRENT_TIMESTAMP, 1),
(2, 474, 1, CURRENT_TIMESTAMP, 1),
(2, 475, 1, CURRENT_TIMESTAMP, 1),
(2, 476, 1, CURRENT_TIMESTAMP, 1),
(2, 497, 1, CURRENT_TIMESTAMP, 1),
(2, 498, 1, CURRENT_TIMESTAMP, 1),
(2, 499, 1, CURRENT_TIMESTAMP, 1),
(2, 500, 1, CURRENT_TIMESTAMP, 1),
(2, 501, 1, CURRENT_TIMESTAMP, 1);




INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(502, 7, 'setting.ProjectRegion.manage', 'إدارة النطاق الجغرافي المشروع'),
(503, 7, 'setting.ProjectRegion.create', 'إنشاء نطاق الجغرافي جديد'),
(504, 7, 'setting.ProjectRegion.update', 'تحرير بيانات نطاق الجغرافيمشروع'),
(505, 7, 'setting.ProjectRegion.delete', 'حذف نطاق الجغرافي مشروع'),
(506, 7, 'setting.ProjectRegion.view', 'عرض بيانات نطاق الجغرافي مشروع');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES

(1, 502,  1, CURRENT_TIMESTAMP, 1),
(1, 503,  1, CURRENT_TIMESTAMP, 1),
(1, 504,  1, CURRENT_TIMESTAMP, 1),
(1, 505,  1, CURRENT_TIMESTAMP, 1),
(1, 506,  1, CURRENT_TIMESTAMP, 1),

(2, 502,  1, CURRENT_TIMESTAMP, 1),
(2, 503,  1, CURRENT_TIMESTAMP, 1),
(2, 504,  1, CURRENT_TIMESTAMP, 1),
(2, 505,  1, CURRENT_TIMESTAMP, 1),
(2, 506,  1, CURRENT_TIMESTAMP, 1)
;


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(509, 7, 'setting.ProjectCategory.manage', 'إدارة تصنيفات المشروع'),
(510, 7, 'setting.ProjectCategory.create', 'إنشاء تصنيف مشروع جديد'),
(511, 7, 'setting.ProjectCategory.update', 'تحرير بيانات تصنيف مشروع'),
(512, 7, 'setting.ProjectCategory.delete', 'حذف تصنيف مشروع'),
(513, 7, 'setting.ProjectCategory.view', 'عرض بيانات تصنيف مشروع'),
(514, 7, 'setting.ProjectType.manage', 'إدارة أنواع المشروع'),
(515, 7, 'setting.ProjectType.create', 'إنشاء نوع مشروع جديد'),
(516, 7, 'setting.ProjectType.update', 'تحرير بيانات نوع مشروع'),
(517, 7, 'setting.ProjectType.delete', 'حذف نوع مشروع'),
(518, 7, 'setting.ProjectType.view', 'عرض بيانات تصنيف نوع');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 509,  1, CURRENT_TIMESTAMP, 1),
(1, 510,  1, CURRENT_TIMESTAMP, 1),
(1, 511,  1, CURRENT_TIMESTAMP, 1),
(1, 512,  1, CURRENT_TIMESTAMP, 1),
(1, 513,  1, CURRENT_TIMESTAMP, 1),
(1, 514,  1, CURRENT_TIMESTAMP, 1),
(1, 515,  1, CURRENT_TIMESTAMP, 1),
(1, 516,  1, CURRENT_TIMESTAMP, 1),
(1, 517,  1, CURRENT_TIMESTAMP, 1),
(1, 518,  1, CURRENT_TIMESTAMP, 1),

(2, 509,  1, CURRENT_TIMESTAMP, 1),
(2, 510,  1, CURRENT_TIMESTAMP, 1),
(2, 511,  1, CURRENT_TIMESTAMP, 1),
(2, 512,  1, CURRENT_TIMESTAMP, 1),
(2, 513,  1, CURRENT_TIMESTAMP, 1),
(2, 514,  1, CURRENT_TIMESTAMP, 1),
(2, 515,  1, CURRENT_TIMESTAMP, 1),
(2, 516,  1, CURRENT_TIMESTAMP, 1),
(2, 517,  1, CURRENT_TIMESTAMP, 1),
(2, 518,  1, CURRENT_TIMESTAMP, 1);




CREATE TABLE `char_project_category` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على تصنيفات المشروع من مشاريع الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_project_category_i18n` (
  `category_id` int(10) UNSIGNED NOT NULL COMMENT 'تصنيف المشروع',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات تصنيفات المشروع وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_project_category`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_project_category_i18n`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `fk_project_category_i18n_2_idx` (`language_id`);

ALTER TABLE `char_project_category`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_project_category_i18n`
  ADD CONSTRAINT `fk_project_category_i18n_1` FOREIGN KEY (`category_id`) REFERENCES `char_project_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_project_category_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

INSERT INTO `char_project_category` (`id`) VALUES (1),(2);
INSERT INTO `char_project_category_i18n` (`category_id`, `language_id`, `name`) VALUES
(1, 1, 'لحوم أضاحي'),
(2, 1, 'مدرسي');

CREATE TABLE `char_project_type` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على أنواع مشاريع الجمعيات' ROW_FORMAT=COMPACT;

CREATE TABLE `char_project_type_i18n` (
  `type_id` int(10) UNSIGNED NOT NULL COMMENT 'نوع المشروع',
  `language_id` int(10) UNSIGNED NOT NULL COMMENT 'اللغة',
  `name` varchar(255) NOT NULL COMMENT 'التسمية'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على مسميات أنواع مشاريع الجمعيات وترجماتها' ROW_FORMAT=COMPACT;

ALTER TABLE `char_project_type`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_project_type_i18n`
  ADD PRIMARY KEY (`type_id`,`language_id`),
  ADD KEY `fk_char_project_type_i18n_2_idx` (`language_id`);

ALTER TABLE `char_project_type`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_project_type_i18n`
  ADD CONSTRAINT `fk_char_project_type_i18n_1` FOREIGN KEY (`type_id`) REFERENCES `char_project_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_char_project_type_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

INSERT INTO `char_project_type` (`id`) VALUES (1),(2);
INSERT INTO `char_project_type_i18n` (`type_id`, `language_id`, `name`) VALUES
(1, 1, 'موسمي'),
(2, 1, 'غير موسمي');


ALTER TABLE `char_organization_projects`
ADD `type_id` INT(10) UNSIGNED NULL COMMENT 'نوع المشروع' AFTER `date`,
ADD `distribute` TINYINT NULL COMMENT 'طبيعة التوزيع : 1 - نقدي 2- عيني' AFTER `type_id`,
ADD `category_id` INT(10) UNSIGNED NULL COMMENT 'تصنيف المشروع' AFTER `distribute`;

ALTER TABLE `char_organization_projects`
  ADD KEY `fk_organization_projects_typeId_idx` (`type_id`),
  ADD KEY `fk_organization_projects_categoryId_idx` (`category_id`);

ALTER TABLE `char_organization_projects`
  ADD CONSTRAINT `fk_organization_projects_typeId` FOREIGN KEY (`type_id`) REFERENCES `char_project_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_organization_projects_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_project_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `char_organization_projects`
ADD `sub_category_id` INT(10) UNSIGNED NULL COMMENT 'تصنيف المشروع - الفرعي' AFTER `category_id`;

ALTER TABLE `char_organization_projects`
  ADD KEY `fk_organization_projects_sub_categoryId_idx` (`sub_category_id`);

ALTER TABLE `char_organization_projects`
  ADD CONSTRAINT `fk_organization_projects_sub_categoryId` FOREIGN KEY (`sub_category_id`) REFERENCES `char_project_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `char_project_category` ADD `parent_id` INT(10) UNSIGNED NULL AFTER `id`;

ALTER TABLE `char_organization_projects` CHANGE `sponsor_id` `sponsor_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'جهة التمويل', CHANGE `country_id` `country_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'الدولة التي بها المقر الرئيس لجهة التمويل';


ALTER TABLE `char_organization_projects` ADD `has_sponsor` TINYINT(4) NULL DEFAULT '2'  AFTER `shared_organizations` ;
ALTER TABLE `char_organization_projects` ADD `sponsor_organizations` TEXT NULL DEFAULT NULL AFTER `has_sponsor`;


CREATE TABLE `char_organization_projects_attachments` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم المشروع',
  `document_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الملف',
  `created_at` datetime NOT NULL DEFAULT current_timestamp() ,
  `updated_at` datetime NULL DEFAULT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مرفقات المشاريع' ROW_FORMAT=COMPACT;


ALTER TABLE `char_organization_projects_attachments`
 ADD PRIMARY KEY (`id`),
--   ADD PRIMARY KEY (`project_id`,`document_id`),
  ADD KEY `organization_projects_attachments_document_id_idx` (`document_id`),
  ADD KEY `organization_projects_attachments_project_id_idx` (`project_id`);


ALTER TABLE `char_organization_projects_attachments`
  ADD CONSTRAINT `fk_organization_projects_attachments_document_id` FOREIGN KEY (`document_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_organization_projects_attachments_project_id` FOREIGN KEY (`project_id`) REFERENCES `char_organization_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `char_organization_projects_attachments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;


-- -------------------------------------------
ALTER TABLE `char_organizations` ADD `registration_number` VARCHAR(255) NULL DEFAULT NULL COMMENT 'رقم التسجيل' AFTER `phone`;
