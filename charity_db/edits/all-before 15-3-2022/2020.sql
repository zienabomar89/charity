ALTER TABLE `char_persons` CHANGE `spouses` `spouses` TINYINT(1) NULL DEFAULT '0' COMMENT 'عدد الزوجات';
ALTER TABLE `char_persons` CHANGE `family_cnt` `family_cnt` INT(10) NULL DEFAULT '1' COMMENT 'عدد أفراد العائلة';

ALTER TABLE `char_administrative_report` ADD `data_status` TINYINT NOT NULL DEFAULT '1' AFTER `status`;
ALTER TABLE `char_administrative_report_organization` ADD `status` TINYINT NOT NULL DEFAULT '1';
ALTER TABLE `char_administrative_report_tab` ADD `tab_order` TINYINT NOT NULL DEFAULT '1' AFTER `description`;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(519, 8, 'org.organizationsProjects.trashed', 'إدارة مشاريع الجمعيات المحذوفة');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 519, 1, CURRENT_TIMESTAMP, 1),
(2, 519, 1, CURRENT_TIMESTAMP, 1);

UPDATE `char_permissions` SET `name` = ' اضافة التقارير الادارية' WHERE `char_permissions`.`id` = 449;
