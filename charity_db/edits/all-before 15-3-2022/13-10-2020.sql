ALTER TABLE `char_vouchers` ADD `case_category_id` INT(10) UNSIGNED NULL AFTER `category_id`;

--
-- ALTER TABLE `char_vouchers`
--   ADD KEY `fk_vouchers_case_categoryId_idx` (`category_id`);
--
-- ALTER TABLE `char_vouchers`
--   ADD CONSTRAINT `fk_vouchers_case_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),

DROP VIEW  IF EXISTS char_vouchers_persons_view;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW char_vouchers_persons_view  AS
  select char_vouchers_persons.id AS id,char_vouchers_persons.voucher_id AS voucher_id,
         char_vouchers_persons.person_id AS person_id,char_vouchers_persons.individual_id AS individual_id,
         char_vouchers_persons.receipt_date AS receipt_date,char_vouchers_persons.receipt_time AS receipt_time,
         char_vouchers_persons.receipt_location AS receipt_location,char_vouchers_persons.status AS rec_status,
         v.organization_id AS organization_id,v.category_id AS category_id,v.project_id AS project_id,
         org.name AS organization_name,org.en_name AS en_organization_name,sponsor.name AS sponsor_name,
         sponsor.en_name AS en_sponsor_name,v.type AS type,v.value AS value,
         (v.exchange_rate * v.value) AS shekel_value,v.title AS title,v.beneficiary AS beneficiary,
         v.currency_id AS currency_id,v.exchange_rate AS exchange_rate,v.template AS template,
         v.content AS content,v.header AS header,v.footer AS footer,v.notes AS notes,
         v.created_at AS created_at,v.updated_at AS updated_at,v.deleted_at AS deleted_at,
         v.sponsor_id AS sponsor_id,v.voucher_date AS voucher_date,v.count AS count,
         v.urgent AS urgent,v.status AS status,v.transfer AS transfer,v.transfer_company_id AS transfer_company_id,
         v.allow_day AS allow_day,char_vouchers_categories.name AS category_type,
         (case when isnull(v.project_id) then 'من مشروع' else 'يدوي' end) AS voucher_source,
         (case when (v.type = 1) then 'نقدية' when (v.type = 2) then 'عينية' end) AS voucher_type,
         (case when (v.urgent = 0) then 'عادية' when (v.urgent = 1) then 'عاجلة' end) AS voucher_urgent,
         (case when (v.transfer = 0) then 'مباشر' when (v.transfer = 1) then 'شركة تحويل' end) AS transfer_status,
         (case when isnull(v.transfer_company_id) then '-' else char_transfer_company.name end) AS transfer_company_name,
         (case when isnull(v.currency_id) then '-' else char_currencies.name end) AS currency_name,
         (case when isnull(v.currency_id) then '-' else char_currencies.en_name end) AS en_currency_name,
         concat(ifnull(c.first_name,' '),' ',ifnull(c.second_name,' '),' ',
                 ifnull(c.third_name,' '),' ',ifnull(c.last_name,' ')) AS name,c.first_name AS first_name,
                 c.second_name AS second_name,c.third_name AS third_name,c.last_name AS last_name,
                 (case when isnull(c.marital_status_id) then ' ' else m.name end) AS marital_status
                 ,(case when isnull(c.gender) then ' ' when (c.gender = 1) then 'ذكر ' when (c.gender = 2) then 'أنثى' end) AS gender,
                 c.gender AS p_gender,(case when isnull(c.id_card_number) then ' ' else c.id_card_number end) AS id_card_number,
                 char_get_family_count('left',c.id,NULL,c.id) AS family_count,
                 concat(ifnull(ind.first_name,' '),' ',ifnull(ind.second_name,' '),' ',ifnull(ind.third_name,' '),' ',
                 ifnull(ind.last_name,' ')) AS individual_name,ind.first_name AS ind_first_name,ind.second_name AS ind_second_name,
                 ind.third_name AS ind_third_name,ind.last_name AS ind_last_name,
                 (case when isnull(ind.id_card_number) then ' ' else ind.id_card_number end) AS individual_id_card_number,
                 (case when isnull(ind.gender) then ' ' when (ind.gender = 1) then 'ذكر '
                 when (ind.gender = 2) then 'أنثى' end) AS individual_gender,ind.gender AS ind_gender,
                 (case when isnull(ind.marital_status_id) then ' ' else min.name end) AS individual_marital_status,(case when (v.beneficiary = 1) then concat(ifnull(c.first_name,' '),' ',ifnull(c.second_name,' '),' ',ifnull(c.third_name,' '),' ',ifnull(c.last_name,' ')) else concat(ifnull(ind.first_name,' '),' ',ifnull(ind.second_name,' '),' ',ifnull(ind.third_name,' '),' ',ifnull(ind.last_name,' ')) end) AS beneficiary_name,(case when isnull(primary_mob_number.contact_value) then ' ' else primary_mob_number.contact_value end) AS primary_mobile,(case when isnull(wataniya_mob_number.contact_value) then ' ' else wataniya_mob_number.contact_value end) AS wataniya,(case when isnull(secondary_mob_number.contact_value) then ' ' else secondary_mob_number.contact_value end) AS secondery_mobile,(case when isnull(phone_number.contact_value) then ' ' else phone_number.contact_value end) AS phone,ifnull(country_name.name,' ') AS country,ifnull(district_name.name,' ') AS district,ifnull(region_name.name,' ') AS region,ifnull(location_name.name,' ') AS nearlocation,ifnull(square_name.name,' ') AS square,ifnull(mosques_name.name,' ') AS mosque,ifnull(c.street_address,' ') AS street_address,concat(ifnull(country_name.name,' '),'  ',ifnull(district_name.name,' '),'  ',ifnull(region_name.name,' '),'  ',ifnull(location_name.name,' '),'  ',ifnull(square_name.name,' '),'  ',ifnull(mosques_name.name,' '),'  ',ifnull(c.street_address,' ')) AS full_address,time_format(char_vouchers_persons.receipt_time,'%r') AS receipt_time_,char_vouchers_persons.status AS receipt_status,(case when (char_vouchers_persons.status = 1) then 'تم الاستلام' when (char_vouchers_persons.status = 0) then 'تم الاستلام' when (char_vouchers_persons.status = 2) then ' لم يتم الاستلام' end) AS receipt_status_name
                 ,v.case_category_id,ca.name as case_category_name,ca.en_name as en_case_category_name
from char_vouchers_persons
join char_vouchers v on((v.id = char_vouchers_persons.voucher_id))
join char_categories ca on((ca.id = v.case_category_id))
join char_organizations org on((org.id = v.organization_id))
join char_organizations sponsor on((sponsor.id = v.sponsor_id))
left join char_currencies on((char_currencies.id = v.currency_id))
left join char_transfer_company on((char_transfer_company.id = v.transfer_company_id))
left join char_vouchers_categories on((char_vouchers_categories.id = v.category_id))
join char_persons c on((c.id = char_vouchers_persons.person_id))
left join char_marital_status_i18n m on(((m.marital_status_id = c.marital_status_id) and (m.language_id = 1)))
left join char_persons_contact primary_mob_number on(((c.id = primary_mob_number.person_id) and
                                                                           (primary_mob_number.contact_type = 'primary_mobile')))
left join char_persons_contact wataniya_mob_number on(((c.id = wataniya_mob_number.person_id) and
                                                                            (wataniya_mob_number.contact_type = 'wataniya')))
left join char_persons_contact secondary_mob_number on(((c.id = secondary_mob_number.person_id) and
                                                                             (secondary_mob_number.contact_type = 'secondery_mobile')))
left join char_persons_contact phone_number on(((c.id = phone_number.person_id) and (phone_number.contact_type = 'phone')))
left join char_aids_locations_i18n country_name on(((c.adscountry_id = country_name.location_id) and
                                                                       (country_name.language_id = 1)))
left join char_aids_locations_i18n district_name on(((c.adsdistrict_id = district_name.location_id) and
                  (district_name.language_id = 1)))
left join char_aids_locations_i18n region_name on(((c.adsregion_id = region_name.location_id) and (region_name.language_id = 1)))
left join char_aids_locations_i18n location_name on(((c.adsneighborhood_id = location_name.location_id)
                   and (location_name.language_id = 1)))
left join char_aids_locations_i18n square_name on(((c.adssquare_id = square_name.location_id) and (square_name.language_id = 1)))
left join char_aids_locations_i18n mosques_name on(((c.adsmosques_id = mosques_name.location_id) and (mosques_name.language_id = 1)))
left join char_persons ind on((ind.id = char_vouchers_persons.individual_id))
left join char_marital_status_i18n min on(((min.marital_status_id = ind.marital_status_id) and (min.language_id = 1))) ;

-- --------------------------------------------------------


DROP VIEW  IF EXISTS char_vouchers_persons_view_en;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW char_vouchers_persons_view_en  AS
select char_vouchers_persons.id AS id,char_vouchers_persons.voucher_id AS voucher_id,char_vouchers_persons.person_id AS person_id,char_vouchers_persons.individual_id AS individual_id,char_vouchers_persons.receipt_date AS receipt_date,char_vouchers_persons.receipt_time AS receipt_time,char_vouchers_persons.receipt_location AS receipt_location,char_vouchers_persons.status AS rec_status,v.organization_id AS organization_id,v.category_id AS category_id,v.project_id AS project_id,org.name AS organization_name,org.en_name AS en_organization_name,sponsor.name AS sponsor_name,sponsor.en_name AS en_sponsor_name,v.type AS type,v.value AS value,(v.exchange_rate * v.value) AS shekel_value,v.title AS title,v.beneficiary AS beneficiary,v.currency_id AS currency_id,v.exchange_rate AS exchange_rate,v.template AS template,v.content AS content,v.header AS header,v.footer AS footer,v.notes AS notes,v.created_at AS created_at,v.updated_at AS updated_at,v.deleted_at AS deleted_at,v.sponsor_id AS sponsor_id,v.voucher_date AS voucher_date,v.count AS count,v.urgent AS urgent,v.status AS status,v.transfer AS transfer,v.transfer_company_id AS transfer_company_id,v.allow_day AS allow_day,char_vouchers_categories.name AS category_type,(case when isnull(v.project_id) then 'from_project' else 'manually' end) AS voucher_source,(case when (v.type = 1) then 'financial' when (v.type = 2) then 'non_financial' end) AS voucher_type,(case when (v.urgent = 0) then 'not_urgent' when (v.urgent = 1) then 'urgent' end) AS voucher_urgent,(case when (v.transfer = 0) then 'direct' when (v.transfer = 1) then 'transfer_company' end) AS transfer_status,(case when isnull(v.transfer_company_id) then '-' else char_transfer_company.name end) AS transfer_company_name,(case when isnull(v.currency_id) then '-' else char_currencies.name end) AS currency_name,(case when isnull(v.currency_id)
then '-' else char_currencies.en_name end) AS en_currency_name,concat(ifnull(c.first_name,' '),' ',ifnull(c.second_name,' '),' ',ifnull(c.third_name,' '),' ',ifnull(c.last_name,' ')) AS name,c.first_name AS first_name,c.second_name AS second_name,c.third_name AS third_name,c.last_name AS last_name,(case when isnull(c.marital_status_id) then ' ' else m.name end) AS marital_status,(case when isnull(c.gender) then ' ' when (c.gender = 1) then 'male' when (c.gender = 2) then 'female' end) AS gender,c.gender AS p_gender,(case when isnull(c.id_card_number) then ' ' else c.id_card_number end) AS id_card_number,char_get_family_count('left',c.id,NULL,c.id) AS family_count,concat(ifnull(ind.first_name,' '),' ',ifnull(ind.second_name,' '),' ',ifnull(ind.third_name,' '),' ',ifnull(ind.last_name,' ')) AS individual_name,ind.first_name AS ind_first_name,ind.second_name AS ind_second_name,ind.third_name AS ind_third_name,ind.last_name AS ind_last_name,(case when isnull(ind.id_card_number) then ' ' else ind.id_card_number end) AS individual_id_card_number,(case when isnull(ind.gender) then ' ' when (ind.gender = 1) then 'male' when (ind.gender = 2) then 'female' end) AS individual_gender,ind.gender AS ind_gender,(case when isnull(ind.marital_status_id) then ' ' else min.name end) AS individual_marital_status,(case when (v.beneficiary = 1) then concat(ifnull(c.first_name,' '),' ',ifnull(c.second_name,' '),' ',ifnull(c.third_name,' '),' ',ifnull(c.last_name,' ')) else concat(ifnull(ind.first_name,' '),' ',ifnull(ind.second_name,' '),' ',ifnull(ind.third_name,' '),' ',ifnull(ind.last_name,' ')) end) AS beneficiary_name,(case when isnull(primary_mob_number.contact_value) then ' ' else primary_mob_number.contact_value end) AS primary_mobile,(case when isnull(wataniya_mob_number.contact_value) then ' ' else wataniya_mob_number.contact_value end) AS wataniya,(case when isnull(secondary_mob_number.contact_value) then ' ' else secondary_mob_number.contact_value end) AS secondery_mobile,(case when isnull(phone_number.contact_value) then ' ' else phone_number.contact_value end) AS phone,ifnull(country_name.name,' ') AS country,ifnull(district_name.name,' ') AS district,ifnull(region_name.name,' ') AS region,ifnull(location_name.name,' ') AS nearlocation,ifnull(square_name.name,' ') AS square,ifnull(mosques_name.name,' ') AS mosque,ifnull(c.street_address,' ') AS street_address,concat(ifnull(country_name.name,' '),'  ',
ifnull(district_name.name,' '),'  ',ifnull(region_name.name,' '),'  ',ifnull(location_name.name,' '),'  ',
ifnull(square_name.name,' '),'  ',ifnull(mosques_name.name,' '),'  ',ifnull(c.street_address,' ')) AS full_address,
time_format(char_vouchers_persons.receipt_time,'%r') AS receipt_time_,char_vouchers_persons.status AS receipt_status,(case when (char_vouchers_persons.status = 1) then 'receipt' when (char_vouchers_persons.status = 0)
 then 'receipt' when (char_vouchers_persons.status = 2) then 'not_receipt' end) AS receipt_status_name
 ,v.case_category_id,ca.name as case_category_name,ca.en_name as en_case_category_name
from char_vouchers_persons
join char_vouchers v on((v.id = char_vouchers_persons.voucher_id))
join char_categories ca on((ca.id = v.case_category_id))
join char_organizations org on((org.id = v.organization_id))
join char_organizations sponsor on((sponsor.id = v.sponsor_id))
left join char_currencies on((char_currencies.id = v.currency_id))
left join char_transfer_company on((char_transfer_company.id = v.transfer_company_id))
left join char_vouchers_categories on((char_vouchers_categories.id = v.category_id))
join char_persons c on((c.id = char_vouchers_persons.person_id))
left join char_marital_status_i18n m on(((m.marital_status_id = c.marital_status_id) and (m.language_id = 2)))
left join char_persons_contact primary_mob_number on(((c.id = primary_mob_number.person_id) and
                                                                           (primary_mob_number.contact_type = 'primary_mobile')))
left join char_persons_contact wataniya_mob_number on(((c.id = wataniya_mob_number.person_id) and
                                                                            (wataniya_mob_number.contact_type = 'wataniya')))

left join char_persons_contact secondary_mob_number on(((c.id = secondary_mob_number.person_id) and
                                                           (secondary_mob_number.contact_type = 'secondery_mobile')))
left join char_persons_contact phone_number on(((c.id = phone_number.person_id) and (phone_number.contact_type = 'phone')))
left join char_aids_locations_i18n country_name on(((c.adscountry_id = country_name.location_id) and
 (country_name.language_id = 2)))
left join char_aids_locations_i18n district_name on(((c.adsdistrict_id = district_name.location_id) and
 (district_name.language_id = 2)))
left join char_aids_locations_i18n region_name on(((c.adsregion_id = region_name.location_id) and (region_name.language_id = 2)))
left join char_aids_locations_i18n location_name on(((c.adsneighborhood_id = location_name.location_id)
      and (location_name.language_id = 2)))
left join char_aids_locations_i18n square_name on(((c.adssquare_id = square_name.location_id) and (square_name.language_id = 2)))
left join char_aids_locations_i18n mosques_name on(((c.adsmosques_id = mosques_name.location_id) and (mosques_name.language_id = 2)))
left join char_persons ind on((ind.id = char_vouchers_persons.individual_id))
left join char_marital_status_i18n min on(((min.marital_status_id = ind.marital_status_id) and (min.language_id = 2))) ;
