
DELIMITER $$
CREATE  FUNCTION `char_get_project_person_organization_count`(`proId` INT, `key_id` INT, `key_type` TINYINT(4)) RETURNS  INT
BEGIN
DECLARE c  INT;

    if key_type  = 0 then
				 			set c = (
				 			          select count(`person_id`) from `aid_projects_persons`
				 			          where (`organization_id` = key_id  and `project_id` = proId )
				 			        );

		elseif key_type  = 1 then
				 		set c = (
			          select count(`person_id`) 
			          from `aid_projects_persons` 
			          where ( 
			                  (`project_id` = proId ) and 
			                  (`organization_id` in ( select char_user_organizations.organization_id
			                                          from char_user_organizations
			                                          where char_user_organizations.user_id = key_id) )
			                )
			        );
		elseif key_type  = 2 then
				 	set c = (
			          select count(`person_id`) 
			          from `aid_projects_persons` 
			          where ( 
			                  (`project_id` = proId ) and 
			                  (`organization_id` in ( select char_organizations_closure.descendant_id
			                                          from char_organizations_closure
			                                          where char_organizations_closure.ancestor_id = key_id) )
			                )
			        );
		end if;
		

RETURN c;
END$$
DELIMITER ;

