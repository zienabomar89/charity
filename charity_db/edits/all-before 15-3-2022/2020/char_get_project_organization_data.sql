DELIMITER $$
CREATE  FUNCTION `char_get_project_organization_data`(`proId` INT, `key_id` INT, `key_type` TINYINT(4), `target` TINYINT(4)) RETURNS  FLOAT
BEGIN
DECLARE c  FLOAT;

    if target is null then
            if key_type  = 0 then
               set c = ( select `quantity` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));

            elseif key_type  = 1 then
                   set c = ( select SUM(`quantity`)
                             from `aid_projects_organizations`
                             where ((`project_id` = proId ) and
                                    (`organization_id` in ( select char_user_organizations.organization_id
                                                            from char_user_organizations
                                                            where char_user_organizations.user_id = key_id) ))
                           );

            elseif key_type  = 2 then
                   set c = ( select `quantity` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));

            end if;
    else

            if key_type = 0 then
               set c = ( select `ratio` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));

            elseif key_type  = 1 then
                 set c = ( select SUM(`ratio`)
                           from `aid_projects_organizations`
                           where ((`project_id` = proId ) and
                                   (`organization_id` in ( select char_user_organizations.organization_id
                                                           from char_user_organizations
                                                           where char_user_organizations.user_id = key_id) ))
                        );

            elseif key_type  = 2 then
                   set c = ( select `ratio` from `aid_projects_organizations`  where (`organization_id` = key_id  and `project_id` = proId ));

            end if;

    end if;

RETURN c;
END$$
DELIMITER ;

