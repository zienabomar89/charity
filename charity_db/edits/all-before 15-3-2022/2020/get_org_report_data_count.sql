DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_org_report_data_count`(`id` INT, `options` VARCHAR(255)) RETURNS int(11)
BEGIN
declare c INTEGER;

		set c = ( select count(`d`.`value`)
                  FROM  `char_administrative_report_data` AS `d`
                  WHERE ( (`d`.`organization_id` = id ) and (FIND_IN_SET(`d`.`option_id`,options) != 0 ) )
                );
RETURN c;
END$$
DELIMITER ;