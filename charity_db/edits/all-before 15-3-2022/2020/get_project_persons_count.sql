DELIMITER $$
CREATE FUNCTION `get_project_persons_count`(`pId` INT(11) ,`statusId` INT(11)) RETURNS int(11)
BEGIN
declare c INTEGER;

		set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							 from `aid_projects_persons`
						 	 join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
						 	 where `aid_projects_persons`.`project_id` = pId and  `aid_projects_persons`.`status` = statusId
						  );

RETURN c;
END$$
DELIMITER ;