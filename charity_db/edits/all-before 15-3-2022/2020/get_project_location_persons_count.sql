DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_project_location_persons_count`(`pId` INT(11) ,
                                             `statusId` INT(11) , `loc_type` INT(11) ,`loc_id` INT(11) ,`orgId` INT(11)) RETURNS int(11)
BEGIN
declare c INTEGER;

		if loc_type  = 1 then
				  set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsdistrict_id` = loc_id
						  );
		elseif loc_type  = 2 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsregion_id` = loc_id
						  );
		elseif loc_type  = 3 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId and `char_persons`.`adsneighborhood_id` = loc_id
						  );
		 elseif loc_type  = 4 then
				  set c = (
							select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId  and `char_persons`.`adssquare_id` = loc_id
						  );
		elseif loc_type  = 5 then
				  set c = (
						   select COUNT(`aid_projects_persons`.`person_id`) AS `persons`
							from `aid_projects_persons`
							join `char_persons` on `char_persons`.`id` = `aid_projects_persons`.`person_id`
							where `aid_projects_persons`.`project_id` = pId  and
                     `aid_projects_persons`.`status` = statusId and
                     `aid_projects_persons`.`organization_id` = orgId  and `char_persons`.`adsmosques_id` = loc_id
						  );
		end if;

RETURN c;
END$$
DELIMITER ;