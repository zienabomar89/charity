UPDATE `char_forms_elements` SET `label` = 'المسكن - سقف وجدران المنزل' WHERE `char_forms_elements`.`id` = 27;
UPDATE `char_forms_elements` SET `label` = 'سقف وجدران المنزل ( المعيل )' WHERE `char_forms_elements`.`id` = 91;
UPDATE `char_forms_elements` SET `label` = 'نوع السقف وجدران المنزل' WHERE `char_forms_elements`.`id` = 115;
UPDATE `char_forms_elements` SET `label` = 'الدخل الشهري الكلي' WHERE `char_forms_elements`.`id` = 12;
UPDATE `char_forms_elements` SET `label` = 'الدخل الشهري الكلي' WHERE `char_forms_elements`.`id` = 62;
UPDATE `char_forms_elements` SET `label` = ' وضع الأثاث' WHERE `char_forms_elements`.`id` = 117;
UPDATE `char_forms_elements` SET `label` = 'المسكن - حالة المنزل/ المسكن' WHERE `char_forms_elements`.`id` = 28;
UPDATE `char_forms_elements` SET `label` = 'حالة المنزل/ المسكن (المعيل)' WHERE `char_forms_elements`.`id` = 92;
UPDATE `char_forms_elements` SET `label` = 'المسكن - حاجة المنزل للترميم' WHERE `char_forms_elements`.`id` = 30;
UPDATE `char_forms_elements` SET `label` = 'المسكن - تقييم وضع المنزل' WHERE `char_forms_elements`.`id` = 31;
----------------------------------------------------------------------------------
INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`) VALUES

(148, '1', '1', 'actual_monthly_income', 'الدخل الشهري الفعلي', 'رقم', NULL, '3', '0'),
(149, '1', '1', 'receivables', 'هل لديه ذمم مالية ؟', 'رقم', NULL, '3', '0'),
(150, '1', '1', 'receivables_sk_amount', 'قيمة الذمم بالشيكل', 'رقم', NULL, '3', '0'),

(151, '1', '1', 'has_commercial_records', 'هل لديه سجل تجاري', 'حقل رقمي', NULL, '3', '0'),
(152, '1', '1', 'active_commercial_records', 'عدد السجلات التجارية الفعالة', 'حقل رقمي', NULL, '3', '0'),
(153, '1', '1', 'gov_commercial_records_details', 'بيان السجلات التجارية', 'رقم', NULL, '3', '0'),

(154, '1', '1', 'visitor_card', 'رقم هوية الباحث الاجتماعي', 'رقم', NULL, '3', '0'),
(155, '1', '1', 'visitor_opinion', 'رأي الباحث الاجتماعي', 'حقل نصي', NULL, '3', '0'),
(156, '1', '1', 'visitor_evaluation', 'تقييم الباحث لحالة المنزل', 'حقل نصي', NULL, '3', '0'),

(157, '1', '1', 'has_health_insurance', 'هل لديك تأمين صحي', 'حقل رقمي', NULL, '3', '0'),
(158, '1', '1', 'health_insurance_number', 'رقم التأمين الصحي', 'حقل رقمي', NULL, '3', '0'),
(159, '1', '1', 'health_insurance_type', 'نوع التأمين الصحي', 'رقم', NULL, '3', '0'),

(160, '1', '1', 'has_device', 'هل يستخدم جهاز', 'حقل رقمي', NULL, '3', '0'),
(161, '1', '1', 'used_device_name', 'اسم الجهاز المستخدم', 'حقل رقمي', NULL, '3', '0'),

(162, '1', '1', 'need_repair', 'هل بحاجة لترميم المنزل للموائمة', 'حقل رقمي', NULL, '3', '0'),
(163, '1', '1', 'repair_notes', 'وصف وضع المنزل', 'حقل رقمي', NULL, '3', '0'),

(164, '1', '1', 'fm_currently_study', 'ملتحق حاليا ؟', 'حقل رقمي', NULL, '3', '0'),

(165, '1', '1', 'gov_work_details', 'ملخص بيانات العمل', 'حقل رقمي', NULL, '3', '0'),
(166, '1', '1', 'gov_health_details', 'ملخص التقارير الطبية', 'حقل رقمي', NULL, '3', '0'),
(167, '1', '1', 'has_other_work_resources', 'هل لديه مصادر دخل أخرى ؟', 'حقل رقمي', NULL, '3', '0'),

(168, '1', '1', 'is_qualified', 'أهلية رب الأسرة', 'حقل رقمي', NULL, '3', '0'),
(169, '1', '1', 'qualified_card_number', 'رقم هوية المعييل', 'حقل رقمي', NULL, '3', '0');
----------------------------------------------------------------------------------

INSERT INTO `char_categories_form_element_priority` (`category_id`, `element_id`, `priority`) VALUES
(5, 148, 3),
(5, 149, 3),
(5, 150, 3),
(5, 151, 3),
(5, 152, 3),
(5, 153, 3),
(5, 154, 3),
(5, 155, 3),
(5, 156, 3),
(5, 157, 3),
(5, 158, 3),
(5, 159, 3),
(5, 160, 3),
(5, 161, 3),
(5, 162, 3),
(5, 163, 3),
(5, 164, 3),
(5, 165, 3),
(5, 166, 3),
(6, 148, 3),
(6, 149, 3),
(6, 150, 3),
(6, 151, 3),
(6, 152, 3),
(6, 153, 3),
(6, 154, 3),
(6, 155, 3),
(6, 156, 3),
(6, 157, 3),
(6, 158, 3),
(6, 159, 3),
(6, 160, 3),
(6, 161, 3),
(6, 162, 3),
(6, 163, 3),
(6, 164, 3),
(6, 165, 3),
(6, 166, 3),
(7, 148, 3),
(7, 149, 3),
(7, 150, 3),
(7, 151, 3),
(7, 152, 3),
(7, 153, 3),
(7, 154, 3),
(7, 155, 3),
(7, 156, 3),
(7, 157, 3),
(7, 158, 3),
(7, 159, 3),
(7, 160, 3),
(7, 161, 3),
(7, 162, 3),
(7, 163, 3),
(7, 164, 3),
(7, 165, 3),
(7, 166, 3),
(5, 167, 3),
(6, 167, 3),
(7, 167, 3),

(5, 168, 3),
(6, 168, 3),
(7, 168, 3),

(5, 169, 3),
(6, 169, 3),
(7, 169, 3);

----------------------------------------------------------------------------------

ALTER TABLE `char_persons` CHANGE `deserted` `deserted` TINYINT(4) NULL DEFAULT '0' COMMENT 'مهجورة في حال انثى';
ALTER TABLE `char_persons` CHANGE `spouses` `spouses` TINYINT(1) NULL DEFAULT NULL COMMENT 'عدد الزوجات';
ALTER TABLE `char_persons` CHANGE `monthly_income` `monthly_income` FLOAT NULL DEFAULT NULL COMMENT 'الدخل الشهري الكلي';

ALTER TABLE `char_persons` ADD `is_qualified` tinyint(1) DEFAULT '1'  AFTER `id_card_number`;
ALTER TABLE `char_persons` ADD `qualified_card_number` float NULL AFTER `is_qualified`;

ALTER TABLE `char_persons` ADD `actual_monthly_income` float NULL AFTER `monthly_income`;
ALTER TABLE `char_persons` ADD `receivables` TINYINT(3) NOT NULL DEFAULT '0' AFTER `actual_monthly_income`;
ALTER TABLE `char_persons` ADD `receivables_sk_amount` float NULL AFTER `receivables`;
ALTER TABLE `char_persons` ADD `has_commercial_records` TINYINT(3) NOT NULL DEFAULT '0' AFTER `receivables_sk_amount`;
ALTER TABLE `char_persons` ADD `active_commercial_records` int(10) NOT NULL DEFAULT '0' AFTER `has_commercial_records`;
ALTER TABLE `char_persons` ADD `gov_commercial_records_details` text DEFAULT NULL AFTER `active_commercial_records`;

----------------------------------------------------------------------------------

ALTER TABLE `char_cases` ADD `visitor_card` int(10) NOT NULL DEFAULT '0' AFTER `visitor`;
ALTER TABLE `char_cases` ADD `visitor_evaluation` TINYINT(3) NOT NULL DEFAULT '0' AFTER `visitor_card`;
ALTER TABLE `char_cases` ADD `visitor_opinion` text DEFAULT NULL AFTER `visitor_evaluation`;

ALTER TABLE `char_persons_health` ADD `has_health_insurance` TINYINT(3) NOT NULL DEFAULT '0' AFTER `disease_id`;
ALTER TABLE `char_persons_health` ADD `health_insurance_number` int(10) NULL DEFAULT '0' AFTER `has_health_insurance`;
ALTER TABLE `char_persons_health` ADD `health_insurance_type` text DEFAULT NULL AFTER `health_insurance_number`;

ALTER TABLE `char_persons_health` ADD `has_device` TINYINT(3) NOT NULL DEFAULT '0' AFTER `health_insurance_type`;
ALTER TABLE `char_persons_health` ADD `used_device_name` text DEFAULT NULL AFTER `has_device`;
ALTER TABLE `char_persons_health` ADD `gov_health_details` text DEFAULT NULL AFTER `used_device_name`;

ALTER TABLE `char_residence` ADD `need_repair` TINYINT(3) NOT NULL DEFAULT '0' AFTER `area`;
ALTER TABLE `char_residence` ADD `repair_notes` text DEFAULT NULL AFTER `need_repair`;
ALTER TABLE `char_residence` CHANGE `property_type_id` `property_type_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'كود نوع الملكية', CHANGE `roof_material_id` `roof_material_id` INT(10) UNSIGNED NULL DEFAULT NULL COMMENT 'سقف المنزل';

ALTER TABLE `char_persons_education` ADD `currently_study` TINYINT(3) NOT NULL DEFAULT '0' AFTER `study`;

ALTER TABLE `char_persons_work` ADD `gov_work_details` text DEFAULT NULL AFTER `work_location`;
ALTER TABLE `char_persons_work` ADD `has_other_work_resources` TINYINT(3) NOT NULL DEFAULT '0' AFTER `work_location`;

------------------------------------
ALTER TABLE `char_vouchers` ADD `center` TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'هل المشروع مركزي : 0 غير مركزي ، 1 مركزي' AFTER `type`;
