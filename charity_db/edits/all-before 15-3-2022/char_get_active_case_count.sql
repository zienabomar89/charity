DELIMITER $$
CREATE FUNCTION `char_get_active_case_count`(`id` INT) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الحالات الفعالة للشخص لدى كافة الجمعيات'
BEGIN
     declare c INTEGER;
     set c = ( SELECT COUNT(`char_cases`.`id`)
               FROM `char_cases`
               WHERE `char_cases`.`status` = 0 and `char_cases`.`person_id` = id and  `char_cases`.`category_id` in (SELECT  `char_categories`.`id`  FROM `char_categories` WHERE `type` = 2) ) ;
RETURN c;
END$$
DELIMITER ;