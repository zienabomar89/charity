DROP FUNCTION IF EXISTS `char_get_sponsor_voucher_person_amount`;
DROP FUNCTION IF EXISTS char_get_sponsor_voucher_person_count;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsor_voucher_person_amount`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب قيمة القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT SUM(`char_vouchers`.`value` * `char_vouchers`.`exchange_rate`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_get_sponsor_voucher_person_count`(`p_person_id` INT, `spon_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
    COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما من جهة كافلة معينة'
BEGIN
DECLARE c INTEGER;
 if ( p_date_from is null OR p_date_from = 'null' ) and (p_date_to is null OR p_date_to = 'null')  then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );
    elseif ( p_date_from is not null And p_date_from != 'null' ) and (p_date_to is null OR p_date_to = 'null') then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );
    elseif ( p_date_from is null OR p_date_from = 'null' ) and ( p_date_to is not null And p_date_to != 'null' ) then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );
    elseif ( p_date_from is not null And p_date_from != 'null' )  and ( p_date_to is not null And p_date_to != 'null' )then
		set c = (
              SELECT COUNT(`char_vouchers`.`value` )
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id` and  `char_vouchers`.`sponsor_id` = spon_id
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;
RETURN c;
END$$
DELIMITER ;