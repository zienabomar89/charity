

CREATE TABLE `aid_projects_nominations` (
  `id` int(10) NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `label` varchar(255) NOT NULL COMMENT 'التسمية',
  `project_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم المشروع',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم المستخدم	',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ التعديل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على ترشيحات المشروع';


ALTER TABLE `aid_projects_nominations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aid_projects_nominations_projectId_idx` (`project_id`),
  ADD KEY `aid_projects_nominations_userId_idx` (`user_id`);


ALTER TABLE `aid_projects_nominations`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=4;

ALTER TABLE `aid_projects_nominations`
  ADD CONSTRAINT `fk_aid_projects_nominations_projectId` FOREIGN KEY (`project_id`) REFERENCES `aid_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aid_projects_nominations_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `aid_projects_rules` ADD `nomination_id` INT NOT NULL COMMENT 'رقم الترشيح' FIRST;
ALTER TABLE `aid_projects_persons`  ADD `nomination_id` INT NOT NULL COMMENT 'رقم الترشيح'  FIRST;

ALTER TABLE `aid_projects_rules` DROP PRIMARY KEY;
ALTER TABLE `aid_projects_rules` ADD PRIMARY KEY (`nomination_id`,`project_id`,`rule_name`);