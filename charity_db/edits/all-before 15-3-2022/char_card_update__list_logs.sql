

CREATE TABLE `char_card_update__list_logs` (
  `id` int(11) NOT NULL,
  `id_card_number` int(9) NOT NULL COMMENT 'رقم الهوية',
  `source` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'المصدر 0 من النظام ، 1 من الخدمات الالكترونية',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'حالة التحديث 0 : غير محدث ، 1 محدث',
  `user_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'اسم المستخدم',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'تاريخ التحديث '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يتم من خلاله اضافة أرقام الهوية لجدولتها ضمن التحديث';

ALTER TABLE `char_card_update__list_logs` ADD PRIMARY KEY (`id`);
ALTER TABLE `char_card_update__list_logs` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
