INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(Null, 8, 'org.AdministrativeReport.manage', 'إدارة التقارير الإدارية'),
(Null, 8, 'org.AdministrativeReport.manageOrg', 'بيانات التقارير الإدارية'),
(Null, 8, 'org.AdministrativeReport.create', 'إضافة تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.view', 'عرض تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.update', 'تحرير بيانات تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.OrgUpdate', 'إدخال بيانات تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.delete', 'حذف تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.upload', 'رفع قالب تقرير إداري'),
(Null, 8, 'org.AdministrativeReport.download', 'تنزيل تقرير إداري');


CREATE TABLE `char_administrative_report` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'اسم التقرير',
  `template` varchar(255) DEFAULT NULL,
  `date` date NOT NULL COMMENT 'تاريخ انشاء التقرير',
  `month` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `date_from` date NOT NULL COMMENT 'تاريخ بداية فترة التقرير',
  `date_to` date NOT NULL COMMENT 'تاريخ نهاية فترة التقرير',
  `type` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'تصنيف التقرير : شهري - ربع سنوي - نصف سنوي - سنوي',
  `status` tinyint(3) UNSIGNED DEFAULT '1',
  `notes` text,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول التقارير الادارية';

-- --------------------------------------------------------

--
-- Table structure for table `char_administrative_report_data`
--

CREATE TABLE `char_administrative_report_data` (
  `option_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `list_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'في حال كان الحقل متعدد الخيارات تخزن هنا قيمة الخيار أو تترك فارغة في حالة الحقول غير متعددة الخيارات',
  `value` text,
  `amount` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على البيانات الخاصة بالجمعيات لكل حقل في التقرير';

-- --------------------------------------------------------

--
-- Table structure for table `char_administrative_report_tab`
--

CREATE TABLE `char_administrative_report_tab` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'اسم التبويبة',
  `description` text COMMENT 'الوصف',
  `report_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم التقرير',
  `status` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'الحالة',
  `type` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'تصنيف التبويبة :  نصية فقط - رقمية فقط - نصية ورقمية',
  `order` tinyint(3) DEFAULT NULL COMMENT 'الترتيب',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول تبويبات التقارير الادارية';

-- --------------------------------------------------------

--
-- Table structure for table `char_administrative_report_tab_options`
--

CREATE TABLE `char_administrative_report_tab_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `tab_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم التبويبة',
  `name` varchar(255) NOT NULL COMMENT 'الاسم باللغة الانجليزية',
  `label` varchar(255) DEFAULT NULL COMMENT 'مسمى الخيار',
  `type` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'تصنيف التبويبة :  نصية فقط - رقمية فقط - نصية ورقمية',
  `priority` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول تبويبات التقارير الادارية';

-- --------------------------------------------------------

--
-- Table structure for table `char_administrative_report_tab_option_list`
--

CREATE TABLE `char_administrative_report_tab_option_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `char_administrative_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_organizationId_idx` (`organization_id`),
  ADD KEY `fk_char_administrative_report_userId_idx` (`user_id`);

--
-- Indexes for table `char_administrative_report_data`
--
ALTER TABLE `char_administrative_report_data`
  ADD PRIMARY KEY (`organization_id`,`option_id`),
  ADD KEY `fk_administrative_report_data_organization_id_idx` (`organization_id`),
  ADD KEY `fk_administrative_report_data_list_id_idx` (`list_id`),
  ADD KEY `fk_administrative_report_data_option_id` (`option_id`);

--
-- Indexes for table `char_administrative_report_tab`
--
ALTER TABLE `char_administrative_report_tab`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_tab_reportId_idx` (`report_id`);

--
-- Indexes for table `char_administrative_report_tab_options`
--
ALTER TABLE `char_administrative_report_tab_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_tab_options_tabId_idx` (`tab_id`);

--
-- Indexes for table `char_administrative_report_tab_option_list`
--
ALTER TABLE `char_administrative_report_tab_option_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_tab_option_id_idx` (`option_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `char_administrative_report`
--
ALTER TABLE `char_administrative_report`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `char_administrative_report_tab`
--
ALTER TABLE `char_administrative_report_tab`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `char_administrative_report_tab_options`
--
ALTER TABLE `char_administrative_report_tab_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `char_administrative_report_tab_option_list`
--
ALTER TABLE `char_administrative_report_tab_option_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `char_administrative_report`
--
ALTER TABLE `char_administrative_report`
  ADD CONSTRAINT `fk_char_administrative_report_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_char_administrative_report_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_administrative_report_data`
--
ALTER TABLE `char_administrative_report_data`
  ADD CONSTRAINT `fk_administrative_report_data_list_id` FOREIGN KEY (`list_id`) REFERENCES `char_administrative_report_tab_option_list` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrative_report_data_option_id` FOREIGN KEY (`option_id`) REFERENCES `char_administrative_report_tab_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_administrative_report_data_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_administrative_report_tab`
--
ALTER TABLE `char_administrative_report_tab`
  ADD CONSTRAINT `fk_char_administrative_report_tab_reportId` FOREIGN KEY (`report_id`) REFERENCES `char_administrative_report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_administrative_report_tab_options`
--
ALTER TABLE `char_administrative_report_tab_options`
  ADD CONSTRAINT `fk_char_administrative_report_tab_options_tabId` FOREIGN KEY (`tab_id`) REFERENCES `char_administrative_report_tab` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_administrative_report_tab_option_list`
--
ALTER TABLE `char_administrative_report_tab_option_list`
  ADD CONSTRAINT `fk_char_administrative_report_tab_option_id` FOREIGN KEY (`option_id`) REFERENCES `char_administrative_report_tab_options` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------


CREATE TABLE `char_administrative_report_organization` (
  `id` int(10) UNSIGNED NOT NULL,
   `report_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم التقرير',
  `organization_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول التقارير الادارية';

ALTER TABLE `char_administrative_report_organization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_administrative_report_organization_organizationId_idx` (`organization_id`),
 ADD KEY `fk_char_administrative_report_organization_reportId_idx` (`report_id`);


ALTER TABLE `char_administrative_report_organization`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `char_administrative_report_organization`
ADD CONSTRAINT `fk_char_administrative_report_organization_reportId` FOREIGN KEY (`report_id`) REFERENCES `char_administrative_report`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `char_administrative_report_organization`
ADD CONSTRAINT `fk_char_administrative_report_organization_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


