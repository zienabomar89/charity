ALTER TABLE `char_payments_recipient`
ADD `exchange_rate` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `amount`,
ADD `organization_share` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `exchange_rate`,
ADD `administration_fees` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `organization_share`;

ALTER TABLE `char_payments_cases`
ADD `exchange_rate` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `amount`,
ADD `organization_share` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `exchange_rate`,
ADD `administration_fees` FLOAT UNSIGNED NOT NULL DEFAULT ''1'' AFTER `organization_share`;

UPDATE  char_payments t1,  char_payments_cases t2
SET     t2.exchange_rate    = t1.exchange_rate ,
        t2.organization_share    = t1.organization_share ,
        t2.administration_fees    = t1.administration_fees
WHERE   t1.id = t2.payment_id ;

UPDATE  char_payments t1,  char_payments_recipient t2
SET     t2.exchange_rate    = t1.exchange_rate ,
        t2.organization_share    = t1.organization_share ,
        t2.administration_fees    = t1.administration_fees
WHERE   t1.id = t2.payment_id ;

UPDATE  char_payments_cases t2
SET     t2.amount_after_discount        = (t2.amount - (t2.amount * t2.organization_share )) ,
        t2.shekel_amount_before_discount = (t2.amount * t2.exchange_rate) ,
        t2.shekel_amount                = ROUND( (t2.amount - (t2.amount * t2.organization_share ) ) * t2.exchange_rate)
WHERE   1 ;

UPDATE  char_payments_recipient t2
SET     t2.amount_after_discount        = (t2.amount - (t2.amount * t2.organization_share )) ,
        t2.shekel_amount_before_discount = (t2.amount * t2.exchange_rate) ,
        t2.shekel_amount                = ROUND( (t2.amount - (t2.amount * t2.organization_share ) ) * t2.exchange_rate)
WHERE   1 ;

ALTER TABLE `char_payments_recipient` DROP `exchange_rate`, DROP `organization_share`, DROP `administration_fees`;
ALTER TABLE `char_payments_cases` DROP `exchange_rate`, DROP `organization_share`, DROP `administration_fees`;
