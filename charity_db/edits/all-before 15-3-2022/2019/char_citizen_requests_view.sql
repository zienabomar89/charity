

CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_citizen_requests_view_en`  AS

select  char_citizens.first_name  AS  first_name , char_citizens.second_name  AS  second_name , char_citizens.third_name  AS  third_name ,
 char_citizens.last_name  AS  last_name , char_citizens.id_card_number  AS  id_card_number ,
concat(ifnull( char_citizens.first_name ,' '),' ',ifnull( char_citizens.second_name ,' '),' ',ifnull( char_citizens.third_name ,' '),
' ',ifnull( char_citizens.last_name ,' ')) AS  full_name , char_citizens.mobile  AS  mobile ,
 char_citizens.wataniya  AS  wataniya , char_citizens.phone  AS  phone , char_citizens.prev_family_name  AS  prev_family_name ,
 char_citizens.gender  AS  gender , char_citizens.marital_status_id  AS  marital_status_id ,
   ms.name AS  marital_status ,
 char_citizens.birthday  AS  birthday ,
 char_citizens.adscountry_id  AS  adscountry_id ,
 char_citizens.adsdistrict_id  AS  adsdistrict_id ,
  char_citizens.adsregion_id  AS  adsregion_id ,
 char_citizens.adsneighborhood_id  AS  adsneighborhood_id ,
  char_citizens.adssquare_id  AS  adssquare_id ,
 char_citizens.adsmosques_id  AS  adsmosques_id ,
 ifnull( country.name ,' ')   AS country_name,
 ifnull( district.name ,' ')   AS district_name,
 ifnull( region.name ,' ')   AS region_name,
 ifnull( location.name ,' ')  AS nearLocation_name,
 ifnull( square.name ,' ')    AS square_name,
 ifnull( mosques.name ,' ')   AS mosque_name,
 char_citizens.street_address  AS street_address,
  char_citizen_requests.*
from   char_citizen_requests
join  char_citizens  on  char_citizens.id  =  char_citizen_requests.citizen_id
left join  char_marital_status_i18n  as ms on  ms.marital_status_id  =  char_citizens.marital_status_id and  ms.language_id = 2
left join char_aids_locations_i18n As country on char_citizens.adscountry_id = country.location_id and country.language_id = 2
left join char_aids_locations_i18n As district on char_citizens.adsdistrict_id = district.location_id and district.language_id = 2
left join char_aids_locations_i18n As region on char_citizens.adsregion_id = region.location_id  and region.language_id = 2
left join char_aids_locations_i18n As location  on char_citizens.adsneighborhood_id = location.location_id and location.language_id = 2
left join char_aids_locations_i18n As square on char_citizens.adssquare_id = square.location_id and square.language_id = 2
left join char_aids_locations_i18n As mosques on char_citizens.adsmosques_id = mosques.location_id and mosques.language_id = 2
 order by  char_citizen_requests.created_at  desc ;

