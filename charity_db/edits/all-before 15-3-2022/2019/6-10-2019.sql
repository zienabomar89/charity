DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_mosque_person_count`(`vId` INT(11) , `mosque_id` INT(11)) RETURNS int(11)
BEGIN
declare c INTEGER;
		set c = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons`
					from `char_vouchers_persons`
					join `char_persons` on `char_persons`.`id` = `char_vouchers_persons`.`person_id`
					where `char_vouchers_persons`.`voucher_id` = vId and `char_persons`.`adsmosques_id` = mosque_id);
RETURN c;
END$$
DELIMITER ;

ALTER TABLE `char_vouchers` ADD `beneficiary` TINYINT NOT NULL DEFAULT '1' AFTER `title`;
ALTER TABLE `char_users` ADD `type` TINYINT NOT NULL DEFAULT '1' AFTER `username`;
ALTER TABLE `char_users` ADD `connect_with_sponsor` TINYINT NULL DEFAULT '0' AFTER `type`;

UPDATE `char_users` SET `char_users`.`type` = 5 WHERE 1;
UPDATE `char_users` SET `char_users`.`type` = 0 WHERE `char_users`.super_admin =1;
UPDATE `char_users` SET `char_users`.`type` = 1 WHERE `char_users`.firstname LIKE '%مدير منطقة%';
UPDATE `char_users` SET `char_users`.`type` = 3 WHERE `char_users`.firstname LIKE '%مدير جمعية%';
UPDATE `char_users` SET `char_users`.`type` = 4 WHERE `char_users`.`organization_id` in (SELECT id FROM `char_organizations` WHERE type != 1);

UPDATE `char_users` SET `char_users`.`connect_with_sponsor` = 0 WHERE 1;
UPDATE `char_users` SET `char_users`.`connect_with_sponsor` = 1 WHERE `char_users`.super_admin =1;
UPDATE `char_users` SET `char_users`.`connect_with_sponsor` = 1 WHERE `char_users`.firstname LIKE '%مدير منطقة%';
UPDATE `char_users` SET `char_users`.`connect_with_sponsor` = 1 WHERE `char_users`.firstname LIKE '%مدير جمعية%';

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (417, '10', 'reports.case.paymentsStatistic', 'إحصائية الصرفيات');
ALTER TABLE `char_persons` ADD`card_type` varchar(45) DEFAULT 1  AFTER `id_card_number`;
ALTER TABLE `aid_projects` ADD `allow_day` INT(10) NOT NULL DEFAULT '0' AFTER `deleted_at`;
ALTER TABLE `aid_projects` ADD `date` DATE NULL AFTER `allow_day`;
ALTER TABLE `char_vouchers` ADD `allow_day` INT(10) NOT NULL DEFAULT '0' AFTER `transfer_company_id`;

ALTER TABLE `char_visitor_notes`
 ADD `en_for_male`   text NULL DEFAULT NULL AFTER `for_female`,
 ADD `en_for_female` text NULL DEFAULT NULL AFTER `en_for_male`;

ALTER TABLE `char_templates`
 ADD `en_filename`   varchar(255) NULL DEFAULT NULL AFTER `other_filename`,
 ADD `en_other_filename` varchar(255) NULL DEFAULT NULL AFTER `en_filename`;

ALTER TABLE `char_reports` ADD `language` VARCHAR(45) NOT NULL DEFAULT 'ar' AFTER `title`;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '1','auth.suggestion.create', 'إضافة مقترح ');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '1','auth.suggestion.manage', 'استعراض مقترحاتي');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '1','auth.suggestion.manageSuggestion', 'إدارة المقترحات');

INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`) VALUES (NULL, '1', '1', 'card_type', 'نوع رقم الهوية', 'حقل رقمي', NULL, '4', '0');
INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`) VALUES (NULL, '2', '1', 'card_type', 'نوع رقم الهوية', 'حقل رقمي', NULL, '4', '0');

INSERT INTO `char_categories_form_element_priority`(`category_id`, `element_id`, `priority`) SELECT category_id , 143 , 4 FROM `char_categories_form_element_priority` where category_id in (select id from char_categories where type = 1) GROUP by category_id;
INSERT INTO `char_categories_form_element_priority`(`category_id`, `element_id`, `priority`) SELECT category_id , 142 , 4 FROM `char_categories_form_element_priority` where category_id in (select id from char_categories where type = 2) GROUP by category_id;


INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`)
VALUES
(145, '1', '1', 'wataniya', 'رقم الجوال (وطنية)', 'حقل رقمي', NULL, '3', '0'),
(146, '2', '1', 'guardian_wataniya', 'رقم الجوال (وطنية) - المعيل', '', NULL, '4', '0'),
(147, '2', '1', 'wataniya', 'رقم الجوال (وطنية)', '', NULL, '4', '0');

--  aids
INSERT INTO `char_categories_form_element_priority`(`category_id`, `element_id`, `priority`)
 SELECT category_id , 145 , 3 FROM `char_categories_form_element_priority`
 where category_id in (select id from char_categories where type = 2) GROUP by category_id;

-- sponsorship
 INSERT INTO `char_categories_form_element_priority`(`category_id`, `element_id`, `priority`)
 SELECT category_id , 146 , 3 FROM `char_categories_form_element_priority`
 where category_id in (select id from char_categories where type = 1) GROUP by category_id;

 INSERT INTO `char_categories_form_element_priority`(`category_id`, `element_id`, `priority`)
 SELECT category_id , 147 , 3 FROM `char_categories_form_element_priority`
 where category_id in (select id from char_categories where type = 1) GROUP by category_id;

