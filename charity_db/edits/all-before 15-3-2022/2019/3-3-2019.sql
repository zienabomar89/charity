/**   aid_repositories sql edit **/
ALTER TABLE `aid_repositories`
  ADD `date` DATE NOT NULL COMMENT 'تاريخ الوعاء' AFTER `name`,
  ADD `notes` TEXT NULL COMMENT 'ملاحظات على الوعاء' AFTER `date`,
  ADD `organization_id` int(10) UNSIGNED NOT NULL COMMENT 'الجمعية التي أنشأت الوعاء' AFTER `name`,
  ADD `user_id` int(10) UNSIGNED NOT NULL COMMENT 'المستخدم الذب أنشأ الوعاء' AFTER `organization_id`;
ALTER TABLE `aid_repositories` ADD `level` TINYINT NOT NULL DEFAULT '1' AFTER `date`;

ALTER TABLE `aid_repositories`
  ADD KEY `fk_aid_repositories_organizationId_idx` (`organization_id`),
  ADD KEY `fk_aid_repositories_userId_idx` (`user_id`);

ALTER TABLE `aid_repositories`
   ADD CONSTRAINT `fk_aid_repositories_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_aid_repositories_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `aid_projects` ADD `currency_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `name`;
ALTER TABLE `aid_projects` ADD `exchange_rate` FLOAT NOT NULL DEFAULT '1' AFTER `currency_id`;
ALTER TABLE `aid_projects` ADD `custom_ratio` FLOAT NOT NULL DEFAULT '0' AFTER `organization_ratio`;
ALTER TABLE `aid_projects` ADD KEY `fk_aid_projects_currency_id_idx` (`currency_id`);
ALTER TABLE `aid_projects` ADD CONSTRAINT `fk_aid_projects_currency_id_fk` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `aid_projects` ADD `adjust` TINYINT NOT NULL DEFAULT '0' AFTER `notes`;


ALTER TABLE `aid_projects` ADD `custom_organization` FLOAT NOT NULL DEFAULT '0' AFTER `organization_ratio`;
ALTER TABLE `aid_projects` ADD KEY `fk_aid_repositories_custom_organizationId_idx` (`custom_organization`);

