﻿ALTER TABLE `char_payments_cases` CHANGE `shekel_amount_after_discount` `shekel_amount_before_discount` DOUBLE NOT NULL DEFAULT '0';
ALTER TABLE `char_payments_recipient` CHANGE `shekel_amount_after_discount` `shekel_amount_before_discount` DOUBLE NOT NULL DEFAULT '0';
ALTER TABLE `char_payments_documents` ADD `attachment_type` TINYINT NOT NULL DEFAULT '4' AFTER `document_id`;

-- --------------------------------------------------------

CREATE TABLE `char_internal_messages` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `msg_id` varchar(45) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `details` longtext,
  `msg_importance_id` int(11) DEFAULT NULL,
  `msg_type_id` int(11) DEFAULT NULL,
  `msg_status_id` int(10) UNSIGNED NOT NULL,
  `msg_replay_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

CREATE TABLE `char_internal_message_attachments` (
  `id` int(11) NOT NULL,
  `n ame` varchar(45) DEFAULT NULL,
  `file_id` int(10) UNSIGNED DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


CREATE TABLE `char_internal_message_receipt` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `receipt_user_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `msg_id` int(11) NOT NULL,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

CREATE TABLE `char_message_importance` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `char_message_importance` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'عادي', NULL, NULL),
(2, 'متوسط', NULL, NULL),
(3, 'عاجل', NULL, NULL);

-- --------------------------------------------------------

CREATE TABLE `char_message_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `char_message_status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'فعالة', NULL, NULL),
(2, 'مغلقة', NULL, NULL);

-- --------------------------------------------------------

CREATE TABLE `char_message_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `char_message_type` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'صادر', NULL, NULL),
(2, 'وارد', NULL, NULL);


ALTER TABLE `char_internal_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_internal_message$msg_replay_id_indx` (`msg_replay_id`),
  ADD KEY `char_internal_message$user_id_indx` (`user_id`),
  ADD KEY `char_internal_message$msg_importance_id_indx` (`msg_importance_id`),
  ADD KEY `char_internal_message$msg_type_id_indx` (`msg_type_id`),
  ADD KEY `char_internal_message$msg_status_id_indx` (`msg_status_id`);

ALTER TABLE `char_internal_message_attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_internal_message_attachments$file_id_indx` (`file_id`),
  ADD KEY `char_internal_message_attachments$msg_id_indx` (`msg_id`);

ALTER TABLE `char_internal_message_receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_internal_message_receipt$receipt_user_id_indx` (`receipt_user_id`),
  ADD KEY `char_internal_message_receipt$msg_id_indx` (`msg_id`);

ALTER TABLE `char_message_importance`  ADD PRIMARY KEY (`id`);
ALTER TABLE `char_message_status`ADD PRIMARY KEY (`id`);
ALTER TABLE `char_message_type`  ADD PRIMARY KEY (`id`);


ALTER TABLE `char_internal_messages`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `char_internal_message_attachments`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `char_internal_message_receipt` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `char_message_importance` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `char_message_status`  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `char_message_type`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

ALTER TABLE `char_internal_messages`
  ADD CONSTRAINT `char_internal_message$msg_importance_id_fk` FOREIGN KEY (`msg_importance_id`) REFERENCES `char_message_importance` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `char_internal_message$msg_status_id_fk` FOREIGN KEY (`msg_status_id`) REFERENCES `char_message_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `char_internal_message$msg_type_id_fk` FOREIGN KEY (`msg_type_id`) REFERENCES `char_message_type` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  ADD CONSTRAINT `char_internal_message$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `char_internal_message_attachments`
  ADD CONSTRAINT `char_internal_message_attachments$file_id_fk` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `char_internal_message_attachments$msg_id_fk` FOREIGN KEY (`msg_id`) REFERENCES `char_internal_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `char_internal_message_receipt`
  ADD CONSTRAINT `char_internal_message_receipt$msg_id_fk` FOREIGN KEY (`msg_id`) REFERENCES `char_internal_messages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `char_internal_message_receipt$receipt_user_id_fk` FOREIGN KEY (`receipt_user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `char_organizations` ADD`en_name` varchar(255) NOT NULL  AFTER `name`;
ALTER TABLE `char_organizations` ADD`email` varchar(255) NOT NULL  AFTER `en_name`;
ALTER TABLE `char_organizations` ADD`mobile` varchar(45) DEFAULT NULL  AFTER `email`;
ALTER TABLE `char_organizations` ADD`wataniya` varchar(45) DEFAULT NULL  AFTER `mobile`;
ALTER TABLE `char_organizations` ADD`phone` varchar(45) DEFAULT NULL  AFTER `mobile`;
ALTER TABLE `char_organizations` ADD`fax` varchar(45) DEFAULT NULL  AFTER `phone`;

ALTER TABLE `char_notifications` ADD`user_id_to` int(10) UNSIGNED NOT NULL  AFTER `user_id`;
ALTER TABLE `char_notifications` ADD KEY `fk_notifications_userToId_idx` (`user_id_to`) ;
ALTER TABLE `char_notifications` CHANGE `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP();
ALTER TABLE ADD CONSTRAINT `fk_notifications_userToId` FOREIGN KEY (`user_id_to`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DROP TABLE IF EXISTS `auth_users_permissions_view`;
CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `auth_users_permissions_view`  AS
select `char_roles_permissions`.`permission_id` AS `permission_id`,
`char_permissions`.`name` AS `name`,`char_permissions`.`code` AS `code`,
`char_permissions`.`module_id` AS `module_id`,`char_roles_permissions`.`created_at` AS `created_at`,
`char_users_roles`.`user_id` AS `user_id`,`char_users`.`organization_id` AS `organization_id`,`char_roles_permissions`.`role_id` AS `role_id`,`char_roles`.`name` AS `role_name`,
`char_roles_permissions`.`allow` AS `allow`,`char_roles_permissions`.`created_by` AS `created_by`,
`char_users`.`firstname` AS `created_by_firstname`,`char_users`.`lastname` AS `created_by_lastname`
 from ((((`char_users_roles`
 join `char_roles_permissions` on((`char_users_roles`.`role_id` = `char_roles_permissions`.`role_id`)))
 join `char_roles` on((`char_users_roles`.`role_id` = `char_roles`.`id`)))
 join `char_permissions` on((`char_roles_permissions`.`permission_id` = `char_permissions`.`id`)))
 left join `char_users` on((`char_roles_permissions`.`created_by` = `char_users`.`id`)))
 union
 all select `char_users_permissions`.`permission_id` AS `permission_id`,
           `char_permissions`.`name` AS `name`,`char_permissions`.`code` AS `code`,`char_permissions`.`module_id` AS `module_id`,
           `char_users_permissions`.`created_at` AS `created_at`,`char_users_permissions`.`user_id` AS `user_id`,
           `char_users`.`organization_id` AS `organization_id`,NULL AS `role_id`,NULL AS `role_name`,`char_users_permissions`.`allow` AS `allow`,`char_users_permissions`.`created_by` AS `created_by`,`char_users`.`firstname` AS `created_by_firstname`,`char_users`.`lastname` AS `created_by_lastname` from ((`char_users_permissions` join `char_permissions` on((`char_users_permissions`.`permission_id` = `char_permissions`.`id`))) left join `char_users` on((`char_users_permissions`.`created_by` = `char_users`.`id`))) ;




-- --------------------------------------------------------

CREATE TABLE `char_suggestions` (
    `id` int(11) NOT NULL,
    `date` date NOT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL,
    `subject` varchar(100) DEFAULT NULL,
    `details` longtext,
    `status_id` int(10) UNSIGNED NOT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `char_suggestions` CHANGE `created_at` `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP();

-- --------------------------------------------------------
 CREATE TABLE `char_suggestion_attachments` (
    `id` int(11) NOT NULL,
    `name` varchar(45) DEFAULT NULL,
    `file_id` int(10) UNSIGNED DEFAULT NULL,
    `suggestion_id` int(11) DEFAULT NULL,
    `date` date DEFAULT NULL,
    `created_at` timestamp NULL DEFAULT NULL,
    `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
 CREATE TABLE `char_suggestion_replays` (
    `id` int(11) NOT NULL,
    `date` date NOT NULL,
    `replay_user_id` int(10) UNSIGNED NOT NULL,
    `details` longtext,
    `suggestion_id` int(11) NOT NULL,
    `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



ALTER TABLE `char_suggestions`
ADD PRIMARY KEY (`id`),
    ADD KEY `char_suggestion$user_id_indx` (`user_id`);

ALTER TABLE `char_suggestion_attachments`
ADD PRIMARY KEY (`id`),
    ADD KEY `char_suggestion_attachments$file_id_indx` (`file_id`),
    ADD KEY `char_suggestion_attachments$suggestion_id_indx` (`suggestion_id`);


ALTER TABLE `char_suggestion_replays`
    ADD PRIMARY KEY (`id`),
    ADD KEY `char_suggestion_replays$suggestion_id_indx` (`suggestion_id`);

ALTER TABLE `char_suggestions`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `char_suggestion_attachments`  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `char_suggestion_replays` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `char_suggestions`
    ADD CONSTRAINT `char_suggestion$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `char_suggestion_attachments`
ADD CONSTRAINT `char_suggestion_attachments$file_id_fk` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `char_suggestion_attachments$suggestion_id_fk` FOREIGN KEY (`suggestion_id`) REFERENCES `char_suggestions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `char_suggestion_replays`
ADD CONSTRAINT `char_suggestion_replays$suggestion_id_fk` FOREIGN KEY (`suggestion_id`) REFERENCES `char_suggestions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `char_suggestion_replays$replay_user_id_fk` FOREIGN KEY (`replay_user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.voucherSearch', 'البحث في قسائم المساعدات');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.organization.orgSearch', 'البحث في بيانات الجمعيات');


DELIMITER $$
CREATE  FUNCTION `char_get_sponsorship_count`(`case_id` INT, `status` TINYINT , `organization_id` INT) RETURNS int(11)
BEGIN
  declare c int;
    set c =  (select count(`char_sponsorship_cases`.`id`)
              FROM `char_sponsorship_cases`
              join `char_sponsorships`   on `char_sponsorships` .`id` = `char_sponsorship_cases` .`sponsorship_id` and  `char_sponsorships` .`organization_id` = organization_id
              where `char_sponsorship_cases`.`case_id`=case_id AND
                    `char_sponsorship_cases`.`status`=status );

RETURN c;
END$$
DELIMITER ;

CREATE TABLE `char_archive_cheques` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
    `file_id` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `char_archive_cheques`
    ADD KEY `char_archive_cheques$file_id_indx` (`file_id`),
    ADD CONSTRAINT `char_archive_cheques$file_id_fk` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `char_payments`
 ADD `transfer` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`,
 ADD `transfer_company_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `char_payments`
    ADD KEY `fk_char_payments$transfer_company_id_idx` (`transfer_company_id`);

ALTER TABLE `char_payments`
 ADD CONSTRAINT `fk__char_payments$transfer_company_id` FOREIGN KEY (`transfer_company_id`) REFERENCES `char_transfer_company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



DELIMITER $$
CREATE FUNCTION `get_recipient_amount`(`recipient_id` INT, `payment_id` INT, `father_id` INT, `mother_id` INT, `type` TINYINT) RETURNS int(11)
BEGIN
declare c INTEGER;
      if type = 0 then
        		        set c = ( SELECT ifnull(sum(char_payments_cases.amount), 0)
                          from `char_payments_cases`
                          join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                          join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                          join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                          where (
                                     (
                                         (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                         or
                                         (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                     )
                                     and (
                                          `char_payments_cases`.`payment_id` = payment_id and
                                          `char_persons`.`father_id` = father_id and
                                          `char_persons`.`mother_id` = mother_id
                                         )
                                  )
                        );
         elseif type = 1 THEN
                    set c = ( SELECT ifnull(sum(char_payments_cases.amount_after_discount), 0)
                        from `char_payments_cases`
                        join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                        join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                        join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                        where (
                                   (
                                       (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                       or
                                       (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                   )
                                   and (
                                        `char_payments_cases`.`payment_id` = payment_id and
                                        `char_persons`.`father_id` = father_id and
                                        `char_persons`.`mother_id` = mother_id
                                       )
                                )
                      );
        elseif type = 2 THEN
                     set c = ( SELECT ifnull(sum(char_payments_cases.shekel_amount_before_discount), 0)
                        from `char_payments_cases`
                        join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                        join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                        join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                        where (
                                   (
                                       (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                       or
                                       (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                   )
                                   and (
                                        `char_payments_cases`.`payment_id` = payment_id and
                                        `char_persons`.`father_id` = father_id and
                                        `char_persons`.`mother_id` = mother_id
                                       )
                                )
                      );
               else
                     set c = ( SELECT ifnull(sum(char_payments_cases.shekel_amount), 0)
                                      from `char_payments_cases`
                                      join `char_payments` on `char_payments`.`id` = `char_payments_cases`.`payment_id`
                                      join `char_cases` on `char_cases`.`id` = `char_payments_cases`.`case_id`
                                      join `char_persons` on `char_persons`.`id` = `char_cases`.`person_id`
                                      where (
                                                 (
                                                     (`char_payments_cases`.`guardian_id` = recipient_id and `char_payments_cases`.`recipient` = 1)
                                                     or
                                                     (`char_cases`.`person_id`= recipient_id and `char_payments_cases`.`recipient` = 0)
                                                 )
                                                 and (
                                                      `char_payments_cases`.`payment_id` = payment_id and
                                                      `char_persons`.`father_id` = father_id and
                                                      `char_persons`.`mother_id` = mother_id
                                                     )
                                              )
                                    );
      end if;
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
DROP FUNCTION `char_get_payment_document_type_count` ;
CREATE  FUNCTION `char_get_payment_document_type_count`(`pId` INT, `type_` TINYINT) RETURNS int(11)
BEGIN
	declare c int;
    set c =  (select count(`char_payments_documents`.`document_id`) FROM `char_payments_documents`
              where `char_payments_documents`.`payment_id` = pId AND
                    `char_payments_documents`.`attachment_type` = type_
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
DROP FUNCTION `char_get_payment_document_type_id` ;
CREATE  FUNCTION `char_get_payment_document_type_id`(`pId` INT, `type_` TINYINT) RETURNS int(11)
BEGIN
	declare c int;
    set c =  (select `char_payments_documents`.`document_id` FROM `char_payments_documents`
              where `char_payments_documents`.`payment_id` = pId AND
                    `char_payments_documents`.`attachment_type`=type_
            );
RETURN c;
END$$
DELIMITER ;



ALTER TABLE `char_internal_message_receipt`
	ADD COLUMN `origin_msg_id` INT(11) NOT NULL AFTER `msg_id`;