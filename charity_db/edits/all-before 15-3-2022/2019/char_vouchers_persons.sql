

DROP INDEX fk_vouchersBeneficiaries_pId_idx ON char_vouchers_persons;
DROP INDEX fk_vouchersBeneficiaries_InId_idx ON char_vouchers_persons;

ALTER TABLE char_vouchers_persons DROP PRIMARY KEY;

ALTER TABLE `char_vouchers_persons`
  ADD `individual_id` int(10) UNSIGNED NULL,
  ADD KEY `fk_vouchersBeneficiaries_vId_idx` (`voucher_id`),
  ADD KEY `fk_vouchersBeneficiaries_pId_idx` (`person_id`),
  ADD KEY `fk_vouchersBeneficiaries_InId_idx` (`individual_id`);

ALTER TABLE `char_vouchers_persons`
  ADD CONSTRAINT `fk_vouchersBeneficiaries_vId` FOREIGN KEY (`voucher_id`) REFERENCES `char_vouchers` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vouchersBeneficiaries_pId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ;
--   ADD CONSTRAINT `fk_vouchersBeneficiaries_InId` FOREIGN KEY (`individual_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


ALTER TABLE `char_vouchers_persons`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `char_vouchers_persons`
  ADD `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
