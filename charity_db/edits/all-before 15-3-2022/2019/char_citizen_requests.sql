
CREATE TABLE `char_citizen_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `citizen_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `category` TINYINT NULL DEFAULT '0',
  `notes` text,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `reason` text,
  `user_id` int(10) UNSIGNED NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط المواطنين بسجل الشكاوي والاعتراضات وطلبات المساعدة المقدمة منهم';

-- --------------------------------------------------------

--
-- Table structure for table `char_citizens`
--

CREATE TABLE `char_citizens` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `third_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `prev_family_name` varchar(255) DEFAULT NULL,
  `id_card_number` varchar(9) DEFAULT NULL,
  `card_type` varchar(45) DEFAULT '1',
  `gender` tinyint(4) DEFAULT NULL,
  `deserted` tinyint(4) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL  ,
  `wataniya` varchar(45) DEFAULT NULL  ,
  `phone` varchar(45) DEFAULT NULL  ,
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `spouses` tinyint(1) DEFAULT NULL,
  `refugee` tinyint(4) DEFAULT NULL,
  `unrwa_card_number` varchar(45) DEFAULT NULL,
  `monthly_income` float DEFAULT NULL,
  `family_cnt` int(10) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `male_live` int(10) UNSIGNED DEFAULT NULL,
  `female_live` int(10) UNSIGNED DEFAULT NULL,
  `adscountry_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (الدولة)',
  `adsdistrict_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المحافظة)',
  `adsregion_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المنطقة)',
  `adsneighborhood_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (الحي)',
  `adssquare_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المربع)',
  `adsmosques_id` int(10) UNSIGNED DEFAULT NULL COMMENT'عنوان الشخص (المسجد)',
  `street_address` varchar(255) DEFAULT NULL COMMENT 'العنوان (الشارع)',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البيانات الشخصية لمقدمي التظلمات والطلبات';

--
-- Indexes for dumped tables
--
--
-- Indexes for table `char_citizen_requests`
--
ALTER TABLE `char_citizen_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_citizen_requests_personId_idx` (`citizen_id`),
  ADD KEY `fk_citizen_requests_userId_idx` (`user_id`);
--
-- Indexes for table `char_citizens`
--
ALTER TABLE `char_citizens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_card_number_UNIQUE` (`id_card_number`),
  ADD KEY `fk_citizens_marital_status_id_idx` (`marital_status_id`),
  ADD KEY `fk_citizens_adscountry_id_idx` (`adscountry_id`),
  ADD KEY `fk_citizens_adsdistrict_id_idx` (`adsdistrict_id`),
  ADD KEY `fk_citizens_adsregion_id_idx` (`adsregion_id`),
  ADD KEY `fk_citizens_adsneighborhood_id_idx` (`adsneighborhood_id`),
  ADD KEY `fk_citizens_adssquare_id_idx` (`adssquare_id`),
  ADD KEY `fk_citizens_adsmosques_id_idx` (`adsmosques_id`);

--
-- AUTO_INCREMENT for dumped tables
--
--
-- AUTO_INCREMENT for table `char_citizen_requests`
--

ALTER TABLE `char_citizen_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `char_citizens`
--
ALTER TABLE `char_citizens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- Constraints for table `char_citizen_requests`
--
ALTER TABLE `char_citizen_requests`
  ADD CONSTRAINT `fk_citizen_requests_personId` FOREIGN KEY (`citizen_id`) REFERENCES `char_citizens` (`id`),
  ADD CONSTRAINT `fk_citizen_requests_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_citizens`
--
ALTER TABLE `char_citizens`
  ADD CONSTRAINT `fk_citizens_adscountry_id` FOREIGN KEY (`adscountry_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_adsdistrict_id` FOREIGN KEY (`adsdistrict_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_adsmosques_id` FOREIGN KEY (`adsmosques_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_adsneighborhood_id` FOREIGN KEY (`adsneighborhood_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_adsregion_id` FOREIGN KEY (`adsregion_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_adssquare_id` FOREIGN KEY (`adssquare_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_citizens_marital_status_id` FOREIGN KEY (`marital_status_id`) REFERENCES `char_marital_status` (`id`);




CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_citizen_requests_view`  AS

select  char_citizens.first_name  AS  first_name , char_citizens.second_name  AS  second_name , char_citizens.third_name  AS  third_name ,
 char_citizens.last_name  AS  last_name , char_citizens.id_card_number  AS  id_card_number ,
concat(ifnull( char_citizens.first_name ,' '),' ',ifnull( char_citizens.second_name ,' '),' ',ifnull( char_citizens.third_name ,' '),
' ',ifnull( char_citizens.last_name ,' ')) AS  full_name , char_citizens.mobile  AS  mobile ,
 char_citizens.wataniya  AS  wataniya , char_citizens.phone  AS  phone , char_citizens.prev_family_name  AS  prev_family_name ,
 char_citizens.gender  AS  gender , char_citizens.marital_status_id  AS  marital_status_id ,
   ms.name AS  marital_status ,
 char_citizens.birthday  AS  birthday ,
 char_citizens.adscountry_id  AS  adscountry_id ,
 char_citizens.adsdistrict_id  AS  adsdistrict_id ,
  char_citizens.adsregion_id  AS  adsregion_id ,
 char_citizens.adsneighborhood_id  AS  adsneighborhood_id ,
  char_citizens.adssquare_id  AS  adssquare_id ,
 char_citizens.adsmosques_id  AS  adsmosques_id ,
 ifnull( country.name ,' ')   AS country_name,
 ifnull( district.name ,' ')   AS district_name,
 ifnull( region.name ,' ')   AS region_name,
 ifnull( location.name ,' ')  AS nearLocation_name,
 ifnull( square.name ,' ')    AS square_name,
 ifnull( mosques.name ,' ')   AS mosque_name,
 char_citizens.street_address  AS street_address,
  char_citizen_requests.*
from   char_citizen_requests
join  char_citizens  on  char_citizens.id  =  char_citizen_requests.citizen_id
left join  char_marital_status_i18n  as ms on  ms.marital_status_id  =  char_citizens.marital_status_id and  ms.language_id  = 1
left join char_aids_locations_i18n As country on char_citizens.adscountry_id = country.location_id and country.language_id = 1
left join char_aids_locations_i18n As district on char_citizens.adsdistrict_id = district.location_id and district.language_id = 1
left join char_aids_locations_i18n As region on char_citizens.adsregion_id = region.location_id  and region.language_id = 1
left join char_aids_locations_i18n As location  on char_citizens.adsneighborhood_id = location.location_id and location.language_id = 1
left join char_aids_locations_i18n As square on char_citizens.adssquare_id = square.location_id and square.language_id = 1
left join char_aids_locations_i18n As mosques on char_citizens.adsmosques_id = mosques.location_id and mosques.language_id = 1
 order by  char_citizen_requests.created_at  desc ;

