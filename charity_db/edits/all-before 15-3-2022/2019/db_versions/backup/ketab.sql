-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2017 at 05:12 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ketab`
--

DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `char_get_person_payments` (IN `id` INT(10), IN `org` INT(10))  BEGIN
    IF org = 1  then
        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id ;
      end if;

    if org <> 1  then

        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id  and L4.organization_id = org;
              end if;


END$$

CREATE  PROCEDURE `char_organizations_closure_init` ()  BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE v_id, v_parent_id INT;
  DECLARE cur1 CURSOR FOR SELECT `id`, `parent_id` FROM `char_organizations` ORDER BY `id`;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO v_id, v_parent_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    
    INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
		SELECT `c`.`ancestor_id`, v_id, `c`.`depth` + 1
		FROM `char_organizations_closure` AS `c`
		WHERE `c`.`descendant_id` = v_parent_id
		UNION ALL
		SELECT v_id, v_id, 0;
    
  END LOOP;

  CLOSE cur1;
END$$

CREATE  PROCEDURE `char_update_case_rank` (IN `p_id` INT, IN `p_context` VARCHAR(50))  BEGIN
	case p_context
		when 'death_cause' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `death_cause_id` = p_id
            );
        when 'marital_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `marital_status_id` = p_id
            );
        when 'disease' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_health` where `disease_id` = p_id
            );
		
        when 'property_type' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `property_type_id` = p_id
            );
        when 'roof_materials' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `roof_material_id` = p_id
            );
        when 'work_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_status_id` = p_id
            );
        when 'work_reason' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_reason_id` = p_id
            );
		when 'work_jobs' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_job_id` = p_id
            );
        when 'work_wage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_wage_id` = p_id
            );
		when 'education_authority' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `authority` = p_id
            );
		when 'education_degree' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `degree` = p_id
            );
		when 'education_stage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `stage` = p_id
            );
        else
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `id` = p_id;
	end case;
END$$

--
-- Functions
--
CREATE  FUNCTION `char_get_case_aid_sources` (`id` INT) RETURNS TEXT CHARSET latin1 BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_aid_sources`.`name` SEPARATOR ' - ' )
FROM `char_persons_aids`
join `char_aid_sources` ON `char_aid_sources`.`id` = `char_persons_aids`.`aid_source_id`
where `char_persons_aids`.`person_id`=id  and `char_persons_aids`.`aid_take` =1 
    );

RETURN c;
END$$

CREATE  FUNCTION `char_get_case_essentials_need` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
  declare c text CHARSET utf8; 
    set c = (select GROUP_CONCAT(DISTINCT `char_essentials`.`name` SEPARATOR ' - ' )
             FROM `char_cases_essentials`
             join `char_essentials` ON `char_essentials`.`id` = `char_cases_essentials`.`essential_id`
             where `char_cases_essentials`.`case_id`=id and
                  (`char_cases_essentials`.`needs` is not null ) and 
                  ( `char_cases_essentials`.`needs` != 0)
            );

RETURN c;
END$$

CREATE  FUNCTION `char_get_case_rank` (`c_id` INT) RETURNS FLOAT BEGIN
	DECLARE v_rank FLOAT;
    
	SET v_rank := (select (
      ifnull(`char_death_causes`.`weight`, 0) 
    + ifnull(`char_marital_status`.`weight`, 0) 
    + ifnull(`char_diseases`.`weight`, 0) 
    + ifnull(`char_roof_materials`.`weight`, 0) 
    + ifnull(`char_property_types`.`weight`, 0)
    + ifnull(`char_work_status`.`weight`, 0) 
    + ifnull(`char_work_reasons`.`weight`, 0) 
    + ifnull(`char_work_jobs`.`weight`, 0) 
    + ifnull(`char_work_wages`.`weight`, 0)
    + sum(ifnull(`char_aid_sources_weights`.`weight`, 0))
    + sum(ifnull(`char_essentials_weights`.`weight`, 0))
    + sum(ifnull(`char_properties_weights`.`weight`, 0))) AS rank
	from `char_cases`
	inner join  `char_persons` on `char_cases`.`person_id` = `char_persons`.`id`

	left join `char_death_causes` on `char_persons`.`death_cause_id` = `char_death_causes`.`id`
	left join `char_marital_status` on `char_persons`.`marital_status_id` = `char_marital_status`.`id`

	left join `char_persons_health` on `char_cases`.`person_id` = `char_persons_health`.`person_id`
	left join `char_diseases` on `char_persons_health`.`disease_id` = `char_diseases`.`weight`
    
    left join `char_residence` on `char_cases`.`person_id` = `char_residence`.`person_id`
    left join `char_property_types` on `char_residence`.`property_type_id` = `char_property_types`.`id`
    left join `char_roof_materials` on `char_residence`.`roof_material_id` = `char_roof_materials`.`id`
    
    left join `char_persons_properties` on `char_cases`.`person_id` = `char_persons_properties`.`person_id`
    left join `char_properties_weights` on `char_persons_properties`.`property_id` = `char_properties_weights`.`property_id`
    AND `char_persons_properties`.`quantity` BETWEEN `char_properties_weights`.`value_min` AND `char_properties_weights`.`value_max`

	left join `char_persons_work` on `char_cases`.`person_id` = `char_persons_work`.`person_id`
	left join `char_work_status` on `char_persons_work`.`work_status_id` = `char_work_status`.`id`
	left join `char_work_reasons` on `char_persons_work`.`work_reason_id` = `char_work_reasons`.`id`
	left join `char_work_jobs` on `char_persons_work`.`work_job_id` = `char_work_jobs`.`id`
	left join `char_work_wages` on `char_persons_work`.`work_wage_id` = `char_work_wages`.`id`
    
    left join `char_persons_aids` on `char_cases`.`person_id` = `char_persons_aids`.`person_id`
    left join `char_aid_sources_weights` on `char_persons_aids`.`aid_source_id` = `char_aid_sources_weights`.`aid_source_id`
    AND `char_persons_aids`.`aid_value` BETWEEN `char_aid_sources_weights`.`value_min` AND `char_aid_sources_weights`.`value_max`
    
    left join `char_cases_essentials` on `char_cases`.`id` = `char_cases_essentials`.`case_id`
    left join `char_essentials_weights` on `char_cases_essentials`.`essential_id` = `char_essentials_weights`.`essential_id`
    AND `char_cases_essentials`.`condition` = `char_essentials_weights`.`value`
    WHERE `char_cases`.`id` = c_id);
RETURN v_rank;
END$$

CREATE  FUNCTION `char_get_children_count` (`person_id` INT(10), `gender` TINYINT(4)) RETURNS INT(11) BEGIN
declare c INTEGER;
    
   	if gender is null then
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE `a`.`father_id` = person_id  or `a`.`mother_id` = person_id
					);
	       
	       else
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE (`a`.`father_id` = person_id  or `a`.`mother_id` = person_id) and `a`.`gender`= gender
					);
	       				
	       end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_family_count` (`p_via` VARCHAR(15), `master_id` INT(10), `gender` TINYINT(4), `edge_id` INT(10)) RETURNS INT(11) NO SQL
BEGIN
declare c INTEGER;
    
      if p_via = 'left' then
	if gender is null then
				set c = (
						  SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
					      WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
		                );
	       
	       else
				set c = (
						 SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
			  			 JOIN  `char_persons` ON ((`char_persons`.`id`=`k`.`r_person_id`) and (`char_persons`.`gender`= gender))
						 WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id 
	                    );
	   
	       end if;
    elseif p_via = 'right' then
		if gender is null then
				set c = (
  						   SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			      		   WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id 
			                 );
	       
	       else
				set c = (
						  SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			    		 JOIN  `char_persons` ON ( (`char_persons`.`id`=`k`.`l_person_id`) and  (`char_persons`.`gender`= gender ))
						 WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id 
	                    );
	        end if;
   
    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_family_total_category_payments` (`father` INT(10), `p_date_from` DATE, `p_date_to` DATE, `category` INT(10)) RETURNS DOUBLE BEGIN
  declare sum double;
  IF category is null THEN
 
   if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))  
                               AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     end if;
 
     ELSE

     if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))  
                                AND   ((`char_cases`.`category_id` = category ))
                                AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
 


        end if;

     END IF;
RETURN sum;
END$$

CREATE  FUNCTION `char_get_family_total_payments` (`l_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
      set sum = (   SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                    FROM  `char_payments_cases`  AS `p`
                    join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                    join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                    join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                    WHERE ((`p`.`status` = 1) OR (`p`.`status` = 2))
                );
    elseif p_date_from is not null and p_date_to is null then
     set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)))
              );

    elseif p_date_from is null and p_date_to is not null then
     set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to)))
              );


    elseif p_date_from is not null and p_date_to is not null then
        set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                        )
              );
    end if;

RETURN sum;
END$$

CREATE  FUNCTION `CHAR_GET_FORM_ELEMENT_PRIORITY` (`p_category_id` INT, `p_element_name` VARCHAR(255)) RETURNS INT(11) BEGIN

                DECLARE v_priority INT;

   

    SET v_priority = (SELECT `e`.`priority` 
                        FROM `char_categories_form_element_priority` `e`
                        JOIN `char_forms_elements` AS `el` on `el`.`id` =`e`.`element_id` and `el`.`name` = p_element_name 
						where`e`.`category_id` = p_category_id);

RETURN v_priority;

END$$

CREATE  FUNCTION `char_get_guardian_persons_count` (`p_guardian_id` INT) RETURNS INT(11) BEGIN
DECLARE c INTEGER;

set c = (SELECT COUNT(`individual_id`) FROM `char_guardians` WHERE `guardian_id` = p_guardian_id);

RETURN c;
END$$

CREATE  FUNCTION `char_get_persons_properties` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
  declare c text CHARSET utf8; 
    set c = (select GROUP_CONCAT(DISTINCT `char_properties_i18n`.`name` SEPARATOR ' - ' )
             FROM `char_persons_properties`
             join `char_properties_i18n` ON `char_properties_i18n`.`property_id` = `char_persons_properties`.`property_id` and    `char_properties_i18n`.`language_id` =1
             where `char_persons_properties`.`person_id`=id 
            );

RETURN c;
END$$

CREATE  FUNCTION `char_get_person_average_payments` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare average double;

    if p_date_from is null and p_date_to is null then
    set average = (SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                       (`char_cases`.`person_id` = p_person_id) and
                       ( (`p`.`status` = 1) or
                           (`p`.`status` = 2)
                       ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and  ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                    )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and
                        ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and 
                        ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    end if;

RETURN average;
END$$

CREATE  FUNCTION `char_get_person_total_payments` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
    set sum = (SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or
                                 (`p`.`status` = 2)
                              ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE ( 
                              (`char_cases`.`person_id` = p_person_id) and
                              ((`p`.`status` = 1) or (`p`.`status` = 2)) and
                              ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                              ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    end if;

RETURN sum;
END$$

CREATE  FUNCTION `char_get_siblings_count` (`p_person_id` INT, `p_via` VARCHAR(15), `gender` TINYINT(4)) RETURNS INT(11) BEGIN
	declare c INTEGER;
    
      if p_via = 'mother' then

      	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`mother_id` = (
								select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	       
	       else
	       	set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`mother_id` = (
							select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id	
					);

	       				
	       end if;

	
    elseif p_via = 'father' then

     	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`father_id` = (
								select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	    else
			set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`father_id` = (
							select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);
        				
	    end if;


    else

     	if gender is null then

		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id
			)) and `a`.`id` !=  p_person_id
		);
	    else


		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id and `b`.`gender`= gender
			)) and `a`.`id` !=  p_person_id
		);
        				
	    end if;
    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_voucher_persons_count` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS INT(11) BEGIN
	DECLARE c INTEGER;

 if p_date_from is null and p_date_to is null then
		set c = (
              SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );

    elseif p_date_from is not null and p_date_to is null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );


    elseif p_date_from is null and p_date_to is not null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );


    elseif p_date_from is not null and p_date_to is not null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_voucher_amount` (`id` INT(1)) RETURNS INT(11) BEGIN
declare c INTEGER;  
declare d INTEGER;  

		set d = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons` from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));      
                 
		set c = (select (`char_vouchers`.`value` * d) AS `amount` from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$

CREATE  FUNCTION `get_case_documents` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_cases_files`
          where `case_id`=id  
		);

RETURN c;
END$$

CREATE  FUNCTION `get_document` (`id` INT) RETURNS TEXT CHARSET latin1 BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_persons_documents`
          where `person_id`=id  
		);

RETURN c;
END$$

CREATE  FUNCTION `get_organization_payments_cases_amount` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;  
end if;


RETURN c;
END$$

CREATE  FUNCTION `get_organization_payments_cases_count` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;  
end if;


RETURN c;
END$$

CREATE  FUNCTION `get_organization_vouchers_value` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then
          
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null) != 0)));
           
               end if;
else
   if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
           
               end if;

   end if;
else

    if type = 'sponsors' then
          
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id) != 0)));
           
               end if;
else

  if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
           
               end if;

    end if;



end if;


RETURN c;
END$$

CREATE  FUNCTION `get_payments_beneficiary_count` (`id` INT(10), `c_via` VARCHAR(45)) RETURNS INT(11) BEGIN
declare c INTEGER;
    
      if c_via = 'all' then
		      set c = (	select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( `p`.`payment_id`  =  id )	);
      elseif c_via = 'guaranteed' then
			    set c = (select COUNT(`p`.`sponsor_number`) 
FROM `char_payments_cases` AS `p` 
WHERE ( (`p`.`payment_id` = id)  and ( (`p`.`status` = 1) or (`p`.`status` = 2)) ));
      else
			    set c = (select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( (`p`.`payment_id` = id) and (`p`.`status` = 3)	));
      end if;

RETURN c;
END$$

CREATE  FUNCTION `get_payments_cases_count` (`id` INT(10), `payment_id` INT(10), `sponsor_number` VARCHAR(45)) RETURNS INT(11) BEGIN
declare c INTEGER;
  
		set c = (
			select COUNT(`p`.`payment_id`) FROM `char_payments_cases` AS `p`
			WHERE (`p`.`sponsor_number` = sponsor_number) and 
             (`p`.`case_id` = case_id) and (`p`.`payment_id` !=  payment_id)
		);
  
RETURN c;
END$$

CREATE  FUNCTION `get_payments_recipient_amount` (`id` INT(10), `p_via` VARCHAR(45), `payment_id` INT(10)) RETURNS INT(11) BEGIN
declare c INTEGER;

        if p_via = 'person' then
		set c = (             
            select sum(`p`.`amount`) 
FROM `char_payments_cases` AS `p`
join `char_cases` ON `char_cases`.`id` =`p`.`case_id`
join `char_persons` ON `char_persons`.`id` =`char_cases`.`person_id`
WHERE (`char_persons`.`id` = id ) and (`p`.`payment_id` =  payment_id) and (`p`.`status` !=  3)
);
  
    else
		set c = (  
                   select sum(`p`.`amount`) 
                   FROM `char_payments_cases` AS `p`
                   WHERE (`p`.`guardian_id` = id) and (`p`.`payment_id` =  payment_id)
                );
    end if;

  
RETURN c;
END$$

CREATE  FUNCTION `get_voucher_count` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
   
 if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)));

     elseif date_from is not null and date_to is null then
        
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from )));
    
     elseif date_from is null and date_to is not null then
   
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date`  <= date_to )));
   
     elseif date_from is not null and date_to is not null then
  
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
   
     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
  
     end if;

     end if;

else


    if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`category_id` = category_id)));

     elseif date_from is not null and date_to is null then
        
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
    
     elseif date_from is null and date_to is not null then
   
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
   
     elseif date_from is not null and date_to is not null then
  
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
   
     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`category_id` = category_id) ));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
  
     end if;

     end if;



end if;


RETURN c;
END$$

CREATE  FUNCTION `normalize_text_ar` (`p_text` TEXT) RETURNS TEXT CHARSET utf8 BEGIN
	
	DECLARE v_comma VARCHAR(255) DEFAULT '،';
	DECLARE v_semicolon VARCHAR(255) DEFAULT '؛';
	DECLARE v_question VARCHAR(255) DEFAULT '؟';
	DECLARE v_hamza VARCHAR(255) DEFAULT 'ء';
	DECLARE v_alef_madda VARCHAR(255) DEFAULT 'آ';
	DECLARE v_alef_hamza_above VARCHAR(255) DEFAULT 'أ';
	DECLARE v_waw_hamza VARCHAR(255) DEFAULT 'ؤ';
	DECLARE v_alef_hamza_below VARCHAR(255) DEFAULT 'إ';
	DECLARE v_yeh_hamza VARCHAR(255) DEFAULT 'ئ';
	DECLARE v_alef VARCHAR(255) DEFAULT 'ا';
	DECLARE v_beh VARCHAR(255) DEFAULT 'ب';
	DECLARE v_teh_marbuta VARCHAR(255) DEFAULT 'ة';
	DECLARE v_teh VARCHAR(255) DEFAULT 'ت';
	DECLARE v_theh VARCHAR(255) DEFAULT 'ث';
	DECLARE v_jeem VARCHAR(255) DEFAULT 'ج';
	DECLARE v_hah VARCHAR(255) DEFAULT 'ح';
	DECLARE v_khah VARCHAR(255) DEFAULT 'خ';
	DECLARE v_dal VARCHAR(255) DEFAULT 'د';
	DECLARE v_thal VARCHAR(255) DEFAULT 'ذ';
	DECLARE v_reh VARCHAR(255) DEFAULT 'ر';
	DECLARE v_zain VARCHAR(255) DEFAULT 'ز';
	DECLARE v_seen VARCHAR(255) DEFAULT 'س';
	DECLARE v_sheen VARCHAR(255) DEFAULT 'ش';
	DECLARE v_sad VARCHAR(255) DEFAULT 'ص';
	DECLARE v_dad VARCHAR(255) DEFAULT 'ض';
	DECLARE v_tah VARCHAR(255) DEFAULT 'ط';
	DECLARE v_zah VARCHAR(255) DEFAULT 'ظ';
	DECLARE v_ain VARCHAR(255) DEFAULT 'ع';
	DECLARE v_ghain VARCHAR(255) DEFAULT 'غ';
	DECLARE v_tatweel VARCHAR(255) DEFAULT 'ـ';
	DECLARE v_feh VARCHAR(255) DEFAULT 'ف';
	DECLARE v_qaf VARCHAR(255) DEFAULT 'ق';
	DECLARE v_kaf VARCHAR(255) DEFAULT 'ك';
	DECLARE v_lam VARCHAR(255) DEFAULT 'ل';
	DECLARE v_meem VARCHAR(255) DEFAULT 'م';
	DECLARE v_noon VARCHAR(255) DEFAULT 'ن';
	DECLARE v_heh VARCHAR(255) DEFAULT 'ه';
	DECLARE v_waw VARCHAR(255) DEFAULT 'و';
	DECLARE v_alef_maksura VARCHAR(255) DEFAULT 'ى';
	DECLARE v_yeh VARCHAR(255) DEFAULT 'ي';
	DECLARE v_madda_above VARCHAR(255) DEFAULT 'ٓ';
	DECLARE v_hamza_above VARCHAR(255) DEFAULT 'ٔ';
	DECLARE v_hamza_below VARCHAR(255) DEFAULT 'ٕ';
	DECLARE v_zero VARCHAR(255) DEFAULT '٠';
	DECLARE v_one VARCHAR(255) DEFAULT '١';
	DECLARE v_two VARCHAR(255) DEFAULT '٢';
	DECLARE v_three VARCHAR(255) DEFAULT '٣';
	DECLARE v_four VARCHAR(255) DEFAULT '٤';
	DECLARE v_five VARCHAR(255) DEFAULT '٥';
	DECLARE v_six VARCHAR(255) DEFAULT '٦';
	DECLARE v_seven VARCHAR(255) DEFAULT '٧';
	DECLARE v_eight VARCHAR(255) DEFAULT '٨';
	DECLARE v_nine VARCHAR(255) DEFAULT '٩';
	DECLARE v_percent VARCHAR(255) DEFAULT '٪';
	DECLARE v_decimal VARCHAR(255) DEFAULT '٫';
	DECLARE v_thousands VARCHAR(255) DEFAULT '٬';
	DECLARE v_star VARCHAR(255) DEFAULT '٭';
	DECLARE v_mini_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_alef_wasla VARCHAR(255) DEFAULT 'ٱ';
	DECLARE v_full_stop VARCHAR(255) DEFAULT '۔';
	DECLARE v_byte_order_mark VARCHAR(255) DEFAULT '﻿';
	DECLARE v_fathatan VARCHAR(255) DEFAULT 'ً';
	DECLARE v_dammatan VARCHAR(255) DEFAULT 'ٌ';
	DECLARE v_kasratan VARCHAR(255) DEFAULT 'ٍ';
	DECLARE v_fatha VARCHAR(255) DEFAULT 'َ';
	DECLARE v_damma VARCHAR(255) DEFAULT 'ُ';
	DECLARE v_kasra VARCHAR(255) DEFAULT 'ِ';
	DECLARE v_shadda VARCHAR(255) DEFAULT 'ّ';
	DECLARE v_sukun VARCHAR(255) DEFAULT 'ْ';
	DECLARE v_small_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_small_waw VARCHAR(255) DEFAULT 'ۥ';
	DECLARE v_small_yeh VARCHAR(255) DEFAULT 'ۦ';
	DECLARE v_lam_alef VARCHAR(255) DEFAULT 'ﻻ';
	DECLARE v_lam_alef_hamza_above VARCHAR(255) DEFAULT 'ﻷ';
	DECLARE v_lam_alef_hamza_below VARCHAR(255) DEFAULT 'ﻹ';
	DECLARE v_lam_alef_madda_above VARCHAR(255) DEFAULT 'ﻵ';
	DECLARE v_simple_lam_alef VARCHAR(255) DEFAULT 'لَا';
	DECLARE v_simple_lam_alef_hamza_above VARCHAR(255) DEFAULT 'لأ';
	DECLARE v_simple_lam_alef_hamza_below VARCHAR(255) DEFAULT 'لإ';
	DECLARE v_simple_lam_alef_madda_above VARCHAR(255) DEFAULT 'لءَا';
    
        SET p_text = REPLACE(p_text, v_tatweel, '');
    
        SET p_text = REPLACE(p_text, v_fathatan, '');
    SET p_text = REPLACE(p_text, v_dammatan, '');
    SET p_text = REPLACE(p_text, v_kasratan, '');
    SET p_text = REPLACE(p_text, v_fatha, '');
    SET p_text = REPLACE(p_text, v_damma, '');
    SET p_text = REPLACE(p_text, v_kasra, '');
    SET p_text = REPLACE(p_text, v_shadda, '');
    SET p_text = REPLACE(p_text, v_sukun, '');
    
        SET p_text = REPLACE(p_text, v_waw_hamza, v_waw);
    SET p_text = REPLACE(p_text, v_yeh_hamza, v_yeh);
    SET p_text = REPLACE(p_text, v_alef_madda, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_below, v_alef);
    
        SET p_text = REPLACE(p_text, v_lam_alef, v_simple_lam_alef);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_above, v_simple_lam_alef_hamza_above);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_below, v_simple_lam_alef_hamza_below);
    SET p_text = REPLACE(p_text, v_lam_alef_madda_above, v_simple_lam_alef_madda_above);
    
        SET p_text = REPLACE(p_text, v_teh_marbuta, v_heh);
    
RETURN p_text;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `aid_categories`
--

CREATE TABLE `aid_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_projects`
--

CREATE TABLE `aid_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `repository_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sponsor_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `organization_ratio` float UNSIGNED NOT NULL,
  `amount` float UNSIGNED NOT NULL,
  `quantity` float UNSIGNED NOT NULL,
  `notes` text,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_projects_organizations`
--

CREATE TABLE `aid_projects_organizations` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `ratio` float UNSIGNED DEFAULT NULL,
  `amount` float UNSIGNED DEFAULT NULL,
  `quantity` float UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_projects_organizations_meta`
--

CREATE TABLE `aid_projects_organizations_meta` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `meta_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_projects_persons`
--

CREATE TABLE `aid_projects_persons` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `nominated_organization_id` int(10) UNSIGNED NOT NULL,
  `reason` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_projects_rules`
--

CREATE TABLE `aid_projects_rules` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `rule_name` varchar(255) NOT NULL,
  `rule_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_repositories`
--

CREATE TABLE `aid_repositories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aid_repositories_meta`
--

CREATE TABLE `aid_repositories_meta` (
  `id` int(10) UNSIGNED NOT NULL,
  `repository_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_logs`
--

CREATE TABLE `app_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `action` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `message` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `app_modules`
--

CREATE TABLE `app_modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(45) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `app_modules`
--

INSERT INTO `app_modules` (`id`, `code`, `name`, `description`, `status`) VALUES
(1, 'auth', 'المستخدمين والصلاحيات', '', 1),
(2, 'doc', 'الملفات والأرشفة', '', 0),
(3, 'aid', 'المساعدات', '', 0),
(4, 'sponsorship', 'الكفالات', '', 0),
(5, 'sms', 'الرسائل القصيرة', '', 0),
(6, 'form', 'النماذج', '', 0),
(7, 'app', 'النظام', '', 0),
(8, 'organization', 'الجمعيات والمؤسسات', NULL, NULL),
(9, 'aid-repository', 'إدارة وعاء المساعدات', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `app_routes`
--

CREATE TABLE `app_routes` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `auth_users_permissions_view`
--
CREATE TABLE `auth_users_permissions_view` (
`permission_id` int(11) unsigned
,`name` varchar(255)
,`code` varchar(255)
,`module_id` int(11) unsigned
,`created_at` datetime
,`user_id` int(11) unsigned
,`role_id` int(10) unsigned
,`role_name` varchar(255)
,`allow` tinyint(4) unsigned
,`created_by` int(11) unsigned
,`created_by_firstname` varchar(45)
,`created_by_lastname` varchar(45)
);

-- --------------------------------------------------------

--
-- Table structure for table `char_activities`
--

CREATE TABLE `char_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `template` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_activities_log`
--

CREATE TABLE `char_activities_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `activity_id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على سجلات للعمليات التي تحدث ومن قام بالعملية ووقتها';

-- --------------------------------------------------------

--
-- Table structure for table `char_aid_cases_incomplete_data`
--

CREATE TABLE `char_aid_cases_incomplete_data` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `person_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_name` varchar(255) DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `notes` text,
  `visited_at` date DEFAULT NULL,
  `visitor` varchar(255) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `rank` float DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `case_created_at` datetime DEFAULT NULL,
  `case_updated_at` datetime DEFAULT NULL,
  `case_deleted_at` datetime DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `third_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `id_card_number` varchar(9) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(45) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `death_date` date DEFAULT NULL,
  `death_cause_id` int(10) UNSIGNED DEFAULT NULL,
  `father_id` int(11) UNSIGNED DEFAULT NULL,
  `mother_id` int(11) UNSIGNED DEFAULT NULL,
  `location_id` int(10) UNSIGNED DEFAULT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `refugee` tinyint(4) DEFAULT NULL,
  `unrwa_card_number` varchar(45) DEFAULT NULL,
  `monthly_income` float DEFAULT NULL,
  `spouses` tinyint(1) DEFAULT NULL,
  `person_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `person_updated_at` datetime DEFAULT NULL,
  `person_deleted_at` datetime DEFAULT NULL,
  `name` varchar(183) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_aid_sources`
--

CREATE TABLE `char_aid_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الجهات الخارجية المقدمة للمساعدات';

-- --------------------------------------------------------

--
-- Table structure for table `char_aid_sources_weights`
--

CREATE TABLE `char_aid_sources_weights` (
  `id` int(10) UNSIGNED NOT NULL,
  `aid_source_id` int(10) UNSIGNED NOT NULL,
  `value_min` float UNSIGNED NOT NULL,
  `value_max` float UNSIGNED DEFAULT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_banks`
--

CREATE TABLE `char_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `template` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_block_categories`
--

CREATE TABLE `char_block_categories` (
  `block_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_block_id_card_number`
--

CREATE TABLE `char_block_id_card_number` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_card_number` varchar(9) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_branches`
--

CREATE TABLE `char_branches` (
  `id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_building_status`
--

CREATE TABLE `char_building_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_building_status_i18n`
--

CREATE TABLE `char_building_status_i18n` (
  `building_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_cases`
--

CREATE TABLE `char_cases` (
  `id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `notes` text,
  `visited_at` date DEFAULT NULL,
  `visitor` varchar(255) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `rank` float NOT NULL DEFAULT '0',
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط المستفيدين (الأشخاص) مع الجمعية وتحديد نوع الاستفادة (كفالات ومساعدات)';

-- --------------------------------------------------------

--
-- Table structure for table `char_cases_essentials`
--

CREATE TABLE `char_cases_essentials` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `essential_id` int(10) UNSIGNED NOT NULL,
  `condition` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'الحالة: 5 ممتاز، 4 جيد جدا، 3 جيد، 2 سيء، 0 لا يوجد',
  `needs` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'عدد الاحتياج المطلوب'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول ربط الحالات (المستفيدين) بحالة أثاث وضروريات المنزل';

-- --------------------------------------------------------

--
-- Table structure for table `char_cases_files`
--

CREATE TABLE `char_cases_files` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `document_type_id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_cases_status_log`
--

CREATE TABLE `char_cases_status_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `case_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_case_needs`
--

CREATE TABLE `char_case_needs` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `needs` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_categories`
--

CREATE TABLE `char_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1-sponsorship 2-aid',
  `case_is` tinyint(1) DEFAULT NULL COMMENT '1- الحالة نفسها 2- المعيل',
  `family_structure` tinyint(1) DEFAULT NULL COMMENT '1-غير مصنفين 2- مصنفين (مرشحين -غير مرشحين)',
  `has_mother` tinyint(1) DEFAULT NULL COMMENT '0 - لا يشترط  1- يشترط ',
  `father_dead` tinyint(1) DEFAULT NULL COMMENT '0- على قيد الحياة 1 - نعم متوفي',
  `has_Wives` tinyint(1) DEFAULT NULL COMMENT '0 - لايمتلك اكثر من زوجة 1- يمتلك'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول أنواع/تصنيف الكفالات والمساعدات';

--
-- Dumping data for table `char_categories`
--

INSERT INTO `char_categories` (`id`, `name`, `type`, `case_is`, `family_structure`, `has_mother`, `father_dead`, `has_Wives`) VALUES
(1, 'كفالة الأسر الفقيرة', 1, 2, 1, 0, 0, 1),
(2, 'كفالة المرضى', 1, 2, 1, 1, 0, 1),
(3, 'كفالة طالب علم', 1, 1, 1, 1, 0, 1),
(4, 'كفالة يتيم', 1, 1, 2, 1, 1, 0),
(5, 'مساعدة أسرة فقيرة', 2, NULL, NULL, NULL, NULL, NULL),
(6, 'مساعدة طالب علم', 2, NULL, NULL, NULL, NULL, NULL),
(7, 'مساعدة مرضى	', 2, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_categories_document_types`
--

CREATE TABLE `char_categories_document_types` (
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `document_type_id` int(10) UNSIGNED NOT NULL,
  `scope` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_categories_document_types`
--

INSERT INTO `char_categories_document_types` (`category_id`, `document_type_id`, `scope`) VALUES
(1, 10, 1),
(1, 17, 1),
(1, 19, 4),
(1, 21, 1),
(1, 22, 3),
(2, 10, 2),
(2, 17, 1),
(2, 18, 2),
(2, 19, 4),
(2, 20, 3),
(2, 21, 1),
(3, 15, 3),
(3, 16, 3),
(3, 20, 3),
(3, 22, 3),
(4, 4, 4),
(4, 10, 2),
(4, 16, 3),
(4, 17, 1),
(4, 18, 2),
(4, 19, 4),
(4, 20, 3),
(5, 16, 0),
(5, 20, 0),
(5, 21, 0),
(6, 15, 3),
(6, 16, 3),
(6, 20, 3),
(6, 22, 3);

-- --------------------------------------------------------

--
-- Table structure for table `char_categories_forms_sections`
--

CREATE TABLE `char_categories_forms_sections` (
  `id` varchar(255) NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول أقسام النموذج تبعا لنوع المساعدة أو الكفالة';

--
-- Dumping data for table `char_categories_forms_sections`
--

INSERT INTO `char_categories_forms_sections` (`id`, `category_id`, `status`) VALUES
('CaseData', 3, 1),
('CaseDocumentData', 5, 1),
('CaseDocumentData', 6, 1),
('CaseDocumentData', 7, 1),
('caseDocuments', 2, 1),
('caseDocuments', 3, 1),
('customFormData', 7, 1),
('FamilyData', 1, 1),
('FamilyData', 2, 1),
('FamilyData', 3, 1),
('FamilyData', 4, 1),
('familyDocuments', 1, 1),
('familyDocuments', 2, 1),
('familyDocuments', 4, 1),
('familyStructure', 5, 1),
('familyStructure', 6, 1),
('familyStructure', 7, 1),
('homeIndoorData', 5, 1),
('homeIndoorData', 6, 1),
('homeIndoorData', 7, 1),
('otherData', 5, 1),
('otherData', 6, 1),
('otherData', 7, 1),
('ParentData', 1, 1),
('ParentData', 2, 1),
('ParentData', 4, 1),
('personalInfo', 5, 1),
('personalInfo', 6, 1),
('personalInfo', 7, 1),
('providerData', 1, 1),
('providerData', 2, 1),
('providerData', 4, 1),
('recommendations', 5, 1),
('recommendations', 6, 1),
('recommendations', 7, 1),
('residenceData', 5, 1),
('residenceData', 6, 1),
('residenceData', 7, 1),
('vouchersData', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `char_categories_form_element_priority`
--

CREATE TABLE `char_categories_form_element_priority` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `element_id` int(10) UNSIGNED NOT NULL,
  `priority` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_categories_form_element_priority`
--

INSERT INTO `char_categories_form_element_priority` (`category_id`, `element_id`, `priority`) VALUES
(1, 23, 2),
(1, 56, 4),
(1, 58, 2),
(1, 59, 2),
(1, 60, 2),
(1, 61, 2),
(1, 62, 2),
(1, 63, 2),
(1, 64, 2),
(1, 65, 2),
(1, 66, 2),
(1, 67, 2),
(1, 68, 2),
(1, 69, 4),
(1, 70, 4),
(1, 71, 2),
(1, 72, 2),
(1, 73, 4),
(1, 74, 4),
(1, 75, 4),
(1, 76, 4),
(1, 77, 4),
(1, 78, 4),
(1, 79, 4),
(1, 80, 4),
(1, 81, 2),
(1, 82, 4),
(1, 83, 4),
(1, 84, 4),
(1, 85, 4),
(1, 86, 4),
(1, 87, 4),
(1, 88, 4),
(1, 89, 4),
(1, 90, 4),
(1, 91, 4),
(1, 92, 4),
(1, 93, 4),
(1, 94, 4),
(1, 95, 4),
(1, 96, 4),
(1, 97, 4),
(1, 101, 4),
(1, 102, 4),
(1, 103, 4),
(1, 104, 4),
(1, 105, 4),
(1, 106, 4),
(1, 107, 4),
(1, 108, 4),
(1, 109, 4),
(1, 110, 4),
(1, 111, 4),
(1, 112, 4),
(1, 113, 4),
(1, 114, 4),
(1, 115, 4),
(1, 116, 4),
(1, 117, 4),
(1, 118, 4),
(1, 119, 4),
(1, 120, 4),
(1, 121, 4),
(1, 122, 4),
(1, 123, 4),
(1, 124, 4),
(1, 125, 4),
(1, 126, 4),
(1, 127, 4),
(1, 128, 4),
(2, 23, 4),
(2, 58, 4),
(2, 59, 4),
(2, 60, 4),
(2, 61, 4),
(2, 62, 4),
(2, 63, 4),
(2, 64, 1),
(2, 65, 4),
(2, 66, 4),
(2, 67, 1),
(2, 68, 4),
(2, 69, 4),
(2, 70, 4),
(2, 71, 4),
(2, 72, 4),
(2, 73, 4),
(2, 74, 4),
(2, 75, 4),
(2, 76, 4),
(2, 77, 4),
(2, 78, 4),
(2, 79, 4),
(2, 80, 4),
(2, 81, 4),
(2, 82, 4),
(2, 83, 4),
(2, 84, 4),
(2, 85, 4),
(2, 86, 4),
(2, 87, 4),
(2, 88, 4),
(2, 89, 4),
(2, 90, 4),
(2, 91, 4),
(2, 92, 4),
(2, 93, 4),
(2, 94, 4),
(2, 95, 4),
(2, 96, 4),
(2, 97, 4),
(2, 101, 4),
(2, 102, 4),
(2, 103, 4),
(2, 104, 4),
(2, 105, 4),
(2, 106, 4),
(2, 107, 4),
(2, 108, 4),
(2, 109, 4),
(2, 110, 4),
(2, 111, 4),
(2, 112, 4),
(2, 113, 4),
(2, 114, 4),
(2, 115, 4),
(2, 116, 4),
(2, 117, 4),
(2, 118, 4),
(2, 119, 4),
(2, 120, 4),
(2, 121, 4),
(2, 122, 4),
(2, 123, 4),
(2, 124, 4),
(2, 125, 4),
(2, 126, 4),
(2, 127, 4),
(2, 128, 4),
(3, 23, 4),
(3, 58, 4),
(3, 59, 4),
(3, 60, 4),
(3, 61, 2),
(3, 62, 2),
(3, 63, 2),
(3, 64, 2),
(3, 65, 4),
(3, 66, 3),
(3, 67, 2),
(3, 68, 2),
(3, 69, 4),
(3, 70, 4),
(3, 71, 4),
(3, 72, 4),
(3, 73, 4),
(3, 74, 4),
(3, 75, 4),
(3, 76, 4),
(3, 77, 4),
(3, 78, 4),
(3, 79, 4),
(3, 80, 4),
(3, 81, 4),
(3, 82, 4),
(3, 83, 4),
(3, 84, 4),
(3, 85, 4),
(3, 86, 4),
(3, 87, 4),
(3, 88, 4),
(3, 89, 4),
(3, 90, 4),
(3, 91, 4),
(3, 92, 4),
(3, 93, 4),
(3, 94, 4),
(3, 95, 4),
(3, 96, 4),
(3, 97, 4),
(3, 101, 4),
(3, 102, 4),
(3, 103, 4),
(3, 104, 4),
(3, 105, 4),
(3, 106, 4),
(3, 107, 4),
(3, 108, 4),
(3, 109, 4),
(3, 110, 4),
(3, 111, 4),
(3, 112, 4),
(3, 113, 4),
(3, 114, 4),
(3, 115, 4),
(3, 116, 4),
(3, 117, 4),
(3, 118, 4),
(3, 119, 4),
(3, 120, 4),
(3, 121, 4),
(3, 122, 4),
(3, 123, 4),
(3, 124, 4),
(3, 125, 4),
(3, 126, 4),
(3, 127, 4),
(3, 128, 4),
(4, 23, 2),
(4, 56, 4),
(4, 58, 2),
(4, 59, 4),
(4, 60, 4),
(4, 61, 4),
(4, 62, 2),
(4, 63, 3),
(4, 64, 3),
(4, 65, 4),
(4, 66, 4),
(4, 67, 4),
(4, 68, 4),
(4, 69, 4),
(4, 70, 1),
(4, 71, 4),
(4, 72, 4),
(4, 73, 3),
(4, 74, 4),
(4, 75, 3),
(4, 76, 3),
(4, 77, 4),
(4, 78, 4),
(4, 79, 4),
(4, 80, 3),
(4, 81, 4),
(4, 82, 2),
(4, 83, 4),
(4, 84, 4),
(4, 85, 4),
(4, 86, 4),
(4, 87, 3),
(4, 88, 4),
(4, 89, 2),
(4, 90, 2),
(4, 91, 2),
(4, 92, 4),
(4, 93, 2),
(4, 94, 4),
(4, 95, 4),
(4, 96, 2),
(4, 97, 2),
(4, 101, 4),
(4, 102, 4),
(4, 103, 4),
(4, 104, 4),
(4, 105, 2),
(4, 106, 4),
(4, 107, 2),
(4, 108, 4),
(4, 109, 4),
(4, 110, 4),
(4, 111, 4),
(4, 112, 4),
(4, 113, 4),
(4, 114, 4),
(4, 115, 4),
(4, 116, 4),
(4, 117, 4),
(4, 118, 4),
(4, 119, 4),
(4, 120, 4),
(4, 121, 4),
(4, 122, 4),
(4, 123, 4),
(4, 124, 4),
(4, 125, 4),
(4, 126, 4),
(4, 127, 4),
(4, 128, 4),
(5, 1, 4),
(5, 2, 4),
(5, 3, 3),
(5, 4, 4),
(5, 5, 4),
(5, 6, 4),
(5, 7, 1),
(5, 8, 2),
(5, 9, 3),
(5, 10, 4),
(5, 11, 4),
(5, 12, 2),
(5, 13, 4),
(5, 14, 2),
(5, 15, 4),
(5, 16, 4),
(5, 17, 4),
(5, 18, 4),
(5, 19, 4),
(5, 20, 4),
(5, 21, 4),
(5, 22, 4),
(5, 23, 4),
(5, 24, 4),
(5, 27, 4),
(5, 28, 2),
(5, 29, 4),
(5, 30, 4),
(5, 31, 4),
(5, 32, 4),
(5, 33, 4),
(5, 34, 4),
(5, 35, 4),
(5, 36, 4),
(5, 37, 4),
(5, 38, 4),
(5, 39, 4),
(5, 40, 4),
(5, 41, 4),
(5, 42, 4),
(5, 43, 4),
(5, 44, 4),
(5, 45, 4),
(5, 46, 4),
(5, 47, 4),
(5, 48, 4),
(5, 49, 4),
(5, 50, 4),
(5, 51, 4),
(5, 52, 4),
(5, 53, 4),
(5, 54, 2),
(5, 55, 4),
(5, 57, 4),
(6, 1, 4),
(6, 2, 4),
(6, 3, 4),
(6, 4, 4),
(6, 5, 4),
(6, 6, 4),
(6, 7, 4),
(6, 8, 4),
(6, 9, 4),
(6, 10, 4),
(6, 11, 4),
(6, 12, 4),
(6, 13, 4),
(6, 14, 4),
(6, 15, 4),
(6, 16, 4),
(6, 17, 4),
(6, 18, 4),
(6, 19, 4),
(6, 20, 4),
(6, 21, 4),
(6, 22, 4),
(6, 23, 4),
(6, 24, 4),
(6, 27, 4),
(6, 28, 4),
(6, 29, 4),
(6, 30, 4),
(6, 31, 4),
(6, 32, 4),
(6, 33, 4),
(6, 34, 4),
(6, 35, 4),
(6, 36, 4),
(6, 37, 4),
(6, 38, 4),
(6, 39, 4),
(6, 40, 4),
(6, 41, 4),
(6, 42, 4),
(6, 43, 4),
(6, 44, 4),
(6, 45, 4),
(6, 46, 4),
(6, 47, 4),
(6, 48, 4),
(6, 49, 4),
(6, 50, 4),
(6, 51, 4),
(6, 52, 4),
(6, 53, 4),
(6, 54, 4),
(6, 55, 4),
(6, 56, 4),
(6, 57, 4),
(7, 1, 4),
(7, 2, 4),
(7, 3, 4),
(7, 4, 4),
(7, 5, 4),
(7, 6, 4),
(7, 7, 4),
(7, 8, 4),
(7, 9, 4),
(7, 10, 4),
(7, 11, 4),
(7, 12, 4),
(7, 13, 4),
(7, 14, 4),
(7, 15, 4),
(7, 16, 4),
(7, 17, 4),
(7, 18, 4),
(7, 19, 4),
(7, 20, 4),
(7, 21, 4),
(7, 22, 4),
(7, 23, 4),
(7, 24, 4),
(7, 27, 4),
(7, 28, 4),
(7, 29, 4),
(7, 30, 4),
(7, 31, 4),
(7, 32, 4),
(7, 33, 4),
(7, 34, 4),
(7, 35, 4),
(7, 36, 4),
(7, 37, 4),
(7, 38, 4),
(7, 39, 4),
(7, 40, 4),
(7, 41, 4),
(7, 42, 4),
(7, 43, 4),
(7, 44, 4),
(7, 45, 4),
(7, 46, 4),
(7, 47, 4),
(7, 48, 4),
(7, 49, 4),
(7, 50, 4),
(7, 51, 4),
(7, 52, 4),
(7, 53, 4),
(7, 54, 4),
(7, 55, 4),
(7, 56, 4),
(7, 57, 4);

-- --------------------------------------------------------

--
-- Table structure for table `char_categories_policy`
--

CREATE TABLE `char_categories_policy` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) NOT NULL,
  `level` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `amount` float UNSIGNED DEFAULT '0',
  `count_of_family` int(2) UNSIGNED DEFAULT '0',
  `started_at` date NOT NULL,
  `ends_at` date DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_currencies`
--

CREATE TABLE `char_currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` varchar(45) DEFAULT NULL,
  `symbol` varchar(45) DEFAULT NULL,
  `default` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_currencies`
--

INSERT INTO `char_currencies` (`id`, `name`, `code`, `symbol`, `default`) VALUES
(1, 'دولار الأمريكي', 'USD', '$', 1),
(2, 'يورو', 'EUR', '€', 0),
(3, 'دينار أردني', 'JOD', 'د.أ', 0),
(4, 'شيكل اسرائيلي', 'ILS', '₪', 0);

-- --------------------------------------------------------

--
-- Table structure for table `char_currencies_exchange`
--

CREATE TABLE `char_currencies_exchange` (
  `from_currency_id` int(10) UNSIGNED NOT NULL,
  `to_currency_id` int(10) UNSIGNED NOT NULL,
  `rate_date` date NOT NULL,
  `rate` float NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_death_causes`
--

CREATE TABLE `char_death_causes` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_death_causes_i18n`
--

CREATE TABLE `char_death_causes_i18n` (
  `death_cause_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_death_causes_view`
--

CREATE TABLE `char_death_causes_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_diseases`
--

CREATE TABLE `char_diseases` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_diseases_i18n`
--

CREATE TABLE `char_diseases_i18n` (
  `disease_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_diseases_view`
--

CREATE TABLE `char_diseases_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_documents`
--

CREATE TABLE `char_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `document_type_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) NOT NULL COMMENT 'اسم الملف ومسار تخزينه',
  `mimetype` varchar(255) NOT NULL COMMENT 'نوع الملف Mime-Type',
  `filesize` int(10) UNSIGNED NOT NULL COMMENT 'حجم الملف (بايت)',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على تفاصيل الملفات والمستندات التي يتم رفعها إلى النظام بشكل عام';

-- --------------------------------------------------------

--
-- Table structure for table `char_document_types`
--

CREATE TABLE `char_document_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `expiry` tinyint(4) NOT NULL COMMENT 'فترة صلاحية المستند بالأشهر',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_document_types`
--

INSERT INTO `char_document_types` (`id`, `expiry`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 0, '2016-05-29 12:50:41', NULL, NULL),
(5, 0, '2016-05-29 12:50:41', NULL, NULL),
(7, 0, '2016-05-29 12:50:41', NULL, NULL),
(8, 6, '2016-05-29 12:50:41', NULL, NULL),
(9, 0, '2016-05-29 12:50:41', NULL, NULL),
(10, 0, '2016-05-29 12:50:41', NULL, NULL),
(11, 0, '2016-05-29 12:50:42', NULL, NULL),
(12, 0, '2016-05-29 12:50:42', NULL, NULL),
(13, 12, '0000-00-00 00:00:00', NULL, NULL),
(15, 6, '0000-00-00 00:00:00', NULL, NULL),
(16, 12, '0000-00-00 00:00:00', NULL, NULL),
(17, 0, '0000-00-00 00:00:00', NULL, NULL),
(18, 120, '0000-00-00 00:00:00', NULL, NULL),
(19, 120, '0000-00-00 00:00:00', NULL, NULL),
(20, 0, '0000-00-00 00:00:00', NULL, NULL),
(21, 120, '0000-00-00 00:00:00', NULL, NULL),
(22, 120, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_document_types_i18n`
--

CREATE TABLE `char_document_types_i18n` (
  `document_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_document_types_i18n`
--

INSERT INTO `char_document_types_i18n` (`document_type_id`, `language_id`, `name`) VALUES
(4, 1, 'حجة الحضانة'),
(4, 2, 'Hadana'),
(5, 1, 'حجة الوصاية'),
(7, 1, 'الوكالة'),
(8, 1, 'التوكيل'),
(9, 1, 'حجة إعالة'),
(10, 1, 'شهادة وفاة الأم'),
(11, 1, 'تقرير طبي'),
(12, 1, 'حجة العزوبية'),
(13, 1, 'وصل مالي بإثبات الدين'),
(15, 1, 'شهادة قيد جامعية'),
(16, 1, 'صورة شخصية'),
(17, 1, 'شهادة وفاة الأب'),
(18, 1, 'هوية الأم'),
(19, 1, 'هوية المعيل'),
(20, 1, 'شهادة الميلاد'),
(21, 1, 'هوية الأب'),
(22, 1, 'صورة الهوية');

-- --------------------------------------------------------

--
-- Table structure for table `char_document_types_view`
--

CREATE TABLE `char_document_types_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `expiry` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_authorities`
--

CREATE TABLE `char_edu_authorities` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_authorities_i18n`
--

CREATE TABLE `char_edu_authorities_i18n` (
  `edu_authority_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_degrees`
--

CREATE TABLE `char_edu_degrees` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_degrees_i18n`
--

CREATE TABLE `char_edu_degrees_i18n` (
  `edu_degree_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_stages`
--

CREATE TABLE `char_edu_stages` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_edu_stages_i18n`
--

CREATE TABLE `char_edu_stages_i18n` (
  `edu_stage_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_essentials`
--

CREATE TABLE `char_essentials` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على خيارات أثاث ومحتويات المنزل لاستخدامها في استمارات المساعدات';

-- --------------------------------------------------------

--
-- Table structure for table `char_essentials_weights`
--

CREATE TABLE `char_essentials_weights` (
  `essential_id` int(10) UNSIGNED NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_forms`
--

CREATE TABLE `char_forms` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'الجميعة: في حال كان النموذج عام على مستوى النظام، لا يتم تحديد معرّف الجمعية في هذا الحقل',
  `name` varchar(255) NOT NULL COMMENT 'اسم داخلي للنموذج يحدد من قبل النظام وليس المستخدم',
  `type` tinyint(3) UNSIGNED NOT NULL COMMENT 'نوع النموذج: 1 = نظام، 2 = مخصص',
  `status` tinyint(4) UNSIGNED NOT NULL COMMENT 'حالة النموذج: 1 = فعال، 2 = غير فعال، 3 = محذوف',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='النماذج المستخدمة في النظام أو التي يتم انشاؤها من قبل مستخدمي النظام (الجمعيات)';

--
-- Dumping data for table `char_forms`
--

INSERT INTO `char_forms` (`id`, `organization_id`, `name`, `type`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'استمارة مساعدات الحالة الاجتماعية', 1, 1, '0000-00-00 00:00:00', NULL, NULL),
(2, 1, 'استمارة كفالة اليتيم', 1, 1, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_forms_cases_data`
--

CREATE TABLE `char_forms_cases_data` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `element_id` int(10) UNSIGNED NOT NULL,
  `option_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'في حال كان الحقل متعدد الخيارات تخزن هنا قيمة الخيار أو تترك فارغة في حالة الحقول غير متعددة الخيارات',
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على البيانات الخاصة بالحالة (المستفيد) لكل حقل في النموذج';

-- --------------------------------------------------------

--
-- Table structure for table `char_forms_elements`
--

CREATE TABLE `char_forms_elements` (
  `id` int(10) UNSIGNED NOT NULL,
  `form_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(45) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `value` text,
  `priority` tinyint(4) NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_forms_elements`
--

INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`) VALUES
(1, 1, '1', 'name', 'الاسم رباعي ', 'حقل نصي يحتوي على حروف فقط لايزيد طوله عن 45 حرف', NULL, 4, 0),
(2, 1, '1', 'id_card_number', 'رقم الهوية', 'حقل رقمي لايزيد طوله عن 9 ارقام', NULL, 4, 0),
(3, 1, '4', 'gender', 'الجنس', 'حقل رقمي', NULL, 4, 0),
(4, 1, '1', 'birthday', 'تاريخ الميلاد', 'تاريخ', NULL, 4, 0),
(5, 1, '4', 'birth_place', 'مكان الميلاد', 'حقل رقمي', NULL, 4, 0),
(6, 1, '4', 'marital_status_id', 'الحالة الاجتماعية  لرب الأسرة', 'حقل رقمي', NULL, 4, 0),
(7, 1, '4', 'refugee', 'حالة المواطنة  لرب الأسرة', 'حقل رقمي', NULL, 3, 0),
(8, 1, '1', 'unrwa_card_number', 'رقم كرت المؤن', 'حقل رقمي', NULL, 3, 0),
(9, 1, '4', 'nationality', 'جنسية رب الأسرة', 'حقل رقمي', NULL, 4, 0),
(10, 1, '4', 'location', 'بيانات عنوان الأسرة', 'حقل رقمي', NULL, 4, 0),
(11, 1, '1', 'street_address', 'عنوان الشارع', 'حقل نصي', NULL, 4, 0),
(12, 1, '1', 'monthly_income', 'الدخل الشهري', 'حقل رقمي', NULL, 4, 0),
(13, 1, '1', 'primary_mobile', 'رقم الجوال', 'حقل رقمي', NULL, 3, 0),
(14, 1, '1', 'secondary_mobile', 'رقم الجوال الاحتياطي', 'حقل رقمي', NULL, 3, 0),
(15, 1, '1', 'phone', 'رقم التلفون', 'حقل رقمي', NULL, 3, 0),
(16, 1, '4', 'working', 'هل يعمل رب الأسرة', 'حقل رقمي', NULL, 4, 0),
(17, 1, '4', 'can_work', 'القدرة على عمل رب الاسرة', 'حقل رقمي', NULL, 4, 0),
(18, 1, '4', 'work_reason_id', 'سبب عدم القدرة على العمل', 'حقل رقمي', NULL, 3, 0),
(19, 1, '4', 'work_job_id', 'المسمى الوظيفي لعمل رب الأسرة', 'حقل رقمي', NULL, 3, 0),
(20, 1, '4', 'work_status_id', 'حالة العمل لرب الأسرة', 'حقل رقمي', NULL, 3, 0),
(21, 1, '4', 'work_wage_id', 'الدخل الشهري حسب الفئة', 'حقل رقمي', NULL, 3, 0),
(22, 1, '1', 'work_location', 'مكان عمل رب الأسرة', 'حقل نصي', NULL, 3, 0),
(23, 1, '4', 'banks', 'الحسابات البنكية لرب الأسرة', 'حقل رقمي', NULL, 3, 0),
(24, 1, '1', 'study', 'هل يدرس فرد العائلة', 'نعم أو لا ', NULL, 3, 0),
(25, 1, '4', 'property_type_id', 'ملكية المنزل', 'حقل رقمي', NULL, 4, 0),
(26, 1, '1', 'rent_value', 'قيمة الايجار الشهري', 'حقل رقمي', NULL, 4, 0),
(27, 1, '4', 'roof_material_id', 'سقف المنزل', 'حقل رقمي', NULL, 4, 0),
(28, 1, '4', 'residence_condition', 'وضع المنزل', 'حقل رقمي', NULL, 4, 0),
(29, 1, '4', 'indoor_condition', 'وضع الأثاث', 'حقل رقمي', NULL, 3, 0),
(30, 1, '4', 'habitable', 'حالة المنزل', 'حقل رقمي', NULL, 3, 0),
(31, 1, '4', 'house_condition', 'صلاحية المنزل', 'حقل رقمي', NULL, 3, 0),
(32, 1, '1', 'rooms', 'عدد الغرف', 'حقل رقمي', NULL, 3, 0),
(33, 1, '1', 'area', 'المساحة بالمتر', 'حقل رقمي', NULL, 3, 0),
(34, 1, '4', 'essential_condition', 'أثاث المنزل', 'حقل رقمي', NULL, 3, 0),
(35, 1, '4', 'financial_aid_condition', 'المساعدة النقدية', '', NULL, 3, 0),
(36, 1, '1', 'non_financial_aid_condition', 'المساعدات العينية', '', NULL, 3, 0),
(37, 1, '4', 'properties_condition', ' العقارات و الأملاك ', '', NULL, 3, 0),
(38, 1, '4', 'reconstructions_promised', 'وعد العائلة بالبناء', 'حقل رقمي', NULL, 3, 0),
(39, 1, '1', 'reconstructions_organization_name', 'المؤسسة التي وعدت بالبناء للعائلة', 'حقل نصي', NULL, 3, 0),
(40, 1, '1', 'case_needs', 'احتياجات العائلة', 'مساحة نصية', NULL, 3, 0),
(41, 1, '1', 'notes', 'توصيات الباحث الاجتماعي', 'مساحة نصية', NULL, 3, 0),
(42, 1, '3', 'visitor', 'اسم الباحث الاجتماعي', 'حقل نصي', NULL, 3, 0),
(43, 1, '1', 'visited_at', 'تاريخ زيارة الباحث الاجتماعي', 'تاريخ', NULL, 3, 0),
(44, 1, '3', 'kinship_id', 'صلة قرابة فرد العائلة', ' ', NULL, 3, 0),
(45, 1, '4', 'authority', 'الجهة المشرفة على تعليم فرد العائلة', ' ', NULL, 3, 0),
(46, 1, '4', 'degree', 'الدرجة العلمية لفرد العائلة', ' ', NULL, 3, 0),
(47, 1, '4', 'stage', 'المرحلة الدراسية لفرد العائلة', ' ', NULL, 3, 0),
(48, 1, '4', 'grade', 'الصف ', ' ', NULL, 3, 0),
(49, 1, '1', 'school', 'المؤسسة العلمية لفرد العائلة', ' ', NULL, 3, 0),
(50, 1, '4', 'fm_working', 'حالة العمل لفرد العائلة', ' ', NULL, 3, 0),
(51, 1, '4', 'fm_work_job_id', 'المسمى الوظيفي لعمل فرد العائلة', ' ', NULL, 3, 0),
(52, 1, '4', 'fm_monthly_income', 'الدخل الشهري لفرد العائلة', ' ', NULL, 3, 0),
(53, 1, '4', 'condition', 'الحالة الصحية لفرد العائلة', ' ', NULL, 3, 0),
(54, 1, '4', 'disease_id', 'المرض أو الاعاقة لفرد العائلة', ' ', NULL, 3, 0),
(55, 1, '1', 'details', 'تفاصيل الحالة الصحية لفرد العائلة', ' ', NULL, 3, 0),
(56, 2, '1', 'name', 'الاسم', 'حقل نصي يحتوي على حروف فقط لايزيد طوله عن 45 حرف', NULL, 4, 0),
(57, 2, '1', 'id_card_number', 'رقم الهوية', 'حقل رقمي', NULL, 4, 0),
(58, 2, '1', 'birthday', 'تاريخ الميلاد', '', NULL, 4, 0),
(59, 2, '1', 'working', 'هل يعمل', 'حقل رقمي', NULL, 4, 0),
(60, 2, '1', 'work_job', 'نوع العمل', '', NULL, 4, 0),
(61, 2, '1', 'work_location', 'مكان العمل', 'الخاص بالأم', NULL, 4, 0),
(62, 2, '1', 'monthly_income', 'الدخل الشهري', '', NULL, 4, 0),
(63, 2, '1', 'specialization', 'التخصص', 'حقل رقمي', NULL, 4, 0),
(64, 2, '1', 'degree', 'المؤهل العلمي ( الأب )', 'حقل رقمي', NULL, 4, 0),
(65, 2, '1', 'death_date', 'تاريخ الوفاة ( الأب )', '', NULL, 4, 0),
(66, 2, '1', 'death_cause', 'سبب الوفاة ( الأب )', 'حقل رقمي', NULL, 4, 0),
(67, 2, '1', 'spouses', 'عدد الزوجات', 'حقل رقمي', NULL, 4, 0),
(68, 2, '1', 'father_condition', 'الحالة الصحية ( الأب)', '', NULL, 4, 0),
(69, 2, '1', 'father_disease', 'نوع المرض ( الأب )', '', NULL, 4, 0),
(70, 2, '1', 'father_details', 'تفاصيل المرض/الإعاقة ( الأب )', '', NULL, 4, 0),
(71, 2, '1', 'mother_marital_status', 'الحالة الإجتماعية-الوالدة', 'حقل رقمي', NULL, 4, 0),
(72, 2, '1', 'alive', 'على قيد الحياة', '', NULL, 4, 0),
(73, 2, '1', 'mother_death_date', 'تاريخ الوفاة -الوالدة', 'تاريخ الوفاة للام', NULL, 4, 0),
(74, 2, '1', 'mother_death_cause', 'سبب الوفاة-الوالدة', 'سبب الوفاة', NULL, 4, 0),
(75, 2, '1', 'mother_condition', 'الحالة الصحية ( الأم )', '', NULL, 4, 0),
(76, 2, '1', 'mother_disease', 'نوع المرض ( الأم )', '', NULL, 4, 0),
(77, 2, '1', 'mother_details', 'تفاصيل المرض/الإعاقة  ( الأم )', '', NULL, 4, 0),
(78, 2, '1', 'mother_nationality', 'جنسية الأم', 'حقل نصي', NULL, 4, 0),
(79, 2, '1', 'mother_stage', 'المرحلة الدراسية -الوالدة', 'حقل رقمي', NULL, 4, 0),
(80, 2, '1', 'mother_prev_family_name', 'اسم العائلة سابقا ( الأم )', 'حقل نصي', NULL, 4, 0),
(81, 2, '1', 'WhoIsGuardian', 'من هو المعيل ؟', '', NULL, 4, 0),
(82, 2, '1', 'guardian_kinship', 'صلة القرابة - المعيل', '', NULL, 4, 0),
(83, 2, '1', 'guardian_stage', 'المرحلة الدراسية - المعيل', '', NULL, 4, 0),
(84, 2, '1', 'guardian_marital_status', 'الحالة الاجتماعية ( المعيل )', 'حقل رقمي', NULL, 4, 0),
(85, 2, '1', 'guardian_nationality', 'جنسية المعيل ', 'حقل رقمي ', NULL, 4, 0),
(86, 2, '1', 'guardian_address', 'عنوان السكن ( المعيل )', '', NULL, 4, 0),
(87, 2, '1', 'guardian_phone', 'رقم التلفون ( المعيل )', '', NULL, 4, 0),
(88, 2, '1', 'guardian_primary_mobile', 'رقم الجوال الأساسي ( المعيل )', '', NULL, 4, 0),
(89, 2, '1', 'guardian_secondary_mobile', 'رقم الجوال الاحتياطي ( المعيل )', '', NULL, 4, 0),
(90, 2, '4', 'guardian_property_type', 'ملكية المنزل ( المعيل )', 'حقل رقمي', NULL, 4, 0),
(91, 2, '4', 'guardian_roof_material', 'سقف المنزل ( المعيل )', 'حقل رقمي', NULL, 4, 0),
(92, 2, '4', 'guardian_residence_condition', 'وضع المنزل ( المعيل )', 'حقل رقمي', NULL, 4, 0),
(93, 2, '4', 'guardian_indoor_condition', 'وضع الأثاث ( المعيل )', 'حقل رقمي', NULL, 3, 0),
(94, 2, '1', 'guardian_rooms', 'عدد الغرف', 'حقل رقمي', NULL, 3, 0),
(95, 2, '4', 'guardian_banks', 'اسم البنك ( المعيل )', 'حقل رقمي', NULL, 4, 0),
(96, 2, '1', 'gender', 'الجنس', '', NULL, 4, 0),
(97, 2, '1', 'nationality', 'الجنسية', '', NULL, 4, 0),
(98, 2, '1', 'birth_place', 'مكان الميلاد', '', NULL, 4, 0),
(99, 2, '1', 'address', 'عنوان السكن', '', NULL, 4, 0),
(100, 2, '1', 'phone', 'رقم التلفون', '', NULL, 4, 0),
(101, 2, '1', 'primary_mobile', 'رقم الجوال الأساسي', '', NULL, 4, 0),
(102, 2, '1', 'secondary_mobile', 'رقم الجوال الاحتياطي', '', NULL, 4, 0),
(103, 2, '1', 'type', 'نوع الدراسة', '', NULL, 4, 0),
(104, 2, '1', 'authority', 'الجهة التعليمية', '', NULL, 4, 0),
(105, 2, '1', 'stage', 'المرحلة الدراسية', '', NULL, 4, 0),
(106, 2, '1', 'grade', 'الصف', '', NULL, 4, 0),
(107, 2, '1', 'year', 'سنة الدراسة', '', NULL, 4, 0),
(108, 2, '1', 'school', 'اسم المؤسسة التعليمية', '', NULL, 4, 0),
(109, 2, '1', 'points', 'اخر نتيجة دراسية', '', NULL, 4, 0),
(110, 2, '1', 'level', 'مستوى التحصيل الدراسي', '', NULL, 4, 0),
(111, 2, '1', 'health_condition', 'الحالة الصحية', '', NULL, 4, 0),
(112, 2, '1', 'disease_id', 'نوع المرض', '', NULL, 4, 0),
(113, 2, '1', 'details', 'تفاصيل المرض/الإعاقة', '', NULL, 4, 0),
(114, 2, '1', 'property_type', 'نوع السكن', '', NULL, 4, 0),
(115, 2, '1', 'roof_material', 'نوع السقف', '', NULL, 4, 0),
(116, 2, '1', 'residence_condition', 'حالة السكن', '', NULL, 4, 0),
(117, 2, '1', 'indoor_condition', 'حالة أثاث المنزل', '', NULL, 4, 0),
(118, 2, '1', 'prayer', 'هل يصلي', '', NULL, 4, 0),
(119, 2, '1', 'prayer_reason', 'سبب ترك الصلاة', '', NULL, 4, 0),
(120, 2, '1', 'save_quran', 'هل يحفظ القران', '', NULL, 4, 0),
(121, 2, '1', 'quran_parts', 'عدد الأجزاء', '', NULL, 4, 0),
(122, 2, '1', 'quran_chapters', 'عدد السور', '', NULL, 4, 0),
(123, 2, '1', 'quran_reason', 'سبب ترك التحفيظ', '', NULL, 4, 0),
(124, 2, '1', 'banks', 'اسم البنك', 'حقل رقمي', NULL, 4, 0),
(125, 2, '1', 'quran_center', 'هل ملتحق بمركز تحفيظ', '', NULL, 4, 0),
(126, 2, '1', 'kinship', 'صلة القرابة', '', NULL, 4, 0),
(127, 2, '1', 'fm_working', 'هل يعمل / تعمل ( لفرد العائلة )', '', NULL, 4, 0),
(128, 2, '1', 'study', 'هل يدرس ?', 'نعم أو لا ', NULL, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `char_forms_elements_options`
--

CREATE TABLE `char_forms_elements_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `element_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_furniture_status`
--

CREATE TABLE `char_furniture_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_furniture_status_i18n`
--

CREATE TABLE `char_furniture_status_i18n` (
  `furniture_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_guardians`
--

CREATE TABLE `char_guardians` (
  `guardian_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الكفيل من جدول persons',
  `individual_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الشخص المكفول من جدول persons',
  `kinship_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'صلة القرابة',
  `organization_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط الكفيل مع الشخص (الحالة) المكفول';

-- --------------------------------------------------------

--
-- Table structure for table `char_habitable_status`
--

CREATE TABLE `char_habitable_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_habitable_status_i18n`
--

CREATE TABLE `char_habitable_status_i18n` (
  `habitable_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_house_status`
--

CREATE TABLE `char_house_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_house_status_i18n`
--

CREATE TABLE `char_house_status_i18n` (
  `house_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_kinship`
--

CREATE TABLE `char_kinship` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'معرف يتم تحديده يدويا',
  `level` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول صلة القرابة';

-- --------------------------------------------------------

--
-- Table structure for table `char_kinship_i18n`
--

CREATE TABLE `char_kinship_i18n` (
  `kinship_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_languages`
--

CREATE TABLE `char_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `native_name` varchar(255) NOT NULL,
  `default` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على اللغات التي يدعمها النظام لعرض المعلومات حسب اللغة المحددة';

--
-- Dumping data for table `char_languages`
--

INSERT INTO `char_languages` (`id`, `code`, `name`, `native_name`, `default`) VALUES
(1, 'ar', 'العربية', 'العربية', 1),
(2, 'en', 'الانجليزية', 'english', 0);

-- --------------------------------------------------------

--
-- Table structure for table `char_locations`
--

CREATE TABLE `char_locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_type` tinyint(3) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على المناطق والعناوين مخزنة بشكل شجري لاستيعاب اكبر عدد من مستويات تقسيم العناوين';

-- --------------------------------------------------------

--
-- Table structure for table `char_locations_i18n`
--

CREATE TABLE `char_locations_i18n` (
  `location_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_locations_view`
--

CREATE TABLE `char_locations_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `location_type` tinyint(3) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_marital_status`
--

CREATE TABLE `char_marital_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_marital_status_i18n`
--

CREATE TABLE `char_marital_status_i18n` (
  `marital_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_marital_status_view`
--

CREATE TABLE `char_marital_status_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_notifications`
--

CREATE TABLE `char_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text NOT NULL,
  `url` varchar(255) DEFAULT NULL COMMENT 'رابط لتوجيه المستخدم عند النقر على التنبيه',
  `status` tinyint(4) NOT NULL COMMENT '1 = جديد، 2 = مقروء',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول التنيهات والاشعارات لمستخدمي النظام';

-- --------------------------------------------------------

--
-- Table structure for table `char_organizations`
--

CREATE TABLE `char_organizations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الجمعية الأب',
  `logo` varchar(255) DEFAULT NULL COMMENT 'مسار صورة شعار الجمعية',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1=Organization,2=Sponsor',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'Active=1, Inactive=2,Closed=3,Deleted=4',
  `country` int(10) DEFAULT NULL,
  `container_share` float DEFAULT NULL COMMENT 'حصة الجمعية في الوعاء',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `district_id` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `location_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول بيانات الجمعيات والجهات الكافلة';

--
-- Dumping data for table `char_organizations`
--

INSERT INTO `char_organizations` (`id`, `name`, `parent_id`, `logo`, `type`, `status`, `country`, `container_share`, `created_at`, `updated_at`, `deleted_at`, `district_id`, `city_id`, `location_id`) VALUES
(1, 'جمعية دار الكتاب والسنة', NULL, NULL, 1, 1, 1, 0, '2017-10-16 22:18:00', NULL, NULL, NULL, NULL, NULL),
(30, 'دار الكتاب والسنة', 1, NULL, 1, 1, 1, 0, '2017-10-16 22:18:00', NULL, NULL, NULL, NULL, NULL);

--
-- Triggers `char_organizations`
--
DELIMITER $$
CREATE TRIGGER `char_organizations_closure_delete` AFTER DELETE ON `char_organizations` FOR EACH ROW BEGIN
	DELETE FROM `char_organizations_closure`
		WHERE `descendant_id` IN (
		SELECT d FROM (
			SELECT b.`descendant_id` AS d FROM `char_organizations_closure` b
				WHERE b.`ancestor_id` = OLD.id
		) AS dct
	);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `char_organizations_closure_insert` AFTER INSERT ON `char_organizations` FOR EACH ROW BEGIN
	INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
	SELECT `c`.`ancestor_id`, NEW.id, `c`.`depth` + 1
	FROM `char_organizations_closure` AS `c`
	WHERE `c`.`descendant_id` = NEW.parent_id
	UNION ALL
	SELECT NEW.id, NEW.id, 0;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `char_organizations_closure_update` AFTER UPDATE ON `char_organizations` FOR EACH ROW BEGIN
	IF NEW.parent_id <> OLD.parent_id THEN
		
		DELETE FROM `char_organizations_closure`
		WHERE `descendant_id` IN (
		  SELECT d FROM (SELECT `descendant_id` AS d FROM `char_organizations_closure` WHERE `ancestor_id` = NEW.id) AS dct
		)
		AND `ancestor_id` IN (
			SELECT a FROM (
				SELECT `ancestor_id` AS a FROM `char_organizations_closure`
				WHERE `descendant_id` = NEW.id
				AND `ancestor_id` <> NEW.id
			) AS ct
		);

		IF NEW.parent_id > 0 OR NEW.parent_id IS NOT NULL THEN
			INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
			SELECT supertbl.`ancestor_id`, subtbl.`descendant_id`, supertbl.`depth` + subtbl.`depth` + 1
			FROM `char_organizations_closure` as supertbl
			CROSS JOIN `char_organizations_closure` as subtbl
			WHERE supertbl.`descendant_id` = NEW.parent_id
			AND subtbl.`ancestor_id` = NEW.id;
		END IF;
	END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `char_organizations_closure`
--

CREATE TABLE `char_organizations_closure` (
  `ancestor_id` int(10) UNSIGNED NOT NULL COMMENT 'الأب',
  `descendant_id` int(10) UNSIGNED NOT NULL COMMENT 'الابن',
  `depth` int(10) UNSIGNED NOT NULL COMMENT 'المستوى'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لبناء العلاقة الشجرية بين المؤسسات';

--
-- Dumping data for table `char_organizations_closure`
--

INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`) VALUES
(1, 1, 0),
(1, 30, 1),
(30, 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `char_organizations_sms_providers`
--

CREATE TABLE `char_organizations_sms_providers` (
  `provider_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `api_username` varchar(255) DEFAULT NULL,
  `api_password` varchar(255) DEFAULT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_organization_neighborhoods_ratios`
--

CREATE TABLE `char_organization_neighborhoods_ratios` (
  `id` int(10) UNSIGNED NOT NULL,
  `location_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `ratio` int(10) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_organization_persons_banks`
--

CREATE TABLE `char_organization_persons_banks` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='معلومات الحسابات البنكية للشخص حسب االجمعية';

-- --------------------------------------------------------

--
-- Table structure for table `char_payments`
--

CREATE TABLE `char_payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `sponsorship_id` int(10) UNSIGNED DEFAULT NULL,
  `sponsor_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` float UNSIGNED NOT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL,
  `exchange_rate` float NOT NULL COMMENT 'سعر الصرف',
  `exchange_date` date DEFAULT NULL COMMENT 'تاريخ سعر الصرف',
  `organization_share` float UNSIGNED DEFAULT NULL COMMENT 'نسبة الجمعية',
  `administration_fees` float UNSIGNED DEFAULT NULL COMMENT 'الرسوم الإدارية',
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `exchange_type` tinyint(1) NOT NULL,
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_exchange` tinyint(1) UNSIGNED DEFAULT NULL,
  `payment_target` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الصرفيات';

-- --------------------------------------------------------

--
-- Table structure for table `char_payments_cases`
--

CREATE TABLE `char_payments_cases` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `case_id` int(10) UNSIGNED NOT NULL,
  `sponsor_number` varchar(45) NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `amount` float NOT NULL DEFAULT '0',
  `shekel_amount` float NOT NULL DEFAULT '0',
  `guardian_id` int(10) UNSIGNED DEFAULT NULL,
  `recipient` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الحالات المستفيدة من الصرفية';

-- --------------------------------------------------------

--
-- Table structure for table `char_payments_documents`
--

CREATE TABLE `char_payments_documents` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `document_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `person_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مرفقات وملفات الصرفية';

-- --------------------------------------------------------

--
-- Table structure for table `char_payments_persons`
--

CREATE TABLE `char_payments_persons` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `date_from` date DEFAULT NULL,
  `date_to` date DEFAULT NULL,
  `sponsor_number` varchar(45) NOT NULL,
  `amount` float NOT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `guardian_id` int(10) UNSIGNED DEFAULT NULL,
  `cheque_number` varchar(45) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `months` tinyint(4) DEFAULT NULL,
  `notes` text,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول المستفيدين من الصرفية';

-- --------------------------------------------------------

--
-- Table structure for table `char_payments_recipient`
--

CREATE TABLE `char_payments_recipient` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `mother_id` int(10) UNSIGNED DEFAULT NULL,
  `father_id` int(10) UNSIGNED DEFAULT NULL,
  `amount` float NOT NULL DEFAULT '0',
  `shekel_amount` float NOT NULL DEFAULT '0',
  `bank_id` int(10) UNSIGNED DEFAULT NULL,
  `cheque_account_number` varchar(45) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول االمستلمين للصرفية';

-- --------------------------------------------------------

--
-- Table structure for table `char_permissions`
--

CREATE TABLE `char_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL COMMENT 'كود فريد للصلاحية يستخدم برمجيا في النظام',
  `name` varchar(255) NOT NULL COMMENT 'اسم الصلاحية مقروء'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول معرفات صلاحيات النظام';

--
-- Dumping data for table `char_permissions`
--

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(1, 1, 'auth.users.manage', 'إدارة مستخدمي النظام'),
(2, 1, 'auth.users.view', 'عرض بيانات المستخدم'),
(3, 1, 'auth.users.create', 'إنشاء حساب مستخدم جديد'),
(4, 1, 'auth.users.update', 'تحرير بيانات مستخدم'),
(5, 1, 'auth.users.delete', 'حذف حساب مستخدم'),
(6, 1, 'auth.users.changePassword', 'تغيير كلمة المرور لحساب مستخدم'),
(7, 1, 'auth.roles.manage', 'إدارة مجموعات الصلاحيات'),
(8, 1, 'auth.roles.view', 'عرض معلومات مجموعة الصلاحيات'),
(9, 1, 'auth.roles.create', 'إنشاء مجموعة صلاحيات جديدة'),
(10, 1, 'auth.roles.update', 'تحرير مجموعة صلاحيات'),
(11, 1, 'auth.roles.delete', 'حذف مجموعة صلاحيات'),
(12, 2, 'doc.catregories.manage', 'إدارة تصنيفات الملفات'),
(13, 2, 'doc.catregories.view', 'عرض بيانات تصنيف الملفات'),
(14, 2, 'doc.catregories.create', 'إنشاء تصنيف ملف جديد'),
(15, 2, 'doc.catregories.update', 'تحرير تصنيف ملف'),
(16, 2, 'doc.catregories.delete', 'حذف تصنيف ملف'),
(17, 2, 'doc.files.manage', 'إدارة الملفات'),
(18, 2, 'doc.files.view', 'عرض معلومات ملف'),
(19, 2, 'doc.files.create', 'إنشاء ملف جديد'),
(20, 2, 'doc.files.update', 'تحرير معلومات ملف'),
(21, 2, 'doc.files.delete', 'حذف ملف'),
(22, 2, 'doc.tags.manage', 'إدارة وسوم الملفات'),
(23, 2, 'doc.tags.view', 'عرض معلومات الوسم'),
(24, 2, 'doc.tags.create', 'إنشاء وسم جديد'),
(25, 2, 'doc.tags.update', 'تحرير معلومات وسم'),
(26, 2, 'doc.tags.delete', 'حذف وسم'),
(27, 5, 'sms.smsProviders.manage', 'إدارة مزودي خدمة الرسائل القصيرة'),
(28, 5, 'sms.organizationsSmsProviders.manage', 'إدارة بيانات خدمة الرسائل '),
(29, 5, 'sms.smsProviders.create', 'إنشاء مزود خدمة جديد'),
(30, 5, 'sms.organizationsSmsProviders.create', 'إنشاء سجل بيانات خدمة رسائل جديد'),
(31, 5, 'sms.smsProviders.update', 'تحرير بيانات مزود خدمة '),
(32, 5, 'sms.organizationsSmsProviders.update', 'تحرير بيانات سجل الخدمة'),
(33, 5, 'sms.smsProviders.delete', 'حذف مزود خدمة'),
(34, 5, 'sms.organizationsSmsProviders.delete', 'حذف سجل بيانات الخدمة'),
(35, 5, 'sms.smsProviders.view', 'عرض بيانات مزود خدمة '),
(36, 5, 'sms.organizationsSmsProviders.view', 'عرض سجل بيانات خدمة رسائل'),
(37, 5, 'sms.smsProviders.sentSms', 'إرسال رسائل قصيرة'),
(38, 7, 'setting.aidSources.manage', 'إدارة مصادر المساعدات'),
(39, 7, 'setting.aidSources.create', 'إنشاء مصدر مساعدة جديد'),
(40, 7, 'setting.aidSources.update', 'تحرير بيانات مصدر مساعدة'),
(41, 7, 'setting.aidSources.delete', 'حذف مصدر مساعدة'),
(42, 7, 'setting.aidSources.view', 'عرض بيانات مصدر مساعدة'),
(43, 7, 'setting.banks.manage', 'إدارة البنوك'),
(44, 7, 'setting.banks.create', 'إنشاء بنك جديد'),
(45, 7, 'setting.banks.update', 'تحرير بيانات بنك'),
(46, 7, 'setting.banks.delete', 'حذف بنك '),
(47, 7, 'setting.banks.view', 'عرض بيانات بنك'),
(48, 7, 'setting.categories.manage', 'إدارة تصنيف الخدمات'),
(49, 7, 'setting.categories.create', 'إنشاء تصنيف جديد'),
(50, 7, 'setting.categories.update', 'تحرير بيانات تصنيف خدمة'),
(51, 7, 'setting.categories.delete', 'حذ تصنيف'),
(52, 7, 'setting.categories.view', 'عرض بيانات تصنيف'),
(53, 7, 'setting.currency.manage', 'إدارة العملات'),
(54, 7, 'setting.currency.create', 'إنشاء عملة جديدة'),
(55, 7, 'setting.currency.update', 'تحرير بيانات عملة'),
(56, 7, 'setting.currency.delete', 'حذف عملة'),
(57, 7, 'setting.currency.view', 'عرض بيانات عملة'),
(58, 7, 'setting.deathCauses.manage', 'إدارة أسباب الوفاة'),
(59, 7, 'setting.deathCauses.create', 'إنشاء سبب وفاة جديد'),
(60, 7, 'setting.deathCauses.update', 'تحرير بيانات سبب وفاة'),
(61, 7, 'setting.deathCauses.delete', 'حذف سبب وفاة'),
(62, 7, 'setting.deathCauses.view', 'عرض  بيانات سبب وفاة'),
(63, 7, 'setting.diseases.manage', 'إدارة الأمراض والاعاقات'),
(64, 7, 'setting.diseases.create', 'إنشاء نوع مرض أو اعاقة جديد'),
(65, 7, 'setting.diseases.update', 'تحرير بيانات مرض أو اعاقة'),
(66, 7, 'setting.diseases.delete', 'حذف مرض أو اعاقة'),
(67, 7, 'setting.diseases.view', 'عرض بيانات مرض أو اعاقة'),
(68, 7, 'setting.documentTypes.manage', 'إدارة أنواع المرفقات'),
(69, 7, 'setting.documentTypes.create', 'إنشاء نوع مرفق جديد'),
(70, 7, 'setting.documentTypes.update', 'تحرير بيانات مرفق'),
(71, 7, 'setting.documentTypes.delete', 'حذف نوع مرفق'),
(72, 7, 'setting.documentTypes.view', 'عرض بيانات مرفق '),
(73, 7, 'setting.eduAuthorities.manage', 'إدارة المؤسسات العلمية'),
(74, 7, 'setting.eduAuthorities.create', 'إنشاء مؤسسة علمية جديدة'),
(75, 7, 'setting.eduAuthorities.update', 'تحرير بيانات مؤسسة علمية'),
(76, 7, 'setting.eduAuthorities.delete', 'حذف مؤسسة علمية'),
(77, 7, 'setting.eduAuthorities.view', 'عرض بيانات مؤسسة علمية'),
(78, 7, 'setting.eduDegrees.manage', 'إدارة الدرجات العلمية'),
(79, 7, 'setting.eduDegrees.create', 'إنشاء درجة علمية جديدة'),
(80, 7, 'setting.eduDegrees.update', 'تحرير بيانات درجة علمية'),
(81, 7, 'setting.eduDegrees.delete', 'حذف درجة علمية'),
(82, 7, 'setting.eduDegrees.view', 'عرض بيانات درجة علمية'),
(83, 7, 'setting.eduStages.manage', 'إدارة المراحل التعليمية'),
(84, 7, 'setting.eduStages.create', 'إنشاء مرحلة علمية جديدة'),
(85, 7, 'setting.eduStages.update', 'تحرير بيانات مرحلة علمية'),
(86, 7, 'setting.eduStages.delete', 'حذف مرحلة علمية'),
(87, 7, 'setting.eduStages.view', 'عرض بيانات مرحلة علمية'),
(88, 7, 'setting.essentials.manage', 'إدارة مستلزمات المنزل'),
(89, 7, 'setting.essentials.create', 'إنشاء مستلزم منزل جديد'),
(90, 7, 'setting.essentials.update', 'تحرير بيانات مستلزم منزل'),
(91, 7, 'setting.essentials.delete', 'حذف مستلزم منزل'),
(92, 7, 'setting.essentials.view', 'عرض بيانات مستلزم المنزل'),
(93, 7, 'setting.kinship.manage', 'إدارة صلة القرابة'),
(94, 7, 'setting.kinship.create', 'إنشاء صلة قرابة جديدة'),
(95, 7, 'setting.kinship.update', 'تحرير بيانات صلة قرابة'),
(96, 7, 'setting.kinship.delete', 'حذف صلة قرابة'),
(97, 7, 'setting.kinship.view', 'عرض بيانات صلة القرابة'),
(98, 7, 'setting.language.manage', 'إدارة اللغات'),
(99, 7, 'setting.language.create', 'إنشاء لغة جديدة'),
(100, 7, 'setting.language.update', 'تحرير لغة'),
(101, 7, 'setting.language.delete', 'حذف لغة'),
(102, 7, 'setting.language.view', 'عرض معلومات لغة'),
(103, 7, 'setting.locations.manage', 'إدارة المناطق والعناوين'),
(104, 7, 'setting.locations.create', 'إنشاء عنوان أو منطقة جديدة'),
(105, 7, 'setting.locations.update', 'تحرير بيانات عنوان أو منطقة'),
(106, 7, 'setting.locations.delete', 'حذف منطقة او عنوان'),
(107, 7, 'setting.locations.view', 'عرض بيانات العنوان أو المنطقة'),
(108, 7, 'setting.maritalStatus.manage', 'إدارة الحالات الاجتماعية'),
(109, 7, 'setting.maritalStatus.create', 'إنشاء حالة اجتماعية جديدة'),
(110, 7, 'setting.maritalStatus.update', 'تحرير بيانات حالة اجتماعية'),
(111, 7, 'setting.maritalStatus.delete', 'حذف حالة اجتماعية'),
(112, 7, 'setting.maritalStatus.view', 'عرض بيانات الحالة الاجتماعية'),
(113, 7, 'setting.properties.manage', 'إدارة أنواع الممتلكات والعقارات'),
(114, 7, 'setting.properties.create', 'إنشاء نوع ممتلكات أو عقارات جديد'),
(115, 7, 'setting.properties.update', 'تحرير بيانات نوع ممتلكات أو العقارات'),
(116, 7, 'setting.properties.delete', 'حذف نوع ممتلكات او عقارات'),
(117, 7, 'setting.properties.view', 'عرض بيانات نوع الممتلكات أو العقارات'),
(118, 7, 'setting.roofMaterials.manage', 'إدارة أنواع أسقف المنازل'),
(119, 7, 'setting.roofMaterials.create', 'إنشاء نوع سقف جديد'),
(120, 7, 'setting.roofMaterials.update', 'تحرير بيانات  سقف منزل'),
(121, 7, 'setting.roofMaterials.delete', 'حذف سقف منزل'),
(122, 7, 'setting.roofMaterials.view', 'عرض بيانات سقف منزل'),
(123, 7, 'setting.PropertyTypes.manage', 'إدارة أنواع الملكيات للمنازل'),
(124, 7, 'setting.PropertyTypes.create', 'إنشاء نوع ملكية منزل جديد'),
(125, 7, 'setting.PropertyTypes.update', 'تحرير بيانات نوع الملكية'),
(126, 7, 'setting.PropertyTypes.delete', 'حذف نوع ملكية'),
(127, 7, 'setting.PropertyTypes.view', 'عرض بيانات نوع الملكية'),
(128, 7, 'setting.setting.manage', 'إدارة الاعدادات العامة'),
(129, 7, 'setting.setting.create', 'إنشاء اعداد عام جديد'),
(130, 7, 'setting.setting.update', 'تحرير بيانات اعداد'),
(131, 7, 'setting.setting.delete', 'حذف نوع الاعداد'),
(132, 7, 'setting.setting.view', 'عرض بيانات الاعداد'),
(133, 7, 'setting.workJob.manage', 'إدارة المسميات الوظيفية'),
(134, 7, 'setting.workJob.create', 'إنشاء مسمى وظيفي جديد'),
(135, 7, 'setting.workJob.update', 'تحرير بيانات مسمى وظيفي'),
(136, 7, 'setting.workJob.delete', 'حذف المسمى الوظيفي'),
(137, 7, 'setting.workJob.view', 'عرض بيانات المسمى الوظيفي'),
(138, 7, 'setting.workReason.manage', 'إدارة أسباب ترك العمل'),
(139, 7, 'setting.workReason.create', 'إنشاء سبب ترك عمل جديد'),
(140, 7, 'setting.workReason.update', 'تحرير بيانات سبب ترك عمل'),
(141, 7, 'setting.workReason.delete', 'حذف سبب ترك العمل'),
(142, 7, 'setting.workReason.view', 'عرض بيانات سبب ترك العمل'),
(143, 7, 'setting.workStatus.manage', 'إدارة حالات العمل'),
(144, 7, 'setting.workStatus.create', 'إنشاء حالة عمل جديدة'),
(145, 7, 'setting.workStatus.update', 'تحرير بيانات حالة عمل'),
(146, 7, 'setting.workStatus.delete', 'حذف حالة العمل'),
(147, 7, 'setting.workStatus.view', 'عرض بيانات حالة العمل'),
(148, 7, 'setting.workWages.manage', 'إدارة فئات الأجور'),
(149, 7, 'setting.workWages.create', 'إنشاء فئة أجر جديدة'),
(150, 7, 'setting.workWages.update', 'تحرير بيانات فئة أجر'),
(151, 7, 'setting.workWages.delete', 'حذف فئة الأجر'),
(152, 7, 'setting.workWages.view', 'عرض بيانات فئة الأجور'),
(153, 6, 'forms.customForms.manage', 'إدارة النماذج المخصصة'),
(154, 6, 'forms.customForms.create', 'إنشاء نموذج مخصص جديد'),
(155, 6, 'forms.customForms.update', 'تحرير بيانات نموذج مخصص'),
(156, 6, 'forms.customForms.delete', 'حذف نموذج مخصص'),
(157, 6, 'forms.customForms.view', 'عرض بيانات نموذج مخصص'),
(158, 6, 'forms.elementOption.manage', 'إدارة خيارات العناصر'),
(159, 6, 'forms.elementOption.create', 'إنشاء خيار عنصر جديد'),
(160, 6, 'forms.elementOption.update', 'تحرير بيانات خيار عنصر'),
(161, 6, 'forms.elementOption.delete', 'حذف خيار عنصر'),
(162, 6, 'forms.elementOption.view', 'عرض بيانات خيار عنصر'),
(163, 6, 'forms.formElement.manage', 'إدارة عناصر النموذج'),
(164, 6, 'forms.formElement.create', 'إنشاء عنصر نموذج جديد'),
(165, 6, 'forms.formElement.update', 'تحرير بيانات عنصر نموذج'),
(166, 6, 'forms.formElement.delete', 'حذف عنصر نموذج'),
(167, 6, 'forms.formElement.view', 'عرض بيانات عنصر نموذج'),
(168, 6, 'forms.formsCasesData.manage', 'إدارة بيانات النماذج المخصصة '),
(169, 6, 'forms.formsCasesData.create', 'إنشاءسحل بيانات نموذج مخصص جديد'),
(170, 6, 'forms.formsCasesData.update', 'نحرير سجل بيانات نموذج مخصص'),
(171, 6, 'forms.formsCasesData.delete', 'حذف سجل بيانات نموذج مخصص'),
(172, 6, 'forms.formsCasesData.view', 'عرض سجل بيانات نموذج مخصص'),
(173, 3, 'aid.category.manage', 'إدارة أنواع الاستمارات المساعدات'),
(174, 3, 'aid.category.create', 'إنشاء نوع استمارة مساعدات جديد'),
(175, 3, 'aid.category.update', 'تحرير بيانات نوع استمارة'),
(176, 3, 'aid.category.delete', 'حذف نوع استمارة'),
(177, 3, 'aid.category.view', 'عرض بيانات نوع الاستمارة'),
(178, 3, 'aid.voucher.manage', 'إدارة قسائم المساعدات'),
(179, 3, 'aid.voucher.create', 'إنشاء قسيمة مساعدة جديدة'),
(180, 3, 'aid.voucher.update', 'تحرير بيانات قسيمة مساعدة '),
(181, 3, 'aid.voucher.delete', 'حذف بيانات قسيمة مساعدة '),
(182, 3, 'aid.voucher.view', 'عرض بينات قسيمة مساعدة'),
(183, 3, 'aid.blockIDCard.manage', 'إدارة أرقام بطاقات الهوية المحظورة'),
(184, 3, 'aid.blockIDCard.create', 'إنشاء سجل رقم هوية محظور جديد'),
(185, 3, 'aid.blockIDCard.update', 'تحرير سجل رقم الهوية المحظور'),
(186, 3, 'aid.blockIDCard.delete', 'حذف سجل لرقم هوية محظور'),
(187, 3, 'aid.blockIDCard.view', 'عرض بيانات السجل المحظور'),
(188, 3, 'aid.case.manage', 'إدارة الحالات'),
(189, 3, 'aid.case.create', 'إنشاء سجل حالة اجتماعية جدي'),
(190, 3, 'aid.case.update', 'تحرير بيانات حالة'),
(191, 3, 'aid.case.delete', 'حذف حالة اجتماعية'),
(192, 3, 'aid.case.view', 'عرض بيانات الحالة'),
(193, 3, 'aid.case.export', 'تصدير بيانات الحالات'),
(194, 3, 'aid.personsVoucher.manage', 'إدارة قسائم المساعدات '),
(195, 3, 'aid.personsVoucher.create', 'إنشاء سجل تقديم مساعدات للأشخاص جديد'),
(196, 3, 'aid.personsVoucher.update', 'تحرير بيانات سجل تلقي مساعدة'),
(197, 3, 'aid.personsVoucher.delete', 'حذف سجل بيانات تلقي مساعدة لشخص'),
(198, 3, 'aid.personsVoucher.view', 'عرض مساعدات الشخص'),
(199, 3, 'aid.personsVoucher.export', 'تصدير كبونات مساعدات الاشخاص'),
(200, 4, 'sponsorship.category.manage', 'إدارة أنواع الاستمارات'),
(201, 4, 'sponsorship.category.create', 'إنشاء نوع استمارة كفالات جديد'),
(202, 4, 'sponsorship.category.update', 'تحرير بيانات نوع استمارة'),
(203, 4, 'sponsorship.category.delete', 'حذف نوع استمارة'),
(204, 4, 'sponsorship.category.view', 'عرض بيانات نوع الاستمارة'),
(205, 4, 'sponsorship.blockIDCard.manage', 'إدارة أرقام بطاقات الهوية المحظورة'),
(206, 4, 'sponsorship.blockIDCard.create', 'إنشاء سجل رقم هوية محظور جديد'),
(207, 4, 'sponsorship.blockIDCard.update', 'تحرير سجل رقم الهوية المحظور'),
(208, 4, 'sponsorship.blockIDCard.delete', 'حذف سجل لرقم هوية محظور'),
(209, 4, 'sponsorship.blockIDCard.view', 'عرض بيانات السجل المحظور'),
(210, 4, 'sponsorship.case.manage', 'إدارة الحالات'),
(211, 4, 'sponsorship.case.create', 'إنشاء سجل حالة اجتماعية جدي'),
(212, 4, 'sponsorship.case.update', 'تحرير بيانات حالة'),
(213, 4, 'sponsorship.case.delete', 'حذف حالة اجتماعية'),
(214, 4, 'sponsorship.case.view', 'عرض بيانات الحالة'),
(215, 4, 'sponsorship.case.export', 'تصدير بيانات الحالات'),
(222, 4, 'sponsorship.sponsorships.manage', 'إدارة الترشيحات'),
(223, 4, 'sponsorship.sponsorships.create', 'إنشاء ترشيح جديد لجهة كافلة'),
(224, 4, 'sponsorship.sponsorships.update', 'تحرير سجل ترشيح لجهة كافلة'),
(225, 4, 'sponsorship.sponsorships.delete', 'حذف ترشيح لجهة كافلة '),
(226, 4, 'sponsorship.sponsorships.view', 'عرض ترشيح'),
(227, 4, 'sponsorship.sponsorshipsPerson.manage', 'إدارة سجلات الترشيحات لجهات كافلة'),
(228, 4, 'sponsorship.sponsorshipsPerson.create', 'إنشاء سجل ترشيح جديد لشخص'),
(229, 4, 'sponsorship.sponsorshipsPerson.update', 'تحرير سجل ترشيح شخص '),
(230, 4, 'sponsorship.sponsorshipsPerson.delete', 'حذف سجل ترشيح شخص'),
(231, 4, 'sponsorship.sponsorshipsPerson.view', 'عرض بيانات ترشيح شخص'),
(232, 4, 'sponsorship.sponsorshipsPerson.export', 'تصدير سجلات ترشيح معين '),
(233, 4, 'sponsorship.payments.manage', 'إدارة الصرفيات'),
(234, 4, 'sponsorship.payments.create', 'إنشاء صرفية جديدة'),
(235, 4, 'sponsorship.payments.update', 'تحرير صرفية'),
(236, 4, 'sponsorship.payments.delete', 'حذف صرفية'),
(237, 4, 'sponsorship.payments.view', 'عرض بيانات صرفية'),
(238, 4, 'sponsorship.payments.export', 'تصدير بيانات او شيكات مستفيدين من صرفية'),
(239, 4, 'sponsorship.payments.import', 'استيراد كشوفات مستفيدين الصرفية'),
(240, 1, 'auth.users.permissions', 'تحرير الصلاحيات على مستوى المستخدم'),
(241, 1, 'auth.users.status', 'تفعيل وتعطيل حسابات المستخدمين'),
(242, 1, 'auth.roles.copy', 'نسخ مجموعة صلاحيات'),
(243, 1, 'auth.settings.manage', 'التحكم في إعدادات الدخول إلى النظام'),
(244, 8, 'org.organizations.manage', 'إدارة المؤسسات والجمعيات المحلية'),
(245, 8, 'org.organizations.create', 'إضافة مؤسسة أو جمعية محلية'),
(246, 8, 'org.organizations.update', 'تحرير بيانات مؤسسة أو جمعية محلية'),
(247, 8, 'org.organizations.view', 'مشاهدة بيانات مؤسسة أو جمعية محلية'),
(248, 8, 'org.organizations.delete', 'حذف مؤسسة أو جمعية محلية'),
(249, 8, 'org.sponsors.manage', 'إدارة المؤسسات والجمعيات الكافلة/الممولة'),
(250, 8, 'org.sponsors.create', 'إضافة مؤسسة أو جمعية كافلة/ممولة'),
(251, 8, 'org.sponsors.update', 'تحرير بيانات مؤسسة أو جمعية كافلة/ممولة'),
(252, 8, 'org.sponsors.view', 'مشاهدة بيانات مؤسسة أو جمعية كافلة/ممولة'),
(253, 8, 'org.sponsors.delete', 'حذف مؤسسة أو جمعية كافلة/ممولة'),
(254, 3, 'aid.voucherCategory.manage', 'إدارة أنواع قسائم المساعدات'),
(255, 3, 'aid.voucherCategory.create', 'إنشاء نوع قسيمة مساعدات جديد'),
(256, 3, 'aid.voucherCategory.update', 'تحرير بيانات نوع قسيمة'),
(257, 3, 'aid.voucherCategory.delete', 'حذف نوع قسيمة'),
(270, 4, 'sponsorship.sponsorshipCategoriesPolicy.manage', 'إدارة قيود أنواع الكفالات '),
(271, 4, 'sponsorship.sponsorshipCategoriesPolicy.create', 'إنشاء قيد كفالة جديد'),
(272, 4, 'sponsorship.sponsorshipCategoriesPolicy.update', 'تحرير بيانات قيد كفالة'),
(273, 4, 'sponsorship.sponsorshipCategoriesPolicy.delete', 'حذف قيد  كفالة'),
(274, 4, 'sponsorship.sponsorshipCategoriesPolicy.view', 'عرض بيانات قيد كفالة'),
(275, 3, 'aid.voucher.upload', 'رفع قالب قسيمة مساعدات'),
(276, 3, 'aid.voucherCategory.view', 'عرض بيانات نوع قسيمة'),
(277, 3, 'aid.aidsCategoriesPolicy.manage', 'إدارة قيود قسائم المساعدات'),
(278, 3, 'aid.aidsCategoriesPolicy.create', 'إنشاء قيد قسيمة جديد'),
(279, 3, 'aid.aidsCategoriesPolicy.update', 'تحرير بيانات قيد قسيمة'),
(280, 3, 'aid.aidsCategoriesPolicy.delete', 'حذف قيد قسيمة'),
(281, 3, 'aid.aidsCategoriesPolicy.view', 'عرض بيانات قيد'),
(282, 4, 'sponsorship.paymentsCases.manage', 'إدارة صرفيات الحالات'),
(283, 3, 'aid.categoriesFormElement.manage', 'إدارة عناصر نموذج استمارة المساعدات'),
(284, 3, 'aid.categoriesFormElement.create', 'إنشاء عنصر نموذج استمارة المساعدات'),
(285, 3, 'aid.categoriesFormElement.update', 'تعديل بيانات عنصر نموذج استمارة المساعدات'),
(286, 3, 'aid.categoriesFormElement.delete', 'حذف بيانات عنصر نموذج استمارة المساعدات'),
(287, 4, 'sponsorship.categoriesFormElement.manage', 'إدارة عناصر نموذج استمارة الكفالات'),
(288, 4, 'sponsorship.categoriesFormElement.create', 'إنشاء عنصر نموذج استمارة الكفالات'),
(289, 4, 'sponsorship.categoriesFormElement.update', 'تعديل بيانات عنصر نموذج استمارة الكفالات'),
(290, 4, 'sponsorship.categoriesFormElement.delete', 'حذف بيانات عنصر نموذج استمارة الكفالات'),
(291, 3, 'aid.categoriesFormElement.view', 'عرض بيانات عنصر نموذج استمارة المساعدات'),
(292, 4, 'sponsorship.categoriesFormElement.view', 'عرض بيانات عنصر نموذج استمارة الكفالات'),
(293, 4, 'sponsorship.sponsorshipCases.manage', 'إدارة تصنيفات المكفولين'),
(294, 4, 'sponsorship.sponsorshipCases.update', 'تعديل بيانات سجل في تصنيفات المكفولين'),
(297, 4, 'sponsorship.sponsorshipCases.view', 'عرض سجل  مكفول'),
(298, 4, 'sponsorship.sponsorshipCases.export', 'تصدير سجلات بيانات المكفولين'),
(299, 1, 'auth.users.trashed', 'إدارة المستخدمين الذين تم حذفهم'),
(300, 1, 'auth.users.restore', 'استرجاع المستخدمين الذين تم حذفهم'),
(301, 4, 'sponsorship.updateSetting.manage', 'إدارة إعدادات تحديث البيانات'),
(302, 4, 'sponsorship.updateSetting.create', 'إنشاء  سجل تحديث بيانات جديد'),
(303, 4, 'sponsorship.updateSetting.update', 'تعديل بيانات اعدادا سجل التحديث'),
(304, 4, 'sponsorship.updateSetting.delete', 'حذف سجل اعداد تحديث البيانات'),
(305, 4, 'sponsorship.updateSetting.view', 'عرض بيانات اعداد سجل التحدث'),
(306, 3, 'aid.aidsVisitorNotes.manage', 'إدارة إعدادات رأي الباحث'),
(307, 3, 'aid.aidsVisitorNotes.create', 'إنشاء اعداد رأي باحث جديد'),
(308, 3, 'aid.aidsVisitorNotes.update', 'تحرير اعداد رأي باحث'),
(309, 3, 'aid.aidsVisitorNotes.delete', 'حذف سجل اعداد رأي الباحث'),
(310, 3, 'aid.aidsVisitorNotes.view', 'عرض بيانات اعداد رأي باحث'),
(311, 4, 'sponsorship.sponsorshipVisitorNotes.manage', 'إدارة إعدادات رأي الباحث'),
(312, 4, 'sponsorship.sponsorshipVisitorNotes.create', 'إنشاء اعداد رأي باحث جديد'),
(313, 4, 'sponsorship.sponsorshipVisitorNotes.update', 'تحرير اعداد رأي باحث'),
(314, 4, 'sponsorship.sponsorshipVisitorNotes.delete', 'حذف سجل اعداد رأي الباحث'),
(315, 4, 'sponsorship.sponsorshipVisitorNotes.view', 'عرض بيانات اعداد رأي باحث'),
(316, 3, 'aid.neighborhoodsRatios.manage', 'إدارة نسب الأهلية للمناطق'),
(317, 3, 'aid.neighborhoodsRatios.create', 'إنشاء نسبة أهلية لمنطقة'),
(318, 3, 'aid.neighborhoodsRatios.update', 'تحرير نسب الأهلية للمناطق'),
(319, 3, 'aid.neighborhoodsRatios.delete', 'حذف نسبة الأهلية للمنطقة'),
(320, 3, 'aid.neighborhoodsRatios.view', 'عرض نسب الأهلية للمناطق'),
(321, 9, 'aidRepository.categories.manage', 'إدارة تصنيفات مشاريع إدارة الوعاء'),
(322, 9, 'aidRepository.categories.create', 'إنشاء تصنيف جديد لمشاريع إدارة الوعاء'),
(323, 9, 'aidRepository.categories.update', 'تحرير تصنيف مشروع إدارة الوعاء'),
(324, 9, 'aidRepository.categories.delete', 'حذف تصنيف مشروع إدارة الوعاء'),
(325, 9, 'aidRepository.categories.view', 'عرض معلومات تصنيف مشروع إدارة الوعاء'),
(326, 9, 'aidRepository.repositories.manage', 'إدارة الأوعية'),
(327, 9, 'aidRepository.repositories.create', 'إنشاء وعاء جديد'),
(328, 9, 'aidRepository.repositories.update', 'تحرير بيانات وعاء'),
(329, 9, 'aidRepository.repositories.delete', 'حذف وعاء'),
(330, 9, 'aidRepository.repositories.view', 'عرض معلومات الوعاء'),
(331, 9, 'aidRepository.projects.manage', 'إدارة مشاريع الوعاء'),
(332, 9, 'aidRepository.projects.create', 'إنشاء مشروع جديد في الوعاء'),
(333, 9, 'aidRepository.projects.update', 'تحرير بيانات مشروع'),
(334, 9, 'aidRepository.projects.delete', 'حذف مشروع من الوعاء'),
(335, 9, 'aidRepository.projects.view', 'عرض معلومات المشروع'),
(336, 9, 'aidRepository.organizationProjects.manage', 'مشاريع الوعاء الخاصة بالجمعية'),
(337, 9, 'aidRepository.organizationProjects.candidates', 'عرض المرشحين للمشروع في الجمعية'),
(338, 9, 'aidRepository.organizationProjects.nominate', 'ترشيح مستفيدين لمشروع في الوعاء'),
(339, 3, 'aid.category.upload', 'قوالب تصدير الاستمارات'),
(340, 4, 'sponsorship.category.upload', 'قوالب تصدير الاستمارات'),
(341, 4, 'sponsorship.SponsorCase.manage', 'إدارة الحالات المكفولة (حساب الكافل)'),
(342, 1, 'auth.users.permissionsUpdate', 'تحرير ومنح صلاحيات بشكل مباشر للمستخدم'),
(343, 1, 'log.logs.manage', 'مشاهدة سجلات العمليات والإجراءات'),
(349, 9, 'aidRepository.projects.updateCandidatesStatus', 'تعديل حالة مرشح للمشروع'),
(350, 9, 'aidRepository.projects.deleteCandidates', 'حذف مرشح للمشروع'),
(351, 7, 'setting.houseStatus.manage', 'إدارة خيارات حالة المنزل'),
(352, 7, 'setting.houseStatus.create', 'إنشاء خيار حالة منزل جديد'),
(353, 7, 'setting.houseStatus.update', 'تحرير سجل خيار حالة منزل'),
(354, 7, 'setting.houseStatus.delete', 'حذف سجل خيار حالة منزل'),
(355, 7, 'setting.houseStatus.view', 'عرض سجل خيار حالة منزل'),
(356, 7, 'setting.buildingStatus.manage', 'إدارة خيارات حالة بناء المنزل'),
(357, 7, 'setting.buildingStatus.create', 'إنشاء خيار حالة بناء المنزل جديد'),
(358, 7, 'setting.buildingStatus.update', 'تحرير سجل خيار حالة بناء منزل'),
(359, 7, 'setting.buildingStatus.delete', 'حذف سجل خيار حالة بناء منزل'),
(360, 7, 'setting.buildingStatus.view', 'عرض سجل خيار حالة بناء منزل'),
(361, 7, 'setting.furnitureStatus.manage', 'إدارة خيارات حالة الأثاث'),
(362, 7, 'setting.furnitureStatus.create', 'إنشاء خيار حالة أثاث جديد'),
(363, 7, 'setting.furnitureStatus.update', 'تحرير سجل خيار حالة أثاث'),
(364, 7, 'setting.furnitureStatus.delete', 'حذف سجل خيار حالة أثاث'),
(365, 7, 'setting.furnitureStatus.view', 'عرض سجل خيار حالة أثاث'),
(366, 7, 'setting.habitableStatus.manage', 'إدارة خيارات صلاحية المنزل للسكن'),
(367, 7, 'setting.habitableStatus.create', 'إنشاء خيار صلاحية المنزل للسكن جديد'),
(368, 7, 'setting.habitableStatus.update', 'تحرير سجل خيار صلاحية المنزل للسكن'),
(369, 7, 'setting.habitableStatus.delete', 'حذف سجل خيارصلاحية المنزل للسكن'),
(370, 7, 'setting.habitableStatus.view', 'عرض سجل خيار صلاحية المنزل للسكن');

-- --------------------------------------------------------

--
-- Table structure for table `char_persons`
--

CREATE TABLE `char_persons` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `third_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `prev_family_name` varchar(255) DEFAULT NULL,
  `id_card_number` varchar(9) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(45) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `death_date` date DEFAULT NULL,
  `death_cause_id` int(10) UNSIGNED DEFAULT NULL,
  `father_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'معرف الأب من جدول persons',
  `mother_id` int(11) UNSIGNED DEFAULT NULL COMMENT 'معرف الأم من جدول persons',
  `spouses` tinyint(1) DEFAULT NULL,
  `refugee` tinyint(4) DEFAULT NULL,
  `unrwa_card_number` varchar(45) DEFAULT NULL,
  `country` int(10) UNSIGNED DEFAULT NULL,
  `governarate` int(10) UNSIGNED DEFAULT NULL,
  `city` int(10) UNSIGNED DEFAULT NULL,
  `location_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المنطقة)',
  `street_address` varchar(255) DEFAULT NULL COMMENT 'العنوان (الشارع)',
  `monthly_income` float DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البيانات الشخصية';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_aids`
--

CREATE TABLE `char_persons_aids` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `aid_source_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الجهة المقدمة للمساعدة',
  `aid_type` tinyint(3) UNSIGNED NOT NULL COMMENT 'نوع المساعدة: 1=نقدية، 2=عينية',
  `aid_take` tinyint(1) NOT NULL,
  `aid_value` float DEFAULT NULL,
  `currency_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='المساعدات النقدية أو العينية الدائمة للشخص';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_banks`
--

CREATE TABLE `char_persons_banks` (
  `id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `bank_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `account_number` varchar(45) DEFAULT NULL COMMENT 'رقم الحساب',
  `account_owner` varchar(255) DEFAULT NULL COMMENT 'اسم مالك الحساب',
  `branch_name` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='معلومات الحسابات البنكية للشخص';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_contact`
--

CREATE TABLE `char_persons_contact` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `contact_type` varchar(255) NOT NULL,
  `contact_value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_documents`
--

CREATE TABLE `char_persons_documents` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `document_type_id` int(10) UNSIGNED NOT NULL,
  `document_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_education`
--

CREATE TABLE `char_persons_education` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `study` tinyint(1) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL COMMENT 'نوع الدراسة: 1 أكاديمي، 2 مهني',
  `authority` int(10) UNSIGNED DEFAULT NULL,
  `stage` int(10) UNSIGNED DEFAULT NULL,
  `specialization` varchar(255) DEFAULT NULL,
  `degree` int(10) UNSIGNED DEFAULT NULL,
  `grade` tinyint(4) DEFAULT NULL COMMENT 'الصف',
  `year` varchar(45) DEFAULT NULL COMMENT 'السنة الدراسية',
  `school` varchar(255) DEFAULT NULL COMMENT 'المدرسة/المؤسسة التعليمية',
  `level` tinyint(4) DEFAULT NULL COMMENT 'مستوى التحصيل: 1ضعيف، 2 مقبول، 3 جيد، 4 جيد جدا، 5 ممتاز',
  `points` float DEFAULT NULL COMMENT 'المعدل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول المستوى التعليمي للحالة (الشخص)';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_health`
--

CREATE TABLE `char_persons_health` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `condition` tinyint(4) DEFAULT NULL COMMENT 'الحالة: 1 جيدة، 2 يعاني من مرض مزمن، 3 من ذوي الاحتياجات الخاصة',
  `details` text COMMENT 'وصف نوع المرض أو الإعاقة',
  `disease_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الحالة الصحية للحالة (الشخص)';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_i18n`
--

CREATE TABLE `char_persons_i18n` (
  `person_id` int(11) UNSIGNED NOT NULL,
  `language_id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `third_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول معلومات الأشخاص مترجمة حسب اللغة';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_islamic_commitment`
--

CREATE TABLE `char_persons_islamic_commitment` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `prayer` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'الصلاة: 1 يواظب، 2 لا يصلي، 3 متقطع',
  `quran_center` tinyint(3) UNSIGNED DEFAULT NULL,
  `quran_parts` varchar(255) DEFAULT NULL COMMENT 'حفظ القرآن: عدد الأجزاء',
  `quran_chapters` varchar(255) DEFAULT NULL COMMENT 'حفظ القرآن: عدد السور',
  `prayer_reason` text COMMENT 'سبب ترك الصلاة',
  `quran_reason` text COMMENT 'سبب ترك حفظ القرآن'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الوضع الإسلامي للحالة (الشخص)';

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_kinship`
--

CREATE TABLE `char_persons_kinship` (
  `l_person_id` int(10) UNSIGNED NOT NULL,
  `r_person_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `kinship_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_properties`
--

CREATE TABLE `char_persons_properties` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `has_property` tinyint(1) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `notes` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_persons_work`
--

CREATE TABLE `char_persons_work` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `working` tinyint(1) UNSIGNED DEFAULT NULL,
  `can_work` tinyint(1) UNSIGNED DEFAULT NULL,
  `work_status_id` int(10) UNSIGNED DEFAULT NULL,
  `work_reason_id` int(10) UNSIGNED DEFAULT NULL,
  `work_job_id` int(10) UNSIGNED DEFAULT NULL,
  `work_wage_id` int(10) UNSIGNED DEFAULT NULL,
  `work_location` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_projects`
--

CREATE TABLE `char_projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الجمعية',
  `name` varchar(255) DEFAULT NULL COMMENT 'اسم المشروع',
  `start_date` date DEFAULT NULL COMMENT 'تاريخ بدء المشروع',
  `end_date` date DEFAULT NULL COMMENT 'تاريخ انتهاء المشروع',
  `notes` text,
  `status` tinyint(4) DEFAULT NULL COMMENT 'حالة المشروع: 1 = فعال، 2 = مغلق',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول المشاريع';

-- --------------------------------------------------------

--
-- Table structure for table `char_projects_persons`
--

CREATE TABLE `char_projects_persons` (
  `project_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول المستفيدين ضمن المشروع';

-- --------------------------------------------------------

--
-- Table structure for table `char_properties`
--

CREATE TABLE `char_properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_properties_i18n`
--

CREATE TABLE `char_properties_i18n` (
  `property_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_properties_weights`
--

CREATE TABLE `char_properties_weights` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL,
  `value_min` float UNSIGNED NOT NULL,
  `value_max` float UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_property_types`
--

CREATE TABLE `char_property_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_property_types_i18n`
--

CREATE TABLE `char_property_types_i18n` (
  `property_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_property_types_view`
--

CREATE TABLE `char_property_types_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_reconstructions`
--

CREATE TABLE `char_reconstructions` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `promised` tinyint(1) NOT NULL,
  `organization_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_residence`
--

CREATE TABLE `char_residence` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `property_type_id` int(10) UNSIGNED DEFAULT NULL,
  `rent_value` float DEFAULT NULL,
  `roof_material_id` int(10) UNSIGNED DEFAULT NULL,
  `residence_condition` tinyint(3) UNSIGNED DEFAULT NULL,
  `indoor_condition` tinyint(3) UNSIGNED DEFAULT NULL,
  `habitable` tinyint(3) UNSIGNED DEFAULT NULL,
  `house_condition` tinyint(3) UNSIGNED DEFAULT NULL,
  `rooms` smallint(5) UNSIGNED DEFAULT NULL,
  `area` float UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_roles`
--

CREATE TABLE `char_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مجموعات الصلاحيات';

--
-- Dumping data for table `char_roles`
--

INSERT INTO `char_roles` (`id`, `name`, `organization_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'مدير النظام', 1, '2016-09-18 09:32:07', NULL, NULL),
(2, 'مدير جمعية', 1, '2016-09-18 09:32:16', NULL, NULL),
(3, 'مدير قسم الكفالات', 1, '2016-12-17 14:12:25', NULL, NULL),
(4, 'مدير قسم المساعدات', 1, '2016-12-17 14:12:41', NULL, NULL),
(12, 'مدير قسم الكفالات', 9, '2017-05-31 16:24:28', NULL, NULL),
(13, 'مدير قسم الكفالات', 10, '2017-05-31 16:24:28', NULL, NULL),
(14, 'مدير قسم الكفالات', 11, '2017-05-31 16:24:28', NULL, NULL),
(15, 'مدير قسم الكفالات', 12, '2017-05-31 16:24:28', NULL, NULL),
(23, 'مدير قسم المساعدات', 9, '2017-05-31 16:25:00', NULL, NULL),
(24, 'مدير قسم المساعدات', 10, '2017-05-31 16:25:00', NULL, NULL),
(25, 'مدير قسم المساعدات', 11, '2017-05-31 16:25:00', NULL, NULL),
(26, 'مدير قسم المساعدات', 12, '2017-05-31 16:25:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_roles_permissions`
--

CREATE TABLE `char_roles_permissions` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `allow` tinyint(1) UNSIGNED DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_roles_permissions`
--

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(1, 1, 1, '2017-05-31 16:13:28', 1),
(1, 2, 1, '2017-05-31 16:13:28', 1),
(1, 3, 1, '2017-05-31 16:13:28', 1),
(1, 4, 1, '2017-05-31 16:13:28', 1),
(1, 5, 1, '2017-05-31 16:13:28', 1),
(1, 6, 1, '2017-05-31 16:13:28', 1),
(1, 7, 1, '2017-05-31 16:13:28', 1),
(1, 8, 1, '2017-05-31 16:13:28', 1),
(1, 9, 1, '2017-05-31 16:13:28', 1),
(1, 10, 1, '2017-05-31 16:13:28', 1),
(1, 11, 1, '2017-05-31 16:13:28', 1),
(1, 12, 1, '2017-05-31 16:13:28', 1),
(1, 13, 1, '2017-05-31 16:13:28', 1),
(1, 14, 1, '2017-05-31 16:13:28', 1),
(1, 15, 1, '2017-05-31 16:13:28', 1),
(1, 16, 1, '2017-05-31 16:13:28', 1),
(1, 17, 1, '2017-05-31 16:13:28', 1),
(1, 18, 1, '2017-05-31 16:13:28', 1),
(1, 19, 1, '2017-05-31 16:13:28', 1),
(1, 20, 1, '2017-05-31 16:13:28', 1),
(1, 21, 1, '2017-05-31 16:13:28', 1),
(1, 22, 1, '2017-05-31 16:13:28', 1),
(1, 23, 1, '2017-05-31 16:13:28', 1),
(1, 24, 1, '2017-05-31 16:13:28', 1),
(1, 25, 1, '2017-05-31 16:13:28', 1),
(1, 26, 1, '2017-05-31 16:13:28', 1),
(1, 27, 1, '2017-05-31 16:13:28', 1),
(1, 28, 1, '2017-05-31 16:13:28', 1),
(1, 29, 1, '2017-05-31 16:13:28', 1),
(1, 30, 1, '2017-05-31 16:13:28', 1),
(1, 31, 1, '2017-05-31 16:13:28', 1),
(1, 32, 1, '2017-05-31 16:13:28', 1),
(1, 33, 1, '2017-05-31 16:13:28', 1),
(1, 34, 1, '2017-05-31 16:13:28', 1),
(1, 35, 1, '2017-05-31 16:13:28', 1),
(1, 36, 1, '2017-05-31 16:13:28', 1),
(1, 37, 1, '2017-05-31 16:13:28', 1),
(1, 130, 1, '2017-05-31 16:13:28', 1),
(1, 132, 1, '2017-05-31 16:13:28', 1),
(1, 153, 1, '2017-05-31 16:13:28', 1),
(1, 154, 1, '2017-05-31 16:13:28', 1),
(1, 155, 1, '2017-05-31 16:13:28', 1),
(1, 156, 1, '2017-05-31 16:13:28', 1),
(1, 157, 1, '2017-05-31 16:13:28', 1),
(1, 158, 1, '2017-05-31 16:13:28', 1),
(1, 159, 1, '2017-05-31 16:13:28', 1),
(1, 160, 1, '2017-05-31 16:13:28', 1),
(1, 161, 1, '2017-05-31 16:13:28', 1),
(1, 162, 1, '2017-05-31 16:13:28', 1),
(1, 163, 1, '2017-05-31 16:13:28', 1),
(1, 164, 1, '2017-05-31 16:13:28', 1),
(1, 165, 1, '2017-05-31 16:13:28', 1),
(1, 166, 1, '2017-05-31 16:13:28', 1),
(1, 167, 1, '2017-05-31 16:13:28', 1),
(1, 168, 1, '2017-05-31 16:13:28', 1),
(1, 169, 1, '2017-05-31 16:13:28', 1),
(1, 170, 1, '2017-05-31 16:13:28', 1),
(1, 171, 1, '2017-05-31 16:13:28', 1),
(1, 172, 1, '2017-05-31 16:13:28', 1),
(1, 173, 1, '2017-05-31 16:13:28', 1),
(1, 174, 1, '2017-05-31 16:13:28', 1),
(1, 175, 1, '2017-05-31 16:13:28', 1),
(1, 176, 1, '2017-05-31 16:13:28', 1),
(1, 177, 1, '2017-05-31 16:13:28', 1),
(1, 178, 1, '2017-05-31 16:13:28', 1),
(1, 179, 1, '2017-05-31 16:13:28', 1),
(1, 180, 1, '2017-05-31 16:13:28', 1),
(1, 181, 1, '2017-05-31 16:13:28', 1),
(1, 182, 1, '2017-05-31 16:13:28', 1),
(1, 183, 1, '2017-05-31 16:13:28', 1),
(1, 184, 1, '2017-05-31 16:13:28', 1),
(1, 185, 1, '2017-05-31 16:13:28', 1),
(1, 186, 1, '2017-05-31 16:13:28', 1),
(1, 187, 1, '2017-05-31 16:13:28', 1),
(1, 188, 1, '2017-05-31 16:13:28', 1),
(1, 189, 1, '2017-05-31 16:13:28', 1),
(1, 190, 1, '2017-05-31 16:13:28', 1),
(1, 191, 1, '2017-05-31 16:13:28', 1),
(1, 192, 1, '2017-05-31 16:13:28', 1),
(1, 193, 1, '2017-05-31 16:13:28', 1),
(1, 194, 1, '2017-05-31 16:13:28', 1),
(1, 195, 1, '2017-05-31 16:13:28', 1),
(1, 196, 1, '2017-05-31 16:13:28', 1),
(1, 197, 1, '2017-05-31 16:13:28', 1),
(1, 198, 1, '2017-05-31 16:13:28', 1),
(1, 199, 1, '2017-05-31 16:13:28', 1),
(1, 200, 1, '2017-05-31 16:13:28', 1),
(1, 201, 1, '2017-05-31 16:13:28', 1),
(1, 202, 1, '2017-05-31 16:13:28', 1),
(1, 203, 1, '2017-05-31 16:13:28', 1),
(1, 204, 1, '2017-05-31 16:13:28', 1),
(1, 205, 1, '2017-05-31 16:13:28', 1),
(1, 206, 1, '2017-05-31 16:13:28', 1),
(1, 207, 1, '2017-05-31 16:13:28', 1),
(1, 208, 1, '2017-05-31 16:13:28', 1),
(1, 209, 1, '2017-05-31 16:13:28', 1),
(1, 210, 1, '2017-05-31 16:13:28', 1),
(1, 211, 1, '2017-05-31 16:13:28', 1),
(1, 212, 1, '2017-05-31 16:13:28', 1),
(1, 213, 1, '2017-05-31 16:13:28', 1),
(1, 214, 1, '2017-05-31 16:13:28', 1),
(1, 215, 1, '2017-05-31 16:13:28', 1),
(1, 222, 1, '2017-05-31 16:13:28', 1),
(1, 223, 1, '2017-05-31 16:13:28', 1),
(1, 224, 1, '2017-05-31 16:13:28', 1),
(1, 225, 1, '2017-05-31 16:13:28', 1),
(1, 226, 1, '2017-05-31 16:13:28', 1),
(1, 227, 1, '2017-05-31 16:13:28', 1),
(1, 228, 1, '2017-05-31 16:13:28', 1),
(1, 229, 1, '2017-05-31 16:13:28', 1),
(1, 230, 1, '2017-05-31 16:13:28', 1),
(1, 231, 1, '2017-05-31 16:13:28', 1),
(1, 232, 1, '2017-05-31 16:13:28', 1),
(1, 233, 1, '2017-05-31 16:13:28', 1),
(1, 234, 1, '2017-05-31 16:13:28', 1),
(1, 235, 1, '2017-05-31 16:13:28', 1),
(1, 236, 1, '2017-05-31 16:13:28', 1),
(1, 237, 1, '2017-05-31 16:13:28', 1),
(1, 238, 1, '2017-05-31 16:13:28', 1),
(1, 239, 1, '2017-05-31 16:13:28', 1),
(1, 240, 1, '2017-05-31 16:13:28', 1),
(1, 241, 1, '2017-05-31 16:13:28', 1),
(1, 242, 1, '2017-05-31 16:13:28', 1),
(1, 243, 1, '2017-05-31 16:13:28', 1),
(1, 244, 1, '2017-05-31 16:13:28', 1),
(1, 245, 1, '2017-05-31 16:13:28', 1),
(1, 246, 1, '2017-05-31 16:13:28', 1),
(1, 247, 1, '2017-05-31 16:13:28', 1),
(1, 248, 1, '2017-05-31 16:13:28', 1),
(1, 249, 1, '2017-05-31 16:13:28', 1),
(1, 250, 1, '2017-05-31 16:13:28', 1),
(1, 251, 1, '2017-05-31 16:13:28', 1),
(1, 252, 1, '2017-05-31 16:13:28', 1),
(1, 253, 1, '2017-05-31 16:13:28', 1),
(1, 254, 1, '2017-05-31 16:13:28', 1),
(1, 255, 1, '2017-05-31 16:13:28', 1),
(1, 256, 1, '2017-05-31 16:13:28', 1),
(1, 257, 1, '2017-05-31 16:13:28', 1),
(1, 270, 1, '2017-05-31 16:13:28', 1),
(1, 271, 1, '2017-05-31 16:13:28', 1),
(1, 272, 1, '2017-05-31 16:13:28', 1),
(1, 273, 1, '2017-05-31 16:13:28', 1),
(1, 274, 1, '2017-05-31 16:13:28', 1),
(1, 275, 1, '2017-05-31 16:13:28', 1),
(1, 276, 1, '2017-05-31 16:13:28', 1),
(1, 277, 1, '2017-05-31 16:13:28', 1),
(1, 278, 1, '2017-05-31 16:13:28', 1),
(1, 279, 1, '2017-05-31 16:13:28', 1),
(1, 280, 1, '2017-05-31 16:13:28', 1),
(1, 281, 1, '2017-05-31 16:13:28', 1),
(1, 282, 1, '2017-05-31 16:13:28', 1),
(1, 283, 1, '2017-05-31 16:13:28', 1),
(1, 284, 1, '2017-05-31 16:13:28', 1),
(1, 285, 1, '2017-05-31 16:13:28', 1),
(1, 286, 1, '2017-05-31 16:13:28', 1),
(1, 287, 1, '2017-05-31 16:13:28', 1),
(1, 288, 1, '2017-05-31 16:13:28', 1),
(1, 289, 1, '2017-05-31 16:13:28', 1),
(1, 290, 1, '2017-05-31 16:13:28', 1),
(1, 291, 1, '2017-05-31 16:13:28', 1),
(1, 292, 1, '2017-05-31 16:13:28', 1),
(1, 293, 1, '2017-05-31 16:13:28', 1),
(1, 294, 1, '2017-05-31 16:13:28', 1),
(1, 297, 1, '2017-05-31 16:13:28', 1),
(1, 298, 1, '2017-05-31 16:13:28', 1),
(1, 301, 1, '2017-05-31 16:13:28', 1),
(1, 302, 1, '2017-05-31 16:13:28', 1),
(1, 303, 1, '2017-05-31 16:13:28', 1),
(1, 304, 1, '2017-05-31 16:13:28', 1),
(1, 305, 1, '2017-05-31 16:13:28', 1),
(1, 311, 1, '2017-05-31 16:13:28', 1),
(1, 312, 1, '2017-05-31 16:13:28', 1),
(1, 313, 1, '2017-05-31 16:13:28', 1),
(1, 314, 1, '2017-05-31 16:13:28', 1),
(1, 315, 1, '2017-05-31 16:13:28', 1),
(1, 340, 1, '2017-05-31 16:13:28', 1),
(1, 341, 1, '2017-05-31 16:13:28', 1),
(2, 1, 1, '2017-09-10 18:45:18', 1),
(2, 2, 1, '2017-09-10 18:45:18', 1),
(2, 3, 1, '2017-09-10 18:45:18', 1),
(2, 4, 1, '2017-09-10 18:45:18', 1),
(2, 5, 1, '2017-09-10 18:45:18', 1),
(2, 6, 1, '2017-09-10 18:45:18', 1),
(2, 7, 1, '2017-09-10 18:45:18', 1),
(2, 8, 1, '2017-09-10 18:45:18', 1),
(2, 9, 1, '2017-09-10 18:45:18', 1),
(2, 10, 1, '2017-09-10 18:45:18', 1),
(2, 11, 1, '2017-09-10 18:45:18', 1),
(2, 12, 1, '2017-09-10 18:45:18', 1),
(2, 13, 1, '2017-09-10 18:45:18', 1),
(2, 14, 1, '2017-09-10 18:45:18', 1),
(2, 15, 1, '2017-09-10 18:45:18', 1),
(2, 16, 1, '2017-09-10 18:45:18', 1),
(2, 17, 1, '2017-09-10 18:45:18', 1),
(2, 18, 1, '2017-09-10 18:45:18', 1),
(2, 19, 1, '2017-09-10 18:45:18', 1),
(2, 20, 1, '2017-09-10 18:45:18', 1),
(2, 21, 1, '2017-09-10 18:45:18', 1),
(2, 22, 1, '2017-09-10 18:45:18', 1),
(2, 23, 1, '2017-09-10 18:45:18', 1),
(2, 24, 1, '2017-09-10 18:45:18', 1),
(2, 25, 1, '2017-09-10 18:45:18', 1),
(2, 26, 1, '2017-09-10 18:45:18', 1),
(2, 28, 1, '2017-09-10 18:45:18', 1),
(2, 30, 1, '2017-09-10 18:45:18', 1),
(2, 32, 1, '2017-09-10 18:45:18', 1),
(2, 34, 1, '2017-09-10 18:45:18', 1),
(2, 36, 1, '2017-09-10 18:45:18', 1),
(2, 37, 1, '2017-09-10 18:45:18', 1),
(2, 153, 1, '2017-09-10 18:45:18', 1),
(2, 154, 1, '2017-09-10 18:45:18', 1),
(2, 155, 1, '2017-09-10 18:45:18', 1),
(2, 156, 1, '2017-09-10 18:45:18', 1),
(2, 157, 1, '2017-09-10 18:45:18', 1),
(2, 158, 1, '2017-09-10 18:45:18', 1),
(2, 159, 1, '2017-09-10 18:45:18', 1),
(2, 160, 1, '2017-09-10 18:45:18', 1),
(2, 161, 1, '2017-09-10 18:45:18', 1),
(2, 162, 1, '2017-09-10 18:45:18', 1),
(2, 163, 1, '2017-09-10 18:45:18', 1),
(2, 164, 1, '2017-09-10 18:45:18', 1),
(2, 165, 1, '2017-09-10 18:45:18', 1),
(2, 166, 1, '2017-09-10 18:45:18', 1),
(2, 167, 1, '2017-09-10 18:45:18', 1),
(2, 168, 1, '2017-09-10 18:45:18', 1),
(2, 169, 1, '2017-09-10 18:45:18', 1),
(2, 170, 1, '2017-09-10 18:45:18', 1),
(2, 171, 1, '2017-09-10 18:45:18', 1),
(2, 172, 1, '2017-09-10 18:45:18', 1),
(2, 177, 1, '2017-09-10 18:45:18', 1),
(2, 178, 1, '2017-09-10 18:45:18', 1),
(2, 179, 1, '2017-09-10 18:45:18', 1),
(2, 180, 1, '2017-09-10 18:45:18', 1),
(2, 181, 1, '2017-09-10 18:45:18', 1),
(2, 182, 1, '2017-09-10 18:45:18', 1),
(2, 183, 1, '2017-09-10 18:45:18', 1),
(2, 184, 1, '2017-09-10 18:45:18', 1),
(2, 185, 1, '2017-09-10 18:45:18', 1),
(2, 186, 1, '2017-09-10 18:45:18', 1),
(2, 187, 1, '2017-09-10 18:45:18', 1),
(2, 188, 1, '2017-09-10 18:45:18', 1),
(2, 189, 1, '2017-09-10 18:45:18', 1),
(2, 190, 1, '2017-09-10 18:45:18', 1),
(2, 191, 1, '2017-09-10 18:45:18', 1),
(2, 192, 1, '2017-09-10 18:45:18', 1),
(2, 193, 1, '2017-09-10 18:45:18', 1),
(2, 194, 1, '2017-09-10 18:45:18', 1),
(2, 195, 1, '2017-09-10 18:45:18', 1),
(2, 196, 1, '2017-09-10 18:45:18', 1),
(2, 197, 1, '2017-09-10 18:45:18', 1),
(2, 198, 1, '2017-09-10 18:45:18', 1),
(2, 199, 1, '2017-09-10 18:45:18', 1),
(2, 201, 1, '2017-09-10 18:45:18', 1),
(2, 210, 1, '2017-09-10 18:45:18', 1),
(2, 211, 1, '2017-09-10 18:45:18', 1),
(2, 212, 1, '2017-09-10 18:45:18', 1),
(2, 213, 1, '2017-09-10 18:45:18', 1),
(2, 214, 1, '2017-09-10 18:45:18', 1),
(2, 215, 1, '2017-09-10 18:45:18', 1),
(2, 222, 1, '2017-09-10 18:45:18', 1),
(2, 223, 1, '2017-09-10 18:45:18', 1),
(2, 224, 1, '2017-09-10 18:45:18', 1),
(2, 225, 1, '2017-09-10 18:45:18', 1),
(2, 226, 1, '2017-09-10 18:45:18', 1),
(2, 227, 1, '2017-09-10 18:45:18', 1),
(2, 228, 1, '2017-09-10 18:45:18', 1),
(2, 229, 1, '2017-09-10 18:45:18', 1),
(2, 230, 1, '2017-09-10 18:45:18', 1),
(2, 231, 1, '2017-09-10 18:45:18', 1),
(2, 232, 1, '2017-09-10 18:45:18', 1),
(2, 233, 1, '2017-09-10 18:45:18', 1),
(2, 234, 1, '2017-09-10 18:45:18', 1),
(2, 235, 1, '2017-09-10 18:45:18', 1),
(2, 236, 1, '2017-09-10 18:45:18', 1),
(2, 237, 1, '2017-09-10 18:45:18', 1),
(2, 238, 1, '2017-09-10 18:45:18', 1),
(2, 239, 1, '2017-09-10 18:45:18', 1),
(2, 240, 1, '2017-09-10 18:45:18', 1),
(2, 241, 1, '2017-09-10 18:45:18', 1),
(2, 242, 1, '2017-09-10 18:45:18', 1),
(2, 243, 1, '2017-09-10 18:45:18', 1),
(2, 244, 1, '2017-09-10 18:45:18', 1),
(2, 245, 1, '2017-09-10 18:45:18', 1),
(2, 246, 1, '2017-09-10 18:45:18', 1),
(2, 247, 1, '2017-09-10 18:45:18', 1),
(2, 248, 1, '2017-09-10 18:45:18', 1),
(2, 249, 1, '2017-09-10 18:45:18', 1),
(2, 250, 1, '2017-09-10 18:45:18', 1),
(2, 251, 1, '2017-09-10 18:45:18', 1),
(2, 252, 1, '2017-09-10 18:45:18', 1),
(2, 254, 1, '2017-09-10 18:45:18', 1),
(2, 255, 1, '2017-09-10 18:45:18', 1),
(2, 256, 1, '2017-09-10 18:45:18', 1),
(2, 257, 1, '2017-09-10 18:45:18', 1),
(2, 275, 1, '2017-09-10 18:45:18', 1),
(2, 276, 1, '2017-09-10 18:45:18', 1),
(2, 282, 1, '2017-09-10 18:45:18', 1),
(2, 283, 1, '2017-09-10 18:45:18', 1),
(2, 284, 1, '2017-09-10 18:45:18', 1),
(2, 285, 1, '2017-09-10 18:45:18', 1),
(2, 286, 1, '2017-09-10 18:45:18', 1),
(2, 287, 1, '2017-09-10 18:45:18', 1),
(2, 288, 1, '2017-09-10 18:45:18', 1),
(2, 289, 1, '2017-09-10 18:45:18', 1),
(2, 290, 1, '2017-09-10 18:45:18', 1),
(2, 291, 1, '2017-09-10 18:45:18', 1),
(2, 292, 1, '2017-09-10 18:45:18', 1),
(2, 293, 1, '2017-09-10 18:45:18', 1),
(2, 294, 1, '2017-09-10 18:45:18', 1),
(2, 297, 1, '2017-09-10 18:45:18', 1),
(2, 298, 1, '2017-09-10 18:45:18', 1),
(2, 299, 1, '2017-09-10 18:45:18', 1),
(2, 300, 1, '2017-09-10 18:45:18', 1),
(2, 301, 1, '2017-09-10 18:45:18', 1),
(2, 302, 1, '2017-09-10 18:45:18', 1),
(2, 303, 1, '2017-09-10 18:45:18', 1),
(2, 304, 1, '2017-09-10 18:45:18', 1),
(2, 305, 1, '2017-09-10 18:45:18', 1),
(2, 306, 1, '2017-09-10 18:45:18', 1),
(2, 307, 1, '2017-09-10 18:45:18', 1),
(2, 308, 1, '2017-09-10 18:45:18', 1),
(2, 309, 1, '2017-09-10 18:45:18', 1),
(2, 310, 1, '2017-09-10 18:45:18', 1),
(2, 311, 1, '2017-09-10 18:45:18', 1),
(2, 312, 1, '2017-09-10 18:45:18', 1),
(2, 313, 1, '2017-09-10 18:45:18', 1),
(2, 314, 1, '2017-09-10 18:45:18', 1),
(2, 315, 1, '2017-09-10 18:45:18', 1),
(2, 316, 1, '2017-09-10 18:45:18', 1),
(2, 317, 1, '2017-09-10 18:45:18', 1),
(2, 318, 1, '2017-09-10 18:45:18', 1),
(2, 319, 1, '2017-09-10 18:45:18', 1),
(2, 320, 1, '2017-09-10 18:45:18', 1),
(2, 335, 1, '2017-09-10 18:45:18', 1),
(2, 336, 1, '2017-09-10 18:45:18', 1),
(2, 337, 1, '2017-09-10 18:45:18', 1),
(2, 338, 1, '2017-09-10 18:45:18', 1),
(2, 339, 1, '2017-09-10 18:45:18', 1),
(2, 340, 1, '2017-09-10 18:45:18', 1),
(2, 341, 0, '2017-09-10 18:45:18', 1),
(2, 342, 1, '2017-09-10 18:45:18', 1),
(2, 343, 1, '2017-09-10 18:45:18', 1),
(3, 37, 1, '2017-05-31 16:13:28', 1),
(3, 210, 1, '2017-05-31 16:13:28', 1),
(3, 211, 1, '2017-05-31 16:13:28', 1),
(3, 212, 1, '2017-05-31 16:13:28', 1),
(3, 213, 1, '2017-05-31 16:13:28', 1),
(3, 214, 1, '2017-05-31 16:13:28', 1),
(3, 215, 1, '2017-05-31 16:13:28', 1),
(3, 222, 1, '2017-05-31 16:13:28', 1),
(3, 223, 1, '2017-05-31 16:13:28', 1),
(3, 224, 1, '2017-05-31 16:13:28', 1),
(3, 225, 1, '2017-05-31 16:13:28', 1),
(3, 226, 1, '2017-05-31 16:13:28', 1),
(3, 227, 1, '2017-05-31 16:13:28', 1),
(3, 228, 1, '2017-05-31 16:13:28', 1),
(3, 229, 1, '2017-05-31 16:13:28', 1),
(3, 230, 1, '2017-05-31 16:13:28', 1),
(3, 231, 1, '2017-05-31 16:13:28', 1),
(3, 232, 1, '2017-05-31 16:13:28', 1),
(3, 233, 1, '2017-05-31 16:13:28', 1),
(3, 234, 1, '2017-05-31 16:13:28', 1),
(3, 235, 1, '2017-05-31 16:13:28', 1),
(3, 236, 1, '2017-05-31 16:13:28', 1),
(3, 237, 1, '2017-05-31 16:13:28', 1),
(3, 238, 1, '2017-05-31 16:13:28', 1),
(3, 239, 1, '2017-05-31 16:13:28', 1),
(3, 249, 1, '2017-05-31 16:13:28', 1),
(3, 250, 1, '2017-05-31 16:13:28', 1),
(3, 251, 1, '2017-05-31 16:13:28', 1),
(3, 252, 1, '2017-05-31 16:13:28', 1),
(3, 282, 1, '2017-05-31 16:13:28', 1),
(3, 287, 1, '2017-05-31 16:13:28', 1),
(3, 288, 1, '2017-05-31 16:13:28', 1),
(3, 289, 1, '2017-05-31 16:13:28', 1),
(3, 290, 1, '2017-05-31 16:13:28', 1),
(3, 292, 1, '2017-05-31 16:13:28', 1),
(3, 293, 1, '2017-05-31 16:13:28', 1),
(3, 294, 1, '2017-05-31 16:13:28', 1),
(3, 297, 1, '2017-05-31 16:13:28', 1),
(3, 298, 1, '2017-05-31 16:13:28', 1),
(4, 37, 1, '2017-05-31 16:13:28', 1),
(4, 178, 1, '2017-05-31 16:13:28', 1),
(4, 179, 1, '2017-05-31 16:13:28', 1),
(4, 180, 1, '2017-05-31 16:13:28', 1),
(4, 181, 1, '2017-05-31 16:13:28', 1),
(4, 182, 1, '2017-05-31 16:13:28', 1),
(4, 188, 1, '2017-05-31 16:13:28', 1),
(4, 189, 1, '2017-05-31 16:13:28', 1),
(4, 190, 1, '2017-05-31 16:13:28', 1),
(4, 191, 1, '2017-05-31 16:13:28', 1),
(4, 192, 1, '2017-05-31 16:13:28', 1),
(4, 193, 1, '2017-05-31 16:13:28', 1),
(4, 194, 1, '2017-05-31 16:13:28', 1),
(4, 195, 1, '2017-05-31 16:13:28', 1),
(4, 196, 1, '2017-05-31 16:13:28', 1),
(4, 197, 1, '2017-05-31 16:13:28', 1),
(4, 198, 1, '2017-05-31 16:13:28', 1),
(4, 199, 1, '2017-05-31 16:13:28', 1),
(4, 254, 1, '2017-05-31 16:13:28', 1),
(4, 255, 1, '2017-05-31 16:13:28', 1),
(4, 256, 1, '2017-05-31 16:13:28', 1),
(4, 257, 1, '2017-05-31 16:13:28', 1),
(4, 275, 1, '2017-05-31 16:13:28', 1),
(4, 276, 1, '2017-05-31 16:13:28', 1),
(4, 283, 1, '2017-05-31 16:13:28', 1),
(4, 284, 1, '2017-05-31 16:13:28', 1),
(4, 285, 1, '2017-05-31 16:13:28', 1),
(4, 286, 1, '2017-05-31 16:13:28', 1),
(4, 291, 1, '2017-05-31 16:13:28', 1),
(12, 37, 1, '2017-05-31 16:27:12', NULL),
(12, 210, 1, '2017-05-31 16:27:12', NULL),
(12, 211, 1, '2017-05-31 16:27:12', NULL),
(12, 212, 1, '2017-05-31 16:27:12', NULL),
(12, 213, 1, '2017-05-31 16:27:12', NULL),
(12, 214, 1, '2017-05-31 16:27:12', NULL),
(12, 215, 1, '2017-05-31 16:27:12', NULL),
(12, 222, 1, '2017-05-31 16:27:12', NULL),
(12, 223, 1, '2017-05-31 16:27:12', NULL),
(12, 224, 1, '2017-05-31 16:27:12', NULL),
(12, 225, 1, '2017-05-31 16:27:12', NULL),
(12, 226, 1, '2017-05-31 16:27:12', NULL),
(12, 227, 1, '2017-05-31 16:27:12', NULL),
(12, 228, 1, '2017-05-31 16:27:12', NULL),
(12, 229, 1, '2017-05-31 16:27:12', NULL),
(12, 230, 1, '2017-05-31 16:27:12', NULL),
(12, 231, 1, '2017-05-31 16:27:12', NULL),
(12, 232, 1, '2017-05-31 16:27:12', NULL),
(12, 233, 1, '2017-05-31 16:27:12', NULL),
(12, 234, 1, '2017-05-31 16:27:12', NULL),
(12, 235, 1, '2017-05-31 16:27:12', NULL),
(12, 236, 1, '2017-05-31 16:27:12', NULL),
(12, 237, 1, '2017-05-31 16:27:12', NULL),
(12, 238, 1, '2017-05-31 16:27:12', NULL),
(12, 239, 1, '2017-05-31 16:27:12', NULL),
(12, 249, 1, '2017-05-31 16:27:12', NULL),
(12, 250, 1, '2017-05-31 16:27:12', NULL),
(12, 251, 1, '2017-05-31 16:27:12', NULL),
(12, 252, 1, '2017-05-31 16:27:12', NULL),
(12, 282, 1, '2017-05-31 16:27:12', NULL),
(12, 287, 1, '2017-05-31 16:27:12', NULL),
(12, 288, 1, '2017-05-31 16:27:12', NULL),
(12, 289, 1, '2017-05-31 16:27:12', NULL),
(12, 290, 1, '2017-05-31 16:27:12', NULL),
(12, 292, 1, '2017-05-31 16:27:12', NULL),
(12, 293, 1, '2017-05-31 16:27:12', NULL),
(12, 294, 1, '2017-05-31 16:27:12', NULL),
(12, 297, 1, '2017-05-31 16:27:12', NULL),
(12, 298, 1, '2017-05-31 16:27:12', NULL),
(13, 37, 1, '2017-05-31 16:27:17', NULL),
(13, 210, 1, '2017-05-31 16:27:17', NULL),
(13, 211, 1, '2017-05-31 16:27:17', NULL),
(13, 212, 1, '2017-05-31 16:27:17', NULL),
(13, 213, 1, '2017-05-31 16:27:17', NULL),
(13, 214, 1, '2017-05-31 16:27:17', NULL),
(13, 215, 1, '2017-05-31 16:27:17', NULL),
(13, 222, 1, '2017-05-31 16:27:17', NULL),
(13, 223, 1, '2017-05-31 16:27:17', NULL),
(13, 224, 1, '2017-05-31 16:27:17', NULL),
(13, 225, 1, '2017-05-31 16:27:17', NULL),
(13, 226, 1, '2017-05-31 16:27:17', NULL),
(13, 227, 1, '2017-05-31 16:27:17', NULL),
(13, 228, 1, '2017-05-31 16:27:17', NULL),
(13, 229, 1, '2017-05-31 16:27:17', NULL),
(13, 230, 1, '2017-05-31 16:27:17', NULL),
(13, 231, 1, '2017-05-31 16:27:17', NULL),
(13, 232, 1, '2017-05-31 16:27:17', NULL),
(13, 233, 1, '2017-05-31 16:27:17', NULL),
(13, 234, 1, '2017-05-31 16:27:17', NULL),
(13, 235, 1, '2017-05-31 16:27:17', NULL),
(13, 236, 1, '2017-05-31 16:27:17', NULL),
(13, 237, 1, '2017-05-31 16:27:17', NULL),
(13, 238, 1, '2017-05-31 16:27:17', NULL),
(13, 239, 1, '2017-05-31 16:27:17', NULL),
(13, 249, 1, '2017-05-31 16:27:17', NULL),
(13, 250, 1, '2017-05-31 16:27:17', NULL),
(13, 251, 1, '2017-05-31 16:27:17', NULL),
(13, 252, 1, '2017-05-31 16:27:17', NULL),
(13, 282, 1, '2017-05-31 16:27:17', NULL),
(13, 287, 1, '2017-05-31 16:27:17', NULL),
(13, 288, 1, '2017-05-31 16:27:17', NULL),
(13, 289, 1, '2017-05-31 16:27:17', NULL),
(13, 290, 1, '2017-05-31 16:27:17', NULL),
(13, 292, 1, '2017-05-31 16:27:17', NULL),
(13, 293, 1, '2017-05-31 16:27:17', NULL),
(13, 294, 1, '2017-05-31 16:27:17', NULL),
(13, 297, 1, '2017-05-31 16:27:17', NULL),
(13, 298, 1, '2017-05-31 16:27:17', NULL),
(14, 37, 1, '2017-05-31 16:27:23', NULL),
(14, 210, 1, '2017-05-31 16:27:23', NULL),
(14, 211, 1, '2017-05-31 16:27:23', NULL),
(14, 212, 1, '2017-05-31 16:27:23', NULL),
(14, 213, 1, '2017-05-31 16:27:23', NULL),
(14, 214, 1, '2017-05-31 16:27:23', NULL),
(14, 215, 1, '2017-05-31 16:27:23', NULL),
(14, 222, 1, '2017-05-31 16:27:23', NULL),
(14, 223, 1, '2017-05-31 16:27:23', NULL),
(14, 224, 1, '2017-05-31 16:27:23', NULL),
(14, 225, 1, '2017-05-31 16:27:23', NULL),
(14, 226, 1, '2017-05-31 16:27:23', NULL),
(14, 227, 1, '2017-05-31 16:27:23', NULL),
(14, 228, 1, '2017-05-31 16:27:23', NULL),
(14, 229, 1, '2017-05-31 16:27:23', NULL),
(14, 230, 1, '2017-05-31 16:27:23', NULL),
(14, 231, 1, '2017-05-31 16:27:23', NULL),
(14, 232, 1, '2017-05-31 16:27:23', NULL),
(14, 233, 1, '2017-05-31 16:27:23', NULL),
(14, 234, 1, '2017-05-31 16:27:23', NULL),
(14, 235, 1, '2017-05-31 16:27:23', NULL),
(14, 236, 1, '2017-05-31 16:27:23', NULL),
(14, 237, 1, '2017-05-31 16:27:23', NULL),
(14, 238, 1, '2017-05-31 16:27:23', NULL),
(14, 239, 1, '2017-05-31 16:27:23', NULL),
(14, 249, 1, '2017-05-31 16:27:23', NULL),
(14, 250, 1, '2017-05-31 16:27:23', NULL),
(14, 251, 1, '2017-05-31 16:27:23', NULL),
(14, 252, 1, '2017-05-31 16:27:23', NULL),
(14, 282, 1, '2017-05-31 16:27:23', NULL),
(14, 287, 1, '2017-05-31 16:27:23', NULL),
(14, 288, 1, '2017-05-31 16:27:23', NULL),
(14, 289, 1, '2017-05-31 16:27:23', NULL),
(14, 290, 1, '2017-05-31 16:27:23', NULL),
(14, 292, 1, '2017-05-31 16:27:23', NULL),
(14, 293, 1, '2017-05-31 16:27:23', NULL),
(14, 294, 1, '2017-05-31 16:27:23', NULL),
(14, 297, 1, '2017-05-31 16:27:23', NULL),
(14, 298, 1, '2017-05-31 16:27:23', NULL),
(15, 37, 1, '2017-05-31 16:27:29', NULL),
(15, 210, 1, '2017-05-31 16:27:29', NULL),
(15, 211, 1, '2017-05-31 16:27:29', NULL),
(15, 212, 1, '2017-05-31 16:27:29', NULL),
(15, 213, 1, '2017-05-31 16:27:29', NULL),
(15, 214, 1, '2017-05-31 16:27:29', NULL),
(15, 215, 1, '2017-05-31 16:27:29', NULL),
(15, 222, 1, '2017-05-31 16:27:29', NULL),
(15, 223, 1, '2017-05-31 16:27:29', NULL),
(15, 224, 1, '2017-05-31 16:27:29', NULL),
(15, 225, 1, '2017-05-31 16:27:29', NULL),
(15, 226, 1, '2017-05-31 16:27:29', NULL),
(15, 227, 1, '2017-05-31 16:27:29', NULL),
(15, 228, 1, '2017-05-31 16:27:29', NULL),
(15, 229, 1, '2017-05-31 16:27:29', NULL),
(15, 230, 1, '2017-05-31 16:27:29', NULL),
(15, 231, 1, '2017-05-31 16:27:29', NULL),
(15, 232, 1, '2017-05-31 16:27:29', NULL),
(15, 233, 1, '2017-05-31 16:27:29', NULL),
(15, 234, 1, '2017-05-31 16:27:29', NULL),
(15, 235, 1, '2017-05-31 16:27:29', NULL),
(15, 236, 1, '2017-05-31 16:27:29', NULL),
(15, 237, 1, '2017-05-31 16:27:29', NULL),
(15, 238, 1, '2017-05-31 16:27:29', NULL),
(15, 239, 1, '2017-05-31 16:27:29', NULL),
(15, 249, 1, '2017-05-31 16:27:29', NULL),
(15, 250, 1, '2017-05-31 16:27:29', NULL),
(15, 251, 1, '2017-05-31 16:27:29', NULL),
(15, 252, 1, '2017-05-31 16:27:29', NULL),
(15, 282, 1, '2017-05-31 16:27:29', NULL),
(15, 287, 1, '2017-05-31 16:27:29', NULL),
(15, 288, 1, '2017-05-31 16:27:29', NULL),
(15, 289, 1, '2017-05-31 16:27:29', NULL),
(15, 290, 1, '2017-05-31 16:27:29', NULL),
(15, 292, 1, '2017-05-31 16:27:29', NULL),
(15, 293, 1, '2017-05-31 16:27:29', NULL),
(15, 294, 1, '2017-05-31 16:27:29', NULL),
(15, 297, 1, '2017-05-31 16:27:29', NULL),
(15, 298, 1, '2017-05-31 16:27:29', NULL),
(23, 37, 1, '2017-05-31 16:28:04', NULL),
(23, 178, 1, '2017-05-31 16:28:04', NULL),
(23, 179, 1, '2017-05-31 16:28:04', NULL),
(23, 180, 1, '2017-05-31 16:28:04', NULL),
(23, 181, 1, '2017-05-31 16:28:04', NULL),
(23, 182, 1, '2017-05-31 16:28:04', NULL),
(23, 188, 1, '2017-05-31 16:28:04', NULL),
(23, 189, 1, '2017-05-31 16:28:04', NULL),
(23, 190, 1, '2017-05-31 16:28:04', NULL),
(23, 191, 1, '2017-05-31 16:28:04', NULL),
(23, 192, 1, '2017-05-31 16:28:04', NULL),
(23, 193, 1, '2017-05-31 16:28:04', NULL),
(23, 194, 1, '2017-05-31 16:28:04', NULL),
(23, 195, 1, '2017-05-31 16:28:04', NULL),
(23, 196, 1, '2017-05-31 16:28:04', NULL),
(23, 197, 1, '2017-05-31 16:28:04', NULL),
(23, 198, 1, '2017-05-31 16:28:04', NULL),
(23, 199, 1, '2017-05-31 16:28:04', NULL),
(23, 254, 1, '2017-05-31 16:28:04', NULL),
(23, 255, 1, '2017-05-31 16:28:04', NULL),
(23, 256, 1, '2017-05-31 16:28:04', NULL),
(23, 257, 1, '2017-05-31 16:28:04', NULL),
(23, 275, 1, '2017-05-31 16:28:04', NULL),
(23, 276, 1, '2017-05-31 16:28:04', NULL),
(23, 283, 1, '2017-05-31 16:28:04', NULL),
(23, 284, 1, '2017-05-31 16:28:04', NULL),
(23, 285, 1, '2017-05-31 16:28:04', NULL),
(23, 286, 1, '2017-05-31 16:28:04', NULL),
(23, 291, 1, '2017-05-31 16:28:04', NULL),
(24, 37, 1, '2017-05-31 16:28:08', NULL),
(24, 178, 1, '2017-05-31 16:28:08', NULL),
(24, 179, 1, '2017-05-31 16:28:08', NULL),
(24, 180, 1, '2017-05-31 16:28:08', NULL),
(24, 181, 1, '2017-05-31 16:28:08', NULL),
(24, 182, 1, '2017-05-31 16:28:08', NULL),
(24, 188, 1, '2017-05-31 16:28:08', NULL),
(24, 189, 1, '2017-05-31 16:28:08', NULL),
(24, 190, 1, '2017-05-31 16:28:08', NULL),
(24, 191, 1, '2017-05-31 16:28:08', NULL),
(24, 192, 1, '2017-05-31 16:28:08', NULL),
(24, 193, 1, '2017-05-31 16:28:08', NULL),
(24, 194, 1, '2017-05-31 16:28:08', NULL),
(24, 195, 1, '2017-05-31 16:28:08', NULL),
(24, 196, 1, '2017-05-31 16:28:08', NULL),
(24, 197, 1, '2017-05-31 16:28:08', NULL),
(24, 198, 1, '2017-05-31 16:28:08', NULL),
(24, 199, 1, '2017-05-31 16:28:08', NULL),
(24, 254, 1, '2017-05-31 16:28:08', NULL),
(24, 255, 1, '2017-05-31 16:28:08', NULL),
(24, 256, 1, '2017-05-31 16:28:08', NULL),
(24, 257, 1, '2017-05-31 16:28:08', NULL),
(24, 275, 1, '2017-05-31 16:28:08', NULL),
(24, 276, 1, '2017-05-31 16:28:08', NULL),
(24, 283, 1, '2017-05-31 16:28:08', NULL),
(24, 284, 1, '2017-05-31 16:28:08', NULL),
(24, 285, 1, '2017-05-31 16:28:08', NULL),
(24, 286, 1, '2017-05-31 16:28:08', NULL),
(24, 291, 1, '2017-05-31 16:28:08', NULL),
(25, 37, 1, '2017-05-31 16:28:11', NULL),
(25, 178, 1, '2017-05-31 16:28:11', NULL),
(25, 179, 1, '2017-05-31 16:28:11', NULL),
(25, 180, 1, '2017-05-31 16:28:11', NULL),
(25, 181, 1, '2017-05-31 16:28:11', NULL),
(25, 182, 1, '2017-05-31 16:28:11', NULL),
(25, 188, 1, '2017-05-31 16:28:11', NULL),
(25, 189, 1, '2017-05-31 16:28:11', NULL),
(25, 190, 1, '2017-05-31 16:28:11', NULL),
(25, 191, 1, '2017-05-31 16:28:11', NULL),
(25, 192, 1, '2017-05-31 16:28:11', NULL),
(25, 193, 1, '2017-05-31 16:28:11', NULL),
(25, 194, 1, '2017-05-31 16:28:11', NULL),
(25, 195, 1, '2017-05-31 16:28:11', NULL),
(25, 196, 1, '2017-05-31 16:28:11', NULL),
(25, 197, 1, '2017-05-31 16:28:11', NULL),
(25, 198, 1, '2017-05-31 16:28:11', NULL),
(25, 199, 1, '2017-05-31 16:28:11', NULL),
(25, 254, 1, '2017-05-31 16:28:11', NULL),
(25, 255, 1, '2017-05-31 16:28:11', NULL),
(25, 256, 1, '2017-05-31 16:28:11', NULL),
(25, 257, 1, '2017-05-31 16:28:11', NULL),
(25, 275, 1, '2017-05-31 16:28:11', NULL),
(25, 276, 1, '2017-05-31 16:28:11', NULL),
(25, 283, 1, '2017-05-31 16:28:11', NULL),
(25, 284, 1, '2017-05-31 16:28:11', NULL),
(25, 285, 1, '2017-05-31 16:28:11', NULL),
(25, 286, 1, '2017-05-31 16:28:11', NULL),
(25, 291, 1, '2017-05-31 16:28:11', NULL),
(26, 37, 1, '2017-05-31 16:28:21', NULL),
(26, 178, 1, '2017-05-31 16:28:21', NULL),
(26, 179, 1, '2017-05-31 16:28:21', NULL),
(26, 180, 1, '2017-05-31 16:28:21', NULL),
(26, 181, 1, '2017-05-31 16:28:21', NULL),
(26, 182, 1, '2017-05-31 16:28:21', NULL),
(26, 188, 1, '2017-05-31 16:28:21', NULL),
(26, 189, 1, '2017-05-31 16:28:21', NULL),
(26, 190, 1, '2017-05-31 16:28:21', NULL),
(26, 191, 1, '2017-05-31 16:28:21', NULL),
(26, 192, 1, '2017-05-31 16:28:21', NULL),
(26, 193, 1, '2017-05-31 16:28:21', NULL),
(26, 194, 1, '2017-05-31 16:28:21', NULL),
(26, 195, 1, '2017-05-31 16:28:21', NULL),
(26, 196, 1, '2017-05-31 16:28:21', NULL),
(26, 197, 1, '2017-05-31 16:28:21', NULL),
(26, 198, 1, '2017-05-31 16:28:21', NULL),
(26, 199, 1, '2017-05-31 16:28:21', NULL),
(26, 254, 1, '2017-05-31 16:28:21', NULL),
(26, 255, 1, '2017-05-31 16:28:21', NULL),
(26, 256, 1, '2017-05-31 16:28:21', NULL),
(26, 257, 1, '2017-05-31 16:28:21', NULL),
(26, 275, 1, '2017-05-31 16:28:21', NULL),
(26, 276, 1, '2017-05-31 16:28:21', NULL),
(26, 283, 1, '2017-05-31 16:28:21', NULL),
(26, 284, 1, '2017-05-31 16:28:21', NULL),
(26, 285, 1, '2017-05-31 16:28:21', NULL),
(26, 286, 1, '2017-05-31 16:28:21', NULL),
(26, 291, 1, '2017-05-31 16:28:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_roof_materials`
--

CREATE TABLE `char_roof_materials` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float DEFAULT NULL COMMENT 'الوزن لحساب نقاط الاستمارة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على أنواع اسقف المنازل';

-- --------------------------------------------------------

--
-- Table structure for table `char_roof_materials_i18n`
--

CREATE TABLE `char_roof_materials_i18n` (
  `roof_material_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على اسماء أنواع اسقف المنازل حسب اللغة';

-- --------------------------------------------------------

--
-- Table structure for table `char_roof_materials_view`
--

CREATE TABLE `char_roof_materials_view` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_settings`
--

CREATE TABLE `char_settings` (
  `id` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول للإعدادت العامة على مستوى النظام أو الجمعية';

--
-- Dumping data for table `char_settings`
--

INSERT INTO `char_settings` (`id`, `value`, `organization_id`) VALUES
('aid-default-template', 'templates/361080cb47122c4647101e189309019d.docx', 1),
('aid_custom_form', '40', 1),
('aid_custom_form', '39', 14),
('aid_custom_form', '37', 16),
('auth_throttlesLogins_decayMinutes', '0', 0),
('auth_throttlesLogins_maxAttempts', '5', 0),
('daughter_kinship', '20', 1),
('default-cheque-template', 'templates/475a5e18370713dbe08e9f2055268c8a.docx', 14),
('default-voucher-template', 'templates/984cb023bd9ff01484cfef5732a2e08b.docx', 1),
('default-voucher-template', 'templates/984cb023bd9ff01484cfef5732a2e08b.docx', 19),
('entities', 'max_organization_share,max_total_amount_of_payments,father_kinship,mother_kinship,son_kinship,daughter_kinship,wife_kinship,husband_kinship', 1),
('father_kinship', '2', 1),
('husband_kinship', '23', 1),
('max_organization_share', '0.2', 1),
('max_total_amount_of_payments', '8000', 1),
('max_total_amount_of_voucher', '400', 1),
('mother_kinship', '3', 1),
('son_kinship', '21', 1),
('sponsorship_custom_form', '41', 1),
('sponsorship_custom_form', '3', 9),
('sponsorship_custom_form', '39', 14),
('sponsorship_custom_form', '34', 16),
('wife_kinship', '22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `char_sms_providers`
--

CREATE TABLE `char_sms_providers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `Label` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `api_url` varchar(255) DEFAULT NULL,
  `api_sms_balance_url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `char_sms_providers`
--

INSERT INTO `char_sms_providers` (`id`, `name`, `Label`, `status`, `api_url`, `api_sms_balance_url`) VALUES
(1, 'nepras', 'شركة نبراس', 1, 'http://www.nsms.ps/api.php?comm=sendsms&user=$username&pass=$password&to=$mobile_number&message=$message&sender=$sender', 'http://www.nsms.ps/api.php?comm=chk_balance&user=$username&pass=$password'),
(2, 'Alquds', 'شركة القدس', 1, NULL, NULL),
(3, 'wordLinux', 'ووردلينكس', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsorships`
--

CREATE TABLE `char_sponsorships` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الجهة الكافلة المرشح لها المستفيد',
  `sponsor_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `sponsorship_number` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'حالة الكفالة 1- مرشح',
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول ترشيح الكفالات وتتبع حالتها';

--
-- Triggers `char_sponsorships`
--
DELIMITER $$
CREATE TRIGGER `sponsorships_number_insert` BEFORE INSERT ON `char_sponsorships` FOR EACH ROW BEGIN 
declare v_count int; declare v_year int;
set v_year = year(now());
set v_count = (SELECT ifnull(MAX(`sponsorship_number`), 0) + 1 FROM `char_sponsorships` WHERE substring(`sponsorship_number`, 1, 4) = v_year);
if v_count = 1 then
set NEW.sponsorship_number = concat(v_year, lpad(v_count, 4, '0'));
else
set NEW.sponsorship_number = v_count;
end if;END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsorships_persons`
--

CREATE TABLE `char_sponsorships_persons` (
  `sponsorship_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `sponsor_number` varchar(45) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '1' COMMENT '1-مرشح للكفالة ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsorship_cases`
--

CREATE TABLE `char_sponsorship_cases` (
  `id` int(10) UNSIGNED NOT NULL,
  `case_id` int(10) UNSIGNED NOT NULL,
  `sponsorship_id` int(10) UNSIGNED DEFAULT NULL,
  `sponsor_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sponsor_number` varchar(45) DEFAULT NULL,
  `sponsorship_date` date DEFAULT NULL,
  `last_status_date` date NOT NULL,
  `last_status_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsorship_cases_incomplete_data`
--

CREATE TABLE `char_sponsorship_cases_incomplete_data` (
  `id` int(10) UNSIGNED DEFAULT NULL,
  `person_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_name` varchar(255) DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `notes` text,
  `visited_at` date DEFAULT NULL,
  `visitor` varchar(255) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `rank` float DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `case_created_at` datetime DEFAULT NULL,
  `case_updated_at` datetime DEFAULT NULL,
  `case_deleted_at` datetime DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `second_name` varchar(45) DEFAULT NULL,
  `third_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `id_card_number` varchar(9) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `birth_place` varchar(45) DEFAULT NULL,
  `nationality` varchar(45) DEFAULT NULL,
  `death_date` date DEFAULT NULL,
  `death_cause_id` int(10) UNSIGNED DEFAULT NULL,
  `father_id` int(11) UNSIGNED DEFAULT NULL,
  `mother_id` int(11) UNSIGNED DEFAULT NULL,
  `location_id` int(10) UNSIGNED DEFAULT NULL,
  `street_address` varchar(255) DEFAULT NULL,
  `refugee` tinyint(4) DEFAULT NULL,
  `unrwa_card_number` varchar(45) DEFAULT NULL,
  `monthly_income` float DEFAULT NULL,
  `spouses` tinyint(1) DEFAULT NULL,
  `person_created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `person_updated_at` datetime DEFAULT NULL,
  `person_deleted_at` datetime DEFAULT NULL,
  `name` varchar(183) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsorship_cases_status_log`
--

CREATE TABLE `char_sponsorship_cases_status_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `sponsorship_cases_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `date` date DEFAULT NULL,
  `status_reason` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_sponsor_persons`
--

CREATE TABLE `char_sponsor_persons` (
  `person_id` int(10) UNSIGNED NOT NULL,
  `sponsor_id` int(10) UNSIGNED NOT NULL,
  `sponsor_number` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_templates`
--

CREATE TABLE `char_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `template` varchar(255) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_update_data_setting`
--

CREATE TABLE `char_update_data_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `target_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `updated_period` int(10) UNSIGNED DEFAULT NULL,
  `allowed_period` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_users`
--

CREATE TABLE `char_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الجمعية التي يتبع لها المستخدم',
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `super_admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Active=1,Inactive=2,Deleted=3',
  `remember_token` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مستخدمي النظام';

--
-- Dumping data for table `char_users`
--

INSERT INTO `char_users` (`id`, `username`, `password`, `email`, `firstname`, `lastname`, `job_title`, `mobile`, `organization_id`, `parent_id`, `super_admin`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'msafadi', '$2y$10$o9Umr5wvsE9Y45GMts7tH.gCJIRUc.f2BayQ0NgCAapNIO3DmbSpK', 'm.safadi@nepras.com', 'محمد', 'الصفدي', 'مبرمج', '0599885474', 1, NULL, 1, 1, 'giIIL095PB6CBe82cFriYxsbJ5bH91TsdysIqFaycNU62STCLZ8VHXIEOW9P', '2016-08-31 09:36:59', '2017-02-04 08:42:37', NULL),
(16, 'zienab', '$2y$10$o9Umr5wvsE9Y45GMts7tH.gCJIRUc.f2BayQ0NgCAapNIO3DmbSpK', 'zienab@charity.info', 'زينب', 'أبوحطب', 'مبرمجة', '059000000', 1, NULL, 1, 1, NULL, '2017-09-16 20:11:22', '2017-09-16 23:11:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `char_users_permissions`
--

CREATE TABLE `char_users_permissions` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `allow` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_users_roles`
--

CREATE TABLE `char_users_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_verification`
--

CREATE TABLE `char_verification` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) NOT NULL,
  `context` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_visitor_notes`
--

CREATE TABLE `char_visitor_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `for_male` text,
  `for_female` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_vouchers`
--

CREATE TABLE `char_vouchers` (
  `id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `project_id` int(10) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `value` float NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `template` varchar(255) DEFAULT NULL,
  `content` text,
  `header` varchar(255) DEFAULT NULL,
  `footer` varchar(255) DEFAULT NULL,
  `notes` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `sponsor_id` int(10) UNSIGNED DEFAULT NULL,
  `voucher_date` date DEFAULT NULL,
  `count` int(11) NOT NULL,
  `urgent` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول قسائم المساعدات (الكوبونات)';

-- --------------------------------------------------------

--
-- Table structure for table `char_vouchers_categories`
--

CREATE TABLE `char_vouchers_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `template` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_vouchers_persons`
--

CREATE TABLE `char_vouchers_persons` (
  `voucher_id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `receipt_date` date DEFAULT NULL,
  `receipt_location` varchar(255) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول المستفيدين من قسائم المساعدات';

-- --------------------------------------------------------

--
-- Table structure for table `char_work_jobs`
--

CREATE TABLE `char_work_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_work_jobs_i18n`
--

CREATE TABLE `char_work_jobs_i18n` (
  `work_job_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `char_work_jobs_view`
--
CREATE TABLE `char_work_jobs_view` (
`id` int(10) unsigned
,`weight` float
,`language_id` int(10) unsigned
,`name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `char_work_reasons`
--

CREATE TABLE `char_work_reasons` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_work_reasons_i18n`
--

CREATE TABLE `char_work_reasons_i18n` (
  `work_reason_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `char_work_reasons_view`
--
CREATE TABLE `char_work_reasons_view` (
`id` int(10) unsigned
,`weight` float
,`language_id` int(10) unsigned
,`name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `char_work_status`
--

CREATE TABLE `char_work_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `char_work_status_i18n`
--

CREATE TABLE `char_work_status_i18n` (
  `work_status_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `char_work_status_view`
--
CREATE TABLE `char_work_status_view` (
`id` int(10) unsigned
,`weight` float
,`language_id` int(10) unsigned
,`name` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `char_work_wages`
--

CREATE TABLE `char_work_wages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `weight` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_attributes`
--

CREATE TABLE `doc_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(45) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `validation` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_attributes_values`
--

CREATE TABLE `doc_attributes_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_categories`
--

CREATE TABLE `doc_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `description` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_files`
--

CREATE TABLE `doc_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `filepath` varchar(255) NOT NULL,
  `size` bigint(20) NOT NULL,
  `mimetype` varchar(255) NOT NULL,
  `file_status_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ملفات الارشفة';

-- --------------------------------------------------------

--
-- Table structure for table `doc_files_attributes`
--

CREATE TABLE `doc_files_attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `attribute_value_id` int(10) UNSIGNED DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_files_revisions`
--

CREATE TABLE `doc_files_revisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `notes` varchar(45) DEFAULT NULL,
  `filepath` varchar(255) NOT NULL,
  `mimetype` varchar(255) NOT NULL,
  `size` bigint(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_files_status`
--

CREATE TABLE `doc_files_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_files_tags`
--

CREATE TABLE `doc_files_tags` (
  `file_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `doc_mimetypes`
--

CREATE TABLE `doc_mimetypes` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `extension` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doc_mimetypes`
--

INSERT INTO `doc_mimetypes` (`id`, `name`, `extension`) VALUES
('application/andrew-inset', 'Andrew Toolkit', 'N/A'),
('application/applixware', 'Applixware', '.aw'),
('application/atom+xml', 'Atom Syndication Format', '.atom, .xml'),
('application/atomcat+xml', 'Atom Publishing Protocol', '.atomcat'),
('application/atomsvc+xml', 'Atom Publishing Protocol Service Document', '.atomsvc'),
('application/ccxml+xml,', 'Voice Browser Call Control', '.ccxml'),
('application/cdmi-capability', 'Cloud Data Management Interface (CDMI) - Capability', '.cdmia'),
('application/cdmi-container', 'Cloud Data Management Interface (CDMI) - Contaimer', '.cdmic'),
('application/cdmi-domain', 'Cloud Data Management Interface (CDMI) - Domain', '.cdmid'),
('application/cdmi-object', 'Cloud Data Management Interface (CDMI) - Object', '.cdmio'),
('application/cdmi-queue', 'Cloud Data Management Interface (CDMI) - Queue', '.cdmiq'),
('application/cu-seeme', 'CU-SeeMe', '.cu'),
('application/davmount+xml', 'Web Distributed Authoring and Versioning', '.davmount'),
('application/dssc+der', 'Data Structure for the Security Suitability of Cryptographic Algorithms', '.dssc'),
('application/dssc+xml', 'Data Structure for the Security Suitability of Cryptographic Algorithms', '.xdssc'),
('application/ecmascript', 'ECMAScript', '.es'),
('application/emma+xml', 'Extensible MultiModal Annotation', '.emma'),
('application/epub+zip', 'Electronic Publication', '.epub'),
('application/exi', 'Efficient XML Interchange', '.exi'),
('application/font-tdpfr', 'Portable Font Resource', '.pfr'),
('application/hyperstudio', 'Hyperstudio', '.stk'),
('application/ipfix', 'Internet Protocol Flow Information Export', '.ipfix'),
('application/java-archive', 'Java Archive', '.jar'),
('application/java-serialized-object', 'Java Serialized Object', '.ser'),
('application/java-vm', 'Java Bytecode File', '.class'),
('application/javascript', 'JavaScript', '.js'),
('application/json', 'JavaScript Object Notation (JSON)', '.json'),
('application/mac-binhex40', 'Macintosh BinHex 4.0', '.hqx'),
('application/mac-compactpro', 'Compact Pro', '.cpt'),
('application/mads+xml', 'Metadata Authority Description Schema', '.mads'),
('application/marc', 'MARC Formats', '.mrc'),
('application/marcxml+xml', 'MARC21 XML Schema', '.mrcx'),
('application/mathematica', 'Mathematica Notebooks', '.ma'),
('application/mathml+xml', 'Mathematical Markup Language', '.mathml'),
('application/mbox', 'Mbox database files', '.mbox'),
('application/mediaservercontrol+xml', 'Media Server Control Markup Language', '.mscml'),
('application/metalink4+xml', 'Metalink', '.meta4'),
('application/mets+xml', 'Metadata Encoding and Transmission Standard', '.mets'),
('application/mods+xml', 'Metadata Object Description Schema', '.mods'),
('application/mp21', 'MPEG-21', '.m21'),
('application/mp4', 'MPEG4', '.mp4'),
('application/msword', 'Microsoft Word', '.doc'),
('application/mxf', 'Material Exchange Format', '.mxf'),
('application/octet-stream', 'Binary Data', '.bin'),
('application/oda', 'Office Document Architecture', '.oda'),
('application/oebps-package+xml', 'Open eBook Publication Structure', '.opf'),
('application/ogg', 'Ogg', '.ogx'),
('application/onenote', 'Microsoft OneNote', '.onetoc'),
('application/patch-ops-error+xml', 'XML Patch Framework', '.xer'),
('application/pdf', 'Adobe Portable Document Format', '.pdf'),
('application/pgp-encrypted', 'Pretty Good Privacy', ''),
('application/pgp-signature', 'Pretty Good Privacy - Signature', '.pgp'),
('application/pics-rules', 'PICSRules', '.prf'),
('application/pkcs10', 'PKCS #10 - Certification Request Standard', '.p10'),
('application/pkcs7-mime', 'PKCS #7 - Cryptographic Message Syntax Standard', '.p7m'),
('application/pkcs7-signature', 'PKCS #7 - Cryptographic Message Syntax Standard', '.p7s'),
('application/pkcs8', 'PKCS #8 - Private-Key Information Syntax Standard', '.p8'),
('application/pkix-attr-cert', 'Attribute Certificate', '.ac'),
('application/pkix-cert', 'Internet Public Key Infrastructure - Certificate', '.cer'),
('application/pkix-crl', 'Internet Public Key Infrastructure - Certificate Revocation Lists', '.crl'),
('application/pkix-pkipath', 'Internet Public Key Infrastructure - Certification Path', '.pkipath'),
('application/pkixcmp', 'Internet Public Key Infrastructure - Certificate Management Protocole', '.pki'),
('application/pls+xml', 'Pronunciation Lexicon Specification', '.pls'),
('application/postscript', 'PostScript', '.ai'),
('application/prs.cww', 'CU-Writer', '.cww'),
('application/pskc+xml', 'Portable Symmetric Key Container', '.pskcxml'),
('application/rdf+xml', 'Resource Description Framework', '.rdf'),
('application/reginfo+xml', 'IMS Networks', '.rif'),
('application/relax-ng-compact-syntax', 'Relax NG Compact Syntax', '.rnc'),
('application/resource-lists+xml', 'XML Resource Lists', '.rl'),
('application/resource-lists-diff+xml', 'XML Resource Lists Diff', '.rld'),
('application/rls-services+xml', 'XML Resource Lists', '.rs'),
('application/rsd+xml', 'Really Simple Discovery', '.rsd'),
('application/rss+xml', 'RSS - Really Simple Syndication', '.rss, .xml'),
('application/rtf', 'Rich Text Format', '.rtf'),
('application/sbml+xml', 'Systems Biology Markup Language', '.sbml'),
('application/scvp-cv-request', 'Server-Based Certificate Validation Protocol - Validation Request', '.scq'),
('application/scvp-cv-response', 'Server-Based Certificate Validation Protocol - Validation Response', '.scs'),
('application/scvp-vp-request', 'Server-Based Certificate Validation Protocol - Validation Policies - Request', '.spq'),
('application/scvp-vp-response', 'Server-Based Certificate Validation Protocol - Validation Policies - Response', '.spp'),
('application/sdp', 'Session Description Protocol', '.sdp'),
('application/set-payment-initiation', 'Secure Electronic Transaction - Payment', '.setpay'),
('application/set-registration-initiation', 'Secure Electronic Transaction - Registration', '.setreg'),
('application/shf+xml', 'S Hexdump Format', '.shf'),
('application/smil+xml', 'Synchronized Multimedia Integration Language', '.smi'),
('application/sparql-query', 'SPARQL - Query', '.rq'),
('application/sparql-results+xml', 'SPARQL - Results', '.srx'),
('application/srgs', 'Speech Recognition Grammar Specification', '.gram'),
('application/srgs+xml', 'Speech Recognition Grammar Specification - XML', '.grxml'),
('application/sru+xml', 'Search/Retrieve via URL Response Format', '.sru'),
('application/ssml+xml', 'Speech Synthesis Markup Language', '.ssml'),
('application/tei+xml', 'Text Encoding and Interchange', '.tei'),
('application/thraud+xml', 'Sharing Transaction Fraud Data', '.tfi'),
('application/timestamped-data', 'Time Stamped Data Envelope', '.tsd'),
('application/vnd.3gpp.pic-bw-large', '3rd Generation Partnership Project - Pic Large', '.plb'),
('application/vnd.3gpp.pic-bw-small', '3rd Generation Partnership Project - Pic Small', '.psb'),
('application/vnd.3gpp.pic-bw-var', '3rd Generation Partnership Project - Pic Var', '.pvb'),
('application/vnd.3gpp2.tcap', '3rd Generation Partnership Project - Transaction Capabilities Application Part', '.tcap'),
('application/vnd.3m.post-it-notes', '3M Post It Notes', '.pwn'),
('application/vnd.accpac.simply.aso', 'Simply Accounting', '.aso'),
('application/vnd.accpac.simply.imp', 'Simply Accounting - Data Import', '.imp'),
('application/vnd.acucobol', 'ACU Cobol', '.acu'),
('application/vnd.acucorp', 'ACU Cobol', '.atc'),
('application/vnd.adobe.air-application-installer-package+zip', 'Adobe AIR Application', '.air'),
('application/vnd.adobe.fxp', 'Adobe Flex Project', '.fxp'),
('application/vnd.adobe.xdp+xml', 'Adobe XML Data Package', '.xdp'),
('application/vnd.adobe.xfdf', 'Adobe XML Forms Data Format', '.xfdf'),
('application/vnd.ahead.space', 'Ahead AIR Application', '.ahead'),
('application/vnd.airzip.filesecure.azf', 'AirZip FileSECURE', '.azf'),
('application/vnd.airzip.filesecure.azs', 'AirZip FileSECURE', '.azs'),
('application/vnd.amazon.ebook', 'Amazon Kindle eBook format', '.azw'),
('application/vnd.americandynamics.acc', 'Active Content Compression', '.acc'),
('application/vnd.amiga.ami', 'AmigaDE', '.ami'),
('application/vnd.android.package-archive', 'Android Package Archive', '.apk'),
('application/vnd.anser-web-certificate-issue-initiation', 'ANSER-WEB Terminal Client - Certificate Issue', '.cii'),
('application/vnd.anser-web-funds-transfer-initiation', 'ANSER-WEB Terminal Client - Web Funds Transfer', '.fti'),
('application/vnd.antix.game-component', 'Antix Game Player', '.atx'),
('application/vnd.apple.installer+xml', 'Apple Installer Package', '.mpkg'),
('application/vnd.apple.mpegurl', 'Multimedia Playlist Unicode', '.m3u8'),
('application/vnd.aristanetworks.swi', 'Arista Networks Software Image', '.swi'),
('application/vnd.audiograph', 'Audiograph', '.aep'),
('application/vnd.blueice.multipass', 'Blueice Research Multipass', '.mpm'),
('application/vnd.bmi', 'BMI Drawing Data Interchange', '.bmi'),
('application/vnd.businessobjects', 'BusinessObjects', '.rep'),
('application/vnd.chemdraw+xml', 'CambridgeSoft Chem Draw', '.cdxml'),
('application/vnd.chipnuts.karaoke-mmd', 'Karaoke on Chipnuts Chipsets', '.mmd'),
('application/vnd.cinderella', 'Interactive Geometry Software Cinderella', '.cdy'),
('application/vnd.claymore', 'Claymore Data Files', '.cla'),
('application/vnd.cloanto.rp9', 'RetroPlatform Player', '.rp9'),
('application/vnd.clonk.c4group', 'Clonk Game', '.c4g'),
('application/vnd.cluetrust.cartomobile-config', 'ClueTrust CartoMobile - Config', '.c11amc'),
('application/vnd.cluetrust.cartomobile-config-pkg', 'ClueTrust CartoMobile - Config Package', '.c11amz'),
('application/vnd.commonspace', 'Sixth Floor Media - CommonSpace', '.csp'),
('application/vnd.contact.cmsg', 'CIM Database', '.cdbcmsg'),
('application/vnd.cosmocaller', 'CosmoCaller', '.cmc'),
('application/vnd.crick.clicker', 'CrickSoftware - Clicker', '.clkx'),
('application/vnd.crick.clicker.keyboard', 'CrickSoftware - Clicker - Keyboard', '.clkk'),
('application/vnd.crick.clicker.palette', 'CrickSoftware - Clicker - Palette', '.clkp'),
('application/vnd.crick.clicker.template', 'CrickSoftware - Clicker - Template', '.clkt'),
('application/vnd.crick.clicker.wordbank', 'CrickSoftware - Clicker - Wordbank', '.clkw'),
('application/vnd.criticaltools.wbs+xml', 'Critical Tools - PERT Chart EXPERT', '.wbs'),
('application/vnd.ctc-posml', 'PosML', '.pml'),
('application/vnd.cups-ppd', 'Adobe PostScript Printer Description File Format', '.ppd'),
('application/vnd.curl.car', 'CURL Applet', '.car'),
('application/vnd.curl.pcurl', 'CURL Applet', '.pcurl'),
('application/vnd.data-vision.rdz', 'RemoteDocs R-Viewer', '.rdz'),
('application/vnd.denovo.fcselayout-link', 'FCS Express Layout Link', '.fe_launch'),
('application/vnd.dna', 'New Moon Liftoff/DNA', '.dna'),
('application/vnd.dolby.mlp', 'Dolby Meridian Lossless Packing', '.mlp'),
('application/vnd.dpgraph', 'DPGraph', '.dpg'),
('application/vnd.dreamfactory', 'DreamFactory', '.dfac'),
('application/vnd.dvb.ait', 'Digital Video Broadcasting', '.ait'),
('application/vnd.dvb.service', 'Digital Video Broadcasting', '.svc'),
('application/vnd.dynageo', 'DynaGeo', '.geo'),
('application/vnd.ecowin.chart', 'EcoWin Chart', '.mag'),
('application/vnd.enliven', 'Enliven Viewer', '.nml'),
('application/vnd.epson.esf', 'QUASS Stream Player', '.esf'),
('application/vnd.epson.msf', 'QUASS Stream Player', '.msf'),
('application/vnd.epson.quickanime', 'QuickAnime Player', '.qam'),
('application/vnd.epson.salt', 'SimpleAnimeLite Player', '.slt'),
('application/vnd.epson.ssf', 'QUASS Stream Player', '.ssf'),
('application/vnd.eszigno3+xml', 'MICROSEC e-Szign�', '.es3'),
('application/vnd.ezpix-album', 'EZPix Secure Photo Album', '.ez2'),
('application/vnd.ezpix-package', 'EZPix Secure Photo Album', '.ez3'),
('application/vnd.fdf', 'Forms Data Format', '.fdf'),
('application/vnd.fdsn.seed', 'Digital Siesmograph Networks - SEED Datafiles', '.seed'),
('application/vnd.flographit', 'NpGraphIt', '.gph'),
('application/vnd.fluxtime.clip', 'FluxTime Clip', '.ftc'),
('application/vnd.framemaker', 'FrameMaker Normal Format', '.fm'),
('application/vnd.frogans.fnc', 'Frogans Player', '.fnc'),
('application/vnd.frogans.ltf', 'Frogans Player', '.ltf'),
('application/vnd.fsc.weblaunch', 'Friendly Software Corporation', '.fsc'),
('application/vnd.fujitsu.oasys', 'Fujitsu Oasys', '.oas'),
('application/vnd.fujitsu.oasys2', 'Fujitsu Oasys', '.oa2'),
('application/vnd.fujitsu.oasys3', 'Fujitsu Oasys', '.oa3'),
('application/vnd.fujitsu.oasysgp', 'Fujitsu Oasys', '.fg5'),
('application/vnd.fujitsu.oasysprs', 'Fujitsu Oasys', '.bh2'),
('application/vnd.fujixerox.ddd', 'Fujitsu - Xerox 2D CAD Data', '.ddd'),
('application/vnd.fujixerox.docuworks', 'Fujitsu - Xerox DocuWorks', '.xdw'),
('application/vnd.fujixerox.docuworks.binder', 'Fujitsu - Xerox DocuWorks Binder', '.xbd'),
('application/vnd.fuzzysheet', 'FuzzySheet', '.fzs'),
('application/vnd.genomatix.tuxedo', 'Genomatix Tuxedo Framework', '.txd'),
('application/vnd.geogebra.file', 'GeoGebra', '.ggb'),
('application/vnd.geogebra.tool', 'GeoGebra', '.ggt'),
('application/vnd.geometry-explorer', 'GeoMetry Explorer', '.gex'),
('application/vnd.geonext', 'GEONExT and JSXGraph', '.gxt'),
('application/vnd.geoplan', 'GeoplanW', '.g2w'),
('application/vnd.geospace', 'GeospacW', '.g3w'),
('application/vnd.gmx', 'GameMaker ActiveX', '.gmx'),
('application/vnd.google-earth.kml+xml', 'Google Earth - KML', '.kml'),
('application/vnd.google-earth.kmz', 'Google Earth - Zipped KML', '.kmz'),
('application/vnd.grafeq', 'GrafEq', '.gqf'),
('application/vnd.groove-account', 'Groove - Account', '.gac'),
('application/vnd.groove-help', 'Groove - Help', '.ghf'),
('application/vnd.groove-identity-message', 'Groove - Identity Message', '.gim'),
('application/vnd.groove-injector', 'Groove - Injector', '.grv'),
('application/vnd.groove-tool-message', 'Groove - Tool Message', '.gtm'),
('application/vnd.groove-tool-template', 'Groove - Tool Template', '.tpl'),
('application/vnd.groove-vcard', 'Groove - Vcard', '.vcg'),
('application/vnd.hal+xml', 'Hypertext Application Language', '.hal'),
('application/vnd.handheld-entertainment+xml', 'ZVUE Media Manager', '.zmm'),
('application/vnd.hbci', 'Homebanking Computer Interface (HBCI)', '.hbci'),
('application/vnd.hhe.lesson-player', 'Archipelago Lesson Player', '.les'),
('application/vnd.hp-hpgl', 'HP-GL/2 and HP RTL', '.hpgl'),
('application/vnd.hp-hpid', 'Hewlett Packard Instant Delivery', '.hpid'),
('application/vnd.hp-hps', 'Hewlett-Packard''s WebPrintSmart', '.hps'),
('application/vnd.hp-jlyt', 'HP Indigo Digital Press - Job Layout Languate', '.jlt'),
('application/vnd.hp-pcl', 'HP Printer Command Language', '.pcl'),
('application/vnd.hp-pclxl', 'PCL 6 Enhanced (Formely PCL XL)', '.pclxl'),
('application/vnd.hydrostatix.sof-data', 'Hydrostatix Master Suite', '.sfd-hdstx'),
('application/vnd.hzn-3d-crossword', '3D Crossword Plugin', '.x3d'),
('application/vnd.ibm.minipay', 'MiniPay', '.mpy'),
('application/vnd.ibm.modcap', 'MO:DCA-P', '.afp'),
('application/vnd.ibm.rights-management', 'IBM DB2 Rights Manager', '.irm'),
('application/vnd.ibm.secure-container', 'IBM Electronic Media Management System - Secure Container', '.sc'),
('application/vnd.iccprofile', 'ICC profile', '.icc'),
('application/vnd.igloader', 'igLoader', '.igl'),
('application/vnd.immervision-ivp', 'ImmerVision PURE Players', '.ivp'),
('application/vnd.immervision-ivu', 'ImmerVision PURE Players', '.ivu'),
('application/vnd.insors.igm', 'IOCOM Visimeet', '.igm'),
('application/vnd.intercon.formnet', 'Intercon FormNet', '.xpw'),
('application/vnd.intergeo', 'Interactive Geometry Software', '.i2g'),
('application/vnd.intu.qbo', 'Open Financial Exchange', '.qbo'),
('application/vnd.intu.qfx', 'Quicken', '.qfx'),
('application/vnd.ipunplugged.rcprofile', 'IP Unplugged Roaming Client', '.rcprofile'),
('application/vnd.irepository.package+xml', 'iRepository / Lucidoc Editor', '.irp'),
('application/vnd.is-xpr', 'Express by Infoseek', '.xpr'),
('application/vnd.isac.fcs', 'International Society for Advancement of Cytometry', '.fcs'),
('application/vnd.jam', 'Lightspeed Audio Lab', '.jam'),
('application/vnd.jcp.javame.midlet-rms', 'Mobile Information Device Profile', '.rms'),
('application/vnd.jisp', 'RhymBox', '.jisp'),
('application/vnd.joost.joda-archive', 'Joda Archive', '.joda'),
('application/vnd.kahootz', 'Kahootz', '.ktz'),
('application/vnd.kde.karbon', 'KDE KOffice Office Suite - Karbon', '.karbon'),
('application/vnd.kde.kchart', 'KDE KOffice Office Suite - KChart', '.chrt'),
('application/vnd.kde.kformula', 'KDE KOffice Office Suite - Kformula', '.kfo'),
('application/vnd.kde.kivio', 'KDE KOffice Office Suite - Kivio', '.flw'),
('application/vnd.kde.kontour', 'KDE KOffice Office Suite - Kontour', '.kon'),
('application/vnd.kde.kpresenter', 'KDE KOffice Office Suite - Kpresenter', '.kpr'),
('application/vnd.kde.kspread', 'KDE KOffice Office Suite - Kspread', '.ksp'),
('application/vnd.kde.kword', 'KDE KOffice Office Suite - Kword', '.kwd'),
('application/vnd.kenameaapp', 'Kenamea App', '.htke'),
('application/vnd.kidspiration', 'Kidspiration', '.kia'),
('application/vnd.kinar', 'Kinar Applications', '.kne'),
('application/vnd.koan', 'SSEYO Koan Play File', '.skp'),
('application/vnd.kodak-descriptor', 'Kodak Storyshare', '.sse'),
('application/vnd.las.las+xml', 'Laser App Enterprise', '.lasxml'),
('application/vnd.llamagraphics.life-balance.desktop', 'Life Balance - Desktop Edition', '.lbd'),
('application/vnd.llamagraphics.life-balance.exchange+xml', 'Life Balance - Exchange Format', '.lbe'),
('application/vnd.lotus-1-2-3', 'Lotus 1-2-3', '0.123'),
('application/vnd.lotus-approach', 'Lotus Approach', '.apr'),
('application/vnd.lotus-freelance', 'Lotus Freelance', '.pre'),
('application/vnd.lotus-notes', 'Lotus Notes', '.nsf'),
('application/vnd.lotus-organizer', 'Lotus Organizer', '.org'),
('application/vnd.lotus-screencam', 'Lotus Screencam', '.scm'),
('application/vnd.lotus-wordpro', 'Lotus Wordpro', '.lwp'),
('application/vnd.macports.portpkg', 'MacPorts Port System', '.portpkg'),
('application/vnd.mcd', 'Micro CADAM Helix D&D', '.mcd'),
('application/vnd.medcalcdata', 'MedCalc', '.mc1'),
('application/vnd.mediastation.cdkey', 'MediaRemote', '.cdkey'),
('application/vnd.mfer', 'Medical Waveform Encoding Format', '.mwf'),
('application/vnd.mfmp', 'Melody Format for Mobile Platform', '.mfm'),
('application/vnd.micrografx.flo', 'Micrografx', '.flo'),
('application/vnd.micrografx.igx', 'Micrografx iGrafx Professional', '.igx'),
('application/vnd.mif', 'FrameMaker Interchange Format', '.mif'),
('application/vnd.mobius.daf', 'Mobius Management Systems - UniversalArchive', '.daf'),
('application/vnd.mobius.dis', 'Mobius Management Systems - Distribution Database', '.dis'),
('application/vnd.mobius.mbk', 'Mobius Management Systems - Basket file', '.mbk'),
('application/vnd.mobius.mqy', 'Mobius Management Systems - Query File', '.mqy'),
('application/vnd.mobius.msl', 'Mobius Management Systems - Script Language', '.msl'),
('application/vnd.mobius.plc', 'Mobius Management Systems - Policy Definition Language File', '.plc'),
('application/vnd.mobius.txf', 'Mobius Management Systems - Topic Index File', '.txf'),
('application/vnd.mophun.application', 'Mophun VM', '.mpn'),
('application/vnd.mophun.certificate', 'Mophun Certificate', '.mpc'),
('application/vnd.mozilla.xul+xml', 'XUL - XML User Interface Language', '.xul'),
('application/vnd.ms-artgalry', 'Microsoft Artgalry', '.cil'),
('application/vnd.ms-cab-compressed', 'Microsoft Cabinet File', '.cab'),
('application/vnd.ms-excel', 'Microsoft Excel', '.xls'),
('application/vnd.ms-excel.addin.macroenabled.12', 'Microsoft Excel - Add-In File', '.xlam'),
('application/vnd.ms-excel.sheet.binary.macroenabled.12', 'Microsoft Excel - Binary Workbook', '.xlsb'),
('application/vnd.ms-excel.sheet.macroenabled.12', 'Microsoft Excel - Macro-Enabled Workbook', '.xlsm'),
('application/vnd.ms-excel.template.macroenabled.12', 'Microsoft Excel - Macro-Enabled Template File', '.xltm'),
('application/vnd.ms-fontobject', 'Microsoft Embedded OpenType', '.eot'),
('application/vnd.ms-htmlhelp', 'Microsoft Html Help File', '.chm'),
('application/vnd.ms-ims', 'Microsoft Class Server', '.ims'),
('application/vnd.ms-lrm', 'Microsoft Learning Resource Module', '.lrm'),
('application/vnd.ms-officetheme', 'Microsoft Office System Release Theme', '.thmx'),
('application/vnd.ms-pki.seccat', 'Microsoft Trust UI Provider - Security Catalog', '.cat'),
('application/vnd.ms-pki.stl', 'Microsoft Trust UI Provider - Certificate Trust Link', '.stl'),
('application/vnd.ms-powerpoint', 'Microsoft PowerPoint', '.ppt'),
('application/vnd.ms-powerpoint.addin.macroenabled.12', 'Microsoft PowerPoint - Add-in file', '.ppam'),
('application/vnd.ms-powerpoint.presentation.macroenabled.12', 'Microsoft PowerPoint - Macro-Enabled Presentation File', '.pptm'),
('application/vnd.ms-powerpoint.slide.macroenabled.12', 'Microsoft PowerPoint - Macro-Enabled Open XML Slide', '.sldm'),
('application/vnd.ms-powerpoint.slideshow.macroenabled.12', 'Microsoft PowerPoint - Macro-Enabled Slide Show File', '.ppsm'),
('application/vnd.ms-powerpoint.template.macroenabled.12', 'Micosoft PowerPoint - Macro-Enabled Template File', '.potm'),
('application/vnd.ms-project', 'Microsoft Project', '.mpp'),
('application/vnd.ms-word.document.macroenabled.12', 'Micosoft Word - Macro-Enabled Document', '.docm'),
('application/vnd.ms-word.template.macroenabled.12', 'Micosoft Word - Macro-Enabled Template', '.dotm'),
('application/vnd.ms-works', 'Microsoft Works', '.wps'),
('application/vnd.ms-wpl', 'Microsoft Windows Media Player Playlist', '.wpl'),
('application/vnd.ms-xpsdocument', 'Microsoft XML Paper Specification', '.xps'),
('application/vnd.mseq', '3GPP MSEQ File', '.mseq'),
('application/vnd.musician', 'MUsical Score Interpreted Code Invented for the ASCII designation of Notation', '.mus'),
('application/vnd.muvee.style', 'Muvee Automatic Video Editing', '.msty'),
('application/vnd.neurolanguage.nlu', 'neuroLanguage', '.nlu'),
('application/vnd.noblenet-directory', 'NobleNet Directory', '.nnd'),
('application/vnd.noblenet-sealer', 'NobleNet Sealer', '.nns'),
('application/vnd.noblenet-web', 'NobleNet Web', '.nnw'),
('application/vnd.nokia.n-gage.data', 'N-Gage Game Data', '.ngdat'),
('application/vnd.nokia.n-gage.symbian.install', 'N-Gage Game Installer', '.n-gage'),
('application/vnd.nokia.radio-preset', 'Nokia Radio Application - Preset', '.rpst'),
('application/vnd.nokia.radio-presets', 'Nokia Radio Application - Preset', '.rpss'),
('application/vnd.novadigm.edm', 'Novadigm''s RADIA and EDM products', '.edm'),
('application/vnd.novadigm.edx', 'Novadigm''s RADIA and EDM products', '.edx'),
('application/vnd.novadigm.ext', 'Novadigm''s RADIA and EDM products', '.ext'),
('application/vnd.oasis.opendocument.chart', 'OpenDocument Chart', '.odc'),
('application/vnd.oasis.opendocument.chart-template', 'OpenDocument Chart Template', '.otc'),
('application/vnd.oasis.opendocument.database', 'OpenDocument Database', '.odb'),
('application/vnd.oasis.opendocument.formula', 'OpenDocument Formula', '.odf'),
('application/vnd.oasis.opendocument.formula-template', 'OpenDocument Formula Template', '.odft'),
('application/vnd.oasis.opendocument.graphics', 'OpenDocument Graphics', '.odg'),
('application/vnd.oasis.opendocument.graphics-template', 'OpenDocument Graphics Template', '.otg'),
('application/vnd.oasis.opendocument.image', 'OpenDocument Image', '.odi'),
('application/vnd.oasis.opendocument.image-template', 'OpenDocument Image Template', '.oti'),
('application/vnd.oasis.opendocument.presentation', 'OpenDocument Presentation', '.odp'),
('application/vnd.oasis.opendocument.presentation-template', 'OpenDocument Presentation Template', '.otp'),
('application/vnd.oasis.opendocument.spreadsheet', 'OpenDocument Spreadsheet', '.ods'),
('application/vnd.oasis.opendocument.spreadsheet-template', 'OpenDocument Spreadsheet Template', '.ots'),
('application/vnd.oasis.opendocument.text', 'OpenDocument Text', '.odt'),
('application/vnd.oasis.opendocument.text-master', 'OpenDocument Text Master', '.odm'),
('application/vnd.oasis.opendocument.text-template', 'OpenDocument Text Template', '.ott'),
('application/vnd.oasis.opendocument.text-web', 'Open Document Text Web', '.oth'),
('application/vnd.olpc-sugar', 'Sugar Linux Application Bundle', '.xo'),
('application/vnd.oma.dd2+xml', 'OMA Download Agents', '.dd2'),
('application/vnd.openofficeorg.extension', 'Open Office Extension', '.oxt'),
('application/vnd.openxmlformats-officedocument.presentationml.presentation', 'Microsoft Office - OOXML - Presentation', '.pptx'),
('application/vnd.openxmlformats-officedocument.presentationml.slide', 'Microsoft Office - OOXML - Presentation (Slide)', '.sldx'),
('application/vnd.openxmlformats-officedocument.presentationml.slideshow', 'Microsoft Office - OOXML - Presentation (Slideshow)', '.ppsx'),
('application/vnd.openxmlformats-officedocument.presentationml.template', 'Microsoft Office - OOXML - Presentation Template', '.potx'),
('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Microsoft Office - OOXML - Spreadsheet', '.xlsx'),
('application/vnd.openxmlformats-officedocument.spreadsheetml.template', 'Microsoft Office - OOXML - Spreadsheet Teplate', '.xltx'),
('application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'Microsoft Office - OOXML - Word Document', '.docx'),
('application/vnd.openxmlformats-officedocument.wordprocessingml.template', 'Microsoft Office - OOXML - Word Document Template', '.dotx'),
('application/vnd.osgeo.mapguide.package', 'MapGuide DBXML', '.mgp'),
('application/vnd.osgi.dp', 'OSGi Deployment Package', '.dp'),
('application/vnd.palm', 'PalmOS Data', '.pdb'),
('application/vnd.pawaafile', 'PawaaFILE', '.paw'),
('application/vnd.pg.format', 'Proprietary P&G Standard Reporting System', '.str'),
('application/vnd.pg.osasli', 'Proprietary P&G Standard Reporting System', '.ei6'),
('application/vnd.picsel', 'Pcsel eFIF File', '.efif'),
('application/vnd.pmi.widget', 'Qualcomm''s Plaza Mobile Internet', '.wg'),
('application/vnd.pocketlearn', 'PocketLearn Viewers', '.plf'),
('application/vnd.powerbuilder6', 'PowerBuilder', '.pbd'),
('application/vnd.previewsystems.box', 'Preview Systems ZipLock/VBox', '.box'),
('application/vnd.proteus.magazine', 'EFI Proteus', '.mgz'),
('application/vnd.publishare-delta-tree', 'PubliShare Objects', '.qps'),
('application/vnd.pvi.ptid1', 'Princeton Video Image', '.ptid'),
('application/vnd.quark.quarkxpress', 'QuarkXpress', '.qxd'),
('application/vnd.realvnc.bed', 'RealVNC', '.bed'),
('application/vnd.recordare.musicxml', 'Recordare Applications', '.mxl'),
('application/vnd.recordare.musicxml+xml', 'Recordare Applications', '.musicxml'),
('application/vnd.rig.cryptonote', 'CryptoNote', '.cryptonote'),
('application/vnd.rim.cod', 'Blackberry COD File', '.cod'),
('application/vnd.rn-realmedia', 'RealMedia', '.rm'),
('application/vnd.route66.link66+xml', 'ROUTE 66 Location Based Services', '.link66'),
('application/vnd.sailingtracker.track', 'SailingTracker', '.st'),
('application/vnd.seemail', 'SeeMail', '.see'),
('application/vnd.sema', 'Secured eMail', '.sema'),
('application/vnd.semd', 'Secured eMail', '.semd'),
('application/vnd.semf', 'Secured eMail', '.semf'),
('application/vnd.shana.informed.formdata', 'Shana Informed Filler', '.ifm'),
('application/vnd.shana.informed.formtemplate', 'Shana Informed Filler', '.itp'),
('application/vnd.shana.informed.interchange', 'Shana Informed Filler', '.iif'),
('application/vnd.shana.informed.package', 'Shana Informed Filler', '.ipk'),
('application/vnd.simtech-mindmapper', 'SimTech MindMapper', '.twd'),
('application/vnd.smaf', 'SMAF File', '.mmf'),
('application/vnd.smart.teacher', 'SMART Technologies Apps', '.teacher'),
('application/vnd.solent.sdkm+xml', 'SudokuMagic', '.sdkm'),
('application/vnd.spotfire.dxp', 'TIBCO Spotfire', '.dxp'),
('application/vnd.spotfire.sfs', 'TIBCO Spotfire', '.sfs'),
('application/vnd.stardivision.calc', 'StarOffice - Calc', '.sdc'),
('application/vnd.stardivision.draw', 'StarOffice - Draw', '.sda'),
('application/vnd.stardivision.impress', 'StarOffice - Impress', '.sdd'),
('application/vnd.stardivision.math', 'StarOffice - Math', '.smf'),
('application/vnd.stardivision.writer', 'StarOffice - Writer', '.sdw'),
('application/vnd.stardivision.writer-global', 'StarOffice - Writer (Global)', '.sgl'),
('application/vnd.stepmania.stepchart', 'StepMania', '.sm'),
('application/vnd.sun.xml.calc', 'OpenOffice - Calc (Spreadsheet)', '.sxc'),
('application/vnd.sun.xml.calc.template', 'OpenOffice - Calc Template (Spreadsheet)', '.stc'),
('application/vnd.sun.xml.draw', 'OpenOffice - Draw (Graphics)', '.sxd'),
('application/vnd.sun.xml.draw.template', 'OpenOffice - Draw Template (Graphics)', '.std'),
('application/vnd.sun.xml.impress', 'OpenOffice - Impress (Presentation)', '.sxi'),
('application/vnd.sun.xml.impress.template', 'OpenOffice - Impress Template (Presentation)', '.sti'),
('application/vnd.sun.xml.math', 'OpenOffice - Math (Formula)', '.sxm'),
('application/vnd.sun.xml.writer', 'OpenOffice - Writer (Text - HTML)', '.sxw'),
('application/vnd.sun.xml.writer.global', 'OpenOffice - Writer (Text - HTML)', '.sxg'),
('application/vnd.sun.xml.writer.template', 'OpenOffice - Writer Template (Text - HTML)', '.stw'),
('application/vnd.sus-calendar', 'ScheduleUs', '.sus'),
('application/vnd.svd', 'SourceView Document', '.svd'),
('application/vnd.symbian.install', 'Symbian Install Package', '.sis'),
('application/vnd.syncml+xml', 'SyncML', '.xsm'),
('application/vnd.syncml.dm+wbxml', 'SyncML - Device Management', '.bdm'),
('application/vnd.syncml.dm+xml', 'SyncML - Device Management', '.xdm'),
('application/vnd.tao.intent-module-archive', 'Tao Intent', '.tao'),
('application/vnd.tmobile-livetv', 'MobileTV', '.tmo'),
('application/vnd.trid.tpt', 'TRI Systems Config', '.tpt'),
('application/vnd.triscape.mxs', 'Triscape Map Explorer', '.mxs'),
('application/vnd.trueapp', 'True BASIC', '.tra'),
('application/vnd.ufdl', 'Universal Forms Description Language', '.ufd'),
('application/vnd.uiq.theme', 'User Interface Quartz - Theme (Symbian)', '.utz'),
('application/vnd.umajin', 'UMAJIN', '.umj'),
('application/vnd.unity', 'Unity 3d', '.unityweb'),
('application/vnd.uoml+xml', 'Unique Object Markup Language', '.uoml'),
('application/vnd.vcx', 'VirtualCatalog', '.vcx'),
('application/vnd.visio', 'Microsoft Visio', '.vsd'),
('application/vnd.visionary', 'Visionary', '.vis'),
('application/vnd.vsf', 'Viewport+', '.vsf'),
('application/vnd.wap.wbxml', 'WAP Binary XML (WBXML)', '.wbxml'),
('application/vnd.wap.wmlc', 'Compiled Wireless Markup Language (WMLC)', '.wmlc'),
('application/vnd.wap.wmlscriptc', 'WMLScript', '.wmlsc'),
('application/vnd.webturbo', 'WebTurbo', '.wtb'),
('application/vnd.wolfram.player', 'Mathematica Notebook Player', '.nbp'),
('application/vnd.wordperfect', 'Wordperfect', '.wpd'),
('application/vnd.wqd', 'SundaHus WQ', '.wqd'),
('application/vnd.wt.stf', 'Worldtalk', '.stf'),
('application/vnd.xara', 'CorelXARA', '.xar'),
('application/vnd.xfdl', 'Extensible Forms Description Language', '.xfdl'),
('application/vnd.yamaha.hv-dic', 'HV Voice Dictionary', '.hvd'),
('application/vnd.yamaha.hv-script', 'HV Script', '.hvs'),
('application/vnd.yamaha.hv-voice', 'HV Voice Parameter', '.hvp'),
('application/vnd.yamaha.openscoreformat', 'Open Score Format', '.osf'),
('application/vnd.yamaha.openscoreformat.osfpvg+xml', 'OSFPVG', '.osfpvg'),
('application/vnd.yamaha.smaf-audio', 'SMAF Audio', '.saf'),
('application/vnd.yamaha.smaf-phrase', 'SMAF Phrase', '.spf'),
('application/vnd.yellowriver-custom-menu', 'CustomMenu', '.cmp'),
('application/vnd.zul', 'Z.U.L. Geometry', '.zir'),
('application/vnd.zzazz.deck+xml', 'Zzazz Deck', '.zaz'),
('application/voicexml+xml', 'VoiceXML', '.vxml'),
('application/widget', 'Widget Packaging and XML Configuration', '.wgt'),
('application/winhlp', 'WinHelp', '.hlp'),
('application/wsdl+xml', 'WSDL - Web Services Description Language', '.wsdl'),
('application/wspolicy+xml', 'Web Services Policy', '.wspolicy'),
('application/x-7z-compressed', '7-Zip', '.7z'),
('application/x-abiword', 'AbiWord', '.abw'),
('application/x-ace-compressed', 'Ace Archive', '.ace'),
('application/x-authorware-bin', 'Adobe (Macropedia) Authorware - Binary File', '.aab'),
('application/x-authorware-map', 'Adobe (Macropedia) Authorware - Map', '.aam'),
('application/x-authorware-seg', 'Adobe (Macropedia) Authorware - Segment File', '.aas'),
('application/x-bcpio', 'Binary CPIO Archive', '.bcpio'),
('application/x-bittorrent', 'BitTorrent', '.torrent'),
('application/x-bzip', 'Bzip Archive', '.bz'),
('application/x-bzip2', 'Bzip2 Archive', '.bz2'),
('application/x-cdlink', 'Video CD', '.vcd'),
('application/x-chat', 'pIRCh', '.chat'),
('application/x-chess-pgn', 'Portable Game Notation (Chess Games)', '.pgn'),
('application/x-cpio', 'CPIO Archive', '.cpio'),
('application/x-csh', 'C Shell Script', '.csh'),
('application/x-debian-package', 'Debian Package', '.deb'),
('application/x-director', 'Adobe Shockwave Player', '.dir'),
('application/x-doom', 'Doom Video Game', '.wad'),
('application/x-dtbncx+xml', 'Navigation Control file for XML (for ePub)', '.ncx'),
('application/x-dtbook+xml', 'Digital Talking Book', '.dtb'),
('application/x-dtbresource+xml', 'Digital Talking Book - Resource File', '.res'),
('application/x-dvi', 'Device Independent File Format (DVI)', '.dvi'),
('application/x-font-bdf', 'Glyph Bitmap Distribution Format', '.bdf'),
('application/x-font-ghostscript', 'Ghostscript Font', '.gsf'),
('application/x-font-linux-psf', 'PSF Fonts', '.psf'),
('application/x-font-otf', 'OpenType Font File', '.otf'),
('application/x-font-pcf', 'Portable Compiled Format', '.pcf'),
('application/x-font-snf', 'Server Normal Format', '.snf'),
('application/x-font-ttf', 'TrueType Font', '.ttf'),
('application/x-font-type1', 'PostScript Fonts', '.pfa'),
('application/x-font-woff', 'Web Open Font Format', '.woff'),
('application/x-futuresplash', 'FutureSplash Animator', '.spl'),
('application/x-gnumeric', 'Gnumeric', '.gnumeric'),
('application/x-gtar', 'GNU Tar Files', '.gtar'),
('application/x-hdf', 'Hierarchical Data Format', '.hdf'),
('application/x-java-jnlp-file', 'Java Network Launching Protocol', '.jnlp'),
('application/x-latex', 'LaTeX', '.latex'),
('application/x-mobipocket-ebook', 'Mobipocket', '.prc'),
('application/x-ms-application', 'Microsoft ClickOnce', '.application'),
('application/x-ms-wmd', 'Microsoft Windows Media Player Download Package', '.wmd'),
('application/x-ms-wmz', 'Microsoft Windows Media Player Skin Package', '.wmz'),
('application/x-ms-xbap', 'Microsoft XAML Browser Application', '.xbap'),
('application/x-msaccess', 'Microsoft Access', '.mdb'),
('application/x-msbinder', 'Microsoft Office Binder', '.obd'),
('application/x-mscardfile', 'Microsoft Information Card', '.crd'),
('application/x-msclip', 'Microsoft Clipboard Clip', '.clp'),
('application/x-msdownload', 'Microsoft Application', '.exe'),
('application/x-msmediaview', 'Microsoft MediaView', '.mvb'),
('application/x-msmetafile', 'Microsoft Windows Metafile', '.wmf'),
('application/x-msmoney', 'Microsoft Money', '.mny'),
('application/x-mspublisher', 'Microsoft Publisher', '.pub'),
('application/x-msschedule', 'Microsoft Schedule+', '.scd'),
('application/x-msterminal', 'Microsoft Windows Terminal Services', '.trm'),
('application/x-mswrite', 'Microsoft Wordpad', '.wri'),
('application/x-netcdf', 'Network Common Data Form (NetCDF)', '.nc'),
('application/x-pkcs12', 'PKCS #12 - Personal Information Exchange Syntax Standard', '.p12'),
('application/x-pkcs7-certificates', 'PKCS #7 - Cryptographic Message Syntax Standard (Certificates)', '.p7b'),
('application/x-pkcs7-certreqresp', 'PKCS #7 - Cryptographic Message Syntax Standard (Certificate Request Response)', '.p7r'),
('application/x-rar-compressed', 'RAR Archive', '.rar'),
('application/x-sh', 'Bourne Shell Script', '.sh'),
('application/x-shar', 'Shell Archive', '.shar'),
('application/x-shockwave-flash', 'Adobe Flash', '.swf'),
('application/x-silverlight-app', 'Microsoft Silverlight', '.xap'),
('application/x-stuffit', 'Stuffit Archive', '.sit'),
('application/x-stuffitx', 'Stuffit Archive', '.sitx'),
('application/x-sv4cpio', 'System V Release 4 CPIO Archive', '.sv4cpio'),
('application/x-sv4crc', 'System V Release 4 CPIO Checksum Data', '.sv4crc'),
('application/x-tar', 'Tar File (Tape Archive)', '.tar'),
('application/x-tcl', 'Tcl Script', '.tcl'),
('application/x-tex', 'TeX', '.tex'),
('application/x-tex-tfm', 'TeX Font Metric', '.tfm'),
('application/x-texinfo', 'GNU Texinfo Document', '.texinfo'),
('application/x-ustar', 'Ustar (Uniform Standard Tape Archive)', '.ustar'),
('application/x-wais-source', 'WAIS Source', '.src'),
('application/x-x509-ca-cert', 'X.509 Certificate', '.der'),
('application/x-xfig', 'Xfig', '.fig'),
('application/x-xpinstall', 'XPInstall - Mozilla', '.xpi'),
('application/xcap-diff+xml', 'XML Configuration Access Protocol - XCAP Diff', '.xdf'),
('application/xenc+xml', 'XML Encryption Syntax and Processing', '.xenc'),
('application/xhtml+xml', 'XHTML - The Extensible HyperText Markup Language', '.xhtml'),
('application/xml', 'XML - Extensible Markup Language', '.xml'),
('application/xml-dtd', 'Document Type Definition', '.dtd'),
('application/xop+xml', 'XML-Binary Optimized Packaging', '.xop'),
('application/xslt+xml', 'XML Transformations', '.xslt'),
('application/xspf+xml', 'XSPF - XML Shareable Playlist Format', '.xspf'),
('application/xv+xml', 'MXML', '.mxml'),
('application/yang', 'YANG Data Modeling Language', '.yang'),
('application/yin+xml', 'YIN (YANG - XML)', '.yin'),
('application/zip', 'Zip Archive', '.zip'),
('audio/adpcm', 'Adaptive differential pulse-code modulation', '.adp'),
('audio/basic', 'Sun Audio - Au file format', '.au'),
('audio/midi', 'MIDI - Musical Instrument Digital Interface', '.mid'),
('audio/mp4', 'MPEG-4 Audio', '.mp4a'),
('audio/mpeg', 'MPEG Audio', '.mpga'),
('audio/ogg', 'Ogg Audio', '.oga'),
('audio/vnd.dece.audio', 'DECE Audio', '.uva'),
('audio/vnd.digital-winds', 'Digital Winds Music', '.eol'),
('audio/vnd.dra', 'DRA Audio', '.dra'),
('audio/vnd.dts', 'DTS Audio', '.dts'),
('audio/vnd.dts.hd', 'DTS High Definition Audio', '.dtshd'),
('audio/vnd.lucent.voice', 'Lucent Voice', '.lvp'),
('audio/vnd.ms-playready.media.pya', 'Microsoft PlayReady Ecosystem', '.pya'),
('audio/vnd.nuera.ecelp4800', 'Nuera ECELP 4800', '.ecelp4800'),
('audio/vnd.nuera.ecelp7470', 'Nuera ECELP 7470', '.ecelp7470'),
('audio/vnd.nuera.ecelp9600', 'Nuera ECELP 9600', '.ecelp9600'),
('audio/vnd.rip', 'Hit''n''Mix', '.rip'),
('audio/webm', 'Open Web Media Project - Audio', '.weba'),
('audio/x-aac', 'Advanced Audio Coding (AAC)', '.aac'),
('audio/x-aiff', 'Audio Interchange File Format', '.aif'),
('audio/x-mpegurl', 'M3U (Multimedia Playlist)', '.m3u'),
('audio/x-ms-wax', 'Microsoft Windows Media Audio Redirector', '.wax'),
('audio/x-ms-wma', 'Microsoft Windows Media Audio', '.wma'),
('audio/x-pn-realaudio', 'Real Audio Sound', '.ram'),
('audio/x-pn-realaudio-plugin', 'Real Audio Sound', '.rmp'),
('audio/x-wav', 'Waveform Audio File Format (WAV)', '.wav'),
('chemical/x-cdx', 'ChemDraw eXchange file', '.cdx'),
('chemical/x-cif', 'Crystallographic Interchange Format', '.cif'),
('chemical/x-cmdf', 'CrystalMaker Data Format', '.cmdf'),
('chemical/x-cml', 'Chemical Markup Language', '.cml'),
('chemical/x-csml', 'Chemical Style Markup Language', '.csml'),
('chemical/x-xyz', 'XYZ File Format', '.xyz'),
('image/bmp', 'Bitmap Image File', '.bmp'),
('image/cgm', 'Computer Graphics Metafile', '.cgm'),
('image/g3fax', 'G3 Fax Image', '.g3'),
('image/gif', 'Graphics Interchange Format', '.gif'),
('image/ief', 'Image Exchange Format', '.ief'),
('image/jpeg', 'JPEG Image', '.jpeg, .jpg'),
('image/ktx', 'OpenGL Textures (KTX)', '.ktx'),
('image/png', 'Portable Network Graphics (PNG)', '.png'),
('image/prs.btif', 'BTIF', '.btif'),
('image/svg+xml', 'Scalable Vector Graphics (SVG)', '.svg'),
('image/tiff', 'Tagged Image File Format', '.tiff'),
('image/vnd.adobe.photoshop', 'Photoshop Document', '.psd'),
('image/vnd.dece.graphic', 'DECE Graphic', '.uvi'),
('image/vnd.djvu', 'DjVu', '.djvu'),
('image/vnd.dvb.subtitle', 'Close Captioning - Subtitle', '.sub'),
('image/vnd.dwg', 'DWG Drawing', '.dwg'),
('image/vnd.dxf', 'AutoCAD DXF', '.dxf'),
('image/vnd.fastbidsheet', 'FastBid Sheet', '.fbs'),
('image/vnd.fpx', 'FlashPix', '.fpx'),
('image/vnd.fst', 'FAST Search & Transfer ASA', '.fst'),
('image/vnd.fujixerox.edmics-mmr', 'EDMICS 2000', '.mmr'),
('image/vnd.fujixerox.edmics-rlc', 'EDMICS 2000', '.rlc'),
('image/vnd.ms-modi', 'Microsoft Document Imaging Format', '.mdi'),
('image/vnd.net-fpx', 'FlashPix', '.npx'),
('image/vnd.wap.wbmp', 'WAP Bitamp (WBMP)', '.wbmp'),
('image/vnd.xiff', 'eXtended Image File Format (XIFF)', '.xif'),
('image/webp', 'WebP Image', '.webp'),
('image/x-cmu-raster', 'CMU Image', '.ras'),
('image/x-cmx', 'Corel Metafile Exchange (CMX)', '.cmx'),
('image/x-freehand', 'FreeHand MX', '.fh'),
('image/x-icon', 'Icon Image', '.ico'),
('image/x-pcx', 'PCX Image', '.pcx'),
('image/x-pict', 'PICT Image', '.pic'),
('image/x-portable-anymap', 'Portable Anymap Image', '.pnm'),
('image/x-portable-bitmap', 'Portable Bitmap Format', '.pbm'),
('image/x-portable-graymap', 'Portable Graymap Format', '.pgm'),
('image/x-portable-pixmap', 'Portable Pixmap Format', '.ppm'),
('image/x-rgb', 'Silicon Graphics RGB Bitmap', '.rgb'),
('image/x-xbitmap', 'X BitMap', '.xbm'),
('image/x-xpixmap', 'X PixMap', '.xpm'),
('image/x-xwindowdump', 'X Window Dump', '.xwd'),
('message/rfc822', 'Email Message', '.eml'),
('model/iges', 'Initial Graphics Exchange Specification (IGES)', '.igs'),
('model/mesh', 'Mesh Data Type', '.msh'),
('model/vnd.collada+xml', 'COLLADA', '.dae'),
('model/vnd.dwf', 'Autodesk Design Web Format (DWF)', '.dwf'),
('model/vnd.gdl', 'Geometric Description Language (GDL)', '.gdl'),
('model/vnd.gtw', 'Gen-Trix Studio', '.gtw'),
('model/vnd.mts', 'Virtue MTS', '.mts'),
('model/vnd.vtu', 'Virtue VTU', '.vtu'),
('model/vrml', 'Virtual Reality Modeling Language', '.wrl'),
('text/calendar', 'iCalendar', '.ics'),
('text/css', 'Cascading Style Sheets (CSS)', '.css'),
('text/csv', 'Comma-Seperated Values', '.csv'),
('text/html', 'HyperText Markup Language (HTML)', '.html'),
('text/n3', 'Notation3', '.n3'),
('text/plain', 'Text File', '.txt'),
('text/plain-bas', 'BAS Partitur Format', '.par'),
('text/prs.lines.tag', 'PRS Lines Tag', '.dsc'),
('text/richtext', 'Rich Text Format (RTF)', '.rtx'),
('text/sgml', 'Standard Generalized Markup Language (SGML)', '.sgml'),
('text/tab-separated-values', 'Tab Seperated Values', '.tsv'),
('text/troff', 'troff', '.t'),
('text/turtle', 'Turtle (Terse RDF Triple Language)', '.ttl'),
('text/uri-list', 'URI Resolution Services', '.uri'),
('text/vnd.curl', 'Curl - Applet', '.curl'),
('text/vnd.curl.dcurl', 'Curl - Detached Applet', '.dcurl'),
('text/vnd.curl.mcurl', 'Curl - Manifest File', '.mcurl'),
('text/vnd.curl.scurl', 'Curl - Source Code', '.scurl'),
('text/vnd.fly', 'mod_fly / fly.cgi', '.fly'),
('text/vnd.fmi.flexstor', 'FLEXSTOR', '.flx'),
('text/vnd.graphviz', 'Graphviz', '.gv'),
('text/vnd.in3d.3dml', 'In3D - 3DML', '.3dml'),
('text/vnd.in3d.spot', 'In3D - 3DML', '.spot'),
('text/vnd.sun.j2me.app-descriptor', 'J2ME App Descriptor', '.jad'),
('text/vnd.wap.wml', 'Wireless Markup Language (WML)', '.wml'),
('text/vnd.wap.wmlscript', 'Wireless Markup Language Script (WMLScript)', '.wmls'),
('text/x-asm', 'Assembler Source File', '.s'),
('text/x-c', 'C Source File', '.c'),
('text/x-fortran', 'Fortran Source File', '.f'),
('text/x-java-source,java', 'Java Source File', '.java'),
('text/x-pascal', 'Pascal Source File', '.p'),
('text/x-setext', 'Setext', '.etx'),
('text/x-uuencode', 'UUEncode', '.uu'),
('text/x-vcalendar', 'vCalendar', '.vcs'),
('text/x-vcard', 'vCard', '.vcf'),
('text/yaml', 'YAML Ain''t Markup Language / Yet Another Markup Language', '.yaml'),
('video/3gpp', '3GP', '.3gp'),
('video/3gpp2', '3GP2', '.3g2'),
('video/h261', 'H.261', '.h261'),
('video/h263', 'H.263', '.h263'),
('video/h264', 'H.264', '.h264'),
('video/jpeg', 'JPGVideo', '.jpgv'),
('video/jpm', 'JPEG 2000 Compound Image File Format', '.jpm'),
('video/mj2', 'Motion JPEG 2000', '.mj2'),
('video/mp4', 'MPEG-4 Video', '.mp4'),
('video/mpeg', 'MPEG Video', '.mpeg'),
('video/ogg', 'Ogg Video', '.ogv'),
('video/quicktime', 'Quicktime Video', '.qt'),
('video/vnd.dece.hd', 'DECE High Definition Video', '.uvh'),
('video/vnd.dece.mobile', 'DECE Mobile Video', '.uvm'),
('video/vnd.dece.pd', 'DECE PD Video', '.uvp'),
('video/vnd.dece.sd', 'DECE SD Video', '.uvs'),
('video/vnd.dece.video', 'DECE Video', '.uvv'),
('video/vnd.fvt', 'FAST Search & Transfer ASA', '.fvt'),
('video/vnd.mpegurl', 'MPEG Url', '.mxu'),
('video/vnd.ms-playready.media.pyv', 'Microsoft PlayReady Ecosystem Video', '.pyv'),
('video/vnd.uvvu.mp4', 'DECE MP4', '.uvu'),
('video/vnd.vivo', 'Vivo', '.viv'),
('video/webm', 'Open Web Media Project - Video', '.webm'),
('video/x-f4v', 'Flash Video', '.f4v'),
('video/x-fli', 'FLI/FLC Animation Format', '.fli'),
('video/x-flv', 'Flash Video', '.flv'),
('video/x-m4v', 'M4v', '.m4v'),
('video/x-ms-asf', 'Microsoft Advanced Systems Format (ASF)', '.asf'),
('video/x-ms-wm', 'Microsoft Windows Media', '.wm'),
('video/x-ms-wmv', 'Microsoft Windows Media Video', '.wmv'),
('video/x-ms-wmx', 'Microsoft Windows Media Audio/Video Playlist', '.wmx'),
('video/x-ms-wvx', 'Microsoft Windows Media Video Playlist', '.wvx'),
('video/x-msvideo', 'Audio Video Interleave (AVI)', '.avi'),
('video/x-sgi-movie', 'SGI Movie', '.movie'),
('x-conference/x-cooltalk', 'CoolTalk', '.ice');

-- --------------------------------------------------------

--
-- Table structure for table `doc_tags`
--

CREATE TABLE `doc_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_06_01_000001_create_oauth_auth_codes_table', 1),
('2016_06_01_000002_create_oauth_access_tokens_table', 1),
('2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
('2016_06_01_000004_create_oauth_clients_table', 1),
('2016_06_01_000005_create_oauth_personal_access_clients_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `notifiable_id` int(10) UNSIGNED NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('5751d787b60f470a344b23aa41f10b7a1f61fd005cb5a0ac28da11609dde0aa38435bb4052e55d6e', 16, 2, NULL, '[]', 0, '2017-10-17 09:59:12', '2017-10-17 09:59:12', '2017-11-01 12:59:11');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, ' Personal Access Client', 'bO0JgMdinOwoheHgn1MpgIAX7Q4Z8b4GWv3AYAWM', 'http://charity-test.info', 1, 0, 0, '2016-09-03 16:45:56', '2016-09-03 16:45:56'),
(2, NULL, ' Password Grant Client', 'XEg14aHqrdJmU42LFk1w0MVaUGbYDF91MhOahuLa', 'http://charity-test.info', 0, 1, 0, '2016-09-03 16:45:56', '2016-09-03 16:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2016-09-03 16:45:56', '2016-09-03 16:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `oauth_refresh_tokens`
--

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('ea253c7cd16c0d3705400b9c2d1f1bfbc470f10f69fb8064f465ff61b4c4206a2006d1ffe1351391', '5751d787b60f470a344b23aa41f10b7a1f61fd005cb5a0ac28da11609dde0aa38435bb4052e55d6e', 0, '2017-11-16 12:59:11');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure for view `auth_users_permissions_view`
--
DROP TABLE IF EXISTS `auth_users_permissions_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `auth_users_permissions_view`  AS  select `charity`.`char_roles_permissions`.`permission_id` AS `permission_id`,`charity`.`char_permissions`.`name` AS `name`,`charity`.`char_permissions`.`code` AS `code`,`charity`.`char_permissions`.`module_id` AS `module_id`,`charity`.`char_roles_permissions`.`created_at` AS `created_at`,`charity`.`char_users_roles`.`user_id` AS `user_id`,`charity`.`char_roles_permissions`.`role_id` AS `role_id`,`charity`.`char_roles`.`name` AS `role_name`,`charity`.`char_roles_permissions`.`allow` AS `allow`,`charity`.`char_roles_permissions`.`created_by` AS `created_by`,`charity`.`char_users`.`firstname` AS `created_by_firstname`,`charity`.`char_users`.`lastname` AS `created_by_lastname` from ((((`charity`.`char_users_roles` join `charity`.`char_roles_permissions` on((`charity`.`char_users_roles`.`role_id` = `charity`.`char_roles_permissions`.`role_id`))) join `charity`.`char_roles` on((`charity`.`char_users_roles`.`role_id` = `charity`.`char_roles`.`id`))) join `charity`.`char_permissions` on((`charity`.`char_roles_permissions`.`permission_id` = `charity`.`char_permissions`.`id`))) left join `charity`.`char_users` on((`charity`.`char_roles_permissions`.`created_by` = `charity`.`char_users`.`id`))) union all select `charity`.`char_users_permissions`.`permission_id` AS `permission_id`,`charity`.`char_permissions`.`name` AS `name`,`charity`.`char_permissions`.`code` AS `code`,`charity`.`char_permissions`.`module_id` AS `module_id`,`charity`.`char_users_permissions`.`created_at` AS `created_at`,`charity`.`char_users_permissions`.`user_id` AS `user_id`,NULL AS `role_id`,NULL AS `role_name`,`charity`.`char_users_permissions`.`allow` AS `allow`,`charity`.`char_users_permissions`.`created_by` AS `created_by`,`charity`.`char_users`.`firstname` AS `created_by_firstname`,`charity`.`char_users`.`lastname` AS `created_by_lastname` from ((`charity`.`char_users_permissions` join `charity`.`char_permissions` on((`charity`.`char_users_permissions`.`permission_id` = `charity`.`char_permissions`.`id`))) left join `charity`.`char_users` on((`charity`.`char_users_permissions`.`created_by` = `charity`.`char_users`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `char_work_jobs_view`
--
DROP TABLE IF EXISTS `char_work_jobs_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `char_work_jobs_view`  AS  select `a`.`id` AS `id`,`a`.`weight` AS `weight`,`b`.`language_id` AS `language_id`,`b`.`name` AS `name` from (`charity`.`char_work_jobs` `a` join `charity`.`char_work_jobs_i18n` `b` on((`a`.`id` = `b`.`work_job_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `char_work_reasons_view`
--
DROP TABLE IF EXISTS `char_work_reasons_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `char_work_reasons_view`  AS  select `a`.`id` AS `id`,`a`.`weight` AS `weight`,`b`.`language_id` AS `language_id`,`b`.`name` AS `name` from (`charity`.`char_work_reasons` `a` join `charity`.`char_work_reasons_i18n` `b` on((`a`.`id` = `b`.`work_reason_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `char_work_status_view`
--
DROP TABLE IF EXISTS `char_work_status_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `char_work_status_view`  AS  select `a`.`id` AS `id`,`a`.`weight` AS `weight`,`b`.`language_id` AS `language_id`,`b`.`name` AS `name` from (`charity`.`char_work_status` `a` join `charity`.`char_work_status_i18n` `b` on((`a`.`id` = `b`.`work_status_id`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aid_categories`
--
ALTER TABLE `aid_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aid_projects`
--
ALTER TABLE `aid_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aid_projects_1_idx` (`repository_id`),
  ADD KEY `fk_aid_projects_2_idx` (`sponsor_id`),
  ADD KEY `fk_aid_projects_3_idx` (`organization_id`),
  ADD KEY `fk_aid_projects_4_idx` (`category_id`);

--
-- Indexes for table `aid_projects_organizations`
--
ALTER TABLE `aid_projects_organizations`
  ADD PRIMARY KEY (`project_id`,`organization_id`),
  ADD KEY `fk_projects_organizations_1_idx` (`project_id`),
  ADD KEY `fk_projects_organizations_2_idx` (`organization_id`);

--
-- Indexes for table `aid_projects_organizations_meta`
--
ALTER TABLE `aid_projects_organizations_meta`
  ADD PRIMARY KEY (`project_id`,`organization_id`,`meta_id`),
  ADD KEY `fk_aid_projects_organizations_meta_1_idx` (`project_id`),
  ADD KEY `fk_aid_projects_organizations_meta_2_idx` (`organization_id`),
  ADD KEY `fk_aid_projects_organizations_meta_3_idx` (`meta_id`);

--
-- Indexes for table `aid_projects_persons`
--
ALTER TABLE `aid_projects_persons`
  ADD PRIMARY KEY (`project_id`,`person_id`),
  ADD KEY `fk_aid_projects_persons_1_idx` (`project_id`),
  ADD KEY `fk_aid_projects_persons_2_idx` (`person_id`),
  ADD KEY `fk_aid_projects_organization_id_idx` (`organization_id`),
  ADD KEY `fk_aid_projects_nominated_organization_id_idx` (`nominated_organization_id`);

--
-- Indexes for table `aid_projects_rules`
--
ALTER TABLE `aid_projects_rules`
  ADD PRIMARY KEY (`project_id`,`rule_name`);

--
-- Indexes for table `aid_repositories`
--
ALTER TABLE `aid_repositories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aid_repositories_meta`
--
ALTER TABLE `aid_repositories_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aid_repositories_meta_1_idx` (`repository_id`);

--
-- Indexes for table `app_logs`
--
ALTER TABLE `app_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `app_modules`
--
ALTER TABLE `app_modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`);

--
-- Indexes for table `app_routes`
--
ALTER TABLE `app_routes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_routes_module_idx` (`module_id`),
  ADD KEY `fk_routes_permission_idx` (`permission_id`);

--
-- Indexes for table `char_activities`
--
ALTER TABLE `char_activities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `char_activities_log`
--
ALTER TABLE `char_activities_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activitylog_userId_idx` (`user_id`),
  ADD KEY `fk_activitylog_activityId_idx` (`activity_id`);

--
-- Indexes for table `char_aid_sources`
--
ALTER TABLE `char_aid_sources`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_aid_sources_weights`
--
ALTER TABLE `char_aid_sources_weights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aid_sources_weights_1` (`aid_source_id`);

--
-- Indexes for table `char_banks`
--
ALTER TABLE `char_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_block_categories`
--
ALTER TABLE `char_block_categories`
  ADD PRIMARY KEY (`block_id`,`category_id`,`created_at`),
  ADD KEY `fk_block_category_bidx` (`block_id`),
  ADD KEY `fk_block_category_cidx` (`category_id`);

--
-- Indexes for table `char_block_id_card_number`
--
ALTER TABLE `char_block_id_card_number`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_branches`
--
ALTER TABLE `char_branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_branches_bank_id_idx` (`bank_id`);

--
-- Indexes for table `char_building_status`
--
ALTER TABLE `char_building_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_building_status_i18n`
--
ALTER TABLE `char_building_status_i18n`
  ADD PRIMARY KEY (`building_status_id`,`language_id`),
  ADD KEY `fk_building_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_cases`
--
ALTER TABLE `char_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cases_personId_idx` (`person_id`),
  ADD KEY `fk_cases_organizationId_idx` (`organization_id`),
  ADD KEY `fk_cases_userId_idx` (`user_id`),
  ADD KEY `fk_cases_categoryId_idx` (`category_id`);

--
-- Indexes for table `char_cases_essentials`
--
ALTER TABLE `char_cases_essentials`
  ADD PRIMARY KEY (`case_id`,`essential_id`),
  ADD KEY `fk_persons_essentials_eid_idx` (`essential_id`);

--
-- Indexes for table `char_cases_files`
--
ALTER TABLE `char_cases_files`
  ADD PRIMARY KEY (`case_id`,`document_type_id`),
  ADD KEY `fk_cases_files_cId_idx` (`case_id`),
  ADD KEY `fk_cases_files_doctypeId_idx` (`document_type_id`),
  ADD KEY `fk_cases_files_dId_idx` (`file_id`),
  ADD KEY `fk_cases_files_pId_idx` (`person_id`);

--
-- Indexes for table `char_cases_status_log`
--
ALTER TABLE `char_cases_status_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cases_status_log_caseId_idx` (`case_id`),
  ADD KEY `fk_cases_status_log_userId_idx` (`user_id`);

--
-- Indexes for table `char_case_needs`
--
ALTER TABLE `char_case_needs`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `char_categories`
--
ALTER TABLE `char_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_categories_document_types`
--
ALTER TABLE `char_categories_document_types`
  ADD PRIMARY KEY (`category_id`,`document_type_id`),
  ADD KEY `fk_categories_document_categoryId_idx` (`category_id`),
  ADD KEY `fk_categories_document_document_type_id` (`document_type_id`);

--
-- Indexes for table `char_categories_forms_sections`
--
ALTER TABLE `char_categories_forms_sections`
  ADD PRIMARY KEY (`id`,`category_id`),
  ADD KEY `fk_forms_sections_categoryId_idx` (`category_id`);

--
-- Indexes for table `char_categories_form_element_priority`
--
ALTER TABLE `char_categories_form_element_priority`
  ADD PRIMARY KEY (`category_id`,`element_id`),
  ADD KEY `fk__categories_form_element_priority_cidx` (`category_id`),
  ADD KEY `fk__categories_form_element_priority_pidx` (`element_id`);

--
-- Indexes for table `char_categories_policy`
--
ALTER TABLE `char_categories_policy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categories_policy_cidx` (`category_id`);

--
-- Indexes for table `char_currencies`
--
ALTER TABLE `char_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_currencies_exchange`
--
ALTER TABLE `char_currencies_exchange`
  ADD PRIMARY KEY (`from_currency_id`,`to_currency_id`,`rate_date`),
  ADD KEY `currencies_exchange_tid_idx` (`to_currency_id`);

--
-- Indexes for table `char_death_causes`
--
ALTER TABLE `char_death_causes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_death_causes_i18n`
--
ALTER TABLE `char_death_causes_i18n`
  ADD PRIMARY KEY (`death_cause_id`,`language_id`),
  ADD KEY `fk_death_causes_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_diseases`
--
ALTER TABLE `char_diseases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_diseases_i18n`
--
ALTER TABLE `char_diseases_i18n`
  ADD PRIMARY KEY (`disease_id`,`language_id`),
  ADD KEY `fk_diseases_language_id_idx` (`language_id`);

--
-- Indexes for table `char_documents`
--
ALTER TABLE `char_documents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `filename_UNIQUE` (`filename`),
  ADD KEY `fk_documents_typeId_idx` (`document_type_id`);

--
-- Indexes for table `char_document_types`
--
ALTER TABLE `char_document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_document_types_i18n`
--
ALTER TABLE `char_document_types_i18n`
  ADD PRIMARY KEY (`document_type_id`,`language_id`),
  ADD KEY `fk_documenttypes_lId_idx` (`language_id`),
  ADD KEY `document_type_id` (`document_type_id`);

--
-- Indexes for table `char_edu_authorities`
--
ALTER TABLE `char_edu_authorities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_edu_authorities_i18n`
--
ALTER TABLE `char_edu_authorities_i18n`
  ADD PRIMARY KEY (`edu_authority_id`,`language_id`),
  ADD KEY `fk_edu_authorities_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_edu_degrees`
--
ALTER TABLE `char_edu_degrees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_edu_degrees_i18n`
--
ALTER TABLE `char_edu_degrees_i18n`
  ADD PRIMARY KEY (`edu_degree_id`,`language_id`),
  ADD KEY `fk_char_edu_degrees_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_edu_stages`
--
ALTER TABLE `char_edu_stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_edu_stages_i18n`
--
ALTER TABLE `char_edu_stages_i18n`
  ADD PRIMARY KEY (`edu_stage_id`,`language_id`),
  ADD KEY `fk_char_edu_stages_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_essentials`
--
ALTER TABLE `char_essentials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_essentials_weights`
--
ALTER TABLE `char_essentials_weights`
  ADD PRIMARY KEY (`essential_id`,`value`);

--
-- Indexes for table `char_forms`
--
ALTER TABLE `char_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_forms_org_id_idx` (`organization_id`);

--
-- Indexes for table `char_forms_cases_data`
--
ALTER TABLE `char_forms_cases_data`
  ADD PRIMARY KEY (`case_id`,`element_id`),
  ADD KEY `fk_forms_cases_case_id_idx` (`case_id`),
  ADD KEY `fk_forms_cases_option_id_idx` (`option_id`),
  ADD KEY `fk_forms_cases_element_id` (`element_id`);

--
-- Indexes for table `char_forms_elements`
--
ALTER TABLE `char_forms_elements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_elements_form_id_idx` (`form_id`);

--
-- Indexes for table `char_forms_elements_options`
--
ALTER TABLE `char_forms_elements_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_options_elemet_id_idx` (`element_id`);

--
-- Indexes for table `char_furniture_status`
--
ALTER TABLE `char_furniture_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_furniture_status_i18n`
--
ALTER TABLE `char_furniture_status_i18n`
  ADD PRIMARY KEY (`furniture_status_id`,`language_id`),
  ADD KEY `fk_furniture_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_guardians`
--
ALTER TABLE `char_guardians`
  ADD PRIMARY KEY (`guardian_id`,`individual_id`),
  ADD KEY `fk_guardians_iId_idx` (`individual_id`),
  ADD KEY `fk_guardians_kinshipid_idx` (`kinship_id`),
  ADD KEY `fk_guardians_organizationid_idx` (`organization_id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `char_habitable_status`
--
ALTER TABLE `char_habitable_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_habitable_status_i18n`
--
ALTER TABLE `char_habitable_status_i18n`
  ADD PRIMARY KEY (`habitable_status_id`,`language_id`),
  ADD KEY `fk_habitable_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_house_status`
--
ALTER TABLE `char_house_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_house_status_i18n`
--
ALTER TABLE `char_house_status_i18n`
  ADD PRIMARY KEY (`house_status_id`,`language_id`),
  ADD KEY `fk_house_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_kinship`
--
ALTER TABLE `char_kinship`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_kinship_i18n`
--
ALTER TABLE `char_kinship_i18n`
  ADD PRIMARY KEY (`kinship_id`,`language_id`),
  ADD KEY `fk_kinship_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_languages`
--
ALTER TABLE `char_languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_locations`
--
ALTER TABLE `char_locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_locations_parentId_idx` (`parent_id`),
  ADD KEY `idx_locations_type` (`location_type`);

--
-- Indexes for table `char_locations_i18n`
--
ALTER TABLE `char_locations_i18n`
  ADD PRIMARY KEY (`location_id`,`language_id`),
  ADD KEY `fk_locations_i18n_language_id_idx` (`language_id`);

--
-- Indexes for table `char_marital_status`
--
ALTER TABLE `char_marital_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_marital_status_i18n`
--
ALTER TABLE `char_marital_status_i18n`
  ADD PRIMARY KEY (`marital_status_id`,`language_id`),
  ADD KEY `fk_marital_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_notifications`
--
ALTER TABLE `char_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notifications_userId_idx` (`user_id`);

--
-- Indexes for table `char_organizations`
--
ALTER TABLE `char_organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkorganizations_district_id_idx` (`district_id`),
  ADD KEY `fkorganizations_city_id_idx` (`city_id`),
  ADD KEY `fkorganizations_location_id_idx` (`location_id`);

--
-- Indexes for table `char_organizations_closure`
--
ALTER TABLE `char_organizations_closure`
  ADD PRIMARY KEY (`ancestor_id`,`descendant_id`);

--
-- Indexes for table `char_organizations_sms_providers`
--
ALTER TABLE `char_organizations_sms_providers`
  ADD PRIMARY KEY (`provider_id`,`organization_id`),
  ADD KEY `fk_sms_providers_lId_idx` (`provider_id`),
  ADD KEY `fk_sms_providers_org_id_idx` (`organization_id`);

--
-- Indexes for table `char_organization_neighborhoods_ratios`
--
ALTER TABLE `char_organization_neighborhoods_ratios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_org_neighborhoods_ratios_organizationId_idx` (`organization_id`),
  ADD KEY `fk_org_neighborhoods_ratios_locationId_idx` (`location_id`);

--
-- Indexes for table `char_organization_persons_banks`
--
ALTER TABLE `char_organization_persons_banks`
  ADD PRIMARY KEY (`person_id`,`bank_id`,`organization_id`),
  ADD KEY `fk_org_persons_banks_bank_id_idx` (`bank_id`),
  ADD KEY `fk_org_persons_banks_person_id_idx` (`person_id`),
  ADD KEY `fk_org_persons_banks_organization_id_idx` (`organization_id`);

--
-- Indexes for table `char_payments`
--
ALTER TABLE `char_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_payments_currencyId_idx` (`currency_id`),
  ADD KEY `fk_payments_sponsorshipId_idx` (`sponsorship_id`),
  ADD KEY `fk_payments_categoryId_idx` (`category_id`),
  ADD KEY `fk_payments_organization_id_idx` (`organization_id`),
  ADD KEY `fk_payments_sponsor_id_idx` (`sponsor_id`),
  ADD KEY `fk_payments_bankId_idx` (`bank_id`);

--
-- Indexes for table `char_payments_cases`
--
ALTER TABLE `char_payments_cases`
  ADD PRIMARY KEY (`payment_id`,`sponsor_number`),
  ADD KEY `fk_payments_cases_caseId_idx` (`case_id`),
  ADD KEY `fk_payments_cases_sponsor_number_numx` (`sponsor_number`),
  ADD KEY `fk_payments_persons_guardianid_idx_id` (`guardian_id`);

--
-- Indexes for table `char_payments_documents`
--
ALTER TABLE `char_payments_documents`
  ADD PRIMARY KEY (`payment_id`,`document_id`),
  ADD KEY `fk__payments_documents_docid_idx` (`document_id`),
  ADD KEY `fk_payments_documents_personId_idx` (`person_id`);

--
-- Indexes for table `char_payments_persons`
--
ALTER TABLE `char_payments_persons`
  ADD PRIMARY KEY (`payment_id`,`sponsor_number`),
  ADD KEY `fk_payments_persons_prsnid_idx` (`person_id`),
  ADD KEY `fk_payments_persons_sponsor_number_numx` (`sponsor_number`),
  ADD KEY `fk_payments_persons_guardianid_idx_id` (`guardian_id`);

--
-- Indexes for table `char_payments_recipient`
--
ALTER TABLE `char_payments_recipient`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_id` (`payment_id`,`person_id`,`father_id`,`mother_id`),
  ADD KEY `fk_payments_recipient_prsnId_idx` (`person_id`),
  ADD KEY `fk_payments_recipient_fatherId_idx` (`father_id`),
  ADD KEY `fk_payments_recipient_motherId_idx` (`mother_id`),
  ADD KEY `fk_payments_recipient_bankId_idx` (`bank_id`);

--
-- Indexes for table `char_permissions`
--
ALTER TABLE `char_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code_UNIQUE` (`code`),
  ADD KEY `fk_permissions_module_id_idx` (`module_id`);

--
-- Indexes for table `char_persons`
--
ALTER TABLE `char_persons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_card_number_UNIQUE` (`id_card_number`),
  ADD KEY `fk_persons_father_id_idx` (`father_id`),
  ADD KEY `fk_persons_mother_id_idx` (`mother_id`),
  ADD KEY `fk_persons_location_id_idx` (`location_id`),
  ADD KEY `fk_persons_death_cause_id_idx` (`death_cause_id`),
  ADD KEY `fk_persons_marital_status_id_idx` (`marital_status_id`),
  ADD KEY `fk_persons_birth_place_idx` (`birth_place`),
  ADD KEY `fk_persons_city_id_idx` (`city`),
  ADD KEY `fk_persons_country_id_idx` (`country`),
  ADD KEY `fk_persons_governarate_id_idx` (`governarate`);

--
-- Indexes for table `char_persons_aids`
--
ALTER TABLE `char_persons_aids`
  ADD PRIMARY KEY (`person_id`,`aid_source_id`,`aid_type`),
  ADD KEY `fk_persons_aids_source_id_idx` (`aid_source_id`),
  ADD KEY `fk_persons_aids_currency_idx` (`currency_id`);

--
-- Indexes for table `char_persons_banks`
--
ALTER TABLE `char_persons_banks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `BANK_ACCOUNT_NUMBER_UNIQUE` (`bank_id`,`account_number`),
  ADD KEY `fk_persons_banks_bank_id_idx` (`bank_id`),
  ADD KEY `fk_persons_banks_branch_id_idx` (`branch_name`),
  ADD KEY `fk_persons_banks_person_id` (`person_id`);

--
-- Indexes for table `char_persons_contact`
--
ALTER TABLE `char_persons_contact`
  ADD PRIMARY KEY (`person_id`,`contact_type`);

--
-- Indexes for table `char_persons_documents`
--
ALTER TABLE `char_persons_documents`
  ADD PRIMARY KEY (`person_id`,`document_type_id`),
  ADD KEY `fk_persons_documents_dId_idx` (`document_id`),
  ADD KEY `fk_persons_documents_pId_idx` (`person_id`),
  ADD KEY `fk_persons_documents_typeId_idx` (`document_type_id`);

--
-- Indexes for table `char_persons_education`
--
ALTER TABLE `char_persons_education`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `fk_persons_education_authority_id_idx` (`authority`),
  ADD KEY `fk_persons_education_stage_id_idx` (`stage`),
  ADD KEY `fk_persons_education_degree_id_idx` (`degree`);

--
-- Indexes for table `char_persons_health`
--
ALTER TABLE `char_persons_health`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `fk_persons_health_disease_id_idx` (`disease_id`);

--
-- Indexes for table `char_persons_i18n`
--
ALTER TABLE `char_persons_i18n`
  ADD PRIMARY KEY (`person_id`,`language_id`),
  ADD KEY `fk_persons_i18n_language_id_idx` (`language_id`);

--
-- Indexes for table `char_persons_islamic_commitment`
--
ALTER TABLE `char_persons_islamic_commitment`
  ADD PRIMARY KEY (`person_id`);

--
-- Indexes for table `char_persons_kinship`
--
ALTER TABLE `char_persons_kinship`
  ADD PRIMARY KEY (`l_person_id`,`r_person_id`),
  ADD KEY `fk_relationships_pId2_idx` (`r_person_id`),
  ADD KEY `fk_relationships_kinshipId_idx` (`kinship_id`);

--
-- Indexes for table `char_persons_properties`
--
ALTER TABLE `char_persons_properties`
  ADD PRIMARY KEY (`person_id`,`property_id`),
  ADD KEY `fk_persons_properties_property_id_idx` (`property_id`);

--
-- Indexes for table `char_persons_work`
--
ALTER TABLE `char_persons_work`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `fk_persons_work_status_id_idx` (`work_status_id`),
  ADD KEY `fk_persons_work_reason_id_idx` (`work_reason_id`),
  ADD KEY `fk_persons_work_wage_id_idx` (`work_wage_id`),
  ADD KEY `fk_persons_work_job_id_idx` (`work_job_id`);

--
-- Indexes for table `char_projects`
--
ALTER TABLE `char_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_projects_orgId_idx` (`organization_id`);

--
-- Indexes for table `char_projects_persons`
--
ALTER TABLE `char_projects_persons`
  ADD PRIMARY KEY (`project_id`,`person_id`),
  ADD KEY `fk_projects_nominees_2_idx` (`person_id`);

--
-- Indexes for table `char_properties`
--
ALTER TABLE `char_properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_properties_i18n`
--
ALTER TABLE `char_properties_i18n`
  ADD PRIMARY KEY (`property_id`,`language_id`),
  ADD KEY `fk_properties_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_properties_weights`
--
ALTER TABLE `char_properties_weights`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_aid_properties_weights_1` (`property_id`);

--
-- Indexes for table `char_property_types`
--
ALTER TABLE `char_property_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_property_types_i18n`
--
ALTER TABLE `char_property_types_i18n`
  ADD PRIMARY KEY (`property_type_id`,`language_id`),
  ADD KEY `fk_propertytypes_i18n_langid_idx` (`language_id`);

--
-- Indexes for table `char_reconstructions`
--
ALTER TABLE `char_reconstructions`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `char_residence`
--
ALTER TABLE `char_residence`
  ADD PRIMARY KEY (`person_id`),
  ADD KEY `fk_residence_propertyId_idx` (`property_type_id`),
  ADD KEY `fk_fk_residence_roofId_idx` (`roof_material_id`);

--
-- Indexes for table `char_roles`
--
ALTER TABLE `char_roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_roles_organizationId_idx` (`organization_id`);

--
-- Indexes for table `char_roles_permissions`
--
ALTER TABLE `char_roles_permissions`
  ADD PRIMARY KEY (`role_id`,`permission_id`),
  ADD KEY `fk_rolesPermissions_permissionId_idx` (`permission_id`);

--
-- Indexes for table `char_roof_materials`
--
ALTER TABLE `char_roof_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_roof_materials_i18n`
--
ALTER TABLE `char_roof_materials_i18n`
  ADD PRIMARY KEY (`roof_material_id`,`language_id`),
  ADD KEY `fkr_roof_materials_langid_idx` (`language_id`);

--
-- Indexes for table `char_settings`
--
ALTER TABLE `char_settings`
  ADD PRIMARY KEY (`id`,`organization_id`),
  ADD UNIQUE KEY `ORG_SETTING_UNIQUE` (`id`,`organization_id`),
  ADD KEY `fk_settings_organizationId_idx` (`organization_id`);

--
-- Indexes for table `char_sms_providers`
--
ALTER TABLE `char_sms_providers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_sponsorships`
--
ALTER TABLE `char_sponsorships`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `number_UNIQUE` (`sponsorship_number`),
  ADD KEY `fk_sponsorships_sponsorId_idx` (`sponsor_id`),
  ADD KEY `fk_sponsorships_orgId` (`organization_id`),
  ADD KEY `fk_sponsorships_catId_idx` (`category_id`),
  ADD KEY `fk_sponsorships_userId_idx` (`user_id`);

--
-- Indexes for table `char_sponsorships_persons`
--
ALTER TABLE `char_sponsorships_persons`
  ADD PRIMARY KEY (`sponsorship_id`,`person_id`),
  ADD KEY `fk_sponsorships_persons_pid_idx` (`person_id`);

--
-- Indexes for table `char_sponsorship_cases`
--
ALTER TABLE `char_sponsorship_cases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sponsor_case_UNIQUE` (`case_id`,`sponsor_id`,`sponsor_number`),
  ADD KEY `fksponsorship_cases_sponsor_id_idx` (`sponsor_id`),
  ADD KEY `fksponsorship_cases_case_id_idx` (`case_id`),
  ADD KEY `fksponsorship_cases_sponsorship_id_idx` (`sponsorship_id`);

--
-- Indexes for table `char_sponsorship_cases_status_log`
--
ALTER TABLE `char_sponsorship_cases_status_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fkcases_status_log_sponsorship_cases_idx` (`sponsorship_cases_id`),
  ADD KEY `fkcases_status_log_user_idx` (`user_id`);

--
-- Indexes for table `char_sponsor_persons`
--
ALTER TABLE `char_sponsor_persons`
  ADD PRIMARY KEY (`person_id`,`sponsor_id`),
  ADD KEY `fksponsor_persons_sponsor_id_idx` (`sponsor_id`),
  ADD KEY `fksponsor_persons_person_id_idx` (`person_id`);

--
-- Indexes for table `char_templates`
--
ALTER TABLE `char_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_templates_catId` (`category_id`),
  ADD KEY `fk_templates_orgId` (`organization_id`);

--
-- Indexes for table `char_update_data_setting`
--
ALTER TABLE `char_update_data_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cases_update_organizationId_idx` (`organization_id`);

--
-- Indexes for table `char_users`
--
ALTER TABLE `char_users`
  ADD PRIMARY KEY (`id`,`super_admin`),
  ADD UNIQUE KEY `username_UNIQUE` (`username`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_users_organizationId_idx` (`organization_id`),
  ADD KEY `fk_users_parentId_idx` (`parent_id`);

--
-- Indexes for table `char_users_permissions`
--
ALTER TABLE `char_users_permissions`
  ADD PRIMARY KEY (`user_id`,`permission_id`),
  ADD KEY `fk_users_permissions_permid_idx` (`permission_id`);

--
-- Indexes for table `char_users_roles`
--
ALTER TABLE `char_users_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `fk_usersRoles_roleId_idx` (`role_id`);

--
-- Indexes for table `char_verification`
--
ALTER TABLE `char_verification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_verification_userId_idx` (`user_id`);

--
-- Indexes for table `char_visitor_notes`
--
ALTER TABLE `char_visitor_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vsitor_notes_organizationId_idx` (`organization_id`),
  ADD KEY `fk_vsitor_notes_categoryId_idx` (`category_id`);

--
-- Indexes for table `char_vouchers`
--
ALTER TABLE `char_vouchers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vouchers_organizationId_idx` (`organization_id`),
  ADD KEY `fk_vouchers_categories_categoryIdx` (`category_id`),
  ADD KEY `fk_vouchers_sponsor_id_idx` (`sponsor_id`);

--
-- Indexes for table `char_vouchers_categories`
--
ALTER TABLE `char_vouchers_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_idx` (`parent`);

--
-- Indexes for table `char_vouchers_persons`
--
ALTER TABLE `char_vouchers_persons`
  ADD PRIMARY KEY (`voucher_id`,`person_id`),
  ADD KEY `fk_vouchersBeneficiaries_pId_idx` (`person_id`);

--
-- Indexes for table `char_work_jobs`
--
ALTER TABLE `char_work_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_work_jobs_i18n`
--
ALTER TABLE `char_work_jobs_i18n`
  ADD PRIMARY KEY (`work_job_id`,`language_id`),
  ADD KEY `fk_work_jobs_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_work_reasons`
--
ALTER TABLE `char_work_reasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_work_reasons_i18n`
--
ALTER TABLE `char_work_reasons_i18n`
  ADD PRIMARY KEY (`work_reason_id`,`language_id`),
  ADD KEY `fk_work_reasons_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_work_status`
--
ALTER TABLE `char_work_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `char_work_status_i18n`
--
ALTER TABLE `char_work_status_i18n`
  ADD PRIMARY KEY (`work_status_id`,`language_id`),
  ADD KEY `fk_work_status_i18n_2_idx` (`language_id`);

--
-- Indexes for table `char_work_wages`
--
ALTER TABLE `char_work_wages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_attributes`
--
ALTER TABLE `doc_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_attributes_category_id_idx` (`category_id`);

--
-- Indexes for table `doc_attributes_values`
--
ALTER TABLE `doc_attributes_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_categories_attributes_values_1_idx` (`attribute_id`);

--
-- Indexes for table `doc_categories`
--
ALTER TABLE `doc_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `fk_categories_parent_id_idx` (`parent_id`);

--
-- Indexes for table `doc_files`
--
ALTER TABLE `doc_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_files_status_id_idx` (`file_status_id`),
  ADD KEY `fk_files_category_id_idx` (`category_id`);

--
-- Indexes for table `doc_files_attributes`
--
ALTER TABLE `doc_files_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_files_attributes_1_idx` (`file_id`),
  ADD KEY `fk_files_attributes_2_idx` (`attribute_id`),
  ADD KEY `fk_files_attributes_3_idx` (`attribute_value_id`);

--
-- Indexes for table `doc_files_revisions`
--
ALTER TABLE `doc_files_revisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_files_revisions_file_id_idx` (`file_id`);

--
-- Indexes for table `doc_files_status`
--
ALTER TABLE `doc_files_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `doc_files_tags`
--
ALTER TABLE `doc_files_tags`
  ADD PRIMARY KEY (`file_id`,`tag_id`),
  ADD KEY `fk_files_tags_tag_id` (`tag_id`);

--
-- Indexes for table `doc_mimetypes`
--
ALTER TABLE `doc_mimetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doc_tags`
--
ALTER TABLE `doc_tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_id_notifiable_type_index` (`notifiable_id`,`notifiable_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aid_categories`
--
ALTER TABLE `aid_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aid_projects`
--
ALTER TABLE `aid_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aid_repositories`
--
ALTER TABLE `aid_repositories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aid_repositories_meta`
--
ALTER TABLE `aid_repositories_meta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_logs`
--
ALTER TABLE `app_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `app_modules`
--
ALTER TABLE `app_modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `app_routes`
--
ALTER TABLE `app_routes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_activities`
--
ALTER TABLE `char_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_activities_log`
--
ALTER TABLE `char_activities_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_aid_sources`
--
ALTER TABLE `char_aid_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_aid_sources_weights`
--
ALTER TABLE `char_aid_sources_weights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_banks`
--
ALTER TABLE `char_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_block_id_card_number`
--
ALTER TABLE `char_block_id_card_number`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_branches`
--
ALTER TABLE `char_branches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_building_status`
--
ALTER TABLE `char_building_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_cases`
--
ALTER TABLE `char_cases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_cases_status_log`
--
ALTER TABLE `char_cases_status_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_categories`
--
ALTER TABLE `char_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `char_categories_policy`
--
ALTER TABLE `char_categories_policy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_currencies`
--
ALTER TABLE `char_currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `char_death_causes`
--
ALTER TABLE `char_death_causes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_diseases`
--
ALTER TABLE `char_diseases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_documents`
--
ALTER TABLE `char_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_document_types`
--
ALTER TABLE `char_document_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `char_edu_authorities`
--
ALTER TABLE `char_edu_authorities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_edu_degrees`
--
ALTER TABLE `char_edu_degrees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_edu_stages`
--
ALTER TABLE `char_edu_stages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_essentials`
--
ALTER TABLE `char_essentials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_forms`
--
ALTER TABLE `char_forms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `char_forms_elements`
--
ALTER TABLE `char_forms_elements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `char_forms_elements_options`
--
ALTER TABLE `char_forms_elements_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_furniture_status`
--
ALTER TABLE `char_furniture_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_habitable_status`
--
ALTER TABLE `char_habitable_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_house_status`
--
ALTER TABLE `char_house_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_kinship`
--
ALTER TABLE `char_kinship`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'معرف يتم تحديده يدويا';
--
-- AUTO_INCREMENT for table `char_languages`
--
ALTER TABLE `char_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `char_locations`
--
ALTER TABLE `char_locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_marital_status`
--
ALTER TABLE `char_marital_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_notifications`
--
ALTER TABLE `char_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_organizations`
--
ALTER TABLE `char_organizations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `char_organization_neighborhoods_ratios`
--
ALTER TABLE `char_organization_neighborhoods_ratios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_payments`
--
ALTER TABLE `char_payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_payments_recipient`
--
ALTER TABLE `char_payments_recipient`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_permissions`
--
ALTER TABLE `char_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=371;
--
-- AUTO_INCREMENT for table `char_persons`
--
ALTER TABLE `char_persons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_persons_banks`
--
ALTER TABLE `char_persons_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_projects`
--
ALTER TABLE `char_projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_properties`
--
ALTER TABLE `char_properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_properties_weights`
--
ALTER TABLE `char_properties_weights`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_property_types`
--
ALTER TABLE `char_property_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_roles`
--
ALTER TABLE `char_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `char_roof_materials`
--
ALTER TABLE `char_roof_materials`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_sms_providers`
--
ALTER TABLE `char_sms_providers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `char_sponsorships`
--
ALTER TABLE `char_sponsorships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_sponsorship_cases`
--
ALTER TABLE `char_sponsorship_cases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_sponsorship_cases_status_log`
--
ALTER TABLE `char_sponsorship_cases_status_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_templates`
--
ALTER TABLE `char_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_update_data_setting`
--
ALTER TABLE `char_update_data_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_users`
--
ALTER TABLE `char_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `char_verification`
--
ALTER TABLE `char_verification`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_visitor_notes`
--
ALTER TABLE `char_visitor_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_vouchers`
--
ALTER TABLE `char_vouchers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_vouchers_categories`
--
ALTER TABLE `char_vouchers_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_work_jobs`
--
ALTER TABLE `char_work_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_work_reasons`
--
ALTER TABLE `char_work_reasons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_work_status`
--
ALTER TABLE `char_work_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `char_work_wages`
--
ALTER TABLE `char_work_wages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_attributes`
--
ALTER TABLE `doc_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_attributes_values`
--
ALTER TABLE `doc_attributes_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_categories`
--
ALTER TABLE `doc_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_files`
--
ALTER TABLE `doc_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_files_attributes`
--
ALTER TABLE `doc_files_attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_files_revisions`
--
ALTER TABLE `doc_files_revisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_files_status`
--
ALTER TABLE `doc_files_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `doc_tags`
--
ALTER TABLE `doc_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `aid_projects`
--
ALTER TABLE `aid_projects`
  ADD CONSTRAINT `aid_projects_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `char_vouchers_categories` (`id`),
  ADD CONSTRAINT `fk_aid_projects_1` FOREIGN KEY (`repository_id`) REFERENCES `aid_repositories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aid_projects_2` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_aid_projectss_3` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `aid_projects_organizations`
--
ALTER TABLE `aid_projects_organizations`
  ADD CONSTRAINT `aid_projects_organizations_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `aid_projects` (`id`),
  ADD CONSTRAINT `fk_projects_organizations_2` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `aid_projects_organizations_meta`
--
ALTER TABLE `aid_projects_organizations_meta`
  ADD CONSTRAINT `fk_aid_projects_organizations_meta_1` FOREIGN KEY (`project_id`) REFERENCES `aid_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_aid_projects_organizations_meta_2` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_aid_projects_organizations_meta_3` FOREIGN KEY (`meta_id`) REFERENCES `aid_repositories_meta` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aid_projects_persons`
--
ALTER TABLE `aid_projects_persons`
  ADD CONSTRAINT `fk_aid_projects_nominated_organization` FOREIGN KEY (`nominated_organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_aid_projects_organization` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_aid_projects_persons_1` FOREIGN KEY (`project_id`) REFERENCES `aid_projects` (`id`),
  ADD CONSTRAINT `fk_aid_projects_persons_2` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`);

--
-- Constraints for table `aid_projects_rules`
--
ALTER TABLE `aid_projects_rules`
  ADD CONSTRAINT `fk_projects_rules_pid` FOREIGN KEY (`project_id`) REFERENCES `aid_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `aid_repositories_meta`
--
ALTER TABLE `aid_repositories_meta`
  ADD CONSTRAINT `fk_aid_repositories_meta_1` FOREIGN KEY (`repository_id`) REFERENCES `aid_repositories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `app_routes`
--
ALTER TABLE `app_routes`
  ADD CONSTRAINT `fk_routes_module` FOREIGN KEY (`module_id`) REFERENCES `app_modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_routes_permission` FOREIGN KEY (`permission_id`) REFERENCES `char_permissions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_activities_log`
--
ALTER TABLE `char_activities_log`
  ADD CONSTRAINT `fk_activitylog_activityId` FOREIGN KEY (`activity_id`) REFERENCES `char_activities` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_activitylog_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_aid_sources_weights`
--
ALTER TABLE `char_aid_sources_weights`
  ADD CONSTRAINT `fk_aid_sources_weights_1` FOREIGN KEY (`aid_source_id`) REFERENCES `char_aid_sources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_branches`
--
ALTER TABLE `char_branches`
  ADD CONSTRAINT `fk_branches_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `char_banks` (`id`);

--
-- Constraints for table `char_building_status_i18n`
--
ALTER TABLE `char_building_status_i18n`
  ADD CONSTRAINT `fk_building_status_i18n_1` FOREIGN KEY (`building_status_id`) REFERENCES `char_building_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_building_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_cases`
--
ALTER TABLE `char_cases`
  ADD CONSTRAINT `fk_cases_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_cases_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_cases_personId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_cases_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_cases_essentials`
--
ALTER TABLE `char_cases_essentials`
  ADD CONSTRAINT `fk_persons_essentials_cid` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_essentials_eid` FOREIGN KEY (`essential_id`) REFERENCES `char_essentials` (`id`);

--
-- Constraints for table `char_cases_files`
--
ALTER TABLE `char_cases_files`
  ADD CONSTRAINT `fk_cases_files_cId_idx` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cases_files_dId_idx` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cases_files_doctypeId` FOREIGN KEY (`document_type_id`) REFERENCES `char_document_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cases_files_pId_idx` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_cases_status_log`
--
ALTER TABLE `char_cases_status_log`
  ADD CONSTRAINT `fk_cases_status_log_case_id` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cases_status_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`);

--
-- Constraints for table `char_case_needs`
--
ALTER TABLE `char_case_needs`
  ADD CONSTRAINT `fk_case_needs_case_id` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_categories_document_types`
--
ALTER TABLE `char_categories_document_types`
  ADD CONSTRAINT `fk_categoriesdoc_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_categoriesdoc_document_types_id` FOREIGN KEY (`document_type_id`) REFERENCES `char_document_types` (`id`);

--
-- Constraints for table `char_categories_forms_sections`
--
ALTER TABLE `char_categories_forms_sections`
  ADD CONSTRAINT `fk_forms_sections_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`);

--
-- Constraints for table `char_categories_form_element_priority`
--
ALTER TABLE `char_categories_form_element_priority`
  ADD CONSTRAINT `fk__categories_form_element_priority_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk__categories_form_element_priority_elementId` FOREIGN KEY (`element_id`) REFERENCES `char_forms_elements` (`id`);

--
-- Constraints for table `char_currencies_exchange`
--
ALTER TABLE `char_currencies_exchange`
  ADD CONSTRAINT `currencies_exchange_tid` FOREIGN KEY (`to_currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_currencies_exchange_fid` FOREIGN KEY (`from_currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_death_causes_i18n`
--
ALTER TABLE `char_death_causes_i18n`
  ADD CONSTRAINT `fk_death_causes_i18n_1` FOREIGN KEY (`death_cause_id`) REFERENCES `char_death_causes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_death_causes_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_diseases_i18n`
--
ALTER TABLE `char_diseases_i18n`
  ADD CONSTRAINT `fk_diseases_disease_id` FOREIGN KEY (`disease_id`) REFERENCES `char_diseases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_diseases_language_id` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_documents`
--
ALTER TABLE `char_documents`
  ADD CONSTRAINT `fk_documents_typeId` FOREIGN KEY (`document_type_id`) REFERENCES `doc_files` (`id`);

--
-- Constraints for table `char_document_types_i18n`
--
ALTER TABLE `char_document_types_i18n`
  ADD CONSTRAINT `fk_documenttypes_lId` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkchar_document_types_id` FOREIGN KEY (`document_type_id`) REFERENCES `char_document_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_edu_authorities_i18n`
--
ALTER TABLE `char_edu_authorities_i18n`
  ADD CONSTRAINT `fk_edu_authorities_i18n_1` FOREIGN KEY (`edu_authority_id`) REFERENCES `char_edu_authorities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_edu_authorities_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_edu_degrees_i18n`
--
ALTER TABLE `char_edu_degrees_i18n`
  ADD CONSTRAINT `fk_char_edu_degrees_i18n_1` FOREIGN KEY (`edu_degree_id`) REFERENCES `char_edu_degrees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_char_edu_degrees_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_edu_stages_i18n`
--
ALTER TABLE `char_edu_stages_i18n`
  ADD CONSTRAINT `fk_char_edu_stages_i18n_1` FOREIGN KEY (`edu_stage_id`) REFERENCES `char_edu_stages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_char_edu_stages_i18n_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_essentials_weights`
--
ALTER TABLE `char_essentials_weights`
  ADD CONSTRAINT `fk_essentials_weights_1` FOREIGN KEY (`essential_id`) REFERENCES `char_essentials` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_forms_cases_data`
--
ALTER TABLE `char_forms_cases_data`
  ADD CONSTRAINT `fk_forms_cases_case_id` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_forms_cases_element_id` FOREIGN KEY (`element_id`) REFERENCES `char_forms_elements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_forms_cases_option_id` FOREIGN KEY (`option_id`) REFERENCES `char_forms_elements_options` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_forms_elements`
--
ALTER TABLE `char_forms_elements`
  ADD CONSTRAINT `fk_elements_form_id` FOREIGN KEY (`form_id`) REFERENCES `char_forms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_forms_elements_options`
--
ALTER TABLE `char_forms_elements_options`
  ADD CONSTRAINT `fk_char_options_elemet_id` FOREIGN KEY (`element_id`) REFERENCES `char_forms_elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_furniture_status_i18n`
--
ALTER TABLE `char_furniture_status_i18n`
  ADD CONSTRAINT `fk_furniture_status_i18n_1` FOREIGN KEY (`furniture_status_id`) REFERENCES `char_furniture_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_furniture_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_guardians`
--
ALTER TABLE `char_guardians`
  ADD CONSTRAINT `fk_guardians_gId` FOREIGN KEY (`guardian_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_guardians_iId` FOREIGN KEY (`individual_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_guardians_kinshipid` FOREIGN KEY (`kinship_id`) REFERENCES `char_kinship` (`id`),
  ADD CONSTRAINT `fk_guardians_organizationid` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_habitable_status_i18n`
--
ALTER TABLE `char_habitable_status_i18n`
  ADD CONSTRAINT `fk_habitable_status_i18n_1` FOREIGN KEY (`habitable_status_id`) REFERENCES `char_habitable_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_habitable_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_house_status_i18n`
--
ALTER TABLE `char_house_status_i18n`
  ADD CONSTRAINT `fk_house_status_i18n_1` FOREIGN KEY (`house_status_id`) REFERENCES `char_house_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_house_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_kinship_i18n`
--
ALTER TABLE `char_kinship_i18n`
  ADD CONSTRAINT `fk_kinship_i18n_1` FOREIGN KEY (`kinship_id`) REFERENCES `char_kinship` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_kinship_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_locations`
--
ALTER TABLE `char_locations`
  ADD CONSTRAINT `fk_locations_parentId` FOREIGN KEY (`parent_id`) REFERENCES `char_locations` (`id`);

--
-- Constraints for table `char_locations_i18n`
--
ALTER TABLE `char_locations_i18n`
  ADD CONSTRAINT `fk_locations_i18n_language_id` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_locations_i18n_location_id` FOREIGN KEY (`location_id`) REFERENCES `char_locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_marital_status_i18n`
--
ALTER TABLE `char_marital_status_i18n`
  ADD CONSTRAINT `fk_marital_status_i18n_1` FOREIGN KEY (`marital_status_id`) REFERENCES `char_marital_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_marital_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_notifications`
--
ALTER TABLE `char_notifications`
  ADD CONSTRAINT `fk_notifications_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_organizations`
--
ALTER TABLE `char_organizations`
  ADD CONSTRAINT `fkorganizations_city_id` FOREIGN KEY (`city_id`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fkorganizations_district_id` FOREIGN KEY (`district_id`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fkorganizations_location_id` FOREIGN KEY (`location_id`) REFERENCES `char_locations` (`id`);

--
-- Constraints for table `char_organizations_sms_providers`
--
ALTER TABLE `char_organizations_sms_providers`
  ADD CONSTRAINT `fk_sms_providers_lId` FOREIGN KEY (`provider_id`) REFERENCES `char_sms_providers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sms_providers_org_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_organization_neighborhoods_ratios`
--
ALTER TABLE `char_organization_neighborhoods_ratios`
  ADD CONSTRAINT `fk_org_neighborhoods_ratios_locationId` FOREIGN KEY (`location_id`) REFERENCES `char_locations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_org_neighborhoods_ratios_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_organization_persons_banks`
--
ALTER TABLE `char_organization_persons_banks`
  ADD CONSTRAINT `fk_org_persons_banks_account_id` FOREIGN KEY (`bank_id`) REFERENCES `char_persons_banks` (`id`),
  ADD CONSTRAINT `fk_org_persons_banks_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_org_persons_banks_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_payments`
--
ALTER TABLE `char_payments`
  ADD CONSTRAINT `char_payments_ibfk_1` FOREIGN KEY (`bank_id`) REFERENCES `char_banks` (`id`),
  ADD CONSTRAINT `fk_payments_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_payment_category` (`id`),
  ADD CONSTRAINT `fk_payments_currencyId` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payments_orgId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payments_sponsorId` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payments_sponsorshipId` FOREIGN KEY (`sponsorship_id`) REFERENCES `char_sponsorships` (`id`);

--
-- Constraints for table `char_payments_cases`
--
ALTER TABLE `char_payments_cases`
  ADD CONSTRAINT `fk_payments_cases_caseId` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payments_cases_guardianId` FOREIGN KEY (`guardian_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_payments_cases_pymntId` FOREIGN KEY (`payment_id`) REFERENCES `char_payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_payments_documents`
--
ALTER TABLE `char_payments_documents`
  ADD CONSTRAINT `char_payments_documents_ibfk_1` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk__payments_documents_docid` FOREIGN KEY (`document_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payments_documents_pymntid` FOREIGN KEY (`payment_id`) REFERENCES `char_payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_payments_persons`
--
ALTER TABLE `char_payments_persons`
  ADD CONSTRAINT `fk_payments_persons_guardianid` FOREIGN KEY (`guardian_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_payments_persons_prsnid` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payments_persons_pymntid` FOREIGN KEY (`payment_id`) REFERENCES `char_payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_payments_recipient`
--
ALTER TABLE `char_payments_recipient`
  ADD CONSTRAINT `fk_payments_recipient_bankId` FOREIGN KEY (`bank_id`) REFERENCES `char_banks` (`id`),
  ADD CONSTRAINT `fk_payments_recipient_fatherId` FOREIGN KEY (`father_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_payments_recipient_motherId` FOREIGN KEY (`mother_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_payments_recipient_prsnId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`);

--
-- Constraints for table `char_persons`
--
ALTER TABLE `char_persons`
  ADD CONSTRAINT `fk_persons_city_id` FOREIGN KEY (`city`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fk_persons_country_id` FOREIGN KEY (`country`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fk_persons_death_cause_id` FOREIGN KEY (`death_cause_id`) REFERENCES `char_death_causes` (`id`),
  ADD CONSTRAINT `fk_persons_father_idfk_persons_father_id` FOREIGN KEY (`father_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_persons_governarate_id` FOREIGN KEY (`governarate`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fk_persons_location_id` FOREIGN KEY (`location_id`) REFERENCES `char_locations` (`id`),
  ADD CONSTRAINT `fk_persons_marital_status_id` FOREIGN KEY (`marital_status_id`) REFERENCES `char_marital_status` (`id`),
  ADD CONSTRAINT `fk_persons_mother_id` FOREIGN KEY (`mother_id`) REFERENCES `char_persons` (`id`);

--
-- Constraints for table `char_persons_aids`
--
ALTER TABLE `char_persons_aids`
  ADD CONSTRAINT `fk_persons_aids_currencyId` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`),
  ADD CONSTRAINT `fk_persons_aids_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_aids_source_id` FOREIGN KEY (`aid_source_id`) REFERENCES `char_aid_sources` (`id`);

--
-- Constraints for table `char_persons_banks`
--
ALTER TABLE `char_persons_banks`
  ADD CONSTRAINT `fk_persons_banks_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `char_banks` (`id`),
  ADD CONSTRAINT `fk_persons_banks_branch_name` FOREIGN KEY (`branch_name`) REFERENCES `char_branches` (`id`),
  ADD CONSTRAINT `fk_persons_banks_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`);

--
-- Constraints for table `char_persons_contact`
--
ALTER TABLE `char_persons_contact`
  ADD CONSTRAINT `fk_persons_contact_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_documents`
--
ALTER TABLE `char_persons_documents`
  ADD CONSTRAINT `fk_persons_documents_dId` FOREIGN KEY (`document_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_documents_pId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_documents_typeId` FOREIGN KEY (`document_type_id`) REFERENCES `char_document_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_education`
--
ALTER TABLE `char_persons_education`
  ADD CONSTRAINT `fk_persons_education_authority_id` FOREIGN KEY (`authority`) REFERENCES `char_edu_authorities` (`id`),
  ADD CONSTRAINT `fk_persons_education_degree_id` FOREIGN KEY (`degree`) REFERENCES `char_edu_degrees` (`id`),
  ADD CONSTRAINT `fk_persons_education_pId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_education_stage_id` FOREIGN KEY (`stage`) REFERENCES `char_edu_stages` (`id`);

--
-- Constraints for table `char_persons_health`
--
ALTER TABLE `char_persons_health`
  ADD CONSTRAINT `fk_persons_health_disease_id` FOREIGN KEY (`disease_id`) REFERENCES `char_diseases` (`id`),
  ADD CONSTRAINT `fk_persons_health_pId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_i18n`
--
ALTER TABLE `char_persons_i18n`
  ADD CONSTRAINT `fk_persons_i18n_language_id` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_i18n_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_islamic_commitment`
--
ALTER TABLE `char_persons_islamic_commitment`
  ADD CONSTRAINT `fk_persons_islamic_commitment_pid` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_kinship`
--
ALTER TABLE `char_persons_kinship`
  ADD CONSTRAINT `fk_persons_kinship_kinid` FOREIGN KEY (`kinship_id`) REFERENCES `char_kinship` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persons_kinship_lpid` FOREIGN KEY (`l_person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_kinship_rpid` FOREIGN KEY (`r_person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_persons_properties`
--
ALTER TABLE `char_persons_properties`
  ADD CONSTRAINT `fk_persons_properties_person_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_properties_property_id` FOREIGN KEY (`property_id`) REFERENCES `char_properties` (`id`);

--
-- Constraints for table `char_persons_work`
--
ALTER TABLE `char_persons_work`
  ADD CONSTRAINT `fk_persons_work_job_id` FOREIGN KEY (`work_job_id`) REFERENCES `char_work_jobs` (`id`),
  ADD CONSTRAINT `fk_persons_work_reason_id` FOREIGN KEY (`work_reason_id`) REFERENCES `char_work_reasons` (`id`),
  ADD CONSTRAINT `fk_persons_work_status_id` FOREIGN KEY (`work_status_id`) REFERENCES `char_work_status` (`id`),
  ADD CONSTRAINT `fk_persons_work_wage_id` FOREIGN KEY (`work_wage_id`) REFERENCES `char_work_wages` (`id`);

--
-- Constraints for table `char_projects`
--
ALTER TABLE `char_projects`
  ADD CONSTRAINT `fk_projects_orgId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_projects_persons`
--
ALTER TABLE `char_projects_persons`
  ADD CONSTRAINT `fk_projects_nominees_1` FOREIGN KEY (`project_id`) REFERENCES `char_projects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_projects_nominees_2` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_properties_i18n`
--
ALTER TABLE `char_properties_i18n`
  ADD CONSTRAINT `fk_properties_i18n_1` FOREIGN KEY (`property_id`) REFERENCES `char_properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_properties_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_properties_weights`
--
ALTER TABLE `char_properties_weights`
  ADD CONSTRAINT `fk_aid_properties_weights_1` FOREIGN KEY (`property_id`) REFERENCES `char_properties` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_property_types_i18n`
--
ALTER TABLE `char_property_types_i18n`
  ADD CONSTRAINT `fk_propertytypes_i18n_langid` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_propertytypes_i18n_propid` FOREIGN KEY (`property_type_id`) REFERENCES `char_property_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_reconstructions`
--
ALTER TABLE `char_reconstructions`
  ADD CONSTRAINT `fk_reconstructions_case_id` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_residence`
--
ALTER TABLE `char_residence`
  ADD CONSTRAINT `fk_fk_residence_roofId` FOREIGN KEY (`roof_material_id`) REFERENCES `char_roof_materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_residence_personId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_residence_propertyId` FOREIGN KEY (`property_type_id`) REFERENCES `char_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_roles`
--
ALTER TABLE `char_roles`
  ADD CONSTRAINT `fk_roles_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_roles_permissions`
--
ALTER TABLE `char_roles_permissions`
  ADD CONSTRAINT `fk_rolesPermissions_permissionId` FOREIGN KEY (`permission_id`) REFERENCES `char_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_rolesPermissions_roleId` FOREIGN KEY (`role_id`) REFERENCES `char_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_roof_materials_i18n`
--
ALTER TABLE `char_roof_materials_i18n`
  ADD CONSTRAINT `fk_roof_materials_roofid` FOREIGN KEY (`roof_material_id`) REFERENCES `char_roof_materials` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fkr_roof_materials_langid` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_sponsorships`
--
ALTER TABLE `char_sponsorships`
  ADD CONSTRAINT `fk_sponsorships_catId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_sponsorships_orgId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_sponsorships_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`);

--
-- Constraints for table `char_sponsorships_persons`
--
ALTER TABLE `char_sponsorships_persons`
  ADD CONSTRAINT `fk_sponsorships_persons_pid` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_sponsorships_persons_sid` FOREIGN KEY (`sponsorship_id`) REFERENCES `char_sponsorships` (`id`);

--
-- Constraints for table `char_sponsorship_cases`
--
ALTER TABLE `char_sponsorship_cases`
  ADD CONSTRAINT `fksponsorship_cases_person_id` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fksponsorship_cases_sponsor_id` FOREIGN KEY (`case_id`) REFERENCES `char_cases` (`id`),
  ADD CONSTRAINT `fksponsorship_cases_sponsorship_id` FOREIGN KEY (`sponsorship_id`) REFERENCES `char_sponsorships` (`id`);

--
-- Constraints for table `char_sponsor_persons`
--
ALTER TABLE `char_sponsor_persons`
  ADD CONSTRAINT `fksponsor_persons_person_id` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fksponsor_persons_sponsor_id` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_templates`
--
ALTER TABLE `char_templates`
  ADD CONSTRAINT `fk_templates_fk_templates_catId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_templates_orgId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_update_data_setting`
--
ALTER TABLE `char_update_data_setting`
  ADD CONSTRAINT `fk_cases_update_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_users`
--
ALTER TABLE `char_users`
  ADD CONSTRAINT `fk_users_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_users_parentId` FOREIGN KEY (`parent_id`) REFERENCES `char_users` (`id`);

--
-- Constraints for table `char_users_permissions`
--
ALTER TABLE `char_users_permissions`
  ADD CONSTRAINT `fk_users_permissions_permid` FOREIGN KEY (`permission_id`) REFERENCES `char_permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_users_permissions_userid` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_users_roles`
--
ALTER TABLE `char_users_roles`
  ADD CONSTRAINT `fk_usersRoles_roleId` FOREIGN KEY (`role_id`) REFERENCES `char_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_usersRoles_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_verification`
--
ALTER TABLE `char_verification`
  ADD CONSTRAINT `fk_verification_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `char_visitor_notes`
--
ALTER TABLE `char_visitor_notes`
  ADD CONSTRAINT `fk_vsitor_notes_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_vsitor_notes_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_vouchers`
--
ALTER TABLE `char_vouchers`
  ADD CONSTRAINT `fk_vouchers_category_cid` FOREIGN KEY (`category_id`) REFERENCES `char_vouchers_categories` (`id`),
  ADD CONSTRAINT `fk_vouchers_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_vouchers_sponsorId` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`);

--
-- Constraints for table `char_vouchers_categories`
--
ALTER TABLE `char_vouchers_categories`
  ADD CONSTRAINT `fk_parent_id` FOREIGN KEY (`parent`) REFERENCES `char_vouchers_categories` (`id`);

--
-- Constraints for table `char_vouchers_persons`
--
ALTER TABLE `char_vouchers_persons`
  ADD CONSTRAINT `fk_vouchersBeneficiaries_pId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_vouchersBeneficiaries_vId` FOREIGN KEY (`voucher_id`) REFERENCES `char_vouchers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `char_work_jobs_i18n`
--
ALTER TABLE `char_work_jobs_i18n`
  ADD CONSTRAINT `fk_work_jobs_i18n_1` FOREIGN KEY (`work_job_id`) REFERENCES `char_work_jobs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_work_jobs_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_work_reasons_i18n`
--
ALTER TABLE `char_work_reasons_i18n`
  ADD CONSTRAINT `fk_work_reasons_i18n_1` FOREIGN KEY (`work_reason_id`) REFERENCES `char_work_reasons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_work_reasons_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `char_work_status_i18n`
--
ALTER TABLE `char_work_status_i18n`
  ADD CONSTRAINT `fk_work_status_i18n_1` FOREIGN KEY (`work_status_id`) REFERENCES `char_work_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_work_status_i18n_2` FOREIGN KEY (`language_id`) REFERENCES `char_languages` (`id`);

--
-- Constraints for table `doc_attributes`
--
ALTER TABLE `doc_attributes`
  ADD CONSTRAINT `fk_attributes_category_id` FOREIGN KEY (`category_id`) REFERENCES `doc_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doc_attributes_values`
--
ALTER TABLE `doc_attributes_values`
  ADD CONSTRAINT `fk_attributes_values_1` FOREIGN KEY (`attribute_id`) REFERENCES `doc_attributes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doc_categories`
--
ALTER TABLE `doc_categories`
  ADD CONSTRAINT `fk_categories_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `doc_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doc_files`
--
ALTER TABLE `doc_files`
  ADD CONSTRAINT `fk_files_category_id` FOREIGN KEY (`category_id`) REFERENCES `doc_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_files_status_id` FOREIGN KEY (`file_status_id`) REFERENCES `doc_files_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `doc_files_attributes`
--
ALTER TABLE `doc_files_attributes`
  ADD CONSTRAINT `fk_files_attributes_1` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`),
  ADD CONSTRAINT `fk_files_attributes_2` FOREIGN KEY (`attribute_id`) REFERENCES `doc_attributes` (`id`),
  ADD CONSTRAINT `fk_files_attributes_3` FOREIGN KEY (`attribute_value_id`) REFERENCES `doc_attributes_values` (`id`);

--
-- Constraints for table `doc_files_revisions`
--
ALTER TABLE `doc_files_revisions`
  ADD CONSTRAINT `fk_files_revisions_file_id` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doc_files_tags`
--
ALTER TABLE `doc_files_tags`
  ADD CONSTRAINT `fk_files_tags_file_id` FOREIGN KEY (`file_id`) REFERENCES `doc_files` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_files_tags_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `doc_tags` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$
--
-- Events
--
CREATE  EVENT `char_check_expired_documents` ON SCHEDULE EVERY 1 DAY STARTS '2017-02-16 12:41:55' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
		INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `created_at`)
		SELECT DISTINCT UUID(), 'Nepras\Common\Notification\PersonDocument', `char_users`.`id`, 'Nepras\Auth\Model\User', '{"message": "يوجد مرفقات لعدد من الحالات انتهت فترة الصلاحية المحددة لها"}', NOW()
        FROM `char_persons_documents`
        INNER JOIN `char_document_types` ON `char_persons_documents`.`document_type_id` = `char_document_types`.`id`
        INNER JOIN `char_document_types_i18n` ON `char_document_types`.`id` = `char_document_types_i18n`.`document_type_id` AND  `char_document_types_i18n`.`language_id` = 1
        INNER JOIN `doc_files` ON `char_persons_documents`.`document_id` = `doc_files`.`id`
        INNER JOIN `char_cases` ON `char_persons_documents`.`person_id` = `char_cases`.`person_id`
        INNER JOIN `char_categories` ON `char_cases`.`category_id` = `char_categories`.`id`
        LEFT JOIN (`char_users`
        INNER JOIN `auth_users_permissions_view` ON `char_users`.`id` = `auth_users_permissions_view`.`user_id`
        AND `auth_users_permissions_view`.`code` IN ('sponsorship.case.manage', 'aid.case.manage'))  ON `char_cases`.`organization_id` = `char_users`.`organization_id`
        WHERE  `char_document_types`.`expiry` > 0
        AND NOW() > date_add(`doc_files`.`updated_at`, INTERVAL `char_document_types`.`expiry` MONTH);
	END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
