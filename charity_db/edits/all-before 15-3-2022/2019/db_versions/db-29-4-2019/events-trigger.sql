DELIMITER $$
--
-- Events
--
CREATE  EVENT `char_check_expired_documents` ON SCHEDULE EVERY 1 DAY STARTS '2017-02-16 12:41:55' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
		INSERT INTO `notifications` (`id`, `type`, `notifiable_id`, `notifiable_type`, `data`, `created_at`)
		SELECT DISTINCT UUID(), 'Nepras\Common\Notification\PersonDocument', `char_users`.`id`, 'Nepras\Auth\Model\User', '{"message": "يوجد مرفقات لعدد من الحالات انتهت فترة الصلاحية المحددة لها"}', NOW()
        FROM `char_persons_documents`
        INNER JOIN `char_document_types` ON `char_persons_documents`.`document_type_id` = `char_document_types`.`id`
        INNER JOIN `char_document_types_i18n` ON `char_document_types`.`id` = `char_document_types_i18n`.`document_type_id` AND  `char_document_types_i18n`.`language_id` = 1
        INNER JOIN `doc_files` ON `char_persons_documents`.`document_id` = `doc_files`.`id`
        INNER JOIN `char_cases` ON `char_persons_documents`.`person_id` = `char_cases`.`person_id`
        INNER JOIN `char_categories` ON `char_cases`.`category_id` = `char_categories`.`id`
        LEFT JOIN (`char_users`
        INNER JOIN `auth_users_permissions_view` ON `char_users`.`id` = `auth_users_permissions_view`.`user_id`
        AND `auth_users_permissions_view`.`code` IN ('sponsorship.case.manage', 'aid.case.manage'))  ON `char_cases`.`organization_id` = `char_users`.`organization_id`
        WHERE  `char_document_types`.`expiry` > 0
        AND NOW() > date_add(`doc_files`.`updated_at`, INTERVAL `char_document_types`.`expiry` MONTH);
	END$$

DELIMITER ;
