DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_banks_save`(INOUT `pId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_banks`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_banks` (`name`) VALUES (pName);
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_banks` SET `name` = pName WHERE `id` = pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `CHAR_GET_FORM_ELEMENT_PRIORITY`(`p_category_id` INT, `p_element_name` VARCHAR(255)) RETURNS int(11)
BEGIN

                DECLARE v_priority INT;

   

    SET v_priority = (SELECT `e`.`priority` 
                        FROM `char_categories_form_element_priority` `e`
                        JOIN `char_forms_elements` AS `el` on `el`.`id` =`e`.`element_id` and `el`.`name` = p_element_name 
						where`e`.`category_id` = p_category_id);

RETURN v_priority;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_count_voucher_persons`(`id` INT(1)) RETURNS int(11)
BEGIN
declare c INTEGER;  
		set c = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons` 
        from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));      
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_currencies_save`(INOUT `pId` INT, IN `pName` VARCHAR(255), IN `pCode` VARCHAR(255), IN `pSymbol` VARCHAR(255), IN `pDefault` INT, IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_currencies`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_currencies` (`name`, `code`, `symbole`, `default`) VALUES (pName, pCode, pSymbol, pDefault);
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_currencies` SET `name` = pName, `code` = pCode, `symbol` = pSymbol, `default` = pDefault WHERE `id` = pId;
    END IF;
    
    IF pDefault = 1 THEN
		UPDATE `char_currencies` SET `default` = 0 WHERE `default` = 1 AND `id` <> pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_death_causes_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `death_cause_id` INTO pId
		FROM `char_death_causes_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_death_causes` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_death_causes_i18n` (`death_cause_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
        
    ELSE
		UPDATE `char_death_causes` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_death_causes_i18n` (`death_cause_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_case_file`(`id` INT) RETURNS text CHARSET utf32
BEGIN
	declare c text;
    set c =
            (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_cases_files`
              join `doc_files` on `doc_files`.`id` = `char_cases_files`.`file_id` 
              where `char_cases_files`.`case_id`=id  
            );
      

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_building_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `building_status_id` INTO pId
		FROM `char_building_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_building_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_building_status_i18n` (`building_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_building_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_building_status_i18n` (`building_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_edu_authorities_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_authority_id` INTO pId
		FROM `char_edu_authorities_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_authorities` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_authorities_i18n` (`edu_authority_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_authorities` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_authorities_i18n` (`edu_authority_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_branches_save`(INOUT `pId` INT, IN `pBankId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_branches`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName)
        AND `bank_id` = pBankId;
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_branches` (`bank_id`, `name`) VALUES (pBankId, pName);
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_branches` SET `name` = pName, `bank_id` = pBankId WHERE `id` = pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_document_types_save`(INOUT `pId` INT, IN `pExpiry` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
 
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `document_type_id` INTO pId
		FROM `char_document_types_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_document_types` (`expiry`) VALUES (pExpiry);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_document_types_i18n` (`document_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_document_types` SET `expiry` = pExpiry WHERE `id` = pId;
        
		REPLACE INTO `char_document_types_i18n` (`document_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;

	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_furniture_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `furniture_status_id` INTO pId
		FROM `char_furniture_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_furniture_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_furniture_status_i18n` (`furniture_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_furniture_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_furniture_status_i18n` (`furniture_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_diseases_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `disease_id` INTO pId
		FROM `char_diseases_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_diseases` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_diseases_i18n` (`disease_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_diseases` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_diseases_i18n` (`disease_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_edu_degrees_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_degree_id` INTO pId
		FROM `char_edu_degrees_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_degrees` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_degrees_i18n` (`edu_degree_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_degrees` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_degrees_i18n` (`edu_degree_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_case_essentials_need`(`id` INT) RETURNS text CHARSET utf8
BEGIN
  declare c text CHARSET utf8; 
    set c = (select GROUP_CONCAT(DISTINCT `char_essentials`.`name` SEPARATOR ' - ' )
             FROM `char_cases_essentials`
             join `char_essentials` ON `char_essentials`.`id` = `char_cases_essentials`.`essential_id`
             where `char_cases_essentials`.`case_id`=id and
                  (`char_cases_essentials`.`needs` is not null ) and 
                  ( `char_cases_essentials`.`needs` != 0)
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_get_brother_detail`(IN `id` INT(10))
BEGIN
		select `a`.`id` , `a`.`id_card_number`, `a`.`birthday` , `st`.`name` as education_stage,
		CASE  WHEN `h`.`condition`=1 THEN 'جيدة'
		      WHEN `h`.`condition`=2 THEN 'يعاني من مرض مزمن'
		      WHEN `h`.`condition`=3 THEN 'ذوي احتياجات خاصة'
		END as the_health_status ,
		CASE WHEN `a`.`gender`=1 THEN 'ذكر'Else 'أنثى' END as gender ,
		CONCAT(`a`.`first_name` , ' ' , `a`.`second_name` , ' ' , `a`.`third_name` , ' ' , `a`.`last_name`) AS name,
		CASE
		WHEN `e`.`level` = 1 THEN 'ضعيف'
		WHEN `e`.`level` = 2 THEN 'مقبول'
		wHEN `e`.`level`= 3 THEN 'جيد'
		WHEN `e`.`level` = 4 THEN 'جيد جدا'
		WHEN `e`.`level` = 5 THEN 'ممتاز'

		END as education_level

		FROM `char_persons` AS `a`
		LEFT JOIN  `char_persons_health` AS `h`
		on `h`.`person_id`  = `a`.`id`
		LEFT JOIN  `char_persons_education` AS `e`
		on `e`.`person_id`  = `a`.`id`
		LEFT JOIN  `char_edu_stages_i18n` AS `st`
		on `st`.`edu_stage_id`  = `e`.`stage` and `st`.`language_id`  = 1
		WHERE (`a`.`father_id` = (
						select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = id
					) OR `a`.`mother_id` =(
						select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = id
					) 	) and `a`.`id` <> id   ;

   END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_family_count`(`p_via` VARCHAR(15), `master_id` INT(10), `gender` INT(4), `edge_id` INT(10)) RETURNS int(11)
BEGIN
declare c INTEGER;
    
      if p_via = 'left' then
	if gender is null then
				set c = (
						  SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
					      WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
		                );
	       
	       else
				set c = (
						 SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
			  			 JOIN  `char_persons` ON ((`char_persons`.`id`=`k`.`r_person_id`) and (`char_persons`.`gender`= gender))
						 WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id 
	                    );
	   
	       end if;
    elseif p_via = 'right' then
		if gender is null then
				set c = (
  						   SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			      		   WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id 
			                 );
	       
	       else
				set c = (
						  SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			    		 JOIN  `char_persons` ON ( (`char_persons`.`id`=`k`.`l_person_id`) and  (`char_persons`.`gender`= gender ))
						 WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id 
	                    );
	        end if;
   
    end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_case_rank`(`c_id` INT) RETURNS float
BEGIN
	DECLARE v_rank FLOAT;
    
	SET v_rank := (select (
      ifnull(`char_death_causes`.`weight`, 0) 
    + ifnull(`char_marital_status`.`weight`, 0) 
    + ifnull(`char_diseases`.`weight`, 0) 
    + ifnull(`char_roof_materials`.`weight`, 0) 
    + ifnull(`char_property_types`.`weight`, 0)
    + ifnull(`char_work_status`.`weight`, 0) 
    + ifnull(`char_work_reasons`.`weight`, 0) 
    + ifnull(`char_work_jobs`.`weight`, 0) 
    + ifnull(`char_work_wages`.`weight`, 0)
    + sum(ifnull(`char_aid_sources_weights`.`weight`, 0))
    + sum(ifnull(`char_essentials_weights`.`weight`, 0))
    + sum(ifnull(`char_properties_weights`.`weight`, 0))) AS rank
	from `char_cases`
	inner join  `char_persons` on `char_cases`.`person_id` = `char_persons`.`id`

	left join `char_death_causes` on `char_persons`.`death_cause_id` = `char_death_causes`.`id`
	left join `char_marital_status` on `char_persons`.`marital_status_id` = `char_marital_status`.`id`

	left join `char_persons_health` on `char_cases`.`person_id` = `char_persons_health`.`person_id`
	left join `char_diseases` on `char_persons_health`.`disease_id` = `char_diseases`.`weight`
    
    left join `char_residence` on `char_cases`.`person_id` = `char_residence`.`person_id`
    left join `char_property_types` on `char_residence`.`property_type_id` = `char_property_types`.`id`
    left join `char_roof_materials` on `char_residence`.`roof_material_id` = `char_roof_materials`.`id`
    
    left join `char_persons_properties` on `char_cases`.`person_id` = `char_persons_properties`.`person_id`
    left join `char_properties_weights` on `char_persons_properties`.`property_id` = `char_properties_weights`.`property_id`
    AND `char_persons_properties`.`quantity` BETWEEN `char_properties_weights`.`value_min` AND `char_properties_weights`.`value_max`

	left join `char_persons_work` on `char_cases`.`person_id` = `char_persons_work`.`person_id`
	left join `char_work_status` on `char_persons_work`.`work_status_id` = `char_work_status`.`id`
	left join `char_work_reasons` on `char_persons_work`.`work_reason_id` = `char_work_reasons`.`id`
	left join `char_work_jobs` on `char_persons_work`.`work_job_id` = `char_work_jobs`.`id`
	left join `char_work_wages` on `char_persons_work`.`work_wage_id` = `char_work_wages`.`id`
    
    left join `char_persons_aids` on `char_cases`.`person_id` = `char_persons_aids`.`person_id`
    left join `char_aid_sources_weights` on `char_persons_aids`.`aid_source_id` = `char_aid_sources_weights`.`aid_source_id`
    AND `char_persons_aids`.`aid_value` BETWEEN `char_aid_sources_weights`.`value_min` AND `char_aid_sources_weights`.`value_max`
    
    left join `char_cases_essentials` on `char_cases`.`id` = `char_cases_essentials`.`case_id`
    left join `char_essentials_weights` on `char_cases_essentials`.`essential_id` = `char_essentials_weights`.`essential_id`
    AND `char_cases_essentials`.`condition` = `char_essentials_weights`.`value`
    WHERE `char_cases`.`id` = c_id);
RETURN v_rank;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_case_aid_sources`(`id` INT) RETURNS text CHARSET latin1
BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_aid_sources`.`name` SEPARATOR ' - ' )
FROM `char_persons_aids`
join `char_aid_sources` ON `char_aid_sources`.`id` = `char_persons_aids`.`aid_source_id`
where `char_persons_aids`.`person_id`=id  and `char_persons_aids`.`aid_take` =1 
    );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_get_family_detail`(IN `id` INT(10))
BEGIN
		select `a`.`id` , `a`.`id_card_number`, `a`.`birthday` , `st`.`name` as education_stage,
		CASE  WHEN `h`.`condition`=1 THEN 'جيدة' 
		      WHEN `h`.`condition`=2 THEN 'يعاني من مرض مزمن'
		      WHEN `h`.`condition`=3 THEN 'ذوي احتياجات خاصة'
		END as the_health_status ,
		CASE WHEN `a`.`gender`=1 THEN 'ذكر'Else 'أنثى' END as gender ,
		CONCAT(`a`.`first_name` , ' ' , `a`.`second_name` , ' ' ,   			   `a`.`third_name` , ' ' , `a`.`last_name`) AS name,
		CASE 
		WHEN `e`.`level` = 1 THEN 'ضعيف'
		WHEN `e`.`level` = 2 THEN 'مقبول'
		wHEN `e`.`level`= 3 THEN 'جيد'
		WHEN `e`.`level` = 4 THEN 'جيد جدا'
		WHEN `e`.`level` = 5 THEN 'ممتاز'
		                                   
		END as education_level 
		                                  
		FROM `char_persons` AS `a` 
		LEFT JOIN  `char_persons_health` AS `h`
		on `h`.`person_id`  = `a`.`id` 
		LEFT JOIN  `char_persons_education` AS `e`
		on `e`.`person_id`  = `a`.`id` 
		LEFT JOIN  `char_edu_stages_i18n` AS `st`
		on `st`.`edu_stage_id`  = `e`.`stage` and `st`.`language_id`  = 1
		WHERE `a`.`father_id` =  id   ;

   END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_children_count`(`person_id` INT(10), `gender` TINYINT(4)) RETURNS int(11)
BEGIN
declare c INTEGER;
    
   	if gender is null then
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE `a`.`father_id` = person_id  or `a`.`mother_id` = person_id
					);
	       
	       else
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE (`a`.`father_id` = person_id  or `a`.`mother_id` = person_id) and `a`.`gender`= gender
					);
	       				
	       end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_family_total_category_payments`(`father` INT(10), `p_date_from` DATE, `p_date_to` DATE, `category` INT(10)) RETURNS double
BEGIN
  declare sum double;
  IF category is null THEN
 
   if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))  
                               AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     end if;
 
     ELSE

     if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))  
                                AND   ((`char_cases`.`category_id` = category ))
                                AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
 


        end if;

     END IF;
RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_guardian_persons_count`(`p_guardian_id` INTEGER) RETURNS int(11)
BEGIN
	DECLARE c INTEGER;
    
    set c = (SELECT COUNT(`individual_id`) FROM `char_guardians` WHERE `guardian_id` = p_guardian_id);

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_family_total_payments`(`father` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))  
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                       )
              );

  elseif p_date_from is not null and p_date_to is null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       )
               );

    elseif p_date_from is null and p_date_to is not null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       )
              );

    elseif p_date_from is not null and p_date_to is not null then
    set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  )) 
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       )
              );
    end if;

RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_organizations_child_nominated_count`(`o_id` INT ,`p_id` INT) RETURNS int(11)
BEGIN

declare c INTEGER;

      set c = ( select COUNT(*) FROM `aid_projects_persons` 
                WHERE `project_id` = p_id and 
                organization_id in (SELECT char_organizations.id FROM char_organizations where parent_id = o_id));

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_organization_nominated_count`(`o_id` INT ,`p_id` INT) RETURNS int(11)
BEGIN
declare c INTEGER;
      set c = ( select COUNT(*) FROM `aid_projects_persons`   WHERE `project_id` = p_id and  organization_id  = o_id);
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_get_person_detail`(IN `id` INT(10))
BEGIN

select `p`.`id` as person_id,`p`.`id_card_number`,`p`.`birthday`,`p`.`nationality`,`p`.`monthly_income`,`p`.`location_id`,
CASE WHEN `p`.`refugee` =1 THEN 'مواطن' Else 'لاجيء' END AS refugee,
CASE WHEN `p`.`refugee` =2 THEN  `p`.`unrwa_card_number` Else '-' END AS unrwa_card_number,
CASE WHEN `p`.`marital_status_id` is null THEN '-' Else `m`.`name` END AS MaritalStatus,
CASE WHEN `p`.`nationality` is null THEN '-' Else `L`.`name` END AS nationality,
CASE WHEN `p`.`death_date` is null THEN 'نعم' Else 'لا' END AS is_alive,
CASE WHEN `p`.`location_id` is null THEN '-' Else L8.name END AS country,
CASE WHEN `p`.`location_id` is null THEN '-' Else L6.name END AS governarate,
CASE WHEN `p`.`location_id` is null THEN '-' Else L4.name END AS city,
CASE WHEN `p`.`location_id` is null THEN '-' Else L1.name END AS nearLocation,
CASE WHEN `p`.`monthly_income` is null THEN '-' Else `p`.`monthly_income` END AS monthly_income,
CASE WHEN `p`.`street_address` is null THEN '-' Else `p`.`street_address` END AS street_address,
CASE WHEN `p`.`birth_place` is null THEN '-' Else `L`.`name` END AS birth_place,
CASE WHEN `p`.`gender` =1 THEN 'ذكر' Else 'أنثى' END AS gender,
CONCAT(`p`.`first_name`, ' ' , `p`.`second_name`, ' ', `p`.`third_name`, ' ' , `p`.`last_name`) AS name,
char_get_family_count ( `p`.`id` ) AS family,
char_get_siblings_count ( `p`.`id` ,Null) AS brothers
FROM `char_persons` AS `p`
LEFT JOIN  `char_marital_status_i18n` AS `m` on `m`.`marital_status_id`  = `p`.`marital_status_id` and `m`.`language_id`  = 1
LEFT JOIN  `char_locations_i18n` AS `L` on `L`.`location_id`  = `p`.`birth_place` and `L`.`language_id`  = 1
LEFT JOIN  `char_locations_i18n` AS `L1` on `L1`.`location_id`  = `p`.`location_id` and `L1`.`language_id`  = 1
LEFT JOIN  `char_locations`      AS `L2` on `L2`.`id`  = `p`.`location_id`
LEFT JOIN  `char_locations`      AS `L3` on `L3`.`id`  = `L2`.`parent_id`
LEFT JOIN  `char_locations_i18n` AS `L4` on `L4`.`location_id`  = `L3`.`id` and `L4`.`language_id`  = 1
LEFT JOIN  `char_locations`      AS `L5` on `L5`.`id`  = `L3`.`parent_id`
LEFT JOIN  `char_locations_i18n` AS `L6` on `L6`.`location_id`  = `L5`.`id` and `L6`.`language_id`  = 1
LEFT JOIN  `char_locations`      AS `L7` on `L7`.`id`  = `L5`.`parent_id`
LEFT JOIN  `char_locations_i18n` AS `L8` on `L8`.`location_id`  = `L7`.`id` and `L8`.`language_id`  = 1
WHERE `p`.`id` =  id
LIMIT 1;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_person_total_payments`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
    set sum = (SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or
                                 (`p`.`status` = 2)
                              ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE ( 
                              (`char_cases`.`person_id` = p_person_id) and
                              ((`p`.`status` = 1) or (`p`.`status` = 2)) and
                              ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                              ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    end if;

RETURN sum;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_person_average_payments`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS double
BEGIN
  declare average double;

    if p_date_from is null and p_date_to is null then
    set average = (SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                       (`char_cases`.`person_id` = p_person_id) and
                       ( (`p`.`status` = 1) or
                           (`p`.`status` = 2)
                       ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and  ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                    )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and
                        ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and 
                        ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    end if;

RETURN average;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_siblings_counts`(`p_person_id` INT(10), `l_person_id` INT(10), `e_person_id` INT(10)) RETURNS int(11)
BEGIN
	declare c INTEGER;

       if l_person_id is null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` =  p_person_id)
                  );
       elseif l_person_id is not null and e_person_id is not null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` = l_person_id) and (`k`.`r_person_id` != p_person_id) and (`k`.`r_person_id` != e_person_id)
                  );
       elseif l_person_id is not null and e_person_id is null then
          set c = (
                    select COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
                    WHERE (`k`.`l_person_id` = l_person_id) and (`k`.`r_person_id` != p_person_id)
                  );
  
       end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_get_person_payments`(IN `id` INT(10), IN `org` INT(10))
BEGIN
    IF org = 1  then
        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id ;
      end if;

    if org <> 1  then

        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id  and L4.organization_id = org;
              end if;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_sponsorship_cases_documents_count`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS int(11)
BEGIN
	declare c int;
    set c =  (select count(`char_sponsorship_cases_documents`.`id`)
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_siblings_count`(`p_person_id` INT, `p_via` VARCHAR(15), `gender` INT(4)) RETURNS int(11)
BEGIN
	declare c INTEGER;
    
      if p_via = 'mother' then

      	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`mother_id` = (
								select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	       
	       else
	       	set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`mother_id` = (
							select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id	
					);

	       				
	       end if;

	
    elseif p_via = 'father' then

     	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`father_id` = (
								select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	    else
			set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`father_id` = (
							select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);
        				
	    end if;


    else

     	if gender is null then

		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id
			)) and `a`.`id` !=  p_person_id
		);
	    else


		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id and `c`.`gender`= gender
			)) and `a`.`id` !=  p_person_id
		);
        				
	    end if;
    end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_voucher_persons`(`id` INT, `p_type` TINYINT(3), `status` TINYINT(3)) RETURNS int(11)
BEGIN
	DECLARE c INTEGER;

 if p_type is null and status is null then
			set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id
            );

 elseif p_type is null and status is NOT null then
				set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`status` =  status
            );

 elseif ( p_type = 1  OR p_type = 2 )and status is null then
			set c = (
               SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
               WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`type` =  p_type
            );


 elseif ( p_type = 1  OR p_type = 2 )and status is NOT null then
				set c = (
              SELECT count(`char_vouchers_persons`.`person_id`) FROM `char_vouchers_persons`
              WHERE `char_vouchers_persons`.`voucher_id` = id   AND  `char_vouchers_persons`.`type` =  p_type AND  `char_vouchers_persons`.`status` =  status
            );
    end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_persons_properties`(`id` INT) RETURNS text CHARSET utf8
BEGIN
  declare c text CHARSET utf8; 
    set c = (select GROUP_CONCAT(DISTINCT `char_properties_i18n`.`name` SEPARATOR ' - ' )
             FROM `char_persons_properties`
             join `char_properties_i18n` ON `char_properties_i18n`.`property_id` = `char_persons_properties`.`property_id` and    `char_properties_i18n`.`language_id` =1
             where `char_persons_properties`.`person_id`=id 
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_voucher_persons_count`(`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS int(11)
BEGIN
	DECLARE c INTEGER;

 if p_date_from is null and p_date_to is null then
		set c = (
              SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );

    elseif p_date_from is not null and p_date_to is null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );


    elseif p_date_from is null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );


    elseif p_date_from is not null and p_date_to is not null then
		set c = (
		          SELECT count(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_habitable_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `habitable_status_id` INTO pId
		FROM `char_habitable_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_habitable_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_habitable_status_i18n` (`habitable_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_habitable_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_habitable_status_i18n` (`habitable_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_kinship_save`(INOUT `pId` INT, IN `pLevel` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `death_cause_id` INTO pId
		FROM `char_death_causes_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_kinship` (`level`) VALUES (pLevel);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_kinship_i18n` (`kinship_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_kinship` SET `level` = pLevel WHERE `id` = pId;
        
		REPLACE INTO `char_kinship_i18n` (`kinship_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_house_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `house_status_id` INTO pId
		FROM `char_house_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_house_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_house_status_i18n` (`house_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_house_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_house_status_i18n` (`house_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_edu_stages_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_stage_id` INTO pId
		FROM `char_edu_stages_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_stages` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_stages_i18n` (`edu_stage_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_stages` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_stages_i18n` (`edu_stage_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_organizations_closure_init`()
BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE v_id, v_parent_id INT;
  DECLARE cur1 CURSOR FOR SELECT `id`, `parent_id` FROM `char_organizations` ORDER BY `id`;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO v_id, v_parent_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    
    INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
		SELECT `c`.`ancestor_id`, v_id, `c`.`depth` + 1
		FROM `char_organizations_closure` AS `c`
		WHERE `c`.`descendant_id` = v_parent_id
		UNION ALL
		SELECT v_id, v_id, 0;
    
  END LOOP;

  CLOSE cur1;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_persons_save`(INOUT `person_id` INT, `p_first_name` VARCHAR(255), `p_second_name` VARCHAR(255), `p_third_name` VARCHAR(255), `p_last_name` VARCHAR(255), `prev_family_name` VARCHAR(255), `p_id_card_number` VARCHAR(255), `gender` INT, `marital_status_id` INT, `birthday` DATE, `birth_place` INT, `nationality` INT, `death_date` DATE, `death_cause_id` INT, `father_id` INT, `mother_id` INT, `spouses` INT, `refugee` INT, `unrwa_card_number` VARCHAR(255), `country` INT, `governarate` INT, `city` INT, `location_id` INT, `street_address` VARCHAR(255), `monthly_income` FLOAT, `p_fullname` VARCHAR(255), `en_first_name` VARCHAR(255), `en_second_name` VARCHAR(255), `en_third_name` VARCHAR(255), `en_last_name` VARCHAR(255), `bank_id` INT, `account_number` VARCHAR(255), `account_owner` VARCHAR(255), `branch_id` INT, `study` INT, `p_type` INT, `authority` INT, `stage` INT, `specialization` VARCHAR(255), `degree` INT, `grade` VARCHAR(255), `p_year` VARCHAR(255), `school` VARCHAR(255), `p_level` VARCHAR(255), `points` FLOAT, `p_condition` INT, `details` VARCHAR(255), `disease_id` INT, `prayer` INT, `quran_center` INT, `quran_parts` VARCHAR(255), `quran_chapters` VARCHAR(255), `prayer_reason` VARCHAR(255), `quran_reason` VARCHAR(255), `working` INT, `can_work` INT, `work_status_id` INT, `work_reason_id` INT, `work_job_id` INT, `work_wage_id` INT, `work_location` VARCHAR(255), `property_type_id` INT, `rent_value` FLOAT, `roof_material_id` INT, `residence_condition` INT, `indoor_condition` INT, `habitable` INT, `house_condition` INT, `rooms` INT, `area` FLOAT, `mobile1` VARCHAR(255), `mobile2` VARCHAR(255), `phone` VARCHAR(255))
BEGIN
    START TRANSACTION;
    
    SET p_id_card_number = SUBSTRING(p_id_card_number, 1, 9);
    
    IF person_id IS NULL THEN
		
        IF p_id_card_number IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons` WHERE `id_card_number` = p_id_card_number
            LIMIT 1;
		
        ELSEIF p_fullname IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons`
			WHERE `fullname` = p_fullname
            LIMIT 1;
		END IF;
        
	END IF;
    
    IF person_id IS NULL THEN
		INSERT INTO `char_persons`
		(`first_name`, `second_name`, `third_name`, `last_name`, `prev_family_name`, `id_card_number`, `gender`,
		`marital_status_id`, `birthday`, `birth_place`, `nationality`, `death_date`, `death_cause_id`, `father_id`,
		`mother_id`, `spouses`, `refugee`, `unrwa_card_number`, `country`, `governarate`, `city`, `location_id`,
		`street_address`, `monthly_income`, `created_at`, `fullname`)
		VALUES
		(p_first_name, p_second_name, p_third_name, p_last_name, prev_family_name, p_id_card_number, gender, 
		marital_status_id, birthday, birth_place, nationality, death_date, death_cause_id, father_id, 
		mother_id, spouses, refugee, unrwa_card_number, country, governarate, city, location_id, 
		street_address, monthly_income, NOW(), p_fullname);
        
        SET person_id = LAST_INSERT_ID();
	END IF;
    
    IF en_first_name IS NOT NULL THEN
		REPLACE  INTO `char_persons_i18n`
		(`person_id`, `language_id`, `first_name`, `second_name`, `third_name`, `last_name`)
		VALUES
		(person_id, 2, en_first_name, en_second_name, en_third_name, en_last_name);
	END IF;

	IF bank_id IS NOT NULL AND branch_id IS NOT NULL THEN
		REPLACE  INTO `char_persons_banks`
		(`person_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`)
		VALUES
		(person_id, bank_id, account_number, account_owner, branch_id);
    END IF;

	IF mobile1 IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'primary_mobile', mobile1);
	END IF;
    IF mobile2 IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'secondery_mobile', mobile2);
	END IF;
    IF phone IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'phone', phone);
	END IF;

	IF authority IS NOT NULL OR stage IS NOT NULL OR degree IS NOT NULL OR grade IS NOT NULL
    OR specialization IS NOT NULL OR p_type IS NOT NULL THEN
		REPLACE  INTO `char_persons_education`
		(`person_id`, `study`, `type`, `authority`, `stage`, `specialization`, `degree`, `grade`,
		`year`, `school`, `level`, `points`)
		VALUES
		(person_id, study, p_type, authority, stage, specialization,
		degree, grade, p_year, school, p_level, points);
    END IF;

	IF p_condition IS NOT NULL OR disease_id IS NOT NULL OR details IS NOT NULL THEN
		REPLACE  INTO `char_persons_health`
		(`person_id`, `condition`, `details`, `disease_id`)
		VALUES
		(person_id, p_condition, details, disease_id);
    END IF;

	IF prayer IS NOT NULL OR quran_parts IS NOT NULL OR quran_chapters IS NOT NULL OR prayer_reason IS NOT NULL THEN
		REPLACE  INTO `char_persons_islamic_commitment`
		(`person_id`, `prayer`, `quran_center`, `quran_parts`, `quran_chapters`, `prayer_reason`, `quran_reason`)
		VALUES
		(person_id, prayer, quran_center, quran_parts, quran_chapters, prayer_reason, quran_reason);
	END IF;
    
    IF working IS NOT NULL OR work_status_id IS NOT NULL OR work_job_id IS NOT NULL OR work_location IS NOT NULL THEN
		REPLACE  INTO `char_persons_work`
		(`person_id`, `working`, `can_work`, `work_status_id`, `work_reason_id`, `work_job_id`, `work_wage_id`, `work_location`)
		VALUES
		(person_id, working, can_work, work_status_id, work_reason_id, work_job_id, work_wage_id, work_location);
	END IF;
    
    IF property_type_id IS NOT NULL OR roof_material_id IS NOT NULL OR indoor_condition IS NOT NULL OR habitable IS NOT NULL
    OR house_condition IS NOT NULL THEN
		REPLACE  INTO `char_residence`
		(`person_id`, `property_type_id`, `rent_value`, `roof_material_id`, `residence_condition`, `indoor_condition`,
		`habitable`, `house_condition`, `rooms`, `area`)
		VALUES
		(person_id, property_type_id, rent_value, roof_material_id, residence_condition,
		indoor_condition, habitable, house_condition, rooms, area);
    END IF;
    
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_marital_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `marital_status_id` INTO pId
		FROM `char_marital_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_marital_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();

        INSERT INTO `char_marital_status_i18n` (`marital_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_marital_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_marital_status_i18n` (`marital_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_organizations_save`(INOUT `pId` INT, IN `pName` VARCHAR(255), IN `pParentId` INT, IN `pType` INT, IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_organizations`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_organizations` (`name`, `parent_id`, `type`, `created_at`) VALUES (pName, pParentId, pType, now());
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_organizations` SET `name` = pName, `type` = pType, `parent_id` = pParentId, updated_at = now() WHERE `id` = pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_property_types_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN

    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `property_type_id` INTO pId
		FROM `char_property_types_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_property_types` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_property_types_i18n` (`property_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_property_types` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_property_types_i18n` (`property_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_locations_save`(INOUT `pId` INT, IN `pType` INT, IN `pParentId` INT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `char_locations_i18n`.`location_id` INTO pId
		FROM `char_locations_i18n`
        INNER JOIN `char_locations` ON `char_locations_i18n`.`location_id` = `char_locations`.`id`
		WHERE `char_locations_i18n`.`language_id` = pLanguageId
        AND `char_locations`.`location_type` = pType
        AND normalize_text_ar(`char_locations_i18n`.`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_locations` (`location_type`, `parent_id`, `created_at`) VALUES (pType, pParentId, NOW());
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_locations_i18n` (`location_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		-- UPDATE `char_locations` SET `location_type` = pType, `parent_id` = pParentId, `updated_at` = NOW() WHERE `id` = pId;
        
		REPLACE INTO `char_locations_i18n` (`location_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_properties_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	DECLARE vNew BOOLEAN DEFAULT false;
    DECLARE vName VARCHAR(255);
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `property_id` INTO pId
		FROM `char_properties_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_properties` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_properties_i18n` (`property_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_properties` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_properties_i18n` (`property_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_total_voucher_amount`(`id` INT(1)) RETURNS int(11)
BEGIN
declare c INTEGER;

		set c = (select (`char_vouchers`.`exchange_rate` * `char_vouchers`.`value` * char_count_voucher_persons(`char_vouchers`.`id`)) AS `amount`
                 from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_get_sponsorship_cases_documents_file_path`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS text CHARSET utf8
BEGIN
	declare c text;
    set c =  (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_roof_materials_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `roof_material_id` INTO pId
		FROM `char_roof_materials_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_roof_materials` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_roof_materials_i18n` (`roof_material_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_roof_materials` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_roof_materials_i18n` (`roof_material_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_work_jobs_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_job_id` INTO pId
		FROM `char_work_jobs_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_jobs` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_jobs_i18n` (`work_job_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_jobs` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_work_jobs_i18n` (`work_job_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_voucher_amount`(`id` INT(1)) RETURNS int(11)
BEGIN
declare c INTEGER;  
declare d INTEGER;  

		set d = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons` from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));      
                 
		set c = (select (`char_vouchers`.`value` * d) AS `amount` from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_work_status_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
	IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_status_id` INTO pId
		FROM `char_work_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_status_i18n` (`work_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_status` SET `weight` = pWeight WHERE `id` = pId;
        
		INSERT INTO `char_work_status_i18n` (`work_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `clear_text`(`p_text` TEXT) RETURNS text CHARSET utf8
BEGIN
	-- Normalize
	SET p_text = normalize_text_ar(p_text);
    
    -- Strip Numbers
    SET p_text = REPLACE(p_text, '1', '');
    SET p_text = REPLACE(p_text, '2', '');
    SET p_text = REPLACE(p_text, '3', '');
    SET p_text = REPLACE(p_text, '4', '');
    SET p_text = REPLACE(p_text, '5', '');
    SET p_text = REPLACE(p_text, '6', '');
    SET p_text = REPLACE(p_text, '7', '');
    SET p_text = REPLACE(p_text, '8', '');
    SET p_text = REPLACE(p_text, '9', '');
    SET p_text = REPLACE(p_text, '0', '');
    
    -- Strip Spaces
    SET p_text = REPLACE(p_text, ' ', '');
    
RETURN p_text;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `fix_duplicated_id`(IN `p_postfix` VARCHAR(1))
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT f2.f_id from test.data_father f1 inner join test.data_father f2
	ON f1.f_sin = f2.f_sin AND f1.f_id <> f2.f_id AND f1.fullname <> f2.fullname
	WHERE f1.f_id > f2.f_id
    AND f1.f_sin <> 0 AND f1.f_sin is not Null;
    
	DECLARE cur2 CURSOR FOR SELECT m2.m_id from test.data_mother m1 inner join test.data_mother m2
	ON m1.m_sin = m2.m_sin AND m1.m_id <> m2.m_id AND m1.fullname <> m2.fullname
	WHERE m1.m_id > m2.m_id
    AND m1.m_sin <> 0 AND m1.m_sin is not Null;
    
    DECLARE cur3 CURSOR FOR SELECT c2.id from test.data_case c1 inner join test.data_case c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname <> c2.fullname
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur4 CURSOR FOR SELECT g2.g_id from test.data_guardian g1 inner join test.data_guardian g2
	ON g1.g_sin = g2.g_sin AND g1.g_id <> g2.g_id AND g1.fullname <> g2.fullname
	WHERE g1.g_id > g2.g_id
    AND g1.g_sin <> 0 AND g1.g_sin is not Null;
    
    DECLARE cur5 CURSOR FOR SELECT c2.id, c2.fullname from test.data_case c1 inner join test.data_case c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname
	WHERE c1.fullname > c2.fullname
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		SELECT v_id;
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    /*OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        UPDATE test.data_father SET f_sin = concat(substr(f_sin, 1, length(f_sin) - 1), p_postfix) WHERE f_id = v_id;
        
	END LOOP;
    CLOSE cur;
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        UPDATE test.data_mother SET m_sin = concat(substr(m_sin, 1, length(m_sin) - 1), p_postfix) WHERE m_id = v_id;
        
	END LOOP;
    CLOSE cur2;
    
    SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id;
        IF done THEN
			LEAVE read_loop3;
		END IF;
        
        UPDATE test.data_case SET sin = concat(substr(sin, 1, length(sin) - 1), p_postfix) WHERE id = v_id;
        
	END LOOP;
    CLOSE cur3;
    
    SET done = false;
    OPEN cur4;
    read_loop4: LOOP
		FETCH cur4 INTO v_id;
        IF done THEN
			LEAVE read_loop4;
		END IF;
        
        UPDATE test.data_guardian SET g_sin = concat(substr(g_sin, 1, length(g_sin) - 1), p_postfix) WHERE id = v_id;
        
	END LOOP;
    CLOSE cur4;*/
    
    SET done = false;
    OPEN cur5;
    read_loop5: LOOP
		FETCH cur5 INTO v_id, v_name;
        IF done THEN
			LEAVE read_loop5;
		END IF;
        
        UPDATE test.data_case SET sin = concat(substr(sin, 1, length(sin) - 1), p_postfix)
        WHERE id = v_id AND fullname = v_name;
        
	END LOOP;
    CLOSE cur5;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_case_files`(IN `id` INT(10))
BEGIN

select 
           `char_cases`.`id` , concat(`c`.`first_name` ,' ' , `c`.`second_name` , `c`.`third_name` ,' ' , `c`.`last_name`) as name,
    ( select GROUP_CONCAT(DISTINCT document_type_id ORDER BY person_id ASC SEPARATOR ',' )
          FROM char_persons_documents 
          where `char_persons_documents`.`person_id`=`char_cases`.`person_id` GROUP BY person_id) as person_file ,
          ( select GROUP_CONCAT(DISTINCT document_type_id ORDER BY person_id ASC SEPARATOR ',' )
          FROM char_persons_documents 
          where `char_persons_documents`.`person_id`=`f`.`id` GROUP BY person_id) as father_file ,
            ( select GROUP_CONCAT(DISTINCT document_type_id ORDER BY person_id ASC SEPARATOR ',' )
          FROM char_persons_documents 
          where `char_persons_documents`.`person_id`=`m`.`id` GROUP BY person_id) as mother_file ,
            ( select GROUP_CONCAT(DISTINCT document_type_id ORDER BY person_id ASC SEPARATOR ',' )
          FROM char_persons_documents 
          where `char_persons_documents`.`person_id`=`g`.`id` GROUP BY person_id) as guardian_file      
        
          from `char_cases` 
          inner join `char_categories` as `ca` on `ca`.`id` = `char_cases`.`category_id` and `ca`.`type` = 1 
          inner join `char_persons` as `c` on `c`.`id` = `char_cases`.`person_id` 
          left join `char_persons` as `f` on `f`.`id` = `c`.`father_id` 
          left join `char_persons` as `m` on `m`.`id` = `c`.`mother_id` 
          left join `char_guardians` as `guar` on `guar`.`individual_id` = `c`.`id` 
                and  `guar`.`organization_id` = `char_cases`.`organization_id` 
          left join `char_persons` as `g` on `g`.`id` = `guar`.`guardian_id` 
          where `char_cases`.`organization_id` = id;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_work_reasons_save`(INOUT `pId` INT, IN `pWeight` FLOAT, IN `pLanguageId` INT, IN `pName` VARCHAR(255), IN `in_transaction` BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_reason_id` INTO pId
		FROM `char_work_reasons_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_reasons` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_reasons_i18n` (`work_reason_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_reasons` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_work_reasons_i18n` (`work_reason_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_cases_essentials_count`(`id` INT(10)) RETURNS int(11)
BEGIN
	declare c INTEGER;

    set c = (
			select COUNT(`a`.`essential_id`)
			FROM `char_cases_essentials` AS `a`
			WHERE `a`.`case_id` = id
		);

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_case_documents`(`id` INT) RETURNS text CHARSET utf8
BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_cases_files`
          where `case_id`=id  
		);

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_cases_count`(`organization_id` INT, `location_id` INT) RETURNS int(11)
BEGIN
declare c INTEGER;

 set c = (
  				 SELECT COUNT(`char_cases`.`person_id`)
  				 FROM `char_cases`
  				 join `char_persons` ON (`char_cases`.`person_id` = `char_persons`.`id` and `char_persons`.`location_id` = location_id )
			     WHERE `char_cases`.`status` = 0 and `char_cases`.`organization_id` = organization_id
			   );


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_document`(`id` INT) RETURNS text CHARSET latin1
BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_persons_documents`
          where `person_id`=id  
		);

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `char_update_case_rank`(IN `p_id` INT, IN `p_context` VARCHAR(50))
BEGIN
	case p_context
		when 'death_cause' then

			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `death_cause_id` = p_id
            );
        when 'marital_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `marital_status_id` = p_id
            );
        when 'disease' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_health` where `disease_id` = p_id
            );
		
        when 'property_type' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `property_type_id` = p_id
            );
        when 'roof_materials' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `roof_material_id` = p_id
            );
        when 'work_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_status_id` = p_id
            );
        when 'work_reason' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_reason_id` = p_id
            );
		when 'work_jobs' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_job_id` = p_id
            );
        when 'work_wage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_wage_id` = p_id
            );
		when 'education_authority' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `authority` = p_id
            );
		when 'education_degree' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `degree` = p_id
            );
		when 'education_stage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `stage` = p_id
            );
        else
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `id` = p_id;
	end case;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_payments_cases_count`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;  
end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_payments_cases_count_`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                  (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                            (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_payments_cases_amount_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;  
end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_vouchers_value`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then
          
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null) != 0)));
           
               end if;
else
   if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
           
               end if;

   end if;
else

    if type = 'sponsors' then
          
              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id) != 0)));
           
               end if;
else

  if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then
                  
                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));

              
               elseif date_from is null and date_to is not null then
             
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then
            
                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers` 
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));
           
               end if;

    end if;



end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_payments_beneficiary_count`(`id` INT(10), `c_via` VARCHAR(45)) RETURNS int(11)
BEGIN
declare c INTEGER;
    
      if c_via = 'all' then
		      set c = (	select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( `p`.`payment_id`  =  id )	);
      elseif c_via = 'guaranteed' then
			    set c = (select COUNT(`p`.`sponsor_number`) 
FROM `char_payments_cases` AS `p` 
WHERE ( (`p`.`payment_id` = id)  and ( (`p`.`status` = 1) or (`p`.`status` = 2)) ));
      else
			    set c = (select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( (`p`.`payment_id` = id) and (`p`.`status` = 3)	));
      end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_payments_cases_amount`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then
                 
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and 
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;  
end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_payments_cases_count`(`id` INT(10), `payment_id` INT(10), `sponsor_number` VARCHAR(45)) RETURNS int(11)
BEGIN
declare c INTEGER;
  
		set c = (
			select COUNT(`p`.`payment_id`) FROM `char_payments_cases` AS `p`
			WHERE (`p`.`sponsor_number` = sponsor_number) and 
             (`p`.`case_id` = case_id) and (`p`.`payment_id` !=  payment_id)
		);
  
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_project_total`(`type` INT(11), `id` INT(11), `rep_org_id` INT(11)) RETURNS decimal(10,2)
BEGIN

declare c decimal(10,2);


    if type = 1 then
        set c = (SELECT round(sum(ratio),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));

    elseif type = 2 then                
        set c = (SELECT round(sum(amount),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));
    else
        set c = (SELECT round(sum(quantity),2) FROM `aid_projects_organizations` where project_id = id and organization_id in
(select  `char_organizations`.id from `char_organizations` where `char_organizations`.parent_id = rep_org_id));
	end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_payments_recipient_amount`(`id` INT(10), `p_via` VARCHAR(45), `payment_id` INT(10)) RETURNS int(11)
BEGIN
declare c INTEGER;

        if p_via = 'person' then
		set c = (             
            select sum(`p`.`amount`) 
FROM `char_payments_cases` AS `p`
join `char_cases` ON `char_cases`.`id` =`p`.`case_id`
join `char_persons` ON `char_persons`.`id` =`char_cases`.`person_id`
WHERE (`char_persons`.`id` = id ) and (`p`.`payment_id` =  payment_id) and (`p`.`status` !=  3)
);
  
    else
		set c = (  
                   select sum(`p`.`amount`) 
                   FROM `char_payments_cases` AS `p`
                   WHERE (`p`.`guardian_id` = id) and (`p`.`payment_id` =  payment_id)
                );
    end if;

  
RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_voucher_count_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(45)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then

if type = 'sponsors' then

if date_from is null and date_to is null then
	set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
				FROM  `char_vouchers` AS `v`
				join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
				WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));

elseif date_from is not null and date_to is null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));

elseif date_from is not null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

else

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) ));

elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

end if;

else


if type = 'sponsors' then

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))and (`v`.`category_id` = category_id)));

elseif date_from is not null and date_to is null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id)  and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));

elseif date_from is not null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

else

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs)))  and (`v`.`category_id` = category_id) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (select `char_organizations`.`id` FROM `char_organizations` where  FIND_IN_SET(`char_organizations`.`id`,orgs))) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

end if;



end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_total`(`type` INT(10),`repository_id` INT(10), `org_id` INT(10) , `factory` int(10)) RETURNS decimal(10,2)
BEGIN

declare c decimal(10,2);

    if type = 1 then
        set c = (SELECT round(sum(ratio / factory),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects 
				 where aid_projects.repository_id = repository_id ) );
 
    elseif type = 2 then
        set c = (SELECT round(sum(amount),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects 
			     where aid_projects.repository_id = repository_id ) );
 
    else
     	
        set c = (SELECT round(sum(quantity),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects 
                 where aid_projects.repository_id = repository_id ) );
 				
	    end if;

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_org_ratio`(`repository_id` INT(10), `org_id` INT(10) , `factory` int(10)) RETURNS decimal(5,2)
BEGIN

declare c decimal(5,2);

        set c = ( 
        
        SELECT round(sum(ratio / factory),2) FROM `aid_projects_organizations` where organization_id = org_id and project_id in (SELECT id from aid_projects 
        where aid_projects.repository_id = repository_id ) );
 

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `getfiles`(`id` INT) RETURNS text CHARSET utf8
BEGIN
	declare c text;
    set c =
            (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_persons_documents`
              join `doc_files` on `doc_files`.`id` = `char_persons_documents`.`document_id` 
              where `char_persons_documents`.`person_id`=id  
            );
      

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_banks_branches`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE bank_id, branch_id INT;
    DECLARE bank_name, branch_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `bank`, `branch` FROM
    (SELECT DISTINCT `bank`, `bbranch` as `branch` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_bank` as `bank`, `g_branch` as `branch` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `bank`, `branch` FROM `test`.`data_case`) AS banks_branches
    WHERE (`bank` NOT REGEXP '^[0-9]+$' AND `bank` IS NOT NULL)
    OR (`branch` NOT REGEXP '^[0-9]+$' AND `branch` IS NOT NULL);
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_banks`;
		TRUNCATE TABLE `char_branches`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO bank_name, branch_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET bank_id = NULL;
        IF bank_name IS NOT NULL AND bank_name NOT REGEXP '^[0-9]+$' THEN
			SET bank_name = normalize_text_ar(bank_name);
            
            CALL char_banks_save(bank_id, bank_name, false);
            
			IF bank_id IS NOT NULL THEN
				UPDATE `test`.`data` SET `bank` = bank_id WHERE  normalize_text_ar(`bank`) = bank_name;
				UPDATE `test`.`data_guardian` SET `g_bank` = bank_id WHERE  normalize_text_ar(`g_bank`) = bank_name;
				UPDATE `test`.`data_case` SET `bank` = bank_id WHERE  normalize_text_ar(`bank`) = bank_name;
            END IF;
		ELSEIF bank_name REGEXP '^[0-9]+$' THEN
			SET bank_id = bank_name;
		END IF;
        
        SET branch_id = NULL;
        IF bank_id IS NOT NULL AND branch_name IS NOT NULL AND branch_name NOT REGEXP '^[0-9]+$' THEN
			SET branch_name = normalize_text_ar(branch_name);
            
			CALL char_branches_save(branch_id, bank_id, branch_name, false);
			IF branch_id IS NOT NULL THEN
				UPDATE `test`.`data` SET `bbranch` = branch_id WHERE  normalize_text_ar(`bbranch`) = branch_name;
				UPDATE `test`.`data_guardian` SET `g_branch` = branch_id WHERE  normalize_text_ar(`g_branch`) = branch_name;
				UPDATE `test`.`data_case` SET `branch` = branch_id WHERE  normalize_text_ar(`branch`) = branch_name;
            END IF;
        END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_aid_cases`(IN `p_offset` INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id INT;
        
	DECLARE v_ID1 INT;
    DECLARE v_id VARCHAR(255);
    DECLARE v_sin VARCHAR(255);
    DECLARE v_name1 VARCHAR(255);
    DECLARE v_name2 VARCHAR(255);
    DECLARE v_name3 VARCHAR(255);
    DECLARE v_name4 VARCHAR(255);
    DECLARE v_ename1 VARCHAR(255);
    DECLARE v_ename2 VARCHAR(255);
    DECLARE v_ename3 VARCHAR(255);
    DECLARE v_ename4 VARCHAR(255);
    DECLARE v_bd DATE;
    DECLARE v_bp VARCHAR(255);
    DECLARE v_gender INT;
    DECLARE v_national INT;
    DECLARE v_wives INT;
    DECLARE v_ms INT;
    DECLARE v_refugee INT;
    DECLARE v_unrwa_no VARCHAR(255);
    DECLARE v_edu_degree INT;
    DECLARE v_edu_major VARCHAR(255);
    DECLARE v_family_count INT;
    DECLARE v_family_single INT;
    DECLARE v_males INT;
    DECLARE v_females INT;
    DECLARE v_country INT;
    DECLARE v_sp INT;
    DECLARE v_city INT;
    DECLARE v_street VARCHAR(255);
    DECLARE v_local INT;
    DECLARE v_mousqe VARCHAR(255);
    DECLARE v_mobile1 VARCHAR(255);
    DECLARE v_mobile2 VARCHAR(255);
    DECLARE v_phone VARCHAR(255);
    DECLARE v_bank INT;
    DECLARE v_bbranch INT;
    DECLARE v_acc_holder VARCHAR(255);
    DECLARE v_acc_no VARCHAR(255);
    DECLARE v_working INT;
    DECLARE v_can_work INT;
    DECLARE v_unwork_reason INT;
    DECLARE v_work_status INT;
    DECLARE v_job INT;
    DECLARE v_workplace VARCHAR(255);
    DECLARE v_income FLOAT;
    DECLARE v_property_type INT;
    DECLARE v_roof INT;
    DECLARE v_res_cond INT;
    DECLARE v_indoor_cond INT;
    DECLARE v_rooms INT;
    DECLARE v_house_cond INT;
    DECLARE v_habitable INT;
    DECLARE v_area FLOAT;
    DECLARE v_rent FLOAT;
    DECLARE v_org_id INT;
    DECLARE v_org_name VARCHAR(255);
    DECLARE v_fullname VARCHAR(255);

    
    DECLARE cur CURSOR FOR
    SELECT `data`.`ID1`,
    `data`.`id`,
    `data`.`sin`,
    `data`.`name1`,
    `data`.`name2`,
    `data`.`name3`,
    `data`.`name4`,
    `data`.`ename1`,
    `data`.`ename2`,
    `data`.`ename3`,
    `data`.`ename4`,
    `data`.`bd`,
    `data`.`bp`,
    `data`.`gender`,
    `data`.`national`,
    `data`.`wives`,
    `data`.`ms`,
    `data`.`refugee`,
    `data`.`unrwa_no`,
    `data`.`edu_degree`,
    `data`.`edu_major`,
    `data`.`family_count`,
    `data`.`family_single`,
    `data`.`males`,
    `data`.`females`,
    `data`.`country`,
    `data`.`sp`,
    `data`.`city`,
    `data`.`street`,
    `data`.`local`,
    `data`.`mousqe`,
    `data`.`mobile1`,
    `data`.`mobile2`,
    `data`.`phone`,
    `data`.`bank`,
    `data`.`bbranch`,
    `data`.`acc_holder`,
    `data`.`acc_no`,
    `data`.`working`,
    `data`.`can_work`,
    `data`.`unwork_reason`,
    `data`.`work_status`,
    `data`.`job`,
    `data`.`workplace`,
    `data`.`income`,
    `data`.`property_type`,
    `data`.`roof`,
    `data`.`res_cond`,
    `data`.`indoor_cond`,
    `data`.`rooms`,
    `data`.`house_cond`,
    `data`.`habitable`,
    `data`.`area`,
    `data`.`rent`,
    `data`.`org_id`,
    `data`.`org_name`,
    `data`.`fullname`
FROM `test`.`data`
WHERE `data`.`ID1` >= p_offset;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_ID1,
		v_id,
		v_sin,
		v_name1,
		v_name2,
		v_name3,
		v_name4,
		v_ename1,
		v_ename2,
		v_ename3,
		v_ename4,
		v_bd,
		v_bp,
		v_gender,
		v_national,
		v_wives,
		v_ms,
		v_refugee,
		v_unrwa_no,
		v_edu_degree,
		v_edu_major,
		v_family_count,
		v_family_single,
		v_males,
		v_females,
		v_country,
		v_sp,
		v_city,
		v_street,
		v_local,
		v_mousqe,
		v_mobile1,
		v_mobile2,
		v_phone,
		v_bank,
		v_bbranch,
		v_acc_holder,
		v_acc_no,
		v_working,
		v_can_work,
		v_unwork_reason,
		v_work_status,
		v_job,
		v_workplace,
		v_income,
		v_property_type,
		v_roof,
		v_res_cond,
		v_indoor_cond,
		v_rooms,
		v_house_cond,
		v_habitable,
		v_area,
		v_rent,
		v_org_id,
		v_org_name,
		v_fullname;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET id = null;
        CALL `char_persons_save`(id,
		v_name1, v_name2, v_name3, v_name4, null,
		v_sin,
		v_gender, v_ms, v_bd, v_bp, v_national,
		null, null,
		null, null, v_wives,
		v_refugee, v_unrwa_no, 
		v_country, v_sp, v_city, v_local, concat(v_street, ' -- ', v_mousqe),
		v_income, v_fullname,
		v_ename1, v_ename2, v_ename3, v_ename4,
		v_bank, v_acc_no, v_acc_holder, v_bbranch,
		null, null, null, null, v_edu_major, v_edu_degree, null,  null, null, null, null,
		null, null, null,
		null, null, null, null, null, null,
		v_working, v_can_work, v_work_status, v_unwork_reason, v_job, null, v_workplace,
		v_property_type, v_rent, v_roof, v_res_cond, v_indoor_cond, v_habitable, v_house_cond, v_rooms, v_area,
		v_mobile1, v_mobile2, v_phone);
		
        INSERT INTO `char_cases`
		(`person_id`,
		`organization_id`,
		`category_id`,
		`status`,
		`rank`,
		`user_id`,
		`created_at`)
		VALUES
		(id,
		v_org_id,
		5,
		0,
		0,
		1,
		NOW());
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_case`()
BEGIN
	-- Insert Case Person Data
	REPLACE INTO `char_persons`
	(`first_name`,
	`second_name`,
	`third_name`,
	`last_name`,
	`prev_family_name`,
	`id_card_number`,
	`gender`,
	`marital_status_id`,
	`birthday`,
	`birth_place`,
	`nationality`,
	`death_date`,
	`death_cause_id`,
	`father_id`,
	`mother_id`,
	`spouses`,
	`refugee`,
	`unrwa_card_number`,
	`country`,
	`governarate`,
	`city`,
	`location_id`,
	`street_address`,
	`monthly_income`,
	`created_at`,
	`source_id`,
	`source_org_id`)
	SELECT
		`data_case`.`name1`, -- first_name
		`data_case`.`name2`, -- second_name
		`data_case`.`name3`, -- third_name
		`data_case`.`name4`, -- last_name
		null, -- prev_family_name
		`data_case`.`sin`, -- id_card_number
		`data_case`.`gender`, -- gender
		null, -- marital_status_id
		`data_case`.`bd`, -- birthday
		`data_case`.`bp`, -- birth_place
		`data_case`.`national`, -- nationality
		null, -- death_date
		null, -- death_cause_id
		(SELECT `f`.`id` FROM `char_persons` `f` WHERE `f`.`source_id` = `data_case`.`f_id` LIMIT 1), -- father_id
		(SELECT `m`.`id` FROM `char_persons` `m` WHERE `m`.`source_id` = `data_case`.`m_id` LIMIT 1), -- mother_id
		NULL, -- spouses
		null, -- refugee
		null, -- unrwa_card_number
		`data_case`.`country`, -- country
		`data_case`.`sp`, -- governarate
		`data_case`.`city`, -- city
		`data_case`.`local`, -- location_id
		concat(`data_case`.`street`, ' - ', `data_case`.`mousqe`), -- street_address
		NULL, -- income
		NOW(), -- created_at
		`data_case`.`id`,
		`data_case`.`org_id`
	FROM `test`.`data_case`;

	-- Insert Person English name
	INSERT INTO `char_persons_i18n`
	(`person_id`,
	`language_id`,
	`first_name`,
	`second_name`,
	`third_name`,
	`last_name`)
	SELECT `char_persons`.`id`, 2, `test`.`data_case`.`ename1`, `test`.`data_case`.`ename2`, `test`.`data_case`.`ename3`, `test`.`data_case`.`ename4`
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
    WHERE `test`.`data_case`.`ename1` IS NOT NULL
    OR `test`.`data_case`.`ename2` IS NOT NULL
    OR `test`.`data_case`.`ename3` IS NOT NULL
    OR `test`.`data_case`.`ename4` IS NOT NULL;

	-- Insert Case
	INSERT INTO `char_cases`
	(`person_id`, `organization_id`, `category_id`, `status`, `rank`, `user_id`, `created_at`)
	SELECT
	`char_persons`.`id`, `data_case`.`org_id`, `test`.`data_case`.`category`, 1, 0, 1, NOW()
	FROM `char_persons` INNER JOIN
    `test`.`data_case` ON `char_persons`.`source_id` = `data_case`.`id`;
    
    INSERT INTO `char_guardians`
	(`guardian_id`,
	`individual_id`,
	`kinship_id`,
	`organization_id`)
	SELECT
	`g`.`id`, `i`.`id`, `data_guardian`.`g_kinship`, `data_case`.`org_id`
	FROM `char_persons` `i` INNER JOIN
    `test`.`data_case` ON `i`.`source_id` = `data_case`.`id`
    INNER JOIN `test`.`data_guardian` ON `data_case`.`g_id` = `data_guardian`.`g_id`
    INNER JOIN `char_persons` `g` ON `g`.`source_id` = `data_guardian`.`g_id`;


	-- Insert contact information
	REPLACE INTO `char_persons_contact`
	(`person_id`, `contact_type`, `contact_value`)
	SELECT `char_persons`.`id`, 'primary_mobile', `test`.`data_case`.`mobile1`
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
    WHERE `test`.`data_case`.`mobile1` IS NOT NULL;

	REPLACE INTO `char_persons_contact`
	(`person_id`, `contact_type`, `contact_value`)
	SELECT `char_persons`.`id`, 'secondery_mobile', `test`.`data_case`.`mobile2`
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE `test`.`data_case`.`mobile2` IS NOT NULL;

	REPLACE INTO `char_persons_contact`
	(`person_id`, `contact_type`, `contact_value`)
	SELECT `char_persons`.`id`, 'phone', `test`.`data_case`.`phone`
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE `test`.`data_case`.`phone` IS NOT NULL;

	-- Bank data
	REPLACE  INTO `char_persons_banks`
	(`person_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`)
	SELECT `char_persons`.`id`, `test`.`data_case`.`bank`, `test`.`data_case`.`acc_no`, `test`.`data_case`.`acc_name`, `test`.`data_case`.`branch`
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	AND `char_persons`.`source_org_id` = `test`.`data_case`.`org_id`
	WHERE `test`.`data_case`.bank IS NOT NULL;

	-- Education data
	REPLACE  INTO `char_persons_education`
	(`person_id`, `study`, `type`, `authority`, `stage`, `specialization`, `degree`, `grade`,
	`year`, `school`, `level`, `points`)
	SELECT `char_persons`.`id`, 1, null, `test`.`data_case`.authority,  `test`.`data_case`.grade , NULL,  `test`.`data_case`.stage,
	NULL,  `test`.`data_case`.year,  `test`.`data_case`.school,  `test`.`data_case`.edu_level,  `test`.`data_case`.points
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE `test`.`data_case`.authority IS NOT NULL
    OR `test`.`data_case`.grade IS NOT NULL
    OR`test`.`data_case`.stage IS NOT NULL
    OR `test`.`data_case`.year IS NOT NULL
    OR `test`.`data_case`.school IS NOT NULL
    OR `test`.`data_case`.edu_level IS NOT NULL
    OR `test`.`data_case`.points IS NOT NULL;

	-- Health data
	REPLACE  INTO `char_persons_health`
	(`person_id`, `condition`, `details`, `disease_id`)
	SELECT `char_persons`.`id`, `test`.`data_case`.health, `test`.`data_case`.health_details, `test`.`data_case`.disease
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE  `test`.`data_case`.health IS NOT NULL
    OR `test`.`data_case`.health_details IS NOT NULL
    OR `test`.`data_case`.disease IS NOT NULL;

	-- Prayer & Quran
	REPLACE  INTO `char_persons_islamic_commitment`
	(`person_id`, `prayer`, `quran_center`, `quran_parts`, `quran_chapters`, `prayer_reason`, `quran_reason`)
	SELECT `char_persons`.`id`, `test`.`data_case`.praying, `test`.`data_case`.quran_center, `test`.`data_case`.quran_chapters, `test`.`data_case`.quran_surat, `test`.`data_case`.unpray_reason, `test`.`data_case`.quran_reason
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE `test`.`data_case`.praying IS NOT NULL
    OR `test`.`data_case`.quran_center IS NOT NULL
    OR `test`.`data_case`.quran_chapters IS NOT NULL
    OR `test`.`data_case`.quran_surat IS NOT NULL
    OR `test`.`data_case`.unpray_reason IS NOT NULL
    OR `test`.`data_case`.quran_reason IS NOT NULL;

	-- House data
	REPLACE  INTO `char_residence`
	(`person_id`, `property_type_id`, `rent_value`, `roof_material_id`, /*`residence_condition`,*/ `indoor_condition`,
	`habitable`, `house_condition`, `rooms`, `area`)
	SELECT `char_persons`.`id`, `test`.`data_case`.property_type, `test`.`data_case`.rent, `test`.`data_case`.roof,
	/*`test`.`data_case`.res_cond,*/ `test`.`data_case`.indoor_cond, `test`.`data_case`.habitable, `test`.`data_case`.house_cond, `test`.`data_case`.rooms, `test`.`data_case`.area
	FROM `char_persons` INNER JOIN `test`.`data_case`
	ON `char_persons`.`source_id` = `test`.`data_case`.`id`
	WHERE `test`.`data_case`.property_type IS NOT NULL
    OR `test`.`data_case`.rent IS NOT NULL
    OR `test`.`data_case`.roof IS NOT NULL
    OR `test`.`data_case`.indoor_cond IS NOT NULL
    OR `test`.`data_case`.habitable IS NOT NULL
    OR `test`.`data_case`.house_cond IS NOT NULL
    OR `test`.`data_case`.rooms IS NOT NULL
    OR `test`.`data_case`.area IS NOT NULL;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_death_causes`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `cause` FROM
    (SELECT DISTINCT `f_death_reason` as `cause` FROM `test`.`data_father`
    UNION
    SELECT DISTINCT `m_death_reason` as `cause` FROM `test`.`data_mother`) AS death_causes
    WHERE `cause` IS NOT NULL AND `cause` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_death_causes`;
		TRUNCATE TABLE `char_death_causes_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_death_causes_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_father` SET `f_death_reason` = v_id WHERE normalize_text_ar(`f_death_reason`) = v_name;
            UPDATE `test`.`data_mother` SET `m_death_reason` = v_id WHERE normalize_text_ar(`m_death_reason`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_building_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `house_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_house_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `house_cond` AS `status` FROM `test`.`data_case`) AS building_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_building_status`;
		TRUNCATE TABLE `char_building_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_building_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `house_cond` = v_id WHERE normalize_text_ar(`house_cond`) = v_name;
            UPDATE `test`.`data_case` SET `house_cond` = v_id WHERE normalize_text_ar(`house_cond`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_house_cond` = v_id WHERE normalize_text_ar(`g_house_cond`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_voucher_count`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
   
 if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)));

     elseif date_from is not null and date_to is null then
        
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from )));
    
     elseif date_from is null and date_to is not null then
   
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date`  <= date_to )));
   
     elseif date_from is not null and date_to is not null then
  
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
   
     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
  
     end if;

     end if;

else


    if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`category_id` = category_id)));

     elseif date_from is not null and date_to is null then
        
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
    
     elseif date_from is null and date_to is not null then
   
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
   
     elseif date_from is not null and date_to is not null then
  
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
   
     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`category_id` = category_id) ));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`) 
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));
  
     end if;

     end if;



end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_edu_degrees`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `degree` FROM
    (SELECT DISTINCT `edu_degree` AS `degree` FROM `test`.`data`
    UNION
    SELECT DISTINCT `stage` AS `degree` FROM `test`.`data_case`
    UNION
    SELECT DISTINCT `m_degree` AS `degree` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `g_degree` AS `degree` FROM `test`.`data_guardian`) AS degrees
    WHERE `degree` IS NOT NULL AND `degree` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_degrees`;
		TRUNCATE TABLE `char_edu_degrees_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL AND v_name NOT REGEXP '[0-9]+' THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_degrees_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `stage` = v_id WHERE normalize_text_ar(`stage`) = v_name;
            UPDATE `test`.`data` SET `edu_degree` = v_id WHERE normalize_text_ar(`edu_degree`) = v_name;
            UPDATE `test`.`data_mother` SET `m_degree` = v_id WHERE normalize_text_ar(`m_degree`) = v_name;
            UPDATE `test`.`data_guardian` SET `g_degree` = v_id WHERE normalize_text_ar(`g_degree`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_organization_vouchers_value_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10), `orgs` VARCHAR(255)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
		if type = 'sponsors' then

				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
                       (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null,orgs) != 0)));

				elseif date_from is not null and date_to is null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
                      (`char_vouchers`.`voucher_date` >= date_from )
				      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null,orgs)  != 0)));


				elseif date_from is null and date_to is not null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
                      (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null,orgs)  != 0)));

				elseif date_from is not null and date_to is not null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
                      (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null,orgs) != 0)));

				end if;
		else
				if date_from is null and date_to is null then
				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));

				elseif date_from is not null and date_to is null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
				and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));


				elseif date_from is null and date_to is not null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));

				elseif date_from is not null and date_to is not null then

				set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
				FROM `char_vouchers`
				where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));

				end if;

		end if;
else

if type = 'sponsors' then

		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
              (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id,orgs) != 0)));

		elseif date_from is not null and date_to is null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and 
              (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		      and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id,orgs)  != 0)));


		elseif date_from is null and date_to is not null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id,orgs)  != 0)));

		elseif date_from is not null and date_to is not null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`organization_id` IN (select `char_organizations`.`id`
FROM   `char_organizations`
where FIND_IN_SET(`char_organizations`.`id`,orgs) != 0 )) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id,orgs) != 0)));

		end if;
else

		if date_from is null and date_to is null then
		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,null,null,orgs) != 0)));

		elseif date_from is not null and date_to is null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)
		and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
		and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null,orgs)  != 0)));


		elseif date_from is null and date_to is not null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null,orgs)  != 0)));

		elseif date_from is not null and date_to is not null then

		set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
		FROM `char_vouchers`
		where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count_(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null,orgs) != 0)));

		end if;

end if;

end if;


RETURN c;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_father`()
BEGIN

	DECLARE t DATETIME;
    SET t = now();
	
	INSERT INTO `takaful`.`char_persons`
	(`first_name`,
	`second_name`,
	`third_name`,
	`last_name`,
	`prev_family_name`,
	`id_card_number`,
	`gender`,
	`marital_status_id`,
	`birthday`,
	`birth_place`,
	`nationality`,
	`death_date`,
	`death_cause_id`,
	`father_id`,
	`mother_id`,
	`spouses`,
	`refugee`,
	`unrwa_card_number`,
	`country`,
	`governarate`,
	`city`,
	`location_id`,
	`street_address`,
	`monthly_income`,
	`created_at`,
	`source_id`,
	`source_org_id`)
	SELECT DISTINCT
    `data_father`.`f_name1`, -- first_name
    `data_father`.`f_name2`, -- second_name
    `data_father`.`f_name3`, -- third_name
    `data_father`.`f_name4`, -- last_name
    NULL, -- prev_family_name
    `data_father`.`f_sin`, -- id_card_number
    1, -- gender
    null, -- marital_status_id
    `data_father`.`f_bd`, -- birthday
    null, -- birth_place
    null, -- nationality
    `data_father`.`f_dod`, -- death_date
    `data_father`.`f_death_reason`, -- death_cause_id
    NULL, -- father_id
    NULL, -- mother_id
    `data_father`.`spouses`, -- spouses
    null, -- refugee
    null, -- unrwa_card_number
    null, -- country
    null, -- governarate
    null, -- city
    null, -- location_id
    null, -- street_address
    `data_father`.`f_income`, -- income
    t, -- created_at
    `data_father`.`f_id`,
    null
	FROM `test`.`data_father`;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_edu_authorities`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `authority` FROM `test`.`data_case`
    WHERE `authority` IS NOT NULL AND `authority` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_authorities`;
		TRUNCATE TABLE `char_edu_authorities_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_authorities_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `authority` = v_id WHERE normalize_text_ar(`authority`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_furniture_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `indoor_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_indoor_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `indoor_cond` AS `status` FROM `test`.`data_case`) AS furniture_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_furniture_status`;
		TRUNCATE TABLE `char_furniture_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_furniture_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `indoor_cond` = v_id WHERE normalize_text_ar(`indoor_cond`) = v_name;
            UPDATE `test`.`data_case` SET `indoor_cond` = v_id WHERE normalize_text_ar(`indoor_cond`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_indoor_cond` = v_id WHERE normalize_text_ar(`g_indoor_cond`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_diseases`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `disease` FROM
    (SELECT DISTINCT `m_disease` as `disease` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `disease` FROM `test`.`data_case`) AS diseases
    WHERE `disease` IS NOT NULL AND `disease` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_diseases`;
		TRUNCATE TABLE `char_diseases_i18n`;
		SET foreign_key_checks = 1;
    END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL AND v_name NOT REGEXP '^[0-9]+$' THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_diseases_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_mother` SET `m_disease` = v_id WHERE normalize_text_ar(`m_disease`) = v_name;
            UPDATE `test`.`data_case` SET `disease` = v_id WHERE normalize_text_ar(`disease`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_guardian`()
BEGIN
	
    DECLARE t DATETIME;
    SET t = now();
    
    -- Insert Guardian Person Data
	REPLACE INTO `char_persons`
	(`first_name`,
	`second_name`,
	`third_name`,
	`last_name`,
	`prev_family_name`,
	`id_card_number`,
	`gender`,
	`marital_status_id`,
	`birthday`,
	`birth_place`,
	`nationality`,
	`death_date`,
	`death_cause_id`,
	`father_id`,
	`mother_id`,
	`spouses`,
	`refugee`,
	`unrwa_card_number`,
	`country`,
	`governarate`,
	`city`,
	`location_id`,
	`street_address`,
	`monthly_income`,
	`created_at`,
	`source_id`,
	`source_org_id`)
	SELECT
		`data_guardian`.`g_name1`, -- first_name
		`data_guardian`.`g_name2`, -- second_name
		`data_guardian`.`g_name3`, -- third_name
		`data_guardian`.`g_name4`, -- last_name
		null, -- prev_family_name
		`data_guardian`.`g_sin`, -- id_card_number
		null, -- gender
		`data_guardian`.`g_ms`, -- marital_status_id
		`data_guardian`.`g_bd`, -- birthday
		null, -- birth_place
		1, -- nationality
		null, -- death_date
		null, -- death_cause_id
		NULL, -- father_id
		NULL, -- mother_id
		NULL, -- spouses
		null, -- refugee
		null, -- unrwa_card_number
		`data_guardian`.`g_country`, -- country
		`data_guardian`.`g_sp`, -- governarate
		`data_guardian`.`g_city`, -- city
		`data_guardian`.`g_local`, -- location_id
		concat(`data_guardian`.`g_street`, ' - ', `data_guardian`.`g_mousqe`), -- street_address
		`data_guardian`.`g_income`, -- income
		t, -- created_at
		`data_guardian`.`g_id`,
		null
	FROM `test`.`data_guardian`;
    
    INSERT INTO `char_persons_i18n`
	(`person_id`,
	`language_id`,
	`first_name`,
	`second_name`,
	`third_name`,
	`last_name`)
	SELECT `char_persons`.`id`, 2, `test`.`data_guardian`.`g_ename1`, `test`.`data_guardian`.`g_ename2`, `test`.`data_guardian`.`g_ename3`, `test`.`data_guardian`.`g_ename4`
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `test`.`data_guardian`.`g_ename1` IS NOT NULL
    OR `test`.`data_guardian`.`g_ename2` IS NOT NULL
    OR `test`.`data_guardian`.`g_ename3` IS NOT NULL
    OR `test`.`data_guardian`.`g_ename4` IS NOT NULL;

	-- Insert guardian contact information
	REPLACE INTO `char_persons_contact`
	(`person_id`,
	`contact_type`,
	`contact_value`)
	SELECT `char_persons`.`id`, 'primary_mobile', `test`.`data_guardian`.`g_mobile1`
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `data_guardian`.`g_mobile1` IS NOT NULL;

	REPLACE INTO `char_persons_contact`
	(`person_id`,
	`contact_type`,
	`contact_value`)
	SELECT `char_persons`.`id`, 'secondery_mobile', `test`.`data_guardian`.`g_mobile2`
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `data_guardian`.`g_mobile2` IS NOT NULL;

	REPLACE INTO `char_persons_contact`
	(`person_id`,
	`contact_type`,
	`contact_value`)
	SELECT `char_persons`.`id`, 'phone', `test`.`data_guardian`.`g_phone`
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `data_guardian`.`g_phone` IS NOT NULL;

	-- Bank guardian data
	REPLACE  INTO `char_persons_banks`
	(`person_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`)
	SELECT `char_persons`.`id`, `test`.`data_guardian`.`g_bank`, `test`.`data_guardian`.`g_acc_no`, `test`.`data_guardian`.`g_acc_name`, `test`.`data_guardian`.`g_branch`
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
	WHERE `test`.`data_guardian`.`g_bank` IS NOT NULL;

	-- House guardian data
	REPLACE  INTO `char_residence`
	(`person_id`, `property_type_id`, `rent_value`, `roof_material_id`, /*`residence_condition`, */ `indoor_condition`,
	`habitable`, `house_condition`, `rooms`, `area`)
	SELECT `char_persons`.`id`, `test`.`data_guardian`.g_property_type, `test`.`data_guardian`.g_rent, `test`.`data_guardian`.g_roof,
	/*`test`.`data_guardian`.g_res_cond,*/ `test`.`data_guardian`.g_indoor_cond, `test`.`data_guardian`.g_habitable, `test`.`data_guardian`.g_house_cond, `test`.`data_guardian`.g_rooms, `test`.`data_guardian`.g_area
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `test`.`data_guardian`.g_property_type IS NOT NULL
    OR `test`.`data_guardian`.g_rent IS NOT NULL
    OR `test`.`data_guardian`.g_roof IS NOT NULL
    OR `test`.`data_guardian`.g_indoor_cond IS NOT NULL
    OR `test`.`data_guardian`.g_habitable IS NOT NULL
    OR `test`.`data_guardian`.g_house_cond IS NOT NULL
    OR `test`.`data_guardian`.g_rooms IS NOT NULL
    OR `test`.`data_guardian`.g_area IS NOT NULL;

	-- Work data
	REPLACE  INTO `char_persons_work`
	(`person_id`, `working`, `can_work`, `work_status_id`, `work_reason_id`, `work_job_id`, `work_wage_id`, `work_location`)
	SELECT `char_persons`.`id`, `test`.`data_guardian`.g_working, null, null, null,
	`test`.`data_guardian`.g_job, NULL, `test`.`data_guardian`.g_wp
	FROM `char_persons` INNER JOIN `test`.`data_guardian`
	ON `char_persons`.`source_id` = `test`.`data_guardian`.`g_id`
    WHERE `test`.`data_guardian`.g_working IS NOT NULL
    OR `test`.`data_guardian`.g_job IS NOT NULL
    OR `test`.`data_guardian`.g_wp IS NOT NULL;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_kinship`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `g_kinship` FROM `test`.`data_guardian`
    WHERE `g_kinship` IS NOT NULL AND `g_kinship` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_kinship`;
		TRUNCATE TABLE `char_kinship_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_kinship_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_guardian` SET `g_kinship` = v_id WHERE normalize_text_ar(`g_kinship`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_edu_stages`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `grade` FROM `test`.`data_case`
    WHERE `grade` IS NOT NULL AND `grade` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_stages`;
		TRUNCATE TABLE `char_edu_stages_i18n`;
		SET foreign_key_checks = 1;
    END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_stages_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `grade` = v_id WHERE normalize_text_ar(`grade`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_habitable_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `habitable` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_habitable` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `habitable` AS `status` FROM `test`.`data_case`) AS habitable_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_habitable_status`;
		TRUNCATE TABLE `char_habitable_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_habitable_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `habitable` = v_id WHERE normalize_text_ar(`habitable`) = v_name;
            UPDATE `test`.`data_case` SET `habitable` = v_id WHERE normalize_text_ar(`habitable`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_habitable` = v_id WHERE normalize_text_ar(`g_habitable`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_mother`()
BEGIN

	DECLARE t DATETIME;
    SET t = now();
    
	REPLACE INTO `takaful`.`char_persons`
	(`first_name`,
	`second_name`,
	`third_name`,
	`last_name`,
	`prev_family_name`,
	`id_card_number`,
	`gender`,
	`marital_status_id`,
	`birthday`,
	`birth_place`,
	`nationality`,
	`death_date`,
	`death_cause_id`,
	`father_id`,
	`mother_id`,
	`spouses`,
	`refugee`,
	`unrwa_card_number`,
	`country`,
	`governarate`,
	`city`,
	`location_id`,
	`street_address`,
	`monthly_income`,
	`created_at`,
	`source_id`,
	`source_org_id`)
	SELECT
    `data_mother`.`m_name1`, -- first_name
    `data_mother`.`m_name2`, -- second_name
    `data_mother`.`m_name3`, -- third_name
    `data_mother`.`m_name4`, -- last_name
    `data_mother`.`m_name5`, -- prev_family_name
    `data_mother`.`m_sin`, -- id_card_number
    2, -- gender
    `data_mother`.`m_ms`, -- marital_status_id
    `data_mother`.`m_bd`, -- birthday
    null, -- birth_place
    1, -- nationality
    IF(`data_mother`.`m_dead` IS NOT NULL, `data_mother`.`m_dod`, NULL), -- death_date
    IF(`data_mother`.`m_dead` IS NOT NULL, `data_mother`.`m_death_reason`, NULL), -- death_cause_id
    NULL, -- father_id
    NULL, -- mother_id
	NULL, -- spouses
    null, -- refugee
    null, -- unrwa_card_number
    null, -- country
    null, -- governarate
    null, -- city
    null, -- location_id
    null, -- street_address
    `data_mother`.`m_income`, -- income
    t, -- created_at
    `data_mother`.`m_id`,
    null
	FROM `test`.`data_mother`;
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_house_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `res_cond` FROM `test`.`data`
    WHERE `res_cond` IS NOT NULL AND `res_cond` NOT REGEXP '^[0-9]+$';
    /*SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `res_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_res_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `res_cond` AS `status` FROM `test`.`data_case`) AS house_status;*/
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_house_status`;
		TRUNCATE TABLE `char_house_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_house_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `res_cond` = v_id WHERE  normalize_text_ar(`res_cond`) = v_name;
            -- UPDATE `test`.`data_case` SET `res_cond` = v_id WHERE normalize_text_ar(`res_cond`) = v_name;
			-- UPDATE `test`.`data_guardian` SET `g_habibatable` = v_id WHERE normalize_text_ar(`g_habibatable`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_payments_sponsors`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name, n_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `sponsor_name` FROM `test`.`payments`
    WHERE `sponsor_name` IS NOT NULL AND `sponsor_name` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		DELETE FROM `char_organizations` WHERE `type` = 2;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET v_id = NULL;
		SET n_name = normalize_text_ar(v_name);
		
		CALL `char_organizations_save`(v_id, n_name, NULL, 2, true);
		IF v_id IS NOT NULL THEN
			UPDATE `test`.`payments` SET `sponsor_id` = v_id WHERE normalize_text_ar(`sponsor_name`) = n_name;
		END IF;
    END LOOP;
    
    CLOSE cur;
    
    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_payments_banks`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE bank_id INT;
    DECLARE bank_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `bank` FROM `test`.`payments`
    WHERE `bank` NOT REGEXP '^[0-9]+$' AND `bank` IS NOT NULL;    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_banks`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO bank_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET bank_id = NULL;
			SET bank_name = normalize_text_ar(bank_name);
            CALL char_banks_save(bank_id, bank_name, false);
            
			IF bank_id IS NOT NULL THEN
				UPDATE `test`.`payments` SET `bank` = bank_id WHERE  normalize_text_ar(`bank`) = bank_name;
            END IF;
        
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_marital_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `ms` FROM
    (SELECT DISTINCT `ms` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_ms` AS `ms` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `m_ms` AS `ms` FROM `test`.`data_mother`) AS ms
    WHERE `ms` IS NOT NULL AND `ms` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_marital_status`;
		TRUNCATE TABLE `char_marital_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_marital_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `ms` = v_id WHERE normalize_text_ar(`ms`) = v_name;
            UPDATE `test`.`data_mother` SET `m_ms` = v_id WHERE normalize_text_ar(`m_ms`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_ms` = v_id WHERE normalize_text_ar(`g_ms`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_locations`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE country_id, district_id, city_id, neighborhood_id INT;
    DECLARE country_name, district_name, city_name, neighborhood_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `country`, `sp`, `city`, `local` FROM
    (SELECT DISTINCT `country`, `sp`, `city`, `local` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_country` AS `country`, `g_sp` AS `sp`, `g_city` AS `city`, `g_local` AS `local` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `country`, `sp`, `city`, `local` FROM `test`.`data_case`) AS locations
    WHERE `country` NOT REGEXP '^[0-9]+$'
    OR `sp` NOT REGEXP '^[0-9]+$'
    OR `city` NOT REGEXP '^[0-9]+$'
    OR`local` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_locations`;
		TRUNCATE TABLE `char_locations_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO country_name, district_name, city_name, neighborhood_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET country_id = NULL;
        IF country_name IS NOT NULL AND country_name NOT REGEXP '^[0-9]+$' THEN
			SET country_name = normalize_text_ar(country_name);
            
			CALL `char_locations_save`(country_id, 1, null, 1, country_name, false);
			UPDATE `test`.`data` SET `country` = country_id WHERE normalize_text_ar(`country`) = country_name;
            UPDATE `test`.`data_guardian` SET `g_country` = country_id WHERE normalize_text_ar(`g_country`) = country_name;
            UPDATE `test`.`data_case` SET `country` = country_id WHERE normalize_text_ar(`country`) = country_name;
		ELSEIF country_name REGEXP '^[0-9]+$' THEN
			SET country_id = country_name;
        END IF;
        
        SET district_id = NULL;
        IF country_id IS NOT NULL AND district_name IS NOT NULL AND district_name NOT REGEXP '^[0-9]+$' THEN
			SET district_name = normalize_text_ar(district_name);
            
			CALL `char_locations_save`(district_id, 2, country_id, 1, district_name, false);
			UPDATE `test`.`data` SET `sp` = district_id WHERE normalize_text_ar(`sp`) = district_name;
            UPDATE `test`.`data_guardian` SET `g_sp` = district_id WHERE normalize_text_ar(`g_sp`) = district_name;
            UPDATE `test`.`data_case` SET `sp` = district_id WHERE normalize_text_ar(`sp`) = district_name;
		ELSEIF district_name REGEXP '^[0-9]+$' THEN
			SET district_id = district_name;
        END IF;
        
        SET city_id = NULL;
        IF district_id IS NOT NULL AND city_name IS NOT NULL AND city_name NOT REGEXP '^[0-9]+$' THEN
			SET city_name = normalize_text_ar(city_name);
            
			CALL `char_locations_save`(city_id, 3, district_id, 1, city_name, false);
			UPDATE `test`.`data` SET `city` = city_id WHERE normalize_text_ar(`city`) = city_name;
            UPDATE `test`.`data_guardian` SET `g_city` = city_id WHERE normalize_text_ar(`g_city`) = city_name;
            UPDATE `test`.`data_case` SET `city` = city_id WHERE normalize_text_ar(`city`) = city_name;
		ELSEIF city_name REGEXP '^[0-9]+$' THEN
			SET city_id = city_name;
		END IF;
        
        SET neighborhood_id = NULL;
        IF city_id IS NOT NULL AND neighborhood_name IS NOT NULL AND neighborhood_name NOT REGEXP '^[0-9]+$' THEN
			SET neighborhood_name = normalize_text_ar(neighborhood_name);
            
			CALL `char_locations_save`(neighborhood_id, 4, city_id, 1, neighborhood_name, false);
			UPDATE `test`.`data` SET `local` = neighborhood_id WHERE normalize_text_ar(`local`) = neighborhood_name;
            UPDATE `test`.`data_guardian` SET `g_local` = neighborhood_id WHERE  normalize_text_ar(`g_local`) = neighborhood_name;
            UPDATE `test`.`data_case` SET `local` = neighborhood_id WHERE  normalize_text_ar(`local`) = neighborhood_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_relays_cases`(IN `p_offset` INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id INT;   
	DECLARE v_id INT;
    DECLARE id_card_number VARCHAR(255);
    DECLARE first_name VARCHAR(255);
    DECLARE second_name VARCHAR(255);
    DECLARE third_name VARCHAR(255);
    DECLARE last_name VARCHAR(255);
    DECLARE full_name VARCHAR(255);
    DECLARE prev_family_name VARCHAR(255);
    DECLARE gender INT;
    DECLARE marital_status_id INT;
    DECLARE birthday DATE;
    DECLARE death_date DATE;
    DECLARE male_live INT;
    DECLARE female_live INT;
    DECLARE family_cnt INT;
    DECLARE adscountry_id INT;
    DECLARE adsdistrict_id INT;
    DECLARE adsregion_id INT;
    DECLARE adsneighborhood_id INT;
    DECLARE adssquare_id INT;
    DECLARE adsmosques_id INT;
    DECLARE street_address VARCHAR(255);
    DECLARE primary_mobile VARCHAR(255);
    DECLARE phone VARCHAR(255);

    DECLARE cur CURSOR FOR
    SELECT  `merge`.`id`,
    `merge`.`id_card_number`,
    `merge`.`first_name`,
    `merge`.`second_name`,
    `merge`.`third_name`,
    `merge`.`last_name`,
    `merge`.`full_name`,
    `merge`.`prev_family_name`,
    `merge`.`gender`,
    `merge`.`marital_status_id`,
    `merge`.`birthday`,
    `merge`.`death_date`,
    `merge`.`male_live`,
    `merge`.`female_live`,
    `merge`.`family_cnt`,
    `merge`.`adscountry_id`,
    `merge`.`adsdistrict_id`,
    `merge`.`adsregion_id`,`merge`.`adsneighborhood_id`,`merge`.`adssquare_id`,
    `merge`.`adsmosques_id`,`merge`.`street_address`, `merge`.`primary_mobile`,
    `merge`.`phone`
	FROM `takaful_aft_add_db`.`merge`
	WHERE `merge`.`id` >= p_offset;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id,id_card_number,first_name,second_name,third_name,last_name,full_name,prev_family_name,
								 gender,  marital_status_id,  birthday,death_date,  male_live, female_live, family_cnt,
								 adscountry_id,adsdistrict_id,adsregion_id, adsneighborhood_id, adssquare_id, adsmosques_id,
								 street_address, primary_mobile, phone;
		
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET id = null;
        CALL `relays_cases_save`(id,id_card_number,first_name,second_name,third_name,last_name,full_name,prev_family_name,
								 gender,  marital_status_id,  birthday,death_date,  male_live, female_live, family_cnt,
								 adscountry_id,adsdistrict_id,adsregion_id, adsneighborhood_id, adssquare_id, adsmosques_id,
								 street_address, primary_mobile, phone);
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_payments_currencies`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name1, v_name2 VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `currency1`, `currency2` FROM `test`.`payments`
    WHERE (`currency1` NOT REGEXP '^[0-9]+$' AND `currency1` IS NOT NULL)
    OR (`currency2` NOT REGEXP '^[0-9]+$' AND `currency2` IS NOT NULL);    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_currencies`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name1, v_name2;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
		SET v_name1 = normalize_text_ar(v_name1);
        SET v_name2 = normalize_text_ar(v_name2);
		
        SET v_id = NULL;
        CALL char_currencies_save(v_id, v_name1, '', '', 0, false);
        IF v_id IS NOT NULL THEN
			UPDATE `test`.`payments` SET `currency1` = v_id WHERE  normalize_text_ar(`currency1`) = v_name1;
		END IF;
        
        IF v_name1 <> v_name2 THEN
			SET v_id = NULL;
			CALL char_currencies_save(v_id, v_name2, '', '', 0, false);
        END IF;
        
        IF v_id IS NOT NULL THEN
			UPDATE `test`.`payments` SET `currency2` = v_id WHERE  normalize_text_ar(`currency2`) = v_name2;
		END IF;
        
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_property_types`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `property_type` FROM
    (SELECT DISTINCT `property_type` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_property_type` AS `ms` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `property_type` FROM `test`.`data_case`) AS property_types
    WHERE `property_type` IS NOT NULL AND `property_type` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_property_types`;
		TRUNCATE TABLE `char_property_types_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_property_types_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `property_type` = v_id WHERE  normalize_text_ar(`property_type`) = v_name;
            UPDATE `test`.`data_case` SET `property_type` = v_id WHERE normalize_text_ar(`property_type`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_property_type` = v_id WHERE normalize_text_ar(`g_property_type`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_roof_materials`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `roof` FROM
    (SELECT DISTINCT `roof` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_roof` AS `roof` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `roof` FROM `test`.`data_case`) AS roof_materials
    WHERE `roof` IS NOT NULL AND `roof` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_roof_materials`;
		TRUNCATE TABLE `char_roof_materials_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_roof_materials_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `roof` = v_id WHERE normalize_text_ar(`roof`) = v_name;
            UPDATE `test`.`data_case` SET `roof` = v_id WHERE normalize_text_ar(`roof`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_roof` = v_id WHERE normalize_text_ar(`g_roof`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_vouchers_sponsors`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name, n_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `sponsor_name` FROM `test`.`vouchers`
    WHERE `sponsor_name` IS NOT NULL AND `sponsor_name` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		DELETE FROM `char_organizations` WHERE `type` = 2;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET v_id = NULL;
		SET n_name = normalize_text_ar(v_name);
		
		CALL `char_organizations_save`(v_id, n_name, NULL, 2, true);
		IF v_id IS NOT NULL THEN
			UPDATE `test`.`vouchers` SET `sponsor_id` = v_id WHERE normalize_text_ar(`sponsor_name`) = n_name;
		END IF;
    END LOOP;
    
    CLOSE cur;
    
    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_sponsorships_sponsors`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name, n_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `sponsor_name` FROM `test`.`sponsorships`
    WHERE `sponsor_name` IS NOT NULL AND `sponsor_name` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		DELETE FROM `char_organizations` WHERE `type` = 2;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET v_id = NULL;
		SET n_name = normalize_text_ar(v_name);
		
		CALL `char_organizations_save`(v_id, n_name, NULL, 2, true);
		IF v_id IS NOT NULL THEN
			UPDATE `test`.`sponsorships` SET `sponsor_id` = v_id WHERE normalize_text_ar(`sponsor_name`) = n_name;
		END IF;
    END LOOP;
    
    CLOSE cur;
    
    COMMIT;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_sponsorship_cases`(IN `p_offset` INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    
    DECLARE id, father_id, mother_id, guardian_id INT;
    
    DECLARE v_ID1 INT;
    DECLARE v_id VARCHAR(255);
    DECLARE v_sin VARCHAR(255);
    DECLARE v_name1 VARCHAR(255);
    DECLARE v_name2 VARCHAR(255);
    DECLARE v_name3 VARCHAR(255);
    DECLARE v_name4 VARCHAR(255);
    DECLARE v_ename1 VARCHAR(255);
    DECLARE v_ename2 VARCHAR(255);
    DECLARE v_ename3 VARCHAR(255);
    DECLARE v_ename4 VARCHAR(255);
    DECLARE v_gender INT;
    DECLARE v_national INT;
    DECLARE v_bp VARCHAR(255);
    DECLARE v_bd DATE;
    DECLARE v_study_type INT;
    DECLARE v_authority INT;
    DECLARE v_stage INT;
    DECLARE v_grade INT;
    DECLARE v_year VARCHAR(255);
    DECLARE v_school VARCHAR(255);
    DECLARE v_points VARCHAR(255);
    DECLARE v_edu_level INT;
    DECLARE v_health INT;
    DECLARE v_disease INT;
    DECLARE v_health_details VARCHAR(255);
    DECLARE v_praying INT;
    DECLARE v_unpray_reason VARCHAR(255);
    DECLARE v_quran INT;
    DECLARE v_quran_reason VARCHAR(255);
    DECLARE v_quran_chapters VARCHAR(255);
    DECLARE v_quran_surat VARCHAR(255);
    DECLARE v_quran_center VARCHAR(255);
    DECLARE v_needs VARCHAR(255);
    DECLARE v_skills VARCHAR(255);
    DECLARE v_country INT;
    DECLARE v_sp INT;
    DECLARE v_city INT;
    DECLARE v_local INT;
    DECLARE v_street VARCHAR(255);
    DECLARE v_mousqe VARCHAR(255);
    DECLARE v_property_type INT;
    DECLARE v_roof INT;
    DECLARE v_house_cond INT;
    DECLARE v_indoor_cond INT;
    DECLARE v_rooms INT;
    DECLARE v_habitable INT;
    DECLARE v_area FLOAT;
    DECLARE v_rent FLOAT;
    DECLARE v_bank INT;
    DECLARE v_branch INT;
    DECLARE v_acc_name VARCHAR(255);
    DECLARE v_acc_no VARCHAR(255);
    DECLARE v_sponsored VARCHAR(255);
    DECLARE v_category INT;
    DECLARE v_org_id INT;
    DECLARE v_org_name VARCHAR(255);
    DECLARE v_fullname VARCHAR(255);
    DECLARE v_f_id VARCHAR(255);
    DECLARE v_f_sin VARCHAR(255);
    DECLARE v_f_name1 VARCHAR(255);
    DECLARE v_f_name2 VARCHAR(255);
    DECLARE v_f_name3 VARCHAR(255);
    DECLARE v_f_name4 VARCHAR(255);
    DECLARE v_f_ename1 VARCHAR(255);
    DECLARE v_f_ename2 VARCHAR(255);
    DECLARE v_f_ename3 VARCHAR(255);
    DECLARE v_f_ename4 VARCHAR(255);
    DECLARE v_f_bd DATE;
    DECLARE v_spouses INT;
    DECLARE v_f_working INT;
    DECLARE v_f_job INT;
    DECLARE v_f_wp VARCHAR(255);
    DECLARE v_f_income FLOAT;
    DECLARE v_f_degree INT;
    DECLARE v_f_major VARCHAR(255);
    DECLARE v_f_dead VARCHAR(255);
    DECLARE v_f_dod DATE;
    DECLARE v_f_death_reason INT;
    DECLARE v_f_fullname VARCHAR(255);
    DECLARE v_g_id VARCHAR(255);
    DECLARE v_g_sin VARCHAR(255);
    DECLARE v_g_name1 VARCHAR(255);
    DECLARE v_g_name2 VARCHAR(255);
    DECLARE v_g_name3 VARCHAR(255);
    DECLARE v_g_name4 VARCHAR(255);
    DECLARE v_g_ename1 VARCHAR(255);
    DECLARE v_g_ename2 VARCHAR(255);
    DECLARE v_g_ename3 VARCHAR(255);
    DECLARE v_g_ename4 VARCHAR(255);
    DECLARE v_g_bd DATE;
    DECLARE v_g_ms INT;
    DECLARE v_g_national INT;
    DECLARE v_g_kinship INT;
    DECLARE v_g_working INT;
    DECLARE v_g_job INT;
    DECLARE v_g_wp VARCHAR(255);
    DECLARE v_g_income FLOAT;
    DECLARE v_g_degree INT;
    DECLARE v_g_stage INT;
    DECLARE v_g_country INT;
    DECLARE v_g_sp INT;
    DECLARE v_g_city INT;
    DECLARE v_g_street VARCHAR(255);
    DECLARE v_g_local INT;
    DECLARE v_g_mousqe VARCHAR(255);
    DECLARE v_g_property_type INT;
    DECLARE v_g_roof INT;
    DECLARE v_g_house_cond INT;
    DECLARE v_g_indoor_cond INT;
    DECLARE v_g_rooms INT;
    DECLARE v_g_habitable INT;
    DECLARE v_g_area FLOAT;
    DECLARE v_g_rent FLOAT;
    DECLARE v_g_mobile1 VARCHAR(255);
    DECLARE v_g_mobile2 VARCHAR(255);
    DECLARE v_g_phone VARCHAR(255);
    DECLARE v_g_bank INT;
    DECLARE v_g_branch INT;
    DECLARE v_g_acc_name VARCHAR(255);
    DECLARE v_g_acc_no VARCHAR(255);
    DECLARE v_g_fullname VARCHAR(255);
    DECLARE v_m_id VARCHAR(255);
    DECLARE v_m_sin VARCHAR(255);
    DECLARE v_m_name1 VARCHAR(255);
    DECLARE v_m_name2 VARCHAR(255);
    DECLARE v_m_name3 VARCHAR(255);
    DECLARE v_m_name4 VARCHAR(255);
    DECLARE v_m_name5 VARCHAR(255);
    DECLARE v_m_ename1 VARCHAR(255);
    DECLARE v_m_ename2 VARCHAR(255);
    DECLARE v_m_ename3 VARCHAR(255);
    DECLARE v_m_ename4 VARCHAR(255);
    DECLARE v_m_ename5 VARCHAR(255);
    DECLARE v_m_bd DATE;
    DECLARE v_m_ms INT;
    DECLARE v_m_national INT;
    DECLARE v_m_working INT;
    DECLARE v_m_job INT;
    DECLARE v_m_wp VARCHAR(255);
    DECLARE v_m_income FLOAT;
    DECLARE v_m_degree INT;
    DECLARE v_m_major VARCHAR(255);
    DECLARE v_m_health INT;
    DECLARE v_m_disease INT;
    DECLARE v_m_health_details VARCHAR(255);
    DECLARE v_m_dead VARCHAR(255);
    DECLARE v_m_dod DATE;
    DECLARE v_m_death_reason INT;
    DECLARE v_m_guardian VARCHAR(255);
    DECLARE v_family_count INT;
    DECLARE v_singles INT;
    DECLARE v_males INT;
    DECLARE v_females INT;
    DECLARE v_m_fullname VARCHAR(255);
    
    DECLARE cur CURSOR FOR
    SELECT `data_case`.`ID1`,
    `data_case`.`id`,
    `data_case`.`sin`,
    `data_case`.`name1`,
    `data_case`.`name2`,
    `data_case`.`name3`,
    `data_case`.`name4`,
    `data_case`.`ename1`,
    `data_case`.`ename2`,
    `data_case`.`ename3`,
    `data_case`.`ename4`,
    `data_case`.`gender`,
    `data_case`.`national`,
    `data_case`.`bp`,
    `data_case`.`bd`,
    `data_case`.`study_type`,
    `data_case`.`authority`,
    `data_case`.`stage`,
    `data_case`.`grade`,
    `data_case`.`year`,
    `data_case`.`school`,
    `data_case`.`points`,
    `data_case`.`edu_level`,
    `data_case`.`health`,
    `data_case`.`disease`,
    `data_case`.`health_details`,
    `data_case`.`praying`,
    `data_case`.`unpray_reason`,
    `data_case`.`quran`,
    `data_case`.`quran_reason`,
    `data_case`.`quran_chapters`,
    `data_case`.`quran_surat`,
    `data_case`.`quran_center`,
    `data_case`.`needs`,
    `data_case`.`skills`,
    `data_case`.`country`,
    `data_case`.`sp`,
    `data_case`.`city`,
    `data_case`.`local`,
    `data_case`.`street`,
    `data_case`.`mousqe`,
    `data_case`.`property_type`,
    `data_case`.`roof`,
    `data_case`.`house_cond`,
    `data_case`.`indoor_cond`,
    `data_case`.`rooms`,
    `data_case`.`habitable`,
    `data_case`.`area`,
    `data_case`.`rent`,
    `data_case`.`bank`,
    `data_case`.`branch`,
    `data_case`.`acc_name`,
    `data_case`.`acc_no`,
    `data_case`.`sponsored`,
    `data_case`.`category`,
    `data_case`.`org_id`,
    `data_case`.`org_name`,
    `data_case`.`fullname`,
    `data_father`.`f_id`,
    `data_father`.`f_sin`,
    `data_father`.`f_name1`,
    `data_father`.`f_name2`,
    `data_father`.`f_name3`,
    `data_father`.`f_name4`,
    `data_father`.`f_ename1`,
    `data_father`.`f_ename2`,
    `data_father`.`f_ename3`,
    `data_father`.`f_ename4`,
    `data_father`.`f_bd`,
    `data_father`.`spouses`,
    `data_father`.`f_working`,
    `data_father`.`f_job`,
    `data_father`.`f_wp`,
    `data_father`.`f_income`,
    `data_father`.`f_degree`,
    `data_father`.`f_major`,
    `data_father`.`f_dead`,
    `data_father`.`f_dod`,
    `data_father`.`f_death_reason`,
    `data_father`.`fullname` AS `f_fullname`,
    `data_guardian`.`g_id`,
    `data_guardian`.`g_sin`,
    `data_guardian`.`g_name1`,
    `data_guardian`.`g_name2`,
    `data_guardian`.`g_name3`,
    `data_guardian`.`g_name4`,
    `data_guardian`.`g_ename1`,
    `data_guardian`.`g_ename2`,
    `data_guardian`.`g_ename3`,
    `data_guardian`.`g_ename4`,
    `data_guardian`.`g_bd`,
    `data_guardian`.`g_ms`,
    `data_guardian`.`g_national`,
    `data_guardian`.`g_kinship`,
    `data_guardian`.`g_working`,
    `data_guardian`.`g_job`,
    `data_guardian`.`g_wp`,
    `data_guardian`.`g_income`,
    `data_guardian`.`g_degree`,
    `data_guardian`.`g_stage`,
    `data_guardian`.`g_country`,
    `data_guardian`.`g_sp`,
    `data_guardian`.`g_city`,
    `data_guardian`.`g_street`,
    `data_guardian`.`g_local`,
    `data_guardian`.`g_mousqe`,
    `data_guardian`.`g_property_type`,
    `data_guardian`.`g_roof`,
    `data_guardian`.`g_house_cond`,
    `data_guardian`.`g_indoor_cond`,
    `data_guardian`.`g_rooms`,
    `data_guardian`.`g_habitable`,
    `data_guardian`.`g_area`,
    `data_guardian`.`g_rent`,
    `data_guardian`.`g_mobile1`,
    `data_guardian`.`g_mobile2`,
    `data_guardian`.`g_phone`,
    `data_guardian`.`g_bank`,
    `data_guardian`.`g_branch`,
    `data_guardian`.`g_acc_name`,
    `data_guardian`.`g_acc_no`,
    `data_guardian`.`fullname` AS `g_fullname`,
    `data_mother`.`m_id`,
    `data_mother`.`m_sin`,
    `data_mother`.`m_name1`,
    `data_mother`.`m_name2`,
    `data_mother`.`m_name3`,
    `data_mother`.`m_name4`,
    `data_mother`.`m_name5`,
    `data_mother`.`m_ename1`,
    `data_mother`.`m_ename2`,
    `data_mother`.`m_ename3`,
    `data_mother`.`m_ename4`,
    `data_mother`.`m_ename5`,
    `data_mother`.`m_bd`,
    `data_mother`.`m_ms`,
    `data_mother`.`m_national`,
    `data_mother`.`m_working`,
    `data_mother`.`m_job`,
    `data_mother`.`m_wp`,
    `data_mother`.`m_income`,
    `data_mother`.`m_degree`,
    `data_mother`.`m_major`,
    `data_mother`.`m_health`,
    `data_mother`.`m_disease`,
    `data_mother`.`m_health_details`,
    `data_mother`.`m_dead`,
    `data_mother`.`m_dod`,
    `data_mother`.`m_death_reason`,
    `data_mother`.`m_guardian`,
    `data_mother`.`family_count`,
    `data_mother`.`singles`,
    `data_mother`.`males`,
    `data_mother`.`females`,
    `data_mother`.`fullname` AS `m_fullname`
FROM `test`.`data_case`
INNER JOIN  `test`.`data_father` ON `data_case`.`ID1` = `data_father`.`ID1`
INNER JOIN  `test`.`data_guardian` ON `data_case`.`ID1` = `data_guardian`.`ID1`
INNER JOIN  `test`.`data_mother` ON `data_case`.`ID1` = `data_mother`.`ID1`
WHERE `data_case`.`ID1` >= p_offset
ORDER BY `data_case`.`ID1`;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_ID1, 
		v_id , 
		v_sin , 
		v_name1 , 
		v_name2 , 
		v_name3 , 
		v_name4 , 
		v_ename1 , 
		v_ename2 , 
		v_ename3 , 
		v_ename4 , 
		v_gender, 
		v_national, 
		v_bp , 
		v_bd, 
		v_study_type, 
		v_authority, 
		v_stage, 
		v_grade, 
		v_year , 
		v_school , 
		v_points , 
		v_edu_level, 
		v_health, 
		v_disease, 
		v_health_details , 
		v_praying, 
		v_unpray_reason , 
		v_quran, 
		v_quran_reason , 
		v_quran_chapters , 
		v_quran_surat , 
		v_quran_center , 
		v_needs , 
		v_skills , 
		v_country, 
		v_sp, 
		v_city, 
		v_local, 
		v_street , 
		v_mousqe , 
		v_property_type, 
		v_roof, 
		v_house_cond, 
		v_indoor_cond, 
		v_rooms, 
		v_habitable, 
		v_area, 
		v_rent, 
		v_bank, 
		v_branch, 
		v_acc_name , 
		v_acc_no , 
		v_sponsored , 
		v_category, 
		v_org_id, 
		v_org_name , 
		v_fullname , 
		v_f_id , 
		v_f_sin , 
		v_f_name1 , 
		v_f_name2 , 
		v_f_name3 , 
		v_f_name4 , 
		v_f_ename1 , 
		v_f_ename2 , 
		v_f_ename3 , 
		v_f_ename4 , 
		v_f_bd, 
		v_spouses, 
		v_f_working, 
		v_f_job, 
		v_f_wp , 
		v_f_income, 
		v_f_degree, 
		v_f_major , 
		v_f_dead , 
		v_f_dod, 
		v_f_death_reason, 
		v_f_fullname , 
		v_g_id , 
		v_g_sin , 
		v_g_name1 , 
		v_g_name2 , 
		v_g_name3 , 
		v_g_name4 , 
		v_g_ename1 , 
		v_g_ename2 , 
		v_g_ename3 , 
		v_g_ename4 , 
		v_g_bd, 
		v_g_ms, 
		v_g_national, 
		v_g_kinship, 
		v_g_working, 
		v_g_job, 
		v_g_wp , 
		v_g_income, 
		v_g_degree, 
		v_g_stage, 
		v_g_country, 
		v_g_sp, 
		v_g_city, 
		v_g_street , 
		v_g_local, 
		v_g_mousqe , 
		v_g_property_type, 
		v_g_roof, 
		v_g_house_cond, 
		v_g_indoor_cond, 
		v_g_rooms, 
		v_g_habitable, 
		v_g_area, 
		v_g_rent, 
		v_g_mobile1 , 
		v_g_mobile2 , 
		v_g_phone , 
		v_g_bank, 
		v_g_branch, 
		v_g_acc_name , 
		v_g_acc_no , 
		v_g_fullname , 
		v_m_id , 
		v_m_sin , 
		v_m_name1 , 
		v_m_name2 , 
		v_m_name3 , 
		v_m_name4 , 
		v_m_name5 , 
		v_m_ename1 , 
		v_m_ename2 , 
		v_m_ename3 , 
		v_m_ename4 , 
		v_m_ename5 , 
		v_m_bd, 
		v_m_ms, 
		v_m_national, 
		v_m_working, 
		v_m_job, 
		v_m_wp , 
		v_m_income, 
		v_m_degree, 
		v_m_major , 
		v_m_health, 
		v_m_disease, 
		v_m_health_details , 
		v_m_dead , 
		v_m_dod, 
		v_m_death_reason, 
		v_m_guardian , 
		v_family_count, 
		v_singles, 
		v_males, 
		v_females, 
		v_m_fullname;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET father_id = null;
        CALL `char_persons_save`(father_id,
		v_f_name1, v_f_name2, v_f_name3, v_f_name4, null,
		v_f_sin,
		1, null, v_f_bd, null, 1,
		v_f_dod, v_f_death_reason,
		null, null, v_spouses,
		null, null, 
		null, null, null, null, null,
		null, v_f_fullname,
		v_f_ename1, v_f_ename2, v_f_ename3, v_f_ename4,
		null, null, null, null,
		null, null, null, null, v_f_major, v_f_degree, null,  null, null, null, null,
		null, null, null,
		null, null, null, null, null, null,
		null, null, null, null, v_f_job, null, v_f_wp,
		null, null, null, null, null, null, null, null, null,
		null, null, null);
        
        SET mother_id = null;
        CALL `char_persons_save`(mother_id,
		v_m_name1, v_m_name2, v_m_name3, v_m_name4, v_m_name5,
		v_m_sin,
		2, v_m_ms, v_m_bd, null, v_m_national,
		v_m_dod, v_m_death_reason,
		null, null, null,
		null, null, 
		null, null, null, null, null,
		v_m_income, v_m_fullname,
		v_m_ename1, v_m_ename2, v_m_ename3, v_m_ename4,
		null, null, null, null,
		null, null, null, null, v_m_major, v_m_degree, null,  null, null, null, null,
		v_m_health, v_m_health_details, v_m_disease,
		null, null, null, null, null, null,
		v_m_working, null, null, null, v_m_job, null, v_m_wp,
		null, null, null, null, null, null, null, null, null,
		null, null, null);
        
        IF v_m_guardian = 1 THEN
			SET guardian_id = mother_id;
            IF v_g_name1 IS NULL THEN SET v_g_name1 = v_m_name1; END IF;
            IF v_g_name2 IS NULL THEN SET v_g_name2 = v_m_name2; END IF;
            IF v_g_name3 IS NULL THEN SET v_g_name3 = v_m_name3; END IF;
            IF v_g_name4 IS NULL THEN SET v_g_name4 = v_m_name4; END IF;
            IF v_g_ename1 IS NULL THEN SET v_g_ename1 = v_m_ename1; END IF;
            IF v_g_ename2 IS NULL THEN SET v_g_ename2 = v_m_ename2; END IF;
            IF v_g_ename3 IS NULL THEN SET v_g_ename3 = v_m_ename3; END IF;
            IF v_g_ename4 IS NULL THEN SET v_g_ename4 = v_m_ename4; END IF;
            IF v_g_sin IS NULL THEN SET v_g_sin = v_m_sin; END IF;
            
            CALL `char_persons_save`(guardian_id,
			v_g_name1, v_g_name2, v_g_name3, v_g_name4, null,
			v_g_sin,
			2, ifnull(v_g_ms, v_m_ms), ifnull(v_g_bd, v_m_bd), null, ifnull(v_g_national, v_m_national),
			null, null,
			null, null, null,
			null, null, 
			v_g_country, v_g_sp, v_g_city, v_g_local, concat(v_g_street, ' -- ', v_g_mousqe),
			v_g_income, ifnull(v_g_fullname, v_m_fullname),
			v_g_ename1, v_g_ename2, v_g_ename3, v_g_ename4,
			v_g_bank, v_g_acc_no, v_g_acc_name, v_g_branch,
			null, null, null, v_g_stage, v_m_major, v_g_degree, null, null, null, null, null,
			v_m_health, v_m_health_details, v_m_disease,
			null, null, null, null, null, null,
			v_g_working, null, null, null, v_g_job, null, v_g_wp,
			v_g_property_type, v_g_rent, v_g_roof, null, v_g_indoor_cond, v_g_habitable, v_g_house_cond, v_g_rooms, v_g_area,
			v_g_mobile1, v_g_mobile2, v_g_phone);
            
        ELSE
			SET guardian_id = null;
            CALL `char_persons_save`(guardian_id,
			v_g_name1, v_g_name2, v_g_name3, v_g_name4, null,
			v_g_sin,
			null, v_g_ms, v_g_bd, null, v_g_national,
			null, null,
			null, null, null,
			null, null, 
			v_g_country, v_g_sp, v_g_city, v_g_local, concat(v_g_street, ' -- ', v_g_mousqe),
			v_g_income, v_g_fullname,
			v_g_ename1, v_g_ename2, v_g_ename3, v_g_ename4,
			v_g_bank, v_g_acc_no, v_g_acc_name, v_g_branch,
			null, null, null, v_g_stage, null, v_g_degree, null, null, null, null, null,
			null, null, null,
			null, null, null, null, null, null,
			v_g_working, null, null, null, v_g_job, null, v_g_wp,
			v_g_property_type, v_g_rent, v_g_roof, null, v_g_indoor_cond, v_g_habitable, v_g_house_cond, v_g_rooms, v_g_area,
			v_g_mobile1, v_g_mobile2, v_g_phone);
		END IF;
        
        SET id = null;
        CALL `char_persons_save`(id,
		v_name1, v_name2, v_name3, v_name4, null,
		v_sin,
		v_gender, null, v_bd, v_bp, v_national,
		null, null,
		father_id, mother_id, null,
		null, null, 
		v_country, v_sp, v_city, v_local, concat(v_street, ' -- ', v_mousqe),
		null, v_fullname,
		v_ename1, v_ename2, v_ename3, v_ename4,
		v_bank, v_acc_no, v_acc_name, v_branch,
		null, v_study_type, v_authority, v_stage, null, null, v_grade,  v_year, v_school, v_edu_level, v_points,
		v_health, v_health_details, v_disease,
		v_praying, v_quran_center, v_quran_chapters, v_quran_surat, v_unpray_reason, v_quran_reason,
		null, null, null, null, null, null, null,
		v_property_type, v_rent, v_roof, null, v_indoor_cond, v_habitable, v_house_cond, v_rooms, v_area,
		null, null, null);
		
        INSERT INTO `char_cases`
		(`person_id`,
		`organization_id`,
		`category_id`,
		`status`,
		`rank`,
		`user_id`,
		`created_at`)
		VALUES
		(id,
		v_org_id,
		v_category,
		0,
		0,
		1,
		NOW());
        
		REPLACE INTO `char_guardians`
		(`guardian_id`,
		`individual_id`,
		`kinship_id`,
		`organization_id`)
		VALUES
		(guardian_id,
		id,
		v_g_kinship,
		v_org_id);

		REPLACE INTO `char_persons_kinship`
		(`l_person_id`,
		`r_person_id`,
		`kinship_id`)
		VALUES
		(guardian_id,
		id,
		v_g_kinship);
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_work_jobs`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `job` FROM
    (SELECT DISTINCT `job` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_job` AS `job` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `m_job` AS `job` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `f_job` AS `job` FROM `test`.`data_father`) AS work_jobs
    WHERE `job` IS NOT NULL AND `job` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_jobs`;
		TRUNCATE TABLE `char_work_jobs_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_jobs_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `job` = v_id WHERE normalize_text_ar(`job`) = v_name;
            UPDATE `test`.`data_mother` SET `m_job` = v_id WHERE normalize_text_ar(`m_job`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_job` = v_id WHERE normalize_text_ar(`g_job`) = v_name;
            UPDATE `test`.`data_father` SET `f_job` = v_id WHERE normalize_text_ar(`f_job`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_work_status`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `work_status` FROM `test`.`data` WHERE `work_status` IS NOT NULL;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_status`;
		TRUNCATE TABLE `char_work_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_status_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data` SET `work_status` = v_id WHERE normalize_text_ar(`work_status`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `normalize_text_ar`(`p_text` TEXT) RETURNS text CHARSET utf8
BEGIN
	
	DECLARE v_comma VARCHAR(255) DEFAULT '،';
	DECLARE v_semicolon VARCHAR(255) DEFAULT '؛';
	DECLARE v_question VARCHAR(255) DEFAULT '؟';
	DECLARE v_hamza VARCHAR(255) DEFAULT 'ء';
	DECLARE v_alef_madda VARCHAR(255) DEFAULT 'آ';
	DECLARE v_alef_hamza_above VARCHAR(255) DEFAULT 'أ';
	DECLARE v_waw_hamza VARCHAR(255) DEFAULT 'ؤ';
	DECLARE v_alef_hamza_below VARCHAR(255) DEFAULT 'إ';
	DECLARE v_yeh_hamza VARCHAR(255) DEFAULT 'ئ';
	DECLARE v_alef VARCHAR(255) DEFAULT 'ا';
	DECLARE v_beh VARCHAR(255) DEFAULT 'ب';
	DECLARE v_teh_marbuta VARCHAR(255) DEFAULT 'ة';
	DECLARE v_teh VARCHAR(255) DEFAULT 'ت';
	DECLARE v_theh VARCHAR(255) DEFAULT 'ث';
	DECLARE v_jeem VARCHAR(255) DEFAULT 'ج';
	DECLARE v_hah VARCHAR(255) DEFAULT 'ح';
	DECLARE v_khah VARCHAR(255) DEFAULT 'خ';
	DECLARE v_dal VARCHAR(255) DEFAULT 'د';
	DECLARE v_thal VARCHAR(255) DEFAULT 'ذ';
	DECLARE v_reh VARCHAR(255) DEFAULT 'ر';
	DECLARE v_zain VARCHAR(255) DEFAULT 'ز';
	DECLARE v_seen VARCHAR(255) DEFAULT 'س';
	DECLARE v_sheen VARCHAR(255) DEFAULT 'ش';
	DECLARE v_sad VARCHAR(255) DEFAULT 'ص';
	DECLARE v_dad VARCHAR(255) DEFAULT 'ض';
	DECLARE v_tah VARCHAR(255) DEFAULT 'ط';
	DECLARE v_zah VARCHAR(255) DEFAULT 'ظ';
	DECLARE v_ain VARCHAR(255) DEFAULT 'ع';
	DECLARE v_ghain VARCHAR(255) DEFAULT 'غ';
	DECLARE v_tatweel VARCHAR(255) DEFAULT 'ـ';
	DECLARE v_feh VARCHAR(255) DEFAULT 'ف';
	DECLARE v_qaf VARCHAR(255) DEFAULT 'ق';
	DECLARE v_kaf VARCHAR(255) DEFAULT 'ك';
	DECLARE v_lam VARCHAR(255) DEFAULT 'ل';
	DECLARE v_meem VARCHAR(255) DEFAULT 'م';
	DECLARE v_noon VARCHAR(255) DEFAULT 'ن';
	DECLARE v_heh VARCHAR(255) DEFAULT 'ه';
	DECLARE v_waw VARCHAR(255) DEFAULT 'و';
	DECLARE v_alef_maksura VARCHAR(255) DEFAULT 'ى';
	DECLARE v_yeh VARCHAR(255) DEFAULT 'ي';
	DECLARE v_madda_above VARCHAR(255) DEFAULT 'ٓ';
	DECLARE v_hamza_above VARCHAR(255) DEFAULT 'ٔ';
	DECLARE v_hamza_below VARCHAR(255) DEFAULT 'ٕ';
	DECLARE v_zero VARCHAR(255) DEFAULT '٠';
	DECLARE v_one VARCHAR(255) DEFAULT '١';
	DECLARE v_two VARCHAR(255) DEFAULT '٢';
	DECLARE v_three VARCHAR(255) DEFAULT '٣';
	DECLARE v_four VARCHAR(255) DEFAULT '٤';
	DECLARE v_five VARCHAR(255) DEFAULT '٥';
	DECLARE v_six VARCHAR(255) DEFAULT '٦';
	DECLARE v_seven VARCHAR(255) DEFAULT '٧';
	DECLARE v_eight VARCHAR(255) DEFAULT '٨';
	DECLARE v_nine VARCHAR(255) DEFAULT '٩';
	DECLARE v_percent VARCHAR(255) DEFAULT '٪';
	DECLARE v_decimal VARCHAR(255) DEFAULT '٫';
	DECLARE v_thousands VARCHAR(255) DEFAULT '٬';
	DECLARE v_star VARCHAR(255) DEFAULT '٭';
	DECLARE v_mini_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_alef_wasla VARCHAR(255) DEFAULT 'ٱ';
	DECLARE v_full_stop VARCHAR(255) DEFAULT '۔';
	DECLARE v_byte_order_mark VARCHAR(255) DEFAULT '﻿';
	DECLARE v_fathatan VARCHAR(255) DEFAULT 'ً';
	DECLARE v_dammatan VARCHAR(255) DEFAULT 'ٌ';
	DECLARE v_kasratan VARCHAR(255) DEFAULT 'ٍ';
	DECLARE v_fatha VARCHAR(255) DEFAULT 'َ';
	DECLARE v_damma VARCHAR(255) DEFAULT 'ُ';
	DECLARE v_kasra VARCHAR(255) DEFAULT 'ِ';
	DECLARE v_shadda VARCHAR(255) DEFAULT 'ّ';
	DECLARE v_sukun VARCHAR(255) DEFAULT 'ْ';
	DECLARE v_small_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_small_waw VARCHAR(255) DEFAULT 'ۥ';
	DECLARE v_small_yeh VARCHAR(255) DEFAULT 'ۦ';
	DECLARE v_lam_alef VARCHAR(255) DEFAULT 'ﻻ';
	DECLARE v_lam_alef_hamza_above VARCHAR(255) DEFAULT 'ﻷ';
	DECLARE v_lam_alef_hamza_below VARCHAR(255) DEFAULT 'ﻹ';
	DECLARE v_lam_alef_madda_above VARCHAR(255) DEFAULT 'ﻵ';
	DECLARE v_simple_lam_alef VARCHAR(255) DEFAULT 'لَا';
	DECLARE v_simple_lam_alef_hamza_above VARCHAR(255) DEFAULT 'لأ';
	DECLARE v_simple_lam_alef_hamza_below VARCHAR(255) DEFAULT 'لإ';
	DECLARE v_simple_lam_alef_madda_above VARCHAR(255) DEFAULT 'لءَا';
    
    -- Stripe Tatweel
    SET p_text = REPLACE(p_text, v_tatweel, '');
    
    -- Strip Tashkeel
    SET p_text = REPLACE(p_text, v_fathatan, '');
    SET p_text = REPLACE(p_text, v_dammatan, '');
    SET p_text = REPLACE(p_text, v_kasratan, '');
    SET p_text = REPLACE(p_text, v_fatha, '');
    SET p_text = REPLACE(p_text, v_damma, '');
    SET p_text = REPLACE(p_text, v_kasra, '');
    SET p_text = REPLACE(p_text, v_shadda, '');
    SET p_text = REPLACE(p_text, v_sukun, '');
    
    -- Normalize Hamza
    SET p_text = REPLACE(p_text, v_waw_hamza, v_waw);
    SET p_text = REPLACE(p_text, v_yeh_hamza, v_yeh);
    SET p_text = REPLACE(p_text, v_alef_madda, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_below, v_alef);
    
    -- Normalize Lam Alef
    SET p_text = REPLACE(p_text, v_lam_alef, v_simple_lam_alef);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_above, v_simple_lam_alef_hamza_above);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_below, v_simple_lam_alef_hamza_below);
    SET p_text = REPLACE(p_text, v_lam_alef_madda_above, v_simple_lam_alef_madda_above);
    
    -- Normalize Teh Marbuta
    SET p_text = REPLACE(p_text, v_teh_marbuta, v_heh);
    
    WHILE instr(p_text, '  ') > 0 do
		SET p_text = replace(p_text, '  ', ' ');
	END WHILE;
        
RETURN trim(p_text);
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_case_duplicates`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id1, v_id2, v_org_id1, v_org_id2 INT;
    DECLARE v_sin VARCHAR(45);
	
    DECLARE cur CURSOR FOR SELECT c1.id, c2.id from `test`.`data_case` c1 inner join `test`.`data_case` c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname = c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur2 CURSOR FOR SELECT c1.id, c1.org_id, c2.id, c2.org_id, c1.sin
    from `test`.`data_case` c1 inner join `test`.`data_case` c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname = c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id < c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur3 CURSOR FOR SELECT c1.ID1, c2.ID1
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id < c2.id
    AND c1.sin <> 0 AND c1.sin is not Null
    AND clear_text(c1.name1) = clear_text(c2.name1)
    ORDER BY c1.id;
    
    DECLARE cur4 CURSOR FOR SELECT c1.ID1, c1.org_id, c2.ID1, c2.org_id, c1.sin
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id > c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null
    AND clear_text(c1.name1) = clear_text(c2.name1);
    
    DECLARE cur5 CURSOR FOR SELECT c1.ID1, c2.ID1
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null
    ORDER BY c1.id;
    
    DECLARE cur6 CURSOR FOR SELECT c1.ID1, c1.org_id, c2.ID1, c2.org_id, c1.sin
    from `test`.`data` c1 inner join `test`.`data` c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id > c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null;

    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    /*OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        DELETE FROM `test`.`data_case` WHERE id = v_id2;
	END LOOP;
    CLOSE cur;*/
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        DELETE FROM `test`.`data_case` WHERE id = v_id2;
        INSERT INTO `test`.`data_case_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_case_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur2;
    
    /*SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop3;

		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
	END LOOP;
    CLOSE cur3;*/
    
    /*SET done = false;
    OPEN cur4;
    read_loop4: LOOP
		FETCH cur4 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop4;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur4;*/
    
    /*SET done = false;
    OPEN cur5;
    read_loop5: LOOP
		FETCH cur5 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop5;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
	END LOOP;
    CLOSE cur5;*/
    
    /*SET done = false;
    OPEN cur6;
    read_loop6: LOOP
		FETCH cur6 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop6;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur6;*/
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `person_save`(INOUT `person_id` INT, `p_id_card_number` VARCHAR(255),`p_first_name` VARCHAR(255),
 `p_second_name` VARCHAR(255),`p_third_name` VARCHAR(255), `p_last_name` VARCHAR(255) , `p_prev_family_name` VARCHAR(255) ,
`p_gender` INT, `p_marital_status_id` INT, `p_birthday` DATE, `p_death_date` DATE, `p_family_count` INT, `p_spouses` INT)
BEGIN
    START TRANSACTION;
    
    SET p_id_card_number = SUBSTRING(p_id_card_number, 1, 9);
    
    IF person_id IS NULL THEN
		
        IF p_id_card_number IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons` WHERE `id_card_number` = p_id_card_number
            LIMIT 1;
		END IF;
        
	END IF;
    
    IF person_id IS NULL THEN
		INSERT INTO `char_persons`
		( `id_card_number`,`first_name`,`second_name`,`third_name`, `last_name` , `prev_family_name` ,`gender` , `marital_status_id` ,
        `birthday` ,`death_date`  , `family_cnt` ,`spouses`,`created_at`)
		VALUES
		(p_id_card_number,p_first_name,p_second_name,p_third_name, p_last_name , p_prev_family_name ,
p_gender , p_marital_status_id , p_birthday ,p_death_date , p_family_count, p_spouses , NOW());
        
        SET person_id = LAST_INSERT_ID();
	END IF;
    
   
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_imported`(IN `p_offset` INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id INT;   
	DECLARE v_id INT;
    DECLARE id_card_number VARCHAR(255);
    DECLARE first_name VARCHAR(255);
    DECLARE second_name VARCHAR(255);
    DECLARE third_name VARCHAR(255);
    DECLARE last_name VARCHAR(255);
    DECLARE full_name VARCHAR(255);
    DECLARE gender INT;
    DECLARE marital_status_id INT;
  
    DECLARE cur CURSOR FOR
    SELECT  `all_aids_data`.`id`,
    `all_aids_data`.`id_card_number`,
    `all_aids_data`.`first_name`,
    `all_aids_data`.`second_name`,
    `all_aids_data`.`third_name`,
    `all_aids_data`.`last_name`,
    `all_aids_data`.`full_name`,   
    `all_aids_data`.`gender`,
    `all_aids_data`.`marital_status_id`
	FROM `takaful_aft_add_db`.`all_aids_data`
	WHERE `all_aids_data`.`id` <= 5;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id,id_card_number,first_name,second_name,third_name,last_name,full_name,
								 gender,marital_status_id;
		
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET id = null;
        CALL `update_imported_save`(id,id_card_number,first_name,second_name,third_name,last_name,full_name,
								 gender,marital_status_id);
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `relays_cases_save`(INOUT `person_id` INT, `p_id_card_number` VARCHAR(255),`p_first_name` VARCHAR(255),
 `p_second_name` VARCHAR(255),`p_third_name` VARCHAR(255), `p_last_name` VARCHAR(255),`p_full_name` VARCHAR(255) , `p_prev_family_name` VARCHAR(255) ,
`p_gender` INT, `p_marital_status_id` INT, `p_birthday` DATE, `p_death_date` DATE, `p_male_live` INT, `p_female_live` INT, `p_family_cnt` INT,`p_adscountry_id` INT,
`p_adsdistrict_id`INT,`p_adsregion_id` INT, `p_adsneighborhood_id` INT, `p_adssquare_id`INT, `p_adsmosques_id`INT, `p_street_address` VARCHAR(255), `primary_mobile` VARCHAR(255), `phone` VARCHAR(255))
BEGIN
    START TRANSACTION;
    
    SET p_id_card_number = SUBSTRING(p_id_card_number, 1, 9);
    
    IF person_id IS NULL THEN
		
        IF p_id_card_number IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons` WHERE `id_card_number` = p_id_card_number
            LIMIT 1;
		END IF;
        
	END IF;
    
    IF person_id IS NULL THEN
		INSERT INTO `char_persons`
		( `id_card_number`,`first_name`,`second_name`,`third_name`, `last_name`,`fullname` , `prev_family_name` ,`gender` , `marital_status_id` ,
        `birthday` ,`death_date` , `male_live` , `female_live` , `family_cnt` ,`adscountry_id` ,
`adsdistrict_id`,`adsregion_id` , `adsneighborhood_id` , `adssquare_id`, `adsmosques_id`, `street_address` ,`created_at`)
		VALUES
		(p_id_card_number,p_first_name,p_second_name,p_third_name, p_last_name,p_full_name , p_prev_family_name ,
p_gender , p_marital_status_id , p_birthday ,p_death_date , p_male_live , p_female_live , p_family_cnt ,p_adscountry_id ,
p_adsdistrict_id,p_adsregion_id , p_adsneighborhood_id , p_adssquare_id, p_adsmosques_id, p_street_address , NOW());
        
        SET person_id = LAST_INSERT_ID();
	END IF;
    
     IF phone IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'phone', phone);
	END IF;
    
    
	IF primary_mobile IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'primary_mobile', primary_mobile);
	END IF;

      REPLACE  INTO `char_relays`
		(`id`,`person_id`,`organization_id`,`category_id`,`user_id`,`created_at`)
		VALUES
		(Null,person_id,1,5,1,now());
   
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `remove_duplicates`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id1, v_id2 INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT f1.f_id, f2.f_id from test.data_father f1 inner join test.data_father f2
	ON f1.f_sin = f2.f_sin AND f1.fullname = f2.fullname AND f1.f_id <> f2.f_id
	WHERE f1.f_id > f2.f_id;
    
	DECLARE cur2 CURSOR FOR SELECT m1.m_id, m2.m_id from test.data_mother m1 inner join test.data_mother m2
	ON  m1.m_sin = m2.m_sin AND m1.fullname = m2.fullname AND m1.m_id <> m2.m_id
	WHERE m1.m_id > m2.m_id;
    
    DECLARE cur3 CURSOR FOR SELECT g1.g_id, g2.g_id from test.data_guardian g1 inner join test.data_guardian g2
	ON g1.g_sin = g2.g_sin AND g1.fullname = g2.fullname AND g1.g_id <> g2.g_id
	WHERE g1.g_id > g2.g_id;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        DELETE FROM test.data_father WHERE f_id = v_id2;
        UPDATE test.data_case SET f_id = v_id1 WHERE f_id = v_id2;
	END LOOP;
    CLOSE cur;
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        DELETE FROM test.data_mother WHERE m_id = v_id2;
        UPDATE test.data_case SET m_id = v_id1 WHERE m_id = v_id2;
	END LOOP;
    CLOSE cur2;
    
    SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop3;
		END IF;
        
        DELETE FROM test.data_guardian WHERE g_id = v_id2;
        UPDATE test.data_case SET g_id = v_id1 WHERE g_id = v_id2;
	END LOOP;
    CLOSE cur3;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_imported_save`(INOUT `person_id` INT, `p_id_card_number` VARCHAR(255),`p_first_name` VARCHAR(255),
 `p_second_name` VARCHAR(255),`p_third_name` VARCHAR(255), `p_last_name` VARCHAR(255),`p_full_name` VARCHAR(255) ,
`p_gender` INT, `p_marital_status_id` INT)
BEGIN
    START TRANSACTION;
    
    SET p_id_card_number = SUBSTRING(p_id_card_number, 1, 9);
    IF person_id IS NULL THEN
		IF p_id_card_number IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons` WHERE `id_card_number` = p_id_card_number
            LIMIT 1;
             IF person_id IS Not NULL THEN
    				Update `char_persons` set 
					`first_name` = p_first_name ,
					`second_name` = p_second_name ,
					`third_name` = p_third_name ,
					`last_name` = p_last_name ,
					`gender` = p_gender ,
					`marital_status_id` = p_marital_status_id 
					Where id = person_id;
				END IF;
		END IF;
	END IF;
   
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `replace_person`(IN `p_offset` INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id INT;   
	DECLARE v_id INT;
    DECLARE id_card_number VARCHAR(255);
    DECLARE first_name VARCHAR(255);
    DECLARE second_name VARCHAR(255);
    DECLARE third_name VARCHAR(255);
    DECLARE last_name VARCHAR(255);
    DECLARE full_name VARCHAR(255);
    DECLARE prev_family_name VARCHAR(255);
    DECLARE gender INT;
    DECLARE marital_status_id INT;
    DECLARE birthday DATE;
    DECLARE death_date DATE;
    DECLARE family_count INT;
    DECLARE spouses INT;


    DECLARE cur CURSOR FOR
    SELECT  `char_persons_update`.`id`,
    `char_persons_update`.`id_card_number`,
    `char_persons_update`.`first_name`,
    `char_persons_update`.`second_name`,
    `char_persons_update`.`third_name`,
    `char_persons_update`.`last_name`,
    `char_persons_update`.`prev_family_name`,
    `char_persons_update`.`gender`,
    `char_persons_update`.`marital_status_id`,
    `char_persons_update`.`birthday`,
    `char_persons_update`.`death_date`,
    `char_persons_update`.`family_count`,
    `char_persons_update`.`spouses`
	FROM `takaful_aft_add_db`.`char_persons_update`
	WHERE `char_persons_update`.`id` >= p_offset;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id,id_card_number,first_name,second_name,third_name,last_name,prev_family_name,
								 gender,  marital_status_id,  birthday,death_date, family_count, spouses;
		
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET id = null;
        CALL `person_save`(id,id_card_number,first_name,second_name,third_name,last_name,prev_family_name,
								 gender,  marital_status_id,  birthday,death_date, family_count, spouses);
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `update_persons`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id_card_number INT;
    DECLARE v_first_name VARCHAR(255);
    DECLARE v_second_name VARCHAR(255);
    DECLARE v_third_name VARCHAR(255);
    DECLARE v_last_name VARCHAR(255);
    DECLARE v_prev_family_name VARCHAR(255);
    DECLARE v_gender INT;
    DECLARE v_marital_status_id INT;
    DECLARE v_birth_date DATE;

    DECLARE cur CURSOR FOR 
		    SELECT `id_card_number`,`first_name`,`second_name`,`third_name`,`last_name`,`prev_family_name`,`gender`,`marital_status_id`,`birth_date`
		    FROM `takaful`.`person_basic_updates` ;  
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

    START TRANSACTION;
    SET done = false;
    OPEN cur;
    
    read_loop5: LOOP
		FETCH cur INTO v_id_card_number,v_first_name,v_second_name,v_third_name,v_last_name,v_prev_family_name,v_gender,v_marital_status_id,v_birth_date;
        
        IF done THEN
			LEAVE read_loop5;
		END IF;
        
        UPDATE takaful.char_persons SET first_name        = v_first_name ,
                                        second_name       = v_second_name , 
                                        third_name        = v_third_name ,
                                        second_name       = v_second_name , 
                                        last_name         = v_last_name ,
                                        prev_family_name  = v_prev_family_name
        WHERE id_card_number = v_id_card_number;
        
	END LOOP;
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `import_work_reasons`(IN `p_truncate` BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `unwork_reason` FROM `test`.`data`
    WHERE `unwork_reason` IS NOT NULL AND `unwork_reason` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_reasons`;
		TRUNCATE TABLE `char_work_reasons_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_reasons_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data` SET `unwork_reason` = v_id WHERE normalize_text_ar(`unwork_reason`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;
