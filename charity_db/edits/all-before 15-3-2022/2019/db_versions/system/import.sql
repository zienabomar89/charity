
ALTER TABLE test.data ADD COLUMN fullname VARCHAR(255);
ALTER TABLE test.data_case ADD COLUMN fullname VARCHAR(255);
ALTER TABLE test.data_father ADD COLUMN fullname VARCHAR(255);
ALTER TABLE test.data_guardian ADD COLUMN fullname VARCHAR(255);
ALTER TABLE test.data_mother ADD COLUMN fullname VARCHAR(255);

ALTER TABLE `test`.`data` 
ADD INDEX `data_idx_sin` (`sin` ASC),
ADD INDEX `data_idx_fullname` (`fullname` ASC);

ALTER TABLE `test`.`data_case` 
ADD INDEX `data_case_idx_sin` (`sin` ASC),
ADD INDEX `data_case_idx_fullname` (`fullname` ASC);

ALTER TABLE `test`.`data_father` 
ADD INDEX `data_father_idx_sin` (`f_sin` ASC),
ADD INDEX `data_father_idx_fullname` (`fullname` ASC);

ALTER TABLE `test`.`data_guardian` 
ADD INDEX `data_guardian_idx_sin` (`g_sin` ASC),
ADD INDEX `data_guardian_idx_fullname` (`fullname` ASC);

ALTER TABLE `test`.`data_mother` 
ADD INDEX `data_mother_idx_sin` (`m_sin` ASC),
ADD INDEX `data_mother_idx_fullname` (`fullname` ASC);

UPDATE test.data SET fullname = concat(clear_text(ifnull(name1, '-')), clear_text(ifnull(name2, '-')), clear_text(ifnull(name3, '-')), clear_text(ifnull(name4, '-')));
UPDATE test.data_case SET fullname = concat(clear_text(ifnull(name1, '-')), clear_text(ifnull(name2, '-')), clear_text(ifnull(name3, '-')), clear_text(ifnull(name4, '-')));
UPDATE test.data_father SET fullname = concat(clear_text(ifnull(f_name1, '-')), clear_text(ifnull(f_name2, '-')), clear_text(ifnull(f_name3, '-')), clear_text(ifnull(f_name4, '-')));
UPDATE test.data_guardian SET fullname = concat(clear_text(ifnull(g_name1, '-')), clear_text(ifnull(g_name2, '-')), clear_text(ifnull(g_name3, '-')), clear_text(ifnull(g_name4, '-')));
UPDATE test.data_mother SET fullname = concat(clear_text(ifnull(m_name1, '-')), clear_text(ifnull(m_name2, '-')), clear_text(ifnull(m_name3, '-')), clear_text(ifnull(m_name4, '-')));


CALL `import_banks_branches`(true);
CALL `import_death_causes`(true);
CALL `import_diseases`(true);
CALL `import_edu_authorities`(true);
CALL `import_edu_degrees`(true);
CALL `import_edu_stages`(true);
CALL `import_kinship`(true);
CALL `import_locations`(true);
CALL `import_marital_status`(true);
CALL `import_property_types`(true);
CALL `import_roof_materials`(true);
CALL `import_work_jobs`(true);
CALL `import_work_reasons`(true);
CALL `import_work_status`(true);
CALL `import_building_status`(true);
CALL `import_habitable_status`(true);
CALL `import_furniture_status`(true);
CALL `import_house_status`(true);

CALL `takaful`.`import_aid_cases`(0);
CALL `takaful`.`import_sponsorship_cases`(0);

