
--
-- Triggers `char_organizations`
--
DROP TRIGGER IF EXISTS `char_organizations_closure_delete`;
DELIMITER //
CREATE TRIGGER `char_organizations_closure_delete` AFTER DELETE ON `char_organizations`
 FOR EACH ROW BEGIN
	DELETE FROM `char_organizations_closure`
		WHERE `descendant_id` IN (
		SELECT d FROM (
			SELECT b.`descendant_id` AS d FROM `char_organizations_closure` b
				WHERE b.`ancestor_id` = OLD.id
		) AS dct
	);
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `char_organizations_closure_insert`;
DELIMITER //
CREATE TRIGGER `char_organizations_closure_insert` AFTER INSERT ON `char_organizations`
 FOR EACH ROW BEGIN
	INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
	SELECT `c`.`ancestor_id`, NEW.id, `c`.`depth` + 1
	FROM `char_organizations_closure` AS `c`
	WHERE `c`.`descendant_id` = NEW.parent_id
	UNION ALL
	SELECT NEW.id, NEW.id, 0;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `char_organizations_closure_update`;
DELIMITER //
CREATE TRIGGER `char_organizations_closure_update` AFTER UPDATE ON `char_organizations`
 FOR EACH ROW BEGIN
	IF NEW.parent_id <> OLD.parent_id THEN
		
		DELETE FROM `char_organizations_closure`
		WHERE `descendant_id` IN (
		  SELECT d FROM (SELECT `descendant_id` AS d FROM `char_organizations_closure` WHERE `ancestor_id` = NEW.id) AS dct
		)
		AND `ancestor_id` IN (
			SELECT a FROM (
				SELECT `ancestor_id` AS a FROM `char_organizations_closure`
				WHERE `descendant_id` = NEW.id
				AND `ancestor_id` <> NEW.id
			) AS ct
		);

		IF NEW.parent_id > 0 OR NEW.parent_id IS NOT NULL THEN
			INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
			SELECT supertbl.`ancestor_id`, subtbl.`descendant_id`, supertbl.`depth` + subtbl.`depth` + 1
			FROM `char_organizations_closure` as supertbl
			CROSS JOIN `char_organizations_closure` as subtbl
			WHERE supertbl.`descendant_id` = NEW.parent_id
			AND subtbl.`ancestor_id` = NEW.id;
		END IF;
	END IF;
END
//
DELIMITER ;


--
-- Triggers `char_sponsorships`
--
DROP TRIGGER IF EXISTS `sponsorships_number_insert`;
DELIMITER //
CREATE TRIGGER `sponsorships_number_insert` BEFORE INSERT ON `char_sponsorships`
 FOR EACH ROW BEGIN
declare v_count int; declare v_year int;
set v_year = year(now());
set v_count = (SELECT ifnull(MAX(`sponsorship_number`), 0) + 1 FROM `char_sponsorships` WHERE substring(`sponsorship_number`, 1, 4) = v_year);
if v_count = 1 then
set NEW.sponsorship_number = concat(v_year, lpad(v_count, 4, '0'));
else
set NEW.sponsorship_number = v_count;
end if;END
//
DELIMITER ;
