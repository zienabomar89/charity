ALTER TABLE `char_reports` ADD `target` TEXT NULL AFTER `folder`;
ALTER TABLE `char_templates` CHANGE `filename` `filename` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;
ALTER TABLE `char_templates` ADD `other_filename`  VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER filename;

/********************  SPONSORSHIP CASES REPORTS ********************/
CREATE TABLE `char_reports` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sponsor_id` int(10) UNSIGNED NOT NULL COMMENT 'الجهة الكافلة',
  `category_id` int(10) UNSIGNED NOT NULL COMMENT 'نوع الكفالة',
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category` tinyint(3) NOT NULL COMMENT 'تصنيف التقرير : شهري - ربع سنوي - نصف سنوي - سنوي',
  `report_type` tinyint(3) NOT NULL COMMENT 'نوع التقرير : مضمن - تفصيلي',
  `title` varchar(255)NOT NULL  COMMENT 'اسم التقرير',
  `date` date NOT NULL  COMMENT 'تاريخ انشاء التقرير',
  `date_from` date NOT NULL COMMENT 'تاريخ بداية فترة التقرير',
  `date_to` date NOT NULL COMMENT 'تاريخ نهاية فترة التقرير',
  `status` tinyint(3) UNSIGNED NULL DEFAULT '1',
  `folder` text DEFAULT NULL,
  `notes` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `fk_reports_sponsor_idx` (`sponsor_id`),
   KEY `fk_reports_category_idx` (`category_id`),
   KEY `fk_reports_organization_idx` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول التقارير المؤرشفة للجهات الكافلة';

ALTER TABLE `char_reports`
  ADD CONSTRAINT `fk_reports_sponsor_fk` FOREIGN KEY (`sponsor_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_reports_category_fk` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_reports_organization_fk` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`);

/********************  SPONSORSHIP CASES REPORTS ********************/
CREATE TABLE `char_sponsorship_cases_documents` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sponsorship_cases_id` int(10) UNSIGNED NOT NULL,
  `doc_category_id` tinyint(3) NOT NULL COMMENT 'تصنيف المرفق : شهري - ربع سنوي - نصف سنوي - سنوي',
  `document_type_id` int(10) UNSIGNED NOT NULL  COMMENT 'نوع المرفق : رسالة شكر – صورة طولية – صورة شخصية – شهادة المدرسة – تقرير طبي – حجة عزوبية – ألبوم صور',
  `document_id` int(10) UNSIGNED NOT NULL,
  `date_from` date NOT NULL COMMENT 'تاريخ بداية فترة المرفق',
  `date_to` date NOT NULL COMMENT 'تاريخ نهاية فترة المرفق',
  `status` tinyint(3) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
   PRIMARY KEY (`id`),
   KEY `fk_sponsorship_cases_documents_scidx` (`sponsorship_cases_id`),
   KEY `fk_sponsorship_cases_documents_didx` (`document_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول مرفقات الحالات لتقارير الجهات الكافلة';


ALTER TABLE `char_sponsorship_cases_documents`
  ADD CONSTRAINT `fk_sponsorship_cases_documents_scid` FOREIGN KEY (`sponsorship_cases_id`) REFERENCES `char_sponsorship_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_sponsorship_cases_documents_did`  FOREIGN KEY (`document_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DELIMITER $$
DROP FUNCTION `char_get_sponsorship_cases_documents_count` ;
CREATE  FUNCTION `char_get_sponsorship_cases_documents_count`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS int(11)
BEGIN
	declare c int;
    set c =  (select count(`char_sponsorship_cases_documents`.`id`)
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );

RETURN c;
END$$
DELIMITER ;

DELIMITER $$
DROP FUNCTION `char_get_sponsorship_cases_documents_file_path` ;
CREATE  FUNCTION `char_get_sponsorship_cases_documents_file_path`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS text CHARSET utf8
BEGIN
	declare c text;
    set c =  (select GROUP_CONCAT(DISTINCT `doc_files`.`filepath` SEPARATOR ',' )
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );

RETURN c;
END$$
DELIMITER ;