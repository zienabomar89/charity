
CREATE TABLE `char_user_organizations` (
  `id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `char_user_organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `char_user_organizations$user_id_indx` (`user_id`),
  ADD KEY `char_user_organizations$organization_id_indx` (`organization_id`);


ALTER TABLE `char_user_organizations`
  ADD CONSTRAINT `char_user_organizations$user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `char_user_organizations$organization_id_fk` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
