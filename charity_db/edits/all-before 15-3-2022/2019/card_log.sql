CREATE TABLE `char_users_card_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `id_card_number` varchar(9) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `char_users_card_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_card_log_user_id$user_id_indx` (`user_id`);

ALTER TABLE `char_users_card_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

ALTER TABLE `char_users_card_log`
  ADD CONSTRAINT `fk_user_card_log_user_id` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
