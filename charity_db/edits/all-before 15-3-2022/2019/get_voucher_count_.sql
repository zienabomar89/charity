DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_voucher_count_`(
`id` INT(10), `type` VARCHAR(45),
 `date_from` DATE, 
 `date_to` DATE, 
 `category_id` INT(10),
 `orgs` VARCHAR(45)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then

if type = 'sponsors' then

if date_from is null and date_to is null then
	set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
				FROM  `char_vouchers` AS `v`
				join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
				WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) ));

elseif date_from is not null and date_to is null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) and (`v`.`voucher_date`  <= date_to )));

elseif date_from is not null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

else

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (orgs)) ));

elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (orgs)) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (orgs)) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (orgs))  and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

end if;

else


if type = 'sponsors' then

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs))and (`v`.`category_id` = category_id)));

elseif date_from is not null and date_to is null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id)  and (`v`.`organization_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));

elseif date_from is null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));

elseif date_from is not null and date_to is not null then

set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`sponsor_id` = id) and (`v`.`organization_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

else

if date_from is null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)  and (`v`.`sponsor_id` IN (orgs))  and (`v`.`category_id` = category_id) ));
elseif date_from is not null and date_to is null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
elseif date_from is null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
elseif date_from is not null and date_to is not null then
set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
FROM  `char_vouchers` AS `v`
join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
WHERE ((`v`.`organization_id` = id)   and (`v`.`sponsor_id` IN (orgs)) and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

end if;

end if;



end if;


RETURN c;
END$$
DELIMITER ;