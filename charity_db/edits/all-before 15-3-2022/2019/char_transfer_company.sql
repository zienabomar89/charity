
CREATE TABLE `char_transfer_company` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول شركات التحويل';


ALTER TABLE `char_transfer_company`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `char_transfer_company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (NULL, '7', 'setting.transferCompany.manage', 'إدارة شركات التحويل'),
 (NULL, '7', 'setting.transferCompany.create', 'إنشاء شركة تحويل جديد'),
 (NULL, '7', 'setting.transferCompany.update', 'تحرير بيانات شركة تحويل'),
 (NULL, '7', 'setting.transferCompany.delete', 'حذف شركة تحويل'),
 (NULL, '7', 'setting.transferCompany.view', 'عرض بيانات شركة تحويل');

ALTER TABLE `char_vouchers`
 ADD `transfer` TINYINT(1) NOT NULL DEFAULT '0' AFTER `status`,
 ADD `transfer_company_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `char_vouchers`
    ADD KEY `fk_transfer_company_id_idx` (`transfer_company_id`);

ALTER TABLE `char_vouchers`
 ADD CONSTRAINT `fk_transfer_company_id` FOREIGN KEY (`transfer_company_id`) REFERENCES `char_transfer_company` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;



DELIMITER $$
CREATE  FUNCTION `char_get_sponsorship_cases_documents_id`(`sponsorship_cases_id` INT, `document_type_id` TINYINT, `doc_category_id` TINYINT, `date_from` DATE, `date_to` DATE) RETURNS text
BEGIN
	declare c text;

   if document_type_id = 5 or document_type_id = 7 then
          set c =  (select GROUP_CONCAT(DISTINCT `char_sponsorship_cases_documents`.`id` SEPARATOR ',' )
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
    else
 set c =  (select `char_sponsorship_cases_documents`.`id`
              FROM `char_sponsorship_cases_documents`
              join `doc_files` on `doc_files`.`id` = `char_sponsorship_cases_documents`.`document_id`
              where `char_sponsorship_cases_documents`.`sponsorship_cases_id`=sponsorship_cases_id AND
                     `char_sponsorship_cases_documents`.`document_type_id`=document_type_id AND
                     `char_sponsorship_cases_documents`.`doc_category_id`=doc_category_id AND
                     `char_sponsorship_cases_documents`.`date_from`=date_from AND
                      `char_sponsorship_cases_documents`.`date_to`=date_to
            );
    end if;

RETURN c;
END$$
DELIMITER ;

ALTER TABLE `char_vouchers_persons` ADD `receipt_time` TIME NULL AFTER `receipt_date`;
ALTER TABLE `char_users` ADD `nick_name` TEXT NULL AFTER `email`;
ALTER TABLE `char_users` ADD`wataniya` varchar(45) DEFAULT NULL  AFTER `mobile`;

ALTER TABLE `char_users` ADD `position` TEXT NULL AFTER `nick_name`;
ALTER TABLE `char_users` ADD `document_id` INT(10) UNSIGNED NULL  AFTER `position`;
ALTER TABLE `char_users` ADD  KEY `fk_char_users_document_id_idx` (`document_id`)
-- ALTER TABLE `char_users`
--   ADD CONSTRAINT `fk_char_users_document_id`  FOREIGN KEY (`document_id`) REFERENCES `doc_files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
