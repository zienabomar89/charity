DELIMITER $$
CREATE  FUNCTION `get_org_payments_cases_count_`(
`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                  (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                  (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                            (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                            (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                    (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
 (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                              (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                       (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
(`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                  (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and
                                    (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                   (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
    (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and
                                   (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                               (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
        (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;


RETURN c;
END$$
DELIMITER ;



DELIMITER $$
CREATE  FUNCTION `get_org_payments_cases_amount_`(`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` VARCHAR(255), `orgs` VARCHAR(255)) RETURNS int(11)
BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id)and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                   (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id)and
                                                                                               (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
   (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                                 (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
 (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                             (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and
                                                                                            (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
      (`p`.`organization_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                          (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
        (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 )))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                             (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
     (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                                                                           (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
       (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) )
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and
                                    (`char_cases`.`category_id` IN (select `cat`.`id` FROM   `char_categories` AS `cat` where FIND_IN_SET(`cat`.`id`,category_id) != 0 ))and
         (`p`.`sponsor_id` IN (select `o`.`id` FROM   `char_organizations` AS `o` where FIND_IN_SET(`o`.`id`,orgs) != 0 ))
                                   and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;


RETURN c;
END$$
DELIMITER ;