INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '3', 'aid.personsVoucher.sentSms', 'ارسال رسائل قصيرة لمستفيدي كوبونات المساعدات');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4', 'sponsorship.payments.upload', 'رفع قوالب تصدير السندات / الشيكات');

ALTER TABLE `char_vouchers` ADD `exchange_rate` float NOT NULL DEFAULT '1' AFTER `currency_id` COMMENT 'سعر الصرف مقابل الشيكل' ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_count_voucher_persons`(`id` INT(1)) RETURNS int(11)
BEGIN
declare c INTEGER;
		set c = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons`
        from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));
RETURN c;
END$$
DELIMITER ;


DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `char_total_voucher_amount`(`id` INT(1)) RETURNS int(11)
BEGIN
declare c INTEGER;

		set c = (select (`char_vouchers`.`exchange_rate` * `char_vouchers`.`value` * char_count_voucher_persons(`char_vouchers`.`id`)) AS `amount`
                 from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$
DELIMITER ;