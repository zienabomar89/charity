ALTER TABLE `char_currencies` ADD `en_name` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER `name`;

ALTER TABLE `char_users` ADD `locale` VARCHAR(45) NOT NULL DEFAULT 'ar' AFTER `username`;

ALTER TABLE `char_vouchers` ADD `token` TEXT NULL AFTER `allow_day`,
 ADD `valid_token` INT NOT NULL DEFAULT '0' AFTER `token`;


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL , '10', 'reports.CitizenRequest.manage', 'إدارة طلبات المواطنين');

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (NULL, '7', 'setting.houseStatus.manage', 'إدارة حالة المنزل'),
 (NULL, '7', 'setting.houseStatus.create', 'إنشاء حالة منزل جديدة'),
  (NULL, '7', 'setting.houseStatus.update', 'تحرير حالة المنزل'),
  (NULL, '7', 'setting.houseStatus.delete', 'حذف حالة المنزل'),
  (NULL, '7', 'setting.houseStatus.view', 'عرض حالة المنزل');

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (NULL, '7', 'setting.buildingStatus.manage', 'إدارة وضع المنزل'),
 (NULL, '7', 'setting.buildingStatus.create', 'إنشاء وضح منزل جديد'),
  (NULL, '7', 'setting.buildingStatus.update', 'تحرير وضع المنزل'),
  (NULL, '7', 'setting.buildingStatus.delete', 'حذف وضع المنزل'),
  (NULL, '7', 'setting.buildingStatus.view', 'عرض وضع المنزل');

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (NULL, '7', 'setting.furnitureStatus.manage', 'إدارة حالة الأثاث'),
 (NULL, '7', 'setting.furnitureStatus.create', 'إنشاء حالة أثاث جديدة'),
  (NULL, '7', 'setting.furnitureStatus.update', 'تحرير حالة الأثاث'),
  (NULL, '7', 'setting.furnitureStatus.delete', 'حذف حالة الأثاث'),
  (NULL, '7', 'setting.furnitureStatus.view', 'عرض حالة الأثاث');

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
 (NULL, '7', 'setting.habitableStatus.manage', 'إدارة صلاحية السكن'),
 (NULL, '7', 'setting.habitableStatus.create', 'إنشاء صلاحية سكن جديدة'),
  (NULL, '7', 'setting.habitableStatus.update', 'تحرير صلاحية السكن'),
  (NULL, '7', 'setting.habitableStatus.delete', 'حذف صلاحية السكن'),
  (NULL, '7', 'setting.habitableStatus.view', 'عرض صلاحية السكن');
