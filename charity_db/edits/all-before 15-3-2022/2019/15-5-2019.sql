ALTER TABLE `char_persons`	ADD COLUMN `deserted` TINYINT(4) NULL DEFAULT NULL AFTER `gender`;

INSERT INTO `char_forms_elements` (`id`, `form_id`, `type`, `name`, `label`, `description`, `value`, `priority`, `weight`) VALUES
(129, 1, '1', 'family_cnt', 'أفراد العائلة', 'حقل رقمي', NULL, 4, 0),
(130, 1, '1', 'deserted', 'مهجورة', 'حقل رقمي', NULL, 4, 0);

INSERT INTO `char_categories_form_element_priority` (`category_id`, `element_id`, `priority`) VALUES
(5, 129, 3),
(5, 130, 3),
(6, 129, 3),
(6, 130, 3),
(7, 129, 3),
(7, 130, 3);
