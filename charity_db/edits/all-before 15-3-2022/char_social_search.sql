
CREATE TABLE `char_social_search` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL COMMENT 'اسم البحث',
  `date` date NOT NULL COMMENT 'تاريخ انشاء البحث',
  `date_from` date NOT NULL COMMENT 'تاريخ بداية فترة البحث',
  `date_to` date NOT NULL COMMENT 'تاريخ نهاية فترة البحث',
  `category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'التصنيف',
  `notes` text COMMENT 'ملاحظات',
  `user_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT '1' COMMENT 'حالة البحث',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول البحث الاجتماعي';

ALTER TABLE `char_social_search`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_char_social_search_organizationId_idx` (`organization_id`),
  ADD KEY `fk_char_social_search_category_idId_idx` (`category_id`),
  ADD KEY `fk_char_social_search_userId_idx` (`user_id`);

ALTER TABLE `char_social_search`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `char_social_search`
  ADD CONSTRAINT `fk_char_social_search_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_char_social_search_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_char_social_search_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


CREATE TABLE `char_social_search_templates` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `organization_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الجمعية',
  `category_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'التصنيف',
  `template` varchar(255) NOT NULL COMMENT 'القالب',
  `filename` varchar(255) DEFAULT '' COMMENT 'اسم الملف',
  `other_filename` varchar(255) DEFAULT '' COMMENT 'النسخة الانجليزي ',
  `en_filename` varchar(255) DEFAULT '' COMMENT 'النسخة الانجليزي ',
  `en_other_filename` varchar(255) DEFAULT '' COMMENT 'النسخة الانجليزي ',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل على السجل',
  `deleted_at` datetime DEFAULT NULL COMMENT 'تاريخ الحذف'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على قوالب الاستمارات والتقارير الخاصة بالجمعيات والجهات الكافلة ' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_templates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_social_search_templates_category_id_Idx` (`category_id`),
  ADD KEY `fk_social_search_templates_organization_id_Idx` (`organization_id`);

ALTER TABLE `char_social_search_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=7;

ALTER TABLE `char_social_search_templates`
  ADD CONSTRAINT `fk_social_search_templates_category_id` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_social_search_templates_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------


CREATE TABLE `char_social_search_cases` (

  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `search_id` int(10) UNSIGNED NOT NULL,
  `id_card_number` varchar(9) DEFAULT '' COMMENT 'رقم الهوية',
  `card_type` tinyint(4) DEFAULT '1' COMMENT 'نوع البطاقة:1- رقم الهوية و 2- بطاقة التعريف و  3- جواز السفر',
  `first_name` varchar(45) DEFAULT '' COMMENT 'الاسم الأول',
  `second_name` varchar(45) DEFAULT '' COMMENT 'اسم الأب',
  `third_name` varchar(45) DEFAULT '' COMMENT 'اسم الجد',
  `last_name` varchar(45) DEFAULT '' COMMENT 'اسم العائلة',
  `prev_family_name` varchar(255) DEFAULT '' COMMENT 'اسم العائلة السابق',

  `gender` tinyint(4) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 2- أنثى',
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الحالة الاجتماعية : 4 متوفي ، 10 أعزب / آنسة ، 20 متزوج / ة ، 21 متعدد الزوجات  ، 30 مطلق  / ة ، 40 أرمل / ة',
  `deserted` tinyint(4) DEFAULT '0' COMMENT 'مهجورة في حال انثى متزوجة :1-نعم ، 0-لا ',
  `birthday` date DEFAULT NULL COMMENT 'تاريخ الميلاد',
  `birth_place` int(10) UNSIGNED DEFAULT NULL COMMENT 'مكان الميلاد',
  `death_date` date DEFAULT NULL COMMENT 'تاريخ الوفاة',
  `death_cause_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'سبب الوفاة',
  `nationality` int(10) UNSIGNED DEFAULT NULL COMMENT 'الجنسية',
  `family_cnt` int(10) DEFAULT NULL COMMENT 'عدد أفراد العائلة',
  `spouses` tinyint(1) DEFAULT NULL COMMENT 'عدد الزوجات',
  `male_live` int(10) UNSIGNED DEFAULT NULL COMMENT 'عدد الابناء الغير متزوجين و على قيد الحياة',
  `female_live` int(10) UNSIGNED DEFAULT NULL COMMENT 'عدد البنات الغير متزوجات وعلى قيد الحياة',

  `phone` VARCHAR(255) NULL  COMMENT 'رقم التلفون الارضي',
  `primary_mobile` VARCHAR(255) NULL  COMMENT 'رقم الجوال الاساسي',
  `secondary_mobile` VARCHAR(255) NULL  COMMENT 'رقم الجوال الاحتياطي',
  `watanya` VARCHAR(255) NULL  COMMENT 'رقم الوطنية',

  `country_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الدولة',
  `district_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المحافظة',
  `region_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المنطقة',
  `neighborhood_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الحي',
  `square_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المربع',
  `mosque_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المسجد',
  `street_address` varchar(255) DEFAULT '' COMMENT 'تفاصيل العنوان  - الشارع ',
  `longitude` varchar(45) DEFAULT NULL COMMENT 'خط الطول',
  `Latitude` varchar(45) DEFAULT NULL COMMENT 'خط العرض',
  `status` tinyint(3) UNSIGNED NOT NULL COMMENT 'الحالة',
  `reason` text COMMENT 'السبب',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل',
  `deleted_at` datetime DEFAULT NULL COMMENT 'تاريخ الحذف'

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول بيانات البحث الاجتماعي للمواطنين ' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_social_search_cases_searchId_idx` (`search_id`),
  ADD KEY `fk_social_search_cases_marital_statusId_idx` (`marital_status_id`),
  ADD KEY `fk_social_search_cases_nationalityId_idx` (`nationality`),
  ADD KEY `fk_social_search_cases_birth_placeId_idx` (`birth_place`),
  ADD KEY `fk_social_search_cases_death_causeId_idx` (`death_cause_id`),
  ADD KEY `fk_social_search_cases_countryId_idx` (`country_id`),
  ADD KEY `fk_social_search_cases_districtId_idx` (`district_id`),
  ADD KEY `fk_social_search_cases_regionId_idx` (`region_id`),
  ADD KEY `fk_social_search_cases_neighborhoodId_idx` (`neighborhood_id`),
  ADD KEY `fk_social_search_cases_squareId_idx` (`square_id`),
  ADD KEY `fk_social_search_cases_mosqueId_idx` (`mosque_id`)
  ;

ALTER TABLE `char_social_search_cases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي';

ALTER TABLE `char_social_search_cases`
  ADD CONSTRAINT `fk_social_search_cases_search_id` FOREIGN KEY (`search_id`) REFERENCES `char_social_search` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_birth_place` FOREIGN KEY (`birth_place`) REFERENCES `char_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_death_cause_id` FOREIGN KEY (`death_cause_id`) REFERENCES `char_death_causes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_district_id` FOREIGN KEY (`district_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_marital_status_id` FOREIGN KEY (`marital_status_id`) REFERENCES `char_marital_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_nationality` FOREIGN KEY (`nationality`) REFERENCES `char_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_country_id` FOREIGN KEY (`country_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_region_id` FOREIGN KEY (`region_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_neighborhood_id` FOREIGN KEY (`neighborhood_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_square_id` FOREIGN KEY (`square_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_mosque_id` FOREIGN KEY (`mosque_id`) REFERENCES `char_aids_locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
  ;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------


CREATE TABLE `char_social_search_cases_details` (
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم رب الاسرة في استمارة البحث الاجتماعي',

  `refugee` tinyint(4) DEFAULT NULL COMMENT 'حالة المواطنة:1-مواطن ، 2-لاجيء',
  `unrwa_card_number` varchar(45) DEFAULT NULL COMMENT 'رقم كرت المؤن في ال كان لاجيء',

  `is_qualified` tinyint(1) DEFAULT '1' COMMENT 'هل مؤهل:1-مؤهل  ، 0-غير مؤهل ',
  `qualified_card_number` float DEFAULT NULL COMMENT 'رقم هوية المعييل في حال كان رب الاسرة غير مؤهل',

  `receivables` tinyint(3) NOT NULL DEFAULT '0'COMMENT 'هل لديه ذمم مالية:1-نعم ، 0-لا',
  `receivables_sk_amount` float DEFAULT NULL COMMENT 'قيمة الذمم المالية بالشيكل',

  `monthly_income` float DEFAULT NULL COMMENT 'الدخل الشهري الكلي',
  `actual_monthly_income` float DEFAULT NULL COMMENT 'الدخل الشهري الفعلي',

  `visitor` varchar(255) DEFAULT '' COMMENT 'اسم الباحث الاجتماعي',
  `visitor_card` int(10) NOT NULL DEFAULT '0' COMMENT 'رقم هوية الباحث الاجتماعي',
  `visited_at` date DEFAULT NULL COMMENT 'تاريخ زيارة الباحث',
  `visitor_evaluation` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'تقييم الباحث لحالة المنزل : 4- ممتاز ، 3- جيد جدا ، 2 جيد ، 1 سيء ، 0 سيء جدا ',
  `notes` text COMMENT 'توصيات الباحث الاجتماعي',
  `visitor_opinion` text COMMENT 'رأي الباحث الاجتماعي',
  `needs` text NOT NULL COMMENT 'الاحتياجات',
  `promised` tinyint(1) NOT NULL COMMENT 'هل وعدت بالبناء :  : 1- لا ، 2 نعم',
  `reconstructions_organization_name` varchar(255) NOT NULL COMMENT 'اسم الجمعية او المؤسسة التي وعدت العائلة'

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على بيانات اضافية  ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_details`ADD PRIMARY KEY (`search_cases_id`);

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_work` (
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم رب الاسرة في استمارة البحث الاجتماعي',
  `working` tinyint(1) UNSIGNED DEFAULT NULL COMMENT 'هل يعمل:1-نعم ، 2-لا ',
  `work_status_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'حالة العمل',
  `work_job_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المسمى الوظيفي',
  `work_wage_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'فئة الأجور',
  `work_location` varchar(255) DEFAULT '' COMMENT 'مكان العمل',
  `can_work` tinyint(1) UNSIGNED DEFAULT 1 COMMENT 'المقدرة على العمل:1-نعم ، 2-لا ',
  `work_reason_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'سبب عدم المقدرة على العمل',
  `has_other_work_resources` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'هل لديه مصدر دخل خارجي : 1-نعم ، 0-لا '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على بيانات العمل  ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_work`
  ADD PRIMARY KEY (`search_cases_id`),
  ADD KEY `fk_social_search_cases_work_statusId_idx` (`work_status_id`),
  ADD KEY `fk_social_search_cases_work_jobId_idx` (`work_job_id`),
  ADD KEY `fk_social_search_cases_work_wageId_idx` (`work_wage_id`),
  ADD KEY `fk_social_search_cases_work_reasonId_idx` (`work_reason_id`);

ALTER TABLE `char_social_search_cases_work`
  ADD CONSTRAINT `fk_social_search_cases_work_job_id` FOREIGN KEY (`work_job_id`) REFERENCES `char_work_jobs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_work_status_id` FOREIGN KEY (`work_status_id`) REFERENCES `char_work_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_work_wage_id` FOREIGN KEY (`work_wage_id`) REFERENCES `char_work_wages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_work_reason_id` FOREIGN KEY (`work_reason_id`) REFERENCES `char_work_reasons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_residence` (
  `search_cases_id`  int(10) UNSIGNED NOT NULL COMMENT 'رقم رب الاسرة في استمارة البحث الاجتماعي',
  `property_type_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'ملكية المنزل',
  `rent_value` float DEFAULT NULL COMMENT 'قيمة الايجار بالشيكل',
  `roof_material_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'سقف وجدران المنزل',
  `residence_condition` int(10) UNSIGNED DEFAULT NULL COMMENT 'حالة المنزل/ المسكن',
  `indoor_condition` int(10) UNSIGNED DEFAULT NULL COMMENT 'حالة الأثاث',
  `habitable` int(10) UNSIGNED DEFAULT NULL COMMENT 'حاجة المنزل للترميم',
  `house_condition` int(10) UNSIGNED DEFAULT NULL COMMENT 'تقييم وضع المنزل',
  `rooms` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'عدد الغرف',
  `area` float UNSIGNED DEFAULT '0' COMMENT 'المساحة',
  `need_repair` tinyint(3) DEFAULT '0' COMMENT 'هل بحاجة لترميم المنزل للموائمة : 1-نعم ، 2-لا ',
  `repair_notes` text COMMENT 'وصف وضع المنزل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على بيانات المسكن ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_residence`
  ADD PRIMARY KEY (`search_cases_id`),
  ADD KEY `fk_social_search_cases_residence_propertyId_idx` (`property_type_id`),
  ADD KEY `fk_social_search_cases_residence_roof_materialId_idx` (`roof_material_id`),
  ADD KEY `fk_social_search_cases_residence_residence_conditionId_idx` (`residence_condition`),
  ADD KEY `fk_social_search_cases_residence_indoor_conditionId_idx` (`indoor_condition`),
  ADD KEY `fk_social_search_cases_residence_habitableId_idx` (`habitable`),
  ADD KEY `fk_social_search_cases_residence_house_conditionId_idx` (`house_condition`);

ALTER TABLE `char_social_search_cases_residence`
  ADD CONSTRAINT `fk_social_search_cases_residence_property_id` FOREIGN KEY (`property_type_id`) REFERENCES `char_property_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_residence_roof_material_id` FOREIGN KEY (`roof_material_id`) REFERENCES `char_roof_materials` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_residence_residence_condition_id` FOREIGN KEY (`residence_condition`) REFERENCES `char_house_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_residence_indoor_condition_id` FOREIGN KEY (`indoor_condition`) REFERENCES `char_furniture_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_residence_habitable_id` FOREIGN KEY (`habitable`) REFERENCES `char_habitable_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_residence_house_condition_id` FOREIGN KEY (`house_condition`) REFERENCES `char_building_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ---------------------------------------------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_health` (
  `search_cases_id`  int(10) UNSIGNED NOT NULL COMMENT 'رقم رب الاسرة في استمارة البحث الاجتماعي',
  `condition` tinyint(4) DEFAULT NULL COMMENT 'الحالة: 1 جيدة، 2 يعاني من مرض مزمن، 3 من ذوي الاحتياجات الخاصة',
  `disease_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المرض او الاعاقة',
  `details` text COMMENT 'وصف نوع المرض أو الإعاقة',
  `has_health_insurance` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'هل لديه تأمين صحي:1-نعم ، 0-لا ',
  `health_insurance_type` text COMMENT 'نوع التأمين الصحي',
  `health_insurance_number` int(10) DEFAULT '0' COMMENT 'رقم التأمين الصحي',
  `has_device` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'هل يستخدم جهاز : 1-نعم ، 0-لا ',
  `used_device_name` text COMMENT 'اسم الجهاز المستخدم'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول الحالة الصحية ل رب الاسرة في استمارات البحث ' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_health`
  ADD PRIMARY KEY (`search_cases_id`),
  ADD KEY `fk_social_search_cases_health_diseaseId_idx` (`disease_id`);

ALTER TABLE `char_social_search_cases_health`
  ADD CONSTRAINT `fk_social_search_cases_health_disease_id` FOREIGN KEY (`disease_id`) REFERENCES `char_diseases` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_aids` (
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الشخص',
  `aid_source_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف الجهة المقدمة للمساعدة',
  `aid_type` tinyint(3) UNSIGNED NOT NULL COMMENT 'نوع المساعدة: 1=نقدية، 2=عينية',
  `aid_take` tinyint(1) NOT NULL COMMENT 'حصل على المساعدة : 0=لا، 1=نعم )',
  `aid_value` float DEFAULT NULL COMMENT 'قيمة المساعدة',
  `currency_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'العملة'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='المساعدات النقدية أو العينية الدائمة  ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_aids`
  ADD PRIMARY KEY (`search_cases_id`,`aid_source_id`,`aid_type`),
  ADD KEY `fk_social_search_cases_aids_search_cases_id_idx` (`search_cases_id`),
  ADD KEY `fk_social_search_cases_aids_currency_id_idx` (`currency_id`),
  ADD KEY `fk_social_search_cases_aids_source_id_idx` (`aid_source_id`);

ALTER TABLE `char_social_search_cases_aids`
  ADD CONSTRAINT `fk_social_search_cases_aids_search_cases_id` FOREIGN KEY (`search_cases_id`) REFERENCES `char_social_search_cases`  (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_social_search_cases_aids_currency_id` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_social_search_cases_aids_source_id` FOREIGN KEY (`aid_source_id`) REFERENCES `char_aid_sources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_properties` (
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الشخص',
  `property_id` int(10) UNSIGNED NOT NULL COMMENT 'معرف العقار او الممتلكات',
  `has_property` tinyint(1) NOT NULL COMMENT 'يمتلك ااو لا',
  `quantity` int(11) NOT NULL DEFAULT '1' COMMENT 'الكمية',
  `notes` varchar(255) DEFAULT NULL COMMENT 'ملاحظات'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول يحتوي على ممتلكات  ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_properties`
  ADD PRIMARY KEY (`search_cases_id`,`property_id`),
  ADD KEY `fk_social_search_cases_properties_search_cases_id_idx` (`search_cases_id`),
  ADD KEY `fk_social_search_cases_properties_property_id_idx` (`property_id`);

ALTER TABLE `char_social_search_cases_properties`
  ADD CONSTRAINT `fk_social_search_cases_properties_search_cases_id` FOREIGN KEY (`search_cases_id`) REFERENCES `char_social_search_cases`  (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_social_search_cases_properties_property_id` FOREIGN KEY (`property_id`) REFERENCES `char_properties` (`id`);

-- -----------------------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_essentials` (
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الحالة',
  `essential_id` int(10) UNSIGNED NOT NULL COMMENT 'كود الاثاث',
  `condition` tinyint(4) UNSIGNED DEFAULT NULL COMMENT 'الحالة: 5 ممتاز، 4 جيد جدا، 3 جيد، 2 سيء، 0 لا يوجد',
  `needs` tinyint(3) UNSIGNED DEFAULT NULL COMMENT 'عدد الاحتياج المطلوب'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول أثاث وضروريات المنزل ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;


ALTER TABLE `char_social_search_cases_essentials`
  ADD PRIMARY KEY (`search_cases_id`,`essential_id`),
  ADD KEY `fk_social_search_cases_essentials_essential_id_idx` (`essential_id`);


ALTER TABLE `char_social_search_cases_essentials`
  ADD CONSTRAINT `fk_social_search_cases_essentials_search_cases_id` FOREIGN KEY (`search_cases_id`) REFERENCES `char_social_search_cases` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_social_search_cases_essentials_essential_id` FOREIGN KEY (`essential_id`) REFERENCES `char_essentials` (`id`);

-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_banks` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي للجدول',
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الشخص',
  `bank_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'رقم البنك',
  `account_number` varchar(45) DEFAULT '' COMMENT 'رقم الحساب',
  `account_owner` varchar(255) DEFAULT '' COMMENT 'اسم مالك الحساب',
  `branch_name` int(10) UNSIGNED DEFAULT NULL COMMENT 'اسم الفرع'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='معلومات الحسابات البنكية  ل رب الاسرة في استمارات البحث' ROW_FORMAT=COMPACT;

ALTER TABLE `char_social_search_cases_banks`
  ADD PRIMARY KEY (`id`) ,
  ADD KEY `fk_social_search_cases_banks_search_cases_id_idx` (`search_cases_id`),
  ADD KEY `fk_social_search_cases_banks_bank_id_idx` (`bank_id`),
  ADD KEY `fk_social_search_cases_banks_branch_name_idx` (`branch_name`);

ALTER TABLE `char_social_search_cases_banks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'المفتاح الأساسي للجدول', AUTO_INCREMENT=1;

ALTER TABLE `char_social_search_cases_banks`
  ADD CONSTRAINT `fk_social_search_cases_banks_search_cases_id` FOREIGN KEY (`search_cases_id`) REFERENCES `char_social_search_cases`  (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_social_search_cases_banks_bank_id` FOREIGN KEY (`bank_id`) REFERENCES `char_banks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_social_search_cases_banks_branch_name` FOREIGN KEY (`branch_name`) REFERENCES `char_branches` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION ;



-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------
-- -----------------------------------------------------------------------------------------------

CREATE TABLE `char_social_search_cases_family` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'المفتاح الأساسي',
  `search_cases_id` int(10) UNSIGNED NOT NULL COMMENT 'رقم الشخص',

  `id_card_number` varchar(9) DEFAULT '' COMMENT 'رقم الهوية',
  `card_type` tinyint(4) DEFAULT '1' COMMENT 'نوع البطاقة:1- رقم الهوية و 2- بطاقة التعريف و  3- جواز السفر',
  `first_name` varchar(45) DEFAULT '' COMMENT 'الاسم الأول',
  `second_name` varchar(45) DEFAULT '' COMMENT 'اسم الأب',
  `third_name` varchar(45) DEFAULT '' COMMENT 'اسم الجد',
  `last_name` varchar(45) DEFAULT '' COMMENT 'اسم العائلة',
  `full_name` varchar(45) DEFAULT '' COMMENT 'الاسم',
  `prev_family_name` varchar(255) DEFAULT '' COMMENT 'اسم العائلة السابق',
  `gender` tinyint(4) DEFAULT NULL COMMENT 'الجنس:1- ذكر ، 2- أنثى',
  `marital_status_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'الحالة الاجتماعية : 4 متوفي ، 10 أعزب / آنسة ، 20 متزوج / ة ، 21 متعدد الزوجات  ، 30 مطلق  / ة ، 40 أرمل / ة',
  `birthday` date DEFAULT NULL COMMENT 'تاريخ الميلاد',

  `kinship_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'صلة القرابة',
  `primary_mobile` VARCHAR(255) NULL  COMMENT 'رقم الجوال',
  `watanya` VARCHAR(255) NULL   COMMENT 'رقم الوطنية',

  `condition` tinyint(4) DEFAULT NULL COMMENT 'الحالة: 1 جيدة، 2 يعاني من مرض مزمن، 3 من ذوي الاحتياجات الخاصة',
  `disease_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'المرض او الاعاقة',
  `details` text  DEFAULT '' COMMENT 'وصف نوع المرض أو الإعاقة',

  `grade` tinyint(4) DEFAULT NULL COMMENT 'المرحلة الدراسية : 1-الأول ، 2- الثاني ،  3-الثالث ، 4- الرابع ، 5-الخامس ، 6- السادس ،  7-السابع ، 8- الثامن ، 9- التاسع ، 10- العاشر  ، 11- الحادي عشر  ، 12- الثاني عشر ، 13- دراسات خاصة ، 14- تدريب مهني ، 15-سنة أولى جامعة ، 16-سنة ثانية جامعة ، 17- سنة ثالثة جامعة ، 18- سنة رابعة جامعة ، 19-سنة خامسة جامعة ' ,
  `currently_study` tinyint(3) NOT NULL DEFAULT '0' COMMENT 'هل ملتحق  :1-نعم ، 0-لا',
  `school` varchar(255) DEFAULT '' COMMENT 'اسم المؤسسة التعليمية',

  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'تاريخ الاضافة',
  `updated_at` datetime DEFAULT NULL COMMENT 'تاريخ اخر تعديل على السجل'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط المستفيدين (الأشخاص) مع الجمعية وتحديد نوع الاستفادة (كفالات ومساعدات)' ROW_FORMAT=COMPACT;


ALTER TABLE `char_social_search_cases_family` ADD PRIMARY KEY (`id`);
ALTER TABLE `char_social_search_cases_family`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;



-- -------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(458, 8, 'reports.case.photo', 'عرض صورة المواطن'),
(459, 8, 'org.SocialSearch.manage', 'إدارة البحث الاجتماعي'),
(460, 8, 'org.SocialSearch.manageCases', 'بيانات البحث الاجتماعي'),
(461, 8, 'org.SocialSearch.templates', 'قوالب تصدير البحث الاجتماعي'),
(462, 8, 'org.SocialSearch.create', 'إضافة بحث اجتماعي'),
(463, 8, 'org.SocialSearch.update', 'تحرير بيانات بحث اجتماعي'),
(464, 8, 'org.SocialSearch.delete', 'حذف بحث اجتماعي'),
(465, 8, 'org.SocialSearch.import', 'استيراد بيانات لبحث اجتماعي'),
(466, 8, 'org.SocialSearch.restore', 'استرجاع بحث اجتماعي');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
VALUES
('1', '459', '1', CURRENT_TIMESTAMP, '1'),
('1', '460', '1', CURRENT_TIMESTAMP, '1'),
('1', '461', '1', CURRENT_TIMESTAMP, '1'),
('1', '462', '1', CURRENT_TIMESTAMP, '1'),
('1', '463', '1', CURRENT_TIMESTAMP, '1'),
('1', '464', '1', CURRENT_TIMESTAMP, '1'),
('1', '465', '1', CURRENT_TIMESTAMP, '1'),
('1', '466', '1', CURRENT_TIMESTAMP, '1'),

('2', '459', '1', CURRENT_TIMESTAMP, '1'),
('2', '460', '1', CURRENT_TIMESTAMP, '1'),
('2', '461', '1', CURRENT_TIMESTAMP, '1'),
('2', '462', '1', CURRENT_TIMESTAMP, '1'),
('2', '463', '1', CURRENT_TIMESTAMP, '1'),
('2', '464', '1', CURRENT_TIMESTAMP, '1'),
('2', '465', '1', CURRENT_TIMESTAMP, '1'),
('2', '466', '1', CURRENT_TIMESTAMP, '1')
;