

DELIMITER $$
CREATE DEFINER=`takaful`@`localhost` FUNCTION `char_voucher_total_amount`(`id` INT(1) ,`p_via` VARCHAR(15)) RETURNS int(11)
    COMMENT 'دالة لجلب  اجمالي المبلغ في القسيمة بعملة القسيمة'
BEGIN
declare c INTEGER;
      if p_via = 'o' then
	set c = (select (`char_vouchers`.`value` * `char_vouchers`.`count`) AS `amount`
                         from `char_vouchers` 
                         where (`char_vouchers`.`id` =id));
     elseif p_via = 'sh' then
		set c = (select (`char_vouchers`.`value` * `char_vouchers`.`count` * `char_vouchers`.`exchange_rate`) AS `amount`
                         from `char_vouchers` 
                         where (`char_vouchers`.`id` =id));
    end if;
RETURN c;
END$$
DELIMITER ;

ALTER TABLE `char_payments_cases` ADD `id` INT UNSIGNED NOT NULL AUTO_INCREMENT AFTER `payment_id`, ADD PRIMARY KEY (`id`);


INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(520, 8, 'aidRepository.repositories.manageCentral', 'إدارة سلة المشاريع المركزية');

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`) VALUES
(2, 520, 1, CURRENT_TIMESTAMP, 1),
(488, 520, 1, CURRENT_TIMESTAMP, 1);
