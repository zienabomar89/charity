
CREATE TABLE `char_categories_form` (
  `category_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Ø¬Ø¯ÙˆÙ„ ÙŠØ­ØªÙˆÙŠ Ø¹Ù„Ù‰ Ø§Ù„Ù†Ù…Ø§Ø°Ø¬ Ø§Ù„Ù…Ø®ØµØµØ© Ù„Ù„ÙƒÙ�Ø§Ù„Ø§Øª ÙˆØ§Ù„Ù…Ø³Ø§Ø¹Ø¯Ø§Øª Ø¹Ù„Ù‰ Ù…Ø³ØªÙˆÙ‰';

--
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(507, 4, 'sponsorship.category.forms', 'تعيين النماذج المخصصة للاستمارات - خاص بالجمعيات'),
(508, 3, 'aid.category.forms', 'تعيين النماذج المخصصة للاستمارات - خاص بالجمعيات');


INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
SELECT id as role_id  , 507 as permission_id, 1 as allow  , CURRENT_TIMESTAMP as created_at, user_id as created_by
FROM `char_roles` WHERE `char_roles`.`name` = 'مدير جمعية' or `char_roles`.`name` ='مدير منطقة' or `char_roles`.`name` ='مدير النظام' or `char_roles`.`name` ='نائب مدير المنطقة'
;

INSERT INTO `char_roles_permissions` (`role_id`, `permission_id`, `allow`, `created_at`, `created_by`)
SELECT id as role_id  , 508 as permission_id, 1 as allow  , CURRENT_TIMESTAMP as created_at, user_id as created_by
FROM `char_roles` WHERE `char_roles`.`name` = 'مدير جمعية' or `char_roles`.`name` ='مدير منطقة' or `char_roles`.`name` ='مدير النظام' or `char_roles`.`name` ='نائب مدير المنطقة'

ALTER TABLE `char_administrative_report` ADD `category` TINYINT NULL DEFAULT '1' COMMENT 'نوع التقرير : مركزي او مخصص' AFTER `type`;

DELIMITER $$
CREATE  FUNCTION `get_in_count`(`p_via` VARCHAR(15), `father_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
BEGIN
declare c INTEGER;
      if p_via = 'male' then
		set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
			                 );
     elseif p_via = 'female' then
			set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
			                 );
    end if;
RETURN c;
END$$
DELIMITER ;


DELIMITER $$
CREATE  FUNCTION `get_mother_child_count`(`p_via` VARCHAR(15), `mother_id` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
BEGIN
declare c INTEGER;
      if p_via = 'male' then
		set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
			                 );
     elseif p_via = 'female' then
			set c = (
  						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
			                 );
    end if;
RETURN c;
END$$
DELIMITER ;



DELIMITER $$
CREATE  FUNCTION `get_wive_count`(`p_via` VARCHAR(15), `hus` INT(10)) RETURNS int(11)
    COMMENT 'دالة لارجاع عدد الزوجات '
BEGIN
declare c INTEGER;

set c = (
  					SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
		WHERE `p`.`husband_id` = hus and `p`.`gender` = 2 and death_date is null );


RETURN c;
END$$
DELIMITER ;
