ALTER TABLE `aid_projects` CHANGE `custom_organization` `custom_organization` 
FLOAT NULL DEFAULT NULL COMMENT 'الجمعية التي تمتلك نسبة مخصصة', 
CHANGE `custom_ratio` `custom_ratio` FLOAT NULL DEFAULT '0' COMMENT 'النسبة المخصصة';


ALTER TABLE `char_categories_form` ADD `status` TINYINT NULL DEFAULT '0' AFTER `form_id`;
