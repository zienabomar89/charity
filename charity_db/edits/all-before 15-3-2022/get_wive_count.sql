-- CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_wive_count`(`p_via` VARCHAR(15), `mother_id` INT(10)) RETURNS int(11)
--     COMMENT 'دالة لارجاع عدد أ�?راد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص الأم '
-- BEGIN
-- declare c INTEGER;
--       if p_via = 'male' then
-- 		set c = (
--   						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
-- 			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
-- 			                 );
--      elseif p_via = 'female' then
-- 			set c = (
--   						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
-- 			      		   WHERE `p`.`mother_id` = mother_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
-- 			                 );
--     end if;
-- RETURN c;
-- END


-- DELIMITER $$
-- CREATE DEFINER=`takaful`@`localhost` FUNCTION `get_in_count`(`p_via` VARCHAR(15), `father_id` INT(10)) RETURNS int(11)
--     COMMENT 'دالة لارجاع عدد أ�?راد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل '
-- BEGIN
-- declare c INTEGER;
--       if p_via = 'male' then
-- 		set c = (
--   						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
-- 			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 1 and death_date is null and marital_status_id = 10
-- 			                 );
--      elseif p_via = 'female' then
-- 			set c = (
--   						   SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
-- 			      		   WHERE `p`.`father_id` = father_id and `p`.`gender` = 2 and death_date is null and marital_status_id = 10
-- 			                 );
--     end if;
-- RETURN c;
-- END$$
-- DELIMITER ;


DELIMITER $$
CREATE FUNCTION `get_wive_count`(`husband_id` INT(10)) RETURNS int(11)
    COMMENT '???? ????? ??? ???????'
BEGIN
declare c INTEGER;
    	set c = (
  					 SELECT COUNT(`p`.`id`) FROM `char_persons` AS `p`
			      	 WHERE `p`.`gender` = 2 and `p`.`death_date` is null and `p`.`marital_status_id` = 20 and 
                            `p`.`id` IN ( SELECT `kin`.`r_person_id` 
                                          from `char_persons_kinship` AS `kin` 
                                          where  `kin`.`kinship_id` in(21,29) and  `kin`.`l_person_id` = husband_id)
			     );
RETURN c;
END$$
DELIMITER ;


-- CREATE FUNCTION `get_candidates_count`(`organization_id` INT(10), `status` INT(10)) RETURNS int(11)
--     COMMENT 'حساب عدد المرشحين للكفالات عند الجمعية بناء على الحالة'
-- BEGIN
-- declare c INTEGER;
--     	set c = (
--                  select COUNT(char_sponsorship_cases.id)
--                  from char_sponsorship_cases
--                  join  char_cases on  char_cases.id  =  char_sponsorship_cases.case_id
--                  where char_cases.organization_id = organization_id and char_sponsorship_cases.status = status
--
-- 			     );
-- RETURN c;
-- END$$
-- DELIMITER ;