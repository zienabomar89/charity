ALTER FUNCTION `char_case_file`  COMMENT 'ارجاع مرفقات حالة ما بناء على معرف الحالة' ;
ALTER FUNCTION `char_count_voucher_persons`  COMMENT 'دالة لارجاع عدد الأشخاص المستفيدين من قسيمة معينة بناء على رقم القسيمة' ;
ALTER FUNCTION `char_get_case_aid_sources`  COMMENT 'دالة لارجاع مصادر المساعدات لشخص ما بناء على معرف الشخص ' ;
ALTER FUNCTION `char_get_case_essentials_need`  COMMENT 'دالة لارجاع احتياجات العائلة من الأثاث  ' ;
ALTER FUNCTION `char_get_case_rank`  COMMENT 'دالة لارجاع محصلة الوزن لحالة ما بناء على رقم الحالة' ;
ALTER FUNCTION `char_get_children_count`  COMMENT 'دالة لارجاع عدد الأبناء (ذكور أو اناث او الكل) لشخص ما بناء على معرف الشخص' ;
ALTER FUNCTION `char_get_family_count`  COMMENT 'دالة لارجاع عدد أفراد العائلة  (ذكور أو اناث او الكل) بناء على رقم الشخص او الاب او المعيل ' ;
ALTER FUNCTION `char_get_family_total_category_payments`  COMMENT 'دالة لارجاع كافة مبالغ كفالة ما  تلقتها عائلة خلال فترة معينة من خلال كود معرف الأب' ;
ALTER FUNCTION `char_get_family_total_payments`  COMMENT 'دالة لارجاع كافة المبالغ التي تلقتها عائلة خلال فترة معينة من خلال كود معرف الأب' ;
ALTER FUNCTION `CHAR_GET_FORM_ELEMENT_PRIORITY`  COMMENT 'دالة لارجاع خيار الأولوية لحقل ما' ;
ALTER FUNCTION `char_get_guardian_persons_count`  COMMENT 'دالة لارجاع عدد المعاليين  لشخص ما بناء على معرف الشخص' ;
ALTER FUNCTION `char_get_organization_nominated_count`  COMMENT 'دالة لارجاع عدد المرشحين لجمعية ما ضمن مشروع معين من خلال رقم المشروع ورقم الجمعية' ;
ALTER FUNCTION `char_get_organizations_child_nominated_count`  COMMENT 'دالة لارجاع عدد المرشحين للجمعيات الأبناء لجمعية  ما ضمن مشروع معين من خلال رقم المشروع ورقم الجمعية';
ALTER FUNCTION `char_get_payment_document_type_count`  COMMENT 'دالة لارجاع عدد المرفقات المحملة لصرفية ما من نوع مرفق ما ' ;
ALTER FUNCTION `char_get_payment_document_type_id`  COMMENT 'دالة لارجاع رقم المرفق المرفوع ضمن صرفية بناء على نوع المرفق' ;
ALTER FUNCTION `char_get_person_average_payments`  COMMENT 'دالة لارجاع متوسط المبالغ التي تلقتها حالة ما خلال فترة معينة' ;
ALTER FUNCTION `char_get_person_total_payments`  COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها شخص ما خلال فترة معينة' ;
ALTER FUNCTION `char_get_persons_properties`  COMMENT 'دالة لارجاع ممتلكات شخص ما' ;
ALTER FUNCTION `char_get_project_organization_data`  COMMENT 'دالة لارجاع  اجمالي القيمة او الكمية او النسبة للجمعية ضمن مشروع معين' ;
ALTER FUNCTION `char_get_project_person_organization_count`  COMMENT 'دالة لارجاع عدد المرشحين لمشروع معين اما ضمن جمعية او أبناء لجمعية او جمعيات تابعة لجمعية' ;
ALTER FUNCTION `char_get_siblings_count`  COMMENT 'دالة لارجاع عدد الاخوة  (ذكور أو اناث او الكل)  عن طريق الام أو الاب لشخص ما بناء على معرف الشخص' ;
ALTER FUNCTION `char_get_siblings_counts`  COMMENT 'دالة لارجاع عدد الاخوة  (ذكور أو اناث او الكل)  عن طريق الام أو الاب لشخص ما بناء على معرف الشخص' ;
ALTER FUNCTION `char_get_sponsorship_cases_documents_count`  COMMENT 'دالة لارجاع عدد مرفقات التقرير المحملة لحالة معين ولنوع مرفق معينضمن فترة تقرير ما' ;
ALTER FUNCTION `char_get_sponsorship_cases_documents_file_path`  COMMENT 'دالة لارجاع مسار الملف المرفق لتقرير جهة كافلة حسب نوع المرفق و تصنيفه ' ;
ALTER FUNCTION `char_get_sponsorship_cases_documents_id`  COMMENT 'دالة لارجاع رقم الملف المرفق لتقرير جهة كافلة حسب نوع المرفق و تصنيفه ' ;
ALTER FUNCTION `char_get_sponsorship_count`  COMMENT 'دالة لجلب عدد الجهات الكافلة المرشح او المكفول لهها حالة ما' ;
ALTER FUNCTION `char_get_voucher_persons_count`  COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة' ;
ALTER FUNCTION `char_get_voucher_persons`  COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة' ;
ALTER FUNCTION `char_org_case_count`  COMMENT 'دالة لجلب عدد الحالات المسجلة لدى جمعية معينة' ;
ALTER FUNCTION `char_persons_receipt_count`  COMMENT 'دالة لجلب عدد القسائم المستحقة لشخص ما ضمن قسيمة معينة' ;
ALTER FUNCTION `char_total_voucher_amount`  COMMENT 'دالة لجلب  اجمالي المبلغ المستحق لشخص ما ضمن قسيمة معينة' ;
ALTER FUNCTION `char_voucher_amount`  COMMENT 'دالة لاحتساب اجمالي المبلغ الخاص بقسيمة معينة' ;
ALTER FUNCTION `clear_text`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER FUNCTION `get_case_documents`  COMMENT 'دالة لارجاع ارقام الملفات المرفقة لحالة ' ;
ALTER FUNCTION `get_cases_count`  COMMENT 'دالة لارجاع عدد الحالات التابعة لمنطقة معينة المسجلة لدى جمعية ما' ;
ALTER FUNCTION `get_cases_essentials_count`  COMMENT 'دالة لارجاع عدد مستلزمات الأثاث لدى حالة ما' ;
ALTER FUNCTION `get_document`  COMMENT 'دالة لارجاع ارقام الملفات المرفقة لشخص ما' ;
ALTER FUNCTION `get_mosque_person_count`  COMMENT 'دالة لارجاع عدد الأشخاص التابعين لمسجد معين ضمن قسيمة ما' ;
ALTER FUNCTION `get_org_payments_cases_amount_`  COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها شخص ما خلال فترة معينة ضمن تصنيف معين و من خلال جمعيات معينة' ;
ALTER FUNCTION `get_org_payments_cases_count_`  COMMENT 'دالة لارجاع عدد الصرفيات التي استلمها شخص ما خلال فترة معينة ضمن تصنيف معين و من خلال جمعيات معينة' ;
ALTER FUNCTION `get_org_report_data_count`  COMMENT 'دالة ارجات عدد القيم التي أدخلتها جمعيات معينة لخيار ما في التقرير الإداري' ;
ALTER FUNCTION `get_organization_payments_cases_amount_`  COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';
ALTER FUNCTION `get_organization_payments_cases_amount`  COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها  المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';
ALTER FUNCTION `get_organization_total`  COMMENT 'دالة لارجاع اجمال القيمة او الكمية او النسبة لجمعية ما ضمن مشروع في الوعاء' ;
ALTER FUNCTION `get_payments_beneficiary_count`  COMMENT 'دالة لارجاع عدد المستفيدين من الصرفية حسب تصنيفهم ( مكفولين ، جدد ، ليس لديهم حركة مالية )' ;
ALTER FUNCTION `get_payments_cases_count`  COMMENT 'دالة لارجاع عدد الأرقام الذين يملكون رقم كفالة معين ضمن الصرفية' ;
ALTER FUNCTION `get_payments_recipient_amount`  COMMENT 'دالة لارجاع اجمالي المبالغ المستلمة في الصرفية بناء على المستلم ( الشخص نفسه أو المعيل)' ;
ALTER FUNCTION `get_project_persons_count`  COMMENT 'دالة لارجاع عدد الأشخاص حسب حالتهم في مشروع ضمن وعاء' ;
ALTER FUNCTION `get_project_total`  COMMENT 'دالة لارجاع اجمال القيمة او الكمية او النسبة لجمعية أم ضمن مشروع في الوعاء' ;
ALTER FUNCTION `get_total_org_ratio`  COMMENT 'دالة لارجاع اجمالي النسبة الخاص بجمعية معينة ضمن مشروع ما في الوعاء' ;
ALTER FUNCTION `getfiles`  COMMENT 'ارجاع مرفقات شخص ما بناء على معرف الشخص' ;
ALTER FUNCTION `normalize_text_ar_old`  COMMENT 'عمل توحيد للنصوص المحفوظة على النظام - الاصلية' ;
ALTER FUNCTION `normalize_text_ar`  COMMENT 'عمل توحيد للنصوص المحفوظة على النظام - نسخة معدلة' ;

ALTER FUNCTION `get_voucher_count_` COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';
ALTER FUNCTION `get_voucher_count`  COMMENT 'دالة لارجاع عدد القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_recipient_amount`
      COMMENT 'دالة لارجاع المبلغ الذي استلمه شخص معين في صرفية ما ( المبلغ الأصلي ، المبلغ الأصلي بعد الخصم ، المبلغ بالشيكل ، المبلغ بالشيكل بعد الخصم';


ALTER FUNCTION `get_organization_payments_cases_category_amount`
      COMMENT 'دالة لارجاع اجمالي المبالغ التي استلمها  المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';
      
ALTER FUNCTION `get_organization_payments_cases_category_count`
      COMMENT 'دالة لارجاع عدد الصرفيات التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_payments_cases_count_`
      COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_payments_cases_count`
      COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_payments_category_amount`  
      COMMENT 'دالة لارجاع اجمالي مبالغ الصرفيات التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_payments_category_count`  
      COMMENT 'دالة لارجاع اجمالي عدد المستفيدين  من الصرفيات خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_vouchers_value_`  
      COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_organization_vouchers_value`  
      COMMENT 'دالة لارجاع اجمالي قيم القسائم التي استلمها المستفيدين خلال فترة معينة ضمن تصنيف معين و من خلال جهات كافلة  او جمعيات معينة';

ALTER FUNCTION `get_project_location_persons_count`  
      COMMENT 'دالة لارجاع عدد الأشخاص في مشروع ما تابعين لجمعية معينة ويقعون ضمن منطقة جغرافية ما حسب تصنيف المنطقة ( محافظة ، منطقة ، حي ، مربع ، مسجد)';



ALTER PROCEDURE `char_habitable_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_house_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_locations_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_persons_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_properties_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `person_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `relays_cases_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `update_imported_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_aid_cases`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_banks_branches`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_building_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_case`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_death_causes`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_diseases`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_edu_authorities`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_edu_degrees`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_edu_stages`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_father`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_furniture_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_guardian`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_habitable_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_house_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_kinship`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_locations`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_marital_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_mother`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_payments_banks`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_payments_currencies`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_payments_sponsors`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_property_types`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_relays_cases`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_roof_materials`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_sponsorship_cases`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_sponsorships_sponsors`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_vouchers_sponsors`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_work_jobs`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_work_reasons`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `import_work_status`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `update_imported`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_diseases_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_banks_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_branches_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_organizations_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_currencies_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_work_jobs_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_work_reasons_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_marital_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_edu_degrees_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_edu_stages_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_document_types_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_property_types_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_edu_authorities_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_roof_materials_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_furniture_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_kinship_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_building_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_work_status_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_death_causes_save`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_organizations_closure_init`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `fix_duplicated_id`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `remove_case_duplicates`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `remove_duplicates`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `replace_person`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `update_persons`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `get_case_files`  COMMENT 'غير مستخدمة في النظام و تم استخدامها في عملية استيراد البيانات' ;
ALTER PROCEDURE `char_get_family_detail`  COMMENT 'ارجاع بيانات  الأبناء والبنات لشخص ما بناء على معرف الشخص ' ;
ALTER PROCEDURE `char_get_brother_detail`  COMMENT 'ارجاع بيانات الأخوة والأخوات لشخص ما بناء على معرف الشخص اعتمادا على الام والأب له' ;
ALTER PROCEDURE `char_get_person_detail`  COMMENT 'ارجاع بيانات الشخص اعتمادا على  معرف الشخص  ' ;
ALTER PROCEDURE `char_update_case_rank`  COMMENT 'حساب المحصلة  قيمة معينة للحالة بناء على معرف الحالة' ;
ALTER PROCEDURE `char_get_person_payments`  COMMENT 'دالة لارجاع لمبالغ المالية التي تلقتها حالة اما على مستوى النظام او ع مستوى جمعية معين ' ;