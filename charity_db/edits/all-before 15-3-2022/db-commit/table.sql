SELECT * FROM INFORMATION_SCHEMA.ROUTINES
WHERE ROUTINE_SCHEMA = 'kafalat' ;

SELECT TABLE_NAME , TABLE_COLLATION , TABLE_COMMENT
FROM INFORMATION_SCHEMA.tables
WHERE table_schema = 'kafalat';

SELECT table_name , table_comment     FROM INFORMATION_SCHEMA.TABLES WHERE table_schema='takaful_db' ;
ALTER TABLE `aid_categories` COMMENT = 'جدول يحتوي على تصنيفات المساعدات' ROW_FORMAT = COMPACT;
ALTER TABLE `app_logs` CHANGE `user_id` `user_id` INT(10) UNSIGNED NOT NULL COMMENT 'اسم المستخدم';


-- update table
SELECT table_name,table_comment,
CONCAT('ALTER TABLE `', table_name,   '` COMMENT = ',  column_name,   '` `', ' COMMENT \'',    column_comment,   '\' ;') as script
FROM information_schema.columns
WHERE table_schema = 'takaful_db'
ORDER BY table_name , column_name;


-- update column

SELECT table_name,column_name,CONCAT('ALTER TABLE `', table_name,   '` CHANGE `',  column_name,   '` `',
        column_name,
        '` ',
        column_type,
        ' ',
        IF(is_nullable = 'YES', '' , 'NOT NULL '),
        IF(column_default IS NOT NULL, concat('DEFAULT ', IF(column_default = 'CURRENT_TIMESTAMP', column_default, CONCAT('\'',column_default,'\'') ), ' '), ''),
        IF(column_default IS NULL AND is_nullable = 'YES' AND column_key = '' AND column_type = 'timestamp','NULL ', ''),
        IF(column_default IS NULL AND is_nullable = 'YES' AND column_key = '','DEFAULT NULL ', ''),
        extra,
        ' COMMENT \'',
        column_comment,
        '\' ;') as script
FROM
    information_schema.columns
WHERE
    table_schema = 'takaful_db'
ORDER BY table_name , column_name


/*

//        $total= \DB::select("SELECT
//table_name,
//table_comment,
//CONCAT('ALTER TABLE `', table_name,   '` ',   ' COMMENT =\'',
//        table_comment,   '\'  ROW_FORMAT = COMPACT ;') as script
// FROM table_comments
// where table_name != 'char_aid_cases_incomplete_data' and table_name != 'char_sponsorship_cases_incomplete_data' and table_name not like '%view%' and ( table_comment is not null and table_comment != ' ')
//");
//
//
//        foreach ($total as $key=>$value){
//            \DB::statement("$value->script");
//        }
//        return response()->json(['status'=>false , 'out' => $total]);

//      $data =
//
//        $output = [];
//        foreach ($data as $key=>$value){
//            $str = str_replace('-', ' ', $key);
//            $str_ = str_replace('_', ' ', $str);
//            $output['"' .$key .'"']= '"' .trim(ucwords($str_)).'"';
//
//        }
//
//        return response()->json(['status'=>false , 'x'=>$output]);
*/