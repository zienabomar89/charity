select `i18n`.`location_id` AS `country_id`,`i18n`.`name` AS `country_name`,
        ifnull(`district_i18n`.`location_id`,'-') AS `district_id`,ifnull(`district_i18n`.`name`,'-') AS `district_name`,
        ifnull(`city_i18n`.`location_id`,'-') AS `city_id`,ifnull(`city_i18n`.`name`,'-') AS `city_name`,
        ifnull(`location_i18n`.`location_id`,'-') AS `location_id`,ifnull(`location_i18n`.`name`,'-') AS `location_name`,
        ifnull(`square_i18n`.`location_id`,'-') AS `square_id`,ifnull(`square_i18n`.`name`,'-') AS `square_name` ,
        ifnull(`mosque_i18n`.`location_id`,'-') AS `mosque_id`,ifnull(`mosque_i18n`.`name`,'-') AS `mosque_name`
from `char_aids_locations`
join `char_aids_locations_i18n` `i18n` on(((`i18n`.`location_id` = `char_aids_locations`.`id`) and (`i18n`.`language_id` = 1)))
left join `char_aids_locations` `district` on((`district`.`parent_id` = `char_aids_locations`.`id`))
left join `char_aids_locations_i18n` `district_i18n` on(((`district_i18n`.`location_id` = `district`.`id`) and (`district_i18n`.`language_id` = 1)))
left join `char_aids_locations` `city` on((`city`.`parent_id` = `district`.`id`))
left join `char_aids_locations_i18n` `city_i18n` on(((`city_i18n`.`location_id` = `city`.`id`) and (`city_i18n`.`language_id` = 1)))
left join `char_aids_locations` `location` on((`location`.`parent_id` = `city`.`id`))
left join `char_aids_locations_i18n` `location_i18n` on(((`location_i18n`.`location_id` = `location`.`id`) and (`location_i18n`.`language_id` = 1)))
left join `char_aids_locations` `square` on((`square`.`parent_id` = `location`.`id`))
left join `char_aids_locations_i18n` `square_i18n` on(((`square_i18n`.`location_id` = `square`.`id`) and (`square_i18n`.`language_id` = 1)))
left join `char_aids_locations` `mosque` on((`mosque`.`parent_id` = `square`.`id`))
left join `char_aids_locations_i18n` `mosque_i18n` on(((`mosque_i18n`.`location_id` = `mosque`.`id`) and (`mosque_i18n`.`language_id` = 1)))

where (`mosque`.`location_type` = 5)
order by district_name
