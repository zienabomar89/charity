
DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `char_get_person_payments` (IN `id` INT(10), IN `org` INT(10))  BEGIN
    IF org = 1  then
        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id ;
      end if;

    if org <> 1  then

        SELECT `p`.`payment_id`,`p`.`person_id`,`p`.`category_id`,`p`.`amount`,`p`.`bank_id`,`p`.`guardian_id`,`p`.`cheque_number`,`p`.`cheque_date`,`p`.`months`,`p`.`created_at`,`p`.`status`

        FROM `char_payments_persons` AS `p`
        LEFT JOIN  `char_payments` AS `L4` on `L4`.`id`  = `p`.`payment_id`
        WHERE `p`.`person_id` =  id  and L4.organization_id = org;
              end if;


END$$

CREATE  PROCEDURE `char_organizations_closure_init` ()  BEGIN
  DECLARE done INT DEFAULT FALSE;
  DECLARE v_id, v_parent_id INT;
  DECLARE cur1 CURSOR FOR SELECT `id`, `parent_id` FROM `char_organizations` ORDER BY `id`;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

  OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO v_id, v_parent_id;
    IF done THEN
      LEAVE read_loop;
    END IF;

    INSERT INTO `char_organizations_closure` (`ancestor_id`, `descendant_id`, `depth`)
		SELECT `c`.`ancestor_id`, v_id, `c`.`depth` + 1
		FROM `char_organizations_closure` AS `c`
		WHERE `c`.`descendant_id` = v_parent_id
		UNION ALL
		SELECT v_id, v_id, 0;

  END LOOP;

  CLOSE cur1;
END$$

CREATE  PROCEDURE `char_update_case_rank` (IN `p_id` INT, IN `p_context` VARCHAR(50))  BEGIN
	case p_context
		when 'death_cause' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `death_cause_id` = p_id
            );
        when 'marital_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `id` from `char_persons` where `marital_status_id` = p_id
            );
        when 'disease' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_health` where `disease_id` = p_id
            );

        when 'property_type' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `property_type_id` = p_id
            );
        when 'roof_materials' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_residence` where `roof_material_id` = p_id
            );
        when 'work_status' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_status_id` = p_id
            );
        when 'work_reason' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_reason_id` = p_id
            );
		when 'work_jobs' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_job_id` = p_id
            );
        when 'work_wage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_work` where `work_wage_id` = p_id
            );
		when 'education_authority' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `authority` = p_id
            );
		when 'education_degree' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `degree` = p_id
            );
		when 'education_stage' then
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `person_id` in (
				select `person_id` from `char_persons_education` where `stage` = p_id
            );
        else
			UPDATE `char_cases` SET
			`rank` = char_get_case_rank(`id`)
			WHERE `id` = p_id;
	end case;
END$$

--
-- Functions
--
CREATE  FUNCTION `char_get_case_aid_sources` (`id` INT) RETURNS TEXT CHARSET latin1 BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_aid_sources`.`name` SEPARATOR ' - ' )
FROM `char_persons_aids`
join `char_aid_sources` ON `char_aid_sources`.`id` = `char_persons_aids`.`aid_source_id`
where `char_persons_aids`.`person_id`=id  and `char_persons_aids`.`aid_take` =1
    );

RETURN c;
END$$

CREATE  FUNCTION `char_get_case_essentials_need` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_essentials`.`name` SEPARATOR ' - ' )
             FROM `char_cases_essentials`
             join `char_essentials` ON `char_essentials`.`id` = `char_cases_essentials`.`essential_id`
             where `char_cases_essentials`.`case_id`=id and
                  (`char_cases_essentials`.`needs` is not null ) and
                  ( `char_cases_essentials`.`needs` != 0)
            );

RETURN c;
END$$

CREATE  FUNCTION `char_get_case_rank` (`c_id` INT) RETURNS FLOAT BEGIN
	DECLARE v_rank FLOAT;

	SET v_rank := (select (
      ifnull(`char_death_causes`.`weight`, 0)
    + ifnull(`char_marital_status`.`weight`, 0)
    + ifnull(`char_diseases`.`weight`, 0)
    + ifnull(`char_roof_materials`.`weight`, 0)
    + ifnull(`char_property_types`.`weight`, 0)
    + ifnull(`char_work_status`.`weight`, 0)
    + ifnull(`char_work_reasons`.`weight`, 0)
    + ifnull(`char_work_jobs`.`weight`, 0)
    + ifnull(`char_work_wages`.`weight`, 0)
    + sum(ifnull(`char_aid_sources_weights`.`weight`, 0))
    + sum(ifnull(`char_essentials_weights`.`weight`, 0))
    + sum(ifnull(`char_properties_weights`.`weight`, 0))) AS rank
	from `char_cases`
	inner join  `char_persons` on `char_cases`.`person_id` = `char_persons`.`id`

	left join `char_death_causes` on `char_persons`.`death_cause_id` = `char_death_causes`.`id`
	left join `char_marital_status` on `char_persons`.`marital_status_id` = `char_marital_status`.`id`

	left join `char_persons_health` on `char_cases`.`person_id` = `char_persons_health`.`person_id`
	left join `char_diseases` on `char_persons_health`.`disease_id` = `char_diseases`.`weight`

    left join `char_residence` on `char_cases`.`person_id` = `char_residence`.`person_id`
    left join `char_property_types` on `char_residence`.`property_type_id` = `char_property_types`.`id`
    left join `char_roof_materials` on `char_residence`.`roof_material_id` = `char_roof_materials`.`id`

    left join `char_persons_properties` on `char_cases`.`person_id` = `char_persons_properties`.`person_id`
    left join `char_properties_weights` on `char_persons_properties`.`property_id` = `char_properties_weights`.`property_id`
    AND `char_persons_properties`.`quantity` BETWEEN `char_properties_weights`.`value_min` AND `char_properties_weights`.`value_max`

	left join `char_persons_work` on `char_cases`.`person_id` = `char_persons_work`.`person_id`
	left join `char_work_status` on `char_persons_work`.`work_status_id` = `char_work_status`.`id`
	left join `char_work_reasons` on `char_persons_work`.`work_reason_id` = `char_work_reasons`.`id`
	left join `char_work_jobs` on `char_persons_work`.`work_job_id` = `char_work_jobs`.`id`
	left join `char_work_wages` on `char_persons_work`.`work_wage_id` = `char_work_wages`.`id`

    left join `char_persons_aids` on `char_cases`.`person_id` = `char_persons_aids`.`person_id`
    left join `char_aid_sources_weights` on `char_persons_aids`.`aid_source_id` = `char_aid_sources_weights`.`aid_source_id`
    AND `char_persons_aids`.`aid_value` BETWEEN `char_aid_sources_weights`.`value_min` AND `char_aid_sources_weights`.`value_max`

    left join `char_cases_essentials` on `char_cases`.`id` = `char_cases_essentials`.`case_id`
    left join `char_essentials_weights` on `char_cases_essentials`.`essential_id` = `char_essentials_weights`.`essential_id`
    AND `char_cases_essentials`.`condition` = `char_essentials_weights`.`value`
    WHERE `char_cases`.`id` = c_id);
RETURN v_rank;
END$$

CREATE  FUNCTION `char_get_children_count` (`person_id` INT(10), `gender` TINYINT(4)) RETURNS INT(11) BEGIN
declare c INTEGER;

   	if gender is null then
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE `a`.`father_id` = person_id  or `a`.`mother_id` = person_id
					);

	       else
			 set c = ( select COUNT(`a`.`id`)
			   		   FROM `char_persons` AS `a`
					   WHERE (`a`.`father_id` = person_id  or `a`.`mother_id` = person_id) and `a`.`gender`= gender
					);

	       end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_family_count` (`p_via` VARCHAR(15), `master_id` INT(10), `gender` TINYINT(4), `edge_id` INT(10)) RETURNS INT(11) NO SQL
BEGIN
declare c INTEGER;

      if p_via = 'left' then
	if gender is null then
				set c = (
						  SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
					      WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
		                );

	       else
				set c = (
						 SELECT COUNT(`k`.`r_person_id`) FROM `char_persons_kinship` AS `k`
			  			 JOIN  `char_persons` ON ((`char_persons`.`id`=`k`.`r_person_id`) and (`char_persons`.`gender`= gender))
						 WHERE `k`.`l_person_id` = master_id and `k`.`r_person_id` != master_id and `k`.`r_person_id` != edge_id
	                    );

	       end if;
    elseif p_via = 'right' then
		if gender is null then
				set c = (
  						   SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			      		   WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id
			                 );

	       else
				set c = (
						  SELECT COUNT(`k`.`l_person_id`) FROM `char_persons_kinship` AS `k`
			    		 JOIN  `char_persons` ON ( (`char_persons`.`id`=`k`.`l_person_id`) and  (`char_persons`.`gender`= gender ))
						 WHERE `k`.`r_person_id` = master_id and `k`.`l_person_id` != master_id and `k`.`l_person_id` != edge_id
	                    );
	        end if;

    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_family_total_category_payments` (`father` INT(10), `p_date_from` DATE, `p_date_to` DATE, `category` INT(10)) RETURNS DOUBLE BEGIN
  declare sum double;
  IF category is null THEN

   if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                               AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     end if;

     ELSE

     if p_date_from is null and p_date_to is null then
             set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                       FROM  `char_payments_cases`  AS `p`
                       join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                       join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                       WHERE (
                               (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                                AND   ((`char_cases`.`category_id` = category ))
                                AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                             )  );
     elseif p_date_from is not null and p_date_to is null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                          (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                       ) );
     elseif p_date_from is null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));
     elseif p_date_from is not null and p_date_to is not null then
       set sum = ( SELECT SUM(`p`.`amount` * `char_payments` .`exchange_rate`  )
                 FROM  `char_payments_cases`  AS `p`
                 join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                 join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                 WHERE (
                         (`char_cases`.`person_id`  IN ( select `b`.`id` FROM `char_persons` AS `b` WHERE `b`.`father_id` = father  ))
                         AND   ((`char_cases`.`category_id` = category ))
                         AND   ((`p`.`status` = 1) OR (`p`.`status` = 2))
                         AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                       ));



        end if;

     END IF;
RETURN sum;
END$$

CREATE  FUNCTION `char_get_family_total_payments` (`l_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
      set sum = (   SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                    FROM  `char_payments_cases`  AS `p`
                    join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                    join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                    join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                    WHERE ((`p`.`status` = 1) OR (`p`.`status` = 2))
                );
    elseif p_date_from is not null and p_date_to is null then
     set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)))
              );

    elseif p_date_from is null and p_date_to is not null then
     set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to)))
              );


    elseif p_date_from is not null and p_date_to is not null then
        set sum = (  SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                  FROM  `char_payments_cases`  AS `p`
                  join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                  join  `char_persons_kinship` ON ((`char_persons_kinship`.`r_person_id` = `char_cases`.`person_id`) and (`char_persons_kinship`.`l_person_id` = l_person_id))
                  join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                  WHERE (((`p`.`status` = 1) OR (`p`.`status` = 2)) AND   ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         AND   ((`p`.`date_from` <= p_date_to) AND   (`p`.`date_to` >= p_date_to))
                        )
              );
    end if;

RETURN sum;
END$$

CREATE  FUNCTION `CHAR_GET_FORM_ELEMENT_PRIORITY` (`p_category_id` INT, `p_element_name` VARCHAR(255)) RETURNS INT(11) BEGIN

                DECLARE v_priority INT;



    SET v_priority = (SELECT `e`.`priority`
                        FROM `char_categories_form_element_priority` `e`
                        JOIN `char_forms_elements` AS `el` on `el`.`id` =`e`.`element_id` and `el`.`name` = p_element_name
						where`e`.`category_id` = p_category_id);

RETURN v_priority;

END$$

CREATE  FUNCTION `char_get_guardian_persons_count` (`p_guardian_id` INT) RETURNS INT(11) BEGIN
DECLARE c INTEGER;

set c = (SELECT COUNT(`individual_id`) FROM `char_guardians` WHERE `guardian_id` = p_guardian_id);

RETURN c;
END$$

CREATE  FUNCTION `char_get_persons_properties` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
  declare c text CHARSET utf8;
    set c = (select GROUP_CONCAT(DISTINCT `char_properties_i18n`.`name` SEPARATOR ' - ' )
             FROM `char_persons_properties`
             join `char_properties_i18n` ON `char_properties_i18n`.`property_id` = `char_persons_properties`.`property_id` and    `char_properties_i18n`.`language_id` =1
             where `char_persons_properties`.`person_id`=id
            );

RETURN c;
END$$

CREATE  FUNCTION `char_get_person_average_payments` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare average double;

    if p_date_from is null and p_date_to is null then
    set average = (SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                       (`char_cases`.`person_id` = p_person_id) and
                       ( (`p`.`status` = 1) or
                           (`p`.`status` = 2)
                       ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and  ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                    )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set average = ( SELECT AVG(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                        (`char_cases`.`person_id` = p_person_id) and
                        ( (`p`.`status` = 1) or (`p`.`status` = 2)) and
                        ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                        ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                    )
                   ) ;


    end if;

RETURN average;
END$$

CREATE  FUNCTION `char_get_person_total_payments` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS DOUBLE BEGIN
  declare sum double;

    if p_date_from is null and p_date_to is null then
    set sum = (SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
               FROM  `char_payments_cases`  AS `p`
               join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
               join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
               WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or
                                 (`p`.`status` = 2)
                              ))) ;

  elseif p_date_from is not null and p_date_to is null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from))
                         )
                   ) ;


    elseif p_date_from is null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ( (`p`.`status` = 1) or (`p`.`status` = 2)) and ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    elseif p_date_from is not null and p_date_to is not null then
    set sum = ( SELECT SUM(`p`.`amount` * `char_payments`.`exchange_rate` )
                FROM  `char_payments_cases`  AS `p`
                join  `char_cases` ON `char_cases`.`id` =`p`.`case_id`
                join  `char_payments` ON ((`char_payments`.`id` =`p`.`payment_id`) and (`char_payments`.`organization_id` =`char_cases`.`organization_id`))
                WHERE (
                              (`char_cases`.`person_id` = p_person_id) and
                              ((`p`.`status` = 1) or (`p`.`status` = 2)) and
                              ((`p`.`date_from` <= p_date_from) and   (`p`.`date_to` >= p_date_from)) and
                              ((`p`.`date_from` <= p_date_to) and   (`p`.`date_to` >= p_date_to))
                         )
                   ) ;


    end if;

RETURN sum;
END$$

CREATE  FUNCTION `char_get_siblings_count` (`p_person_id` INT, `p_via` VARCHAR(15), `gender` TINYINT(4)) RETURNS INT(11) BEGIN
	declare c INTEGER;

      if p_via = 'mother' then

      	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`mother_id` = (
								select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);

	       else
	       	set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`mother_id` = (
							select `b`.`mother_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);


	       end if;


    elseif p_via = 'father' then

     	if gender is null then
				set c = (
							select COUNT(`a`.`id`) FROM `char_persons` AS `a`
							WHERE `a`.`father_id` = (
								select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
							) and `a`.`id` !=  p_person_id
						);
	    else
			set c = (
						select COUNT(`a`.`id`) FROM `char_persons` AS `a`
						WHERE `a`.`father_id` = (
							select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
						) and `a`.`id` !=  p_person_id
					);

	    end if;


    else

     	if gender is null then

		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id
			)) and `a`.`id` !=  p_person_id
		);
	    else


		set c = (
			select COUNT(`a`.`id`) FROM `char_persons` AS `a`
			WHERE (`a`.`father_id` = (
				select `b`.`father_id` FROM `char_persons` AS `b` WHERE `b`.`id` = p_person_id and `b`.`gender`= gender
			)
            OR `a`.`mother_id` = (
				select `c`.`mother_id` FROM `char_persons` AS `c` WHERE `c`.`id` = p_person_id and `b`.`gender`= gender
			)) and `a`.`id` !=  p_person_id
		);

	    end if;
    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_get_voucher_persons_count` (`p_person_id` INT, `p_date_from` DATE, `p_date_to` DATE) RETURNS INT(11) BEGIN
	DECLARE c INTEGER;

 if p_date_from is null and p_date_to is null then
		set c = (
              SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
            );

    elseif p_date_from is not null and p_date_to is null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date` >= p_date_from
            );


    elseif p_date_from is null and p_date_to is not null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id AND `char_vouchers_persons`.`receipt_date`<= p_date_to
              );


    elseif p_date_from is not null and p_date_to is not null then
		set c = (
		          SELECT sum(`char_vouchers`.`value`)
              FROM `char_vouchers_persons`
              JOIN `char_vouchers` on `char_vouchers`.`id` = `char_vouchers_persons`.`voucher_id`
              WHERE `char_vouchers_persons`.`person_id` = p_person_id
              AND `char_vouchers_persons`.`receipt_date` >= p_date_from
              AND `char_vouchers_persons`.`receipt_date` <= p_date_to);
    end if;

RETURN c;
END$$

CREATE  FUNCTION `char_voucher_amount` (`id` INT(1)) RETURNS INT(11) BEGIN
declare c INTEGER;
declare d INTEGER;

		set d = (select COUNT(`char_vouchers_persons`.`person_id`) AS `persons` from `char_vouchers_persons` where (`char_vouchers_persons`.`voucher_id` =id));

		set c = (select (`char_vouchers`.`value` * d) AS `amount` from `char_vouchers` where (`char_vouchers`.`id` =id));
RETURN c;
END$$

CREATE  FUNCTION `get_case_documents` (`id` INT) RETURNS TEXT CHARSET utf8 BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_cases_files`
          where `case_id`=id
		);

RETURN c;
END$$

CREATE  FUNCTION `get_document` (`id` INT) RETURNS TEXT CHARSET latin1 BEGIN
	declare c text;
    set c = (select GROUP_CONCAT(DISTINCT `document_type_id` SEPARATOR ',' )
          FROM `char_persons_documents`
          where `person_id`=id
		);

RETURN c;
END$$

CREATE  FUNCTION `get_organization_payments_cases_amount` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    else

         if date_from is null and date_to is null then
                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select sum(round(`char_payments_cases`. `amount` * `p`.`exchange_rate`,0))
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
                                                           (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
                                                           (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;


RETURN c;
END$$

CREATE  FUNCTION `get_organization_payments_cases_count` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON (`p`.`organization_id` =`char_cases`.`organization_id`)
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
else
    if type = 'sponsors' then

          if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`sponsor_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
else

    if date_from is null and date_to is null then
                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id))
                          );

          elseif date_from is not null and date_to is null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ))
                          );

          elseif date_from is null and date_to is not null then
                   set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date`  <= date_to ))
                          );
          elseif date_from is not null and date_to is not null then

                  set c = ( select count(`char_payments_cases`.`case_id`)
                            FROM  `char_payments` AS `p`
                            join `char_cases` ON ((`p`.`organization_id` =`char_cases`.`organization_id`) and (`char_cases`.`category_id` = category_id))
                            join `char_payments_cases` ON  ((`char_payments_cases`.`status` !=3) and
    (`char_cases`.`id` =`char_payments_cases`.`case_id`) and
    (`p`.`id` =`char_payments_cases`.`payment_id`))
                             WHERE ((`p`.`organization_id` = id) and (`p`.`payment_date` >= date_from ) and (`p`.`payment_date`  <= date_to ))
                          );
          end if;
    end if;
end if;


RETURN c;
END$$

CREATE  FUNCTION `get_organization_vouchers_value` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then
    if type = 'sponsors' then

              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then

                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,null)  != 0)));


               elseif date_from is null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,null) != 0)));

               end if;
else
   if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then

                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));


               elseif date_from is null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));

               end if;

   end if;
else

    if type = 'sponsors' then

              if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,null,category_id) != 0)));

               elseif date_from is not null and date_to is null then

                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,null,category_id)  != 0)));


               elseif date_from is null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id) and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,null,date_to,category_id)  != 0)));

               elseif date_from is not null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`sponsor_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`sponsor_id`,'sponsors' ,date_from,date_to,category_id) != 0)));

               end if;
else

  if date_from is null and date_to is null then
                    set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,null,null) != 0)));

               elseif date_from is not null and date_to is null then

                 set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date` >= date_from )
                                    and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,null,null)  != 0)));


               elseif date_from is null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,null,date_to,null)  != 0)));

               elseif date_from is not null and date_to is not null then

                set c = (select SUM(char_voucher_amount(`char_vouchers`.`id`)) as vouchers_value
                                FROM `char_vouchers`
                                where ((`char_vouchers`.`organization_id` =id)  and (`char_vouchers`.`category_id` = category_id)  and (`char_vouchers`.`voucher_date` >= date_from ) and (`char_vouchers`.`voucher_date`  <= date_to ) and (get_voucher_count(`char_vouchers`.`organization_id`,'organizations' ,date_from,date_to,null) != 0)));

               end if;

    end if;



end if;


RETURN c;
END$$

CREATE  FUNCTION `get_payments_beneficiary_count` (`id` INT(10), `c_via` VARCHAR(45)) RETURNS INT(11) BEGIN
declare c INTEGER;

      if c_via = 'all' then
		      set c = (	select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( `p`.`payment_id`  =  id )	);
      elseif c_via = 'guaranteed' then
			    set c = (select COUNT(`p`.`sponsor_number`)
FROM `char_payments_cases` AS `p`
WHERE ( (`p`.`payment_id` = id)  and ( (`p`.`status` = 1) or (`p`.`status` = 2)) ));
      else
			    set c = (select COUNT(`p`.`sponsor_number`) FROM `char_payments_cases` AS `p` WHERE ( (`p`.`payment_id` = id) and (`p`.`status` = 3)	));
      end if;

RETURN c;
END$$

CREATE  FUNCTION `get_payments_cases_count` (`id` INT(10), `payment_id` INT(10), `sponsor_number` VARCHAR(45)) RETURNS INT(11) BEGIN
declare c INTEGER;

		set c = (
			select COUNT(`p`.`payment_id`) FROM `char_payments_cases` AS `p`
			WHERE (`p`.`sponsor_number` = sponsor_number) and
             (`p`.`case_id` = case_id) and (`p`.`payment_id` !=  payment_id)
		);

RETURN c;
END$$

CREATE  FUNCTION `get_payments_recipient_amount` (`id` INT(10), `p_via` VARCHAR(45), `payment_id` INT(10)) RETURNS INT(11) BEGIN
declare c INTEGER;

        if p_via = 'person' then
		set c = (
            select sum(`p`.`amount`)
FROM `char_payments_cases` AS `p`
join `char_cases` ON `char_cases`.`id` =`p`.`case_id`
join `char_persons` ON `char_persons`.`id` =`char_cases`.`person_id`
WHERE (`char_persons`.`id` = id ) and (`p`.`payment_id` =  payment_id) and (`p`.`status` !=  3)
);

    else
		set c = (
                   select sum(`p`.`amount`)
                   FROM `char_payments_cases` AS `p`
                   WHERE (`p`.`guardian_id` = id) and (`p`.`payment_id` =  payment_id)
                );
    end if;


RETURN c;
END$$

CREATE  FUNCTION `get_voucher_count` (`id` INT(10), `type` VARCHAR(45), `date_from` DATE, `date_to` DATE, `category_id` INT(10)) RETURNS INT(11) BEGIN

declare c INTEGER;

if category_id is null then

 if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)));

     elseif date_from is not null and date_to is null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from )));

     elseif date_from is null and date_to is not null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date`  <= date_to )));

     elseif date_from is not null and date_to is not null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

     end if;

     end if;

else


    if type = 'sponsors' then

     if date_from is null and date_to is null then
         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id) and (`v`.`category_id` = category_id)));

     elseif date_from is not null and date_to is null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));

     elseif date_from is null and date_to is not null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));

     elseif date_from is not null and date_to is not null then

         set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`sponsor_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

     end if;

else

    if date_from is null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id) and (`v`.`category_id` = category_id) ));
           elseif date_from is not null and date_to is null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from )));
           elseif date_from is null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                         FROM  `char_vouchers` AS `v`
                         join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                         WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date`  <= date_to )));
           elseif date_from is not null and date_to is not null then
               set c = ( select COUNT(`char_vouchers_persons`.`person_id`)
                   FROM  `char_vouchers` AS `v`
                   join `char_vouchers_persons` ON (`v`.`id` =`char_vouchers_persons`.`voucher_id`)
                   WHERE ((`v`.`organization_id` = id)  and (`v`.`category_id` = category_id) and (`v`.`voucher_date` >= date_from ) and (`v`.`voucher_date`  <= date_to )));

     end if;

     end if;



end if;


RETURN c;
END$$

CREATE  FUNCTION `normalize_text_ar` (`p_text` TEXT) RETURNS TEXT CHARSET utf8 BEGIN

	DECLARE v_comma VARCHAR(255) DEFAULT '،';
	DECLARE v_semicolon VARCHAR(255) DEFAULT '؛';
	DECLARE v_question VARCHAR(255) DEFAULT '؟';
	DECLARE v_hamza VARCHAR(255) DEFAULT 'ء';
	DECLARE v_alef_madda VARCHAR(255) DEFAULT 'آ';
	DECLARE v_alef_hamza_above VARCHAR(255) DEFAULT 'أ';
	DECLARE v_waw_hamza VARCHAR(255) DEFAULT 'ؤ';
	DECLARE v_alef_hamza_below VARCHAR(255) DEFAULT 'إ';
	DECLARE v_yeh_hamza VARCHAR(255) DEFAULT 'ئ';
	DECLARE v_alef VARCHAR(255) DEFAULT 'ا';
	DECLARE v_beh VARCHAR(255) DEFAULT 'ب';
	DECLARE v_teh_marbuta VARCHAR(255) DEFAULT 'ة';
	DECLARE v_teh VARCHAR(255) DEFAULT 'ت';
	DECLARE v_theh VARCHAR(255) DEFAULT 'ث';
	DECLARE v_jeem VARCHAR(255) DEFAULT 'ج';
	DECLARE v_hah VARCHAR(255) DEFAULT 'ح';
	DECLARE v_khah VARCHAR(255) DEFAULT 'خ';
	DECLARE v_dal VARCHAR(255) DEFAULT 'د';
	DECLARE v_thal VARCHAR(255) DEFAULT 'ذ';
	DECLARE v_reh VARCHAR(255) DEFAULT 'ر';
	DECLARE v_zain VARCHAR(255) DEFAULT 'ز';
	DECLARE v_seen VARCHAR(255) DEFAULT 'س';
	DECLARE v_sheen VARCHAR(255) DEFAULT 'ش';
	DECLARE v_sad VARCHAR(255) DEFAULT 'ص';
	DECLARE v_dad VARCHAR(255) DEFAULT 'ض';
	DECLARE v_tah VARCHAR(255) DEFAULT 'ط';
	DECLARE v_zah VARCHAR(255) DEFAULT 'ظ';
	DECLARE v_ain VARCHAR(255) DEFAULT 'ع';
	DECLARE v_ghain VARCHAR(255) DEFAULT 'غ';
	DECLARE v_tatweel VARCHAR(255) DEFAULT 'ـ';
	DECLARE v_feh VARCHAR(255) DEFAULT 'ف';
	DECLARE v_qaf VARCHAR(255) DEFAULT 'ق';
	DECLARE v_kaf VARCHAR(255) DEFAULT 'ك';
	DECLARE v_lam VARCHAR(255) DEFAULT 'ل';
	DECLARE v_meem VARCHAR(255) DEFAULT 'م';
	DECLARE v_noon VARCHAR(255) DEFAULT 'ن';
	DECLARE v_heh VARCHAR(255) DEFAULT 'ه';
	DECLARE v_waw VARCHAR(255) DEFAULT 'و';
	DECLARE v_alef_maksura VARCHAR(255) DEFAULT 'ى';
	DECLARE v_yeh VARCHAR(255) DEFAULT 'ي';
	DECLARE v_madda_above VARCHAR(255) DEFAULT 'ٓ';
	DECLARE v_hamza_above VARCHAR(255) DEFAULT 'ٔ';
	DECLARE v_hamza_below VARCHAR(255) DEFAULT 'ٕ';
	DECLARE v_zero VARCHAR(255) DEFAULT '٠';
	DECLARE v_one VARCHAR(255) DEFAULT '١';
	DECLARE v_two VARCHAR(255) DEFAULT '٢';
	DECLARE v_three VARCHAR(255) DEFAULT '٣';
	DECLARE v_four VARCHAR(255) DEFAULT '٤';
	DECLARE v_five VARCHAR(255) DEFAULT '٥';
	DECLARE v_six VARCHAR(255) DEFAULT '٦';
	DECLARE v_seven VARCHAR(255) DEFAULT '٧';
	DECLARE v_eight VARCHAR(255) DEFAULT '٨';
	DECLARE v_nine VARCHAR(255) DEFAULT '٩';
	DECLARE v_percent VARCHAR(255) DEFAULT '٪';
	DECLARE v_decimal VARCHAR(255) DEFAULT '٫';
	DECLARE v_thousands VARCHAR(255) DEFAULT '٬';
	DECLARE v_star VARCHAR(255) DEFAULT '٭';
	DECLARE v_mini_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_alef_wasla VARCHAR(255) DEFAULT 'ٱ';
	DECLARE v_full_stop VARCHAR(255) DEFAULT '۔';
	DECLARE v_byte_order_mark VARCHAR(255) DEFAULT '﻿';
	DECLARE v_fathatan VARCHAR(255) DEFAULT 'ً';
	DECLARE v_dammatan VARCHAR(255) DEFAULT 'ٌ';
	DECLARE v_kasratan VARCHAR(255) DEFAULT 'ٍ';
	DECLARE v_fatha VARCHAR(255) DEFAULT 'َ';
	DECLARE v_damma VARCHAR(255) DEFAULT 'ُ';
	DECLARE v_kasra VARCHAR(255) DEFAULT 'ِ';
	DECLARE v_shadda VARCHAR(255) DEFAULT 'ّ';
	DECLARE v_sukun VARCHAR(255) DEFAULT 'ْ';
	DECLARE v_small_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_small_waw VARCHAR(255) DEFAULT 'ۥ';
	DECLARE v_small_yeh VARCHAR(255) DEFAULT 'ۦ';
	DECLARE v_lam_alef VARCHAR(255) DEFAULT 'ﻻ';
	DECLARE v_lam_alef_hamza_above VARCHAR(255) DEFAULT 'ﻷ';
	DECLARE v_lam_alef_hamza_below VARCHAR(255) DEFAULT 'ﻹ';
	DECLARE v_lam_alef_madda_above VARCHAR(255) DEFAULT 'ﻵ';
	DECLARE v_simple_lam_alef VARCHAR(255) DEFAULT 'لَا';
	DECLARE v_simple_lam_alef_hamza_above VARCHAR(255) DEFAULT 'لأ';
	DECLARE v_simple_lam_alef_hamza_below VARCHAR(255) DEFAULT 'لإ';
	DECLARE v_simple_lam_alef_madda_above VARCHAR(255) DEFAULT 'لءَا';

        SET p_text = REPLACE(p_text, v_tatweel, '');

        SET p_text = REPLACE(p_text, v_fathatan, '');
    SET p_text = REPLACE(p_text, v_dammatan, '');
    SET p_text = REPLACE(p_text, v_kasratan, '');
    SET p_text = REPLACE(p_text, v_fatha, '');
    SET p_text = REPLACE(p_text, v_damma, '');
    SET p_text = REPLACE(p_text, v_kasra, '');
    SET p_text = REPLACE(p_text, v_shadda, '');
    SET p_text = REPLACE(p_text, v_sukun, '');

        SET p_text = REPLACE(p_text, v_waw_hamza, v_waw);
    SET p_text = REPLACE(p_text, v_yeh_hamza, v_yeh);
    SET p_text = REPLACE(p_text, v_alef_madda, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_below, v_alef);

        SET p_text = REPLACE(p_text, v_lam_alef, v_simple_lam_alef);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_above, v_simple_lam_alef_hamza_above);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_below, v_simple_lam_alef_hamza_below);
    SET p_text = REPLACE(p_text, v_lam_alef_madda_above, v_simple_lam_alef_madda_above);

        SET p_text = REPLACE(p_text, v_teh_marbuta, v_heh);

RETURN p_text;
END$$

DELIMITER ;
