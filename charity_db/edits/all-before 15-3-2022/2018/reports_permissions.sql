INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.manage','إدارة التقارير المؤرشفة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.create','انشاء تقرير مؤرشف جديد');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.update','تعديل تقرير مؤرشف');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.delete','حذف تقرير مؤرشف');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.view','عرض بيانات تقرير مؤرشف');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reports.export','تصدير التقارير المؤرشفة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.manage','إدارة مرفقات التقارير');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.update','تحديث مرفق تقرير');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.delete','حذف مرفق تقرير');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.view','عرض مرفق تقرير');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.export','تصدير مرفقات التقارير');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '4','sponsorship.reportsCasesDocuments.import','استيراد مرفقات التقارير');
            
        

       
    


