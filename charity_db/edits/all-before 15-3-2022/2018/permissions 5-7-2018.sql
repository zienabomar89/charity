
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(351, 3, 'aid.case.search', 'شاشة إدارة الحالات (البحث في حالات كافة الجمعيات)'),
(352, 4, 'sponsorship.case.search', 'شاشة إدارة الحالات (البحث في حالات كافة الجمعيات)'),
(353, 4, 'sponsorship.case.searchIncomplete', 'شاشة نقص البيانات (البحث في حالات كافة الجمعيات)'),
(354, 3, 'aid.case.searchIncomplete', 'شاشة نقص البيانات (البحث في حالات كافة الجمعيات)'),
(355, 3, 'aid.case.searchAttachment', 'شاشة مرفقات الحالات (البحث في حالات كافة الجمعيات)'),
(356, 4, 'sponsorship.case.searchAttachment', 'شاشة مرفقات الحالات (البحث في حالات كافة الجمعيات)');
