--INSERT INTO `char_organization_locations`(`organization_id`, `location_id`, `created_at`)
SELECT char_cases.organization_id,char_persons.location_id,now()
FROM takaful_db.char_cases
join char_persons on char_cases.person_id = char_persons.id 
where char_persons.location_id is not null
group by char_cases.organization_id,char_persons.location_id


SELECT char_cases.person_id as individual_id,
       char_persons.id_card_number as individual_card,
	   concat(ifnull(char_persons.first_name, ' '), ' ',ifnull(char_persons.second_name, ' '), ' ',
              ifnull(char_persons.third_name, ' '), ' ',ifnull(char_persons.last_name, ' ')) as individual_name,
              char_categories.name as category_name,
       mother.id_card_number as mother_card,
	   concat(ifnull(mother.first_name, ' '), ' ',ifnull(mother.second_name, ' '), ' ',
              ifnull(mother.third_name, ' '), ' ',ifnull(mother.last_name, ' ')) as mother_name,
       father.id_card_number as father_card,
	   concat(ifnull(father.first_name, ' '), ' ',ifnull(father.second_name, ' '), ' ',
              ifnull(father.third_name, ' '), ' ',ifnull(father.last_name, ' ')) as father_name,
       char_guardians.guardian_id,
       guardian.id_card_number as guardian_card ,
       concat(ifnull(guardian.first_name, ' '), ' ',ifnull(guardian.second_name, ' '), ' ',
              ifnull(guardian.third_name, ' '), ' ',ifnull(guardian.last_name, ' ')) as guardian_name,
              char_organizations.name as org_name,char_kinship_i18n.name as kinship_name,
              char_guardians.status=1
FROM takaful_db.char_cases
join char_categories on char_categories.id = char_cases.category_id  and char_categories.type = 1
join char_organizations on char_organizations.id = char_cases.organization_id
join char_persons on char_cases.person_id = char_persons.id
left join char_persons father on father.id = char_persons.father_id
left join char_persons mother on mother.id = char_persons.mother_id
left join char_guardians  on char_guardians.individual_id = char_persons.id and char_cases.organization_id = char_guardians.organization_id
left join char_persons guardian on char_guardians.guardian_id = guardian.id
left join char_kinship_i18n on char_kinship_i18n.kinship_id = char_guardians.kinship_id
group by char_cases.id,char_cases.organization_id ,char_guardians.guardian_id
-- where char_cases.id in (select distinct case_id from char_payments_cases )


SELECT char_users.username , char_users.username as password, job_title,char_organizations.name as organization_name FROM takaful_db.char_users
join takaful_db.char_organizations on takaful_db.char_organizations.id = char_users.organization_id;

SELECT char_payments_cases.guardian_id,guardian.id_card_number as guardian_card ,
       char_cases.person_id as individual_id,char_persons.id_card_number as individual_card ,char_persons.father_id,
	   concat(ifnull(char_persons.first_name, ' '), ' ',ifnull(char_persons.second_name, ' '), ' ',
              ifnull(char_persons.third_name, ' '), ' ',ifnull(char_persons.last_name, ' ')) as individual_name,
       concat(ifnull(guardian.first_name, ' '), ' ',ifnull(guardian.second_name, ' '), ' ',
              ifnull(guardian.third_name, ' '), ' ',ifnull(guardian.last_name, ' ')) as guardian_name,
              char_organizations.name as org_name,
              sponsor.name as sponsor_name
FROM takaful_db.char_payments_cases
join char_payments on char_payments.id = char_payments_cases.payment_id
join char_organizations on char_organizations.id = char_payments.organization_id
join char_organizations sponsor on sponsor.id = char_payments.sponsor_id
join char_cases on char_cases.id = char_payments_cases.case_id and char_cases.organization_id = char_payments.organization_id
join char_persons on char_cases.person_id = char_persons.id
join char_persons guardian on char_payments_cases.guardian_id = guardian.id
where char_cases.person_id not in (select individual_id from char_guardians
where char_guardians.organization_id = char_cases.organization_id ) and char_payments_cases.guardian_id is not null
group by char_cases.person_id,char_payments_cases.guardian_id;


--INSERT INTO `char_guardians`(`guardian_id`, `individual_id`, `kinship_id`, `organization_id`, `status`, `created_at`)
SELECT char_payments_cases.guardian_id , char_cases.person_id as individual_id,3 as kinship_id, char_cases.organization_id, 1 as status, now() as created_at
FROM takaful_db.char_payments_cases
join char_payments on char_payments.id = char_payments_cases.payment_id
join char_cases on char_cases.id = char_payments_cases.case_id and char_cases.organization_id = char_payments.organization_id
join char_persons on char_cases.person_id = char_persons.id
join char_persons as guardian on char_payments_cases.guardian_id = guardian.id and char_persons.father_id = guardian.father_id
where char_cases.person_id not in (select individual_id from char_guardians
where char_guardians.organization_id = char_cases.organization_id ) and char_payments_cases.guardian_id is not null
group by char_cases.person_id,char_payments_cases.guardian_id;


--INSERT INTO `char_guardians`(`guardian_id`, `individual_id`, `kinship_id`, `organization_id`, `status`, `created_at`)
SELECT char_payments_cases.guardian_id , char_cases.person_id as individual_id,12 as kinship_id, char_cases.organization_id, 1 as status, now() as created_at
FROM takaful_db.char_payments_cases
join char_payments on char_payments.id = char_payments_cases.payment_id
join char_cases on char_cases.id = char_payments_cases.case_id and char_cases.organization_id = char_payments.organization_id
join char_persons on char_cases.person_id = char_persons.id and char_cases.person_id = char_payments_cases.guardian_id
where char_cases.person_id not in (select individual_id from char_guardians
where char_guardians.organization_id = char_cases.organization_id ) and char_payments_cases.guardian_id is not null
group by char_cases.person_id,char_payments_cases.guardian_id;

--INSERT INTO `char_guardians`(`guardian_id`, `individual_id`, `kinship_id`, `organization_id`, `status`, `created_at`)
SELECT char_payments_cases.guardian_id , char_cases.person_id as individual_id,5 as kinship_id, char_cases.organization_id, 1 as status, now() as created_at
FROM takaful_db.char_payments_cases
join char_payments on char_payments.id = char_payments_cases.payment_id
join char_cases on char_cases.id = char_payments_cases.case_id and char_cases.organization_id = char_payments.organization_id
join char_persons on char_cases.person_id = char_persons.id and char_persons.mother_id = char_payments_cases.guardian_id
where char_cases.person_id not in (select individual_id from char_guardians
where char_guardians.organization_id = char_cases.organization_id ) and char_payments_cases.guardian_id is not null
group by char_cases.person_id,char_payments_cases.guardian_id

