ALTER TABLE `char_guardians` ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`;
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '1', 'auth.BackupVersions.create', 'إنشاء النسخة الاحتياطية');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '1', 'auth.BackupVersions.manage', 'إدارة النسخ الاحتياطية');



CREATE TABLE `char_backup_versions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `char_backup_versions`
  ADD PRIMARY KEY (`id`),
 ADD KEY `fk_backup_versions_userId_idx` (`user_id`),
 ADD CONSTRAINT `fk_backup_versions_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
;


ALTER TABLE `char_backup_versions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;