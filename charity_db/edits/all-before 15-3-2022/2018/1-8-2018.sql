UPDATE `char_sponsorships`, `char_organizations`
SET    `char_sponsorships`.`sponsor_id` = `char_organizations`.`old`
WHERE  `char_sponsorships`.`sponsor_id` = `char_organizations`.`id`;

UPDATE `char_vouchers`, `char_organizations`
SET    `char_vouchers`.`sponsor_id` = `char_organizations`.`old`
WHERE  `char_vouchers`.`sponsor_id` = `char_organizations`.`id`;

UPDATE `char_payments`,   `char_organizations`
SET    `char_payments`.`sponsor_id` = `char_organizations`.`old`
WHERE  `char_payments`.`sponsor_id` = `char_organizations`.`id`;

UPDATE   `char_sponsorship_cases`,  `char_organizations`
SET      `char_sponsorship_cases`.`sponsor_id` = `char_organizations`.`old`
WHERE    `char_sponsorship_cases`.`sponsor_id`= `char_organizations`.`id`;

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (NULL, '8', 'org.organizations.connectSponsor', 'ربط الجمعيات بالجهات الممولة / الكافلة');

