DELIMITER $$
CREATE  PROCEDURE `char_banks_save`(INOUT pId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_banks`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_banks` (`name`) VALUES (pName);
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_banks` SET `name` = pName WHERE `id` = pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_branches_save`(INOUT pId INT, IN pBankId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `id` INTO pId
		FROM `char_branches`
		WHERE normalize_text_ar(`name`) = normalize_text_ar(pName)
        AND `bank_id` = pBankId;
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_branches` (`bank_id`, `name`) VALUES (pBankId, pName);
        SET pId = LAST_INSERT_ID();
	ELSE
		UPDATE `char_branches` SET `name` = pName, `bank_id` = pBankId WHERE `id` = pId;
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_building_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `building_status_id` INTO pId
		FROM `char_building_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_building_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_building_status_i18n` (`building_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_building_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_building_status_i18n` (`building_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_death_causes_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `death_cause_id` INTO pId
		FROM `char_death_causes_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_death_causes` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_death_causes_i18n` (`death_cause_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
        
    ELSE
		UPDATE `char_death_causes` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_death_causes_i18n` (`death_cause_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_diseases_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `disease_id` INTO pId
		FROM `char_diseases_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_diseases` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_diseases_i18n` (`disease_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_diseases` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_diseases_i18n` (`disease_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_document_types_save`(INOUT pId INT, IN pExpiry FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
 
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `document_type_id` INTO pId
		FROM `char_document_types_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_document_types` (`expiry`) VALUES (pExpiry);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_document_types_i18n` (`document_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_document_types` SET `expiry` = pExpiry WHERE `id` = pId;
        
		REPLACE INTO `char_document_types_i18n` (`document_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;

	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_edu_authorities_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_authority_id` INTO pId
		FROM `char_edu_authorities_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_authorities` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_authorities_i18n` (`edu_authority_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_authorities` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_authorities_i18n` (`edu_authority_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_edu_degrees_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_degree_id` INTO pId
		FROM `char_edu_degrees_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_degrees` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_degrees_i18n` (`edu_degree_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_degrees` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_degrees_i18n` (`edu_degree_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_edu_stages_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `edu_stage_id` INTO pId
		FROM `char_edu_stages_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_edu_stages` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_edu_stages_i18n` (`edu_stage_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_edu_stages` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_edu_stages_i18n` (`edu_stage_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_furniture_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `furniture_status_id` INTO pId
		FROM `char_furniture_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_furniture_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_furniture_status_i18n` (`furniture_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_furniture_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_furniture_status_i18n` (`furniture_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_habitable_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `habitable_status_id` INTO pId
		FROM `char_habitable_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_habitable_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_habitable_status_i18n` (`habitable_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_habitable_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_habitable_status_i18n` (`habitable_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_house_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `house_status_id` INTO pId
		FROM `char_house_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_house_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_house_status_i18n` (`house_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_house_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_house_status_i18n` (`house_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_kinship_save`(INOUT pId INT, IN pLevel FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `death_cause_id` INTO pId
		FROM `char_death_causes_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_kinship` (`level`) VALUES (pLevel);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_kinship_i18n` (`kinship_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_kinship` SET `level` = pLevel WHERE `id` = pId;
        
		REPLACE INTO `char_kinship_i18n` (`kinship_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_locations_save`(INOUT pId INT, IN pType INT, IN pParentId INT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `char_locations_i18n`.`location_id` INTO pId
		FROM `char_locations_i18n`
        INNER JOIN `char_locations` ON `char_locations_i18n`.`location_id` = `char_locations`.`id`
		WHERE `char_locations_i18n`.`language_id` = pLanguageId
        AND `char_locations`.`location_type` = pType
        AND normalize_text_ar(`char_locations_i18n`.`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_locations` (`location_type`, `parent_id`, `created_at`) VALUES (pType, pParentId, NOW());
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_locations_i18n` (`location_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		-- UPDATE `char_locations` SET `location_type` = pType, `parent_id` = pParentId, `updated_at` = NOW() WHERE `id` = pId;
        
		REPLACE INTO `char_locations_i18n` (`location_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_marital_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `marital_status_id` INTO pId
		FROM `char_marital_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_marital_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();

        INSERT INTO `char_marital_status_i18n` (`marital_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_marital_status` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_marital_status_i18n` (`marital_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_persons_save`(INOUT person_id INT, p_first_name VARCHAR(255), p_second_name VARCHAR(255), p_third_name VARCHAR(255),
	p_last_name VARCHAR(255), prev_family_name VARCHAR(255), p_id_card_number VARCHAR(255), gender INT, 
    marital_status_id INT, birthday DATE, birth_place INT, nationality INT, death_date DATE, 
    death_cause_id INT, father_id INT, mother_id INT, spouses INT, refugee INT, unrwa_card_number VARCHAR(255), 
    country INT, governarate INT, city INT, location_id INT, street_address VARCHAR(255), monthly_income FLOAT,
    p_fullname VARCHAR(255),
    en_first_name VARCHAR(255), en_second_name VARCHAR(255), en_third_name VARCHAR(255), en_last_name VARCHAR(255),
    bank_id INT, account_number VARCHAR(255), account_owner VARCHAR(255), branch_id INT,
    study INT, p_type INT, authority INT, stage INT, specialization VARCHAR(255), degree INT, grade VARCHAR(255), 
    p_year VARCHAR(255), school VARCHAR(255), p_level VARCHAR(255), points FLOAT,
    p_condition INT, details VARCHAR(255), disease_id INT,
    prayer INT, quran_center INT, quran_parts VARCHAR(255), quran_chapters VARCHAR(255), prayer_reason VARCHAR(255), quran_reason VARCHAR(255),
    working INT, can_work INT, work_status_id INT, work_reason_id INT, work_job_id INT, work_wage_id INT, work_location VARCHAR(255),
    property_type_id INT, rent_value FLOAT, roof_material_id INT, residence_condition INT,
	indoor_condition INT, habitable INT, house_condition INT, rooms INT, area FLOAT,
    mobile1 VARCHAR(255), mobile2 VARCHAR(255), phone VARCHAR(255))
BEGIN
    START TRANSACTION;
    
    SET p_id_card_number = SUBSTRING(p_id_card_number, 1, 9);
    
    IF person_id IS NULL THEN
		
        IF p_id_card_number IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons` WHERE `id_card_number` = p_id_card_number
            LIMIT 1;
		
        ELSEIF p_fullname IS NOT NULL THEN
			SELECT `id` INTO person_id FROM `char_persons`
			WHERE `fullname` = p_fullname
            LIMIT 1;
		END IF;
        
	END IF;
    
    IF person_id IS NULL THEN
		INSERT INTO `char_persons`
		(`first_name`, `second_name`, `third_name`, `last_name`, `prev_family_name`, `id_card_number`, `gender`,
		`marital_status_id`, `birthday`, `birth_place`, `nationality`, `death_date`, `death_cause_id`, `father_id`,
		`mother_id`, `spouses`, `refugee`, `unrwa_card_number`, `country`, `governarate`, `city`, `location_id`,
		`street_address`, `monthly_income`, `created_at`, `fullname`)
		VALUES
		(p_first_name, p_second_name, p_third_name, p_last_name, prev_family_name, p_id_card_number, gender, 
		marital_status_id, birthday, birth_place, nationality, death_date, death_cause_id, father_id, 
		mother_id, spouses, refugee, unrwa_card_number, country, governarate, city, location_id, 
		street_address, monthly_income, NOW(), p_fullname);
        
        SET person_id = LAST_INSERT_ID();
	END IF;
    
    IF en_first_name IS NOT NULL THEN
		REPLACE  INTO `char_persons_i18n`
		(`person_id`, `language_id`, `first_name`, `second_name`, `third_name`, `last_name`)
		VALUES
		(person_id, 2, en_first_name, en_second_name, en_third_name, en_last_name);
	END IF;

	IF bank_id IS NOT NULL AND branch_id IS NOT NULL THEN
		REPLACE  INTO `char_persons_banks`
		(`person_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`)
		VALUES
		(person_id, bank_id, account_number, account_owner, branch_id);
    END IF;

	IF mobile1 IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'primary_mobile', mobile1);
	END IF;
    IF mobile2 IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'secondery_mobile', mobile2);
	END IF;
    IF phone IS NOT NULL THEN
		REPLACE  INTO `char_persons_contact`
		(`person_id`, `contact_type`, `contact_value`)
		VALUES
		(person_id, 'phone', phone);
	END IF;

	IF authority IS NOT NULL OR stage IS NOT NULL OR degree IS NOT NULL OR grade IS NOT NULL
    OR specialization IS NOT NULL OR p_type IS NOT NULL THEN
		REPLACE  INTO `char_persons_education`
		(`person_id`, `study`, `type`, `authority`, `stage`, `specialization`, `degree`, `grade`,
		`year`, `school`, `level`, `points`)
		VALUES
		(person_id, study, p_type, authority, stage, specialization,
		degree, grade, p_year, school, p_level, points);
    END IF;

	IF p_condition IS NOT NULL OR disease_id IS NOT NULL OR details IS NOT NULL THEN
		REPLACE  INTO `char_persons_health`
		(`person_id`, `condition`, `details`, `disease_id`)
		VALUES
		(person_id, p_condition, details, disease_id);
    END IF;

	IF prayer IS NOT NULL OR quran_parts IS NOT NULL OR quran_chapters IS NOT NULL OR prayer_reason IS NOT NULL THEN
		REPLACE  INTO `char_persons_islamic_commitment`
		(`person_id`, `prayer`, `quran_center`, `quran_parts`, `quran_chapters`, `prayer_reason`, `quran_reason`)
		VALUES
		(person_id, prayer, quran_center, quran_parts, quran_chapters, prayer_reason, quran_reason);
	END IF;
    
    IF working IS NOT NULL OR work_status_id IS NOT NULL OR work_job_id IS NOT NULL OR work_location IS NOT NULL THEN
		REPLACE  INTO `char_persons_work`
		(`person_id`, `working`, `can_work`, `work_status_id`, `work_reason_id`, `work_job_id`, `work_wage_id`, `work_location`)
		VALUES
		(person_id, working, can_work, work_status_id, work_reason_id, work_job_id, work_wage_id, work_location);
	END IF;
    
    IF property_type_id IS NOT NULL OR roof_material_id IS NOT NULL OR indoor_condition IS NOT NULL OR habitable IS NOT NULL
    OR house_condition IS NOT NULL THEN
		REPLACE  INTO `char_residence`
		(`person_id`, `property_type_id`, `rent_value`, `roof_material_id`, `residence_condition`, `indoor_condition`,
		`habitable`, `house_condition`, `rooms`, `area`)
		VALUES
		(person_id, property_type_id, rent_value, roof_material_id, residence_condition,
		indoor_condition, habitable, house_condition, rooms, area);
    END IF;
    
	COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_properties_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	DECLARE vNew BOOLEAN DEFAULT false;
    DECLARE vName VARCHAR(255);
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `property_id` INTO pId
		FROM `char_properties_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_properties` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_properties_i18n` (`property_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_properties` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_properties_i18n` (`property_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_property_types_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN

    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `property_type_id` INTO pId
		FROM `char_property_types_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_property_types` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_property_types_i18n` (`property_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_property_types` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_property_types_i18n` (`property_type_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_roof_materials_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `roof_material_id` INTO pId
		FROM `char_roof_materials_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_roof_materials` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_roof_materials_i18n` (`roof_material_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_roof_materials` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_roof_materials_i18n` (`roof_material_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_work_jobs_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_job_id` INTO pId
		FROM `char_work_jobs_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_jobs` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_jobs_i18n` (`work_job_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_jobs` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_work_jobs_i18n` (`work_job_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_work_reasons_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
    
    IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_reason_id` INTO pId
		FROM `char_work_reasons_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_reasons` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_reasons_i18n` (`work_reason_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_reasons` SET `weight` = pWeight WHERE `id` = pId;
        
		REPLACE INTO `char_work_reasons_i18n` (`work_reason_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `char_work_status_save`(INOUT pId INT, IN pWeight FLOAT, IN pLanguageId INT, IN pName VARCHAR(255), IN in_transaction BOOLEAN)
BEGIN
	IF in_transaction = false THEN
		START TRANSACTION;
    END IF;
    
    IF pId IS NULL THEN
		SELECT `work_status_id` INTO pId
		FROM `char_work_status_i18n`
		WHERE `language_id` = pLanguageId
        AND normalize_text_ar(`name`) = normalize_text_ar(pName);
	END IF;
    
    IF pId IS NULL THEN
		INSERT INTO `char_work_status` (`weight`) VALUES (pWeight);
        SET pId = LAST_INSERT_ID();
        
        INSERT INTO `char_work_status_i18n` (`work_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
	ELSE
		UPDATE `char_work_status` SET `weight` = pWeight WHERE `id` = pId;
        
		INSERT INTO `char_work_status_i18n` (`work_status_id`, `language_id`, `name`)
		VALUES (pId, pLanguageId, pName);
    END IF;
    
	IF in_transaction = false THEN
		COMMIT;
	END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `fix_duplicated_id`(IN p_postfix VARCHAR(1))
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT f2.f_id from test.data_father f1 inner join test.data_father f2
	ON f1.f_sin = f2.f_sin AND f1.f_id <> f2.f_id AND f1.fullname <> f2.fullname
	WHERE f1.f_id > f2.f_id
    AND f1.f_sin <> 0 AND f1.f_sin is not Null;
    
	DECLARE cur2 CURSOR FOR SELECT m2.m_id from test.data_mother m1 inner join test.data_mother m2
	ON m1.m_sin = m2.m_sin AND m1.m_id <> m2.m_id AND m1.fullname <> m2.fullname
	WHERE m1.m_id > m2.m_id
    AND m1.m_sin <> 0 AND m1.m_sin is not Null;
    
    DECLARE cur3 CURSOR FOR SELECT c2.id from test.data_case c1 inner join test.data_case c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname <> c2.fullname
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur4 CURSOR FOR SELECT g2.g_id from test.data_guardian g1 inner join test.data_guardian g2
	ON g1.g_sin = g2.g_sin AND g1.g_id <> g2.g_id AND g1.fullname <> g2.fullname
	WHERE g1.g_id > g2.g_id
    AND g1.g_sin <> 0 AND g1.g_sin is not Null;
    
    DECLARE cur5 CURSOR FOR SELECT c2.id, c2.fullname from test.data_case c1 inner join test.data_case c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname
	WHERE c1.fullname > c2.fullname
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		SELECT v_id;
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    /*OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        UPDATE test.data_father SET f_sin = concat(substr(f_sin, 1, length(f_sin) - 1), p_postfix) WHERE f_id = v_id;
        
	END LOOP;
    CLOSE cur;
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        UPDATE test.data_mother SET m_sin = concat(substr(m_sin, 1, length(m_sin) - 1), p_postfix) WHERE m_id = v_id;
        
	END LOOP;
    CLOSE cur2;
    
    SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id;
        IF done THEN
			LEAVE read_loop3;
		END IF;
        
        UPDATE test.data_case SET sin = concat(substr(sin, 1, length(sin) - 1), p_postfix) WHERE id = v_id;
        
	END LOOP;
    CLOSE cur3;
    
    SET done = false;
    OPEN cur4;
    read_loop4: LOOP
		FETCH cur4 INTO v_id;
        IF done THEN
			LEAVE read_loop4;
		END IF;
        
        UPDATE test.data_guardian SET g_sin = concat(substr(g_sin, 1, length(g_sin) - 1), p_postfix) WHERE id = v_id;
        
	END LOOP;
    CLOSE cur4;*/
    
    SET done = false;
    OPEN cur5;
    read_loop5: LOOP
		FETCH cur5 INTO v_id, v_name;
        IF done THEN
			LEAVE read_loop5;
		END IF;
        
        UPDATE test.data_case SET sin = concat(substr(sin, 1, length(sin) - 1), p_postfix)
        WHERE id = v_id AND fullname = v_name;
        
	END LOOP;
    CLOSE cur5;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_aid_cases`(IN p_offset INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE id INT;
        
	DECLARE v_ID1 INT;
    DECLARE v_id VARCHAR(255);
    DECLARE v_sin VARCHAR(255);
    DECLARE v_name1 VARCHAR(255);
    DECLARE v_name2 VARCHAR(255);
    DECLARE v_name3 VARCHAR(255);
    DECLARE v_name4 VARCHAR(255);
    DECLARE v_ename1 VARCHAR(255);
    DECLARE v_ename2 VARCHAR(255);
    DECLARE v_ename3 VARCHAR(255);
    DECLARE v_ename4 VARCHAR(255);
    DECLARE v_bd DATE;
    DECLARE v_bp VARCHAR(255);
    DECLARE v_gender INT;
    DECLARE v_national INT;
    DECLARE v_wives INT;
    DECLARE v_ms INT;
    DECLARE v_refugee INT;
    DECLARE v_unrwa_no VARCHAR(255);
    DECLARE v_edu_degree INT;
    DECLARE v_edu_major VARCHAR(255);
    DECLARE v_family_count INT;
    DECLARE v_family_single INT;
    DECLARE v_males INT;
    DECLARE v_females INT;
    DECLARE v_country INT;
    DECLARE v_sp INT;
    DECLARE v_city INT;
    DECLARE v_street VARCHAR(255);
    DECLARE v_local INT;
    DECLARE v_mousqe VARCHAR(255);
    DECLARE v_mobile1 VARCHAR(255);
    DECLARE v_mobile2 VARCHAR(255);
    DECLARE v_phone VARCHAR(255);
    DECLARE v_bank INT;
    DECLARE v_bbranch INT;
    DECLARE v_acc_holder VARCHAR(255);
    DECLARE v_acc_no VARCHAR(255);
    DECLARE v_working INT;
    DECLARE v_can_work INT;
    DECLARE v_unwork_reason INT;
    DECLARE v_work_status INT;
    DECLARE v_job INT;
    DECLARE v_workplace VARCHAR(255);
    DECLARE v_income FLOAT;
    DECLARE v_property_type INT;
    DECLARE v_roof INT;
    DECLARE v_res_cond INT;
    DECLARE v_indoor_cond INT;
    DECLARE v_rooms INT;
    DECLARE v_house_cond INT;
    DECLARE v_habitable INT;
    DECLARE v_area FLOAT;
    DECLARE v_rent FLOAT;
    DECLARE v_org_id INT;
    DECLARE v_org_name VARCHAR(255);
    DECLARE v_fullname VARCHAR(255);

    
    DECLARE cur CURSOR FOR
    SELECT `data`.`ID1`,
    `data`.`id`,
    `data`.`sin`,
    `data`.`name1`,
    `data`.`name2`,
    `data`.`name3`,
    `data`.`name4`,
    `data`.`ename1`,
    `data`.`ename2`,
    `data`.`ename3`,
    `data`.`ename4`,
    `data`.`bd`,
    `data`.`bp`,
    `data`.`gender`,
    `data`.`national`,
    `data`.`wives`,
    `data`.`ms`,
    `data`.`refugee`,
    `data`.`unrwa_no`,
    `data`.`edu_degree`,
    `data`.`edu_major`,
    `data`.`family_count`,
    `data`.`family_single`,
    `data`.`males`,
    `data`.`females`,
    `data`.`country`,
    `data`.`sp`,
    `data`.`city`,
    `data`.`street`,
    `data`.`local`,
    `data`.`mousqe`,
    `data`.`mobile1`,
    `data`.`mobile2`,
    `data`.`phone`,
    `data`.`bank`,
    `data`.`bbranch`,
    `data`.`acc_holder`,
    `data`.`acc_no`,
    `data`.`working`,
    `data`.`can_work`,
    `data`.`unwork_reason`,
    `data`.`work_status`,
    `data`.`job`,
    `data`.`workplace`,
    `data`.`income`,
    `data`.`property_type`,
    `data`.`roof`,
    `data`.`res_cond`,
    `data`.`indoor_cond`,
    `data`.`rooms`,
    `data`.`house_cond`,
    `data`.`habitable`,
    `data`.`area`,
    `data`.`rent`,
    `data`.`org_id`,
    `data`.`org_name`,
    `data`.`fullname`
FROM `test`.`data`
WHERE `data`.`ID1` >= p_offset;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_ID1,
		v_id,
		v_sin,
		v_name1,
		v_name2,
		v_name3,
		v_name4,
		v_ename1,
		v_ename2,
		v_ename3,
		v_ename4,
		v_bd,
		v_bp,
		v_gender,
		v_national,
		v_wives,
		v_ms,
		v_refugee,
		v_unrwa_no,
		v_edu_degree,
		v_edu_major,
		v_family_count,
		v_family_single,
		v_males,
		v_females,
		v_country,
		v_sp,
		v_city,
		v_street,
		v_local,
		v_mousqe,
		v_mobile1,
		v_mobile2,
		v_phone,
		v_bank,
		v_bbranch,
		v_acc_holder,
		v_acc_no,
		v_working,
		v_can_work,
		v_unwork_reason,
		v_work_status,
		v_job,
		v_workplace,
		v_income,
		v_property_type,
		v_roof,
		v_res_cond,
		v_indoor_cond,
		v_rooms,
		v_house_cond,
		v_habitable,
		v_area,
		v_rent,
		v_org_id,
		v_org_name,
		v_fullname;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET id = null;
        CALL `char_persons_save`(id,
		v_name1, v_name2, v_name3, v_name4, null,
		v_sin,
		v_gender, v_ms, v_bd, v_bp, v_national,
		null, null,
		null, null, v_wives,
		v_refugee, v_unrwa_no, 
		v_country, v_sp, v_city, v_local, concat(v_street, ' -- ', v_mousqe),
		v_income, v_fullname,
		v_ename1, v_ename2, v_ename3, v_ename4,
		v_bank, v_acc_no, v_acc_holder, v_bbranch,
		null, null, null, null, v_edu_major, v_edu_degree, null,  null, null, null, null,
		null, null, null,
		null, null, null, null, null, null,
		v_working, v_can_work, v_work_status, v_unwork_reason, v_job, null, v_workplace,
		v_property_type, v_rent, v_roof, v_res_cond, v_indoor_cond, v_habitable, v_house_cond, v_rooms, v_area,
		v_mobile1, v_mobile2, v_phone);
		
        INSERT INTO `char_cases`
		(`person_id`,
		`organization_id`,
		`category_id`,
		`status`,
		`rank`,
		`user_id`,
		`created_at`)
		VALUES
		(id,
		v_org_id,
		5,
		0,
		0,
		1,
		NOW());
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_banks_branches`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE bank_id, branch_id INT;
    DECLARE bank_name, branch_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `bank`, `branch` FROM
    (SELECT DISTINCT `bank`, `bbranch` as `branch` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_bank` as `bank`, `g_branch` as `branch` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `bank`, `branch` FROM `test`.`data_case`) AS banks_branches
    WHERE (`bank` NOT REGEXP '^[0-9]+$' AND `bank` IS NOT NULL)
    OR (`branch` NOT REGEXP '^[0-9]+$' AND `branch` IS NOT NULL);
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_banks`;
		TRUNCATE TABLE `char_branches`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO bank_name, branch_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET bank_id = NULL;
        IF bank_name IS NOT NULL AND bank_name NOT REGEXP '^[0-9]+$' THEN
			SET bank_name = normalize_text_ar(bank_name);
            
            CALL char_banks_save(bank_id, bank_name, false);
            
			IF bank_id IS NOT NULL THEN
				UPDATE `test`.`data` SET `bank` = bank_id WHERE  normalize_text_ar(`bank`) = bank_name;
				UPDATE `test`.`data_guardian` SET `g_bank` = bank_id WHERE  normalize_text_ar(`g_bank`) = bank_name;
				UPDATE `test`.`data_case` SET `bank` = bank_id WHERE  normalize_text_ar(`bank`) = bank_name;
            END IF;
		ELSEIF bank_name REGEXP '^[0-9]+$' THEN
			SET bank_id = bank_name;
		END IF;
        
        SET branch_id = NULL;
        IF bank_id IS NOT NULL AND branch_name IS NOT NULL AND branch_name NOT REGEXP '^[0-9]+$' THEN
			SET branch_name = normalize_text_ar(branch_name);
            
			CALL char_branches_save(branch_id, bank_id, branch_name, false);
			IF branch_id IS NOT NULL THEN
				UPDATE `test`.`data` SET `bbranch` = branch_id WHERE  normalize_text_ar(`bbranch`) = branch_name;
				UPDATE `test`.`data_guardian` SET `g_branch` = branch_id WHERE  normalize_text_ar(`g_branch`) = branch_name;
				UPDATE `test`.`data_case` SET `branch` = branch_id WHERE  normalize_text_ar(`branch`) = branch_name;
            END IF;
        END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_building_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `house_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_house_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `house_cond` AS `status` FROM `test`.`data_case`) AS building_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_building_status`;
		TRUNCATE TABLE `char_building_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_building_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `house_cond` = v_id WHERE normalize_text_ar(`house_cond`) = v_name;
            UPDATE `test`.`data_case` SET `house_cond` = v_id WHERE normalize_text_ar(`house_cond`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_house_cond` = v_id WHERE normalize_text_ar(`g_house_cond`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_death_causes`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `cause` FROM
    (SELECT DISTINCT `f_death_reason` as `cause` FROM `test`.`data_father`
    UNION
    SELECT DISTINCT `m_death_reason` as `cause` FROM `test`.`data_mother`) AS death_causes
    WHERE `cause` IS NOT NULL AND `cause` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_death_causes`;
		TRUNCATE TABLE `char_death_causes_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_death_causes_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_father` SET `f_death_reason` = v_id WHERE normalize_text_ar(`f_death_reason`) = v_name;
            UPDATE `test`.`data_mother` SET `m_death_reason` = v_id WHERE normalize_text_ar(`m_death_reason`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_diseases`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `disease` FROM
    (SELECT DISTINCT `m_disease` as `disease` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `disease` FROM `test`.`data_case`) AS diseases
    WHERE `disease` IS NOT NULL AND `disease` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_diseases`;
		TRUNCATE TABLE `char_diseases_i18n`;
		SET foreign_key_checks = 1;
    END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL AND v_name NOT REGEXP '^[0-9]+$' THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_diseases_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_mother` SET `m_disease` = v_id WHERE normalize_text_ar(`m_disease`) = v_name;
            UPDATE `test`.`data_case` SET `disease` = v_id WHERE normalize_text_ar(`disease`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_edu_authorities`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `authority` FROM `test`.`data_case`
    WHERE `authority` IS NOT NULL AND `authority` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_authorities`;
		TRUNCATE TABLE `char_edu_authorities_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_authorities_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `authority` = v_id WHERE normalize_text_ar(`authority`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_edu_degrees`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `degree` FROM
    (SELECT DISTINCT `edu_degree` AS `degree` FROM `test`.`data`
    UNION
    SELECT DISTINCT `stage` AS `degree` FROM `test`.`data_case`
    UNION
    SELECT DISTINCT `m_degree` AS `degree` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `g_degree` AS `degree` FROM `test`.`data_guardian`) AS degrees
    WHERE `degree` IS NOT NULL AND `degree` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_degrees`;
		TRUNCATE TABLE `char_edu_degrees_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL AND v_name NOT REGEXP '[0-9]+' THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_degrees_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `stage` = v_id WHERE normalize_text_ar(`stage`) = v_name;
            UPDATE `test`.`data` SET `edu_degree` = v_id WHERE normalize_text_ar(`edu_degree`) = v_name;
            UPDATE `test`.`data_mother` SET `m_degree` = v_id WHERE normalize_text_ar(`m_degree`) = v_name;
            UPDATE `test`.`data_guardian` SET `g_degree` = v_id WHERE normalize_text_ar(`g_degree`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_edu_stages`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `grade` FROM `test`.`data_case`
    WHERE `grade` IS NOT NULL AND `grade` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_edu_stages`;
		TRUNCATE TABLE `char_edu_stages_i18n`;
		SET foreign_key_checks = 1;
    END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_edu_stages_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_case` SET `grade` = v_id WHERE normalize_text_ar(`grade`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_furniture_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `indoor_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_indoor_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `indoor_cond` AS `status` FROM `test`.`data_case`) AS furniture_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_furniture_status`;
		TRUNCATE TABLE `char_furniture_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_furniture_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `indoor_cond` = v_id WHERE normalize_text_ar(`indoor_cond`) = v_name;
            UPDATE `test`.`data_case` SET `indoor_cond` = v_id WHERE normalize_text_ar(`indoor_cond`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_indoor_cond` = v_id WHERE normalize_text_ar(`g_indoor_cond`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_habitable_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `habitable` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_habitable` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `habitable` AS `status` FROM `test`.`data_case`) AS habitable_status
    WHERE `status` IS NOT NULL AND `status` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_habitable_status`;
		TRUNCATE TABLE `char_habitable_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_habitable_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `habitable` = v_id WHERE normalize_text_ar(`habitable`) = v_name;
            UPDATE `test`.`data_case` SET `habitable` = v_id WHERE normalize_text_ar(`habitable`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_habitable` = v_id WHERE normalize_text_ar(`g_habitable`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_house_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `res_cond` FROM `test`.`data`
    WHERE `res_cond` IS NOT NULL AND `res_cond` NOT REGEXP '^[0-9]+$';
    /*SELECT DISTINCT `status` FROM
    (SELECT DISTINCT `res_cond` AS `status` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_res_cond` AS `status` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `res_cond` AS `status` FROM `test`.`data_case`) AS house_status;*/
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_house_status`;
		TRUNCATE TABLE `char_house_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_house_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `res_cond` = v_id WHERE  normalize_text_ar(`res_cond`) = v_name;
            -- UPDATE `test`.`data_case` SET `res_cond` = v_id WHERE normalize_text_ar(`res_cond`) = v_name;
			-- UPDATE `test`.`data_guardian` SET `g_habibatable` = v_id WHERE normalize_text_ar(`g_habibatable`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_kinship`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `g_kinship` FROM `test`.`data_guardian`
    WHERE `g_kinship` IS NOT NULL AND `g_kinship` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_kinship`;
		TRUNCATE TABLE `char_kinship_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_kinship_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data_guardian` SET `g_kinship` = v_id WHERE normalize_text_ar(`g_kinship`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_locations`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE country_id, district_id, city_id, neighborhood_id INT;
    DECLARE country_name, district_name, city_name, neighborhood_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `country`, `sp`, `city`, `local` FROM
    (SELECT DISTINCT `country`, `sp`, `city`, `local` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_country` AS `country`, `g_sp` AS `sp`, `g_city` AS `city`, `g_local` AS `local` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `country`, `sp`, `city`, `local` FROM `test`.`data_case`) AS locations
    WHERE `country` NOT REGEXP '^[0-9]+$'
    OR `sp` NOT REGEXP '^[0-9]+$'
    OR `city` NOT REGEXP '^[0-9]+$'
    OR`local` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_locations`;
		TRUNCATE TABLE `char_locations_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO country_name, district_name, city_name, neighborhood_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET country_id = NULL;
        IF country_name IS NOT NULL AND country_name NOT REGEXP '^[0-9]+$' THEN
			SET country_name = normalize_text_ar(country_name);
            
			CALL `char_locations_save`(country_id, 1, null, 1, country_name, false);
			UPDATE `test`.`data` SET `country` = country_id WHERE normalize_text_ar(`country`) = country_name;
            UPDATE `test`.`data_guardian` SET `g_country` = country_id WHERE normalize_text_ar(`g_country`) = country_name;
            UPDATE `test`.`data_case` SET `country` = country_id WHERE normalize_text_ar(`country`) = country_name;
		ELSEIF country_name REGEXP '^[0-9]+$' THEN
			SET country_id = country_name;
        END IF;
        
        SET district_id = NULL;
        IF country_id IS NOT NULL AND district_name IS NOT NULL AND district_name NOT REGEXP '^[0-9]+$' THEN
			SET district_name = normalize_text_ar(district_name);
            
			CALL `char_locations_save`(district_id, 2, country_id, 1, district_name, false);
			UPDATE `test`.`data` SET `sp` = district_id WHERE normalize_text_ar(`sp`) = district_name;
            UPDATE `test`.`data_guardian` SET `g_sp` = district_id WHERE normalize_text_ar(`g_sp`) = district_name;
            UPDATE `test`.`data_case` SET `sp` = district_id WHERE normalize_text_ar(`sp`) = district_name;
		ELSEIF district_name REGEXP '^[0-9]+$' THEN
			SET district_id = district_name;
        END IF;
        
        SET city_id = NULL;
        IF district_id IS NOT NULL AND city_name IS NOT NULL AND city_name NOT REGEXP '^[0-9]+$' THEN
			SET city_name = normalize_text_ar(city_name);
            
			CALL `char_locations_save`(city_id, 3, district_id, 1, city_name, false);
			UPDATE `test`.`data` SET `city` = city_id WHERE normalize_text_ar(`city`) = city_name;
            UPDATE `test`.`data_guardian` SET `g_city` = city_id WHERE normalize_text_ar(`g_city`) = city_name;
            UPDATE `test`.`data_case` SET `city` = city_id WHERE normalize_text_ar(`city`) = city_name;
		ELSEIF city_name REGEXP '^[0-9]+$' THEN
			SET city_id = city_name;
		END IF;
        
        SET neighborhood_id = NULL;
        IF city_id IS NOT NULL AND neighborhood_name IS NOT NULL AND neighborhood_name NOT REGEXP '^[0-9]+$' THEN
			SET neighborhood_name = normalize_text_ar(neighborhood_name);
            
			CALL `char_locations_save`(neighborhood_id, 4, city_id, 1, neighborhood_name, false);
			UPDATE `test`.`data` SET `local` = neighborhood_id WHERE normalize_text_ar(`local`) = neighborhood_name;
            UPDATE `test`.`data_guardian` SET `g_local` = neighborhood_id WHERE  normalize_text_ar(`g_local`) = neighborhood_name;
            UPDATE `test`.`data_case` SET `local` = neighborhood_id WHERE  normalize_text_ar(`local`) = neighborhood_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_marital_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `ms` FROM
    (SELECT DISTINCT `ms` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_ms` AS `ms` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `m_ms` AS `ms` FROM `test`.`data_mother`) AS ms
    WHERE `ms` IS NOT NULL AND `ms` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_marital_status`;
		TRUNCATE TABLE `char_marital_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_marital_status_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `ms` = v_id WHERE normalize_text_ar(`ms`) = v_name;
            UPDATE `test`.`data_mother` SET `m_ms` = v_id WHERE normalize_text_ar(`m_ms`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_ms` = v_id WHERE normalize_text_ar(`g_ms`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_property_types`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `property_type` FROM
    (SELECT DISTINCT `property_type` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_property_type` AS `ms` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `property_type` FROM `test`.`data_case`) AS property_types
    WHERE `property_type` IS NOT NULL AND `property_type` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_property_types`;
		TRUNCATE TABLE `char_property_types_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_property_types_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `property_type` = v_id WHERE  normalize_text_ar(`property_type`) = v_name;
            UPDATE `test`.`data_case` SET `property_type` = v_id WHERE normalize_text_ar(`property_type`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_property_type` = v_id WHERE normalize_text_ar(`g_property_type`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_roof_materials`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `roof` FROM
    (SELECT DISTINCT `roof` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_roof` AS `roof` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `roof` FROM `test`.`data_case`) AS roof_materials
    WHERE `roof` IS NOT NULL AND `roof` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_roof_materials`;
		TRUNCATE TABLE `char_roof_materials_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_roof_materials_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `roof` = v_id WHERE normalize_text_ar(`roof`) = v_name;
            UPDATE `test`.`data_case` SET `roof` = v_id WHERE normalize_text_ar(`roof`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_roof` = v_id WHERE normalize_text_ar(`g_roof`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_sponsorship_cases`(IN p_offset INT)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    
    DECLARE id, father_id, mother_id, guardian_id INT;
    
    DECLARE v_ID1 INT;
    DECLARE v_id VARCHAR(255);
    DECLARE v_sin VARCHAR(255);
    DECLARE v_name1 VARCHAR(255);
    DECLARE v_name2 VARCHAR(255);
    DECLARE v_name3 VARCHAR(255);
    DECLARE v_name4 VARCHAR(255);
    DECLARE v_ename1 VARCHAR(255);
    DECLARE v_ename2 VARCHAR(255);
    DECLARE v_ename3 VARCHAR(255);
    DECLARE v_ename4 VARCHAR(255);
    DECLARE v_gender INT;
    DECLARE v_national INT;
    DECLARE v_bp VARCHAR(255);
    DECLARE v_bd DATE;
    DECLARE v_study_type INT;
    DECLARE v_authority INT;
    DECLARE v_stage INT;
    DECLARE v_grade INT;
    DECLARE v_year VARCHAR(255);
    DECLARE v_school VARCHAR(255);
    DECLARE v_points VARCHAR(255);
    DECLARE v_edu_level INT;
    DECLARE v_health INT;
    DECLARE v_disease INT;
    DECLARE v_health_details VARCHAR(255);
    DECLARE v_praying INT;
    DECLARE v_unpray_reason VARCHAR(255);
    DECLARE v_quran INT;
    DECLARE v_quran_reason VARCHAR(255);
    DECLARE v_quran_chapters VARCHAR(255);
    DECLARE v_quran_surat VARCHAR(255);
    DECLARE v_quran_center VARCHAR(255);
    DECLARE v_needs VARCHAR(255);
    DECLARE v_skills VARCHAR(255);
    DECLARE v_country INT;
    DECLARE v_sp INT;
    DECLARE v_city INT;
    DECLARE v_local INT;
    DECLARE v_street VARCHAR(255);
    DECLARE v_mousqe VARCHAR(255);
    DECLARE v_property_type INT;
    DECLARE v_roof INT;
    DECLARE v_house_cond INT;
    DECLARE v_indoor_cond INT;
    DECLARE v_rooms INT;
    DECLARE v_habitable INT;
    DECLARE v_area FLOAT;
    DECLARE v_rent FLOAT;
    DECLARE v_bank INT;
    DECLARE v_branch INT;
    DECLARE v_acc_name VARCHAR(255);
    DECLARE v_acc_no VARCHAR(255);
    DECLARE v_sponsored VARCHAR(255);
    DECLARE v_category INT;
    DECLARE v_org_id INT;
    DECLARE v_org_name VARCHAR(255);
    DECLARE v_fullname VARCHAR(255);
    DECLARE v_f_id VARCHAR(255);
    DECLARE v_f_sin VARCHAR(255);
    DECLARE v_f_name1 VARCHAR(255);
    DECLARE v_f_name2 VARCHAR(255);
    DECLARE v_f_name3 VARCHAR(255);
    DECLARE v_f_name4 VARCHAR(255);
    DECLARE v_f_ename1 VARCHAR(255);
    DECLARE v_f_ename2 VARCHAR(255);
    DECLARE v_f_ename3 VARCHAR(255);
    DECLARE v_f_ename4 VARCHAR(255);
    DECLARE v_f_bd DATE;
    DECLARE v_spouses INT;
    DECLARE v_f_working INT;
    DECLARE v_f_job INT;
    DECLARE v_f_wp VARCHAR(255);
    DECLARE v_f_income FLOAT;
    DECLARE v_f_degree INT;
    DECLARE v_f_major VARCHAR(255);
    DECLARE v_f_dead VARCHAR(255);
    DECLARE v_f_dod DATE;
    DECLARE v_f_death_reason INT;
    DECLARE v_f_fullname VARCHAR(255);
    DECLARE v_g_id VARCHAR(255);
    DECLARE v_g_sin VARCHAR(255);
    DECLARE v_g_name1 VARCHAR(255);
    DECLARE v_g_name2 VARCHAR(255);
    DECLARE v_g_name3 VARCHAR(255);
    DECLARE v_g_name4 VARCHAR(255);
    DECLARE v_g_ename1 VARCHAR(255);
    DECLARE v_g_ename2 VARCHAR(255);
    DECLARE v_g_ename3 VARCHAR(255);
    DECLARE v_g_ename4 VARCHAR(255);
    DECLARE v_g_bd DATE;
    DECLARE v_g_ms INT;
    DECLARE v_g_national INT;
    DECLARE v_g_kinship INT;
    DECLARE v_g_working INT;
    DECLARE v_g_job INT;
    DECLARE v_g_wp VARCHAR(255);
    DECLARE v_g_income FLOAT;
    DECLARE v_g_degree INT;
    DECLARE v_g_stage INT;
    DECLARE v_g_country INT;
    DECLARE v_g_sp INT;
    DECLARE v_g_city INT;
    DECLARE v_g_street VARCHAR(255);
    DECLARE v_g_local INT;
    DECLARE v_g_mousqe VARCHAR(255);
    DECLARE v_g_property_type INT;
    DECLARE v_g_roof INT;
    DECLARE v_g_house_cond INT;
    DECLARE v_g_indoor_cond INT;
    DECLARE v_g_rooms INT;
    DECLARE v_g_habitable INT;
    DECLARE v_g_area FLOAT;
    DECLARE v_g_rent FLOAT;
    DECLARE v_g_mobile1 VARCHAR(255);
    DECLARE v_g_mobile2 VARCHAR(255);
    DECLARE v_g_phone VARCHAR(255);
    DECLARE v_g_bank INT;
    DECLARE v_g_branch INT;
    DECLARE v_g_acc_name VARCHAR(255);
    DECLARE v_g_acc_no VARCHAR(255);
    DECLARE v_g_fullname VARCHAR(255);
    DECLARE v_m_id VARCHAR(255);
    DECLARE v_m_sin VARCHAR(255);
    DECLARE v_m_name1 VARCHAR(255);
    DECLARE v_m_name2 VARCHAR(255);
    DECLARE v_m_name3 VARCHAR(255);
    DECLARE v_m_name4 VARCHAR(255);
    DECLARE v_m_name5 VARCHAR(255);
    DECLARE v_m_ename1 VARCHAR(255);
    DECLARE v_m_ename2 VARCHAR(255);
    DECLARE v_m_ename3 VARCHAR(255);
    DECLARE v_m_ename4 VARCHAR(255);
    DECLARE v_m_ename5 VARCHAR(255);
    DECLARE v_m_bd DATE;
    DECLARE v_m_ms INT;
    DECLARE v_m_national INT;
    DECLARE v_m_working INT;
    DECLARE v_m_job INT;
    DECLARE v_m_wp VARCHAR(255);
    DECLARE v_m_income FLOAT;
    DECLARE v_m_degree INT;
    DECLARE v_m_major VARCHAR(255);
    DECLARE v_m_health INT;
    DECLARE v_m_disease INT;
    DECLARE v_m_health_details VARCHAR(255);
    DECLARE v_m_dead VARCHAR(255);
    DECLARE v_m_dod DATE;
    DECLARE v_m_death_reason INT;
    DECLARE v_m_guardian VARCHAR(255);
    DECLARE v_family_count INT;
    DECLARE v_singles INT;
    DECLARE v_males INT;
    DECLARE v_females INT;
    DECLARE v_m_fullname VARCHAR(255);
    
    DECLARE cur CURSOR FOR
    SELECT `data_case`.`ID1`,
    `data_case`.`id`,
    `data_case`.`sin`,
    `data_case`.`name1`,
    `data_case`.`name2`,
    `data_case`.`name3`,
    `data_case`.`name4`,
    `data_case`.`ename1`,
    `data_case`.`ename2`,
    `data_case`.`ename3`,
    `data_case`.`ename4`,
    `data_case`.`gender`,
    `data_case`.`national`,
    `data_case`.`bp`,
    `data_case`.`bd`,
    `data_case`.`study_type`,
    `data_case`.`authority`,
    `data_case`.`stage`,
    `data_case`.`grade`,
    `data_case`.`year`,
    `data_case`.`school`,
    `data_case`.`points`,
    `data_case`.`edu_level`,
    `data_case`.`health`,
    `data_case`.`disease`,
    `data_case`.`health_details`,
    `data_case`.`praying`,
    `data_case`.`unpray_reason`,
    `data_case`.`quran`,
    `data_case`.`quran_reason`,
    `data_case`.`quran_chapters`,
    `data_case`.`quran_surat`,
    `data_case`.`quran_center`,
    `data_case`.`needs`,
    `data_case`.`skills`,
    `data_case`.`country`,
    `data_case`.`sp`,
    `data_case`.`city`,
    `data_case`.`local`,
    `data_case`.`street`,
    `data_case`.`mousqe`,
    `data_case`.`property_type`,
    `data_case`.`roof`,
    `data_case`.`house_cond`,
    `data_case`.`indoor_cond`,
    `data_case`.`rooms`,
    `data_case`.`habitable`,
    `data_case`.`area`,
    `data_case`.`rent`,
    `data_case`.`bank`,
    `data_case`.`branch`,
    `data_case`.`acc_name`,
    `data_case`.`acc_no`,
    `data_case`.`sponsored`,
    `data_case`.`category`,
    `data_case`.`org_id`,
    `data_case`.`org_name`,
    `data_case`.`fullname`,
    `data_father`.`f_id`,
    `data_father`.`f_sin`,
    `data_father`.`f_name1`,
    `data_father`.`f_name2`,
    `data_father`.`f_name3`,
    `data_father`.`f_name4`,
    `data_father`.`f_ename1`,
    `data_father`.`f_ename2`,
    `data_father`.`f_ename3`,
    `data_father`.`f_ename4`,
    `data_father`.`f_bd`,
    `data_father`.`spouses`,
    `data_father`.`f_working`,
    `data_father`.`f_job`,
    `data_father`.`f_wp`,
    `data_father`.`f_income`,
    `data_father`.`f_degree`,
    `data_father`.`f_major`,
    `data_father`.`f_dead`,
    `data_father`.`f_dod`,
    `data_father`.`f_death_reason`,
    `data_father`.`fullname` AS `f_fullname`,
    `data_guardian`.`g_id`,
    `data_guardian`.`g_sin`,
    `data_guardian`.`g_name1`,
    `data_guardian`.`g_name2`,
    `data_guardian`.`g_name3`,
    `data_guardian`.`g_name4`,
    `data_guardian`.`g_ename1`,
    `data_guardian`.`g_ename2`,
    `data_guardian`.`g_ename3`,
    `data_guardian`.`g_ename4`,
    `data_guardian`.`g_bd`,
    `data_guardian`.`g_ms`,
    `data_guardian`.`g_national`,
    `data_guardian`.`g_kinship`,
    `data_guardian`.`g_working`,
    `data_guardian`.`g_job`,
    `data_guardian`.`g_wp`,
    `data_guardian`.`g_income`,
    `data_guardian`.`g_degree`,
    `data_guardian`.`g_stage`,
    `data_guardian`.`g_country`,
    `data_guardian`.`g_sp`,
    `data_guardian`.`g_city`,
    `data_guardian`.`g_street`,
    `data_guardian`.`g_local`,
    `data_guardian`.`g_mousqe`,
    `data_guardian`.`g_property_type`,
    `data_guardian`.`g_roof`,
    `data_guardian`.`g_house_cond`,
    `data_guardian`.`g_indoor_cond`,
    `data_guardian`.`g_rooms`,
    `data_guardian`.`g_habitable`,
    `data_guardian`.`g_area`,
    `data_guardian`.`g_rent`,
    `data_guardian`.`g_mobile1`,
    `data_guardian`.`g_mobile2`,
    `data_guardian`.`g_phone`,
    `data_guardian`.`g_bank`,
    `data_guardian`.`g_branch`,
    `data_guardian`.`g_acc_name`,
    `data_guardian`.`g_acc_no`,
    `data_guardian`.`fullname` AS `g_fullname`,
    `data_mother`.`m_id`,
    `data_mother`.`m_sin`,
    `data_mother`.`m_name1`,
    `data_mother`.`m_name2`,
    `data_mother`.`m_name3`,
    `data_mother`.`m_name4`,
    `data_mother`.`m_name5`,
    `data_mother`.`m_ename1`,
    `data_mother`.`m_ename2`,
    `data_mother`.`m_ename3`,
    `data_mother`.`m_ename4`,
    `data_mother`.`m_ename5`,
    `data_mother`.`m_bd`,
    `data_mother`.`m_ms`,
    `data_mother`.`m_national`,
    `data_mother`.`m_working`,
    `data_mother`.`m_job`,
    `data_mother`.`m_wp`,
    `data_mother`.`m_income`,
    `data_mother`.`m_degree`,
    `data_mother`.`m_major`,
    `data_mother`.`m_health`,
    `data_mother`.`m_disease`,
    `data_mother`.`m_health_details`,
    `data_mother`.`m_dead`,
    `data_mother`.`m_dod`,
    `data_mother`.`m_death_reason`,
    `data_mother`.`m_guardian`,
    `data_mother`.`family_count`,
    `data_mother`.`singles`,
    `data_mother`.`males`,
    `data_mother`.`females`,
    `data_mother`.`fullname` AS `m_fullname`
FROM `test`.`data_case`
INNER JOIN  `test`.`data_father` ON `data_case`.`ID1` = `data_father`.`ID1`
INNER JOIN  `test`.`data_guardian` ON `data_case`.`ID1` = `data_guardian`.`ID1`
INNER JOIN  `test`.`data_mother` ON `data_case`.`ID1` = `data_mother`.`ID1`
WHERE `data_case`.`ID1` >= p_offset
ORDER BY `data_case`.`ID1`;

	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_ID1, 
		v_id , 
		v_sin , 
		v_name1 , 
		v_name2 , 
		v_name3 , 
		v_name4 , 
		v_ename1 , 
		v_ename2 , 
		v_ename3 , 
		v_ename4 , 
		v_gender, 
		v_national, 
		v_bp , 
		v_bd, 
		v_study_type, 
		v_authority, 
		v_stage, 
		v_grade, 
		v_year , 
		v_school , 
		v_points , 
		v_edu_level, 
		v_health, 
		v_disease, 
		v_health_details , 
		v_praying, 
		v_unpray_reason , 
		v_quran, 
		v_quran_reason , 
		v_quran_chapters , 
		v_quran_surat , 
		v_quran_center , 
		v_needs , 
		v_skills , 
		v_country, 
		v_sp, 
		v_city, 
		v_local, 
		v_street , 
		v_mousqe , 
		v_property_type, 
		v_roof, 
		v_house_cond, 
		v_indoor_cond, 
		v_rooms, 
		v_habitable, 
		v_area, 
		v_rent, 
		v_bank, 
		v_branch, 
		v_acc_name , 
		v_acc_no , 
		v_sponsored , 
		v_category, 
		v_org_id, 
		v_org_name , 
		v_fullname , 
		v_f_id , 
		v_f_sin , 
		v_f_name1 , 
		v_f_name2 , 
		v_f_name3 , 
		v_f_name4 , 
		v_f_ename1 , 
		v_f_ename2 , 
		v_f_ename3 , 
		v_f_ename4 , 
		v_f_bd, 
		v_spouses, 
		v_f_working, 
		v_f_job, 
		v_f_wp , 
		v_f_income, 
		v_f_degree, 
		v_f_major , 
		v_f_dead , 
		v_f_dod, 
		v_f_death_reason, 
		v_f_fullname , 
		v_g_id , 
		v_g_sin , 
		v_g_name1 , 
		v_g_name2 , 
		v_g_name3 , 
		v_g_name4 , 
		v_g_ename1 , 
		v_g_ename2 , 
		v_g_ename3 , 
		v_g_ename4 , 
		v_g_bd, 
		v_g_ms, 
		v_g_national, 
		v_g_kinship, 
		v_g_working, 
		v_g_job, 
		v_g_wp , 
		v_g_income, 
		v_g_degree, 
		v_g_stage, 
		v_g_country, 
		v_g_sp, 
		v_g_city, 
		v_g_street , 
		v_g_local, 
		v_g_mousqe , 
		v_g_property_type, 
		v_g_roof, 
		v_g_house_cond, 
		v_g_indoor_cond, 
		v_g_rooms, 
		v_g_habitable, 
		v_g_area, 
		v_g_rent, 
		v_g_mobile1 , 
		v_g_mobile2 , 
		v_g_phone , 
		v_g_bank, 
		v_g_branch, 
		v_g_acc_name , 
		v_g_acc_no , 
		v_g_fullname , 
		v_m_id , 
		v_m_sin , 
		v_m_name1 , 
		v_m_name2 , 
		v_m_name3 , 
		v_m_name4 , 
		v_m_name5 , 
		v_m_ename1 , 
		v_m_ename2 , 
		v_m_ename3 , 
		v_m_ename4 , 
		v_m_ename5 , 
		v_m_bd, 
		v_m_ms, 
		v_m_national, 
		v_m_working, 
		v_m_job, 
		v_m_wp , 
		v_m_income, 
		v_m_degree, 
		v_m_major , 
		v_m_health, 
		v_m_disease, 
		v_m_health_details , 
		v_m_dead , 
		v_m_dod, 
		v_m_death_reason, 
		v_m_guardian , 
		v_family_count, 
		v_singles, 
		v_males, 
		v_females, 
		v_m_fullname;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        SET father_id = null;
        CALL `char_persons_save`(father_id,
		v_f_name1, v_f_name2, v_f_name3, v_f_name4, null,
		v_f_sin,
		1, null, v_f_bd, null, 1,
		v_f_dod, v_f_death_reason,
		null, null, v_spouses,
		null, null, 
		null, null, null, null, null,
		null, v_f_fullname,
		v_f_ename1, v_f_ename2, v_f_ename3, v_f_ename4,
		null, null, null, null,
		null, null, null, null, v_f_major, v_f_degree, null,  null, null, null, null,
		null, null, null,
		null, null, null, null, null, null,
		null, null, null, null, v_f_job, null, v_f_wp,
		null, null, null, null, null, null, null, null, null,
		null, null, null);
        
        SET mother_id = null;
        CALL `char_persons_save`(mother_id,
		v_m_name1, v_m_name2, v_m_name3, v_m_name4, v_m_name5,
		v_m_sin,
		2, v_m_ms, v_m_bd, null, v_m_national,
		v_m_dod, v_m_death_reason,
		null, null, null,
		null, null, 
		null, null, null, null, null,
		v_m_income, v_m_fullname,
		v_m_ename1, v_m_ename2, v_m_ename3, v_m_ename4,
		null, null, null, null,
		null, null, null, null, v_m_major, v_m_degree, null,  null, null, null, null,
		v_m_health, v_m_health_details, v_m_disease,
		null, null, null, null, null, null,
		v_m_working, null, null, null, v_m_job, null, v_m_wp,
		null, null, null, null, null, null, null, null, null,
		null, null, null);
        
        IF v_m_guardian = 1 THEN
			SET guardian_id = mother_id;
            IF v_g_name1 IS NULL THEN SET v_g_name1 = v_m_name1; END IF;
            IF v_g_name2 IS NULL THEN SET v_g_name2 = v_m_name2; END IF;
            IF v_g_name3 IS NULL THEN SET v_g_name3 = v_m_name3; END IF;
            IF v_g_name4 IS NULL THEN SET v_g_name4 = v_m_name4; END IF;
            IF v_g_ename1 IS NULL THEN SET v_g_ename1 = v_m_ename1; END IF;
            IF v_g_ename2 IS NULL THEN SET v_g_ename2 = v_m_ename2; END IF;
            IF v_g_ename3 IS NULL THEN SET v_g_ename3 = v_m_ename3; END IF;
            IF v_g_ename4 IS NULL THEN SET v_g_ename4 = v_m_ename4; END IF;
            IF v_g_sin IS NULL THEN SET v_g_sin = v_m_sin; END IF;
            
            CALL `char_persons_save`(guardian_id,
			v_g_name1, v_g_name2, v_g_name3, v_g_name4, null,
			v_g_sin,
			2, ifnull(v_g_ms, v_m_ms), ifnull(v_g_bd, v_m_bd), null, ifnull(v_g_national, v_m_national),
			null, null,
			null, null, null,
			null, null, 
			v_g_country, v_g_sp, v_g_city, v_g_local, concat(v_g_street, ' -- ', v_g_mousqe),
			v_g_income, ifnull(v_g_fullname, v_m_fullname),
			v_g_ename1, v_g_ename2, v_g_ename3, v_g_ename4,
			v_g_bank, v_g_acc_no, v_g_acc_name, v_g_branch,
			null, null, null, v_g_stage, v_m_major, v_g_degree, null, null, null, null, null,
			v_m_health, v_m_health_details, v_m_disease,
			null, null, null, null, null, null,
			v_g_working, null, null, null, v_g_job, null, v_g_wp,
			v_g_property_type, v_g_rent, v_g_roof, null, v_g_indoor_cond, v_g_habitable, v_g_house_cond, v_g_rooms, v_g_area,
			v_g_mobile1, v_g_mobile2, v_g_phone);
            
        ELSE
			SET guardian_id = null;
            CALL `char_persons_save`(guardian_id,
			v_g_name1, v_g_name2, v_g_name3, v_g_name4, null,
			v_g_sin,
			null, v_g_ms, v_g_bd, null, v_g_national,
			null, null,
			null, null, null,
			null, null, 
			v_g_country, v_g_sp, v_g_city, v_g_local, concat(v_g_street, ' -- ', v_g_mousqe),
			v_g_income, v_g_fullname,
			v_g_ename1, v_g_ename2, v_g_ename3, v_g_ename4,
			v_g_bank, v_g_acc_no, v_g_acc_name, v_g_branch,
			null, null, null, v_g_stage, null, v_g_degree, null, null, null, null, null,
			null, null, null,
			null, null, null, null, null, null,
			v_g_working, null, null, null, v_g_job, null, v_g_wp,
			v_g_property_type, v_g_rent, v_g_roof, null, v_g_indoor_cond, v_g_habitable, v_g_house_cond, v_g_rooms, v_g_area,
			v_g_mobile1, v_g_mobile2, v_g_phone);
		END IF;
        
        SET id = null;
        CALL `char_persons_save`(id,
		v_name1, v_name2, v_name3, v_name4, null,
		v_sin,
		v_gender, null, v_bd, v_bp, v_national,
		null, null,
		father_id, mother_id, null,
		null, null, 
		v_country, v_sp, v_city, v_local, concat(v_street, ' -- ', v_mousqe),
		null, v_fullname,
		v_ename1, v_ename2, v_ename3, v_ename4,
		v_bank, v_acc_no, v_acc_name, v_branch,
		null, v_study_type, v_authority, v_stage, null, null, v_grade,  v_year, v_school, v_edu_level, v_points,
		v_health, v_health_details, v_disease,
		v_praying, v_quran_center, v_quran_chapters, v_quran_surat, v_unpray_reason, v_quran_reason,
		null, null, null, null, null, null, null,
		v_property_type, v_rent, v_roof, null, v_indoor_cond, v_habitable, v_house_cond, v_rooms, v_area,
		null, null, null);
		
        INSERT INTO `char_cases`
		(`person_id`,
		`organization_id`,
		`category_id`,
		`status`,
		`rank`,
		`user_id`,
		`created_at`)
		VALUES
		(id,
		v_org_id,
		v_category,
		0,
		0,
		1,
		NOW());
        
		REPLACE INTO `char_guardians`
		(`guardian_id`,
		`individual_id`,
		`kinship_id`,
		`organization_id`)
		VALUES
		(guardian_id,
		id,
		v_g_kinship,
		v_org_id);

		REPLACE INTO `char_persons_kinship`
		(`l_person_id`,
		`r_person_id`,
		`kinship_id`)
		VALUES
		(guardian_id,
		id,
		v_g_kinship);
        
	END LOOP;
    CLOSE cur;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_work_jobs`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `job` FROM
    (SELECT DISTINCT `job` FROM `test`.`data`
    UNION
    SELECT DISTINCT `g_job` AS `job` FROM `test`.`data_guardian`
    UNION
    SELECT DISTINCT `m_job` AS `job` FROM `test`.`data_mother`
    UNION
    SELECT DISTINCT `f_job` AS `job` FROM `test`.`data_father`) AS work_jobs
    WHERE `job` IS NOT NULL AND `job` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_jobs`;
		TRUNCATE TABLE `char_work_jobs_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_jobs_save`(v_id, 0, 1, v_name, false);
            UPDATE `test`.`data` SET `job` = v_id WHERE normalize_text_ar(`job`) = v_name;
            UPDATE `test`.`data_mother` SET `m_job` = v_id WHERE normalize_text_ar(`m_job`) = v_name;
			UPDATE `test`.`data_guardian` SET `g_job` = v_id WHERE normalize_text_ar(`g_job`) = v_name;
            UPDATE `test`.`data_father` SET `f_job` = v_id WHERE normalize_text_ar(`f_job`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_work_reasons`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `unwork_reason` FROM `test`.`data`
    WHERE `unwork_reason` IS NOT NULL AND `unwork_reason` NOT REGEXP '^[0-9]+$';
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_reasons`;
		TRUNCATE TABLE `char_work_reasons_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_reasons_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data` SET `unwork_reason` = v_id WHERE normalize_text_ar(`unwork_reason`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `import_work_status`(IN p_truncate BOOL)
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT DISTINCT `work_status` FROM `test`.`data` WHERE `work_status` IS NOT NULL;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;
    
    IF p_truncate = true THEN
		SET foreign_key_checks = 0;
		TRUNCATE TABLE `char_work_status`;
		TRUNCATE TABLE `char_work_status_i18n`;
		SET foreign_key_checks = 1;
	END IF;
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_name;
        IF done THEN
			LEAVE read_loop;
		END IF;
        IF v_name IS NOT NULL THEN
			SET v_name = normalize_text_ar(v_name);
            SET v_id = NULL;
            
			CALL `char_work_status_save`(v_id, 0, 1, v_name, false);
			UPDATE `test`.`data` SET `work_status` = v_id WHERE normalize_text_ar(`work_status`) = v_name;
		END IF;
	END LOOP;
    
    CLOSE cur;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `remove_case_duplicates`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id1, v_id2, v_org_id1, v_org_id2 INT;
    DECLARE v_sin VARCHAR(45);
	
    DECLARE cur CURSOR FOR SELECT c1.id, c2.id from `test`.`data_case` c1 inner join `test`.`data_case` c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname = c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur2 CURSOR FOR SELECT c1.id, c1.org_id, c2.id, c2.org_id, c1.sin
    from `test`.`data_case` c1 inner join `test`.`data_case` c2
	ON c1.sin = c2.sin AND c1.id <> c2.id AND c1.fullname = c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id < c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null;
    
    DECLARE cur3 CURSOR FOR SELECT c1.ID1, c2.ID1
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id < c2.id
    AND c1.sin <> 0 AND c1.sin is not Null
    AND clear_text(c1.name1) = clear_text(c2.name1)
    ORDER BY c1.id;
    
    DECLARE cur4 CURSOR FOR SELECT c1.ID1, c1.org_id, c2.ID1, c2.org_id, c1.sin
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id > c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null
    AND clear_text(c1.name1) = clear_text(c2.name1);
    
    DECLARE cur5 CURSOR FOR SELECT c1.ID1, c2.ID1
    from test.data c1 inner join test.data c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id = c2.org_id
	WHERE c1.id > c2.id
    AND c1.sin <> 0 AND c1.sin is not Null
    ORDER BY c1.id;
    
    DECLARE cur6 CURSOR FOR SELECT c1.ID1, c1.org_id, c2.ID1, c2.org_id, c1.sin
    from `test`.`data` c1 inner join `test`.`data` c2
	ON c1.sin = c2.sin AND c1.fullname <> c2.fullname AND c1.org_id <> c2.org_id
	WHERE c1.org_id > c2.org_id
    AND c1.sin <> 0 AND c1.sin is not Null;

    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    /*OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        DELETE FROM `test`.`data_case` WHERE id = v_id2;
	END LOOP;
    CLOSE cur;*/
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        DELETE FROM `test`.`data_case` WHERE id = v_id2;
        INSERT INTO `test`.`data_case_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_case_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur2;
    
    /*SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop3;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
	END LOOP;
    CLOSE cur3;*/
    
    /*SET done = false;
    OPEN cur4;
    read_loop4: LOOP
		FETCH cur4 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop4;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur4;*/
    
    /*SET done = false;
    OPEN cur5;
    read_loop5: LOOP
		FETCH cur5 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop5;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
	END LOOP;
    CLOSE cur5;*/
    
    /*SET done = false;
    OPEN cur6;
    read_loop6: LOOP
		FETCH cur6 INTO v_id1, v_org_id1, v_id2, v_org_id2, v_sin;
        IF done THEN
			LEAVE read_loop6;
		END IF;
        
        DELETE FROM `test`.`data` WHERE ID1 = v_id2;
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id1, v_org_id1, v_id2, v_org_id2, v_sin);
        INSERT INTO `test`.`data_duplicates` (id1, org_id1, id2, org_id2, sin)
        VALUES(v_id2, v_org_id2, v_id1, v_org_id1, v_sin);
	END LOOP;
    CLOSE cur6;*/
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  PROCEDURE `remove_duplicates`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
    DECLARE v_id1, v_id2 INT;
    DECLARE v_name VARCHAR(255);
      
    DECLARE cur CURSOR FOR SELECT f1.f_id, f2.f_id from test.data_father f1 inner join test.data_father f2
	ON f1.f_sin = f2.f_sin AND f1.fullname = f2.fullname AND f1.f_id <> f2.f_id
	WHERE f1.f_id > f2.f_id;
    
	DECLARE cur2 CURSOR FOR SELECT m1.m_id, m2.m_id from test.data_mother m1 inner join test.data_mother m2
	ON  m1.m_sin = m2.m_sin AND m1.fullname = m2.fullname AND m1.m_id <> m2.m_id
	WHERE m1.m_id > m2.m_id;
    
    DECLARE cur3 CURSOR FOR SELECT g1.g_id, g2.g_id from test.data_guardian g1 inner join test.data_guardian g2
	ON g1.g_sin = g2.g_sin AND g1.fullname = g2.fullname AND g1.g_id <> g2.g_id
	WHERE g1.g_id > g2.g_id;
    
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    /*DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
	BEGIN
		ROLLBACK;
	END;*/
    
    START TRANSACTION;

    OPEN cur;
    read_loop: LOOP
		FETCH cur INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop;
		END IF;
        
        DELETE FROM test.data_father WHERE f_id = v_id2;
        UPDATE test.data_case SET f_id = v_id1 WHERE f_id = v_id2;
	END LOOP;
    CLOSE cur;
    
    SET done = false;
    OPEN cur2;
    read_loop2: LOOP
		FETCH cur2 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop2;
		END IF;
        
        DELETE FROM test.data_mother WHERE m_id = v_id2;
        UPDATE test.data_case SET m_id = v_id1 WHERE m_id = v_id2;
	END LOOP;
    CLOSE cur2;
    
    SET done = false;
    OPEN cur3;
    read_loop3: LOOP
		FETCH cur3 INTO v_id1, v_id2;
        IF done THEN
			LEAVE read_loop3;
		END IF;
        
        DELETE FROM test.data_guardian WHERE g_id = v_id2;
        UPDATE test.data_case SET g_id = v_id1 WHERE g_id = v_id2;
	END LOOP;
    CLOSE cur3;
    
    COMMIT;
END$$
DELIMITER ;

DELIMITER $$
CREATE  FUNCTION `normalize_text_ar`(p_text TEXT) RETURNS text CHARSET utf8
BEGIN
	
	DECLARE v_comma VARCHAR(255) DEFAULT '،';
	DECLARE v_semicolon VARCHAR(255) DEFAULT '؛';
	DECLARE v_question VARCHAR(255) DEFAULT '؟';
	DECLARE v_hamza VARCHAR(255) DEFAULT 'ء';
	DECLARE v_alef_madda VARCHAR(255) DEFAULT 'آ';
	DECLARE v_alef_hamza_above VARCHAR(255) DEFAULT 'أ';
	DECLARE v_waw_hamza VARCHAR(255) DEFAULT 'ؤ';
	DECLARE v_alef_hamza_below VARCHAR(255) DEFAULT 'إ';
	DECLARE v_yeh_hamza VARCHAR(255) DEFAULT 'ئ';
	DECLARE v_alef VARCHAR(255) DEFAULT 'ا';
	DECLARE v_beh VARCHAR(255) DEFAULT 'ب';
	DECLARE v_teh_marbuta VARCHAR(255) DEFAULT 'ة';
	DECLARE v_teh VARCHAR(255) DEFAULT 'ت';
	DECLARE v_theh VARCHAR(255) DEFAULT 'ث';
	DECLARE v_jeem VARCHAR(255) DEFAULT 'ج';
	DECLARE v_hah VARCHAR(255) DEFAULT 'ح';
	DECLARE v_khah VARCHAR(255) DEFAULT 'خ';
	DECLARE v_dal VARCHAR(255) DEFAULT 'د';
	DECLARE v_thal VARCHAR(255) DEFAULT 'ذ';
	DECLARE v_reh VARCHAR(255) DEFAULT 'ر';
	DECLARE v_zain VARCHAR(255) DEFAULT 'ز';
	DECLARE v_seen VARCHAR(255) DEFAULT 'س';
	DECLARE v_sheen VARCHAR(255) DEFAULT 'ش';
	DECLARE v_sad VARCHAR(255) DEFAULT 'ص';
	DECLARE v_dad VARCHAR(255) DEFAULT 'ض';
	DECLARE v_tah VARCHAR(255) DEFAULT 'ط';
	DECLARE v_zah VARCHAR(255) DEFAULT 'ظ';
	DECLARE v_ain VARCHAR(255) DEFAULT 'ع';
	DECLARE v_ghain VARCHAR(255) DEFAULT 'غ';
	DECLARE v_tatweel VARCHAR(255) DEFAULT 'ـ';
	DECLARE v_feh VARCHAR(255) DEFAULT 'ف';
	DECLARE v_qaf VARCHAR(255) DEFAULT 'ق';
	DECLARE v_kaf VARCHAR(255) DEFAULT 'ك';
	DECLARE v_lam VARCHAR(255) DEFAULT 'ل';
	DECLARE v_meem VARCHAR(255) DEFAULT 'م';
	DECLARE v_noon VARCHAR(255) DEFAULT 'ن';
	DECLARE v_heh VARCHAR(255) DEFAULT 'ه';
	DECLARE v_waw VARCHAR(255) DEFAULT 'و';
	DECLARE v_alef_maksura VARCHAR(255) DEFAULT 'ى';
	DECLARE v_yeh VARCHAR(255) DEFAULT 'ي';
	DECLARE v_madda_above VARCHAR(255) DEFAULT 'ٓ';
	DECLARE v_hamza_above VARCHAR(255) DEFAULT 'ٔ';
	DECLARE v_hamza_below VARCHAR(255) DEFAULT 'ٕ';
	DECLARE v_zero VARCHAR(255) DEFAULT '٠';
	DECLARE v_one VARCHAR(255) DEFAULT '١';
	DECLARE v_two VARCHAR(255) DEFAULT '٢';
	DECLARE v_three VARCHAR(255) DEFAULT '٣';
	DECLARE v_four VARCHAR(255) DEFAULT '٤';
	DECLARE v_five VARCHAR(255) DEFAULT '٥';
	DECLARE v_six VARCHAR(255) DEFAULT '٦';
	DECLARE v_seven VARCHAR(255) DEFAULT '٧';
	DECLARE v_eight VARCHAR(255) DEFAULT '٨';
	DECLARE v_nine VARCHAR(255) DEFAULT '٩';
	DECLARE v_percent VARCHAR(255) DEFAULT '٪';
	DECLARE v_decimal VARCHAR(255) DEFAULT '٫';
	DECLARE v_thousands VARCHAR(255) DEFAULT '٬';
	DECLARE v_star VARCHAR(255) DEFAULT '٭';
	DECLARE v_mini_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_alef_wasla VARCHAR(255) DEFAULT 'ٱ';
	DECLARE v_full_stop VARCHAR(255) DEFAULT '۔';
	DECLARE v_byte_order_mark VARCHAR(255) DEFAULT '﻿';
	DECLARE v_fathatan VARCHAR(255) DEFAULT 'ً';
	DECLARE v_dammatan VARCHAR(255) DEFAULT 'ٌ';
	DECLARE v_kasratan VARCHAR(255) DEFAULT 'ٍ';
	DECLARE v_fatha VARCHAR(255) DEFAULT 'َ';
	DECLARE v_damma VARCHAR(255) DEFAULT 'ُ';
	DECLARE v_kasra VARCHAR(255) DEFAULT 'ِ';
	DECLARE v_shadda VARCHAR(255) DEFAULT 'ّ';
	DECLARE v_sukun VARCHAR(255) DEFAULT 'ْ';
	DECLARE v_small_alef VARCHAR(255) DEFAULT 'ٰ';
	DECLARE v_small_waw VARCHAR(255) DEFAULT 'ۥ';
	DECLARE v_small_yeh VARCHAR(255) DEFAULT 'ۦ';
	DECLARE v_lam_alef VARCHAR(255) DEFAULT 'ﻻ';
	DECLARE v_lam_alef_hamza_above VARCHAR(255) DEFAULT 'ﻷ';
	DECLARE v_lam_alef_hamza_below VARCHAR(255) DEFAULT 'ﻹ';
	DECLARE v_lam_alef_madda_above VARCHAR(255) DEFAULT 'ﻵ';
	DECLARE v_simple_lam_alef VARCHAR(255) DEFAULT 'لَا';
	DECLARE v_simple_lam_alef_hamza_above VARCHAR(255) DEFAULT 'لأ';
	DECLARE v_simple_lam_alef_hamza_below VARCHAR(255) DEFAULT 'لإ';
	DECLARE v_simple_lam_alef_madda_above VARCHAR(255) DEFAULT 'لءَا';
    
    -- Stripe Tatweel
    SET p_text = REPLACE(p_text, v_tatweel, '');
    
    -- Strip Tashkeel
    SET p_text = REPLACE(p_text, v_fathatan, '');
    SET p_text = REPLACE(p_text, v_dammatan, '');
    SET p_text = REPLACE(p_text, v_kasratan, '');
    SET p_text = REPLACE(p_text, v_fatha, '');
    SET p_text = REPLACE(p_text, v_damma, '');
    SET p_text = REPLACE(p_text, v_kasra, '');
    SET p_text = REPLACE(p_text, v_shadda, '');
    SET p_text = REPLACE(p_text, v_sukun, '');
    
    -- Normalize Hamza
    SET p_text = REPLACE(p_text, v_waw_hamza, v_waw);
    SET p_text = REPLACE(p_text, v_yeh_hamza, v_yeh);
    SET p_text = REPLACE(p_text, v_alef_madda, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_alef_hamza_below, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_above, v_alef);
    SET p_text = REPLACE(p_text, v_hamza_below, v_alef);
    
    -- Normalize Lam Alef
    SET p_text = REPLACE(p_text, v_lam_alef, v_simple_lam_alef);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_above, v_simple_lam_alef_hamza_above);
    SET p_text = REPLACE(p_text, v_lam_alef_hamza_below, v_simple_lam_alef_hamza_below);
    SET p_text = REPLACE(p_text, v_lam_alef_madda_above, v_simple_lam_alef_madda_above);
    
    -- Normalize Teh Marbuta
    SET p_text = REPLACE(p_text, v_teh_marbuta, v_heh);
    
    WHILE instr(p_text, '  ') > 0 do
		SET p_text = replace(p_text, '  ', ' ');
	END WHILE;
        
RETURN trim(p_text);
END$$
DELIMITER ;
