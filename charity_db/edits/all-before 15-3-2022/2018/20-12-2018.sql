
INSERT INTO `app_modules` (`id`, `code`, `name`, `description`, `status`) VALUES(10, 'reports', 'تفارير واستعلامات', '', 1);
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (378, '10', 'reports.case.islamicCommitmentStatistic', 'إحصائية الإلتزام الديني');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (379, '10', 'reports.case.sponsorshipsStatistic', 'إحصائية كفالات الجهات الكافلة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (380, '10', 'reports.case.vouchersStatistic', 'احصائية الجهات الممولة للقسائم');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (381, '10', 'reports.case.charityAct', 'احصائية العمل الخيري')
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (382, '10', 'reports.case.generalSearch', 'البحث العام');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (383, '10', 'reports.case.guardiansSearch', 'البحث في بيانات المعيلين');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (384, '10', 'reports.case.sponsorshipSearch', 'البحث في كفالات الأيتام');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.ExportCitizenRepository', 'تصدير نتائج الاستعلام في السجل المدني');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.citizenRepository', 'الاستعلام في السجل المدني');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.socialAffairs', 'الاستعلام في الشئون الاجتماعية');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.ExportSocialAffairs', 'تصدير نتائج الاستعلام في الشئون الاجتماعية');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.aidsCommittee', 'الاستعلام في لجان الزكاة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.ExportAidsCommittee', 'تصدير نتائج الاستعلام في لجان الزكاة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.case.RelayCitizenRepository', 'ترحيل الحالة - الاستعلام في السجل المدني');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '3','aid.case.selectCustomForm', 'تعيين النموذج المخصص');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '4','sponsorship.case.selectCustomForm', 'تعيين النموذج المخصص');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '3','aid.case.changeStatus', 'تعديل الحالة للافراد');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '4','sponsorship.case.changeStatus', 'تعديل الحالة للافراد');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '3','aid.case.import', 'استيراد حالات وأفراد عائلة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '4','sponsorship.case.import', 'استيراد حالات وأفراد عائلة');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.relays.confirm', 'اعتماد المرحلين ');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.relays.manageConfirm', 'إدارة المرحلين المعتمدين');
INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES (Null, '10','reports.relays.manage', 'إدارة المرحلين غير المعتمدين');

ALTER TABLE `char_persons`
    ADD  `mosques_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المسجد)',
    ADD  `adscountry_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (الدولة)',
    ADD  `adsdistrict_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المحافظة)',
    ADD  `adsregion_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المنطقة)',
    ADD  `adsneighborhood_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (الحي)',
    ADD  `adssquare_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المربع)',
    ADD  `adsmosques_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'عنوان الشخص (المسجد)';

ALTER TABLE `char_persons`
  ADD KEY `fk_persons_mosques_id_idx` (`mosques_id`),
  ADD KEY `fk_persons_adscountry_id_idx` (`adscountry_id`),
  ADD KEY `fk_persons_adsdistrict_id_idx` (`adsdistrict_id`),
  ADD KEY `fk_persons_adsregion_id_idx` (`adsregion_id`),
  ADD KEY `fk_persons_adsneighborhood_id_idx` (`adsneighborhood_id`),
  ADD KEY `fk_persons_adssquare_id_idx` (`adssquare_id`),
  ADD KEY `fk_persons_adsmosques_id_idx` (`adsmosques_id`);

ALTER TABLE `char_persons`
  ADD CONSTRAINT `fk_persons_mosques_id`         FOREIGN KEY (`mosques_id`)         REFERENCES `char_locations`,
  ADD CONSTRAINT `fk_persons_adscountry_id`      FOREIGN KEY (`adscountry_id`)      REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_persons_adsdistrict_id`     FOREIGN KEY (`adsdistrict_id`)     REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_persons_adsregion_id`       FOREIGN KEY (`adsregion_id`)       REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_persons_adsneighborhood_id` FOREIGN KEY (`adsneighborhood_id`) REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_persons_adssquare_id`       FOREIGN KEY (`adssquare_id`)       REFERENCES `char_aids_locations` (`id`),
  ADD CONSTRAINT `fk_persons_adsmosques_id`      FOREIGN KEY (`adsmosques_id`)      REFERENCES `char_aids_locations` (`id`);


CREATE TABLE `char_relays` (
  `id` int(10) UNSIGNED NOT NULL,
  `person_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط الاشخاص المرحلين مع التصنيف (كفالات ومساعدات)';

ALTER TABLE `char_relays`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_relays_personId_idx` (`person_id`),
  ADD KEY `fk_relays_organizationId_idx` (`organization_id`),
  ADD KEY `fk_relays_userId_idx` (`user_id`),
  ADD KEY `fk_relays_categoryId_idx` (`category_id`);

ALTER TABLE `char_relays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `char_relays`
  ADD CONSTRAINT `fk_relays_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_relays_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_relays_personId` FOREIGN KEY (`person_id`) REFERENCES `char_persons` (`id`),
  ADD CONSTRAINT `fk_relays_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE TABLE `char_relays_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `relay_id` int(10) UNSIGNED NOT NULL,
  `organization_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='جدول لربط الاشخاص المرحلين مع الجمعيات المرحلة';

ALTER TABLE `char_relays_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_relays_log_relayId_idx` (`relay_id`),
  ADD KEY `fk_relays_log_organizationId_idx` (`organization_id`),
  ADD KEY `fk_relays_log_userId_idx` (`user_id`),
  ADD KEY `fk_relays_log_categoryId_idx` (`category_id`);

ALTER TABLE `char_relays_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `char_relays_log`
  ADD CONSTRAINT `fk_relays_log_categoryId` FOREIGN KEY (`category_id`) REFERENCES `char_categories` (`id`),
  ADD CONSTRAINT `fk_relays_log_organizationId` FOREIGN KEY (`organization_id`) REFERENCES `char_organizations` (`id`),
  ADD CONSTRAINT `fk_relays_log_relayId` FOREIGN KEY (`relay_id`) REFERENCES `char_relays` (`id`),
  ADD CONSTRAINT `fk_relays_log_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `char_organizations` ADD `level` tinyint(4) DEFAULT NULL COMMENT '1- أم 2- منطقة 3-جمعية',
ALTER TABLE `app_modules` ADD `display` tinyint(4) DEFAULT '1';
ALTER TABLE `char_organizations` ADD `level` TINYINT NULL AFTER `name`, ADD `deleted` TINYINT NOT NULL DEFAULT '1' AFTER `level`;
ALTER TABLE `char_users` ADD `os_deleted` TINYINT NOT NULL DEFAULT '1' AFTER `status`;

ALTER TABLE `char_roles`
 ADD `user_id` int(10) UNSIGNED default NULL,
  ADD KEY `fk_roles_userId_idx` (`user_id`),
  ADD CONSTRAINT `fk_roles_userId` FOREIGN KEY (`user_id`) REFERENCES `char_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;



CREATE ALGORITHM=UNDEFINED  SQL SECURITY DEFINER VIEW `char_relays_view`  AS
select
`char_cases`.`id` AS `case_id`,
`char_categories`.`type` AS `category_type`,
concat(ifnull(`char_users`.`firstname`,' '),' ',ifnull(`char_users`.`lastname`,' ')) AS `user_name`,
`char_persons`.`id_card_number` AS `id_card_number`,
concat(ifnull(`char_persons`.`first_name`,' '),' ',ifnull(`char_persons`.`second_name`,' '),' ',
       ifnull(`char_persons`.`third_name`,' '),' ',ifnull(`char_persons`.`last_name`,' ')) AS `full_name`,
       ifnull(`mobNo`.`contact_value`,'-') as primary_mobile,
       ifnull(`phoNo`.`contact_value`,'-') as phone,
`char_persons`.`first_name` AS `first_name`,`char_persons`.`second_name` AS `second_name`,`char_persons`.`third_name` AS `third_name`,`char_persons`.`last_name` AS `last_name`,
`char_persons`.`prev_family_name` AS `prev_family_name`,`char_persons`.`gender` AS `gender`,`char_persons`.`marital_status_id` AS `marital_status_id`,
`char_persons`.`birthday` AS `birthday`,
`char_persons`.`country` AS `country`,`char_persons`.`governarate` AS `governarate`,`char_persons`.`city` AS `city`,
`char_persons`.`location_id` AS `location_id`,`char_persons`.`mosques_id` AS `mosques_id`,
`char_persons`.`street_address` AS `street_address`,`char_persons`.`adscountry_id` AS `adscountry_id`,
`char_persons`.`adsdistrict_id` AS `adsdistrict_id`,
`char_persons`.`adsregion_id` AS `adsregion_id`,`char_persons`.`adsneighborhood_id` AS `adsneighborhood_id`,
`char_persons`.`adssquare_id` AS `adssquare_id`,`char_persons`.`adsmosques_id` AS `adsmosques_id`,
`char_relays`.`id` AS `id`,`char_relays`.`person_id` AS `person_id`,`char_relays`.`organization_id` AS `organization_id`,
`char_relays`.`category_id` AS `category_id`,`char_relays`.`user_id` AS `user_id`,
`char_relays`.`created_at` AS `created_at`,`char_relays`.`updated_at` AS `updated_at`,
`char_relays`.`deleted_at` AS `deleted_at`,`char_categories`.`name` AS `category_name`

from `char_relays`
join `char_categories` on`char_categories`.`id` = `char_relays`.`category_id`
join `char_persons` on`char_persons`.`id` = `char_relays`.`person_id`
left join `char_persons_contact` `mobNo` on ((`mobNo`.`person_id` = `char_relays`.`person_id`) and ( `mobNo`.`contact_type` = 'primary_mobile'))
left join `char_persons_contact` `phoNo` on ((`phoNo`.`person_id` = `char_relays`.`person_id`) and ( `phoNo`.`contact_type` = 'phone'))

join `char_organizations` on`char_organizations`.`id` = `char_relays`.`organization_id`
join `char_users` on  `char_users`.`id` = `char_relays`.`user_id`
left join `char_cases` on
 (
   (`char_cases`.`person_id` = `char_relays`.`person_id`) and
   (`char_cases`.`category_id` = `char_relays`.`category_id`) and
   (`char_cases`.`organization_id` = 1)
  )
  order by `char_relays`.`created_at` desc ;
