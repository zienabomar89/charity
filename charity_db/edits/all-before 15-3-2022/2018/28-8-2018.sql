UPDATE `char_persons` SET location_id =305 where location_id =311 or location_id = 305

INSERT INTO `char_permissions` (`id`, `module_id`, `code`, `name`) VALUES
(NULL, '8', 'org.organizations.connectLocations', 'ربط الجمعيات بالأحياء والمناطق');

ALTER TABLE `char_vouchers` ADD `currency_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `title`;
ALTER TABLE `char_vouchers` ADD KEY `fk_vouchers_currency_id_idx` (`currency_id`);
ALTER TABLE `char_vouchers` ADD CONSTRAINT `fk_vouchers_currency_id_fk` FOREIGN KEY (`currency_id`) REFERENCES `char_currencies` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE `char_vouchers` ADD `status` TINYINT(3) NOT NULL DEFAULT '0' AFTER `urgent`;
UPDATE `char_vouchers` SET `status`=1 WHERE 1;

DELIMITER $$

DROP FUNCTION `get_cases_count`;
CREATE  FUNCTION `get_cases_count`(`organization_id` INT, `location_id` INT) RETURNS INT(11) NOT DETERMINISTIC CONTAINS SQL SQL SECURITY DEFINER
BEGIN
declare c INTEGER;

 set c = (
  				 SELECT COUNT(`char_cases`.`person_id`)
  				 FROM `char_cases`
  				 join `char_persons` ON (`char_cases`.`person_id` = `char_persons`.`id` and `char_persons`.`location_id` = location_id )
			     WHERE `char_cases`.`status` = 0 and `char_cases`.`organization_id` = organization_id
			   );


RETURN c;
END
END$$
