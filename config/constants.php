<?php

return [
    'locale' => 'ar',
    'records_per_page' => 50,
    'date_format' =>'dd-MM-yyyy',
    'logo_dir' => 'org_logo/',
    'allowedExtForImage' => 'image/png,image/jpg,image/jpeg,image/gif'

];